Group init:
	 [x] Step 1: check inputs
	 [x] Step 2: Sensors pre-processing
	 [x] Step 3: Generate a common masks for each sensors
	 [x] Step 4: Compute validity raster by tile
Group sampling:
	 [x] Step 5: Generate tile's envelope
	 [x] Step 6: Generate a region vector
	 [x] Step 7: Prepare samples
	 [x] Step 8: merge samples by models
	 [x] Step 9: Generate samples statistics by models
	 [x] Step 10: Select pixels in learning polygons by models
	 [x] Step 11: Split pixels selected to learn models by tiles
	 [x] Step 12: Extract pixels values by tiles
	 [x] Step 13: Merge samples dedicated to the same model
Group learning:
	 [x] Step 14: Learn model
Group classification:
	 [x] Step 15: Generate classifications
Group mosaic:
	 [x] Step 16: Mosaic
Group validation:
	 [x] Step 17: generate confusion matrix by tiles
	 [x] Step 18: Merge all confusions
	 [x] Step 19: Generate final report

