#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Module used to print methods available to access data when writing external features code
"""
from iota2.common import custom_numpy_features as CNF
from iota2.common.custom_numpy_features import CustomFeaturesConfiguration
from iota2.configuration_files import read_config_file as rcf


def print_get_methods(config_file: str, tile: str = "T31TCJ"):
    """
    This function prints the list of methods available to access data
    """
    # At this stage, the tile name does not matter as it only used to
    # create the dictionary. This dictionary is the same for all tiles.
    sensors_parameters = rcf.Iota2Parameters(config_file).get_sensors_parameters(tile)
    enable_gap, enable_raw = rcf.ReadConfigFile(config_file).get_param(
        "python_data_managing", "data_mode_access"
    )
    exogeneous_data = rcf.ReadConfigFile(config_file).get_param(
        "external_features", "exogeneous_data"
    )
    data_cont = CNF.DataContainer(
        sensors_parameters,
        configuration=CustomFeaturesConfiguration(
            enabled_raw=enable_raw,
            enabled_gap=enable_gap,
        ),
        exogeneous_data_file=exogeneous_data,
    )

    # This line show you all get methods available

    functions = [fun for fun in dir(data_cont) if "get_" in fun]
    functions.sort()
    for fun in functions:
        print(fun)
