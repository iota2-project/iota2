"""A function to infere the model using iota2"""

import glob
import pickle as cp
import sqlite3
from pathlib import Path
from typing import Literal

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import HuberRegressor
from sklearn.preprocessing import StandardScaler

from iota2.classification.py_classifiers import reorder_features
from iota2.vector_tools.vector_functions import get_all_fields_in_shape


def train_model(
    learning_samples_file: str, out_path: str, mode: Literal["classif", "regression"]
) -> None:
    """Train a scikit RF classifier and store it in a serializable object."""

    if mode not in ["classif", "regression"]:
        raise ValueError("mode must be in ['classif', 'regression']")
    all_columns = get_all_fields_in_shape(learning_samples_file, driver_name="SQLite")

    # filter to get only the features
    all_feat = [name for name in all_columns if "sentinel2" in name]
    feat_reo = sorted(all_feat)

    conn = sqlite3.connect(learning_samples_file)

    # Define a model
    if mode == "classif":
        model = RandomForestClassifier(n_estimators=20, n_jobs=-10)
        df_labels = pd.read_sql_query("select i2label from output", conn).to_numpy()
    elif mode == "regression":
        model = HuberRegressor()
        df_labels = pd.read_sql_query("select ndvi from output", conn).to_numpy()

    df_feat = pd.read_sql_query(f"select {','.join(feat_reo)} from output", conn)

    scaler = StandardScaler()
    scaler.fit(df_feat)

    model.fit(  # pylint: disable=possibly-used-before-assignment
        scaler.transform(df_feat),
        df_labels.ravel(),  # pylint: disable=possibly-used-before-assignment
    )

    object_to_save = {"model": model, "scaler": scaler, "features_order": feat_reo}
    with open(out_path + "/model.pickle", "wb") as out:
        cp.dump(object_to_save, out)


def train_all_models(
    input_learning_samples_folder: str,
    output_model_folder: str,
    mode: Literal["classif", "regression"],
):
    """Train all required models according an iota2 folder content.

    As several region or seed can be involved, we need one model for each case.
    """
    all_learning_file = glob.glob(
        str(Path(input_learning_samples_folder) / "Samples_region_*_learn.sqlite")
    )
    for learning_file in all_learning_file:
        train_model(learning_file, output_model_folder, mode)


def inference_skrf(data_stack_ori: np.ndarray, feat_labels: list[str], model: str):
    """Predict using scikit RF model."""
    all_pred_labels = None
    with open(model, "rb") as in_file:
        # load from training mandatory information
        object_to_load = cp.load(in_file)
        model = object_to_load["model"]
        scaler = object_to_load["scaler"]
        labels_order = object_to_load["features_order"]
        # compute the expected feature order w.r.t iota2 pipeline
        new_feat_order = reorder_features(labels_order, feat_labels)
        # return None if order are the same
        if new_feat_order is not None:
            feat_stack = data_stack_ori[:, :, new_feat_order]
            # the original array is an image (3D) but in sqlite the shape is 2D
            data_stack = np.reshape(
                feat_stack,
                (feat_stack.shape[0] * feat_stack.shape[1], feat_stack.shape[2]),
            )
            # apply the scaler on reshaped data
            data_stack = scaler.transform(data_stack)
            # Then predict
            all_pred_labels = model.predict(data_stack)
            # Reshape the labels to an image shape (3D)
            # if a label encoder is used, apply it before reshaping
            all_pred_labels = np.reshape(
                all_pred_labels, (data_stack_ori.shape[0], data_stack_ori.shape[1], 1)
            )
    # Return only the labels, the confidence and probas are set to None
    return all_pred_labels, None, None
