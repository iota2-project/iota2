FAQ
===


Facing the "Too many open files" error
--------------------------------------

iota2 may open a lot of files at the same time to complete a run. With some distribution configuration, the error ``Too many open files`` the solution is to increase the number of files that can be opened at the same time thanks to the command :

.. code-block:: bash

    ulimit -u #inform you about the current limit (ie : 4096)
    ulimit -u 5000 # increase user limit to 5000

More information at : https://linuxhint.com/permanently_set_ulimit_value/

.. Note:: If your are using Sentinel-1 data, you have to fill the parameter :ref:`srtm <s1_srtm_desc>` which is the path that contains DEM files. If you are facing the ``Too many open files``, try to reduce the content of this directory as much as possible.

Facing the "PermissionError: [Errno 13] Permission denied: '/tmp/dask-worker-space/'" error
-------------------------------------------------------------------------------------------

iota2 uses dask to schedule tasks. Dask needs to write temporary files at runtime. For some reason, dask may try to write files where it cannot, leading to a 'Permission denied' error. The solution is to tell dask where to put these temporary files by adding the line `temporary-directory: /directory/path/to/store/temporary_dask_files` to the file `$HOME/.config/dask/dask.yaml`.


Warning about parameters when the chain start
---------------------------------------------

These warnings implies that the parameter is detected but not known by iota2.
If you are not using a particular branch, ensure that your parameters are correct by looking at the corresponding builder:

- :doc:`I2Classification <i2_classification_builder>`
- :doc:`I2Obia <i2_obia_builder>`
- :doc:`I2FeaturesMap <i2_features_map_builder>`


If all parameters seem correct, that probably means that you are not using the latest iota2 version.
Ensure that the ``.condarc`` file is correctly filled, and try to :doc:`re-install iota2 <HowToGetIOTA2>`.
In last case, open a issue with the chain version, the ``.condarc`` contents and the warning message.

is iota2 able to mix sensors ?
------------------------------

In the general case, no. Except in the case of Sentinel-2 and Sentinel-1 where the Sentinel-1 data has been tiled with iota2.

Possible workaround : The images from the various sensors must first be processed by the user so that all the data can be superimposed (same origins, x/y sizes, spatial resolution).
