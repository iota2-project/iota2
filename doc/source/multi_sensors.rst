Multi sensors time series
*************************

``iota2`` is able to handle several sensors at the same time.

Optical
=======

- Sentinel 2 from Theia and sen2cor providers
- Landsat 5 and 8 from Theia

Radar
=====

- Sentinel 1

How to use several sensors together
===================================

For each sensor, a corresponding `XXXX_path` is available in the chain section of the configuration file.
To enable a sensor, simply fill this parameter. For instance `s2_path` for theia Sentinel 2.

Once this parameter is enabled, a new section is available in the configuration file named by the sensor. For instance `Sentinel2`.

Each section allows a customisation of the parameters for each sensor.
The main parameters are for interpolation and gapfilling, and the starting and ending dates of the considered period.
See :doc:`here <i2_classification_builder>` for more details.
Each sensor can have its own temporal period, is own temporal resolution, etc.
The only constraint is to use the same spatial resolution (see next section for data preparation).

With all these parameters enabled and set, the chain will create the time series accordingly.
The sensors are sorted in the source code, and they will always be returned in the same order.
In addition, in sqlite learning and validation samples, each feature is prefixed by the sensor's name.

Data preparation
================

Currently it is mandatory that all the data for each considered sensor share the same physical footprint.

This can be achieved for instance by using the `SuperImpose` otb application.

In addition, the tile name must be the same for all sensors. Otherwise, a multi tile scenario will be considered.
