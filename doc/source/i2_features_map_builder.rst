I2FeaturesMap
#############

.. _I2FeaturesMap.Landsat5_old:

Landsat5_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.Landsat5_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2FeaturesMap.Landsat5_old.end_date:
      * - :ref:`end_date <desc_I2FeaturesMap.Landsat5_old.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2FeaturesMap.Landsat5_old.end_date>`


          .. _I2FeaturesMap.Landsat5_old.keep_bands:
      * - :ref:`keep_bands <desc_I2FeaturesMap.Landsat5_old.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2FeaturesMap.Landsat5_old.keep_bands>`


          .. _I2FeaturesMap.Landsat5_old.start_date:
      * - :ref:`start_date <desc_I2FeaturesMap.Landsat5_old.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2FeaturesMap.Landsat5_old.start_date>`


          .. _I2FeaturesMap.Landsat5_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2FeaturesMap.Landsat5_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2FeaturesMap.Landsat5_old.end_date:

:ref:`end_date <I2FeaturesMap.Landsat5_old.end_date>`
-----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2FeaturesMap.Landsat5_old.keep_bands:

:ref:`keep_bands <I2FeaturesMap.Landsat5_old.keep_bands>`
---------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2FeaturesMap.Landsat5_old.start_date:

:ref:`start_date <I2FeaturesMap.Landsat5_old.start_date>`
---------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2FeaturesMap.Landsat8:

Landsat8
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.Landsat8.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2FeaturesMap.Landsat8.end_date:
      * - :ref:`end_date <desc_I2FeaturesMap.Landsat8.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2FeaturesMap.Landsat8.end_date>`


          .. _I2FeaturesMap.Landsat8.keep_bands:
      * - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8.keep_bands>`


          .. _I2FeaturesMap.Landsat8.start_date:
      * - :ref:`start_date <desc_I2FeaturesMap.Landsat8.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2FeaturesMap.Landsat8.start_date>`


          .. _I2FeaturesMap.Landsat8.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2FeaturesMap.Landsat8.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2FeaturesMap.Landsat8.end_date:

:ref:`end_date <I2FeaturesMap.Landsat8.end_date>`
-------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2FeaturesMap.Landsat8.keep_bands:

:ref:`keep_bands <I2FeaturesMap.Landsat8.keep_bands>`
-----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2FeaturesMap.Landsat8.start_date:

:ref:`start_date <I2FeaturesMap.Landsat8.start_date>`
-----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2FeaturesMap.Landsat8_old:

Landsat8_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.Landsat8_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2FeaturesMap.Landsat8_old.end_date:
      * - :ref:`end_date <desc_I2FeaturesMap.Landsat8_old.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2FeaturesMap.Landsat8_old.end_date>`


          .. _I2FeaturesMap.Landsat8_old.keep_bands:
      * - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8_old.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8_old.keep_bands>`


          .. _I2FeaturesMap.Landsat8_old.start_date:
      * - :ref:`start_date <desc_I2FeaturesMap.Landsat8_old.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2FeaturesMap.Landsat8_old.start_date>`


          .. _I2FeaturesMap.Landsat8_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2FeaturesMap.Landsat8_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2FeaturesMap.Landsat8_old.end_date:

:ref:`end_date <I2FeaturesMap.Landsat8_old.end_date>`
-----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2FeaturesMap.Landsat8_old.keep_bands:

:ref:`keep_bands <I2FeaturesMap.Landsat8_old.keep_bands>`
---------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2FeaturesMap.Landsat8_old.start_date:

:ref:`start_date <I2FeaturesMap.Landsat8_old.start_date>`
---------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2FeaturesMap.Landsat8_usgs:

Landsat8_usgs
*************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.Landsat8_usgs.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2FeaturesMap.Landsat8_usgs.end_date:
      * - :ref:`end_date <desc_I2FeaturesMap.Landsat8_usgs.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2FeaturesMap.Landsat8_usgs.end_date>`


          .. _I2FeaturesMap.Landsat8_usgs.keep_bands:
      * - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8_usgs.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8_usgs.keep_bands>`


          .. _I2FeaturesMap.Landsat8_usgs.start_date:
      * - :ref:`start_date <desc_I2FeaturesMap.Landsat8_usgs.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2FeaturesMap.Landsat8_usgs.start_date>`


          .. _I2FeaturesMap.Landsat8_usgs.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2FeaturesMap.Landsat8_usgs.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2FeaturesMap.Landsat8_usgs.end_date:

:ref:`end_date <I2FeaturesMap.Landsat8_usgs.end_date>`
------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2FeaturesMap.Landsat8_usgs.keep_bands:

:ref:`keep_bands <I2FeaturesMap.Landsat8_usgs.keep_bands>`
----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2FeaturesMap.Landsat8_usgs.start_date:

:ref:`start_date <I2FeaturesMap.Landsat8_usgs.start_date>`
----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2FeaturesMap.Landsat8_usgs_infrared:

Landsat8_usgs_infrared
**********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.Landsat8_usgs_infrared.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2FeaturesMap.Landsat8_usgs_infrared.enable_sensor_gapfilling:
      * - enable_sensor_gapfilling
        - False
        - Enable or disable gapfilling for landsat 8 and 9 IR data
        - bool
        - False
        - enable_sensor_gapfilling


          .. _I2FeaturesMap.Landsat8_usgs_infrared.end_date:
      * - :ref:`end_date <desc_I2FeaturesMap.Landsat8_usgs_infrared.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2FeaturesMap.Landsat8_usgs_infrared.end_date>`


          .. _I2FeaturesMap.Landsat8_usgs_infrared.keep_bands:
      * - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8_usgs_infrared.keep_bands>`
        - ['B10', 'B11']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8_usgs_infrared.keep_bands>`


          .. _I2FeaturesMap.Landsat8_usgs_infrared.start_date:
      * - :ref:`start_date <desc_I2FeaturesMap.Landsat8_usgs_infrared.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2FeaturesMap.Landsat8_usgs_infrared.start_date>`


          .. _I2FeaturesMap.Landsat8_usgs_infrared.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2FeaturesMap.Landsat8_usgs_infrared.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2FeaturesMap.Landsat8_usgs_infrared.end_date:

:ref:`end_date <I2FeaturesMap.Landsat8_usgs_infrared.end_date>`
---------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2FeaturesMap.Landsat8_usgs_infrared.keep_bands:

:ref:`keep_bands <I2FeaturesMap.Landsat8_usgs_infrared.keep_bands>`
-------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2FeaturesMap.Landsat8_usgs_infrared.start_date:

:ref:`start_date <I2FeaturesMap.Landsat8_usgs_infrared.start_date>`
-------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2FeaturesMap.Landsat8_usgs_optical:

Landsat8_usgs_optical
*********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.Landsat8_usgs_optical.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2FeaturesMap.Landsat8_usgs_optical.enable_sensor_gapfilling:
      * - enable_sensor_gapfilling
        - True
        - Enable or disable gapfilling for landsat 8 and 9 optical data
        - bool
        - False
        - enable_sensor_gapfilling


          .. _I2FeaturesMap.Landsat8_usgs_optical.end_date:
      * - :ref:`end_date <desc_I2FeaturesMap.Landsat8_usgs_optical.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2FeaturesMap.Landsat8_usgs_optical.end_date>`


          .. _I2FeaturesMap.Landsat8_usgs_optical.keep_bands:
      * - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8_usgs_optical.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8_usgs_optical.keep_bands>`


          .. _I2FeaturesMap.Landsat8_usgs_optical.start_date:
      * - :ref:`start_date <desc_I2FeaturesMap.Landsat8_usgs_optical.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2FeaturesMap.Landsat8_usgs_optical.start_date>`


          .. _I2FeaturesMap.Landsat8_usgs_optical.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2FeaturesMap.Landsat8_usgs_optical.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2FeaturesMap.Landsat8_usgs_optical.end_date:

:ref:`end_date <I2FeaturesMap.Landsat8_usgs_optical.end_date>`
--------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2FeaturesMap.Landsat8_usgs_optical.keep_bands:

:ref:`keep_bands <I2FeaturesMap.Landsat8_usgs_optical.keep_bands>`
------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2FeaturesMap.Landsat8_usgs_optical.start_date:

:ref:`start_date <I2FeaturesMap.Landsat8_usgs_optical.start_date>`
------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2FeaturesMap.Landsat8_usgs_thermal:

Landsat8_usgs_thermal
*********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.Landsat8_usgs_thermal.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2FeaturesMap.Landsat8_usgs_thermal.enable_sensor_gapfilling:
      * - enable_sensor_gapfilling
        - False
        - Enable or disable gapfilling for landsat 8 and 9 thermal data(temperature and emissivity)
        - bool
        - False
        - enable_sensor_gapfilling


          .. _I2FeaturesMap.Landsat8_usgs_thermal.end_date:
      * - :ref:`end_date <desc_I2FeaturesMap.Landsat8_usgs_thermal.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2FeaturesMap.Landsat8_usgs_thermal.end_date>`


          .. _I2FeaturesMap.Landsat8_usgs_thermal.keep_bands:
      * - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8_usgs_thermal.keep_bands>`
        - ['B10', 'EMIS']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2FeaturesMap.Landsat8_usgs_thermal.keep_bands>`


          .. _I2FeaturesMap.Landsat8_usgs_thermal.start_date:
      * - :ref:`start_date <desc_I2FeaturesMap.Landsat8_usgs_thermal.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2FeaturesMap.Landsat8_usgs_thermal.start_date>`


          .. _I2FeaturesMap.Landsat8_usgs_thermal.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2FeaturesMap.Landsat8_usgs_thermal.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2FeaturesMap.Landsat8_usgs_thermal.end_date:

:ref:`end_date <I2FeaturesMap.Landsat8_usgs_thermal.end_date>`
--------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2FeaturesMap.Landsat8_usgs_thermal.keep_bands:

:ref:`keep_bands <I2FeaturesMap.Landsat8_usgs_thermal.keep_bands>`
------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2FeaturesMap.Landsat8_usgs_thermal.start_date:

:ref:`start_date <I2FeaturesMap.Landsat8_usgs_thermal.start_date>`
------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2FeaturesMap.Sentinel_2:

Sentinel_2
**********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.Sentinel_2.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2FeaturesMap.Sentinel_2.end_date:
      * - :ref:`end_date <desc_I2FeaturesMap.Sentinel_2.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2FeaturesMap.Sentinel_2.end_date>`


          .. _I2FeaturesMap.Sentinel_2.keep_bands:
      * - :ref:`keep_bands <desc_I2FeaturesMap.Sentinel_2.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2FeaturesMap.Sentinel_2.keep_bands>`


          .. _I2FeaturesMap.Sentinel_2.start_date:
      * - :ref:`start_date <desc_I2FeaturesMap.Sentinel_2.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2FeaturesMap.Sentinel_2.start_date>`


          .. _I2FeaturesMap.Sentinel_2.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2FeaturesMap.Sentinel_2.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2FeaturesMap.Sentinel_2.end_date:

:ref:`end_date <I2FeaturesMap.Sentinel_2.end_date>`
---------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2FeaturesMap.Sentinel_2.keep_bands:

:ref:`keep_bands <I2FeaturesMap.Sentinel_2.keep_bands>`
-------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2FeaturesMap.Sentinel_2.start_date:

:ref:`start_date <I2FeaturesMap.Sentinel_2.start_date>`
-------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2FeaturesMap.Sentinel_2_L3A:

Sentinel_2_L3A
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.Sentinel_2_L3A.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2FeaturesMap.Sentinel_2_L3A.end_date:
      * - :ref:`end_date <desc_I2FeaturesMap.Sentinel_2_L3A.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2FeaturesMap.Sentinel_2_L3A.end_date>`


          .. _I2FeaturesMap.Sentinel_2_L3A.keep_bands:
      * - :ref:`keep_bands <desc_I2FeaturesMap.Sentinel_2_L3A.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2FeaturesMap.Sentinel_2_L3A.keep_bands>`


          .. _I2FeaturesMap.Sentinel_2_L3A.start_date:
      * - :ref:`start_date <desc_I2FeaturesMap.Sentinel_2_L3A.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2FeaturesMap.Sentinel_2_L3A.start_date>`


          .. _I2FeaturesMap.Sentinel_2_L3A.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2FeaturesMap.Sentinel_2_L3A.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2FeaturesMap.Sentinel_2_L3A.end_date:

:ref:`end_date <I2FeaturesMap.Sentinel_2_L3A.end_date>`
-------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2FeaturesMap.Sentinel_2_L3A.keep_bands:

:ref:`keep_bands <I2FeaturesMap.Sentinel_2_L3A.keep_bands>`
-----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2FeaturesMap.Sentinel_2_L3A.start_date:

:ref:`start_date <I2FeaturesMap.Sentinel_2_L3A.start_date>`
-----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2FeaturesMap.Sentinel_2_S2C:

Sentinel_2_S2C
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.Sentinel_2_S2C.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2FeaturesMap.Sentinel_2_S2C.end_date:
      * - :ref:`end_date <desc_I2FeaturesMap.Sentinel_2_S2C.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2FeaturesMap.Sentinel_2_S2C.end_date>`


          .. _I2FeaturesMap.Sentinel_2_S2C.keep_bands:
      * - :ref:`keep_bands <desc_I2FeaturesMap.Sentinel_2_S2C.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2FeaturesMap.Sentinel_2_S2C.keep_bands>`


          .. _I2FeaturesMap.Sentinel_2_S2C.start_date:
      * - :ref:`start_date <desc_I2FeaturesMap.Sentinel_2_S2C.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2FeaturesMap.Sentinel_2_S2C.start_date>`


          .. _I2FeaturesMap.Sentinel_2_S2C.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2FeaturesMap.Sentinel_2_S2C.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2FeaturesMap.Sentinel_2_S2C.end_date:

:ref:`end_date <I2FeaturesMap.Sentinel_2_S2C.end_date>`
-------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2FeaturesMap.Sentinel_2_S2C.keep_bands:

:ref:`keep_bands <I2FeaturesMap.Sentinel_2_S2C.keep_bands>`
-----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2FeaturesMap.Sentinel_2_S2C.start_date:

:ref:`start_date <I2FeaturesMap.Sentinel_2_S2C.start_date>`
-----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2FeaturesMap.arg_train:

arg_train
*********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.arg_train.features:
      * - :ref:`features <desc_I2FeaturesMap.arg_train.features>`
        - ['NDVI', 'NDWI', 'Brightness']
        - List of additional features computed
        - list
        - False
        - :ref:`features <desc_I2FeaturesMap.arg_train.features>`





Notes
=====

.. _desc_I2FeaturesMap.arg_train.features:

:ref:`features <I2FeaturesMap.arg_train.features>`
--------------------------------------------------
This parameter enable the computation of the three indices if available for the sensor used.There is no choice for using only one of them

.. _I2FeaturesMap.builders:

builders
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.builders.builders_class_name:
      * - :ref:`builders_class_name <desc_I2FeaturesMap.builders.builders_class_name>`
        - ['I2Classification']
        - The name of the class defining the builder
        - list
        - False
        - :ref:`builders_class_name <desc_I2FeaturesMap.builders.builders_class_name>`


          .. _I2FeaturesMap.builders.builders_paths:
      * - :ref:`builders_paths <desc_I2FeaturesMap.builders.builders_paths>`
        - /path/to/iota2/sources
        - The path to user builders
        - str
        - False
        - :ref:`builders_paths <desc_I2FeaturesMap.builders.builders_paths>`





Notes
=====

.. _desc_I2FeaturesMap.builders.builders_class_name:

:ref:`builders_class_name <I2FeaturesMap.builders.builders_class_name>`
-----------------------------------------------------------------------
Available builders are : 'I2Classification', 'I2FeaturesMap' and 'I2Obia'

.. _desc_I2FeaturesMap.builders.builders_paths:

:ref:`builders_paths <I2FeaturesMap.builders.builders_paths>`
-------------------------------------------------------------
If not indicated, the iota2 source directory is used: */iota2/sequence_builders/

.. _I2FeaturesMap.chain:

chain
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.chain.compression_algorithm:
      * - compression_algorithm
        - ZSTD
        - Set the gdal compression algorithm to use: NONE, LZW, ZSTD (default).All rasters write with OTB will be compress with the chosen algorithm.
        - str
        - False
        - compression_algorithm


          .. _I2FeaturesMap.chain.compression_predictor:
      * - :ref:`compression_predictor <desc_I2FeaturesMap.chain.compression_predictor>`
        - 2
        - Set the predictor for LZW and ZSTD compression: 1 (no predictor), 2 (horizontal differencing, default)
        - int
        - False
        - :ref:`compression_predictor <desc_I2FeaturesMap.chain.compression_predictor>`


          .. _I2FeaturesMap.chain.first_step:
      * - first_step
        - None
        - The step group name indicating where the chain start
        - str
        - False
        - first_step


          .. _I2FeaturesMap.chain.l5_path_old:
      * - l5_path_old
        - None
        - Absolute path to Landsat-5 images coming from old THEIA format (D*H*)
        - str
        - False
        - l5_path_old


          .. _I2FeaturesMap.chain.l8_path:
      * - l8_path
        - None
        - Absolute path to Landsat-8 images comingfrom new tiled THEIA data
        - str
        - False
        - l8_path


          .. _I2FeaturesMap.chain.l8_path_old:
      * - l8_path_old
        - None
        - Absolute path to Landsat-8 images coming from old THEIA format (D*H*)
        - str
        - False
        - l8_path_old


          .. _I2FeaturesMap.chain.l8_usgs_infrared_path:
      * - l8_usgs_infrared_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_infrared_path


          .. _I2FeaturesMap.chain.l8_usgs_optical_path:
      * - l8_usgs_optical_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_optical_path


          .. _I2FeaturesMap.chain.l8_usgs_path:
      * - l8_usgs_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_path


          .. _I2FeaturesMap.chain.l8_usgs_thermal_path:
      * - l8_usgs_thermal_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_thermal_path


          .. _I2FeaturesMap.chain.last_step:
      * - last_step
        - None
        - The step group name indicating where the chain ends
        - str
        - False
        - last_step


          .. _I2FeaturesMap.chain.list_tile:
      * - list_tile
        - None
        - List of tile to process, separated by space
        - str
        - True
        - list_tile


          .. _I2FeaturesMap.chain.logger_level:
      * - logger_level
        - INFO
        - Set the logger level: NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
        - str
        - False
        - logger_level


          .. _I2FeaturesMap.chain.minimum_required_dates:
      * - minimum_required_dates
        - 2
        - required minimum number of available dates for each sensor
        - int
        - False
        - minimum_required_dates


          .. _I2FeaturesMap.chain.output_path:
      * - :ref:`output_path <desc_I2FeaturesMap.chain.output_path>`
        - None
        - Absolute path to the output directory
        - str
        - True
        - :ref:`output_path <desc_I2FeaturesMap.chain.output_path>`


          .. _I2FeaturesMap.chain.proj:
      * - proj
        - None
        - The projection wanted. Format EPSG:XXXX is mandatory
        - str
        - True
        - proj


          .. _I2FeaturesMap.chain.remove_output_path:
      * - :ref:`remove_output_path <desc_I2FeaturesMap.chain.remove_output_path>`
        - True
        - Before the launch of iota2, remove the content of `output_path`
        - bool
        - False
        - :ref:`remove_output_path <desc_I2FeaturesMap.chain.remove_output_path>`


          .. _I2FeaturesMap.chain.s1_path:
      * - s1_path
        - None
        - Absolute path to Sentinel-1 configuration file
        - str
        - False
        - s1_path


          .. _I2FeaturesMap.chain.s2_l3a_output_path:
      * - s2_l3a_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_l3a_output_path


          .. _I2FeaturesMap.chain.s2_l3a_path:
      * - s2_l3a_path
        - None
        - Absolute path to Sentinel-2 L3A images (THEIA format)
        - str
        - False
        - s2_l3a_path


          .. _I2FeaturesMap.chain.s2_output_path:
      * - s2_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_output_path


          .. _I2FeaturesMap.chain.s2_path:
      * - s2_path
        - None
        - Absolute path to Sentinel-2 images (THEIA format)
        - str
        - False
        - s2_path


          .. _I2FeaturesMap.chain.s2_s2c_output_path:
      * - s2_s2c_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_s2c_output_path


          .. _I2FeaturesMap.chain.s2_s2c_path:
      * - s2_s2c_path
        - None
        - Absolute path to Sentinel-2 images (Sen2Cor format)
        - str
        - False
        - s2_s2c_path


          .. _I2FeaturesMap.chain.spatial_resolution:
      * - :ref:`spatial_resolution <desc_I2FeaturesMap.chain.spatial_resolution>`
        - []
        - Output spatial resolution
        - list or scalar
        - False
        - :ref:`spatial_resolution <desc_I2FeaturesMap.chain.spatial_resolution>`


          .. _I2FeaturesMap.chain.user_feat_path:
      * - :ref:`user_feat_path <desc_I2FeaturesMap.chain.user_feat_path>`
        - None
        - Absolute path to the user's features path
        - str
        - False
        - :ref:`user_feat_path <desc_I2FeaturesMap.chain.user_feat_path>`





Notes
=====

.. _desc_I2FeaturesMap.chain.compression_predictor:

:ref:`compression_predictor <I2FeaturesMap.chain.compression_predictor>`
------------------------------------------------------------------------
It has been noted that in some cases, once the features are written to disk, the raster file may be empty. If this is the case, please change the predictor to 1 or 3.

.. _desc_I2FeaturesMap.chain.output_path:

:ref:`output_path <I2FeaturesMap.chain.output_path>`
----------------------------------------------------
Absolute path to the output directory.It is recommended to have one directory per run of the chain

.. _desc_I2FeaturesMap.chain.remove_output_path:

:ref:`remove_output_path <I2FeaturesMap.chain.remove_output_path>`
------------------------------------------------------------------
Before the launch of iota2, remove the content of `output_path`. Only if the `first_step` is `init` and the folder name is valid 

.. _desc_I2FeaturesMap.chain.spatial_resolution:

:ref:`spatial_resolution <I2FeaturesMap.chain.spatial_resolution>`
------------------------------------------------------------------
The spatial resolution expected.It can be provided as integer or float,or as a list containing two values for non squared resolution

.. _desc_I2FeaturesMap.chain.user_feat_path:

:ref:`user_feat_path <I2FeaturesMap.chain.user_feat_path>`
----------------------------------------------------------
Absolute path to the user's features path. They must be stored by tiles

.. _I2FeaturesMap.external_features:

external_features
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.external_features.concat_mode:
      * - :ref:`concat_mode <desc_I2FeaturesMap.external_features.concat_mode>`
        - True
        - enable the use of all features
        - bool
        - False
        - :ref:`concat_mode <desc_I2FeaturesMap.external_features.concat_mode>`


          .. _I2FeaturesMap.external_features.exogeneous_data:
      * - :ref:`exogeneous_data <desc_I2FeaturesMap.external_features.exogeneous_data>`
        - None
        - Path to a Geotiff file containing additional data to be used in external features
        - str
        - False
        - :ref:`exogeneous_data <desc_I2FeaturesMap.external_features.exogeneous_data>`


          .. _I2FeaturesMap.external_features.external_features_flag:
      * - external_features_flag
        - False
        - enable the external features mode
        - bool
        - False
        - external_features_flag


          .. _I2FeaturesMap.external_features.functions:
      * - :ref:`functions <desc_I2FeaturesMap.external_features.functions>`
        - None
        - function list to be used to compute features
        - str/list
        - False
        - :ref:`functions <desc_I2FeaturesMap.external_features.functions>`


          .. _I2FeaturesMap.external_features.module:
      * - module
        - /path/to/iota2/sources
        - absolute path for user source code
        - str
        - False
        - module


          .. _I2FeaturesMap.external_features.no_data_value:
      * - no_data_value
        - -10000
        - value considered as no_data in features map mosaic ('I2FeaturesMap' builder name)
        - int
        - False
        - no_data_value


          .. _I2FeaturesMap.external_features.output_name:
      * - output_name
        - None
        - temporary chunks are written using this name as prefix
        - str
        - False
        - output_name





Notes
=====

.. _desc_I2FeaturesMap.external_features.concat_mode:

:ref:`concat_mode <I2FeaturesMap.external_features.concat_mode>`
----------------------------------------------------------------
if disabled, only external features are used in the whole processing

.. _desc_I2FeaturesMap.external_features.exogeneous_data:

:ref:`exogeneous_data <I2FeaturesMap.external_features.exogeneous_data>`
------------------------------------------------------------------------
If the =exogeneous_data= contains '$TILE', it will be replaced by the tile name being processed.If you want to reproject your data on given tiles, you can use the =split_raster_into_tiles.py= command line tool.

Usage: =split_raster_into_tiles.py --help=.

.. _desc_I2FeaturesMap.external_features.functions:

:ref:`functions <I2FeaturesMap.external_features.functions>`
------------------------------------------------------------
Can be a string of space-separated function namesCan be a list of either strings of function nameor lists of one function name and one argument mapping

.. _I2FeaturesMap.pretrained_model:

pretrained_model
****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.pretrained_model.boundary_buffer:
      * - boundary_buffer
        - None
        - List of boundary buffer size
        - list
        - False
        - boundary_buffer


          .. _I2FeaturesMap.pretrained_model.function:
      * - :ref:`function <desc_I2FeaturesMap.pretrained_model.function>`
        - None
        - Predict function name
        - str
        - False
        - :ref:`function <desc_I2FeaturesMap.pretrained_model.function>`


          .. _I2FeaturesMap.pretrained_model.mode:
      * - :ref:`mode <desc_I2FeaturesMap.pretrained_model.mode>`
        - None
        - Algorythm nature (classification or regression)
        - str
        - False
        - :ref:`mode <desc_I2FeaturesMap.pretrained_model.mode>`


          .. _I2FeaturesMap.pretrained_model.model:
      * - :ref:`model <desc_I2FeaturesMap.pretrained_model.model>`
        - None
        - Serialized object containing the model
        - str
        - False
        - :ref:`model <desc_I2FeaturesMap.pretrained_model.model>`


          .. _I2FeaturesMap.pretrained_model.module:
      * - :ref:`module <desc_I2FeaturesMap.pretrained_model.module>`
        - /path/to/iota2/sources
        - Absolute path to the python module
        - str
        - False
        - :ref:`module <desc_I2FeaturesMap.pretrained_model.module>`





Notes
=====

.. _desc_I2FeaturesMap.pretrained_model.function:

:ref:`function <I2FeaturesMap.pretrained_model.function>`
---------------------------------------------------------
This function must have the imposed signature. It not accept any others parameters. All model dedicated parameters must be stored alongside the model.

.. _desc_I2FeaturesMap.pretrained_model.mode:

:ref:`mode <I2FeaturesMap.pretrained_model.mode>`
-------------------------------------------------
The python module must contains the predict function It must handle all the potential dependencies and import related to the correct model instanciation

.. _desc_I2FeaturesMap.pretrained_model.model:

:ref:`model <I2FeaturesMap.pretrained_model.model>`
---------------------------------------------------
In the configuration file, the mandatory keys $REGION and $SEED must be present as they are replaced by iota2. In case of only one region, the region value is set to 1. Look at the documentation about the model constraint.

.. _desc_I2FeaturesMap.pretrained_model.module:

:ref:`module <I2FeaturesMap.pretrained_model.module>`
-----------------------------------------------------
The python module must contains the predict function It must handle all the potential dependencies and import related to the correct model instanciation

.. _I2FeaturesMap.python_data_managing:

python_data_managing
********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.python_data_managing.chunk_size_mode:
      * - chunk_size_mode
        - split_number
        - The chunk split mode, currently the choice is 'split_number'
        - str
        - False
        - chunk_size_mode


          .. _I2FeaturesMap.python_data_managing.chunk_size_x:
      * - chunk_size_x
        - 50
        - number of cols for one chunk
        - int
        - False
        - chunk_size_x


          .. _I2FeaturesMap.python_data_managing.chunk_size_y:
      * - chunk_size_y
        - 50
        - number of rows for one chunk
        - int
        - False
        - chunk_size_y


          .. _I2FeaturesMap.python_data_managing.data_mode_access:
      * - :ref:`data_mode_access <desc_I2FeaturesMap.python_data_managing.data_mode_access>`
        - gapfilled
        - choose which data can be accessed in custom features
        - str
        - False
        - :ref:`data_mode_access <desc_I2FeaturesMap.python_data_managing.data_mode_access>`


          .. _I2FeaturesMap.python_data_managing.fill_missing_dates:
      * - :ref:`fill_missing_dates <desc_I2FeaturesMap.python_data_managing.fill_missing_dates>`
        - False
        - fill raw data with no data if dates are missing
        - bool
        - False
        - :ref:`fill_missing_dates <desc_I2FeaturesMap.python_data_managing.fill_missing_dates>`


          .. _I2FeaturesMap.python_data_managing.max_nn_inference_size:
      * - :ref:`max_nn_inference_size <desc_I2FeaturesMap.python_data_managing.max_nn_inference_size>`
        - None
        - maximum batch inference size
        - int
        - False
        - :ref:`max_nn_inference_size <desc_I2FeaturesMap.python_data_managing.max_nn_inference_size>`


          .. _I2FeaturesMap.python_data_managing.number_of_chunks:
      * - number_of_chunks
        - 50
        - the expected number of chunks
        - int
        - False
        - number_of_chunks


          .. _I2FeaturesMap.python_data_managing.padding_size_x:
      * - padding_size_x
        - 0
        - The padding for chunk
        - int
        - False
        - padding_size_x


          .. _I2FeaturesMap.python_data_managing.padding_size_y:
      * - padding_size_y
        - 0
        - The padding for chunk
        - int
        - False
        - padding_size_y





Notes
=====

.. _desc_I2FeaturesMap.python_data_managing.data_mode_access:

:ref:`data_mode_access <I2FeaturesMap.python_data_managing.data_mode_access>`
-----------------------------------------------------------------------------
Three values are allowed:

- gapfilled: give access only the gapfilled data

- raw: gives access only the original raw data

- both: provides access to both data

..Notes:: Data are spatialy resampled, these parameters concern only temporal interpolation

.. _desc_I2FeaturesMap.python_data_managing.fill_missing_dates:

:ref:`fill_missing_dates <I2FeaturesMap.python_data_managing.fill_missing_dates>`
---------------------------------------------------------------------------------
If raw data access is enabled, this option considers all unique dates for all tiles and identify which dates are missing for each tile. A missing date is filled using a no data constant value.Cloud or saturation are not corrected, but masks are provided Masks contain three value: 0 for valid data, 1 for cloudy or saturated pixels, 2 for a missing date

.. _desc_I2FeaturesMap.python_data_managing.max_nn_inference_size:

:ref:`max_nn_inference_size <I2FeaturesMap.python_data_managing.max_nn_inference_size>`
---------------------------------------------------------------------------------------
Involved if a neural network inference is performed. If not set (None), the inference size will be the same as the one used during the learning stage

.. _I2FeaturesMap.sensors_data_interpolation:

sensors_data_interpolation
**************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.sensors_data_interpolation.auto_date:
      * - :ref:`auto_date <desc_I2FeaturesMap.sensors_data_interpolation.auto_date>`
        - True
        - Enable the use of `start_date` and `end_date`
        - bool
        - False
        - :ref:`auto_date <desc_I2FeaturesMap.sensors_data_interpolation.auto_date>`


          .. _I2FeaturesMap.sensors_data_interpolation.use_additional_features:
      * - use_additional_features
        - False
        - enable the use of additional features
        - bool
        - False
        - use_additional_features


          .. _I2FeaturesMap.sensors_data_interpolation.use_gapfilling:
      * - use_gapfilling
        - True
        - enable the use of gapfilling (clouds/temporal interpolation)
        - bool
        - False
        - use_gapfilling


          .. _I2FeaturesMap.sensors_data_interpolation.write_outputs:
      * - :ref:`write_outputs <desc_I2FeaturesMap.sensors_data_interpolation.write_outputs>`
        - False
        - write temporary files
        - bool
        - False
        - :ref:`write_outputs <desc_I2FeaturesMap.sensors_data_interpolation.write_outputs>`





Notes
=====

.. _desc_I2FeaturesMap.sensors_data_interpolation.auto_date:

:ref:`auto_date <I2FeaturesMap.sensors_data_interpolation.auto_date>`
---------------------------------------------------------------------
If True, iota2 will automatically guess the first and the last interpolation date. Else, `start_date` and `end_date` of each sensors will be used

.. _desc_I2FeaturesMap.sensors_data_interpolation.write_outputs:

:ref:`write_outputs <I2FeaturesMap.sensors_data_interpolation.write_outputs>`
-----------------------------------------------------------------------------
Write the time series before and after gapfilling, the mask time series, and also the feature time series. This option required a large amount of free disk space.

.. _I2FeaturesMap.slurm:

slurm
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.slurm.account:
      * - :ref:`account <desc_I2FeaturesMap.slurm.account>`
        - None
        - Feed the sbatch parameter 'account'
        - str
        - False
        - :ref:`account <desc_I2FeaturesMap.slurm.account>`





Notes
=====

.. _desc_I2FeaturesMap.slurm.account:

:ref:`account <I2FeaturesMap.slurm.account>`
--------------------------------------------
The section 'slurm' is only available once the `Slurm <https://slurm.schedmd.com/documentation.html>`_ orchestrator is involved in jobs submission.

.. _I2FeaturesMap.task_retry_limits:

task_retry_limits
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.task_retry_limits.allowed_retry:
      * - allowed_retry
        - 0
        - allow dask to retry a failed job N times
        - int
        - False
        - allowed_retry


          .. _I2FeaturesMap.task_retry_limits.maximum_cpu:
      * - :ref:`maximum_cpu <desc_I2FeaturesMap.task_retry_limits.maximum_cpu>`
        - 4
        - the maximum number of CPU available
        - int
        - False
        - :ref:`maximum_cpu <desc_I2FeaturesMap.task_retry_limits.maximum_cpu>`


          .. _I2FeaturesMap.task_retry_limits.maximum_ram:
      * - :ref:`maximum_ram <desc_I2FeaturesMap.task_retry_limits.maximum_ram>`
        - 16.0
        - the maximum amount of RAM available. (gB)
        - float
        - False
        - :ref:`maximum_ram <desc_I2FeaturesMap.task_retry_limits.maximum_ram>`





Notes
=====

.. _desc_I2FeaturesMap.task_retry_limits.maximum_cpu:

:ref:`maximum_cpu <I2FeaturesMap.task_retry_limits.maximum_cpu>`
----------------------------------------------------------------
the amount of cpu will be doubled if the task is killed due to ram overconsumption until maximum_cpu or allowed_retry are reach

.. _desc_I2FeaturesMap.task_retry_limits.maximum_ram:

:ref:`maximum_ram <I2FeaturesMap.task_retry_limits.maximum_ram>`
----------------------------------------------------------------
the amount of RAM will be doubled if the task is killed due to ram overconsumption until maximum_ram or allowed_retry are reach

.. _I2FeaturesMap.userFeat:

userFeat
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesMap.userFeat.arbo:
      * - arbo
        - /*
        - input folder hierarchy
        - str
        - False
        - arbo


          .. _I2FeaturesMap.userFeat.patterns:
      * - patterns
        - ALT,ASP,SLP
        - key name for detect the input images
        - str
        - False
        - patterns





