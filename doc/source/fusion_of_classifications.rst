====================================
Fusion of classifications with iota2
====================================

Iota2 is able to fuse classifications together (using the ``FusionOfClassifications`` otb application) and create the corresponding confusion matrix. For this, first run iota2 as usual for all the cases you need. Let's suppose you have the following file structure:

.. code-block::

└── iota2_directory
    ├── nomenclature.txt
    ├── ground_truth/
    │   ├── ground_truth.shp
    │   ├── ground_truth.shx
    │   ├── ground_truth.prj
    │   ├── ground_truth.dbf
    │   └── ground_truth.cpg
    ├── results1/
    │   ├── final
    │   ├── logs
    │   └── ...
    ├── results2/
    │   └── ...
    ├── results3/
    │   └── ...
    ├── fusion_output/
    └── ...

You can then use the following command:

``fusion.py -iota2_directories iota2_directory/results1 iota2_directory/results2 iota2_directory/results3 -output_dir  iota2_directory/fusion_output -path_reference_data  iota2_directory/ground_truth/test_ground_truth.shp -data_field code -path_nomenclature iota2_directory/nomenclature.txt
``

The directory ``fusion_output`` will contain:
 * ``final_classification.tif``: the fusion of all the classifications
 * ``final_confusion_matrix.csv``: the confusion matrix corresponding to the fusion of the classifications, as a csv
 * ``final_confusion_matrix.png``: the confusion matrix corresponding to the fusion of the classifications, as an image