iota2 external classification
#############################

Purpose
*******

iota2 allows to deploy machine learning model at large scale by providing data to a model.
However, the model must be integrated first to iota2, which can be a costly operation.

The aim of this builder is to provide a simple solution to an end user for using any kind of model (deep learning, scikit learn, home made library...) for both classification and regression.

The processus is split in three steps:

- The samples generation
- Train the model using user solution and the samples provided
- Predict the large scale map using this model

To explain the process, a simple example is chosen.

In this example, we will see how to generate the training set, a simple training example, and then the inference part with iota2. The features order will be shuffled in order to evaluate the robustness of scikit RandomForest classifier. This example details the procedure for classification but the process for regression is similar.


Generate the samples
====================

The first step is to generate the training samples which must be consistent with those used for inference.

For this we will run the first steps of the standard classification chain, the full tutorial of which is available here. It is preferable to be familiar with the iota2 environment, and therefore to have run the standard classification at least once before practising this tutorial where we will use the same data and configuration.

To begin with we will create the configuration file.
From a terminal, duplicate the configuration file and name it `i2_pretrained_model_classification_samples.cfg`, then open it with your favourite editor.

Replace the line `last_step: "validation"` with `last_step: "sampling"`.

Then run the chain. At the end of the run, you should have a `Samples_region_1_seed_0_learn.sqlite` file, located in `/XXXX/IOTA2_TEST_S2/IOTA2_Outputs/Results/learningSamples/`.

These will be our learning points for the rest of the process.

Train the model
===============

The `Samples_region_1_seed_0_learn.sqlite` has a particular shape.
All the columns available can be found using:

.. code-block:: python

    from iota2.vector_tools.vector_functions import get_all_fields_in_shape
    all_cols = get_all_fields_in_shape(learning_samples_file,
                                          driver="SQLite")

Which will provide a list of names:

.. code-block:: python

    ['i2label', 'region', 'code', 'originfid', 'tile_o', 'sentinel2_b2_20180501', 'sentinel2_b3_20180501', 'sentinel2_b4_20180501', 'sentinel2_b5_20180501', 'sentinel2_b6_20180501', 'sentinel2_b7_20180501', 'sentinel2_b8_20180501', 'sentinel2_b8a_20180501', 'sentinel2_b11_20180501', 'sentinel2_b12_20180501', 'sentinel2_b2_20180511', 'sentinel2_b3_20180511', 'sentinel2_b4_20180511', 'sentinel2_b5_20180511', 'sentinel2_b6_20180511', 'sentinel2_b7_20180511', 'sentinel2_b8_20180511', 'sentinel2_b8a_20180511', 'sentinel2_b11_20180511', 'sentinel2_b12_20180511', 'sentinel2_b2_20180521', 'sentinel2_b3_20180521', 'sentinel2_b4_20180521', 'sentinel2_b5_20180521', 'sentinel2_b6_20180521', 'sentinel2_b7_20180521', 'sentinel2_b8_20180521', 'sentinel2_b8a_20180521', 'sentinel2_b11_20180521', 'sentinel2_b12_20180521', 'sentinel2_b2_20180531', 'sentinel2_b3_20180531', 'sentinel2_b4_20180531', 'sentinel2_b5_20180531', 'sentinel2_b6_20180531', 'sentinel2_b7_20180531', 'sentinel2_b8_20180531', 'sentinel2_b8a_20180531', 'sentinel2_b11_20180531', 'sentinel2_b12_20180531', 'sentinel2_ndvi_20180501', 'sentinel2_ndvi_20180511', 'sentinel2_ndvi_20180521', 'sentinel2_ndvi_20180531', 'sentinel2_ndwi_20180501', 'sentinel2_ndwi_20180511', 'sentinel2_ndwi_20180521', 'sentinel2_ndwi_20180531', 'sentinel2_brightness_20180501', 'sentinel2_brightness_20180511', 'sentinel2_brightness_20180521', 'sentinel2_brightness_20180531']


All features starts with the sensor name, i.e. `sentinel2` in this example.
The others columns are:

- i2label: the internal encoding of labels. It is highly recommended to use this column as reference, as in the last part, these labels will be decoded using the same internal mechanism.
- region: the corresponding region number
- code: the original label column. Note that in this example the original name is `code`. If it was `mylabel` in your input reference data, this column would have `mylabel` as name instead of `code`
- originfid: the polygon identifiant
- tile_o: indicates in which tile the sample comes from

It is easy to filter the features bands, and then shuffle them. For reproducibility, in this example, we use `sorted` to change the band order.

We also compute a feature scaler, because it's funny.

Then we save the model into a serializable format (pickle,joblib, torch,...).
For convenience, some information are stored in a dictionary.
In this exemple, only the model, the scaler and the features order are stored, but for more complex models some parameters should be stored too.

Having trained our model, we can start the land cover mapping over tiles.

Inference at large scale
========================

To classify the time series over tiles, a new configuration file must be created.
It is very important to be consistent between the one created in the first part and this one.
Refer to the next section to see the pitfalls and tips related to the configuration file.

Unlike other classification builders, this one does not create a new tree but completes an existing one. This is why the `output_path` must be the same between the two configuration files.

In this new configuration file, the `chain` section must be present and similar to the previous one, concerning the `list_tile`, the `proj`, the `output_path`, the `ground_truth`, the `region_path` the `spatial_resolution`, the `color_table` and the `nomenclature_path`.

A new section must be added: `pretrained_model`.
Four parameters are mandatory:

- module: the full path to your python code
- function: the name of the inference function
- model: the full path to your models.
- mode: either "classification" or "regression"

There is an important constraint about naming your model. In the path, the region and the seed number must be clearly identified. In the configuration file, the two keyword: `$REGION` and `$SEED` are mandatory. They are used by the chain to get the correct model according the tiles and the seed.
In case of a unique seed, the only value allowed is 0.
A few examples of valid models name and their equivalent in the configuration file:

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Model name
        - Configuration file value

      * - /XXXX/model_region_1_seed_0.pickle
        - /XXXX/model_region_$REGION_seed_$SEED.pickle

      * - /XXXX/model_1_0.pickle
        - /XXXX/model_$REGION_$SEED.pickle

      * - /XXXX/region_3/seed_2/model.pickle
        - /XXXX/region_$REGION/seed_$SEED/model.pickle


It is time to write the inference function.
For this, a python file must be created: /XXXX/predicts_function/predict.py. This is the path expected in the `module` field presented above.

The inference function will be named "inference_skrf" in this example; this is the name that should be filled in the "function" field of the configuration file. This name is completely free, and must be present in the "module".

The inference function must have this signature: `def inference_skrf(data_stack_ori, data_mask, feat_labels, masks_labels, model, reference_classes_list, deps, **kwargs)` and return three python objects.

The first object contains the matrix of labels predicted by the model, the second the confidence and the third the probabilities by classes. The shape of these three matrices is defined by the input data for the rows and columns. The label and confidence matrices can be 2D or 3D (single band), but the probability map must contain as many bands as in the input nomenclature.
The confidence and probability matrices can be set to None if you are not interested in these products or if the classifier does not provide them.
The handling of these matrices in output will be illustrated in the example below.

The signature of the function is checked when the chain is launched. The arguments and their order are fixed and should allow a user to satisfy most use cases.

- data_stack_ori: is the numpy array containing all features. It provides the gapfilled time series or the raw data depending of the configuration file.
- data_mask: if enabled, contains the numpy array of binary masks
- feat_labels: list of strings indicating the features order in data_stack_ori
- masks_labels: list of strings containing the date of each mask in data_mask
- model: the model name provided by iota2, i.e the $REGION and $SEED were replaced by values
- reference_classes_list: the list of all classes expected by iota2, to reorder the probability maps output
- deps: an unused variable which ensures that the OTB pipeline is always in scope


Then it is possible to write a minimalist function to inference the data:
- First load the model from the serialized object
- Then call the predict method and return the result

These few steps can be summarized in the following example:

.. code-block:: python

    def inference_skrf(data_stack_ori, data_mask, feat_labels, masks_labels, model,
                       reference_classes_list, deps):
        with open(model, "rb") as in_file:
            object_to_load = cp.load(in_file)
            model = object_to_load["model"]
            labels = model.predict(data_stack_ori)
        return labels

Pretty simple, isn't it? But totally wrong!

The main errors here:

- The features order was changed during the learning
- A feature scaler was used for the learning
- The function returns only one array
- The predict function of scikit learn flattens the data into a 1D array, which doesn't match the expected shape

Only the last point will raise an exception from iota2, as the others as from the users responsibility. From the point of view of iota2, the learning step is unknown, which means that it is impossible to ensure that a feature scaler was used or if the some features are not used, or if a label encoder was used too. If the produced classification looks strange, make sure that your process during learning and inference are identical.

Here is a python file containing:

- A function to train a model
- A function to infere the model using iota2
:download:`train_sk_external.py <./examples/train_sk_external.py>`
