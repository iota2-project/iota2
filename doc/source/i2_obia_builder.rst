I2Obia
######

.. _I2Obia.Landsat5_old:

Landsat5_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.Landsat5_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Obia.Landsat5_old.end_date:
      * - :ref:`end_date <desc_I2Obia.Landsat5_old.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Obia.Landsat5_old.end_date>`


          .. _I2Obia.Landsat5_old.keep_bands:
      * - :ref:`keep_bands <desc_I2Obia.Landsat5_old.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Obia.Landsat5_old.keep_bands>`


          .. _I2Obia.Landsat5_old.start_date:
      * - :ref:`start_date <desc_I2Obia.Landsat5_old.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Obia.Landsat5_old.start_date>`


          .. _I2Obia.Landsat5_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Obia.Landsat5_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Obia.Landsat5_old.end_date:

:ref:`end_date <I2Obia.Landsat5_old.end_date>`
----------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Obia.Landsat5_old.keep_bands:

:ref:`keep_bands <I2Obia.Landsat5_old.keep_bands>`
--------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Obia.Landsat5_old.start_date:

:ref:`start_date <I2Obia.Landsat5_old.start_date>`
--------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Obia.Landsat8:

Landsat8
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.Landsat8.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Obia.Landsat8.end_date:
      * - :ref:`end_date <desc_I2Obia.Landsat8.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Obia.Landsat8.end_date>`


          .. _I2Obia.Landsat8.keep_bands:
      * - :ref:`keep_bands <desc_I2Obia.Landsat8.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Obia.Landsat8.keep_bands>`


          .. _I2Obia.Landsat8.start_date:
      * - :ref:`start_date <desc_I2Obia.Landsat8.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Obia.Landsat8.start_date>`


          .. _I2Obia.Landsat8.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Obia.Landsat8.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Obia.Landsat8.end_date:

:ref:`end_date <I2Obia.Landsat8.end_date>`
------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Obia.Landsat8.keep_bands:

:ref:`keep_bands <I2Obia.Landsat8.keep_bands>`
----------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Obia.Landsat8.start_date:

:ref:`start_date <I2Obia.Landsat8.start_date>`
----------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Obia.Landsat8_old:

Landsat8_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.Landsat8_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Obia.Landsat8_old.end_date:
      * - :ref:`end_date <desc_I2Obia.Landsat8_old.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Obia.Landsat8_old.end_date>`


          .. _I2Obia.Landsat8_old.keep_bands:
      * - :ref:`keep_bands <desc_I2Obia.Landsat8_old.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Obia.Landsat8_old.keep_bands>`


          .. _I2Obia.Landsat8_old.start_date:
      * - :ref:`start_date <desc_I2Obia.Landsat8_old.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Obia.Landsat8_old.start_date>`


          .. _I2Obia.Landsat8_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Obia.Landsat8_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Obia.Landsat8_old.end_date:

:ref:`end_date <I2Obia.Landsat8_old.end_date>`
----------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Obia.Landsat8_old.keep_bands:

:ref:`keep_bands <I2Obia.Landsat8_old.keep_bands>`
--------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Obia.Landsat8_old.start_date:

:ref:`start_date <I2Obia.Landsat8_old.start_date>`
--------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Obia.Landsat8_usgs:

Landsat8_usgs
*************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.Landsat8_usgs.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Obia.Landsat8_usgs.end_date:
      * - :ref:`end_date <desc_I2Obia.Landsat8_usgs.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Obia.Landsat8_usgs.end_date>`


          .. _I2Obia.Landsat8_usgs.keep_bands:
      * - :ref:`keep_bands <desc_I2Obia.Landsat8_usgs.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Obia.Landsat8_usgs.keep_bands>`


          .. _I2Obia.Landsat8_usgs.start_date:
      * - :ref:`start_date <desc_I2Obia.Landsat8_usgs.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Obia.Landsat8_usgs.start_date>`


          .. _I2Obia.Landsat8_usgs.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Obia.Landsat8_usgs.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Obia.Landsat8_usgs.end_date:

:ref:`end_date <I2Obia.Landsat8_usgs.end_date>`
-----------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Obia.Landsat8_usgs.keep_bands:

:ref:`keep_bands <I2Obia.Landsat8_usgs.keep_bands>`
---------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Obia.Landsat8_usgs.start_date:

:ref:`start_date <I2Obia.Landsat8_usgs.start_date>`
---------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Obia.Landsat8_usgs_infrared:

Landsat8_usgs_infrared
**********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.Landsat8_usgs_infrared.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Obia.Landsat8_usgs_infrared.enable_sensor_gapfilling:
      * - enable_sensor_gapfilling
        - False
        - Enable or disable gapfilling for landsat 8 and 9 IR data
        - bool
        - False
        - enable_sensor_gapfilling


          .. _I2Obia.Landsat8_usgs_infrared.end_date:
      * - :ref:`end_date <desc_I2Obia.Landsat8_usgs_infrared.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Obia.Landsat8_usgs_infrared.end_date>`


          .. _I2Obia.Landsat8_usgs_infrared.keep_bands:
      * - :ref:`keep_bands <desc_I2Obia.Landsat8_usgs_infrared.keep_bands>`
        - ['B10', 'B11']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Obia.Landsat8_usgs_infrared.keep_bands>`


          .. _I2Obia.Landsat8_usgs_infrared.start_date:
      * - :ref:`start_date <desc_I2Obia.Landsat8_usgs_infrared.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Obia.Landsat8_usgs_infrared.start_date>`


          .. _I2Obia.Landsat8_usgs_infrared.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Obia.Landsat8_usgs_infrared.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Obia.Landsat8_usgs_infrared.end_date:

:ref:`end_date <I2Obia.Landsat8_usgs_infrared.end_date>`
--------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Obia.Landsat8_usgs_infrared.keep_bands:

:ref:`keep_bands <I2Obia.Landsat8_usgs_infrared.keep_bands>`
------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Obia.Landsat8_usgs_infrared.start_date:

:ref:`start_date <I2Obia.Landsat8_usgs_infrared.start_date>`
------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Obia.Landsat8_usgs_optical:

Landsat8_usgs_optical
*********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.Landsat8_usgs_optical.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Obia.Landsat8_usgs_optical.enable_sensor_gapfilling:
      * - enable_sensor_gapfilling
        - True
        - Enable or disable gapfilling for landsat 8 and 9 optical data
        - bool
        - False
        - enable_sensor_gapfilling


          .. _I2Obia.Landsat8_usgs_optical.end_date:
      * - :ref:`end_date <desc_I2Obia.Landsat8_usgs_optical.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Obia.Landsat8_usgs_optical.end_date>`


          .. _I2Obia.Landsat8_usgs_optical.keep_bands:
      * - :ref:`keep_bands <desc_I2Obia.Landsat8_usgs_optical.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Obia.Landsat8_usgs_optical.keep_bands>`


          .. _I2Obia.Landsat8_usgs_optical.start_date:
      * - :ref:`start_date <desc_I2Obia.Landsat8_usgs_optical.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Obia.Landsat8_usgs_optical.start_date>`


          .. _I2Obia.Landsat8_usgs_optical.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Obia.Landsat8_usgs_optical.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Obia.Landsat8_usgs_optical.end_date:

:ref:`end_date <I2Obia.Landsat8_usgs_optical.end_date>`
-------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Obia.Landsat8_usgs_optical.keep_bands:

:ref:`keep_bands <I2Obia.Landsat8_usgs_optical.keep_bands>`
-----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Obia.Landsat8_usgs_optical.start_date:

:ref:`start_date <I2Obia.Landsat8_usgs_optical.start_date>`
-----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Obia.Landsat8_usgs_thermal:

Landsat8_usgs_thermal
*********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.Landsat8_usgs_thermal.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Obia.Landsat8_usgs_thermal.enable_sensor_gapfilling:
      * - enable_sensor_gapfilling
        - False
        - Enable or disable gapfilling for landsat 8 and 9 thermal data(temperature and emissivity)
        - bool
        - False
        - enable_sensor_gapfilling


          .. _I2Obia.Landsat8_usgs_thermal.end_date:
      * - :ref:`end_date <desc_I2Obia.Landsat8_usgs_thermal.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Obia.Landsat8_usgs_thermal.end_date>`


          .. _I2Obia.Landsat8_usgs_thermal.keep_bands:
      * - :ref:`keep_bands <desc_I2Obia.Landsat8_usgs_thermal.keep_bands>`
        - ['B10', 'EMIS']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Obia.Landsat8_usgs_thermal.keep_bands>`


          .. _I2Obia.Landsat8_usgs_thermal.start_date:
      * - :ref:`start_date <desc_I2Obia.Landsat8_usgs_thermal.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Obia.Landsat8_usgs_thermal.start_date>`


          .. _I2Obia.Landsat8_usgs_thermal.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Obia.Landsat8_usgs_thermal.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Obia.Landsat8_usgs_thermal.end_date:

:ref:`end_date <I2Obia.Landsat8_usgs_thermal.end_date>`
-------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Obia.Landsat8_usgs_thermal.keep_bands:

:ref:`keep_bands <I2Obia.Landsat8_usgs_thermal.keep_bands>`
-----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Obia.Landsat8_usgs_thermal.start_date:

:ref:`start_date <I2Obia.Landsat8_usgs_thermal.start_date>`
-----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Obia.Sentinel_2:

Sentinel_2
**********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.Sentinel_2.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Obia.Sentinel_2.end_date:
      * - :ref:`end_date <desc_I2Obia.Sentinel_2.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Obia.Sentinel_2.end_date>`


          .. _I2Obia.Sentinel_2.keep_bands:
      * - :ref:`keep_bands <desc_I2Obia.Sentinel_2.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Obia.Sentinel_2.keep_bands>`


          .. _I2Obia.Sentinel_2.start_date:
      * - :ref:`start_date <desc_I2Obia.Sentinel_2.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Obia.Sentinel_2.start_date>`


          .. _I2Obia.Sentinel_2.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Obia.Sentinel_2.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Obia.Sentinel_2.end_date:

:ref:`end_date <I2Obia.Sentinel_2.end_date>`
--------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Obia.Sentinel_2.keep_bands:

:ref:`keep_bands <I2Obia.Sentinel_2.keep_bands>`
------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Obia.Sentinel_2.start_date:

:ref:`start_date <I2Obia.Sentinel_2.start_date>`
------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Obia.Sentinel_2_L3A:

Sentinel_2_L3A
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.Sentinel_2_L3A.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Obia.Sentinel_2_L3A.end_date:
      * - :ref:`end_date <desc_I2Obia.Sentinel_2_L3A.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Obia.Sentinel_2_L3A.end_date>`


          .. _I2Obia.Sentinel_2_L3A.keep_bands:
      * - :ref:`keep_bands <desc_I2Obia.Sentinel_2_L3A.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Obia.Sentinel_2_L3A.keep_bands>`


          .. _I2Obia.Sentinel_2_L3A.start_date:
      * - :ref:`start_date <desc_I2Obia.Sentinel_2_L3A.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Obia.Sentinel_2_L3A.start_date>`


          .. _I2Obia.Sentinel_2_L3A.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Obia.Sentinel_2_L3A.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Obia.Sentinel_2_L3A.end_date:

:ref:`end_date <I2Obia.Sentinel_2_L3A.end_date>`
------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Obia.Sentinel_2_L3A.keep_bands:

:ref:`keep_bands <I2Obia.Sentinel_2_L3A.keep_bands>`
----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Obia.Sentinel_2_L3A.start_date:

:ref:`start_date <I2Obia.Sentinel_2_L3A.start_date>`
----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Obia.Sentinel_2_S2C:

Sentinel_2_S2C
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.Sentinel_2_S2C.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Obia.Sentinel_2_S2C.end_date:
      * - :ref:`end_date <desc_I2Obia.Sentinel_2_S2C.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Obia.Sentinel_2_S2C.end_date>`


          .. _I2Obia.Sentinel_2_S2C.keep_bands:
      * - :ref:`keep_bands <desc_I2Obia.Sentinel_2_S2C.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Obia.Sentinel_2_S2C.keep_bands>`


          .. _I2Obia.Sentinel_2_S2C.start_date:
      * - :ref:`start_date <desc_I2Obia.Sentinel_2_S2C.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Obia.Sentinel_2_S2C.start_date>`


          .. _I2Obia.Sentinel_2_S2C.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Obia.Sentinel_2_S2C.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Obia.Sentinel_2_S2C.end_date:

:ref:`end_date <I2Obia.Sentinel_2_S2C.end_date>`
------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Obia.Sentinel_2_S2C.keep_bands:

:ref:`keep_bands <I2Obia.Sentinel_2_S2C.keep_bands>`
----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Obia.Sentinel_2_S2C.start_date:

:ref:`start_date <I2Obia.Sentinel_2_S2C.start_date>`
----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Obia.arg_train:

arg_train
*********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.arg_train.classifier:
      * - classifier
        - None
        - otb classification algorithm
        - str
        - False
        - classifier


          .. _I2Obia.arg_train.otb_classifier_options:
      * - :ref:`otb_classifier_options <desc_I2Obia.arg_train.otb_classifier_options>`
        - None
        - OTB option for classifier.If None, the OTB default values are used
        - dict
        - False
        - :ref:`otb_classifier_options <desc_I2Obia.arg_train.otb_classifier_options>`


          .. _I2Obia.arg_train.random_seed:
      * - :ref:`random_seed <desc_I2Obia.arg_train.random_seed>`
        - None
        - Fix the random seed for random split of reference data
        - int
        - False
        - :ref:`random_seed <desc_I2Obia.arg_train.random_seed>`


          .. _I2Obia.arg_train.ratio:
      * - ratio
        - 0.5
        - Should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the train split.
        - float
        - False
        - ratio


          .. _I2Obia.arg_train.runs:
      * - :ref:`runs <desc_I2Obia.arg_train.runs>`
        - 1
        - Number of independent runs processed
        - int
        - False
        - :ref:`runs <desc_I2Obia.arg_train.runs>`


          .. _I2Obia.arg_train.sample_selection:
      * - :ref:`sample_selection <desc_I2Obia.arg_train.sample_selection>`
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - False
        - :ref:`sample_selection <desc_I2Obia.arg_train.sample_selection>`


          .. _I2Obia.arg_train.sample_validation:
      * - :ref:`sample_validation <desc_I2Obia.arg_train.sample_validation>`
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - False
        - :ref:`sample_validation <desc_I2Obia.arg_train.sample_validation>`


          .. _I2Obia.arg_train.split_ground_truth:
      * - :ref:`split_ground_truth <desc_I2Obia.arg_train.split_ground_truth>`
        - True
        - Enable the split of reference data
        - bool
        - False
        - :ref:`split_ground_truth <desc_I2Obia.arg_train.split_ground_truth>`


          .. _I2Obia.arg_train.validity_threshold:
      * - validity_threshold
        - 1
        - threshold above which a training pixel is considered valid
        - int
        - False
        - validity_threshold





Notes
=====

.. _desc_I2Obia.arg_train.otb_classifier_options:

:ref:`otb_classifier_options <I2Obia.arg_train.otb_classifier_options>`
-----------------------------------------------------------------------
This parameter is a dictionnary which accepts all OTB application parameters. To know the exhaustive parameter list  use `otbcli_TrainVectorClassifier` in a terminal or look at the OTB documentation

.. _desc_I2Obia.arg_train.random_seed:

:ref:`random_seed <I2Obia.arg_train.random_seed>`
-------------------------------------------------
Fix the random seed used for random split of reference data If set, the results must be the same for a given classifier

.. _desc_I2Obia.arg_train.runs:

:ref:`runs <I2Obia.arg_train.runs>`
-----------------------------------
Number of independent runs processed. Each run has his own learning samples. Must be an integer greater than 0

.. _desc_I2Obia.arg_train.sample_selection:

:ref:`sample_selection <I2Obia.arg_train.sample_selection>`
-----------------------------------------------------------
This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
-------

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).

.. _desc_I2Obia.arg_train.sample_validation:

:ref:`sample_validation <I2Obia.arg_train.sample_validation>`
-------------------------------------------------------------
This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
-------

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).

.. _desc_I2Obia.arg_train.split_ground_truth:

:ref:`split_ground_truth <I2Obia.arg_train.split_ground_truth>`
---------------------------------------------------------------
If set to False, the chain use all polygons for both training and validation

.. _I2Obia.builders:

builders
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.builders.builders_class_name:
      * - :ref:`builders_class_name <desc_I2Obia.builders.builders_class_name>`
        - ['I2Classification']
        - The name of the class defining the builder
        - list
        - False
        - :ref:`builders_class_name <desc_I2Obia.builders.builders_class_name>`


          .. _I2Obia.builders.builders_paths:
      * - :ref:`builders_paths <desc_I2Obia.builders.builders_paths>`
        - /path/to/iota2/sources
        - The path to user builders
        - str
        - False
        - :ref:`builders_paths <desc_I2Obia.builders.builders_paths>`





Notes
=====

.. _desc_I2Obia.builders.builders_class_name:

:ref:`builders_class_name <I2Obia.builders.builders_class_name>`
----------------------------------------------------------------
Available builders are : 'I2Classification', 'I2FeaturesMap' and 'I2Obia'

.. _desc_I2Obia.builders.builders_paths:

:ref:`builders_paths <I2Obia.builders.builders_paths>`
------------------------------------------------------
If not indicated, the iota2 source directory is used: */iota2/sequence_builders/

.. _I2Obia.chain:

chain
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.chain.check_inputs:
      * - :ref:`check_inputs <desc_I2Obia.chain.check_inputs>`
        - True
        - Enable the inputs verification
        - bool
        - False
        - :ref:`check_inputs <desc_I2Obia.chain.check_inputs>`


          .. _I2Obia.chain.cloud_threshold:
      * - :ref:`cloud_threshold <desc_I2Obia.chain.cloud_threshold>`
        - 0
        - Threshold to consider that a pixel is valid
        - int
        - False
        - :ref:`cloud_threshold <desc_I2Obia.chain.cloud_threshold>`


          .. _I2Obia.chain.color_table:
      * - color_table
        - None
        - Absolute path to the file that links the classes and their colours
        - str
        - True
        - color_table


          .. _I2Obia.chain.compression_algorithm:
      * - compression_algorithm
        - ZSTD
        - Set the gdal compression algorithm to use: NONE, LZW, ZSTD (default).All rasters write with OTB will be compress with the chosen algorithm.
        - str
        - False
        - compression_algorithm


          .. _I2Obia.chain.compression_predictor:
      * - :ref:`compression_predictor <desc_I2Obia.chain.compression_predictor>`
        - 2
        - Set the predictor for LZW and ZSTD compression: 1 (no predictor), 2 (horizontal differencing, default)
        - int
        - False
        - :ref:`compression_predictor <desc_I2Obia.chain.compression_predictor>`


          .. _I2Obia.chain.data_field:
      * - :ref:`data_field <desc_I2Obia.chain.data_field>`
        - None
        - Field name indicating classes labels in `ground_thruth`
        - str
        - True
        - :ref:`data_field <desc_I2Obia.chain.data_field>`


          .. _I2Obia.chain.first_step:
      * - first_step
        - None
        - The step group name indicating where the chain start
        - str
        - False
        - first_step


          .. _I2Obia.chain.ground_truth:
      * - ground_truth
        - None
        - Absolute path to reference data
        - str
        - True
        - ground_truth


          .. _I2Obia.chain.l5_path_old:
      * - l5_path_old
        - None
        - Absolute path to Landsat-5 images coming from old THEIA format (D*H*)
        - str
        - False
        - l5_path_old


          .. _I2Obia.chain.l8_path:
      * - l8_path
        - None
        - Absolute path to Landsat-8 images comingfrom new tiled THEIA data
        - str
        - False
        - l8_path


          .. _I2Obia.chain.l8_path_old:
      * - l8_path_old
        - None
        - Absolute path to Landsat-8 images coming from old THEIA format (D*H*)
        - str
        - False
        - l8_path_old


          .. _I2Obia.chain.l8_usgs_infrared_path:
      * - l8_usgs_infrared_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_infrared_path


          .. _I2Obia.chain.l8_usgs_optical_path:
      * - l8_usgs_optical_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_optical_path


          .. _I2Obia.chain.l8_usgs_path:
      * - l8_usgs_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_path


          .. _I2Obia.chain.l8_usgs_thermal_path:
      * - l8_usgs_thermal_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_thermal_path


          .. _I2Obia.chain.last_step:
      * - last_step
        - None
        - The step group name indicating where the chain ends
        - str
        - False
        - last_step


          .. _I2Obia.chain.list_tile:
      * - list_tile
        - None
        - List of tile to process, separated by space
        - str
        - True
        - list_tile


          .. _I2Obia.chain.logger_level:
      * - logger_level
        - INFO
        - Set the logger level: NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
        - str
        - False
        - logger_level


          .. _I2Obia.chain.minimum_required_dates:
      * - minimum_required_dates
        - 2
        - required minimum number of available dates for each sensor
        - int
        - False
        - minimum_required_dates


          .. _I2Obia.chain.nomenclature_path:
      * - nomenclature_path
        - None
        - Absolute path to the nomenclature description file
        - str
        - True
        - nomenclature_path


          .. _I2Obia.chain.output_path:
      * - :ref:`output_path <desc_I2Obia.chain.output_path>`
        - None
        - Absolute path to the output directory
        - str
        - True
        - :ref:`output_path <desc_I2Obia.chain.output_path>`


          .. _I2Obia.chain.proj:
      * - proj
        - None
        - The projection wanted. Format EPSG:XXXX is mandatory
        - str
        - True
        - proj


          .. _I2Obia.chain.region_field:
      * - :ref:`region_field <desc_I2Obia.chain.region_field>`
        - region
        - The column name for region indicator in`region_path` file
        - str
        - False
        - :ref:`region_field <desc_I2Obia.chain.region_field>`


          .. _I2Obia.chain.region_path:
      * - region_path
        - None
        - Absolute path to a region vector file
        - str
        - False
        - region_path


          .. _I2Obia.chain.remove_output_path:
      * - :ref:`remove_output_path <desc_I2Obia.chain.remove_output_path>`
        - True
        - Before the launch of iota2, remove the content of `output_path`
        - bool
        - False
        - :ref:`remove_output_path <desc_I2Obia.chain.remove_output_path>`


          .. _I2Obia.chain.s1_path:
      * - s1_path
        - None
        - Absolute path to Sentinel-1 configuration file
        - str
        - False
        - s1_path


          .. _I2Obia.chain.s2_l3a_output_path:
      * - s2_l3a_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_l3a_output_path


          .. _I2Obia.chain.s2_l3a_path:
      * - s2_l3a_path
        - None
        - Absolute path to Sentinel-2 L3A images (THEIA format)
        - str
        - False
        - s2_l3a_path


          .. _I2Obia.chain.s2_output_path:
      * - s2_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_output_path


          .. _I2Obia.chain.s2_path:
      * - s2_path
        - None
        - Absolute path to Sentinel-2 images (THEIA format)
        - str
        - False
        - s2_path


          .. _I2Obia.chain.s2_s2c_output_path:
      * - s2_s2c_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_s2c_output_path


          .. _I2Obia.chain.s2_s2c_path:
      * - s2_s2c_path
        - None
        - Absolute path to Sentinel-2 images (Sen2Cor format)
        - str
        - False
        - s2_s2c_path


          .. _I2Obia.chain.spatial_resolution:
      * - :ref:`spatial_resolution <desc_I2Obia.chain.spatial_resolution>`
        - []
        - Output spatial resolution
        - list or scalar
        - False
        - :ref:`spatial_resolution <desc_I2Obia.chain.spatial_resolution>`


          .. _I2Obia.chain.user_feat_path:
      * - :ref:`user_feat_path <desc_I2Obia.chain.user_feat_path>`
        - None
        - Absolute path to the user's features path
        - str
        - False
        - :ref:`user_feat_path <desc_I2Obia.chain.user_feat_path>`





Notes
=====

.. _desc_I2Obia.chain.check_inputs:

:ref:`check_inputs <I2Obia.chain.check_inputs>`
-----------------------------------------------
Enable the inputs verification. It can take a lot of time for large dataset. Check if region intersect reference data for instance

.. _desc_I2Obia.chain.cloud_threshold:

:ref:`cloud_threshold <I2Obia.chain.cloud_threshold>`
-----------------------------------------------------
Indicates the threshold for a polygon to be used for learning. It use the validity count, which is incremented if a cloud, a cloud shadow or a saturated pixel is detected

.. _desc_I2Obia.chain.compression_predictor:

:ref:`compression_predictor <I2Obia.chain.compression_predictor>`
-----------------------------------------------------------------
It has been noted that in some cases, once the features are written to disk, the raster file may be empty. If this is the case, please change the predictor to 1 or 3.

.. _desc_I2Obia.chain.data_field:

:ref:`data_field <I2Obia.chain.data_field>`
-------------------------------------------
All the labels values must be different to 0.
It is recommended to use a continuous range of values but it is not mandatory.
Keep in mind that the final product type is detected according to the maximum label value.
Try to keep values between 1 and 255 to avoid heavy products.

.. _desc_I2Obia.chain.output_path:

:ref:`output_path <I2Obia.chain.output_path>`
---------------------------------------------
Absolute path to the output directory.It is recommended to have one directory per run of the chain

.. _desc_I2Obia.chain.region_field:

:ref:`region_field <I2Obia.chain.region_field>`
-----------------------------------------------
This column in the database must contains string which can be converted into integers. For instance '1_2' does not match this condition.
It is mandatory that the region identifiers are > 0.

.. _desc_I2Obia.chain.remove_output_path:

:ref:`remove_output_path <I2Obia.chain.remove_output_path>`
-----------------------------------------------------------
Before the launch of iota2, remove the content of `output_path`. Only if the `first_step` is `init` and the folder name is valid 

.. _desc_I2Obia.chain.spatial_resolution:

:ref:`spatial_resolution <I2Obia.chain.spatial_resolution>`
-----------------------------------------------------------
The spatial resolution expected.It can be provided as integer or float,or as a list containing two values for non squared resolution

.. _desc_I2Obia.chain.user_feat_path:

:ref:`user_feat_path <I2Obia.chain.user_feat_path>`
---------------------------------------------------
Absolute path to the user's features path. They must be stored by tiles

.. _I2Obia.obia:

obia
****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.obia.buffer_size:
      * - :ref:`buffer_size <desc_I2Obia.obia.buffer_size>`
        - None
        - define the working size batch in number of pixels
        - int
        - False
        - :ref:`buffer_size <desc_I2Obia.obia.buffer_size>`


          .. _I2Obia.obia.full_learn_segment:
      * - :ref:`full_learn_segment <desc_I2Obia.obia.full_learn_segment>`
        - False
        - enable the use of entire segment for learning
        - bool
        - False
        - :ref:`full_learn_segment <desc_I2Obia.obia.full_learn_segment>`


          .. _I2Obia.obia.obia_segmentation_path:
      * - :ref:`obia_segmentation_path <desc_I2Obia.obia.obia_segmentation_path>`
        - None
        - filename for input segmentation
        - str
        - False
        - :ref:`obia_segmentation_path <desc_I2Obia.obia.obia_segmentation_path>`


          .. _I2Obia.obia.region_priority:
      * - :ref:`region_priority <desc_I2Obia.obia.region_priority>`
        - None
        - define an order for region intersection
        - list
        - False
        - :ref:`region_priority <desc_I2Obia.obia.region_priority>`


          .. _I2Obia.obia.stats_used:
      * - :ref:`stats_used <desc_I2Obia.obia.stats_used>`
        - ['mean']
        - list of stats used for train and classification
        - list
        - False
        - :ref:`stats_used <desc_I2Obia.obia.stats_used>`





Notes
=====

.. _desc_I2Obia.obia.buffer_size:

:ref:`buffer_size <I2Obia.obia.buffer_size>`
--------------------------------------------
this parameter is used to avoid memory issue.In case of a large temporal series,i.e one year of Sentinel2 images a recommended size is 2000.For lower number of date, the buffer size can be increased.If buffer_size is larger than the image size, the whole image will be processed in one time.

.. _desc_I2Obia.obia.full_learn_segment:

:ref:`full_learn_segment <I2Obia.obia.full_learn_segment>`
----------------------------------------------------------
if True: keep each segment which intersect the learning samples. If False, the segments are clipped with learning polygon shape

.. _desc_I2Obia.obia.obia_segmentation_path:

:ref:`obia_segmentation_path <I2Obia.obia.obia_segmentation_path>`
------------------------------------------------------------------
If parameter is None then a segmentation for each tile is processed using SLIC algorithm

.. _desc_I2Obia.obia.region_priority:

:ref:`region_priority <I2Obia.obia.region_priority>`
----------------------------------------------------
if a list is provided, the list order is used instead of the numeric order.This option can be used in case of very unbalanced region size.

.. _desc_I2Obia.obia.stats_used:

:ref:`stats_used <I2Obia.obia.stats_used>`
------------------------------------------
this list accepts only five values: mean, count, min, max, std
The choice of statistics used should be considered in relation to the number of dates used.Because of the constraints on vector formats, one must think about the number of features this creates: nb_stats_choosen * nb_bands * nb_dates. Too many spectral bands can cause an error in the execution of the string.

.. _I2Obia.pretrained_model:

pretrained_model
****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.pretrained_model.boundary_buffer:
      * - boundary_buffer
        - None
        - List of boundary buffer size
        - list
        - False
        - boundary_buffer


          .. _I2Obia.pretrained_model.function:
      * - :ref:`function <desc_I2Obia.pretrained_model.function>`
        - None
        - Predict function name
        - str
        - False
        - :ref:`function <desc_I2Obia.pretrained_model.function>`


          .. _I2Obia.pretrained_model.mode:
      * - :ref:`mode <desc_I2Obia.pretrained_model.mode>`
        - None
        - Algorythm nature (classification or regression)
        - str
        - False
        - :ref:`mode <desc_I2Obia.pretrained_model.mode>`


          .. _I2Obia.pretrained_model.model:
      * - :ref:`model <desc_I2Obia.pretrained_model.model>`
        - None
        - Serialized object containing the model
        - str
        - False
        - :ref:`model <desc_I2Obia.pretrained_model.model>`


          .. _I2Obia.pretrained_model.module:
      * - :ref:`module <desc_I2Obia.pretrained_model.module>`
        - /path/to/iota2/sources
        - Absolute path to the python module
        - str
        - False
        - :ref:`module <desc_I2Obia.pretrained_model.module>`





Notes
=====

.. _desc_I2Obia.pretrained_model.function:

:ref:`function <I2Obia.pretrained_model.function>`
--------------------------------------------------
This function must have the imposed signature. It not accept any others parameters. All model dedicated parameters must be stored alongside the model.

.. _desc_I2Obia.pretrained_model.mode:

:ref:`mode <I2Obia.pretrained_model.mode>`
------------------------------------------
The python module must contains the predict function It must handle all the potential dependencies and import related to the correct model instanciation

.. _desc_I2Obia.pretrained_model.model:

:ref:`model <I2Obia.pretrained_model.model>`
--------------------------------------------
In the configuration file, the mandatory keys $REGION and $SEED must be present as they are replaced by iota2. In case of only one region, the region value is set to 1. Look at the documentation about the model constraint.

.. _desc_I2Obia.pretrained_model.module:

:ref:`module <I2Obia.pretrained_model.module>`
----------------------------------------------
The python module must contains the predict function It must handle all the potential dependencies and import related to the correct model instanciation

.. _I2Obia.sensors_data_interpolation:

sensors_data_interpolation
**************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.sensors_data_interpolation.auto_date:
      * - :ref:`auto_date <desc_I2Obia.sensors_data_interpolation.auto_date>`
        - True
        - Enable the use of `start_date` and `end_date`
        - bool
        - False
        - :ref:`auto_date <desc_I2Obia.sensors_data_interpolation.auto_date>`


          .. _I2Obia.sensors_data_interpolation.use_additional_features:
      * - use_additional_features
        - False
        - enable the use of additional features
        - bool
        - False
        - use_additional_features


          .. _I2Obia.sensors_data_interpolation.use_gapfilling:
      * - use_gapfilling
        - True
        - enable the use of gapfilling (clouds/temporal interpolation)
        - bool
        - False
        - use_gapfilling


          .. _I2Obia.sensors_data_interpolation.write_outputs:
      * - :ref:`write_outputs <desc_I2Obia.sensors_data_interpolation.write_outputs>`
        - False
        - write temporary files
        - bool
        - False
        - :ref:`write_outputs <desc_I2Obia.sensors_data_interpolation.write_outputs>`





Notes
=====

.. _desc_I2Obia.sensors_data_interpolation.auto_date:

:ref:`auto_date <I2Obia.sensors_data_interpolation.auto_date>`
--------------------------------------------------------------
If True, iota2 will automatically guess the first and the last interpolation date. Else, `start_date` and `end_date` of each sensors will be used

.. _desc_I2Obia.sensors_data_interpolation.write_outputs:

:ref:`write_outputs <I2Obia.sensors_data_interpolation.write_outputs>`
----------------------------------------------------------------------
Write the time series before and after gapfilling, the mask time series, and also the feature time series. This option required a large amount of free disk space.

.. _I2Obia.simplification:

simplification
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.simplification.gridsize:
      * - :ref:`gridsize <desc_I2Obia.simplification.gridsize>`
        - None
        - number of lines and columns of the serialization process
        - int
        - False
        - :ref:`gridsize <desc_I2Obia.simplification.gridsize>`


          .. _I2Obia.simplification.inland:
      * - :ref:`inland <desc_I2Obia.simplification.inland>`
        - None
        - inland water limit shapefile
        - str
        - False
        - :ref:`inland <desc_I2Obia.simplification.inland>`


          .. _I2Obia.simplification.nomenclature:
      * - :ref:`nomenclature <desc_I2Obia.simplification.nomenclature>`
        - None
        - configuration file which describe nomenclature
        - configuration file which describe nomenclature
        - False
        - :ref:`nomenclature <desc_I2Obia.simplification.nomenclature>`


          .. _I2Obia.simplification.rssize:
      * - :ref:`rssize <desc_I2Obia.simplification.rssize>`
        - 20
        - Resampling size of input classification raster (projection unit)
        - int
        - False
        - :ref:`rssize <desc_I2Obia.simplification.rssize>`


          .. _I2Obia.simplification.umc1:
      * - :ref:`umc1 <desc_I2Obia.simplification.umc1>`
        - None
        - MMU for the first regularization
        - int
        - False
        - :ref:`umc1 <desc_I2Obia.simplification.umc1>`


          .. _I2Obia.simplification.umc2:
      * - :ref:`umc2 <desc_I2Obia.simplification.umc2>`
        - None
        - MMU for the second regularization
        - int
        - False
        - :ref:`umc2 <desc_I2Obia.simplification.umc2>`





Notes
=====

.. _desc_I2Obia.simplification.gridsize:

:ref:`gridsize <I2Obia.simplification.gridsize>`
------------------------------------------------
This parameter is useful only for large areas for which vectorization process can not be executed (memory limitation). By 'serialization', we mean parallel vectorization processes. If not None, regularized classification raster is splitted in gridsize x gridsize rasters

.. _desc_I2Obia.simplification.inland:

:ref:`inland <I2Obia.simplification.inland>`
--------------------------------------------
to vectorize only inland waters, and not unnecessary sea water areas

.. _desc_I2Obia.simplification.nomenclature:

:ref:`nomenclature <I2Obia.simplification.nomenclature>`
--------------------------------------------------------
This configuration file includes code, color, description and vector field alias of each class

.. code-block:: bash

    Classes:
    {
            Level1:
            {
                    "Urbain":
                    {
                    code:100
                    alias:"Urbain"
                    color:"#b106b1"
                    }
                    ...
            }
            Level2:
            {
                   "Urbain dense":
                   {
                   code:1
                   alias:"UrbainDens"
                   color:"#ff00ff"
                   parent:100
                   }
                   ...
            }
    }


.. _desc_I2Obia.simplification.rssize:

:ref:`rssize <I2Obia.simplification.rssize>`
--------------------------------------------
OSO-like vectorization requires a resampling step in order to regularize and decrease raster polygons number, If None, classification is not resampled

.. _desc_I2Obia.simplification.umc1:

:ref:`umc1 <I2Obia.simplification.umc1>`
----------------------------------------
It is an interface of parameter '-st' of gdal_sieve.py function. If None, classification is not regularized

.. _desc_I2Obia.simplification.umc2:

:ref:`umc2 <I2Obia.simplification.umc2>`
----------------------------------------
OSO-like vectorization process requires 2 successive regularization, if you need a single regularization, let this parameter to None

.. _I2Obia.slurm:

slurm
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.slurm.account:
      * - :ref:`account <desc_I2Obia.slurm.account>`
        - None
        - Feed the sbatch parameter 'account'
        - str
        - False
        - :ref:`account <desc_I2Obia.slurm.account>`





Notes
=====

.. _desc_I2Obia.slurm.account:

:ref:`account <I2Obia.slurm.account>`
-------------------------------------
The section 'slurm' is only available once the `Slurm <https://slurm.schedmd.com/documentation.html>`_ orchestrator is involved in jobs submission.

.. _I2Obia.task_retry_limits:

task_retry_limits
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.task_retry_limits.allowed_retry:
      * - allowed_retry
        - 0
        - allow dask to retry a failed job N times
        - int
        - False
        - allowed_retry


          .. _I2Obia.task_retry_limits.maximum_cpu:
      * - :ref:`maximum_cpu <desc_I2Obia.task_retry_limits.maximum_cpu>`
        - 4
        - the maximum number of CPU available
        - int
        - False
        - :ref:`maximum_cpu <desc_I2Obia.task_retry_limits.maximum_cpu>`


          .. _I2Obia.task_retry_limits.maximum_ram:
      * - :ref:`maximum_ram <desc_I2Obia.task_retry_limits.maximum_ram>`
        - 16.0
        - the maximum amount of RAM available. (gB)
        - float
        - False
        - :ref:`maximum_ram <desc_I2Obia.task_retry_limits.maximum_ram>`





Notes
=====

.. _desc_I2Obia.task_retry_limits.maximum_cpu:

:ref:`maximum_cpu <I2Obia.task_retry_limits.maximum_cpu>`
---------------------------------------------------------
the amount of cpu will be doubled if the task is killed due to ram overconsumption until maximum_cpu or allowed_retry are reach

.. _desc_I2Obia.task_retry_limits.maximum_ram:

:ref:`maximum_ram <I2Obia.task_retry_limits.maximum_ram>`
---------------------------------------------------------
the amount of RAM will be doubled if the task is killed due to ram overconsumption until maximum_ram or allowed_retry are reach

.. _I2Obia.userFeat:

userFeat
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Obia.userFeat.arbo:
      * - arbo
        - /*
        - input folder hierarchy
        - str
        - False
        - arbo


          .. _I2Obia.userFeat.patterns:
      * - patterns
        - ALT,ASP,SLP
        - key name for detect the input images
        - str
        - False
        - patterns





