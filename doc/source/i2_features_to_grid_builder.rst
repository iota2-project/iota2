I2FeaturesToGrid
################

.. _I2FeaturesToGrid.builders:

builders
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesToGrid.builders.builders_class_name:
      * - :ref:`builders_class_name <desc_I2FeaturesToGrid.builders.builders_class_name>`
        - ['I2Classification']
        - The name of the class defining the builder
        - list
        - False
        - :ref:`builders_class_name <desc_I2FeaturesToGrid.builders.builders_class_name>`


          .. _I2FeaturesToGrid.builders.builders_paths:
      * - :ref:`builders_paths <desc_I2FeaturesToGrid.builders.builders_paths>`
        - /path/to/iota2/sources
        - The path to user builders
        - str
        - False
        - :ref:`builders_paths <desc_I2FeaturesToGrid.builders.builders_paths>`





Notes
=====

.. _desc_I2FeaturesToGrid.builders.builders_class_name:

:ref:`builders_class_name <I2FeaturesToGrid.builders.builders_class_name>`
--------------------------------------------------------------------------
Available builders are : 'I2Classification', 'I2FeaturesMap' and 'I2Obia'

.. _desc_I2FeaturesToGrid.builders.builders_paths:

:ref:`builders_paths <I2FeaturesToGrid.builders.builders_paths>`
----------------------------------------------------------------
If not indicated, the iota2 source directory is used: */iota2/sequence_builders/

.. _I2FeaturesToGrid.chain:

chain
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesToGrid.chain.features_path:
      * - features_path
        - None
        - input directory containing features as rasters
        - str
        - False
        - features_path


          .. _I2FeaturesToGrid.chain.from_rasterdb_resampling_method:
      * - from_rasterdb_resampling_method
        - nn
        - output features type choice among `gdalwarp.html#cmdoption-gdalwarp-r <https://www.orfeo-toolbox.org/CookBook/Applications/app_Superimpose.html>`_. Enabled if chain.rasters_grid_path is set
        - str
        - False
        - from_rasterdb_resampling_method


          .. _I2FeaturesToGrid.chain.from_vectordb_resampling_method:
      * - from_vectordb_resampling_method
        - near
        - output features type choice among `gdalwarp.html#cmdoption-gdalwarp-r <https://gdal.org/programs/gdalwarp.html#cmdoption-gdalwarp-r>`_. Enabled if chain.grid is set
        - str
        - False
        - from_vectordb_resampling_method


          .. _I2FeaturesToGrid.chain.grid:
      * - grid
        - None
        - input grid to fit
        - str
        - False
        - grid


          .. _I2FeaturesToGrid.chain.list_tile:
      * - list_tile
        - None
        - List of tile to process, separated by space
        - str
        - True
        - list_tile


          .. _I2FeaturesToGrid.chain.out_worldclim_dtype:
      * - out_worldclim_dtype
        - float32
        - output worlclim data type, ie : 'uint16', 'float32'.
        - np.dtype
        - False
        - out_worldclim_dtype


          .. _I2FeaturesToGrid.chain.out_worldclim_rescale_range:
      * - out_worldclim_rescale_range
        - None
        - rescale worldclim data between 0 and max(np.dtype) at run time for RAM usage purpose.
        - np.dtype
        - False
        - out_worldclim_rescale_range


          .. _I2FeaturesToGrid.chain.output_features_pix_type:
      * - output_features_pix_type
        - float
        - output features type choice among uint8/uint16/int16/uint32/int32/float/double.
        - str
        - False
        - output_features_pix_type


          .. _I2FeaturesToGrid.chain.output_path:
      * - :ref:`output_path <desc_I2FeaturesToGrid.chain.output_path>`
        - None
        - Absolute path to the output directory
        - str
        - True
        - :ref:`output_path <desc_I2FeaturesToGrid.chain.output_path>`


          .. _I2FeaturesToGrid.chain.rasters_grid_path:
      * - :ref:`rasters_grid_path <desc_I2FeaturesToGrid.chain.rasters_grid_path>`
        - None
        - input grid to fit
        - str
        - False
        - :ref:`rasters_grid_path <desc_I2FeaturesToGrid.chain.rasters_grid_path>`


          .. _I2FeaturesToGrid.chain.resampling_bco_radius:
      * - :ref:`resampling_bco_radius <desc_I2FeaturesToGrid.chain.resampling_bco_radius>`
        - 2
        - otb radius for bicubic interpolation.
        - int
        - False
        - :ref:`resampling_bco_radius <desc_I2FeaturesToGrid.chain.resampling_bco_radius>`


          .. _I2FeaturesToGrid.chain.s1_dir:
      * - :ref:`s1_dir <desc_I2FeaturesToGrid.chain.s1_dir>`
        - None
        - Sentinel1 data directory
        - str
        - False
        - :ref:`s1_dir <desc_I2FeaturesToGrid.chain.s1_dir>`


          .. _I2FeaturesToGrid.chain.s2_path:
      * - s2_path
        - None
        - Absolute path to Sentinel-2 images (THEIA format)
        - str
        - False
        - s2_path


          .. _I2FeaturesToGrid.chain.spatial_resolution:
      * - :ref:`spatial_resolution <desc_I2FeaturesToGrid.chain.spatial_resolution>`
        - []
        - Output spatial resolution
        - list or scalar
        - True
        - :ref:`spatial_resolution <desc_I2FeaturesToGrid.chain.spatial_resolution>`


          .. _I2FeaturesToGrid.chain.srtm_path:
      * - srtm_path
        - None
        - Path to a directory containing srtm data
        - str
        - False
        - srtm_path


          .. _I2FeaturesToGrid.chain.tile_field:
      * - tile_field
        - None
        - column name in 'grid' containing tile's name.
        - str
        - False
        - tile_field


          .. _I2FeaturesToGrid.chain.worldclim_path:
      * - worldclim_path
        - None
        - Path to a directory containing world clim data
        - str
        - False
        - worldclim_path





Notes
=====

.. _desc_I2FeaturesToGrid.chain.output_path:

:ref:`output_path <I2FeaturesToGrid.chain.output_path>`
-------------------------------------------------------
Absolute path to the output directory.It is recommended to have one directory per run of the chain

.. _desc_I2FeaturesToGrid.chain.rasters_grid_path:

:ref:`rasters_grid_path <I2FeaturesToGrid.chain.rasters_grid_path>`
-------------------------------------------------------------------
all rasters must be in the same directory and must be named by tile ie : 'T31TCJ.tif'

.. _desc_I2FeaturesToGrid.chain.resampling_bco_radius:

:ref:`resampling_bco_radius <I2FeaturesToGrid.chain.resampling_bco_radius>`
---------------------------------------------------------------------------
otb radius for bicubic interpolation. Enabled if 'nn' was set to bco in 'from_rasterdb_resampling_method' parameter

.. _desc_I2FeaturesToGrid.chain.s1_dir:

:ref:`s1_dir <I2FeaturesToGrid.chain.s1_dir>`
---------------------------------------------
The directory must contains all *.SAFE Sentinel1 data.

.. _desc_I2FeaturesToGrid.chain.spatial_resolution:

:ref:`spatial_resolution <I2FeaturesToGrid.chain.spatial_resolution>`
---------------------------------------------------------------------
The spatial resolution expected.It can be provided as integer or float,or as a list containing two values for non squared resolution

.. _I2FeaturesToGrid.pretrained_model:

pretrained_model
****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2FeaturesToGrid.pretrained_model.boundary_buffer:
      * - boundary_buffer
        - None
        - List of boundary buffer size
        - list
        - False
        - boundary_buffer


          .. _I2FeaturesToGrid.pretrained_model.function:
      * - :ref:`function <desc_I2FeaturesToGrid.pretrained_model.function>`
        - None
        - Predict function name
        - str
        - False
        - :ref:`function <desc_I2FeaturesToGrid.pretrained_model.function>`


          .. _I2FeaturesToGrid.pretrained_model.mode:
      * - :ref:`mode <desc_I2FeaturesToGrid.pretrained_model.mode>`
        - None
        - Algorythm nature (classification or regression)
        - str
        - False
        - :ref:`mode <desc_I2FeaturesToGrid.pretrained_model.mode>`


          .. _I2FeaturesToGrid.pretrained_model.model:
      * - :ref:`model <desc_I2FeaturesToGrid.pretrained_model.model>`
        - None
        - Serialized object containing the model
        - str
        - False
        - :ref:`model <desc_I2FeaturesToGrid.pretrained_model.model>`


          .. _I2FeaturesToGrid.pretrained_model.module:
      * - :ref:`module <desc_I2FeaturesToGrid.pretrained_model.module>`
        - /path/to/iota2/sources
        - Absolute path to the python module
        - str
        - False
        - :ref:`module <desc_I2FeaturesToGrid.pretrained_model.module>`





Notes
=====

.. _desc_I2FeaturesToGrid.pretrained_model.function:

:ref:`function <I2FeaturesToGrid.pretrained_model.function>`
------------------------------------------------------------
This function must have the imposed signature. It not accept any others parameters. All model dedicated parameters must be stored alongside the model.

.. _desc_I2FeaturesToGrid.pretrained_model.mode:

:ref:`mode <I2FeaturesToGrid.pretrained_model.mode>`
----------------------------------------------------
The python module must contains the predict function It must handle all the potential dependencies and import related to the correct model instanciation

.. _desc_I2FeaturesToGrid.pretrained_model.model:

:ref:`model <I2FeaturesToGrid.pretrained_model.model>`
------------------------------------------------------
In the configuration file, the mandatory keys $REGION and $SEED must be present as they are replaced by iota2. In case of only one region, the region value is set to 1. Look at the documentation about the model constraint.

.. _desc_I2FeaturesToGrid.pretrained_model.module:

:ref:`module <I2FeaturesToGrid.pretrained_model.module>`
--------------------------------------------------------
The python module must contains the predict function It must handle all the potential dependencies and import related to the correct model instanciation

