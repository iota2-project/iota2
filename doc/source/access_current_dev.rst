Access develop branch
#####################

Release planning
****************

A release is a long time process, which requires suffisant contribution to be a relevant improvement. Otherwise, the new features must be tested before, then they can be stay in development for several months.

For help development or to use a new feature not yet released, it can be useful to access the current development branch and use it as a full operational conda enviromnent.

Access via conda env
********************

On anaconda cloud, a package named `iota2_develop <https://anaconda.org/iota2/iota2_develop>`_ is available. This package can be updated on demand, and contains the state of develop branch corresponding to his creation.

This environment is fully operationnal but not correspond to the current development branch (bug fix or recent add). But it permits to install easily most of the dependencies.

Real-time access to develop
***************************

Once you have a conda environment (here `iota_dev`) installed you can access to the source of iota2.

.. warning::

   In the following commands the chain 'iota2_dev' refers to the name of environment.
   To have several env see the note at the end of this part.

First, clone iota2 project somewhere:

.. code-block:: console

	cd /home/doc/somewhere/
	git clone https://framagit.org/iota2-project/iota2.git

Go to the freshly cloned source directory, and install with pip:

.. code-block:: console

    pip install -e .

Note the `-e` option for "editable" code. Now, each call to iota2 will use the code located in `/home/doc/somewhere`.

Once done, simply use this environment as usual:

.. code-block:: console

   conda activate iota_dev
   Iota2.py ...

Then you can update the code located in `/home/doc/somewhere/iota2`, with git commands (pull, checkout, branch) or create your own branches and modify the code. The conda environment always uses the current state of your iota2 source repository.

.. code-block:: console

   cd /home/doc/somewhere
   git checkout develop

This command must return that you are already on develop on a fresh cloned repository.

.. warning::

   This manipulation must be done for every iota2 environment that you use.
   A good practice for development is to create two environments, one following the `release` tag (the online available package) and the other using custom sources.
