Sentinel-1
##########

Description
***********

Sentinel-1 images are SAR acquisitions with available in several modes and correction levels. iota2 is only able to use `IW GRD <https://sentinels.copernicus.eu/ja/web/sentinel/technical-guides/sentinel-1-sar/products-algorithms/level-1-algorithms/ground-range-detected>`_ data, which are amplitude (real) images provided at 10 m x 10 m pixel spacing.

This data can be freely downloaded from the data provider's website `PEPS <https://peps.cnes.fr/rocket/#/home>`_.

Using Sentinel-1
****************

``chain.s1_path`` is the parameter to enable the use of Sentinel-1 products by iota2. This Sentinel-1 data can be used together with the other sensors (ie:Sentinel-2) available in iota2.

.. code-block:: python

        chain:
        {
            ...
            s1_path:"/absolute/path/s1.cfg"
            ...
        }

.. Warning::

    The parameter ``s1_path`` contains a path to a configuration file and not a directory as usual.

.. _about_s1:
	
About Sentinel-1 data
*********************

Sentinel-1 acquisitions are not provided tiled (as the data typically usable by iota2) as these are provided in `level 1 <https://sentinels.copernicus.eu/ja/web/sentinel/technical-guides/sentinel-1-sar/products-algorithms/level-1-algorithms/products>`_. The processing performed by iota2 on these data will therefore consist firstly in tiling them according to a grid of interest (for example, the grid of Sentinel-2). This tiling operation will make possible, for example, to superimpose radar data on optical data. To do this, iota2 will rely on reference images that must be available before any processing with Sentinel-1 can be done.

In addition to the geometric processing (tiling and ortho-rectification), iota2 will also perform radiometric corrections. These consist in antenna pattern correction, conversion to backscatter coefficient (gamma naught) and multi-temporal speckle filtering (Quegan filter). See section :ref:`Sentinel-1 data workflow <s1_wf>`

All the information about the reference images and/or the Sentinel-1 proessing parameters must be specified in a configuration file. The table below summarises all the fields available in the configuration file.

Contents of the Sentinel-1 configuration file
*********************************************

The configuration file must be provided in `configparser <https://docs.python.org/3/library/configparser.html#quick-start>`_ format with the following fields and sections:


.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Section
        - Default Value
        - Description
        - Type
        - Mandatory

          .. _output_s1_desc: 
      * - :ref:`output <desc_output_s1>`
        - Paths
        - 
        - output path to store Sentinel-1 images processed by iota2 (tiled and reprojected)
        - str
        - True

          .. _s1_img_desc: 
      * - :ref:`s1images <desc_s1_img>`
        - Paths
        - 
        - path to the directory containing all Sentinel-1 data
        - str
        - True

          .. _s1_srtm_desc: 
      * - :ref:`srtm <desc_s1_srtm>`
        - Paths
        - 
        - path to the directory containing all SRTM data
        - str
        - True

	      .. _s1_geoid_desc: 
      * - :ref:`geoidfile <desc_s1_geoid>`
        - Paths
        - 
        - path to the geoid file (egm96.grd)
        - str
        - True

	      .. _s1_reffolder_desc: 
      * - :ref:`referencesfolder <desc_s1_reffolder>`
        - Processing
        - 
        - path to the directory containing reference raster to reproject Sentinel-1 data on top of it.
        - str
        - True

          .. _s1_rastpattern_desc: 
      * - :ref:`rasterpattern <desc_s1_rastpattern>`
        - Processing
        - 
        - pattern used to find reference raster by tile
        - str
        - True

	      .. _s1_tilesshape_desc: 
      * - :ref:`tilesshapefile <desc_s1_tilesshape>`
        - Processing
        - 
        - shapefile containing the grid of interest over the globe.
        - str
        - True

	      .. _s1_srtmshape_desc: 
      * - :ref:`srtmshapefile <desc_s1_srtmshape>`
        - Processing
        - 
        - shapefile containing the srtm grid over the globe.
        - str
        - True

		  .. _s1_tiles_desc: 
      * - :ref:`tiles <desc_s1_tiles>`
        - Processing
        - 
        - list of tiles to process
        - list
        - True
		  
          .. _s1_timeres_desc: 
      * - :ref:`temporalresolution <desc_s1_timeres>`
        - Processing
        - 10
        - gap (in days) between interpolated data
        - int
        - False
		  
          .. _s1_borderthreshold_desc: 
      * - borderthreshold
        - Processing
        - 1e-3
        - threshold (in dB) above which the data is considered valid
        - float
        - False

		  .. _s1_ram_desc: 
      * - ramperprocess
        - Processing
        - 5000
        - RAM amount (Mo) to process S1 data
        - int
        - False
		  
		  .. _s1_gap_desc: 
      * - gapfilling_interpolation
        - Processing
        - linear
        - 'linear' or 'spline' temporal interpolation method
        - str
        - False

		  .. _s1_windowradius_desc: 
      * - window_radius
        - Filtering
        - 2
        - window radius of the speckle filter
        - int
        - False

.. _desc_output_s1:

:ref:`output <output_s1_desc>`
------------------------------

This directory will contain one directory per tile (e.g. 31TCJ), containing the tree structure :


.. code-block:: console
				
				/path/to/output/31TCJ/
				├── filtered
				├── outcore_S1_vh_ASC.geom
				├── outcore_S1_vh_ASC.tif
				├── outcore_S1_vh_DES.geom
				├── outcore_S1_vh_DES.tif
				├── outcore_S1_vv_ASC.geom
				├── outcore_S1_vv_ASC.tif
				├── outcore_S1_vv_DES.geom
				├── outcore_S1_vv_DES.tif
				├── s1a_31TCJ_vh_ASC_20170519t174702.geom
				├── s1a_31TCJ_vh_ASC_20170519t174702.tif
				├── s1a_31TCJ_vh_DES_20151231t060030.geom
				├── s1a_31TCJ_vh_DES_20151231t060030.tif
				├── s1a_31TCJ_vv_ASC_20170519t174702_BorderMask.tif
				├── s1a_31TCJ_vv_ASC_20170519t174702.geom
				├── s1a_31TCJ_vv_ASC_20170519t174702.tif
				├── ...
				├── s1a_31TCJ_vv_DES_20151231t060030_BorderMask.tif
				├── s1a_31TCJ_vv_DES_20151231t060030.geom
				├── s1a_31TCJ_vv_DES_20151231t060030.tif
				├── S1_vh_ASC_dates.txt
				├── S1_vh_DES_dates.txt
				├── S1_vv_ASC_dates.txt
				└── S1_vv_DES_dates.txt

* tiled Sentinel-1 data : ``s1a_31TCJ_*_*_YYYYMMDDtHHMMSS.tif``, the associated mask ``*BorderMask.tif`` and the geom file ``*.geom*``.
* ``outcore_*.tif`` rasters which are necessary for speckle filtering.
* dates files : ``dates.txt``
  
The ``filtered`` directory is empty by default, but will contain the following tree if the :ref:`write_outputs <i2_classification.sensors_data_interpolation.write_outputs>` parameter is set.

.. code-block:: console

				├── enl_S1_vh_ASC.geom
				├── enl_S1_vh_ASC.tif
				├── enl_S1_vh_DES.geom
				├── enl_S1_vh_DES.tif
				├── enl_S1_vv_ASC.geom
				├── enl_S1_vv_ASC.tif
				├── enl_S1_vv_DES.geom
				├── enl_S1_vv_DES.tif
				├── S1_vh_ASC_Filtered.geom
				├── S1_vh_ASC_Filtered.tif
				├── S1_vh_DES_Filtered.geom
				├── S1_vh_DES_Filtered.tif
				├── S1_vv_ASC_Filtered.geom
				├── S1_vv_ASC_Filtered.tif
				├── S1_vv_DES_Filtered.geom
				└── S1_vv_DES_Filtered.tif

* ``enl_*.tif`` rasters which are necessary for speckle filtering.
* ``S1_*_Filtered.tif`` rasters which compose the Sentinel-1 time series after speckle filtering. These stacks are ordered chronologically, the first band corresponding to the first date (see the text files in the tile's output directory)

.. _desc_s1_img:

:ref:`s1images <s1_img_desc>`
-----------------------------

The ``s1images`` parameter must contain all the Sentinel-1 acquisitions that should be used for the execution of iota2. For example, for 3 Sentinel-1 acquisitions, the directory indicated by the parameter must contain:

.. code-block:: console

				/path/to/s1images/
				├── S1A_IW_GRDH_1SDV_20151231T060030_20151231T060055_009282_00D66B_F0EB.SAFE
				│   ├── annotation
				│   ├── manifest.safe
				│   ├── measurement
				│   ├── preview
				│   ├── S1A_IW_GRDH_1SDV_20151231T060030_20151231T060055_009282_00D66B_F0EB.SAFE-report-20151231T083359.pdf
				│   └── support
				├── S1A_IW_GRDH_1SDV_20170519T174702_20170519T174727_016654_01BA3E_B031.SAFE
				│   ├── annotation
				│   ├── manifest.safe
				│   ├── measurement
				│   ├── preview
				│   ├── S1A_IW_GRDH_1SDV_20170519T174702_20170519T174727_016654_01BA3E_B031.SAFE-report-20170519T201737.pdf
				│   └── support
				└── S1B_IW_GRDH_1SDV_20170518T175426_20170518T175451_005656_009E79_5C47.SAFE
				    ├── annotation
				    ├── manifest.safe
				    ├── measurement
				    ├── preview
				    ├── S1B_IW_GRDH_1SDV_20170518T175426_20170518T175451_005656_009E79_5C47.SAFE-report-20170518T202822.pdf
				    └── support

.. Warning:: The directory must contains only `IW_GRD <https://sentinels.copernicus.eu/ja/web/sentinel/technical-guides/sentinel-1-sar/products-algorithms/level-1-algorithms/ground-range-detected>`_ rasters.

.. _desc_s1_srtm:

:ref:`srtm <s1_srtm_desc>`
--------------------------

In order to reproject Sentinel-1 images as accurately as possible, it is necessary to use the `SRTM <https://en.wikipedia.org/wiki/Shuttle_Radar_Topography_Mission>`_ data. This data is freely available over most of the world.

.. _desc_s1_geoid:

:ref:`geoidfile <s1_geoid_desc>`
--------------------------------

For the same reason that it is necessary to use SRTM data, it is also necessary to use a geoid file (egm96.grd). A version of the file is available on the OTB repository : https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb-data/blob/master/Input/DEM/egm96.grd and https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb-data/-/blob/master/Input/DEM/egm96.grd.hdr which must me placed next to the geoid file. 

.. _desc_s1_reffolder:

:ref:`referencesfolder <s1_reffolder_desc>`
------------------------------------------

As indicated in the section :ref:`about Sentinel-1<about_s1>`, iota2 uses images that have already been reprojected into the correct projection so that the Sentinel-1 data can be superimposed. This parameter indicates the directory containing the data that will be used as a reference. For example, in the case where we are only dealing with the T31TCJ tile, it must be constructed as follows:

.. code-block:: console

			     /path/to/referencesfolder
				 T31TCJ
				 ├── SENTINEL2A_20151130-105641-486_L2A_T31TCJ_D_V1-0
				 │   ├── MASKS
				 │   ├── ...
				 │   └── SENTINEL2A_20151130-105641-486_L2A_T31TCJ_D_V1-0_FRE_STACK.tif
				 └── SENTINEL2A_20151203-110846-328_L2A_T31TCJ_D_V1-0
				     ├── MASKS
				     ├── ...
				     └── SENTINEL2A_20151203-110846-328_L2A_T31TCJ_D_V1-0_FRE_STACK.tif

Where ``SENTINEL2A_20151130-105641-486_L2A_T31TCJ_D_V1-0`` and ``SENTINEL2A_20151203-110846-328_L2A_T31TCJ_D_V1-0`` are 2 acquisitions already processed by iota2 (presence of the raster ``STACK.tif``). In fact, only one reference image per tile is required.


.. _desc_s1_rastpattern:

:ref:`rasterpattern <s1_rastpattern_desc>`
------------------------------------------

This parameter allows to target a particular image in the reference acquisition directory. The raster found will then be the reference image for the reprojection of Sentinel-1 images.

Usually, when the Sentinel-2 data is used as a reference then ``rasterpattern`` is equal to ``STACK.tif`` because these rasters are all of the same resolution and in the same projection regardless of the tile.

.. _desc_s1_tilesshape:

:ref:`tilesshapefile <s1_tilesshape_desc>`
------------------------------------------
Shapefile containing the grid of interest over the globe. Sentinel-2 grid is downloadable at https://github.com/CNES/S1Tiling/tree/master/shapefile (files ``Features.*``)

.. _desc_s1_srtmshape:

:ref:`srtmshapefile <s1_srtmshape_desc>`
----------------------------------------
Shapefile containing the SRTM grid over the globe. Downloadable at https://github.com/CNES/S1Tiling/tree/master/shapefile (files ``srtm.*``)

.. _desc_s1_tiles:

:ref:`tiles <s1_tiles_desc>`
----------------------------
List of tiles to process, comma separated and without any empty spaces :

.. code-block:: console

				tiles:31TCJ,31TDJ

.. _desc_s1_timeres:

:ref:`temporalresolution <s1_timeres_desc>`
-------------------------------------------
iota2 needs to homogenise the number of dates used over the whole area of interest (the number of dates can be different per tile and pixel). To do this, iota2 places all Sentinel-1 data in a common time grid. This parameter will then be the gap between each interpolated Sentinel-1 date.

.. Note::

    As example, two Sentinel-1 configuration files are provided :download:`s1_minimal.cfg <./config/s1_minimal.cfg>` and :download:`s1_full.cfg <./config/s1_full.cfg>` which are respectivelly a configuration file using all default values and with all parameters set.


.. _s1_wf:

Sentinel-1 data workflow
************************

All Sentinel-1 images are subjected to the gamma calibration phase using the OTB 
`SARCalibration <https://www.orfeo-toolbox.org/CookBook/Applications/app_SARCalibration.html>`_ application, and then these images are reprojected onto the grid of interest. Next, we proceed to filter the speckle noise in the images. To filter this noise, we use the multi-temporal filter proposed by `Gelas et al.` [1]_ and implemented in [2]_. It is important to note that to do this filtering, a time series is used and this one is built according to the data acquisition mode (DESCENDING, ASCENDING) and the polarization (VV, VH).
Then iota2 resamples the output of the speckle filter to a common time grid for the whole area. Finally, this time series will feed the classification system. If another sensor is used, the data will simply be concatenated.

We can summarise this workflow as follows:

.. figure:: ./Images/s1_workflow.png
    :scale: 50 %
    :align: center
    :alt: Sentinel-1 iota2 workflow

  
	
References
----------

.. _authors:

.. [1] `Quegan, S., & Yu, J. J. (2001). Filtering of multichannel sar images. IEEE Transactions on Geoscience and Remote Sensing, 39(11), 2373–2379. <http://dx.doi.org/10.1109/36.964973>`_
	   
.. [2] `https://framagit.org/iota2-project/otb-for-biomass/-/tree/memChain/MultitempFiltering <https://framagit.org/iota2-project/otb-for-biomass/-/tree/memChain/MultitempFiltering>`_
