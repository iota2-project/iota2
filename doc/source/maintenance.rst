=========================
Maintenance documentation
=========================

Iota2 is a complicated project, meaning its maintenance is also complicated. This document aims at
giving precise instructions on the different major maintenance tasks any developer could be faced
with. These tasks are:

- Releasing a new Iota2 version
- Using/modifying the CI/CD pipeline
- Building and uploading a conda package
- Updating the python version

New release
-----------

Making a new iota2 release from the develop branch is quite simple. It simply requires a few steps.
First, you need to be sure the code is safe to run. All tests need to successfully pass, both on a
local machine and from the CI pipeline. An additional test needs to be ran on a cluster. All the
required data is already situated in :code:`/work/THEIA/CES/oso/iota2_annual_test/` on TREX. You can
simply use :code:`./run_annual_iota_test.sh` to start it. You can provide a conda environment
to use with :code:`./run_annual_iota_test.sh /path/to/env`; if you don't, make sure the CI building
stage is not currently running (see CI doc for details todo!), as this script will clone the same
environment. You can also download this script :download:`here <../run_annual_iota_test.sh>`.

Once all these tests are validated, only a few easy steps remain:

1. Change the iota2 version in the `conda recipe <https://framagit.org/iota2-project/iota2/-/blob/develop/install/recipes/iota2/meta.yaml>`_

2. Merge the `develop` branch into the `master` branch

3. Add a git tag (you can use either the git CLI or the gitlab web interface,
   see https://stackoverflow.com/a/18223354/12068200)

4. Build and deploy the new conda package (see the conda package section)


CI and CD pipelines
-------------------

Before reading, if you are not used to CI/CD workflows, please check:

- Generic informations on CI/CD: https://about.gitlab.com/topics/ci-cd/
- How to work with CI/CD on gitlab: https://docs.gitlab.com/ee/ci/

The main Iota2 repository is hosted on framagit: https://framagit.org/iota2-project/iota2, but a
mirror repository is hosted on the CNES gitlab's instance: https://gitlab.cnes.fr/cesbio/iota2 (you
need a CNES account to access it).

Runners
+++++++

The CNES gitlab offers three solutions to host and run CI and CD pipelines:

- Shared runners: easy to use but limited ressources
- Run a dedicated virtual machine: a lot of ressources but require more maintenance
- Use custom made runners: quite easy to use and very customizable but requires ressources and,
  depending on the architecture, may not offer 24/24 availability (more details later)


The shared runners don't offer sufficient ressources to run the iota2 tests and suing a dedicated
virtual machine would require more administrative and maintenance work, and wouldn't be as
accessible to new developers as other solutions. So, the third option was chosen: the custom runner
would be started by a slurm job on the TREX infrastructure. Except from the ressources allowance,
the job is only one line: :code:`/path/to/runner/gitlab-runner run &` (see
https://confluence.cnes.fr/pages/viewpage.action?pageId=209524333 for instructions on how to deploy
a runner on the TREX infrastructure).

The main issue with this method is that a user/developer would have to start the runner manually
before starting a CI or CD pipeline, which defeats the purpose of such a workflow (it needs to be
fully automatic and autonomous). However, a simple trick can be used to overcome this issue. A slurm
job can detect when it's about to reach its walltime; when this happens, instead of letting the job
being killed, a function catches the signal and restarts a new job (this job only requires one CPU
and very low RAM). This way, a job will always be running. We can now link this job to a gitlab
runner: when a CI request is picked up, a slurm job with more ressources is started for all stages
required. Hence, each pipeline starts by "waking up" the real runner.

To this end, we have created the Slurm job script :code:`/work/THEIA/CES/oso/iota2_runner/sentinel/sentinel.slurm`,
which can be executed as follows:

.. code-block:: console

    sbatch sentinel.slurm /work/THEIA/CES/oso/iota2_runner/sentinel/sentinel.slurm

This job runs continuously on the cluster and its purpose is to start a new project runner with increased resources to meet the needs of iota2.

CI pipeline
+++++++++++

The main job of the CI is checking if code modifications on a given branch are acceptable, by
running tests and linters. As explained before, the first step of the pipeline is starting the real
runner: this is the `sentinel` stage, which contains only one step: `wake-up-runner`. When the
actual runner is up, the first thing to do is download and install iota2. This is the `init` stage,
which contains the `build-end` step. Then, the `quality` stage is started. It contains three steps:

- `formatter`: checks the code formatting using black
- `linter`: Checks the code quality with flake8
- `pytest`: Run all iota2 tests


CD pipeline
+++++++++++

The CD pipeline is a more complete process. Its job is to build the current develop conda package,
test it (install it and check its quality and validity) and upload it if everything passed. The
building step is pretty straight forward: it simply builds the conda package (see conda package doc
!todo) and stores it in a `/work/THEIA/CES/oso/CD/latest` directory. Then, the same steps as the CI are ran: installation,
formatting, linting, testing. If everything is correct, the package is copied to a `/work/THEIA/CES/oso/CD/stable`
directory. Finally, it is uploaded to the conda chanel, using a token stored as a CI variable.


Conda packaging
---------------

Iota2 heavily relies on OTB: a C++ library for satellite images processing. No pypi package is
available for OTB, which means it has to be compiled from the source code, hence the need for
`conda` (instead of `pip`). The process is, in theory, easy to follow:

1. Create a conda environment to build the package, using the right python version
2. Install `conda-build` in this environment (and `anaconda-client` if you want to upload the conda
   package at the end of the process)
3. Build Iota2
4. (optional) Upload the package to Anaconda. Check out the
   `official anaconda documentation <https://docs.anaconda.com/anacondaorg/user-guide/packages/conda-packages/>`_
   for more details on creating channels, uploading packages etc...


The building process can take several minutes: OTB and some of its dependencies (itk, muparserx,
shark ...) need to be compiled from source, which can take a long time. All iota2 dependencies that
need to be manually compiled are listed in `iota2/install/recipes`: each folder is a package to be
built/compiled.

.. note::
    You can modify your `.condarc` file (which you can find using `conda info`) to use more CPUs to
    speed up the process. Add a line with `default_threads: 5`, or any number to use as many threads
    as indicated.

Here is the code to do this:

.. code-block:: bash

    # 1. create a build environment
    conda create -n build_env python=3.11  # use the iota2 official python version
    conda activate build_env
    # 2. install the required conda packages
    conda install mamba  # optional, mamba is faster than conda
    mamba install conda-build
    mamba install anaconda-client  # optional, required to uploading the package
    # 3. build the iota2 package
    cd /path/to/iota2/install/recipes
    conda build iota2  # this will take a while
    # 4. upload the package on the iota2 channel
    anaconda upload /path/to/package -u iota2

.. note::
    To upload to the iota2 channel, the current Conda account must be added as a member of the organization.
    (https://docs.anaconda.com/psm-cloud/organizations/#inviting-members-to-your-organization)

Updating python version
-----------------------

Updating the python version may sound easy enough, but this process is more complicated than it
seems. Before this, make sure you have read the `Conda packaging`_ section.

Ideally, the only thing to do is update the python version in the iota2 recipe
(`iota-2/install/recipes/iota2/meta.yaml`) and `conda-build` the new package. But, as explained in
the conda section, iota2 relies on C++ and many python libraries, each requiring specific versions
of dependencies. This can (and will) lead to many dependencies compatibility issues. There is no
consistant method to solve this problem, but some tricks can be used to solve some problems or track
down the exact dependency issue.


Conda error log
+++++++++++++++

When using a recent version of conda, the error log should point out the versions conflicts. This
can help identifying where to start. If you haven't changed anything yet, it should mainly highlight
which library can/should be updated (and what version to use to be compatible with the target python
version). Two cases are possible:

1. A library (or more) is not compatible with a newer python version
2. A library (or more) needs to be updated


Strict incompatibility
++++++++++++++++++++++

In the first case, make sure to check the official library's documentation. If the library is not
compatible with the target python version, not much can be done. Follow the updates on the library
and regularly check for compatibility, maybe get in contact with the developer team. Alternatively,
if you really need to upgrade, try to identify if the library can be replaced by another one (or
removed completely).


Updating libraries
++++++++++++++++++

In the iota2 conda recipe, some libraries are pinned to specific versions, to help the conda solver.
Some of these libraries will probably need to be updated. A first easy step is to use the
`conda forge pinning feedstock <https://github.com/conda-forge/conda-forge-pinning-feedstock/blob/main/recipe/conda_build_config.yaml>`_
to update the `iota2/install/recipes/conda_build_config.yaml` file. This github repository stores
updated compatible versions of libraries. This will greatly facilitate the debugging process. The
remaining incompatibilities can be of two types:

1. The library is a "usual" dependency (numpy, matplotlib, pandas ...)
2. The library is one that needs to be manually compiled (one of the folders in `iota2/install/recipes`)


If the library is a usual one, the simplest way to deal with it is again use an official feedstock.
Similar to the one used earlier, a feedstock is basically the official recipe of a given version of
a library hosted on anaconda. Sometimes, there might be more than simply the `meta.yaml` file (for
example, the `libitk` library requires .cmake and bash files for compiling C++ code and advanced
compiling operations). These feedstocks are all hosted on https://github.com/conda-forge and can
easily be found. All of them are listed here: https://github.com/conda-forge/feedstocks or you can
simply search for `package_name feedstock` on your favorite search engine. For example, the `libitk`
feedstock can be found here: https://github.com/conda-forge/libitk-feedstock. For each package,
find the right feedstock, add it/update it in the `iota2`'s recipes folder and try building again.


.. note::
    You can also try removing completely a library's recipe folder. It could have been added because
    the iota2 installation required a specific version of the library not easily available, but this
    may not be the case anymore. Conversely, you might need to add a new specific recipe to iota2.

This workflow can be synthesised with the following flow chart:

.. figure:: ./Images/flowchart_conda.png
    :scale: 80 %
    :align: center
    :alt: flowchart_conda.png

    flowchart_conda.png


However, as stated at the beginning, there is no method that perfectly works every time. This
process is complicated and may require a lot of research and trial and error. This complexity
partially comes from conda: some alternatives are considered but aren't mature enough yet.
For example, `Pixi <https://pixi.sh/latest/>`_, a new package manager based on conda, looks like a
promising alternative, but integrates no equivalent to `conda build` at the moment of writing this
documentation.

Generate html documentation
---------------------------

The iota2 documentation is written in the reStructuredText (rst) format, which allows generating the HTML equivalent using Sphinx and sphinx-multiversion.
Recently, compatibility issues have been observed with different versions of Sphinx and sphinx-multiversion.
We therefore recommend creating a dedicated environment for generating this documentation by following these steps:

.. code-block:: bash

    # 1. create a build environment
    conda create -n iota2_doc python=3.12
    # 2. activate iota2 environment
    conda activate iota2_doc
    # 3. install required libraries
    conda install sphinx=5.1.1 sphinx-multiversion=0.2.4 sphinx_rtd_theme=1.0.0 numpydoc sphinx_autodoc_typehints
    # 4. Go to the directory containing the documentation
    cd iota2/doc
    # To generate the documentation on a specific branch 'branch_name'
    sphinx-multiversion source build/html -D 'smv_branch_whitelist=branch_name' -D 'smv_released_pattern=.*branch_name.*' -D 'svm_lastest_version=branch_name'

.. note::
    The procedure for updating the official documentation is available at
    https://framagit.org/iota2-project/iota2-devtools/-/blob/create_cron_night/crontab/build_doc.sh