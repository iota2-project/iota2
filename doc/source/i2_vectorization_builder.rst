i2_vectorization
################

.. _i2_vectorization.builders:

builders
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_vectorization.builders.builders_class_name:
      * - :ref:`builders_class_name <desc_i2_vectorization.builders.builders_class_name>`
        - ['i2_classification']
        - The name of the class defining the builder
        - list
        - False
        - :ref:`builders_class_name <desc_i2_vectorization.builders.builders_class_name>`


          .. _i2_vectorization.builders.builders_paths:
      * - :ref:`builders_paths <desc_i2_vectorization.builders.builders_paths>`
        - /path/to/iota2/sources
        - The path to user builders
        - list
        - False
        - :ref:`builders_paths <desc_i2_vectorization.builders.builders_paths>`





Notes
=====

.. _desc_i2_vectorization.builders.builders_class_name:

:ref:`builders_class_name <i2_vectorization.builders.builders_class_name>`
--------------------------------------------------------------------------
Available builders are : 'i2_classification', 'i2_features_map', 'i2_obia' and 'i2_vectorization'

.. _desc_i2_vectorization.builders.builders_paths:

:ref:`builders_paths <i2_vectorization.builders.builders_paths>`
----------------------------------------------------------------
If not indicated, the iota2 source directory is used: */iota2/sequence_builders/

.. _i2_vectorization.chain:

chain
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_vectorization.chain.output_path:
      * - :ref:`output_path <desc_i2_vectorization.chain.output_path>`
        - None
        - Absolute path to the output directory.
        - str
        - True
        - :ref:`output_path <desc_i2_vectorization.chain.output_path>`


          .. _i2_vectorization.chain.proj:
      * - proj
        - EPSG:2154
        - The projection wanted. Format EPSG:XXXX is mandatory
        - str
        - False
        - proj





Notes
=====

.. _desc_i2_vectorization.chain.output_path:

:ref:`output_path <i2_vectorization.chain.output_path>`
-------------------------------------------------------
Absolute path to the output directory.It is recommended to have one directory per run of the chain

.. _i2_vectorization.simplification:

simplification
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_vectorization.simplification.angle:
      * - angle
        - True
        - If True, smoothing corners of pixels (45°)
        - bool
        - False
        - angle


          .. _i2_vectorization.simplification.bingdal:
      * - :ref:`bingdal <desc_i2_vectorization.simplification.bingdal>`
        - None
        - path to GDAL binaries
        - str
        - False
        - :ref:`bingdal <desc_i2_vectorization.simplification.bingdal>`


          .. _i2_vectorization.simplification.blocksize:
      * - :ref:`blocksize <desc_i2_vectorization.simplification.blocksize>`
        - 2000
        - block size to split raster to prevent Numpy memory error
        - int
        - False
        - :ref:`blocksize <desc_i2_vectorization.simplification.blocksize>`


          .. _i2_vectorization.simplification.chunk:
      * - :ref:`chunk <desc_i2_vectorization.simplification.chunk>`
        - 10
        - Number of chunks for statistics computing
        - int
        - False
        - :ref:`chunk <desc_i2_vectorization.simplification.chunk>`


          .. _i2_vectorization.simplification.classification:
      * - :ref:`classification <desc_i2_vectorization.simplification.classification>`
        - None
        - Input raster of classification
        - str
        - True
        - :ref:`classification <desc_i2_vectorization.simplification.classification>`


          .. _i2_vectorization.simplification.clipfield:
      * - :ref:`clipfield <desc_i2_vectorization.simplification.clipfield>`
        - None
        - field to identify distinct areas
        - str
        - False
        - :ref:`clipfield <desc_i2_vectorization.simplification.clipfield>`


          .. _i2_vectorization.simplification.clipfile:
      * - :ref:`clipfile <desc_i2_vectorization.simplification.clipfile>`
        - None
        - vector-based file to clip output vector-based classification
        - str
        - False
        - :ref:`clipfile <desc_i2_vectorization.simplification.clipfile>`


          .. _i2_vectorization.simplification.clipvalue:
      * - :ref:`clipvalue <desc_i2_vectorization.simplification.clipvalue>`
        - None
        - value of field which identify distinct areas
        - int
        - False
        - :ref:`clipvalue <desc_i2_vectorization.simplification.clipvalue>`


          .. _i2_vectorization.simplification.confidence:
      * - :ref:`confidence <desc_i2_vectorization.simplification.confidence>`
        - None
        - Input raster of confidence
        - str
        - False
        - :ref:`confidence <desc_i2_vectorization.simplification.confidence>`


          .. _i2_vectorization.simplification.douglas:
      * - douglas
        - 10
        - Douglas-Peucker tolerance for vector-based generalization
        - int
        - False
        - douglas


          .. _i2_vectorization.simplification.dozip:
      * - dozip
        - True
        - Zip output vector-based classification (OSO-like production)
        - bool
        - False
        - dozip


          .. _i2_vectorization.simplification.grasslib:
      * - :ref:`grasslib <desc_i2_vectorization.simplification.grasslib>`
        - None
        - path to grasslib
        - str
        - False
        - :ref:`grasslib <desc_i2_vectorization.simplification.grasslib>`


          .. _i2_vectorization.simplification.gridsize:
      * - :ref:`gridsize <desc_i2_vectorization.simplification.gridsize>`
        - None
        - Number of lines and columns of serialization process
        - int
        - False
        - :ref:`gridsize <desc_i2_vectorization.simplification.gridsize>`


          .. _i2_vectorization.simplification.hermite:
      * - hermite
        - 10
        - Hermite Interpolation threshold for vector-based smoothing
        - int
        - False
        - hermite


          .. _i2_vectorization.simplification.inland:
      * - :ref:`inland <desc_i2_vectorization.simplification.inland>`
        - None
        - Inland water limit shapefile
        - str
        - False
        - :ref:`inland <desc_i2_vectorization.simplification.inland>`


          .. _i2_vectorization.simplification.lcfield:
      * - lcfield
        - Class
        - Name of the field to store landcover class in vector-based classification
        - str
        - False
        - lcfield


          .. _i2_vectorization.simplification.lib64bit:
      * - :ref:`lib64bit <desc_i2_vectorization.simplification.lib64bit>`
        - None
        - Path of BandMath and Concatenate OTB executables returning 64-bits float pixel values
        - str
        - False
        - :ref:`lib64bit <desc_i2_vectorization.simplification.lib64bit>`


          .. _i2_vectorization.simplification.mmu:
      * - mmu
        - 1000
        - MMU of output vector-based classification (projection unit),(Default : 0.1 ha)
        - int
        - False
        - mmu


          .. _i2_vectorization.simplification.nomenclature:
      * - :ref:`nomenclature <desc_i2_vectorization.simplification.nomenclature>`
        - None
        - configuration file which describe nomenclature
        - str
        - False
        - :ref:`nomenclature <desc_i2_vectorization.simplification.nomenclature>`


          .. _i2_vectorization.simplification.outprefix:
      * - :ref:`outprefix <desc_i2_vectorization.simplification.outprefix>`
        - dept
        - Prefix to use for naming of vector-based classifications
        - str
        - False
        - :ref:`outprefix <desc_i2_vectorization.simplification.outprefix>`


          .. _i2_vectorization.simplification.rssize:
      * - :ref:`rssize <desc_i2_vectorization.simplification.rssize>`
        - 20
        - Resampling size of input classification raster (projection unit)
        - int
        - False
        - :ref:`rssize <desc_i2_vectorization.simplification.rssize>`


          .. _i2_vectorization.simplification.seed:
      * - :ref:`seed <desc_i2_vectorization.simplification.seed>`
        - 1
        - Seed of input raster classification
        - int
        - False
        - :ref:`seed <desc_i2_vectorization.simplification.seed>`


          .. _i2_vectorization.simplification.statslist:
      * - :ref:`statslist <desc_i2_vectorization.simplification.statslist>`
        - {1: 'rate', 2: 'statsmaj', 3: 'statsmaj'}
        - dictionnary of requested landcover statistics
        - dict
        - False
        - :ref:`statslist <desc_i2_vectorization.simplification.statslist>`


          .. _i2_vectorization.simplification.systemcall:
      * - systemcall
        - False
        - If True, use yours gdal lib (cf. bingdal)
        - bool
        - False
        - systemcall


          .. _i2_vectorization.simplification.umc1:
      * - :ref:`umc1 <desc_i2_vectorization.simplification.umc1>`
        - None
        - MMU for first regularization
        - int
        - False
        - :ref:`umc1 <desc_i2_vectorization.simplification.umc1>`


          .. _i2_vectorization.simplification.umc2:
      * - :ref:`umc2 <desc_i2_vectorization.simplification.umc2>`
        - None
        - MMU for second regularization
        - int
        - False
        - :ref:`umc2 <desc_i2_vectorization.simplification.umc2>`


          .. _i2_vectorization.simplification.validity:
      * - :ref:`validity <desc_i2_vectorization.simplification.validity>`
        - None
        - Input raster of validity
        - str
        - False
        - :ref:`validity <desc_i2_vectorization.simplification.validity>`


          .. _i2_vectorization.simplification.vectorize_fusion_of_classifications:
      * - :ref:`vectorize_fusion_of_classifications <desc_i2_vectorization.simplification.vectorize_fusion_of_classifications>`
        - False
        - flag to inform iota2 to vectorize the fusion of classifications
        - bool
        - False
        - :ref:`vectorize_fusion_of_classifications <desc_i2_vectorization.simplification.vectorize_fusion_of_classifications>`





Notes
=====

.. _desc_i2_vectorization.simplification.bingdal:

:ref:`bingdal <i2_vectorization.simplification.bingdal>`
--------------------------------------------------------
Some GDAL lib versions (automatically set up with iota²) are not efficient to handle topology errors, use yours !

.. _desc_i2_vectorization.simplification.blocksize:

:ref:`blocksize <i2_vectorization.simplification.blocksize>`
------------------------------------------------------------
Numpy memory error may occur for large areas during serialization process. Split in sub-rasters prevents memory error

.. _desc_i2_vectorization.simplification.chunk:

:ref:`chunk <i2_vectorization.simplification.chunk>`
----------------------------------------------------
Number of chunks (groups of vector-based features) for parallel computing landcover statistics

.. _desc_i2_vectorization.simplification.classification:

:ref:`classification <i2_vectorization.simplification.classification>`
----------------------------------------------------------------------
This parameter is automatically set if the configuration file use the classification and vectorization builders

.. _desc_i2_vectorization.simplification.clipfield:

:ref:`clipfield <i2_vectorization.simplification.clipfield>`
------------------------------------------------------------
field to identify distinct geographical/administrativeareas (cf. "clipfile" parameter)

.. _desc_i2_vectorization.simplification.clipfile:

:ref:`clipfile <i2_vectorization.simplification.clipfile>`
----------------------------------------------------------
vector-based file can contain more than one feature (geographical/administrative areas). An output vector-based classification is produced for each feature (cf. 'clipfield' parameter).

.. _desc_i2_vectorization.simplification.clipvalue:

:ref:`clipvalue <i2_vectorization.simplification.clipvalue>`
------------------------------------------------------------
output vector-based classification is only produced on the specific area (clipfield=clipvalue in clipfile) (cf. 'clipfield' parameter). If None, all areas are produced.

.. _desc_i2_vectorization.simplification.confidence:

:ref:`confidence <i2_vectorization.simplification.confidence>`
--------------------------------------------------------------
This parameter is automatically set if the configuration file use the classification and vectorization builders

.. _desc_i2_vectorization.simplification.grasslib:

:ref:`grasslib <i2_vectorization.simplification.grasslib>`
----------------------------------------------------------
Some functions of GRASS GIS software are used to vectorize, simplify and smooth vector layer. This path corresponds to GRASS install folder

.. _desc_i2_vectorization.simplification.gridsize:

:ref:`gridsize <i2_vectorization.simplification.gridsize>`
----------------------------------------------------------
This parameter is useful only for large areas for which vectorization process can not be executed (memory limitation). By 'serialization', we mean parallel vectorization processes. If not None, regularized classification raster is splitted in gridsize x gridsize rasters

.. _desc_i2_vectorization.simplification.inland:

:ref:`inland <i2_vectorization.simplification.inland>`
------------------------------------------------------
to vectorize only inland waters, and not unnecessary sea water areas

.. _desc_i2_vectorization.simplification.lib64bit:

:ref:`lib64bit <i2_vectorization.simplification.lib64bit>`
----------------------------------------------------------
Band math and concatenate OTB executables with 64 bits capabilities (only for large areas where clumps number > 2²³ bits for mantisse)

.. _desc_i2_vectorization.simplification.nomenclature:

:ref:`nomenclature <i2_vectorization.simplification.nomenclature>`
------------------------------------------------------------------
This configuration file includes code, color, description and vector field alias of each class
Classes:
{
        Level1:
        {
                "Urbain":
                {
                code:100
                alias:"Urbain"
                color:"#b106b1"
                }
                ...
        }
        Level2:
        {
               "Urbain dense":
               {
               code:1
               alias:"UrbainDens"
               color:"#ff00ff"
               parent:100
               }
               ...
        }
}


.. _desc_i2_vectorization.simplification.outprefix:

:ref:`outprefix <i2_vectorization.simplification.outprefix>`
------------------------------------------------------------
Naming of vector-based classifications is as following : prefix_clipvalue

.. _desc_i2_vectorization.simplification.rssize:

:ref:`rssize <i2_vectorization.simplification.rssize>`
------------------------------------------------------
OSO-like vectorization requires a resampling step in order to regularize and decrease raster polygons number, If None, classification is not resampled

.. _desc_i2_vectorization.simplification.seed:

:ref:`seed <i2_vectorization.simplification.seed>`
--------------------------------------------------
This parameter is usefull to vectorize one specific output classification seed

.. _desc_i2_vectorization.simplification.statslist:

:ref:`statslist <i2_vectorization.simplification.statslist>`
------------------------------------------------------------
Different landcover statistics can be computed for vector-based classification file. A python-like dictionnary must be provided. This is the OSO-like statistics : {1: "rate", 2: "statsmaj", 3: "statsmaj"}1: "rate" :      rates of classification classes are computed      for each polygon
2: "statsmaj" :      descriptive stats of classifier confidence are computed      for each polygon by using only majority class pixels
3: "statsmaj" :      descriptive stats of sensor validity are computed      for each polygon by using only majority class pixels

list of available statistics : 
    - stats : mean_b, std_b, max_b, min_b
    - statsmaj : meanmaj, stdmaj, maxmaj, minmaj of maj. class
    - rate : rate of each pixel value (classe names)
    - stats_cl : mean_cl, std_cl, max_cl, min_cl of one class


.. _desc_i2_vectorization.simplification.umc1:

:ref:`umc1 <i2_vectorization.simplification.umc1>`
--------------------------------------------------
It is an interface of parameter '-st' of gdal_sieve.py function. If None, classification is not regularized

.. _desc_i2_vectorization.simplification.umc2:

:ref:`umc2 <i2_vectorization.simplification.umc2>`
--------------------------------------------------
OSO-like vectorization process requires 2 successive regularization, if you need a single regularization, let this parameter to None

.. _desc_i2_vectorization.simplification.validity:

:ref:`validity <i2_vectorization.simplification.validity>`
----------------------------------------------------------
This parameter is automatically set if the configuration file use the classification and vectorization builders

.. _desc_i2_vectorization.simplification.vectorize_fusion_of_classifications:

:ref:`vectorize_fusion_of_classifications <i2_vectorization.simplification.vectorize_fusion_of_classifications>`
----------------------------------------------------------------------------------------------------------------
This flag is only useful if the vectorization is chained with classification workflow

.. _i2_vectorization.task_retry_limits:

task_retry_limits
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_vectorization.task_retry_limits.allowed_retry:
      * - allowed_retry
        - 0
        - Allow dask to retry a failed job N times.
        - int
        - False
        - allowed_retry


          .. _i2_vectorization.task_retry_limits.maximum_cpu:
      * - :ref:`maximum_cpu <desc_i2_vectorization.task_retry_limits.maximum_cpu>`
        - 4
        - The maximum number of CPU available
        - int
        - False
        - :ref:`maximum_cpu <desc_i2_vectorization.task_retry_limits.maximum_cpu>`


          .. _i2_vectorization.task_retry_limits.maximum_ram:
      * - :ref:`maximum_ram <desc_i2_vectorization.task_retry_limits.maximum_ram>`
        - 16
        - The maximum amount of RAM available. (gB)
        - int
        - False
        - :ref:`maximum_ram <desc_i2_vectorization.task_retry_limits.maximum_ram>`





Notes
=====

.. _desc_i2_vectorization.task_retry_limits.maximum_cpu:

:ref:`maximum_cpu <i2_vectorization.task_retry_limits.maximum_cpu>`
-------------------------------------------------------------------
the amount of cpu will be doubled if the task is killed due to ram overconsumption until maximum_cpu or allowed_retry are reach

.. _desc_i2_vectorization.task_retry_limits.maximum_ram:

:ref:`maximum_ram <i2_vectorization.task_retry_limits.maximum_ram>`
-------------------------------------------------------------------
the amount of RAM will be doubled if the task is killed due to ram overconsumption until maximum_ram or allowed_retry are reach

