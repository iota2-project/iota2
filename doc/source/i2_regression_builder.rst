I2Regression
############

.. _I2Regression.Landsat5_old:

Landsat5_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.Landsat5_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Regression.Landsat5_old.end_date:
      * - :ref:`end_date <desc_I2Regression.Landsat5_old.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Regression.Landsat5_old.end_date>`


          .. _I2Regression.Landsat5_old.keep_bands:
      * - :ref:`keep_bands <desc_I2Regression.Landsat5_old.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Regression.Landsat5_old.keep_bands>`


          .. _I2Regression.Landsat5_old.start_date:
      * - :ref:`start_date <desc_I2Regression.Landsat5_old.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Regression.Landsat5_old.start_date>`


          .. _I2Regression.Landsat5_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Regression.Landsat5_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Regression.Landsat5_old.end_date:

:ref:`end_date <I2Regression.Landsat5_old.end_date>`
----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Regression.Landsat5_old.keep_bands:

:ref:`keep_bands <I2Regression.Landsat5_old.keep_bands>`
--------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Regression.Landsat5_old.start_date:

:ref:`start_date <I2Regression.Landsat5_old.start_date>`
--------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Regression.Landsat8:

Landsat8
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.Landsat8.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Regression.Landsat8.end_date:
      * - :ref:`end_date <desc_I2Regression.Landsat8.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Regression.Landsat8.end_date>`


          .. _I2Regression.Landsat8.keep_bands:
      * - :ref:`keep_bands <desc_I2Regression.Landsat8.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Regression.Landsat8.keep_bands>`


          .. _I2Regression.Landsat8.start_date:
      * - :ref:`start_date <desc_I2Regression.Landsat8.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Regression.Landsat8.start_date>`


          .. _I2Regression.Landsat8.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Regression.Landsat8.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Regression.Landsat8.end_date:

:ref:`end_date <I2Regression.Landsat8.end_date>`
------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Regression.Landsat8.keep_bands:

:ref:`keep_bands <I2Regression.Landsat8.keep_bands>`
----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Regression.Landsat8.start_date:

:ref:`start_date <I2Regression.Landsat8.start_date>`
----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Regression.Landsat8_old:

Landsat8_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.Landsat8_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Regression.Landsat8_old.end_date:
      * - :ref:`end_date <desc_I2Regression.Landsat8_old.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Regression.Landsat8_old.end_date>`


          .. _I2Regression.Landsat8_old.keep_bands:
      * - :ref:`keep_bands <desc_I2Regression.Landsat8_old.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Regression.Landsat8_old.keep_bands>`


          .. _I2Regression.Landsat8_old.start_date:
      * - :ref:`start_date <desc_I2Regression.Landsat8_old.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Regression.Landsat8_old.start_date>`


          .. _I2Regression.Landsat8_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Regression.Landsat8_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Regression.Landsat8_old.end_date:

:ref:`end_date <I2Regression.Landsat8_old.end_date>`
----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Regression.Landsat8_old.keep_bands:

:ref:`keep_bands <I2Regression.Landsat8_old.keep_bands>`
--------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Regression.Landsat8_old.start_date:

:ref:`start_date <I2Regression.Landsat8_old.start_date>`
--------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Regression.Sentinel_2:

Sentinel_2
**********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.Sentinel_2.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Regression.Sentinel_2.end_date:
      * - :ref:`end_date <desc_I2Regression.Sentinel_2.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Regression.Sentinel_2.end_date>`


          .. _I2Regression.Sentinel_2.keep_bands:
      * - :ref:`keep_bands <desc_I2Regression.Sentinel_2.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Regression.Sentinel_2.keep_bands>`


          .. _I2Regression.Sentinel_2.start_date:
      * - :ref:`start_date <desc_I2Regression.Sentinel_2.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Regression.Sentinel_2.start_date>`


          .. _I2Regression.Sentinel_2.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Regression.Sentinel_2.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Regression.Sentinel_2.end_date:

:ref:`end_date <I2Regression.Sentinel_2.end_date>`
--------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Regression.Sentinel_2.keep_bands:

:ref:`keep_bands <I2Regression.Sentinel_2.keep_bands>`
------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Regression.Sentinel_2.start_date:

:ref:`start_date <I2Regression.Sentinel_2.start_date>`
------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Regression.Sentinel_2_L3A:

Sentinel_2_L3A
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.Sentinel_2_L3A.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Regression.Sentinel_2_L3A.end_date:
      * - :ref:`end_date <desc_I2Regression.Sentinel_2_L3A.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Regression.Sentinel_2_L3A.end_date>`


          .. _I2Regression.Sentinel_2_L3A.keep_bands:
      * - :ref:`keep_bands <desc_I2Regression.Sentinel_2_L3A.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Regression.Sentinel_2_L3A.keep_bands>`


          .. _I2Regression.Sentinel_2_L3A.start_date:
      * - :ref:`start_date <desc_I2Regression.Sentinel_2_L3A.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Regression.Sentinel_2_L3A.start_date>`


          .. _I2Regression.Sentinel_2_L3A.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Regression.Sentinel_2_L3A.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Regression.Sentinel_2_L3A.end_date:

:ref:`end_date <I2Regression.Sentinel_2_L3A.end_date>`
------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Regression.Sentinel_2_L3A.keep_bands:

:ref:`keep_bands <I2Regression.Sentinel_2_L3A.keep_bands>`
----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Regression.Sentinel_2_L3A.start_date:

:ref:`start_date <I2Regression.Sentinel_2_L3A.start_date>`
----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Regression.Sentinel_2_S2C:

Sentinel_2_S2C
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.Sentinel_2_S2C.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Regression.Sentinel_2_S2C.end_date:
      * - :ref:`end_date <desc_I2Regression.Sentinel_2_S2C.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Regression.Sentinel_2_S2C.end_date>`


          .. _I2Regression.Sentinel_2_S2C.keep_bands:
      * - :ref:`keep_bands <desc_I2Regression.Sentinel_2_S2C.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Regression.Sentinel_2_S2C.keep_bands>`


          .. _I2Regression.Sentinel_2_S2C.start_date:
      * - :ref:`start_date <desc_I2Regression.Sentinel_2_S2C.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Regression.Sentinel_2_S2C.start_date>`


          .. _I2Regression.Sentinel_2_S2C.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Regression.Sentinel_2_S2C.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Regression.Sentinel_2_S2C.end_date:

:ref:`end_date <I2Regression.Sentinel_2_S2C.end_date>`
------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Regression.Sentinel_2_S2C.keep_bands:

:ref:`keep_bands <I2Regression.Sentinel_2_S2C.keep_bands>`
----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Regression.Sentinel_2_S2C.start_date:

:ref:`start_date <I2Regression.Sentinel_2_S2C.start_date>`
----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Regression.arg_train:

arg_train
*********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.arg_train.deep_learning_parameters:
      * - deep_learning_parameters
        - {}
        - deep learning parameter description is available :doc:`here <deep_learning>`
        - dict
        - False
        - deep_learning_parameters


          .. _I2Regression.arg_train.features:
      * - :ref:`features <desc_I2Regression.arg_train.features>`
        - ['NDVI', 'NDWI', 'Brightness']
        - List of additional features computed
        - list
        - False
        - :ref:`features <desc_I2Regression.arg_train.features>`


          .. _I2Regression.arg_train.learning_samples_extension:
      * - :ref:`learning_samples_extension <desc_I2Regression.arg_train.learning_samples_extension>`
        - sqlite
        - learning samples file extension, possible values are 'sqlite' and 'csv'
        - str
        - False
        - :ref:`learning_samples_extension <desc_I2Regression.arg_train.learning_samples_extension>`


          .. _I2Regression.arg_train.random_seed:
      * - :ref:`random_seed <desc_I2Regression.arg_train.random_seed>`
        - None
        - Fix the random seed for random split of reference data
        - int
        - False
        - :ref:`random_seed <desc_I2Regression.arg_train.random_seed>`


          .. _I2Regression.arg_train.ratio:
      * - ratio
        - 0.5
        - Should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the train split.
        - float
        - False
        - ratio


          .. _I2Regression.arg_train.runs:
      * - :ref:`runs <desc_I2Regression.arg_train.runs>`
        - 1
        - Number of independent runs processed
        - int
        - False
        - :ref:`runs <desc_I2Regression.arg_train.runs>`


          .. _I2Regression.arg_train.sample_augmentation:
      * - :ref:`sample_augmentation <desc_I2Regression.arg_train.sample_augmentation>`
        - {'activate': False, 'bins': 10}
        - OTB parameters for sample augmentation
        - dict
        - False
        - :ref:`sample_augmentation <desc_I2Regression.arg_train.sample_augmentation>`


          .. _I2Regression.arg_train.sample_management:
      * - :ref:`sample_management <desc_I2Regression.arg_train.sample_management>`
        - None
        - Absolute path to a CSV file containing samples transfert strategies
        - str
        - False
        - :ref:`sample_management <desc_I2Regression.arg_train.sample_management>`


          .. _I2Regression.arg_train.sample_selection:
      * - :ref:`sample_selection <desc_I2Regression.arg_train.sample_selection>`
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - False
        - :ref:`sample_selection <desc_I2Regression.arg_train.sample_selection>`


          .. _I2Regression.arg_train.sample_validation:
      * - :ref:`sample_validation <desc_I2Regression.arg_train.sample_validation>`
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - False
        - :ref:`sample_validation <desc_I2Regression.arg_train.sample_validation>`


          .. _I2Regression.arg_train.sampling_validation:
      * - sampling_validation
        - False
        - Enable sampling validation
        - bool
        - False
        - sampling_validation





Notes
=====

.. _desc_I2Regression.arg_train.features:

:ref:`features <I2Regression.arg_train.features>`
-------------------------------------------------
This parameter enable the computation of the three indices if available for the sensor used.There is no choice for using only one of them

.. _desc_I2Regression.arg_train.learning_samples_extension:

:ref:`learning_samples_extension <I2Regression.arg_train.learning_samples_extension>`
-------------------------------------------------------------------------------------
Default value is 'sqlite' (faster), in case of the number of features is superior to 2000, as sqlite file doesn't accept more than 2000 columns, it should be turn to 'csv'.

.. _desc_I2Regression.arg_train.random_seed:

:ref:`random_seed <I2Regression.arg_train.random_seed>`
-------------------------------------------------------
Fix the random seed used for random split of reference data If set, the results must be the same for a given classifier

.. _desc_I2Regression.arg_train.runs:

:ref:`runs <I2Regression.arg_train.runs>`
-----------------------------------------
Number of independent runs processed. Each run has his own learning samples. Must be an integer greater than 0

.. _desc_I2Regression.arg_train.sample_augmentation:

:ref:`sample_augmentation <I2Regression.arg_train.sample_augmentation>`
-----------------------------------------------------------------------
In supervised classification the balance between class samples is important. There are any ways to manage class balancing in iota2, using :ref:`refSampleSelection` or the classifier's options to limit the number of samples by class.
An other approch is to generate synthetic samples. It is the purpose of this functionality, which is called 'sample augmentation'.

    .. code-block:: python

        {'activate':False}

Example
^^^^^^^

    .. code-block:: python

        sample_augmentation : {'target_models':['1', '2'],
                              'strategy' : 'jitter',
                              'strategy.jitter.stdfactor' : 10,
                              'strategy.smote.neighbors'  : 5,
                              'samples.strategy' : 'balance',
                              'activate' : True
                              }


iota2 implements an interface to the OTB `SampleAugmentation <https://www.orfeo-toolbox.org/CookBook/Applications/app_SampleSelection.html>`_ application.
There are three methods to generate samples : replicate, jitter and smote.The documentation :doc:`here <sampleAugmentation_explain>` explains the difference between these approaches.

 ``samples.strategy`` specifies how many samples must be created.There are 3 different strategies:    - minNumber
        To set the minimum number of samples by class required
    - balance
        balance all classes with the same number of samples as the majority one
    - byClass
        augment only some of the classes
Parameters related to ``minNumber`` and ``byClass`` strategies are
    - samples.strategy.minNumber
        minimum number of samples
    - samples.strategy.byClass
        path to a CSV file containing in first column the class's label and 
        in the second column the minimum number of samples required.
In the above example, classes of models '1' and '2' will be augmented to the most represented class in the corresponding model using the jitter method.

To perform sample augmentation for regression problems, we return to a configuration similar to classification.
For that purpose we create fake classes using the ``bins`` parameter. The ``bins`` parameter can be an integer in which case the interval of values of the target output variable is divided into ``bins`` sub-intervals of equal width, and each sample gets a fake a class corresponding to the number of the interval in which its label fell.
``bins`` can also be a list of ascending values used as interval boundaries for assign the classes.

.. _desc_I2Regression.arg_train.sample_management:

:ref:`sample_management <I2Regression.arg_train.sample_management>`
-------------------------------------------------------------------
The CSV must contain a row per transfert
    .. code-block:: python

        >>> cat /absolute/path/myRules.csv
            1,2,4,2
Meaning :
        +--------+-------------+------------+----------+
        | source | destination | class name | quantity |
        +========+=============+============+==========+
        |   1    |      2      |      4     |     2    |
        +--------+-------------+------------+----------+

Currently, setting the 'random_seed' parameter has no effect on this workflow.

.. _desc_I2Regression.arg_train.sample_selection:

:ref:`sample_selection <I2Regression.arg_train.sample_selection>`
-----------------------------------------------------------------
This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
-------

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).

.. _desc_I2Regression.arg_train.sample_validation:

:ref:`sample_validation <I2Regression.arg_train.sample_validation>`
-------------------------------------------------------------------
This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
-------

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).

.. _I2Regression.builders:

builders
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.builders.builders_class_name:
      * - :ref:`builders_class_name <desc_I2Regression.builders.builders_class_name>`
        - ['I2Classification']
        - The name of the class defining the builder
        - list
        - False
        - :ref:`builders_class_name <desc_I2Regression.builders.builders_class_name>`


          .. _I2Regression.builders.builders_paths:
      * - :ref:`builders_paths <desc_I2Regression.builders.builders_paths>`
        - /path/to/iota2/sources
        - The path to user builders
        - str
        - False
        - :ref:`builders_paths <desc_I2Regression.builders.builders_paths>`





Notes
=====

.. _desc_I2Regression.builders.builders_class_name:

:ref:`builders_class_name <I2Regression.builders.builders_class_name>`
----------------------------------------------------------------------
Available builders are : 'I2Classification', 'I2FeaturesMap' and 'I2Obia'

.. _desc_I2Regression.builders.builders_paths:

:ref:`builders_paths <I2Regression.builders.builders_paths>`
------------------------------------------------------------
If not indicated, the iota2 source directory is used: */iota2/sequence_builders/

.. _I2Regression.chain:

chain
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.chain.check_inputs:
      * - :ref:`check_inputs <desc_I2Regression.chain.check_inputs>`
        - True
        - Enable the inputs verification
        - bool
        - False
        - :ref:`check_inputs <desc_I2Regression.chain.check_inputs>`


          .. _I2Regression.chain.cloud_threshold:
      * - :ref:`cloud_threshold <desc_I2Regression.chain.cloud_threshold>`
        - 0
        - Threshold to consider that a pixel is valid
        - int
        - False
        - :ref:`cloud_threshold <desc_I2Regression.chain.cloud_threshold>`


          .. _I2Regression.chain.data_field:
      * - :ref:`data_field <desc_I2Regression.chain.data_field>`
        - None
        - Field name indicating classes labels in `ground_thruth`
        - str
        - True
        - :ref:`data_field <desc_I2Regression.chain.data_field>`


          .. _I2Regression.chain.first_step:
      * - first_step
        - None
        - The step group name indicating where the chain start
        - str
        - False
        - first_step


          .. _I2Regression.chain.ground_truth:
      * - ground_truth
        - None
        - Absolute path to reference data
        - str
        - True
        - ground_truth


          .. _I2Regression.chain.last_step:
      * - last_step
        - None
        - The step group name indicating where the chain ends
        - str
        - False
        - last_step


          .. _I2Regression.chain.list_tile:
      * - list_tile
        - None
        - List of tile to process, separated by space
        - str
        - True
        - list_tile


          .. _I2Regression.chain.logger_level:
      * - logger_level
        - INFO
        - Set the logger level: NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
        - str
        - False
        - logger_level


          .. _I2Regression.chain.minimum_required_dates:
      * - minimum_required_dates
        - 2
        - required minimum number of available dates for each sensor
        - int
        - False
        - minimum_required_dates


          .. _I2Regression.chain.output_path:
      * - :ref:`output_path <desc_I2Regression.chain.output_path>`
        - None
        - Absolute path to the output directory
        - str
        - True
        - :ref:`output_path <desc_I2Regression.chain.output_path>`


          .. _I2Regression.chain.proj:
      * - proj
        - None
        - The projection wanted. Format EPSG:XXXX is mandatory
        - str
        - True
        - proj


          .. _I2Regression.chain.region_field:
      * - :ref:`region_field <desc_I2Regression.chain.region_field>`
        - region
        - The column name for region indicator in`region_path` file
        - str
        - False
        - :ref:`region_field <desc_I2Regression.chain.region_field>`


          .. _I2Regression.chain.region_path:
      * - region_path
        - None
        - Absolute path to a region vector file
        - str
        - False
        - region_path


          .. _I2Regression.chain.remove_output_path:
      * - :ref:`remove_output_path <desc_I2Regression.chain.remove_output_path>`
        - True
        - Before the launch of iota2, remove the content of `output_path`
        - bool
        - False
        - :ref:`remove_output_path <desc_I2Regression.chain.remove_output_path>`


          .. _I2Regression.chain.s1_path:
      * - s1_path
        - None
        - Absolute path to Sentinel-1 configuration file
        - str
        - False
        - s1_path


          .. _I2Regression.chain.s2_l3a_output_path:
      * - s2_l3a_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_l3a_output_path


          .. _I2Regression.chain.s2_l3a_path:
      * - s2_l3a_path
        - None
        - Absolute path to Sentinel-2 L3A images (THEIA format)
        - str
        - False
        - s2_l3a_path


          .. _I2Regression.chain.s2_output_path:
      * - s2_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_output_path


          .. _I2Regression.chain.s2_path:
      * - s2_path
        - None
        - Absolute path to Sentinel-2 images (THEIA format)
        - str
        - False
        - s2_path


          .. _I2Regression.chain.s2_s2c_output_path:
      * - s2_s2c_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_s2c_output_path


          .. _I2Regression.chain.s2_s2c_path:
      * - s2_s2c_path
        - None
        - Absolute path to Sentinel-2 images (Sen2Cor format)
        - str
        - False
        - s2_s2c_path


          .. _I2Regression.chain.spatial_resolution:
      * - :ref:`spatial_resolution <desc_I2Regression.chain.spatial_resolution>`
        - []
        - Output spatial resolution
        - list or scalar
        - False
        - :ref:`spatial_resolution <desc_I2Regression.chain.spatial_resolution>`





Notes
=====

.. _desc_I2Regression.chain.check_inputs:

:ref:`check_inputs <I2Regression.chain.check_inputs>`
-----------------------------------------------------
Enable the inputs verification. It can take a lot of time for large dataset. Check if region intersect reference data for instance

.. _desc_I2Regression.chain.cloud_threshold:

:ref:`cloud_threshold <I2Regression.chain.cloud_threshold>`
-----------------------------------------------------------
Indicates the threshold for a polygon to be used for learning. It use the validity count, which is incremented if a cloud, a cloud shadow or a saturated pixel is detected

.. _desc_I2Regression.chain.data_field:

:ref:`data_field <I2Regression.chain.data_field>`
-------------------------------------------------
All the labels values must be different to 0.
It is recommended to use a continuous range of values but it is not mandatory.
Keep in mind that the final product type is detected according to the maximum label value.
Try to keep values between 1 and 255 to avoid heavy products.

.. _desc_I2Regression.chain.output_path:

:ref:`output_path <I2Regression.chain.output_path>`
---------------------------------------------------
Absolute path to the output directory.It is recommended to have one directory per run of the chain

.. _desc_I2Regression.chain.region_field:

:ref:`region_field <I2Regression.chain.region_field>`
-----------------------------------------------------
This column in the database must contains string which can be converted into integers. For instance '1_2' does not match this condition.
It is mandatory that the region identifiers are > 0.

.. _desc_I2Regression.chain.remove_output_path:

:ref:`remove_output_path <I2Regression.chain.remove_output_path>`
-----------------------------------------------------------------
Before the launch of iota2, remove the content of `output_path`. Only if the `first_step` is `init` and the folder name is valid 

.. _desc_I2Regression.chain.spatial_resolution:

:ref:`spatial_resolution <I2Regression.chain.spatial_resolution>`
-----------------------------------------------------------------
The spatial resolution expected.It can be provided as integer or float,or as a list containing two values for non squared resolution

.. _I2Regression.external_features:

external_features
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.external_features.concat_mode:
      * - :ref:`concat_mode <desc_I2Regression.external_features.concat_mode>`
        - True
        - enable the use of all features
        - bool
        - False
        - :ref:`concat_mode <desc_I2Regression.external_features.concat_mode>`


          .. _I2Regression.external_features.exogeneous_data:
      * - :ref:`exogeneous_data <desc_I2Regression.external_features.exogeneous_data>`
        - None
        - Path to a Geotiff file containing additional data to be used in external features
        - str
        - False
        - :ref:`exogeneous_data <desc_I2Regression.external_features.exogeneous_data>`


          .. _I2Regression.external_features.external_features_flag:
      * - external_features_flag
        - False
        - enable the external features mode
        - bool
        - False
        - external_features_flag


          .. _I2Regression.external_features.functions:
      * - :ref:`functions <desc_I2Regression.external_features.functions>`
        - None
        - function list to be used to compute features
        - str/list
        - False
        - :ref:`functions <desc_I2Regression.external_features.functions>`


          .. _I2Regression.external_features.module:
      * - module
        - /path/to/iota2/sources
        - absolute path for user source code
        - str
        - False
        - module


          .. _I2Regression.external_features.no_data_value:
      * - no_data_value
        - -10000
        - value considered as no_data in features map mosaic ('I2FeaturesMap' builder name)
        - int
        - False
        - no_data_value


          .. _I2Regression.external_features.output_name:
      * - output_name
        - None
        - temporary chunks are written using this name as prefix
        - str
        - False
        - output_name





Notes
=====

.. _desc_I2Regression.external_features.concat_mode:

:ref:`concat_mode <I2Regression.external_features.concat_mode>`
---------------------------------------------------------------
if disabled, only external features are used in the whole processing

.. _desc_I2Regression.external_features.exogeneous_data:

:ref:`exogeneous_data <I2Regression.external_features.exogeneous_data>`
-----------------------------------------------------------------------
If the =exogeneous_data= contains '$TILE', it will be replaced by the tile name being processed.If you want to reproject your data on given tiles, you can use the =split_raster_into_tiles.py= command line tool.

Usage: =split_raster_into_tiles.py --help=.

.. _desc_I2Regression.external_features.functions:

:ref:`functions <I2Regression.external_features.functions>`
-----------------------------------------------------------
Can be a string of space-separated function namesCan be a list of either strings of function nameor lists of one function name and one argument mapping

.. _I2Regression.iota2_feature_extraction:

iota2_feature_extraction
************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.iota2_feature_extraction.acor_feat:
      * - :ref:`acor_feat <desc_I2Regression.iota2_feature_extraction.acor_feat>`
        - False
        - Apply atmospherically corrected features
        - bool
        - False
        - :ref:`acor_feat <desc_I2Regression.iota2_feature_extraction.acor_feat>`


          .. _I2Regression.iota2_feature_extraction.copy_input:
      * - copy_input
        - True
        - use spectral bands as features
        - bool
        - False
        - copy_input


          .. _I2Regression.iota2_feature_extraction.extract_bands:
      * - extract_bands
        - False
        - 
        - bool
        - False
        - extract_bands


          .. _I2Regression.iota2_feature_extraction.keep_duplicates:
      * - keep_duplicates
        - True
        - use 'rel_refl' can generate duplicated feature (ie: NDVI), set to False remove these duplicated features
        - bool
        - False
        - keep_duplicates


          .. _I2Regression.iota2_feature_extraction.rel_refl:
      * - rel_refl
        - False
        - compute relative reflectances by the red band
        - bool
        - False
        - rel_refl





Notes
=====

.. _desc_I2Regression.iota2_feature_extraction.acor_feat:

:ref:`acor_feat <I2Regression.iota2_feature_extraction.acor_feat>`
------------------------------------------------------------------
Apply atmospherically corrected featuresas explained at : http://www.cesbio.ups-tlse.fr/multitemp/?p=12746

.. _I2Regression.multi_run_fusion:

multi_run_fusion
****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.multi_run_fusion.fusionof_all_samples_validation:
      * - :ref:`fusionof_all_samples_validation <desc_I2Regression.multi_run_fusion.fusionof_all_samples_validation>`
        - False
        - Enable the use of all reference data to evaluate the fusion raster
        - bool
        - False
        - :ref:`fusionof_all_samples_validation <desc_I2Regression.multi_run_fusion.fusionof_all_samples_validation>`


          .. _I2Regression.multi_run_fusion.keep_runs_results:
      * - :ref:`keep_runs_results <desc_I2Regression.multi_run_fusion.keep_runs_results>`
        - True
        - 
        - bool
        - False
        - :ref:`keep_runs_results <desc_I2Regression.multi_run_fusion.keep_runs_results>`


          .. _I2Regression.multi_run_fusion.merge_run:
      * - merge_run
        - False
        - Enable the fusion of regression mode, merging all run in a unique result.
        - bool
        - False
        - merge_run


          .. _I2Regression.multi_run_fusion.merge_run_method:
      * - :ref:`merge_run_method <desc_I2Regression.multi_run_fusion.merge_run_method>`
        - mean
        - Indicate the fusion of regression method: 'mean' or 'median'
        - str
        - False
        - :ref:`merge_run_method <desc_I2Regression.multi_run_fusion.merge_run_method>`


          .. _I2Regression.multi_run_fusion.merge_run_ratio:
      * - merge_run_ratio
        - 0.1
        - Percentage of samples to use in order to evaluate the fusion raster
        - float
        - False
        - merge_run_ratio





Notes
=====

.. _desc_I2Regression.multi_run_fusion.fusionof_all_samples_validation:

:ref:`fusionof_all_samples_validation <I2Regression.multi_run_fusion.fusionof_all_samples_validation>`
------------------------------------------------------------------------------------------------------
If the fusion mode is enabled, enable the use of all reference data samples for validation

.. _desc_I2Regression.multi_run_fusion.keep_runs_results:

:ref:`keep_runs_results <I2Regression.multi_run_fusion.keep_runs_results>`
--------------------------------------------------------------------------
 

.. _desc_I2Regression.multi_run_fusion.merge_run_method:

:ref:`merge_run_method <I2Regression.multi_run_fusion.merge_run_method>`
------------------------------------------------------------------------
In addition to the regression fusion map, a confidence map is also produced. If the merging method is the mean then the method used to calculate the confidence map will be the standard deviation,if the median is chosen to merge the maps from the different runs then the method used to calculate the confidence map will be th median absolute deviation.

.. _I2Regression.pretrained_model:

pretrained_model
****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.pretrained_model.boundary_buffer:
      * - boundary_buffer
        - None
        - List of boundary buffer size
        - list
        - False
        - boundary_buffer


          .. _I2Regression.pretrained_model.function:
      * - :ref:`function <desc_I2Regression.pretrained_model.function>`
        - None
        - Predict function name
        - str
        - False
        - :ref:`function <desc_I2Regression.pretrained_model.function>`


          .. _I2Regression.pretrained_model.mode:
      * - :ref:`mode <desc_I2Regression.pretrained_model.mode>`
        - None
        - Algorythm nature (classification or regression)
        - str
        - False
        - :ref:`mode <desc_I2Regression.pretrained_model.mode>`


          .. _I2Regression.pretrained_model.model:
      * - :ref:`model <desc_I2Regression.pretrained_model.model>`
        - None
        - Serialized object containing the model
        - str
        - False
        - :ref:`model <desc_I2Regression.pretrained_model.model>`


          .. _I2Regression.pretrained_model.module:
      * - :ref:`module <desc_I2Regression.pretrained_model.module>`
        - /path/to/iota2/sources
        - Absolute path to the python module
        - str
        - False
        - :ref:`module <desc_I2Regression.pretrained_model.module>`





Notes
=====

.. _desc_I2Regression.pretrained_model.function:

:ref:`function <I2Regression.pretrained_model.function>`
--------------------------------------------------------
This function must have the imposed signature. It not accept any others parameters. All model dedicated parameters must be stored alongside the model.

.. _desc_I2Regression.pretrained_model.mode:

:ref:`mode <I2Regression.pretrained_model.mode>`
------------------------------------------------
The python module must contains the predict function It must handle all the potential dependencies and import related to the correct model instanciation

.. _desc_I2Regression.pretrained_model.model:

:ref:`model <I2Regression.pretrained_model.model>`
--------------------------------------------------
In the configuration file, the mandatory keys $REGION and $SEED must be present as they are replaced by iota2. In case of only one region, the region value is set to 1. Look at the documentation about the model constraint.

.. _desc_I2Regression.pretrained_model.module:

:ref:`module <I2Regression.pretrained_model.module>`
----------------------------------------------------
The python module must contains the predict function It must handle all the potential dependencies and import related to the correct model instanciation

.. _I2Regression.python_data_managing:

python_data_managing
********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.python_data_managing.chunk_size_mode:
      * - chunk_size_mode
        - split_number
        - The chunk split mode, currently the choice is 'split_number'
        - str
        - False
        - chunk_size_mode


          .. _I2Regression.python_data_managing.chunk_size_x:
      * - chunk_size_x
        - 50
        - number of cols for one chunk
        - int
        - False
        - chunk_size_x


          .. _I2Regression.python_data_managing.chunk_size_y:
      * - chunk_size_y
        - 50
        - number of rows for one chunk
        - int
        - False
        - chunk_size_y


          .. _I2Regression.python_data_managing.data_mode_access:
      * - :ref:`data_mode_access <desc_I2Regression.python_data_managing.data_mode_access>`
        - gapfilled
        - choose which data can be accessed in custom features
        - str
        - False
        - :ref:`data_mode_access <desc_I2Regression.python_data_managing.data_mode_access>`


          .. _I2Regression.python_data_managing.fill_missing_dates:
      * - :ref:`fill_missing_dates <desc_I2Regression.python_data_managing.fill_missing_dates>`
        - False
        - fill raw data with no data if dates are missing
        - bool
        - False
        - :ref:`fill_missing_dates <desc_I2Regression.python_data_managing.fill_missing_dates>`


          .. _I2Regression.python_data_managing.max_nn_inference_size:
      * - :ref:`max_nn_inference_size <desc_I2Regression.python_data_managing.max_nn_inference_size>`
        - None
        - maximum batch inference size
        - int
        - False
        - :ref:`max_nn_inference_size <desc_I2Regression.python_data_managing.max_nn_inference_size>`


          .. _I2Regression.python_data_managing.number_of_chunks:
      * - number_of_chunks
        - 50
        - the expected number of chunks
        - int
        - False
        - number_of_chunks


          .. _I2Regression.python_data_managing.padding_size_x:
      * - padding_size_x
        - 0
        - The padding for chunk
        - int
        - False
        - padding_size_x


          .. _I2Regression.python_data_managing.padding_size_y:
      * - padding_size_y
        - 0
        - The padding for chunk
        - int
        - False
        - padding_size_y





Notes
=====

.. _desc_I2Regression.python_data_managing.data_mode_access:

:ref:`data_mode_access <I2Regression.python_data_managing.data_mode_access>`
----------------------------------------------------------------------------
Three values are allowed:

- gapfilled: give access only the gapfilled data

- raw: gives access only the original raw data

- both: provides access to both data

..Notes:: Data are spatialy resampled, these parameters concern only temporal interpolation

.. _desc_I2Regression.python_data_managing.fill_missing_dates:

:ref:`fill_missing_dates <I2Regression.python_data_managing.fill_missing_dates>`
--------------------------------------------------------------------------------
If raw data access is enabled, this option considers all unique dates for all tiles and identify which dates are missing for each tile. A missing date is filled using a no data constant value.Cloud or saturation are not corrected, but masks are provided Masks contain three value: 0 for valid data, 1 for cloudy or saturated pixels, 2 for a missing date

.. _desc_I2Regression.python_data_managing.max_nn_inference_size:

:ref:`max_nn_inference_size <I2Regression.python_data_managing.max_nn_inference_size>`
--------------------------------------------------------------------------------------
Involved if a neural network inference is performed. If not set (None), the inference size will be the same as the one used during the learning stage

.. _I2Regression.scikit_models_parameters:

scikit_models_parameters
************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.scikit_models_parameters.cross_validation_folds:
      * - cross_validation_folds
        - 5
        - the number of k-folds
        - int
        - False
        - cross_validation_folds


          .. _I2Regression.scikit_models_parameters.cross_validation_grouped:
      * - cross_validation_grouped
        - False
        - 
        - bool
        - False
        - cross_validation_grouped


          .. _I2Regression.scikit_models_parameters.keyword_arguments:
      * - :ref:`keyword_arguments <desc_I2Regression.scikit_models_parameters.keyword_arguments>`
        - {}
        - keyword arguments to be passed to model
        - dict
        - False
        - :ref:`keyword_arguments <desc_I2Regression.scikit_models_parameters.keyword_arguments>`


          .. _I2Regression.scikit_models_parameters.model_type:
      * - :ref:`model_type <desc_I2Regression.scikit_models_parameters.model_type>`
        - None
        - machine learning algorthm’s name
        - str
        - False
        - :ref:`model_type <desc_I2Regression.scikit_models_parameters.model_type>`


          .. _I2Regression.scikit_models_parameters.standardization:
      * - standardization
        - True
        - 
        - bool
        - False
        - standardization





Notes
=====

.. _desc_I2Regression.scikit_models_parameters.keyword_arguments:

:ref:`keyword_arguments <I2Regression.scikit_models_parameters.keyword_arguments>`
----------------------------------------------------------------------------------
keyword arguments to be passed to model

.. _desc_I2Regression.scikit_models_parameters.model_type:

:ref:`model_type <I2Regression.scikit_models_parameters.model_type>`
--------------------------------------------------------------------
Models comming from scikit-learn are use if scikit_models_parameters.model_type is different from None. More informations about how to use scikit-learn is available at iota2 and scikit-learn machine learning algorithms.

.. _I2Regression.sensors_data_interpolation:

sensors_data_interpolation
**************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.sensors_data_interpolation.auto_date:
      * - :ref:`auto_date <desc_I2Regression.sensors_data_interpolation.auto_date>`
        - True
        - Enable the use of `start_date` and `end_date`
        - bool
        - False
        - :ref:`auto_date <desc_I2Regression.sensors_data_interpolation.auto_date>`


          .. _I2Regression.sensors_data_interpolation.use_gapfilling:
      * - use_gapfilling
        - True
        - enable the use of gapfilling (clouds/temporal interpolation)
        - bool
        - False
        - use_gapfilling


          .. _I2Regression.sensors_data_interpolation.write_outputs:
      * - :ref:`write_outputs <desc_I2Regression.sensors_data_interpolation.write_outputs>`
        - False
        - write temporary files
        - bool
        - False
        - :ref:`write_outputs <desc_I2Regression.sensors_data_interpolation.write_outputs>`





Notes
=====

.. _desc_I2Regression.sensors_data_interpolation.auto_date:

:ref:`auto_date <I2Regression.sensors_data_interpolation.auto_date>`
--------------------------------------------------------------------
If True, iota2 will automatically guess the first and the last interpolation date. Else, `start_date` and `end_date` of each sensors will be used

.. _desc_I2Regression.sensors_data_interpolation.write_outputs:

:ref:`write_outputs <I2Regression.sensors_data_interpolation.write_outputs>`
----------------------------------------------------------------------------
Write the time series before and after gapfilling, the mask time series, and also the feature time series. This option required a large amount of free disk space.

.. _I2Regression.task_retry_limits:

task_retry_limits
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Regression.task_retry_limits.allowed_retry:
      * - allowed_retry
        - 0
        - allow dask to retry a failed job N times
        - int
        - False
        - allowed_retry


          .. _I2Regression.task_retry_limits.maximum_cpu:
      * - :ref:`maximum_cpu <desc_I2Regression.task_retry_limits.maximum_cpu>`
        - 4
        - the maximum number of CPU available
        - int
        - False
        - :ref:`maximum_cpu <desc_I2Regression.task_retry_limits.maximum_cpu>`


          .. _I2Regression.task_retry_limits.maximum_ram:
      * - :ref:`maximum_ram <desc_I2Regression.task_retry_limits.maximum_ram>`
        - 16.0
        - the maximum amount of RAM available. (gB)
        - float
        - False
        - :ref:`maximum_ram <desc_I2Regression.task_retry_limits.maximum_ram>`





Notes
=====

.. _desc_I2Regression.task_retry_limits.maximum_cpu:

:ref:`maximum_cpu <I2Regression.task_retry_limits.maximum_cpu>`
---------------------------------------------------------------
the amount of cpu will be doubled if the task is killed due to ram overconsumption until maximum_cpu or allowed_retry are reach

.. _desc_I2Regression.task_retry_limits.maximum_ram:

:ref:`maximum_ram <I2Regression.task_retry_limits.maximum_ram>`
---------------------------------------------------------------
the amount of RAM will be doubled if the task is killed due to ram overconsumption until maximum_ram or allowed_retry are reach

