I2Classification
################

.. _I2Classification.Landsat5_old:

Landsat5_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.Landsat5_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Classification.Landsat5_old.end_date:
      * - :ref:`end_date <desc_I2Classification.Landsat5_old.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Classification.Landsat5_old.end_date>`


          .. _I2Classification.Landsat5_old.keep_bands:
      * - :ref:`keep_bands <desc_I2Classification.Landsat5_old.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Classification.Landsat5_old.keep_bands>`


          .. _I2Classification.Landsat5_old.start_date:
      * - :ref:`start_date <desc_I2Classification.Landsat5_old.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Classification.Landsat5_old.start_date>`


          .. _I2Classification.Landsat5_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Classification.Landsat5_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Classification.Landsat5_old.end_date:

:ref:`end_date <I2Classification.Landsat5_old.end_date>`
--------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Classification.Landsat5_old.keep_bands:

:ref:`keep_bands <I2Classification.Landsat5_old.keep_bands>`
------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Classification.Landsat5_old.start_date:

:ref:`start_date <I2Classification.Landsat5_old.start_date>`
------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Classification.Landsat8:

Landsat8
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.Landsat8.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Classification.Landsat8.end_date:
      * - :ref:`end_date <desc_I2Classification.Landsat8.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Classification.Landsat8.end_date>`


          .. _I2Classification.Landsat8.keep_bands:
      * - :ref:`keep_bands <desc_I2Classification.Landsat8.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Classification.Landsat8.keep_bands>`


          .. _I2Classification.Landsat8.start_date:
      * - :ref:`start_date <desc_I2Classification.Landsat8.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Classification.Landsat8.start_date>`


          .. _I2Classification.Landsat8.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Classification.Landsat8.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Classification.Landsat8.end_date:

:ref:`end_date <I2Classification.Landsat8.end_date>`
----------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Classification.Landsat8.keep_bands:

:ref:`keep_bands <I2Classification.Landsat8.keep_bands>`
--------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Classification.Landsat8.start_date:

:ref:`start_date <I2Classification.Landsat8.start_date>`
--------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Classification.Landsat8_old:

Landsat8_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.Landsat8_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Classification.Landsat8_old.end_date:
      * - :ref:`end_date <desc_I2Classification.Landsat8_old.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Classification.Landsat8_old.end_date>`


          .. _I2Classification.Landsat8_old.keep_bands:
      * - :ref:`keep_bands <desc_I2Classification.Landsat8_old.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Classification.Landsat8_old.keep_bands>`


          .. _I2Classification.Landsat8_old.start_date:
      * - :ref:`start_date <desc_I2Classification.Landsat8_old.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Classification.Landsat8_old.start_date>`


          .. _I2Classification.Landsat8_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Classification.Landsat8_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Classification.Landsat8_old.end_date:

:ref:`end_date <I2Classification.Landsat8_old.end_date>`
--------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Classification.Landsat8_old.keep_bands:

:ref:`keep_bands <I2Classification.Landsat8_old.keep_bands>`
------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Classification.Landsat8_old.start_date:

:ref:`start_date <I2Classification.Landsat8_old.start_date>`
------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Classification.Landsat8_usgs:

Landsat8_usgs
*************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.Landsat8_usgs.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Classification.Landsat8_usgs.end_date:
      * - :ref:`end_date <desc_I2Classification.Landsat8_usgs.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Classification.Landsat8_usgs.end_date>`


          .. _I2Classification.Landsat8_usgs.keep_bands:
      * - :ref:`keep_bands <desc_I2Classification.Landsat8_usgs.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Classification.Landsat8_usgs.keep_bands>`


          .. _I2Classification.Landsat8_usgs.start_date:
      * - :ref:`start_date <desc_I2Classification.Landsat8_usgs.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Classification.Landsat8_usgs.start_date>`


          .. _I2Classification.Landsat8_usgs.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Classification.Landsat8_usgs.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Classification.Landsat8_usgs.end_date:

:ref:`end_date <I2Classification.Landsat8_usgs.end_date>`
---------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Classification.Landsat8_usgs.keep_bands:

:ref:`keep_bands <I2Classification.Landsat8_usgs.keep_bands>`
-------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Classification.Landsat8_usgs.start_date:

:ref:`start_date <I2Classification.Landsat8_usgs.start_date>`
-------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Classification.Landsat8_usgs_infrared:

Landsat8_usgs_infrared
**********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.Landsat8_usgs_infrared.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Classification.Landsat8_usgs_infrared.enable_sensor_gapfilling:
      * - enable_sensor_gapfilling
        - False
        - Enable or disable gapfilling for landsat 8 and 9 IR data
        - bool
        - False
        - enable_sensor_gapfilling


          .. _I2Classification.Landsat8_usgs_infrared.end_date:
      * - :ref:`end_date <desc_I2Classification.Landsat8_usgs_infrared.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Classification.Landsat8_usgs_infrared.end_date>`


          .. _I2Classification.Landsat8_usgs_infrared.keep_bands:
      * - :ref:`keep_bands <desc_I2Classification.Landsat8_usgs_infrared.keep_bands>`
        - ['B10', 'B11']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Classification.Landsat8_usgs_infrared.keep_bands>`


          .. _I2Classification.Landsat8_usgs_infrared.start_date:
      * - :ref:`start_date <desc_I2Classification.Landsat8_usgs_infrared.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Classification.Landsat8_usgs_infrared.start_date>`


          .. _I2Classification.Landsat8_usgs_infrared.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Classification.Landsat8_usgs_infrared.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Classification.Landsat8_usgs_infrared.end_date:

:ref:`end_date <I2Classification.Landsat8_usgs_infrared.end_date>`
------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Classification.Landsat8_usgs_infrared.keep_bands:

:ref:`keep_bands <I2Classification.Landsat8_usgs_infrared.keep_bands>`
----------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Classification.Landsat8_usgs_infrared.start_date:

:ref:`start_date <I2Classification.Landsat8_usgs_infrared.start_date>`
----------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Classification.Landsat8_usgs_optical:

Landsat8_usgs_optical
*********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.Landsat8_usgs_optical.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Classification.Landsat8_usgs_optical.enable_sensor_gapfilling:
      * - enable_sensor_gapfilling
        - True
        - Enable or disable gapfilling for landsat 8 and 9 optical data
        - bool
        - False
        - enable_sensor_gapfilling


          .. _I2Classification.Landsat8_usgs_optical.end_date:
      * - :ref:`end_date <desc_I2Classification.Landsat8_usgs_optical.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Classification.Landsat8_usgs_optical.end_date>`


          .. _I2Classification.Landsat8_usgs_optical.keep_bands:
      * - :ref:`keep_bands <desc_I2Classification.Landsat8_usgs_optical.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Classification.Landsat8_usgs_optical.keep_bands>`


          .. _I2Classification.Landsat8_usgs_optical.start_date:
      * - :ref:`start_date <desc_I2Classification.Landsat8_usgs_optical.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Classification.Landsat8_usgs_optical.start_date>`


          .. _I2Classification.Landsat8_usgs_optical.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Classification.Landsat8_usgs_optical.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Classification.Landsat8_usgs_optical.end_date:

:ref:`end_date <I2Classification.Landsat8_usgs_optical.end_date>`
-----------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Classification.Landsat8_usgs_optical.keep_bands:

:ref:`keep_bands <I2Classification.Landsat8_usgs_optical.keep_bands>`
---------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Classification.Landsat8_usgs_optical.start_date:

:ref:`start_date <I2Classification.Landsat8_usgs_optical.start_date>`
---------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Classification.Landsat8_usgs_thermal:

Landsat8_usgs_thermal
*********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.Landsat8_usgs_thermal.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Classification.Landsat8_usgs_thermal.enable_sensor_gapfilling:
      * - enable_sensor_gapfilling
        - False
        - Enable or disable gapfilling for landsat 8 and 9 thermal data(temperature and emissivity)
        - bool
        - False
        - enable_sensor_gapfilling


          .. _I2Classification.Landsat8_usgs_thermal.end_date:
      * - :ref:`end_date <desc_I2Classification.Landsat8_usgs_thermal.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Classification.Landsat8_usgs_thermal.end_date>`


          .. _I2Classification.Landsat8_usgs_thermal.keep_bands:
      * - :ref:`keep_bands <desc_I2Classification.Landsat8_usgs_thermal.keep_bands>`
        - ['B10', 'EMIS']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Classification.Landsat8_usgs_thermal.keep_bands>`


          .. _I2Classification.Landsat8_usgs_thermal.start_date:
      * - :ref:`start_date <desc_I2Classification.Landsat8_usgs_thermal.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Classification.Landsat8_usgs_thermal.start_date>`


          .. _I2Classification.Landsat8_usgs_thermal.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Classification.Landsat8_usgs_thermal.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Classification.Landsat8_usgs_thermal.end_date:

:ref:`end_date <I2Classification.Landsat8_usgs_thermal.end_date>`
-----------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Classification.Landsat8_usgs_thermal.keep_bands:

:ref:`keep_bands <I2Classification.Landsat8_usgs_thermal.keep_bands>`
---------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Classification.Landsat8_usgs_thermal.start_date:

:ref:`start_date <I2Classification.Landsat8_usgs_thermal.start_date>`
---------------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Classification.Sentinel_2:

Sentinel_2
**********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.Sentinel_2.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Classification.Sentinel_2.end_date:
      * - :ref:`end_date <desc_I2Classification.Sentinel_2.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Classification.Sentinel_2.end_date>`


          .. _I2Classification.Sentinel_2.keep_bands:
      * - :ref:`keep_bands <desc_I2Classification.Sentinel_2.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Classification.Sentinel_2.keep_bands>`


          .. _I2Classification.Sentinel_2.start_date:
      * - :ref:`start_date <desc_I2Classification.Sentinel_2.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Classification.Sentinel_2.start_date>`


          .. _I2Classification.Sentinel_2.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Classification.Sentinel_2.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Classification.Sentinel_2.end_date:

:ref:`end_date <I2Classification.Sentinel_2.end_date>`
------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Classification.Sentinel_2.keep_bands:

:ref:`keep_bands <I2Classification.Sentinel_2.keep_bands>`
----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Classification.Sentinel_2.start_date:

:ref:`start_date <I2Classification.Sentinel_2.start_date>`
----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Classification.Sentinel_2_L3A:

Sentinel_2_L3A
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.Sentinel_2_L3A.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Classification.Sentinel_2_L3A.end_date:
      * - :ref:`end_date <desc_I2Classification.Sentinel_2_L3A.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Classification.Sentinel_2_L3A.end_date>`


          .. _I2Classification.Sentinel_2_L3A.keep_bands:
      * - :ref:`keep_bands <desc_I2Classification.Sentinel_2_L3A.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Classification.Sentinel_2_L3A.keep_bands>`


          .. _I2Classification.Sentinel_2_L3A.start_date:
      * - :ref:`start_date <desc_I2Classification.Sentinel_2_L3A.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Classification.Sentinel_2_L3A.start_date>`


          .. _I2Classification.Sentinel_2_L3A.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Classification.Sentinel_2_L3A.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Classification.Sentinel_2_L3A.end_date:

:ref:`end_date <I2Classification.Sentinel_2_L3A.end_date>`
----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Classification.Sentinel_2_L3A.keep_bands:

:ref:`keep_bands <I2Classification.Sentinel_2_L3A.keep_bands>`
--------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Classification.Sentinel_2_L3A.start_date:

:ref:`start_date <I2Classification.Sentinel_2_L3A.start_date>`
--------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Classification.Sentinel_2_S2C:

Sentinel_2_S2C
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.Sentinel_2_S2C.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _I2Classification.Sentinel_2_S2C.end_date:
      * - :ref:`end_date <desc_I2Classification.Sentinel_2_S2C.end_date>`
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`end_date <desc_I2Classification.Sentinel_2_S2C.end_date>`


          .. _I2Classification.Sentinel_2_S2C.keep_bands:
      * - :ref:`keep_bands <desc_I2Classification.Sentinel_2_S2C.keep_bands>`
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - :ref:`keep_bands <desc_I2Classification.Sentinel_2_S2C.keep_bands>`


          .. _I2Classification.Sentinel_2_S2C.start_date:
      * - :ref:`start_date <desc_I2Classification.Sentinel_2_S2C.start_date>`
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - :ref:`start_date <desc_I2Classification.Sentinel_2_S2C.start_date>`


          .. _I2Classification.Sentinel_2_S2C.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _I2Classification.Sentinel_2_S2C.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





Notes
=====

.. _desc_I2Classification.Sentinel_2_S2C.end_date:

:ref:`end_date <I2Classification.Sentinel_2_S2C.end_date>`
----------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _desc_I2Classification.Sentinel_2_S2C.keep_bands:

:ref:`keep_bands <I2Classification.Sentinel_2_S2C.keep_bands>`
--------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the extract_bands variable in the iota2_feature_extraction section must also be set to `True`:

    .. code-block:: python

        iota2_feature_extraction : 
        {
          'extract_bands':True,
        }


.. _desc_I2Classification.Sentinel_2_S2C.start_date:

:ref:`start_date <I2Classification.Sentinel_2_S2C.start_date>`
--------------------------------------------------------------


WARNING
-------



For this parameter to be taken into account,the auto_date variable in the sensors_data_interpolationsection must also be set to `False`:

    .. code-block:: python

        sensors_data_interpolation : 
        {
          'auto_date':False,
        }


.. _I2Classification.arg_classification:

arg_classification
******************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.arg_classification.boundary_comparison_mode:
      * - :ref:`boundary_comparison_mode <desc_I2Classification.arg_classification.boundary_comparison_mode>`
        - False
        - Enable classification comparison
        - bool
        - False
        - :ref:`boundary_comparison_mode <desc_I2Classification.arg_classification.boundary_comparison_mode>`


          .. _I2Classification.arg_classification.boundary_exterior_buffer_size:
      * - :ref:`boundary_exterior_buffer_size <desc_I2Classification.arg_classification.boundary_exterior_buffer_size>`
        - 0
        - Buffer size outside the region
        - int
        - False
        - :ref:`boundary_exterior_buffer_size <desc_I2Classification.arg_classification.boundary_exterior_buffer_size>`


          .. _I2Classification.arg_classification.boundary_fusion_epsilon:
      * - :ref:`boundary_fusion_epsilon <desc_I2Classification.arg_classification.boundary_fusion_epsilon>`
        - 0.0
        - Threshold to avoid weights equals to zero
        - float
        - False
        - :ref:`boundary_fusion_epsilon <desc_I2Classification.arg_classification.boundary_fusion_epsilon>`


          .. _I2Classification.arg_classification.boundary_interior_buffer_size:
      * - :ref:`boundary_interior_buffer_size <desc_I2Classification.arg_classification.boundary_interior_buffer_size>`
        - 0
        - Buffer size inside the region
        - int
        - False
        - :ref:`boundary_interior_buffer_size <desc_I2Classification.arg_classification.boundary_interior_buffer_size>`


          .. _I2Classification.arg_classification.classif_mode:
      * - :ref:`classif_mode <desc_I2Classification.arg_classification.classif_mode>`
        - separate
        - 'separate' or 'fusion'
        - str
        - False
        - :ref:`classif_mode <desc_I2Classification.arg_classification.classif_mode>`


          .. _I2Classification.arg_classification.dempstershafer_mob:
      * - :ref:`dempstershafer_mob <desc_I2Classification.arg_classification.dempstershafer_mob>`
        - precision
        - Choose the dempster shafer mass of belief estimation method
        - str
        - False
        - :ref:`dempstershafer_mob <desc_I2Classification.arg_classification.dempstershafer_mob>`


          .. _I2Classification.arg_classification.enable_boundary_fusion:
      * - :ref:`enable_boundary_fusion <desc_I2Classification.arg_classification.enable_boundary_fusion>`
        - False
        - Enable the boundary fusion
        - bool
        - False
        - :ref:`enable_boundary_fusion <desc_I2Classification.arg_classification.enable_boundary_fusion>`


          .. _I2Classification.arg_classification.enable_probability_map:
      * - :ref:`enable_probability_map <desc_I2Classification.arg_classification.enable_probability_map>`
        - False
        - Produce the probability map
        - bool
        - False
        - :ref:`enable_probability_map <desc_I2Classification.arg_classification.enable_probability_map>`


          .. _I2Classification.arg_classification.fusion_options:
      * - fusion_options
        -  -nodatalabel 0 -method majorityvoting
        - OTB FusionOfClassification options for voting method involved if classif_mode is set to 'fusion'
        - str
        - False
        - fusion_options


          .. _I2Classification.arg_classification.fusionofclassification_all_samples_validation:
      * - :ref:`fusionofclassification_all_samples_validation <desc_I2Classification.arg_classification.fusionofclassification_all_samples_validation>`
        - False
        - Enable the use of all reference data to validate the classification merge
        - bool
        - False
        - :ref:`fusionofclassification_all_samples_validation <desc_I2Classification.arg_classification.fusionofclassification_all_samples_validation>`


          .. _I2Classification.arg_classification.generate_final_probability_map:
      * - :ref:`generate_final_probability_map <desc_I2Classification.arg_classification.generate_final_probability_map>`
        - False
        - Enable the mosaicing of probabilities maps.
        - bool
        - False
        - :ref:`generate_final_probability_map <desc_I2Classification.arg_classification.generate_final_probability_map>`


          .. _I2Classification.arg_classification.keep_runs_results:
      * - :ref:`keep_runs_results <desc_I2Classification.arg_classification.keep_runs_results>`
        - True
        - 
        - bool
        - False
        - :ref:`keep_runs_results <desc_I2Classification.arg_classification.keep_runs_results>`


          .. _I2Classification.arg_classification.merge_final_classifications:
      * - merge_final_classifications
        - False
        - Enable the fusion of classifications mode, merging all run in a unique result.
        - bool
        - False
        - merge_final_classifications


          .. _I2Classification.arg_classification.merge_final_classifications_indecidedlabel:
      * - merge_final_classifications_indecidedlabel
        - 255
        - Indicate the label for indecision case during fusion
        - int
        - False
        - merge_final_classifications_indecidedlabel


          .. _I2Classification.arg_classification.merge_final_classifications_method:
      * - merge_final_classifications_method
        - majorityvoting
        - Indicate the fusion of classification method: 'majorityvoting' or 'dempstershafer'
        - str
        - False
        - merge_final_classifications_method


          .. _I2Classification.arg_classification.merge_final_classifications_ratio:
      * - merge_final_classifications_ratio
        - 0.1
        - Percentage of samples to use in order to evaluate the fusion raster
        - float
        - False
        - merge_final_classifications_ratio


          .. _I2Classification.arg_classification.no_label_management:
      * - no_label_management
        - maxConfidence
        - Method for choosing a label in case of fusion
        - str
        - False
        - no_label_management





Notes
=====

.. _desc_I2Classification.arg_classification.boundary_comparison_mode:

:ref:`boundary_comparison_mode <I2Classification.arg_classification.boundary_comparison_mode>`
----------------------------------------------------------------------------------------------
If enabled it will produce two maps The boundary regions will be analysed to measure the method improvements.

.. _desc_I2Classification.arg_classification.boundary_exterior_buffer_size:

:ref:`boundary_exterior_buffer_size <I2Classification.arg_classification.boundary_exterior_buffer_size>`
--------------------------------------------------------------------------------------------------------
This value can be different than the interior buffer. The unit is in meter. The value is divided by the resolution to estimate the distance in pixels.

.. _desc_I2Classification.arg_classification.boundary_fusion_epsilon:

:ref:`boundary_fusion_epsilon <I2Classification.arg_classification.boundary_fusion_epsilon>`
--------------------------------------------------------------------------------------------
If the region shape contains the buffer operations are not bijective. In this condition, several weights can be set to 0. This lead to 0 area in the final map (holes).The weights maps are stored as uint16. The thresold should be higher enough to be different to zero once multiplied by 1000

.. _desc_I2Classification.arg_classification.boundary_interior_buffer_size:

:ref:`boundary_interior_buffer_size <I2Classification.arg_classification.boundary_interior_buffer_size>`
--------------------------------------------------------------------------------------------------------
This value can be different than the exterior buffer. The unit is in meter. The value is divided by the resolution to estimate the distance in pixels.

.. _desc_I2Classification.arg_classification.classif_mode:

:ref:`classif_mode <I2Classification.arg_classification.classif_mode>`
----------------------------------------------------------------------
 If 'fusion' : too huge models will be devided into smaller ones and will classify the same pixels. The treshold between small/big models is define by the parameter 'mode_outside_regionsplit'

.. _desc_I2Classification.arg_classification.dempstershafer_mob:

:ref:`dempstershafer_mob <I2Classification.arg_classification.dempstershafer_mob>`
----------------------------------------------------------------------------------
Two kind of indexes can be used:
* Global: `accuracy` or `kappa`
* Per class: `precision` or `recall`


.. _desc_I2Classification.arg_classification.enable_boundary_fusion:

:ref:`enable_boundary_fusion <I2Classification.arg_classification.enable_boundary_fusion>`
------------------------------------------------------------------------------------------
If enabled probabilities are used to fuse maps between regions

.. _desc_I2Classification.arg_classification.enable_probability_map:

:ref:`enable_probability_map <I2Classification.arg_classification.enable_probability_map>`
------------------------------------------------------------------------------------------
A probability map is a image with N bands , where N is the number of classes in the nomenclature file. The bands are sorted in ascending order more information more information :doc:`here <probability_maps>`

.. _desc_I2Classification.arg_classification.fusionofclassification_all_samples_validation:

:ref:`fusionofclassification_all_samples_validation <I2Classification.arg_classification.fusionofclassification_all_samples_validation>`
----------------------------------------------------------------------------------------------------------------------------------------
If the fusion mode is enabled, enable the use of all reference data samples for validation

.. _desc_I2Classification.arg_classification.generate_final_probability_map:

:ref:`generate_final_probability_map <I2Classification.arg_classification.generate_final_probability_map>`
----------------------------------------------------------------------------------------------------------
This operation can produce heavy maps.If you not need it over the whole area in one image, disable this option.

.. _desc_I2Classification.arg_classification.keep_runs_results:

:ref:`keep_runs_results <I2Classification.arg_classification.keep_runs_results>`
--------------------------------------------------------------------------------
If in fusion mode, two final reports can be provided. One for each seed, and one for the classification fusion

.. _I2Classification.arg_train:

arg_train
*********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.arg_train.classifier:
      * - classifier
        - None
        - otb classification algorithm
        - str
        - False
        - classifier


          .. _I2Classification.arg_train.deep_learning_parameters:
      * - deep_learning_parameters
        - {}
        - deep learning parameter description is available :doc:`here <deep_learning>`
        - dict
        - False
        - deep_learning_parameters


          .. _I2Classification.arg_train.features:
      * - :ref:`features <desc_I2Classification.arg_train.features>`
        - ['NDVI', 'NDWI', 'Brightness']
        - List of additional features computed
        - list
        - False
        - :ref:`features <desc_I2Classification.arg_train.features>`


          .. _I2Classification.arg_train.features_from_raw_dates:
      * - :ref:`features_from_raw_dates <desc_I2Classification.arg_train.features_from_raw_dates>`
        - False
        - learn model from raw sensor's date (no interpolations)
        - bool
        - False
        - :ref:`features_from_raw_dates <desc_I2Classification.arg_train.features_from_raw_dates>`


          .. _I2Classification.arg_train.force_standard_labels:
      * - :ref:`force_standard_labels <desc_I2Classification.arg_train.force_standard_labels>`
        - False
        - Standardize labels for feature extraction
        - bool
        - False
        - :ref:`force_standard_labels <desc_I2Classification.arg_train.force_standard_labels>`


          .. _I2Classification.arg_train.learning_samples_extension:
      * - :ref:`learning_samples_extension <desc_I2Classification.arg_train.learning_samples_extension>`
        - sqlite
        - learning samples file extension, possible values are 'sqlite' and 'csv'
        - str
        - False
        - :ref:`learning_samples_extension <desc_I2Classification.arg_train.learning_samples_extension>`


          .. _I2Classification.arg_train.mode_outside_regionsplit:
      * - :ref:`mode_outside_regionsplit <desc_I2Classification.arg_train.mode_outside_regionsplit>`
        - 0.1
        - Set the threshold for split huge model
        - float
        - False
        - :ref:`mode_outside_regionsplit <desc_I2Classification.arg_train.mode_outside_regionsplit>`


          .. _I2Classification.arg_train.otb_classifier_options:
      * - :ref:`otb_classifier_options <desc_I2Classification.arg_train.otb_classifier_options>`
        - None
        - OTB option for classifier.If None, the OTB default values are used
        - dict
        - False
        - :ref:`otb_classifier_options <desc_I2Classification.arg_train.otb_classifier_options>`


          .. _I2Classification.arg_train.random_seed:
      * - :ref:`random_seed <desc_I2Classification.arg_train.random_seed>`
        - None
        - Fix the random seed for random split of reference data
        - int
        - False
        - :ref:`random_seed <desc_I2Classification.arg_train.random_seed>`


          .. _I2Classification.arg_train.ratio:
      * - ratio
        - 0.5
        - Should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the train split.
        - float
        - False
        - ratio


          .. _I2Classification.arg_train.runs:
      * - :ref:`runs <desc_I2Classification.arg_train.runs>`
        - 1
        - Number of independent runs processed
        - int
        - False
        - :ref:`runs <desc_I2Classification.arg_train.runs>`


          .. _I2Classification.arg_train.sample_augmentation:
      * - :ref:`sample_augmentation <desc_I2Classification.arg_train.sample_augmentation>`
        - {'activate': False, 'bins': 10}
        - OTB parameters for sample augmentation
        - dict
        - False
        - :ref:`sample_augmentation <desc_I2Classification.arg_train.sample_augmentation>`


          .. _I2Classification.arg_train.sample_management:
      * - :ref:`sample_management <desc_I2Classification.arg_train.sample_management>`
        - None
        - Absolute path to a CSV file containing samples transfert strategies
        - str
        - False
        - :ref:`sample_management <desc_I2Classification.arg_train.sample_management>`


          .. _I2Classification.arg_train.sample_selection:
      * - :ref:`sample_selection <desc_I2Classification.arg_train.sample_selection>`
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - False
        - :ref:`sample_selection <desc_I2Classification.arg_train.sample_selection>`


          .. _I2Classification.arg_train.sample_validation:
      * - :ref:`sample_validation <desc_I2Classification.arg_train.sample_validation>`
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - False
        - :ref:`sample_validation <desc_I2Classification.arg_train.sample_validation>`


          .. _I2Classification.arg_train.sampling_validation:
      * - sampling_validation
        - False
        - Enable sampling validation
        - bool
        - False
        - sampling_validation


          .. _I2Classification.arg_train.split_ground_truth:
      * - :ref:`split_ground_truth <desc_I2Classification.arg_train.split_ground_truth>`
        - True
        - Enable the split of reference data
        - bool
        - False
        - :ref:`split_ground_truth <desc_I2Classification.arg_train.split_ground_truth>`


          .. _I2Classification.arg_train.validity_threshold:
      * - validity_threshold
        - 1
        - threshold above which a training pixel is considered valid
        - int
        - False
        - validity_threshold





Notes
=====

.. _desc_I2Classification.arg_train.features:

:ref:`features <I2Classification.arg_train.features>`
-----------------------------------------------------
This parameter enable the computation of the three indices if available for the sensor used.There is no choice for using only one of them

.. _desc_I2Classification.arg_train.features_from_raw_dates:

:ref:`features_from_raw_dates <I2Classification.arg_train.features_from_raw_dates>`
-----------------------------------------------------------------------------------
If True, during the learning and classification step, each pixel will receive a vector of values of the size of the number of all dates detected. As the pixels were not all acquired on the same dates, the vector will contains NaNs on the unacquired dates.

.. _desc_I2Classification.arg_train.force_standard_labels:

:ref:`force_standard_labels <I2Classification.arg_train.force_standard_labels>`
-------------------------------------------------------------------------------
The chain label each features by the sensors name, the spectral band or indice and the date. If activated this parameter use the OTB default value (`value_X`)

.. _desc_I2Classification.arg_train.learning_samples_extension:

:ref:`learning_samples_extension <I2Classification.arg_train.learning_samples_extension>`
-----------------------------------------------------------------------------------------
Default value is 'sqlite' (faster), in case of the number of features is superior to 2000, as sqlite file doesn't accept more than 2000 columns, it should be turn to 'csv'.

.. _desc_I2Classification.arg_train.mode_outside_regionsplit:

:ref:`mode_outside_regionsplit <I2Classification.arg_train.mode_outside_regionsplit>`
-------------------------------------------------------------------------------------
This parameter is available if regionPath is used and arg_train.classif_mode is set to fusion. It represents the maximum size covered by a region. If the regions are larger than this threshold, then N models are built by randomly selecting features inside the region.

.. _desc_I2Classification.arg_train.otb_classifier_options:

:ref:`otb_classifier_options <I2Classification.arg_train.otb_classifier_options>`
---------------------------------------------------------------------------------
This parameter is a dictionnary which accepts all OTB application parameters. To know the exhaustive parameter list  use `otbcli_TrainVectorClassifier` in a terminal or look at the OTB documentation

.. _desc_I2Classification.arg_train.random_seed:

:ref:`random_seed <I2Classification.arg_train.random_seed>`
-----------------------------------------------------------
Fix the random seed used for random split of reference data If set, the results must be the same for a given classifier

.. _desc_I2Classification.arg_train.runs:

:ref:`runs <I2Classification.arg_train.runs>`
---------------------------------------------
Number of independent runs processed. Each run has his own learning samples. Must be an integer greater than 0

.. _desc_I2Classification.arg_train.sample_augmentation:

:ref:`sample_augmentation <I2Classification.arg_train.sample_augmentation>`
---------------------------------------------------------------------------
In supervised classification the balance between class samples is important. There are any ways to manage class balancing in iota2, using :ref:`refSampleSelection` or the classifier's options to limit the number of samples by class.
An other approch is to generate synthetic samples. It is the purpose of this functionality, which is called 'sample augmentation'.

    .. code-block:: python

        {'activate':False}

Example
^^^^^^^

    .. code-block:: python

        sample_augmentation : {'target_models':['1', '2'],
                              'strategy' : 'jitter',
                              'strategy.jitter.stdfactor' : 10,
                              'strategy.smote.neighbors'  : 5,
                              'samples.strategy' : 'balance',
                              'activate' : True
                              }


iota2 implements an interface to the OTB `SampleAugmentation <https://www.orfeo-toolbox.org/CookBook/Applications/app_SampleSelection.html>`_ application.
There are three methods to generate samples : replicate, jitter and smote.The documentation :doc:`here <sampleAugmentation_explain>` explains the difference between these approaches.

 ``samples.strategy`` specifies how many samples must be created.There are 3 different strategies:    - minNumber
        To set the minimum number of samples by class required
    - balance
        balance all classes with the same number of samples as the majority one
    - byClass
        augment only some of the classes
Parameters related to ``minNumber`` and ``byClass`` strategies are
    - samples.strategy.minNumber
        minimum number of samples
    - samples.strategy.byClass
        path to a CSV file containing in first column the class's label and 
        in the second column the minimum number of samples required.
In the above example, classes of models '1' and '2' will be augmented to the most represented class in the corresponding model using the jitter method.

To perform sample augmentation for regression problems, we return to a configuration similar to classification.
For that purpose we create fake classes using the ``bins`` parameter. The ``bins`` parameter can be an integer in which case the interval of values of the target output variable is divided into ``bins`` sub-intervals of equal width, and each sample gets a fake a class corresponding to the number of the interval in which its label fell.
``bins`` can also be a list of ascending values used as interval boundaries for assign the classes.

.. _desc_I2Classification.arg_train.sample_management:

:ref:`sample_management <I2Classification.arg_train.sample_management>`
-----------------------------------------------------------------------
The CSV must contain a row per transfert
    .. code-block:: python

        >>> cat /absolute/path/myRules.csv
            1,2,4,2
Meaning :
        +--------+-------------+------------+----------+
        | source | destination | class name | quantity |
        +========+=============+============+==========+
        |   1    |      2      |      4     |     2    |
        +--------+-------------+------------+----------+

Currently, setting the 'random_seed' parameter has no effect on this workflow.

.. _desc_I2Classification.arg_train.sample_selection:

:ref:`sample_selection <I2Classification.arg_train.sample_selection>`
---------------------------------------------------------------------
This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
-------

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).

.. _desc_I2Classification.arg_train.sample_validation:

:ref:`sample_validation <I2Classification.arg_train.sample_validation>`
-----------------------------------------------------------------------
This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
-------

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).

.. _desc_I2Classification.arg_train.split_ground_truth:

:ref:`split_ground_truth <I2Classification.arg_train.split_ground_truth>`
-------------------------------------------------------------------------
If set to False, the chain use all polygons for both training and validation

.. _I2Classification.builders:

builders
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.builders.builders_class_name:
      * - :ref:`builders_class_name <desc_I2Classification.builders.builders_class_name>`
        - ['I2Classification']
        - The name of the class defining the builder
        - list
        - False
        - :ref:`builders_class_name <desc_I2Classification.builders.builders_class_name>`


          .. _I2Classification.builders.builders_paths:
      * - :ref:`builders_paths <desc_I2Classification.builders.builders_paths>`
        - /path/to/iota2/sources
        - The path to user builders
        - str
        - False
        - :ref:`builders_paths <desc_I2Classification.builders.builders_paths>`





Notes
=====

.. _desc_I2Classification.builders.builders_class_name:

:ref:`builders_class_name <I2Classification.builders.builders_class_name>`
--------------------------------------------------------------------------
Available builders are : 'I2Classification', 'I2FeaturesMap' and 'I2Obia'

.. _desc_I2Classification.builders.builders_paths:

:ref:`builders_paths <I2Classification.builders.builders_paths>`
----------------------------------------------------------------
If not indicated, the iota2 source directory is used: */iota2/sequence_builders/

.. _I2Classification.chain:

chain
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.chain.check_inputs:
      * - :ref:`check_inputs <desc_I2Classification.chain.check_inputs>`
        - True
        - Enable the inputs verification
        - bool
        - False
        - :ref:`check_inputs <desc_I2Classification.chain.check_inputs>`


          .. _I2Classification.chain.cloud_threshold:
      * - :ref:`cloud_threshold <desc_I2Classification.chain.cloud_threshold>`
        - 0
        - Threshold to consider that a pixel is valid
        - int
        - False
        - :ref:`cloud_threshold <desc_I2Classification.chain.cloud_threshold>`


          .. _I2Classification.chain.color_table:
      * - color_table
        - None
        - Absolute path to the file that links the classes and their colours
        - str
        - True
        - color_table


          .. _I2Classification.chain.compression_algorithm:
      * - compression_algorithm
        - ZSTD
        - Set the gdal compression algorithm to use: NONE, LZW, ZSTD (default).All rasters write with OTB will be compress with the chosen algorithm.
        - str
        - False
        - compression_algorithm


          .. _I2Classification.chain.compression_predictor:
      * - :ref:`compression_predictor <desc_I2Classification.chain.compression_predictor>`
        - 2
        - Set the predictor for LZW and ZSTD compression: 1 (no predictor), 2 (horizontal differencing, default)
        - int
        - False
        - :ref:`compression_predictor <desc_I2Classification.chain.compression_predictor>`


          .. _I2Classification.chain.data_field:
      * - :ref:`data_field <desc_I2Classification.chain.data_field>`
        - None
        - Field name indicating classes labels in `ground_thruth`
        - str
        - True
        - :ref:`data_field <desc_I2Classification.chain.data_field>`


          .. _I2Classification.chain.first_step:
      * - first_step
        - None
        - The step group name indicating where the chain start
        - str
        - False
        - first_step


          .. _I2Classification.chain.ground_truth:
      * - ground_truth
        - None
        - Absolute path to reference data
        - str
        - True
        - ground_truth


          .. _I2Classification.chain.l5_path_old:
      * - l5_path_old
        - None
        - Absolute path to Landsat-5 images coming from old THEIA format (D*H*)
        - str
        - False
        - l5_path_old


          .. _I2Classification.chain.l8_path:
      * - l8_path
        - None
        - Absolute path to Landsat-8 images comingfrom new tiled THEIA data
        - str
        - False
        - l8_path


          .. _I2Classification.chain.l8_path_old:
      * - l8_path_old
        - None
        - Absolute path to Landsat-8 images coming from old THEIA format (D*H*)
        - str
        - False
        - l8_path_old


          .. _I2Classification.chain.l8_usgs_infrared_path:
      * - l8_usgs_infrared_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_infrared_path


          .. _I2Classification.chain.l8_usgs_optical_path:
      * - l8_usgs_optical_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_optical_path


          .. _I2Classification.chain.l8_usgs_path:
      * - l8_usgs_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_path


          .. _I2Classification.chain.l8_usgs_thermal_path:
      * - l8_usgs_thermal_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - False
        - l8_usgs_thermal_path


          .. _I2Classification.chain.last_step:
      * - last_step
        - None
        - The step group name indicating where the chain ends
        - str
        - False
        - last_step


          .. _I2Classification.chain.list_tile:
      * - list_tile
        - None
        - List of tile to process, separated by space
        - str
        - True
        - list_tile


          .. _I2Classification.chain.logger_level:
      * - logger_level
        - INFO
        - Set the logger level: NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
        - str
        - False
        - logger_level


          .. _I2Classification.chain.minimum_required_dates:
      * - minimum_required_dates
        - 2
        - required minimum number of available dates for each sensor
        - int
        - False
        - minimum_required_dates


          .. _I2Classification.chain.nomenclature_path:
      * - nomenclature_path
        - None
        - Absolute path to the nomenclature description file
        - str
        - True
        - nomenclature_path


          .. _I2Classification.chain.output_path:
      * - :ref:`output_path <desc_I2Classification.chain.output_path>`
        - None
        - Absolute path to the output directory
        - str
        - True
        - :ref:`output_path <desc_I2Classification.chain.output_path>`


          .. _I2Classification.chain.output_statistics:
      * - output_statistics
        - True
        - output_statistics
        - bool
        - False
        - output_statistics


          .. _I2Classification.chain.proj:
      * - proj
        - None
        - The projection wanted. Format EPSG:XXXX is mandatory
        - str
        - True
        - proj


          .. _I2Classification.chain.region_field:
      * - :ref:`region_field <desc_I2Classification.chain.region_field>`
        - region
        - The column name for region indicator in`region_path` file
        - str
        - False
        - :ref:`region_field <desc_I2Classification.chain.region_field>`


          .. _I2Classification.chain.region_path:
      * - region_path
        - None
        - Absolute path to a region vector file
        - str
        - False
        - region_path


          .. _I2Classification.chain.remove_output_path:
      * - :ref:`remove_output_path <desc_I2Classification.chain.remove_output_path>`
        - True
        - Before the launch of iota2, remove the content of `output_path`
        - bool
        - False
        - :ref:`remove_output_path <desc_I2Classification.chain.remove_output_path>`


          .. _I2Classification.chain.s1_path:
      * - s1_path
        - None
        - Absolute path to Sentinel-1 configuration file
        - str
        - False
        - s1_path


          .. _I2Classification.chain.s2_l3a_output_path:
      * - s2_l3a_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_l3a_output_path


          .. _I2Classification.chain.s2_l3a_path:
      * - s2_l3a_path
        - None
        - Absolute path to Sentinel-2 L3A images (THEIA format)
        - str
        - False
        - s2_l3a_path


          .. _I2Classification.chain.s2_output_path:
      * - s2_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_output_path


          .. _I2Classification.chain.s2_path:
      * - s2_path
        - None
        - Absolute path to Sentinel-2 images (THEIA format)
        - str
        - False
        - s2_path


          .. _I2Classification.chain.s2_s2c_output_path:
      * - s2_s2c_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_s2c_output_path


          .. _I2Classification.chain.s2_s2c_path:
      * - s2_s2c_path
        - None
        - Absolute path to Sentinel-2 images (Sen2Cor format)
        - str
        - False
        - s2_s2c_path


          .. _I2Classification.chain.spatial_resolution:
      * - :ref:`spatial_resolution <desc_I2Classification.chain.spatial_resolution>`
        - []
        - Output spatial resolution
        - list or scalar
        - False
        - :ref:`spatial_resolution <desc_I2Classification.chain.spatial_resolution>`


          .. _I2Classification.chain.user_feat_path:
      * - :ref:`user_feat_path <desc_I2Classification.chain.user_feat_path>`
        - None
        - Absolute path to the user's features path
        - str
        - False
        - :ref:`user_feat_path <desc_I2Classification.chain.user_feat_path>`





Notes
=====

.. _desc_I2Classification.chain.check_inputs:

:ref:`check_inputs <I2Classification.chain.check_inputs>`
---------------------------------------------------------
Enable the inputs verification. It can take a lot of time for large dataset. Check if region intersect reference data for instance

.. _desc_I2Classification.chain.cloud_threshold:

:ref:`cloud_threshold <I2Classification.chain.cloud_threshold>`
---------------------------------------------------------------
Indicates the threshold for a polygon to be used for learning. It use the validity count, which is incremented if a cloud, a cloud shadow or a saturated pixel is detected

.. _desc_I2Classification.chain.compression_predictor:

:ref:`compression_predictor <I2Classification.chain.compression_predictor>`
---------------------------------------------------------------------------
It has been noted that in some cases, once the features are written to disk, the raster file may be empty. If this is the case, please change the predictor to 1 or 3.

.. _desc_I2Classification.chain.data_field:

:ref:`data_field <I2Classification.chain.data_field>`
-----------------------------------------------------
All the labels values must be different to 0.
It is recommended to use a continuous range of values but it is not mandatory.
Keep in mind that the final product type is detected according to the maximum label value.
Try to keep values between 1 and 255 to avoid heavy products.

.. _desc_I2Classification.chain.output_path:

:ref:`output_path <I2Classification.chain.output_path>`
-------------------------------------------------------
Absolute path to the output directory.It is recommended to have one directory per run of the chain

.. _desc_I2Classification.chain.region_field:

:ref:`region_field <I2Classification.chain.region_field>`
---------------------------------------------------------
This column in the database must contains string which can be converted into integers. For instance '1_2' does not match this condition.
It is mandatory that the region identifiers are > 0.

.. _desc_I2Classification.chain.remove_output_path:

:ref:`remove_output_path <I2Classification.chain.remove_output_path>`
---------------------------------------------------------------------
Before the launch of iota2, remove the content of `output_path`. Only if the `first_step` is `init` and the folder name is valid 

.. _desc_I2Classification.chain.spatial_resolution:

:ref:`spatial_resolution <I2Classification.chain.spatial_resolution>`
---------------------------------------------------------------------
The spatial resolution expected.It can be provided as integer or float,or as a list containing two values for non squared resolution

.. _desc_I2Classification.chain.user_feat_path:

:ref:`user_feat_path <I2Classification.chain.user_feat_path>`
-------------------------------------------------------------
Absolute path to the user's features path. They must be stored by tiles

.. _I2Classification.external_features:

external_features
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.external_features.concat_mode:
      * - :ref:`concat_mode <desc_I2Classification.external_features.concat_mode>`
        - True
        - enable the use of all features
        - bool
        - False
        - :ref:`concat_mode <desc_I2Classification.external_features.concat_mode>`


          .. _I2Classification.external_features.exogeneous_data:
      * - :ref:`exogeneous_data <desc_I2Classification.external_features.exogeneous_data>`
        - None
        - Path to a Geotiff file containing additional data to be used in external features
        - str
        - False
        - :ref:`exogeneous_data <desc_I2Classification.external_features.exogeneous_data>`


          .. _I2Classification.external_features.external_features_flag:
      * - external_features_flag
        - False
        - enable the external features mode
        - bool
        - False
        - external_features_flag


          .. _I2Classification.external_features.functions:
      * - :ref:`functions <desc_I2Classification.external_features.functions>`
        - None
        - function list to be used to compute features
        - str/list
        - False
        - :ref:`functions <desc_I2Classification.external_features.functions>`


          .. _I2Classification.external_features.module:
      * - module
        - /path/to/iota2/sources
        - absolute path for user source code
        - str
        - False
        - module


          .. _I2Classification.external_features.no_data_value:
      * - no_data_value
        - -10000
        - value considered as no_data in features map mosaic ('I2FeaturesMap' builder name)
        - int
        - False
        - no_data_value


          .. _I2Classification.external_features.output_name:
      * - output_name
        - None
        - temporary chunks are written using this name as prefix
        - str
        - False
        - output_name





Notes
=====

.. _desc_I2Classification.external_features.concat_mode:

:ref:`concat_mode <I2Classification.external_features.concat_mode>`
-------------------------------------------------------------------
if disabled, only external features are used in the whole processing

.. _desc_I2Classification.external_features.exogeneous_data:

:ref:`exogeneous_data <I2Classification.external_features.exogeneous_data>`
---------------------------------------------------------------------------
If the =exogeneous_data= contains '$TILE', it will be replaced by the tile name being processed.If you want to reproject your data on given tiles, you can use the =split_raster_into_tiles.py= command line tool.

Usage: =split_raster_into_tiles.py --help=.

.. _desc_I2Classification.external_features.functions:

:ref:`functions <I2Classification.external_features.functions>`
---------------------------------------------------------------
Can be a string of space-separated function namesCan be a list of either strings of function nameor lists of one function name and one argument mapping

.. _I2Classification.iota2_feature_extraction:

iota2_feature_extraction
************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.iota2_feature_extraction.acor_feat:
      * - :ref:`acor_feat <desc_I2Classification.iota2_feature_extraction.acor_feat>`
        - False
        - Apply atmospherically corrected features
        - bool
        - False
        - :ref:`acor_feat <desc_I2Classification.iota2_feature_extraction.acor_feat>`


          .. _I2Classification.iota2_feature_extraction.copy_input:
      * - copy_input
        - True
        - use spectral bands as features
        - bool
        - False
        - copy_input


          .. _I2Classification.iota2_feature_extraction.extract_bands:
      * - extract_bands
        - False
        - 
        - bool
        - False
        - extract_bands


          .. _I2Classification.iota2_feature_extraction.keep_duplicates:
      * - keep_duplicates
        - True
        - use 'rel_refl' can generate duplicated feature (ie: NDVI), set to False remove these duplicated features
        - bool
        - False
        - keep_duplicates


          .. _I2Classification.iota2_feature_extraction.rel_refl:
      * - rel_refl
        - False
        - compute relative reflectances by the red band
        - bool
        - False
        - rel_refl





Notes
=====

.. _desc_I2Classification.iota2_feature_extraction.acor_feat:

:ref:`acor_feat <I2Classification.iota2_feature_extraction.acor_feat>`
----------------------------------------------------------------------
Apply atmospherically corrected featuresas explained at : http://www.cesbio.ups-tlse.fr/multitemp/?p=12746

.. _I2Classification.pretrained_model:

pretrained_model
****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.pretrained_model.boundary_buffer:
      * - boundary_buffer
        - None
        - List of boundary buffer size
        - list
        - False
        - boundary_buffer


          .. _I2Classification.pretrained_model.function:
      * - :ref:`function <desc_I2Classification.pretrained_model.function>`
        - None
        - Predict function name
        - str
        - False
        - :ref:`function <desc_I2Classification.pretrained_model.function>`


          .. _I2Classification.pretrained_model.mode:
      * - :ref:`mode <desc_I2Classification.pretrained_model.mode>`
        - None
        - Algorythm nature (classification or regression)
        - str
        - False
        - :ref:`mode <desc_I2Classification.pretrained_model.mode>`


          .. _I2Classification.pretrained_model.model:
      * - :ref:`model <desc_I2Classification.pretrained_model.model>`
        - None
        - Serialized object containing the model
        - str
        - False
        - :ref:`model <desc_I2Classification.pretrained_model.model>`


          .. _I2Classification.pretrained_model.module:
      * - :ref:`module <desc_I2Classification.pretrained_model.module>`
        - /path/to/iota2/sources
        - Absolute path to the python module
        - str
        - False
        - :ref:`module <desc_I2Classification.pretrained_model.module>`





Notes
=====

.. _desc_I2Classification.pretrained_model.function:

:ref:`function <I2Classification.pretrained_model.function>`
------------------------------------------------------------
This function must have the imposed signature. It not accept any others parameters. All model dedicated parameters must be stored alongside the model.

.. _desc_I2Classification.pretrained_model.mode:

:ref:`mode <I2Classification.pretrained_model.mode>`
----------------------------------------------------
The python module must contains the predict function It must handle all the potential dependencies and import related to the correct model instanciation

.. _desc_I2Classification.pretrained_model.model:

:ref:`model <I2Classification.pretrained_model.model>`
------------------------------------------------------
In the configuration file, the mandatory keys $REGION and $SEED must be present as they are replaced by iota2. In case of only one region, the region value is set to 1. Look at the documentation about the model constraint.

.. _desc_I2Classification.pretrained_model.module:

:ref:`module <I2Classification.pretrained_model.module>`
--------------------------------------------------------
The python module must contains the predict function It must handle all the potential dependencies and import related to the correct model instanciation

.. _I2Classification.python_data_managing:

python_data_managing
********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.python_data_managing.chunk_size_mode:
      * - chunk_size_mode
        - split_number
        - The chunk split mode, currently the choice is 'split_number'
        - str
        - False
        - chunk_size_mode


          .. _I2Classification.python_data_managing.chunk_size_x:
      * - chunk_size_x
        - 50
        - number of cols for one chunk
        - int
        - False
        - chunk_size_x


          .. _I2Classification.python_data_managing.chunk_size_y:
      * - chunk_size_y
        - 50
        - number of rows for one chunk
        - int
        - False
        - chunk_size_y


          .. _I2Classification.python_data_managing.data_mode_access:
      * - :ref:`data_mode_access <desc_I2Classification.python_data_managing.data_mode_access>`
        - gapfilled
        - choose which data can be accessed in custom features
        - str
        - False
        - :ref:`data_mode_access <desc_I2Classification.python_data_managing.data_mode_access>`


          .. _I2Classification.python_data_managing.fill_missing_dates:
      * - :ref:`fill_missing_dates <desc_I2Classification.python_data_managing.fill_missing_dates>`
        - False
        - fill raw data with no data if dates are missing
        - bool
        - False
        - :ref:`fill_missing_dates <desc_I2Classification.python_data_managing.fill_missing_dates>`


          .. _I2Classification.python_data_managing.max_nn_inference_size:
      * - :ref:`max_nn_inference_size <desc_I2Classification.python_data_managing.max_nn_inference_size>`
        - None
        - maximum batch inference size
        - int
        - False
        - :ref:`max_nn_inference_size <desc_I2Classification.python_data_managing.max_nn_inference_size>`


          .. _I2Classification.python_data_managing.number_of_chunks:
      * - number_of_chunks
        - 50
        - the expected number of chunks
        - int
        - False
        - number_of_chunks


          .. _I2Classification.python_data_managing.padding_size_x:
      * - padding_size_x
        - 0
        - The padding for chunk
        - int
        - False
        - padding_size_x


          .. _I2Classification.python_data_managing.padding_size_y:
      * - padding_size_y
        - 0
        - The padding for chunk
        - int
        - False
        - padding_size_y





Notes
=====

.. _desc_I2Classification.python_data_managing.data_mode_access:

:ref:`data_mode_access <I2Classification.python_data_managing.data_mode_access>`
--------------------------------------------------------------------------------
Three values are allowed:

- gapfilled: give access only the gapfilled data

- raw: gives access only the original raw data

- both: provides access to both data

..Notes:: Data are spatialy resampled, these parameters concern only temporal interpolation

.. _desc_I2Classification.python_data_managing.fill_missing_dates:

:ref:`fill_missing_dates <I2Classification.python_data_managing.fill_missing_dates>`
------------------------------------------------------------------------------------
If raw data access is enabled, this option considers all unique dates for all tiles and identify which dates are missing for each tile. A missing date is filled using a no data constant value.Cloud or saturation are not corrected, but masks are provided Masks contain three value: 0 for valid data, 1 for cloudy or saturated pixels, 2 for a missing date

.. _desc_I2Classification.python_data_managing.max_nn_inference_size:

:ref:`max_nn_inference_size <I2Classification.python_data_managing.max_nn_inference_size>`
------------------------------------------------------------------------------------------
Involved if a neural network inference is performed. If not set (None), the inference size will be the same as the one used during the learning stage

.. _I2Classification.scikit_models_parameters:

scikit_models_parameters
************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.scikit_models_parameters.cross_validation_folds:
      * - cross_validation_folds
        - 5
        - the number of k-folds
        - int
        - False
        - cross_validation_folds


          .. _I2Classification.scikit_models_parameters.cross_validation_grouped:
      * - cross_validation_grouped
        - False
        - 
        - bool
        - False
        - cross_validation_grouped


          .. _I2Classification.scikit_models_parameters.cross_validation_parameters:
      * - cross_validation_parameters
        - {}
        - 
        - dict
        - False
        - cross_validation_parameters


          .. _I2Classification.scikit_models_parameters.keyword_arguments:
      * - :ref:`keyword_arguments <desc_I2Classification.scikit_models_parameters.keyword_arguments>`
        - {}
        - keyword arguments to be passed to model
        - dict
        - False
        - :ref:`keyword_arguments <desc_I2Classification.scikit_models_parameters.keyword_arguments>`


          .. _I2Classification.scikit_models_parameters.model_type:
      * - :ref:`model_type <desc_I2Classification.scikit_models_parameters.model_type>`
        - None
        - machine learning algorthm’s name
        - str
        - False
        - :ref:`model_type <desc_I2Classification.scikit_models_parameters.model_type>`


          .. _I2Classification.scikit_models_parameters.standardization:
      * - standardization
        - True
        - 
        - bool
        - False
        - standardization





Notes
=====

.. _desc_I2Classification.scikit_models_parameters.keyword_arguments:

:ref:`keyword_arguments <I2Classification.scikit_models_parameters.keyword_arguments>`
--------------------------------------------------------------------------------------
keyword arguments to be passed to model

.. _desc_I2Classification.scikit_models_parameters.model_type:

:ref:`model_type <I2Classification.scikit_models_parameters.model_type>`
------------------------------------------------------------------------
Models comming from scikit-learn are use if scikit_models_parameters.model_type is different from None. More informations about how to use scikit-learn is available at iota2 and scikit-learn machine learning algorithms.

.. _I2Classification.sensors_data_interpolation:

sensors_data_interpolation
**************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.sensors_data_interpolation.auto_date:
      * - :ref:`auto_date <desc_I2Classification.sensors_data_interpolation.auto_date>`
        - True
        - Enable the use of `start_date` and `end_date`
        - bool
        - False
        - :ref:`auto_date <desc_I2Classification.sensors_data_interpolation.auto_date>`


          .. _I2Classification.sensors_data_interpolation.use_additional_features:
      * - use_additional_features
        - False
        - enable the use of additional features
        - bool
        - False
        - use_additional_features


          .. _I2Classification.sensors_data_interpolation.use_gapfilling:
      * - use_gapfilling
        - True
        - enable the use of gapfilling (clouds/temporal interpolation)
        - bool
        - False
        - use_gapfilling


          .. _I2Classification.sensors_data_interpolation.write_outputs:
      * - :ref:`write_outputs <desc_I2Classification.sensors_data_interpolation.write_outputs>`
        - False
        - write temporary files
        - bool
        - False
        - :ref:`write_outputs <desc_I2Classification.sensors_data_interpolation.write_outputs>`





Notes
=====

.. _desc_I2Classification.sensors_data_interpolation.auto_date:

:ref:`auto_date <I2Classification.sensors_data_interpolation.auto_date>`
------------------------------------------------------------------------
If True, iota2 will automatically guess the first and the last interpolation date. Else, `start_date` and `end_date` of each sensors will be used

.. _desc_I2Classification.sensors_data_interpolation.write_outputs:

:ref:`write_outputs <I2Classification.sensors_data_interpolation.write_outputs>`
--------------------------------------------------------------------------------
Write the time series before and after gapfilling, the mask time series, and also the feature time series. This option required a large amount of free disk space.

.. _I2Classification.slurm:

slurm
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.slurm.account:
      * - :ref:`account <desc_I2Classification.slurm.account>`
        - None
        - Feed the sbatch parameter 'account'
        - str
        - False
        - :ref:`account <desc_I2Classification.slurm.account>`





Notes
=====

.. _desc_I2Classification.slurm.account:

:ref:`account <I2Classification.slurm.account>`
-----------------------------------------------
The section 'slurm' is only available once the `Slurm <https://slurm.schedmd.com/documentation.html>`_ orchestrator is involved in jobs submission.

.. _I2Classification.task_retry_limits:

task_retry_limits
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.task_retry_limits.allowed_retry:
      * - allowed_retry
        - 0
        - allow dask to retry a failed job N times
        - int
        - False
        - allowed_retry


          .. _I2Classification.task_retry_limits.maximum_cpu:
      * - :ref:`maximum_cpu <desc_I2Classification.task_retry_limits.maximum_cpu>`
        - 4
        - the maximum number of CPU available
        - int
        - False
        - :ref:`maximum_cpu <desc_I2Classification.task_retry_limits.maximum_cpu>`


          .. _I2Classification.task_retry_limits.maximum_ram:
      * - :ref:`maximum_ram <desc_I2Classification.task_retry_limits.maximum_ram>`
        - 16.0
        - the maximum amount of RAM available. (gB)
        - float
        - False
        - :ref:`maximum_ram <desc_I2Classification.task_retry_limits.maximum_ram>`





Notes
=====

.. _desc_I2Classification.task_retry_limits.maximum_cpu:

:ref:`maximum_cpu <I2Classification.task_retry_limits.maximum_cpu>`
-------------------------------------------------------------------
the amount of cpu will be doubled if the task is killed due to ram overconsumption until maximum_cpu or allowed_retry are reach

.. _desc_I2Classification.task_retry_limits.maximum_ram:

:ref:`maximum_ram <I2Classification.task_retry_limits.maximum_ram>`
-------------------------------------------------------------------
the amount of RAM will be doubled if the task is killed due to ram overconsumption until maximum_ram or allowed_retry are reach

.. _I2Classification.userFeat:

userFeat
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _I2Classification.userFeat.arbo:
      * - arbo
        - /*
        - input folder hierarchy
        - str
        - False
        - arbo


          .. _I2Classification.userFeat.patterns:
      * - patterns
        - ALT,ASP,SLP
        - key name for detect the input images
        - str
        - False
        - patterns





