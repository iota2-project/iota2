All configuration parameters
############################

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Name

      * - account
        - None
        - Feed the sbatch parameter 'account'
        - str
        - account

      * - acor_feat
        - False
        - Apply atmospherically corrected features
        - bool
        - acor_feat

      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - additional_features

      * - allowed_retry
        - 0
        - allow dask to retry a failed job N times
        - int
        - allowed_retry

      * - arbo
        - /*
        - input folder hierarchy
        - str
        - arbo

      * - auto_date
        - True
        - Enable the use of `start_date` and `end_date`
        - bool
        - auto_date

      * - boundary_buffer
        - None
        - List of boundary buffer size
        - list
        - boundary_buffer

      * - boundary_comparison_mode
        - False
        - Enable classification comparison
        - bool
        - boundary_comparison_mode

      * - boundary_exterior_buffer_size
        - 0
        - Buffer size outside the region
        - int
        - boundary_exterior_buffer_size

      * - boundary_fusion_epsilon
        - 0.0
        - Threshold to avoid weights equals to zero
        - float
        - boundary_fusion_epsilon

      * - boundary_interior_buffer_size
        - 0
        - Buffer size inside the region
        - int
        - boundary_interior_buffer_size

      * - buffer_size
        - None
        - define the working size batch in number of pixels
        - int
        - buffer_size

      * - builders_class_name
        - ['I2Classification']
        - The name of the class defining the builder
        - list
        - builders_class_name

      * - builders_paths
        - /path/to/iota2/sources
        - The path to user builders
        - str
        - builders_paths

      * - check_inputs
        - True
        - Enable the inputs verification
        - bool
        - check_inputs

      * - chunk_size_mode
        - split_number
        - The chunk split mode, currently the choice is 'split_number'
        - str
        - chunk_size_mode

      * - chunk_size_x
        - 50
        - number of cols for one chunk
        - int
        - chunk_size_x

      * - chunk_size_y
        - 50
        - number of rows for one chunk
        - int
        - chunk_size_y

      * - classif_mode
        - separate
        - 'separate' or 'fusion'
        - str
        - classif_mode

      * - classifier
        - None
        - otb classification algorithm
        - str
        - classifier

      * - cloud_threshold
        - 0
        - Threshold to consider that a pixel is valid
        - int
        - cloud_threshold

      * - color_table
        - None
        - Absolute path to the file that links the classes and their colours
        - str
        - color_table

      * - compression_algorithm
        - ZSTD
        - Set the gdal compression algorithm to use: NONE, LZW, ZSTD (default).All rasters write with OTB will be compress with the chosen algorithm.
        - str
        - compression_algorithm

      * - compression_predictor
        - 2
        - Set the predictor for LZW and ZSTD compression: 1 (no predictor), 2 (horizontal differencing, default)
        - int
        - compression_predictor

      * - concat_mode
        - True
        - enable the use of all features
        - bool
        - concat_mode

      * - copy_input
        - True
        - use spectral bands as features
        - bool
        - copy_input

      * - cross_validation_folds
        - 5
        - the number of k-folds
        - int
        - cross_validation_folds

      * - cross_validation_grouped
        - False
        - 
        - bool
        - cross_validation_grouped

      * - cross_validation_parameters
        - {}
        - 
        - dict
        - cross_validation_parameters

      * - data_field
        - None
        - Field name indicating classes labels in `ground_thruth`
        - str
        - data_field

      * - data_mode_access
        - gapfilled
        - choose which data can be accessed in custom features
        - str
        - data_mode_access

      * - deep_learning_parameters
        - {}
        - deep learning parameter description is available :doc:`here <deep_learning>`
        - dict
        - deep_learning_parameters

      * - dempstershafer_mob
        - precision
        - Choose the dempster shafer mass of belief estimation method
        - str
        - dempstershafer_mob

      * - enable_boundary_fusion
        - False
        - Enable the boundary fusion
        - bool
        - enable_boundary_fusion

      * - enable_probability_map
        - False
        - Produce the probability map
        - bool
        - enable_probability_map

      * - enable_sensor_gapfilling
        - False
        - Enable or disable gapfilling for landsat 8 and 9 IR data
        - bool
        - enable_sensor_gapfilling

      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - end_date

      * - exogeneous_data
        - None
        - Path to a Geotiff file containing additional data to be used in external features
        - str
        - exogeneous_data

      * - external_features_flag
        - False
        - enable the external features mode
        - bool
        - external_features_flag

      * - extract_bands
        - False
        - 
        - bool
        - extract_bands

      * - features
        - ['NDVI', 'NDWI', 'Brightness']
        - List of additional features computed
        - list
        - features

      * - features_from_raw_dates
        - False
        - learn model from raw sensor's date (no interpolations)
        - bool
        - features_from_raw_dates

      * - features_path
        - None
        - input directory containing features as rasters
        - str
        - features_path

      * - fill_missing_dates
        - False
        - fill raw data with no data if dates are missing
        - bool
        - fill_missing_dates

      * - first_step
        - None
        - The step group name indicating where the chain start
        - str
        - first_step

      * - force_standard_labels
        - False
        - Standardize labels for feature extraction
        - bool
        - force_standard_labels

      * - from_rasterdb_resampling_method
        - nn
        - output features type choice among `gdalwarp.html#cmdoption-gdalwarp-r <https://www.orfeo-toolbox.org/CookBook/Applications/app_Superimpose.html>`_. Enabled if chain.rasters_grid_path is set
        - str
        - from_rasterdb_resampling_method

      * - from_vectordb_resampling_method
        - near
        - output features type choice among `gdalwarp.html#cmdoption-gdalwarp-r <https://gdal.org/programs/gdalwarp.html#cmdoption-gdalwarp-r>`_. Enabled if chain.grid is set
        - str
        - from_vectordb_resampling_method

      * - full_learn_segment
        - False
        - enable the use of entire segment for learning
        - bool
        - full_learn_segment

      * - function
        - None
        - Predict function name
        - str
        - function

      * - functions
        - None
        - function list to be used to compute features
        - str/list
        - functions

      * - fusion_options
        -  -nodatalabel 0 -method majorityvoting
        - OTB FusionOfClassification options for voting method involved if classif_mode is set to 'fusion'
        - str
        - fusion_options

      * - fusionof_all_samples_validation
        - False
        - Enable the use of all reference data to evaluate the fusion raster
        - bool
        - fusionof_all_samples_validation

      * - fusionofclassification_all_samples_validation
        - False
        - Enable the use of all reference data to validate the classification merge
        - bool
        - fusionofclassification_all_samples_validation

      * - generate_final_probability_map
        - False
        - Enable the mosaicing of probabilities maps.
        - bool
        - generate_final_probability_map

      * - grid
        - None
        - input grid to fit
        - str
        - grid

      * - gridsize
        - None
        - number of lines and columns of the serialization process
        - int
        - gridsize

      * - ground_truth
        - None
        - Absolute path to reference data
        - str
        - ground_truth

      * - inland
        - None
        - inland water limit shapefile
        - str
        - inland

      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - keep_bands

      * - keep_duplicates
        - True
        - use 'rel_refl' can generate duplicated feature (ie: NDVI), set to False remove these duplicated features
        - bool
        - keep_duplicates

      * - keep_runs_results
        - True
        - 
        - bool
        - keep_runs_results

      * - keyword_arguments
        - {}
        - keyword arguments to be passed to model
        - dict
        - keyword_arguments

      * - l5_path_old
        - None
        - Absolute path to Landsat-5 images coming from old THEIA format (D*H*)
        - str
        - l5_path_old

      * - l8_path
        - None
        - Absolute path to Landsat-8 images comingfrom new tiled THEIA data
        - str
        - l8_path

      * - l8_path_old
        - None
        - Absolute path to Landsat-8 images coming from old THEIA format (D*H*)
        - str
        - l8_path_old

      * - l8_usgs_infrared_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - l8_usgs_infrared_path

      * - l8_usgs_optical_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - l8_usgs_optical_path

      * - l8_usgs_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - l8_usgs_path

      * - l8_usgs_thermal_path
        - None
        - Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`
        - str
        - l8_usgs_thermal_path

      * - last_step
        - None
        - The step group name indicating where the chain ends
        - str
        - last_step

      * - learning_samples_extension
        - sqlite
        - learning samples file extension, possible values are 'sqlite' and 'csv'
        - str
        - learning_samples_extension

      * - list_tile
        - None
        - List of tile to process, separated by space
        - str
        - list_tile

      * - logger_level
        - INFO
        - Set the logger level: NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
        - str
        - logger_level

      * - max_nn_inference_size
        - None
        - maximum batch inference size
        - int
        - max_nn_inference_size

      * - maximum_cpu
        - 4
        - the maximum number of CPU available
        - int
        - maximum_cpu

      * - maximum_ram
        - 16.0
        - the maximum amount of RAM available. (gB)
        - float
        - maximum_ram

      * - merge_final_classifications
        - False
        - Enable the fusion of classifications mode, merging all run in a unique result.
        - bool
        - merge_final_classifications

      * - merge_final_classifications_indecidedlabel
        - 255
        - Indicate the label for indecision case during fusion
        - int
        - merge_final_classifications_indecidedlabel

      * - merge_final_classifications_method
        - majorityvoting
        - Indicate the fusion of classification method: 'majorityvoting' or 'dempstershafer'
        - str
        - merge_final_classifications_method

      * - merge_final_classifications_ratio
        - 0.1
        - Percentage of samples to use in order to evaluate the fusion raster
        - float
        - merge_final_classifications_ratio

      * - merge_run
        - False
        - Enable the fusion of regression mode, merging all run in a unique result.
        - bool
        - merge_run

      * - merge_run_method
        - mean
        - Indicate the fusion of regression method: 'mean' or 'median'
        - str
        - merge_run_method

      * - merge_run_ratio
        - 0.1
        - Percentage of samples to use in order to evaluate the fusion raster
        - float
        - merge_run_ratio

      * - minimum_required_dates
        - 2
        - required minimum number of available dates for each sensor
        - int
        - minimum_required_dates

      * - mode
        - None
        - Algorythm nature (classification or regression)
        - str
        - mode

      * - mode_outside_regionsplit
        - 0.1
        - Set the threshold for split huge model
        - float
        - mode_outside_regionsplit

      * - model
        - None
        - Serialized object containing the model
        - str
        - model

      * - model_type
        - None
        - machine learning algorthm’s name
        - str
        - model_type

      * - module
        - /path/to/iota2/sources
        - absolute path for user source code
        - str
        - module

      * - no_data_value
        - -10000
        - value considered as no_data in features map mosaic ('I2FeaturesMap' builder name)
        - int
        - no_data_value

      * - no_label_management
        - maxConfidence
        - Method for choosing a label in case of fusion
        - str
        - no_label_management

      * - nomenclature
        - None
        - configuration file which describe nomenclature
        - configuration file which describe nomenclature
        - nomenclature

      * - nomenclature_path
        - None
        - Absolute path to the nomenclature description file
        - str
        - nomenclature_path

      * - number_of_chunks
        - 50
        - the expected number of chunks
        - int
        - number_of_chunks

      * - obia_segmentation_path
        - None
        - filename for input segmentation
        - str
        - obia_segmentation_path

      * - otb_classifier_options
        - None
        - OTB option for classifier.If None, the OTB default values are used
        - dict
        - otb_classifier_options

      * - out_worldclim_dtype
        - float32
        - output worlclim data type, ie : 'uint16', 'float32'.
        - np.dtype
        - out_worldclim_dtype

      * - out_worldclim_rescale_range
        - None
        - rescale worldclim data between 0 and max(np.dtype) at run time for RAM usage purpose.
        - np.dtype
        - out_worldclim_rescale_range

      * - output_features_pix_type
        - float
        - output features type choice among uint8/uint16/int16/uint32/int32/float/double.
        - str
        - output_features_pix_type

      * - output_name
        - None
        - temporary chunks are written using this name as prefix
        - str
        - output_name

      * - output_path
        - None
        - Absolute path to the output directory
        - str
        - output_path

      * - output_statistics
        - True
        - output_statistics
        - bool
        - output_statistics

      * - padding_size_x
        - 0
        - The padding for chunk
        - int
        - padding_size_x

      * - padding_size_y
        - 0
        - The padding for chunk
        - int
        - padding_size_y

      * - patterns
        - ALT,ASP,SLP
        - key name for detect the input images
        - str
        - patterns

      * - proj
        - None
        - The projection wanted. Format EPSG:XXXX is mandatory
        - str
        - proj

      * - random_seed
        - None
        - Fix the random seed for random split of reference data
        - int
        - random_seed

      * - rasters_grid_path
        - None
        - input grid to fit
        - str
        - rasters_grid_path

      * - ratio
        - 0.5
        - Should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the train split.
        - float
        - ratio

      * - region_field
        - region
        - The column name for region indicator in`region_path` file
        - str
        - region_field

      * - region_path
        - None
        - Absolute path to a region vector file
        - str
        - region_path

      * - region_priority
        - None
        - define an order for region intersection
        - list
        - region_priority

      * - rel_refl
        - False
        - compute relative reflectances by the red band
        - bool
        - rel_refl

      * - remove_output_path
        - True
        - Before the launch of iota2, remove the content of `output_path`
        - bool
        - remove_output_path

      * - resampling_bco_radius
        - 2
        - otb radius for bicubic interpolation.
        - int
        - resampling_bco_radius

      * - rssize
        - 20
        - Resampling size of input classification raster (projection unit)
        - int
        - rssize

      * - runs
        - 1
        - Number of independent runs processed
        - int
        - runs

      * - s1_dir
        - None
        - Sentinel1 data directory
        - str
        - s1_dir

      * - s1_path
        - None
        - Absolute path to Sentinel-1 configuration file
        - str
        - s1_path

      * - s2_l3a_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - s2_l3a_output_path

      * - s2_l3a_path
        - None
        - Absolute path to Sentinel-2 L3A images (THEIA format)
        - str
        - s2_l3a_path

      * - s2_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - s2_output_path

      * - s2_path
        - None
        - Absolute path to Sentinel-2 images (THEIA format)
        - str
        - s2_path

      * - s2_s2c_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - s2_s2c_output_path

      * - s2_s2c_path
        - None
        - Absolute path to Sentinel-2 images (Sen2Cor format)
        - str
        - s2_s2c_path

      * - sample_augmentation
        - {'activate': False, 'bins': 10}
        - OTB parameters for sample augmentation
        - dict
        - sample_augmentation

      * - sample_management
        - None
        - Absolute path to a CSV file containing samples transfert strategies
        - str
        - sample_management

      * - sample_selection
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - sample_selection

      * - sample_validation
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - sample_validation

      * - sampling_validation
        - False
        - Enable sampling validation
        - bool
        - sampling_validation

      * - spatial_resolution
        - []
        - Output spatial resolution
        - list or scalar
        - spatial_resolution

      * - split_ground_truth
        - True
        - Enable the split of reference data
        - bool
        - split_ground_truth

      * - srtm_path
        - None
        - Path to a directory containing srtm data
        - str
        - srtm_path

      * - standardization
        - True
        - 
        - bool
        - standardization

      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - start_date

      * - stats_used
        - ['mean']
        - list of stats used for train and classification
        - list
        - stats_used

      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - temporal_resolution

      * - tile_field
        - None
        - column name in 'grid' containing tile's name.
        - str
        - tile_field

      * - umc1
        - None
        - MMU for the first regularization
        - int
        - umc1

      * - umc2
        - None
        - MMU for the second regularization
        - int
        - umc2

      * - use_additional_features
        - False
        - enable the use of additional features
        - bool
        - use_additional_features

      * - use_gapfilling
        - True
        - enable the use of gapfilling (clouds/temporal interpolation)
        - bool
        - use_gapfilling

      * - user_feat_path
        - None
        - Absolute path to the user's features path
        - str
        - user_feat_path

      * - validity_threshold
        - 1
        - threshold above which a training pixel is considered valid
        - int
        - validity_threshold

      * - worldclim_path
        - None
        - Path to a directory containing world clim data
        - str
        - worldclim_path

      * - write_outputs
        - False
        - write temporary files
        - bool
        - write_outputs

      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - write_reproject_resampled_input_dates_stack


