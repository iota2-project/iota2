#!/bin/sh

module load conda

# if an env is given, use it, otherwise clone the env created by the CI
if [ -n "$1" ]; then
    CONDA_ENV_PATH="$1"
else
    CONDA_ENV_PATH="/work/THEIA/CES/oso/CD/conda_env/iota2_annual_test_env"
    REF_ENV="/work/THEIA/CES/oso/CD/conda_env/iota2_env"
    conda create -p $CONDA_ENV_PATH --clone $REF_ENV
fi

conda activate "$CONDA_ENV_PATH"
rm -rf /work/THEIA/CES/oso/iota2_annual_test/fake_test_output
Iota2Cluster.py -config /work/THEIA/CES/oso/iota2_annual_test/config_annual_test.cfg -config_ressources /work/THEIA/CES/oso/iota2_annual_test/ressources.cfg -scheduler_type Slurm -nb_parallel_tasks 5 -only_summary
conda deactivate
