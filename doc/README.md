### Procedure to generate IOTA²'s documentation


Note: the documentation is built everyday by [this cron job](https://framagit.org/iota2-project/iota2-devtools/-/blob/main/crontab/build_doc.sh).  
Note: all the build requirements are available in the iota2 conda package.

The following procedure, help you step by step to compile offline documentation.

1 - First you have to install the iota2 environment by following all steps (1,2 and 3 bis):


http://osr-cesbio.ups-tlse.fr/oso/donneeswww_TheiaOSO/iota2_documentation/master/HowToGetIOTA2.html#how-to-get-iota2

In case of network error use:

http://lannister.ups-tlse.fr/oso/donneeswww_TheiaOSO/iota2_documentation/master/HowToGetIOTA2.html#how-to-install-iota2
2 - Get iota2 sources 
```bash
cd /the/folder/to/copy
# clone the iota2 repository
git clone https://framagit.org/iota2-project/iota2.git -b develop

# documentation will be generate in MyIOTA2Doc
mkdir /the/folder/to/copy/iota2/doc/MyIOTA2Doc
cd /the/folder/to/copy/iota2/doc/MyIOTA2Doc
sphinx-quickstart
```

3 - A list of questions will be asked. Answer the following:
```bash
Separate source and build directories (y/n) [n]: y
Project name: IOTA²
Author name(s):MyName
Project release []:
Project language [en]:

```

4 - Replace the directory "MyIOTA2Doc/source" by the git one
```bash
cp -r ../source/ ./
```

5 - Launch the html generation
```bash
make html
```

6 - Then you can vizualize the doc
```bash
firefox build/html/index.html
```
