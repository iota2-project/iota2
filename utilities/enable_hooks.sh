#!/usr/bin/env python3

import os

src_dir = os.path.dirname(os.path.realpath(__file__))
os.system("pre-commit install")

# push hook
push_hook = os.path.join(src_dir, "..", ".git", "hooks", "pre-push")
if os.path.islink(push_hook):
    os.unlink(push_hook)
os.symlink(os.path.join(src_dir, "pre-push"), push_hook)
