workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
    - if: $CI_COMMIT_BRANCH


stages:
  - sentinel
  - build
  - init
  - quality
  - deploy

# Default configuration for all jobs
default:
  tags:
      - iota2
  timeout: 120 minutes

wake-up-runner:
  stage: sentinel
  tags:
    - i2_sentinel
  script:
    - sbatch $I2_RUNNER
  rules:
    - if: $CI_COMMIT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "schedule"

build-dev-pkg:
  stage: build
  before_script:
    - export no_proxy=$NO_PROXY
    - export http_proxy=$HTTP_PROXY
    - export https_proxy=$HTTP_PROXY
    - module load conda/24.3.0
    - conda remove -p $ENV_LOCATION/iota2_build_env --all -y
    - conda create -p $ENV_LOCATION/iota2_build_env python=3.11 -y
    - conda activate $ENV_LOCATION/iota2_build_env
    - conda install conda-build -y
    - conda install mamba=1.5.0 -y
    - conda install anaconda-client -y
  script:
    # don't check certificates so conda can git clone sensorsio
    - git config --global http.sslVerify false
    - conda config --set default_threads 6
    # - conda config --set solver classic
    - conda config --set solver libmamba
    # build the package
    - cd $CI_PROJECT_DIR/install/recipes
    - conda build purge-all
    - date
    - rm -rf $PKGS_LOCATION/latest
    - mkdir -p $PKGS_LOCATION/latest
    - conda build iota2 -c nvidia -c pytorch -c conda-forge --no-test --output-folder $PKGS_LOCATION/latest
    - date

  rules:
    # Only run if the pipeline is scheduled
    - if: $CI_PIPELINE_SOURCE == "schedule"


build-env:
  stage: init
  dependencies:
    - build-dev-pkg
  script:
    - export no_proxy=$NO_PROXY
    - export http_proxy=$HTTP_PROXY
    - export https_proxy=$HTTP_PROXY
    - module load conda/24.3.0
    - conda create -p $ENV_LOCATION/iota2_env python=3.11 -y
    - conda activate $ENV_LOCATION/iota2_env
    - conda install mamba -c conda-forge -y
    - date
    - var=$( [ $CI_PIPELINE_SOURCE == "schedule" ] && echo "$PKGS_LOCATION/stable" || echo "iota2-cd" )
    - echo $var
    - mamba install iota2_develop -c $var -c nvidia -c pytorch -c conda-forge -c defaults -y
    - mamba install pytest-rerunfailures -c conda-forge -y
    - mamba install flake8=6.0.0 isort=5.11.5 black=22.10.0
    - date
    - conda deactivate
    - conda activate $ENV_LOCATION/iota2_env
    - Iota2.py -h
    - export IOTA2DIR=$CI_PROJECT_DIR/
  timeout: 120 minutes
  rules:
    - if: $CI_COMMIT_BRANCH

pytest:
  stage: quality
  dependencies:
    - build-env
  needs:
    - build-env
  before_script:
    - export no_proxy=$NO_PROXY
    - export http_proxy=$HTTP_PROXY
    - export https_proxy=$HTTP_PROXY

  script:
    - module load conda/24.3.0
    - conda activate $ENV_LOCATION/iota2_env
    - export IOTA2DIR=$CI_PROJECT_DIR/
    - date
    - pytest iota2/tests/ --cov=iota2 --cov-report=xml:.coverage-reports/coverage.xml --cov-report=term -n 6 --reruns 3 --reruns-delay 3
    - date
  rules:
    - if: $CI_COMMIT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "schedule"

linter:
  stage: quality
  before_script:
    - export no_proxy=$NO_PROXY
    - export http_proxy=$HTTP_PROXY
    - export https_proxy=$HTTP_PROXY
    - module load conda/24.3.0
    - conda activate $ENV_LOCATION/iota2_env
    - pip install pylint-gitlab
  script:
    - python -m flake8 --count --statistics --tee iota2
    - pylint --exit-zero --output-format=text --rcfile $CI_PROJECT_DIR/iota2/.pylintrc iota2
#    - pylint --exit-zero --output-format=pylint_gitlab.GitlabCodeClimateReporter --rcfile $CI_PROJECT_DIR/iota2/.pylintrc iota2 > codeclimate.json
  rules:
    - if: $CI_COMMIT_BRANCH

formater:
  stage: quality
  before_script:
  - module load conda/24.3.0
  - conda activate $ENV_LOCATION/iota2_env
  script:
    - python -m black --check --diff iota2
  rules:
    - if: $CI_COMMIT_BRANCH

anaconda-upload:
  stage: deploy
  dependencies:
    - pytest
  needs:
    - pytest
  before_script:
    - export no_proxy=$NO_PROXY
    - export http_proxy=$HTTP_PROXY
    - export https_proxy=$HTTP_PROXY

  script:
    - cp -r $PKGS_LOCATION/latest/* $PKGS_LOCATION/stable
    - module load conda/24.3.0
    - conda activate $ENV_LOCATION/iota2_build_env
    - date
    - anaconda -t $CONDA_UPLOAD_TOKEN upload -u iota2-cd $PKGS_LOCATION/latest/linux-64/*.tar.bz2 --force
    - anaconda -t $CONDA_UPLOAD_TOKEN upload -u iota2-cd $PKGS_LOCATION/latest/noarch/*.tar.bz2 --force
    - date
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
