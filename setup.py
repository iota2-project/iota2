"""
Setup script for the iota2 project.

This script is used to configure the iota2 Python package for distribution.
It uses setuptools to handle packaging, including finding all packages
and including additional data files such as configuration files.

Attributes:
-----------
- name (str): The name of the package ('iota2').
- version (str): The version of the package (currently set to '0').
- packages (list): All Python packages to be included in the distribution, found using
  `setuptools.find_packages()`.
- url (str): URL of the project repository ('https://framagit.org/iota2-project/iota2').
- license (str): License under which the project is distributed
  (GNU Affero General Public License v3.0).
- author (str): The name of the author or development team ('iota2 development team').
- package_data (dict): A dictionary that specifies the files to include in the package.
  Here, all `*.cfg` files in any directory are included.
- include_package_data (bool): Specifies whether to include non-code files
  (like configuration files) in the package (set to `True`).
- author_email (str): The email address of the author (left empty).
- description (str): A short description of the package. In this case, it describes
  iota2 as an "Infrastructure pour l'Occupation des sols par Traitement Automatique
  Incorporant les Orfeo Toolbox Applications".
"""

import setuptools

setuptools.setup(
    name="iota2",
    version="0",
    packages=setuptools.find_packages(),
    url="https://framagit.org/iota2-project/iota2",
    license="GNU Affero General Public License v3.0",
    author="iota2 development team",
    package_data={
        "": ["*.cfg"],
    },
    include_package_data=True,
    author_email="",
    description=(
        "Infrastructure pour l'Occupation des sols par Traitement "
        "Automatique Incorporant les Orfeo Toolbox Applications"
    ),
)
