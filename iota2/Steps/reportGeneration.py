#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Validation import GenResults as GR
from iota2.configuration_files import read_config_file as rcf
from iota2.VectorTools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class reportGeneration(IOTA2Step.Step):
    resources_block_name = "reportGen"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(reportGeneration, self).__init__(cfg, cfg_resources_file,
                                               self.resources_block_name)

        # step variables
        self.workingDirectory = workingDirectory
        output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        nomenclature = rcf.read_config_file(self.cfg).getParam(
            'chain', 'nomenclature_path')
        reference_data = rcf.read_config_file(self.cfg).getParam(
            "chain", "ground_truth")
        reference_data_field = rcf.read_config_file(self.cfg).getParam(
            "chain", "data_field")
        user_labels_to_i2_labels = get_re_encoding_labels_dic(
            reference_data, reference_data_field)
        i2_labels_to_user_labels = {
            v: k
            for k, v in user_labels_to_i2_labels.items()
        }
        task = self.i2_task(task_name=f"final_report",
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters={
                                "f":
                                GR.genResults,
                                "pathRes":
                                os.path.join(output_path, "final"),
                                "pathNom":
                                nomenclature,
                                "labels_conversion_table":
                                i2_labels_to_user_labels,
                            },
                            task_resources=self.get_resources())
        self.add_task_to_i2_processing_graph(
            task,
            task_group="final_report",
            task_sub_group="final_report",
            task_dep_dico={"confusion_merge": ["confusion_merge"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Generate final report")
        return description
