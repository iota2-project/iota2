#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging
from typing import Tuple

from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
from iota2.simplification import manageRegularization as mr
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")


class mergeRegularization(IOTA2Step.Step):
    resources_block_name = "mergeregularisation"

    def __init__(self,
                 cfg,
                 cfg_resources_file,
                 stepname=None,
                 output=None,
                 umc=None,
                 resample=None,
                 water=None,
                 workingDirectory=None):
        # heritage init
        super(mergeRegularization, self).__init__(cfg, cfg_resources_file,
                                                  self.resources_block_name)
        # step variables
        if stepname:
            self.step_name = stepname

        self.RAM = 1024.0 * get_RAM(self.get_resources()["ram"])
        self.CPU = self.get_resources()["cpu"]
        self.workingDirectory = workingDirectory
        self.outputPath = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.resample = resample
        self.water = water
        self.umc = umc
        self.output = output
        self.tmpdir = os.path.join(self.outputPath, "simplification", "tmp")
        nomenclature = rcf.read_config_file(self.cfg).getParam(
            "simplification", "nomenclature")
        nb_reg_rules = len(mr.getMaskRegularisation(nomenclature))
        output, tmpdir = self.get_output_paths()

        expected_rasters = [
            os.path.join(self.tmpdir, f"mask_{rule_num}.tif")
            for rule_num in range(nb_reg_rules)
        ]

        task = self.i2_task(task_name=stepname,
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters={
                                "f": mr.mergeRegularization,
                                "path": tmpdir,
                                "rasters": expected_rasters,
                                "threshold": self.umc,
                                "output": output,
                                "ram": self.RAM,
                                "resample": self.resample,
                                "water": self.water,
                                "working_dir": self.workingDirectory,
                            },
                            task_resources=self.get_resources())
        self.add_task_to_i2_processing_graph(
            task,
            task_group="merge_regul",
            task_sub_group=f"merge_regul",
            task_dep_dico={
                "regul":
                [f"regul_{rule_num}" for rule_num in range(nb_reg_rules)]
            })

    def get_output_paths(self) -> Tuple[str, str]:
        """
        """
        tmpdir = os.path.join(self.outputPath, 'simplification', 'tmp')

        if not self.output:
            output = os.path.join(tmpdir, 'regul1.tif')
        else:
            output = self.output
        return output, tmpdir

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = (
            "Majority regularisation of adaptive-regularized raster")
        return description
