#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" The prepare segmentation for obia step"""
import os
import logging

from iota2.Steps import IOTA2Step

from iota2.Iota2Cluster import get_RAM
from iota2.configuration_files import read_config_file as rcf
from iota2.Segmentation import prepare_segmentation_obia

LOGGER = logging.getLogger("distributed.worker")


class prepare_obia_seg(IOTA2Step.Step):
    resources_block_name = "prepare_obia_seg"

    def __init__(self, cfg, cfg_resources_file, working_directory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = working_directory
        self.RAM = 1024.0 * get_RAM(self.get_resources()["ram"])
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.region_path = rcf.read_config_file(self.cfg).getParam(
            "chain", 'region_path')
        self.region_field = rcf.read_config_file(self.cfg).getParam(
            "chain", 'region_field')

        for tile in self.tiles:
            in_seg_ori = rcf.read_config_file(self.cfg).getParam(
                "obia", "obia_segmentation_path")
            if in_seg_ori is None:
                in_seg = os.path.join(self.output_path, "features", tile,
                                      "tmp", f"SLIC_{tile}.tif")
            else:
                in_seg = in_seg_ori
            parameters = {
                "f": prepare_segmentation_obia.prepare_initial_segmentation,
                "in_seg": in_seg,
                "tile": tile,
                "iota2_directory": self.output_path,
                "region_path": self.region_path,
                "working_directory": self.working_directory
            }
            if self.region_field is not None:
                parameters["region_field"] = self.region_field
            if in_seg_ori is None:
                parameters["is_slic"] = True
            task = self.i2_task(task_name=f"prepare_seg_{tile}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters=parameters,
                                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_tasks",
                task_sub_group=tile,
                task_dep_dico={"tile_tasks": [tile]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Vectorize segmentation by tiles")
        return description


#prepare_initial_segmentation(in_seg, tile, iota2_directory, is_slic=False)
