#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging
from iota2.Steps import IOTA2Step
from iota2.configuration_files import read_config_file as rcf
from iota2.Segmentation import prepare_segmentation_obia as pso
LOGGER = logging.getLogger("distributed.worker")


class intersect_seg_learn(IOTA2Step.Step):
    resources_block_name = "intersect_seg_learn"

    def __init__(self, cfg, cfg_resources_file, working_directory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.working_directory = working_directory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.data_field = self.i2_const.re_encoding_label_name
        self.nb_runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.spatial_resolution = rcf.read_config_file(self.cfg).getParam(
            "chain", "spatial_resolution")
        self.region_field = rcf.read_config_file(self.cfg).getParam(
            "chain", "region_field")
        self.full_segment = rcf.read_config_file(self.cfg).getParam(
            "obia", "full_learn_segment")
        for seed in range(self.nb_runs):
            dep = {}
            for tile in self.tiles:
                dep[tile] = []
            for model_name, model_meta in self.spatial_models_distribution.items(
            ):
                for tile in model_meta["tiles"]:
                    dep[tile].append(f"model_{model_name}_seed_{seed}")
            for tile in self.tiles:

                task = self.i2_task(
                    task_name=f"intersect_{tile}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": pso.
                        compute_intersection_between_segmentation_and_app_samples,
                        "tile": tile,
                        "iota2_directory": self.output_path,
                        "seed": seed,
                        "working_directory": self.working_directory,
                        "spatial_resolution": self.spatial_resolution,
                        "region_field": self.region_field,
                        "seg_field": self.i2_const.i2_segmentation_field_name,
                        "full_segment": self.full_segment
                    },
                    task_resources=self.get_resources())
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks_model",
                    task_sub_group=f"{tile}_{seed}",
                    # create a region dep. Once one region was processed launch training
                    task_dep_dico={"region_tasks": dep[tile]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Intersect samples and segmentation")
        return description
