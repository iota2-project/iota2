#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging
from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
from iota2.Segmentation import segmentation
from iota2.configuration_files import read_config_file as rcf
LOGGER = logging.getLogger("distributed.worker")


class slicSegmentation(IOTA2Step.Step):
    resources_block_name = "slic_segmentation"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(slicSegmentation, self).__init__(cfg, cfg_resources_file,
                                               self.resources_block_name)

        # step variables
        self.RAM = 1024.0 * get_RAM(self.get_resources()["ram"])
        self.workingDirectory = workingDirectory

        running_parameters = rcf.iota2_parameters(self.cfg)

        for tile in self.tiles:
            task = self.i2_task(
                task_name=f"slic_segmentation_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f":
                    segmentation.slicSegmentation,
                    "tile_name":
                    tile,
                    "output_path":
                    rcf.read_config_file(self.cfg).getParam(
                        'chain', 'output_path'),
                    "sensors_parameters":
                    running_parameters.get_sensors_parameters(tile),
                    "ram":
                    self.RAM,
                    "working_dir":
                    self.workingDirectory
                },
                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_tasks",
                task_sub_group=tile,
                task_dep_dico={"tile_tasks": [tile]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Compute SLIC segmentation by tile")
        #~ About SLIC segmentation implementation :
        #~     https://ieeexplore.ieee.org/document/8606448
        return description
