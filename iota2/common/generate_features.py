#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Module dedicated to generate the whole iota2 feature pipeline
"""
import logging
from collections import Counter
from dataclasses import dataclass
from pathlib import Path

from iota2.common.otb_app_bank import (
    AvailableOTBApp,
    create_application,
    get_input_parameter_output,
)
from iota2.common.raster_utils import OtbPipelineCarrier
from iota2.learning.utils import I2Label, I2TemporalLabel
from iota2.sensors.sensorscontainer import SensorsContainer
from iota2.typings.i2_types import (
    I2FeaturesPipeline,
    I2LabelAlias,
    OtbApp,
    OtbDep,
    PathLike,
    PipelinesLabels,
    SensorsParameters,
)

LOGGER = logging.getLogger("distributed.worker")


def build_masks_labels_from_raw_labels(
    raw_labels: list[I2LabelAlias], sensors_names: list[str]
) -> list[I2TemporalLabel]:
    """
    Build temporal masks labels from raw labels.

    This function constructs temporal mask labels from the provided raw labels.
    It ensures that the number of dates for each sensor and feature combination is consistent.
    The resulting list contains instances of the I2TemporalLabel class.

    Parameters
    ----------
    raw_labels:
        The list of raw labels.
    sensors_names:
        The list of sensor names.

    Notes
    -----
    return list of I2TemporalLabel
        The list of temporal labels.

    Raises
    ------
    ValueError
        If there is a mismatch detected during the mask label build.
    """
    buff_dates = dates_from_raw_labels(raw_labels, sensors_names)
    # check if all items contains the same number of dates
    sensor_dates_counter: dict[str, list[int]] = {}
    for (sensor, feat_name), dates in buff_dates.items():
        if sensor == "sentinel1":
            sensor = f"{sensor}{I2Label.separator}{feat_name[0:3]}"
        if sensor not in sensor_dates_counter:
            sensor_dates_counter[sensor] = []
        sensor_dates_counter[sensor].append(len(dates))
    for sensor_name, feat_dates in sensor_dates_counter.items():
        if len(Counter(feat_dates)) != 1:
            raise ValueError(
                "iota2 detected a missmatch during the mask"
                f" label build with sensor {sensor_name}"
            )
    masks_labels = []
    sensors_checked = []
    for (sensor, _), dates in buff_dates.items():
        if sensor in sensors_checked:
            continue
        for date in dates:
            if "sentinel1" in sensor:
                masks_labels.append(
                    I2TemporalLabel(
                        sensor_name="sentinel1",
                        feat_name=f"{sensor[10:13]}MASK",
                        date=date,
                    )
                )
            else:
                masks_labels.append(
                    I2TemporalLabel(
                        sensor_name=sensor,
                        feat_name="MASK",
                        date=date,
                    )
                )
        sensors_checked.append(sensor)
    return masks_labels


def dates_from_raw_labels(
    raw_labels: list[I2LabelAlias], sensors_names: list[str]
) -> dict[tuple[str, str], list[str]]:
    """
    Extracts dates from raw labels for specified sensors and organizes them by sensor and feature.

    This function processes a list of raw labels, checks if the sensor name is in the provided
    list of sensors, and extracts the associated date information. The function handles specific
    cases for the "sentinel1" sensor.

    Parameters
    ----------
    raw_labels : List[I2LabelT]
        A list of raw labels where each label is formatted as a string, split by
        `I2Label.separator`. Labels contain sensor information, feature, and date.

    sensors_names : List[str]
        A list of sensor names to filter the labels. Only labels matching these sensor names
        are processed.

    Returns
    -------
    Dict[Tuple[str, str], List[str]]
        A dictionary where the keys are tuples containing sensor names and features, and the values
        are lists of dates corresponding to those sensor-feature pairs.

        - Keys: (sensor_name, feature)
        - Values: list of dates corresponding to each sensor and feature.
    """
    buff_dates = {}
    for label in raw_labels:
        label_format = str(label).split(I2Label.separator)
        if label_format[0].lower() in sensors_names:
            sensor_name, feat, date = label_format
            if sensor_name == "sentinel1":
                sensor_name = f"{sensor_name}{I2Label.separator}{feat[0:3]}"
            if (sensor_name, feat) not in buff_dates:
                buff_dates[(sensor_name, feat)] = [date]
            else:
                buff_dates[(sensor_name, feat)].append(date)
    return buff_dates


@dataclass
class FeaturesPipelines:
    """
    Dataclass containing OTB apps for gapfilled data, raw data and masks, corresponding labels and
    dependencies
    """

    interp_apps: list[OtbApp]
    feat_labels: list[I2LabelAlias]

    raw_apps: list[OtbApp]
    raw_labels: list[I2LabelAlias]

    masks_apps: list[OtbApp]
    masks_count: int

    dep: list[OtbApp]


def get_features_pipeline(
    sensor_tile_container: SensorsContainer,
    logger: logging.Logger,
) -> tuple[FeaturesPipelines, list[str]]:
    """Get features pipeline."""

    masks_apps = []
    sensors_names = []

    sensors_features = sensor_tile_container.get_sensors_features(
        available_ram=1000, logger=logger
    )

    interp_apps, interp_dep, feat_labels = get_application_list_from_sensors(
        sensors_features, []
    )
    dep: OtbDep = []
    dep += [ccdep for cdep in interp_dep for ccdep in cdep]
    dep += interp_apps

    masks_features = sensor_tile_container.get_sensors_time_series_masks_raw(
        available_ram=1000
    )

    masks_dep = []
    masks_count = 0

    for sname, (mask_feature, mask_dep, nb_mask) in masks_features:
        mask_feature.Execute()
        masks_apps.append(mask_feature)
        masks_dep.append(mask_dep)
        if sname.lower() != "userfeatures":
            sensors_names.append(sname.lower())
            masks_count += nb_mask

    dep += [ccdep for cdep in masks_dep for ccdep in cdep]
    dep += masks_apps

    raw_features = sensor_tile_container.get_sensors_time_series_raw(available_ram=1000)

    raw_apps, raw_dep, raw_labels = get_application_list_from_sensors(raw_features, [])
    if len(raw_apps) == 0:
        # userFeat
        raw_apps = interp_apps
        raw_labels = feat_labels
    else:
        dep += [ccdep for cdep in raw_dep for ccdep in cdep]
        dep += raw_apps
    return (
        FeaturesPipelines(
            interp_apps=interp_apps,
            feat_labels=feat_labels,
            raw_apps=raw_apps,
            raw_labels=raw_labels,
            masks_apps=masks_apps,
            masks_count=masks_count,
            dep=dep,
        ),
        sensors_names,
    )


@dataclass
class FeaturesMapParameters:
    """
    Dataclass containing parameters for the computation of features maps

    Attributes
    ----------

    tile:
        name of the current tile to compute (ex: T31TCJ)
    working_directory:
        path of the working directory
    output_path:
        path where the features by chunks are to be written
    sensors_parameters:
        sensor parameters as a dict yielded by get_sensors_parameters()
    force_standard_labels:
        (de)activate standardize labels for feature extraction
    """

    tile: str
    working_directory: PathLike | None
    output_path: PathLike
    sensors_parameters: SensorsParameters
    force_standard_labels: bool = False


def generate_features(
    features_map_params: FeaturesMapParameters,
    logger: logging.Logger = LOGGER,
) -> tuple[OtbPipelineCarrier, PipelinesLabels]:
    """
    Compute features thanks to otb's pipeline.

    Parameters
    ----------
    features_map_params:
        Parameters for the features map
    logger :
        Logger

    Return
    ------
    AllFeatures [OTB Application object] : otb object ready to Execute()
    feat_labels [list] : list of strings, labels for each output band
    dep [list of OTB Applications]
    """
    logger.info(f"prepare features for tile : {features_map_params.tile}")

    sensor_tile_container = SensorsContainer(
        tile_name=features_map_params.tile,
        working_dir=(
            str(features_map_params.working_directory)
            if features_map_params.working_directory
            else None
        ),
        output_path=(
            str(features_map_params.output_path)
            if features_map_params.output_path
            else None
        ),
        **features_map_params.sensors_parameters,
    )

    features_pipeline, sensors_names = get_features_pipeline(
        sensor_tile_container,
        logger,
    )

    # Prepare concatenation of each pipeline and prepare dictionary output
    interp_name = f"{features_map_params.tile}_Features.tif"
    masks_name = f"{features_map_params.tile}_binary_masks.tif"
    raw_name = f"{features_map_params.tile}_raw_data.tif"
    features_dir = str(
        Path(features_map_params.output_path)
        / "features"
        / features_map_params.tile
        / "tmp"
    )
    features_raster = str(Path(features_dir) / interp_name)
    masks_raster = str(Path(features_dir) / masks_name)
    raw_raster = str(Path(features_dir) / raw_name)

    if len(features_pipeline.interp_apps) > 1:

        interp_all_features, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {"il": features_pipeline.interp_apps, "out": features_raster},
        )

        masks_all_features, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {"il": features_pipeline.masks_apps, "out": masks_raster},
        )

        raw_all_features, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {"il": features_pipeline.raw_apps, "out": raw_raster},
        )

    else:
        interp_all_features = features_pipeline.interp_apps[0]
        output_param_name = get_input_parameter_output(interp_all_features)
        interp_all_features.SetParameterString(output_param_name, features_raster)

        masks_all_features = features_pipeline.masks_apps[0]
        output_param_name = get_input_parameter_output(masks_all_features)
        masks_all_features.SetParameterString(output_param_name, masks_raster)

        raw_all_features = features_pipeline.raw_apps[0]
        output_param_name = get_input_parameter_output(raw_all_features)
        raw_all_features.SetParameterString(output_param_name, raw_raster)

    features_pipeline.dep.append(interp_all_features)
    features_pipeline.dep.append(masks_all_features)
    features_pipeline.dep.append(raw_all_features)

    otb_pipelines = OtbPipelineCarrier(
        interp_all_features,
        raw_all_features,
        masks_all_features,
        pipeline_dependencies=features_pipeline.dep,
    )

    masks_labels = build_masks_labels_from_raw_labels(
        features_pipeline.raw_labels, sensors_names
    )
    if len(masks_labels) != features_pipeline.masks_count:
        raise ValueError(
            f"inconsistency in masks labels, masks labels = {masks_labels}"
            f"and masks count = {features_pipeline.masks_count}"
        )
    # This option allow to impose a standard labeling in vector file
    # so they can be easily merged (useful for multi annual classification)
    if features_map_params.force_standard_labels:
        feat_labels = [
            I2Label(sensor_name="value", feat_name=i)
            for i in range(len(features_pipeline.feat_labels))
        ]
    else:
        feat_labels = features_pipeline.feat_labels
    return otb_pipelines, PipelinesLabels(
        interp=feat_labels, raw=features_pipeline.raw_labels, masks=masks_labels
    )


def get_application_list_from_sensors(
    sensors_features: list[tuple[str, I2FeaturesPipeline]],
    labels_list: list[I2LabelAlias],
) -> tuple[list[OtbApp], list[OtbDep], list[I2LabelAlias]]:
    """
    Function which get all otb application from a sensor object
    In case of reflectance, ancillary_data is a list of labels
    In case of masks, ancillary_data is an integer counting the number of masks
    """
    apps = []
    dep_app = []
    for _, (
        (sensor_features, sensor_features_dep),
        sensor_features_labels,
    ) in sensors_features:
        sensor_features.Execute()
        apps.append(sensor_features)
        dep_app.append(sensor_features_dep)
        labels_list += sensor_features_labels
    return apps, dep_app, labels_list
