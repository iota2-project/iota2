# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Utility function dedicated to debugging
"""

import logging
import pprint
from collections.abc import Callable, Generator
from pathlib import Path
from typing import Any, Self

from sphinx.cmd.build import main as sphinx_main

from iota2.common.file_utils import file_search_and
from iota2.common.utils import run
from iota2.configuration_files import read_config_file as rcf
from iota2.typings.i2_types import ListPathLike, PathLike

LOGGER = logging.getLogger("distributed.worker")


class DisplayablePath:
    """
    Class used to print a directory tree
    """

    display_filename_prefix_middle = "├──"
    display_filename_prefix_last = "└──"
    display_parent_prefix_middle = "    "
    display_parent_prefix_last = "│   "

    def __init__(
        self,
        path: PathLike,
        parent_path: Self | None,
        is_last: bool,
    ):
        self.path = Path(str(path))
        self.parent = parent_path
        self.is_last = is_last
        self.depth: int
        if self.parent:
            self.depth = self.parent.depth + 1
        else:
            self.depth = 0

    @classmethod
    def make_tree(
        cls,
        root: PathLike,
        parent: Self | None = None,
        is_last: bool = False,
        criteria: Callable | None = None,
    ) -> Generator:
        """
        Method used to create the files tree
        """
        root = Path(str(root))
        criteria = criteria or cls._default_criteria
        assert callable(criteria)
        displayable_root = cls(root, parent, is_last)
        yield displayable_root

        children = sorted(
            list(path for path in root.iterdir() if criteria(path)),
            key=lambda s: str(s).lower(),
        )
        count = 1
        for path in children:
            is_last = count == len(children)
            if path.is_dir():
                yield from cls.make_tree(
                    path, parent=displayable_root, is_last=is_last, criteria=criteria
                )
            else:
                yield cls(path, displayable_root, is_last)
            count += 1

    @classmethod
    def _default_criteria(cls, path: Any) -> bool:  # pylint: disable=unused-argument
        return True

    @property
    def displayname(self) -> str:
        """Return name of the directory or file"""
        if self.path.is_dir():
            return self.path.name + "/"
        return self.path.name

    def displayable(self) -> str:
        """
        Return a string containing an element (file, directory) with corresponding prefix
        """
        if self.parent is None:
            return self.displayname

        _filename_prefix = (
            self.display_filename_prefix_last
            if self.is_last
            else self.display_filename_prefix_middle
        )

        parts = [f"{str(_filename_prefix)} {str(self.displayname)}"]

        parent = self.parent
        while parent and parent.parent is not None:
            parts.append(
                self.display_parent_prefix_middle
                if parent.is_last
                else self.display_parent_prefix_last
            )
            parent = parent.parent

        return "".join(reversed(parts))


def get_output_tree(path_to_draw: PathLike) -> str:
    """Return a string containing the complete tree"""
    chain_log = "=" * 79 + "\n"
    chain_log += " " * 30 + "Output Directories" + " " * 31 + "\n"
    chain_log += "=" * 79 + "\n"
    # the call of Path ensure the path is valid ?
    out_cont = DisplayablePath.make_tree(Path(path_to_draw))
    for path in out_cont:
        chain_log += path.displayable() + "\n"
    return chain_log + "\n" * 3


def get_locale(path: PathLike) -> str:
    """
    Return a string with the content of locale.txt file
    Parameters
    ----------
    path:
        Path to file's directory
    """
    tempfile = str(Path(path) / "locale.txt")
    run(f"locale>{tempfile}")
    chain_log = "=" * 79 + "\n"
    chain_log += " " * 36 + "LOCALE" + " " * 37 + "\n"
    chain_log += "=" * 79 + "\n"
    with open(tempfile, encoding="utf-8") as f_tmp:
        chain_log += f_tmp.read()
    Path(tempfile).unlink()
    return chain_log + "\n" * 3


def get_package_version(path: PathLike) -> str:
    """
    Return a string with the content of package.txt file
    Parameters
    ----------
    path:
        Path to file's directory
    """
    tempfile = str(Path(path) / "package.txt")
    run(f"conda list>{tempfile}")
    chain_log = "=" * 79 + "\n"
    chain_log += " " * 30 + "Environment package" + " " * 30 + "\n"
    chain_log += "=" * 79 + "\n"
    with open(tempfile, encoding="utf-8") as f_tmp:
        chain_log += f_tmp.read()
    Path(tempfile).unlink()
    return chain_log + "\n" * 3


def get_config_file(configfile: PathLike, rst_dir: PathLike) -> dict[str, str]:
    """
    Create an rst config file using the provided config file path and return its path in a dict.

    Parameters
    ----------
    configfile:
        Path to the config file
    rst_dir:
        Path of the directory where to store the resulting rst file
    """
    chain_log = "Configuration file\n"
    chain_log += "-" * len(chain_log) + "\n\n"

    chain_log += ".. code-block:: python\n\n"

    with open(configfile, encoding="utf-8") as f_tmp:
        for line in f_tmp:
            chain_log += "\t" + line

    config_file_rst = str(Path(rst_dir) / "configuration_file.rst")
    with open(config_file_rst, "w", encoding="utf-8") as conf_file:
        conf_file.write(chain_log)
    return {"configuration file": config_file_rst}


def get_cont_path_from_config(
    configfile: PathLike, rst_source_dir: PathLike
) -> dict[str, str]:
    """
    Writes the tree of input sensor data in file (one rst file per sensor) and return a dictionary
    with paths to the created files

    Parameters
    ----------
    configfile:
        Path to config file
    rst_source_dir:
        Directory where to store RST files
    """
    cfg = rcf.ReadConfigFile(configfile)
    block_chain = cfg.get_section("chain")

    path_rst_dict = {}
    for key in block_chain.keys():
        if "path" in key.lower():
            path_to_draw = cfg.get_param("chain", key)
            if isinstance(path_to_draw, str):
                if Path(path_to_draw).is_dir():
                    rst_file = str(Path(rst_source_dir) / f"{key}_content.rst")
                    path_rst_dict[f"{key} content"] = rst_file

                    chain_log = f"Content of {key}\n"
                    chain_log += "-" * len(chain_log) + "\n\n"
                    chain_log += ".. code-block:: bash\n\n"
                    out_cont = DisplayablePath.make_tree(Path(path_to_draw))
                    for path in out_cont:
                        chain_log += "\t" + path.displayable() + "\n"
                    with open(rst_file, "w", encoding="utf-8") as rst:
                        rst.write(chain_log)
    return path_rst_dict


def get_cont_file_from_config(
    configfile: PathLike, rst_source_dir: PathLike
) -> dict[str, str]:
    """
    Write the content of all txt and csv input files (nomenclature, color table...) in a single
    rst file, separated by file names and return its path

    Parameters
    ----------
    configfile:
        Path to config file
    rst_source_dir:
        Directory where to store RST file
    """
    cfg = rcf.ReadConfigFile(configfile)
    block_chain = cfg.get_section("chain")
    rst_file = str(Path(rst_source_dir) / "input_files_content.rst")

    chain_log = "Input files content\n"
    chain_log += "-" * len(chain_log) + "\n\n"

    for key in block_chain.keys():
        file_to_read = cfg.get_param("chain", key)
        if isinstance(file_to_read, str):
            if Path(file_to_read).is_file() and (
                "txt" in file_to_read or "csv" in file_to_read
            ):
                chain_log += key + "\n"
                chain_log += "*" * len(key) + "\n\n"
                chain_log += f"content of {file_to_read}\n"
                chain_log += "\n.. code-block:: bash\n\n"
                with open(file_to_read, encoding="utf-8") as contfile:
                    for line in contfile:
                        chain_log += f"\t{line}\n"
                chain_log += "\n\n"
    with open(rst_file, "w", encoding="utf-8") as rst:
        rst.write(chain_log)
    return {"input files content": rst_file}


def get_log_file(logfile: PathLike) -> str:
    """
    Return a string containing the name and content of a given logfile
    Parameters
    ----------
    logfile:
        Path to the log file
    """
    chain_log = "=" * 79 + "\n"
    name = Path(logfile).name
    chain_log += " " * int((79 - len(name)) / 2) + str(name)
    chain_log += " " * int((79 - len(name)) / 2) + "\n"
    chain_log += "=" * 79 + "\n"
    cont = f"Unable to open logfile : {logfile}"
    with open(logfile, encoding="utf-8") as logf:
        cont = logf.read()
    chain_log += cont
    return chain_log + "\n" * 3


def index_logging(rst_source_dir: PathLike, pages: dict) -> None:
    """generate iota2 index.rst dedicated to logs"""

    header = (
        "iota² logging informations\n"
        "==========================\n\n"
        "The aim of this html documentation is to help users to "
        "understand the iota² execution and if something is going "
        "wrong, try to figure it out. If the user does not understand "
        "why the chain crashed or failed, an archive containing the "
        "```/logs``` directory (with or without the ```/html``` directory) "
        "can be attached to an issue at : https://framagit.org/iota2-project/iota2/-/issues.\n"
        "\n"
        ".. toctree::\n"
        "   :maxdepth: 1\n"
        "   :caption: Contents:\n\n"
    )

    body = ""
    for title, rst_file in pages.items():
        body += f"   {title} <{Path(rst_file).stem}>\n"

    with open(str(Path(rst_source_dir) / "index.rst"), "w", encoding="utf-8") as rst:
        rst.write(header + body)


def get_tasks_status_rst(
    svg_status_files: ListPathLike, rst_dir: PathLike
) -> dict[str, str]:
    """Create task status pages (rst format)"""
    rst_dic = {}
    for cpt, svg_file in enumerate(svg_status_files):
        chain_log = "iota² tasks status\n"
        chain_log += "-" * len(chain_log) + "\n\n"

        chain_log += (
            "This page allow users to check every tasks status, "
            "every node are clickable and present the dedicated log\n\n"
        )

        chain_log += ".. raw:: html\n" f"\t:file: {svg_file}\n"
        tasks_status_rst = str(Path(rst_dir) / f"tasks_status_{cpt+1}.rst")
        with open(tasks_status_rst, "w", encoding="utf-8") as rst_file:
            rst_file.write(chain_log)
        rst_dic[f"iota² tasks status graph {cpt+1}"] = tasks_status_rst
    return rst_dic


def get_env_informations(tmp_dir: PathLike, rst_source_dir: PathLike) -> dict[str, str]:
    """
    Write environment information in an rst file and return a dictionary containing the path to
    this file.
    """
    tempfile = str(Path(tmp_dir) / "locale.txt")
    run(f"locale>{tempfile}")
    chain_log = "Running environment informations\n"
    chain_log += "-" * len("Running environment informations") + "\n"
    chain_log += "LOCALE\n"
    chain_log += "*" * len("LOCALE") + "\n"
    chain_log += ".. code-block:: bash\n\n"
    with open(tempfile, encoding="utf-8") as f_tmp:
        for line in f_tmp:
            chain_log += "\t" + line + "\n"
    chain_log += "\n\n"
    Path(tempfile).unlink()

    tempfile = str(Path(tmp_dir) / "package.txt")
    run(f"conda list>{tempfile}")
    chain_log += "CONDA ENVIRONEMENT\n"
    chain_log += "*" * len("CONDA ENVIRONEMENT") + "\n"
    chain_log += ".. code-block:: bash\n\n"
    with open(tempfile, encoding="utf-8") as f_tmp:
        for line in f_tmp:
            chain_log += "\t" + line + "\n"
    chain_log += "\n\n"
    Path(tempfile).unlink()

    rst_file = str(Path(rst_source_dir) / "environment_info.rst")
    with open(rst_file, "w", encoding="utf-8") as rst:
        rst.write(chain_log)

    return {"running environment": rst_file}


def gen_rst_pages(
    config_file: PathLike,
    rst_source_dir: PathLike,
    svg_status_files: ListPathLike,
    i2_output_dir: PathLike,
) -> tuple[dict[str, str], dict[str, str]]:
    """Generate all RST files"""
    config_file_rst = get_config_file(config_file, rst_source_dir)
    tasks_status_rst = get_tasks_status_rst(svg_status_files, rst_source_dir)
    path_content_rst = get_cont_path_from_config(config_file, rst_source_dir)
    file_content_rst = get_cont_file_from_config(config_file, rst_source_dir)
    environment_info_rst = get_env_informations(i2_output_dir, rst_source_dir)
    return (
        dict(
            config_file_rst,
            **tasks_status_rst,
            **path_content_rst,
            **file_content_rst,
            **environment_info_rst,
        ),
        tasks_status_rst,
    )


def set_iota2_tasks_figure_size(html_page: PathLike) -> None:
    """
    the svg figure size seems to be configurable, like done at
    https://github.com/pymedphys/pymedphys/blob/f4d404fa1cf3f551c4aa80ef27438f418c61a436/docs/conf.py
    by using the setup() method in sphinx conf.py
    and specify at
    https://www.sphinx-doc.org/en/master/usage/configuration.html

    However, by using sphinx python API (without conf.py) it seems not to be accessible.
    Currently the solution is to directly alter the html page and set the img size.
    """
    html_buffer = []
    with open(html_page, encoding="utf-8") as origin_html:
        for line in origin_html:
            if "<svg width=" in line:
                html_buffer.append('<svg width="550pt" height="1000pt"')
            else:
                html_buffer.append(line)
    with open(html_page, "w", encoding="utf-8") as dest_html:
        dest_html.write("\n".join(html_buffer))


def gen_html_logging_pages(
    config_file: PathLike,
    html_source_dir: PathLike,
    source_dir: PathLike,
    svg_status_files: ListPathLike,
    i2_output_dir: PathLike,
) -> str:
    """Generate html logs"""

    rst_pages, rst_status_pages = gen_rst_pages(
        config_file, source_dir, svg_status_files, i2_output_dir
    )
    rst_files = file_search_and(source_dir, True, ".rst")
    index_logging(source_dir, rst_pages)

    output_file = html_source_dir

    args = (
        f"{source_dir} -b html -D extensions=sphinx.ext.autodoc "
        f"-D master_doc=index "
        f"-D source_suffix=.rst "
        f"-D html_theme=sphinx_rtd_theme "
        f"-C {output_file} "
        f"{' '.join(rst_files)}"
    )

    sphinx_main(args.split())

    for _, rst_file in rst_status_pages.items():
        rst_tasks_status = Path(rst_file).name
        html_fn = f"{Path(rst_tasks_status).stem}.html"
        # html_source_dir
        tasks_status_html = file_search_and(html_source_dir, True, html_fn)[0]
        set_iota2_tasks_figure_size(tasks_status_html)
    index_file: str = file_search_and(html_source_dir, True, "index.html")[0]
    return index_file


def recursive_type(obj: Any) -> type | list | dict:
    """returns types of object
    if object is a list or a dict, print its values instead
    and keep going recursively
    """
    if isinstance(obj, dict):
        return {k: recursive_type(v) for k, v in obj.items()}
    if isinstance(obj, list):
        return [recursive_type(v) for v in obj]
    return type(obj)


def pprint_type(obj: Any) -> None:
    """Pretty print of recursive type"""
    pprint.pp(recursive_type(obj))
