#!/usr/bin/env python3
# pylint: skip-file
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

"""
Perform pylint on all iota2 project.
Print a table containing all occurence of pylint report.
"""
import argparse
import json
import re
import warnings
from collections import Counter
from concurrent.futures import ProcessPoolExecutor
from io import StringIO
from pathlib import Path

import pandas as pd
from pylint.lint import Run
from pylint.reporters import JSONReporter
from tabulate import tabulate

from iota2.common.file_utils import file_search_and, get_iota2_project_dir

warnings.filterwarnings("ignore")


def collect_pylint_msg(
    file_to_pylint: Path, pylintrc_file: Path
) -> list[tuple[str, str, str]]:
    """Perform pylint to a file and return a list of labels."""
    pylint_output = StringIO()
    reporter = JSONReporter(pylint_output)
    Run(
        ["--rcfile", str(pylintrc_file), str(file_to_pylint)],
        reporter=reporter,
        exit=False,
    )
    report_as_json = json.loads(pylint_output.getvalue())
    return [
        (
            f"{msg['message-id']}, {msg['symbol']}",
            f"{msg['path']}",
            f"line {msg['line']}, col {msg['column']}",
        )
        for msg in report_as_json
    ]


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Perform pylint on all iota2 project.")
    parser.add_argument(
        "-target_msgs",
        dest="target_msgs",
        nargs="+",
        default=None,
        help="target pylint messages, ie : C0303 R1732",
        required=False,
    )
    parser.add_argument(
        "-exclude",
        dest="exclude",
        nargs="+",
        default=[],
        help="exclude patterns (used on file location), ie : unittests integrationtests",
        required=False,
    )
    args = parser.parse_args()
    target_msgs = args.target_msgs
    exclude = args.exclude

    i2_directory = get_iota2_project_dir()
    pylintrc = Path(get_iota2_project_dir()) / "iota2" / ".pylintrc"
    i2_py_files = list(
        filter(
            lambda x: x.endswith(".py")
            and not re.search("|".join(["__init__"] + exclude), x),
            file_search_and(i2_directory, True, ".py"),
        )
    )
    NB_WORKERS = 10
    print(
        "Number of python files detected " f"in {i2_directory} : " f"{len(i2_py_files)}"
    )

    with ProcessPoolExecutor(max_workers=NB_WORKERS) as executor:
        results = executor.map(
            collect_pylint_msg, i2_py_files, [pylintrc] * len(i2_py_files)
        )

    flatten_res = [elem for msg_list in results for elem in msg_list]
    msgs_id, paths, positions = zip(*flatten_res)
    pylint_df = pd.DataFrame(
        {"pylint message": msgs_id, "file": paths, "position": positions}
    )

    pylint_df = pylint_df.groupby(
        "pylint message",
        group_keys=True,
    ).apply(lambda x: x)
    if target_msgs is not None:
        filtered_df = pylint_df[
            pylint_df["pylint message"].apply(
                lambda x: bool(re.search("|".join(target_msgs), x))
            )
        ]
        print(tabulate(filtered_df, headers="keys", tablefmt="github", showindex=False))
    else:
        labels_count = dict(Counter(pylint_df["pylint message"].values))
        pylint_df = pd.DataFrame(
            {
                "pylint message": list(labels_count.keys()),
                "occurrence": list(labels_count.values()),
            }
        )
        PYLINT_DF = pylint_df.sort_values(by=["occurrence"], ascending=False)
        PYLINT_DF["done : :white_check_mark: not done : :white_large_square:"] = [
            ":white_large_square:"
        ] * len(PYLINT_DF)
        print(tabulate(PYLINT_DF, headers="keys", tablefmt="github", showindex=False))
