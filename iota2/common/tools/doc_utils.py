#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to generate documentation"""

import re
from pathlib import Path
from typing import Any

import pandas as pd

from config import Config
from iota2.common.file_utils import get_iota2_project_dir
from iota2.common.utils import run
from iota2.configuration_files.read_config_file import ReadConfigFile
from iota2.tests.utils.tests_utils_vectors import random_ground_truth_generator


def gen_cfg_builders(output_path: str, fake_ground_truth: str) -> tuple[str, str, str]:
    """generate dummy builders cfg files"""
    config_classif_file = str(
        Path(output_path) / "examples" / "config_tutorial_classification.cfg"
    )
    config_fmaps_file = str(
        Path(output_path) / "examples" / "config_tutorial_features_maps.cfg"
    )
    config_obia_file = str(Path(output_path) / "examples" / "config_tutorial_obia.cfg")

    cfg_classif_file_steps = config_classif_file.replace(".cfg", "_steps.cfg")
    cfg_fmaps_file_steps = config_fmaps_file.replace(".cfg", "_steps.cfg")
    cfg_obia_file_steps = config_obia_file.replace(".cfg", "_steps.cfg")

    with open(config_classif_file, encoding="utf-8") as i2_conf_file:
        cfg = Config(i2_conf_file).as_dict()
        cfg["chain"]["nomenclature_path"] = config_classif_file
        cfg["chain"]["ground_truth"] = fake_ground_truth
        cfg["chain"]["color_table"] = config_classif_file
        cfg["chain"]["s2_path"] = config_classif_file
        with open(cfg_classif_file_steps, "w", encoding="utf-8") as file_cfg:
            file_cfg.write(str(cfg))

    with open(config_fmaps_file, encoding="utf-8") as i2_conf_file:
        cfg = Config(i2_conf_file).as_dict()
        cfg["chain"]["s2_path"] = config_classif_file
        with open(cfg_fmaps_file_steps, "w", encoding="utf-8") as file_cfg:
            file_cfg.write(str(cfg))

    with open(config_obia_file, encoding="utf-8") as i2_conf_file:
        cfg = Config(i2_conf_file).as_dict()
        cfg["chain"]["nomenclature_path"] = config_classif_file
        cfg["chain"]["ground_truth"] = config_classif_file
        cfg["chain"]["color_table"] = config_classif_file
        cfg["chain"]["s2_path"] = config_classif_file
        with open(cfg_obia_file_steps, "w", encoding="utf-8") as file_cfg:
            file_cfg.write(str(cfg))

    return (
        cfg_classif_file_steps,
        cfg_fmaps_file_steps,
        cfg_obia_file_steps,
    )


def generate_list_steps_files(output_path: str, fake_ground_truth: str) -> None:
    """
    Load configurations files and write corresponding steps rst files

    Parameters
    ----------
    output_path:
        Directory where to write files
    fake_ground_truth:
        Path to the fake ground truth file
    """
    cfg_files = gen_cfg_builders(output_path, fake_ground_truth)
    classif_cfg, fmap_cfg, obia_cfg = cfg_files

    # Classif tutorial
    steps_file = str(Path(output_path) / "examples" / "steps_classification.txt")
    run(f"Iota2.py -config {classif_cfg} -only_summary > {steps_file}")

    # features maps
    steps_file = str(Path(output_path) / "examples" / "steps_features_maps.txt")
    run(f"Iota2.py -config {fmap_cfg} -only_summary > {steps_file}")

    # obia
    steps_file = str(Path(output_path) / "examples" / "steps_obia.txt")
    run(f"Iota2.py -config {obia_cfg} -only_summary > {steps_file}")

    for cfg_file in cfg_files:
        Path(cfg_file).unlink()


def generate_doc_files() -> None:
    """Calls all functions to generate documentation"""
    iota2_dir = get_iota2_project_dir()
    output_path = str(Path(iota2_dir) / "doc" / "source")
    if not Path(output_path).exists():
        Path(output_path).mkdir()
    create_doc_dataframe(output_path)

    # generating the steps needs a fake ground truth file
    fake_ground_truth = str(Path(output_path) / "fake_gound_truth.shp")
    random_ground_truth_generator(fake_ground_truth, "code", 1)
    # generate steps list
    generate_list_steps_files(output_path, fake_ground_truth)
    # remove fake ground truth
    Path(fake_ground_truth).unlink()


def create_doc_dataframe(output_path: str) -> None:
    """
    use a dataframe to manage the rst parameters table

    Parameters
    ----------
    output_path:
        path to write files
    """

    params_desc = ReadConfigFile.get_params_descriptions()
    df_doc, builders_list = build_doc_dataframe(params_desc)
    check_list: dict = {}
    for builder in builders_list:
        check_list.update(
            generate_builder_rst(builder, check_list, df_doc, output_path)
        )

    glob_output_file = str(Path(output_path) / "all_parameters.rst")
    dico_glob = dict(sorted(check_list.items()))
    glob_chain = (
        ".. list-table::\n"
        + " " * 6
        + ":widths: auto\n"
        + " " * 6
        + ":header-rows: 1\n\n"
        + " " * 6
        + "* - Name\n"
        + " " * 8
        + "- Default Value\n"
        + " " * 8
        + "- Description\n"
        + " " * 8
        + "- Type\n"
        + " " * 8
        + "- Name\n\n"
    )
    for _, value in dico_glob.items():
        glob_chain += value
    with open(glob_output_file, "w", encoding="utf-8") as out_file:
        title = "All configuration parameters"
        out_file.write(title + "\n")
        out_file.write("#" * len(title) + "\n\n")
        out_file.write(glob_chain)
        out_file.write("\n")


def generate_builder_rst(
    builder: str, check_list: dict, df_doc: pd.DataFrame, output_path: str
) -> dict:
    """
    Generate an rst file for a specific builder and update the check_list with
    parameter documentation.

    Parameters
    ----------
    builder : str
        The name of the builder for which the rst file is generated.
    check_list : dict
        A dictionary to track unique parameters across different builders. It contains
        the parameter names as keys and their rst documentation as values.
    df_doc : pd.DataFrame
        The DataFrame containing the documentation of all parameters, including the builder,
        section, and their respective values, types, descriptions, etc.
    output_path : str
        The directory where the generated rst file for the builder will be saved.

    Returns
    -------
    dict
        The updated check_list dictionary containing parameters and their documentation.
        The dictionary is used to aggregate parameter documentation across builders.
    """
    current_check_list = check_list.copy()
    # Produce the expected output file name from builder's name. For example:
    # I2Classification => i2_classification_builder.rst
    output_file_name = re.sub(r"(?<!^)(?=[A-Z])", "_", builder).lower() + "_builder.rst"
    output_file = str(Path(output_path) / output_file_name)
    ss_df = df_doc[df_doc["Builder"] == builder]
    ss_df = ss_df.sort_values(by=["Name"])
    sections = list(ss_df["Section"].unique())
    sections = sorted(sections)
    with open(output_file, "w", encoding="utf-8") as out_file:
        out_file.write(f"{builder}\n")
        out_file.write("#" * len(builder) + "\n" * 2)
        for section in sections:
            chain = (
                ".. list-table::\n"
                + " " * 6
                + ":widths: auto\n"
                + " " * 6
                + ":header-rows: 1\n\n"
                + " " * 6
                + "* - Name\n"
                + " " * 8
                + "- Default Value\n"
                + " " * 8
                + "- Description\n"
                + " " * 8
                + "- Type\n"
                + " " * 8
                + "- Mandatory\n"
                + " " * 8
                + "- Name\n\n"
            )

            out_file.write(f".. _{builder}.{section}:\n\n")
            out_file.write(f"{section}\n")
            out_file.write("*" * (len(section)) + "\n\n")
            ss_df1 = ss_df[ss_df["Section"] == section]

            ss_df1 = ss_df1.drop(["Builder", "Section"], axis="columns")
            long_desc_dict = {}
            for row in ss_df1.iterrows():
                # values[0] -> parameters name
                # values[1] -> default value
                # values[2] -> short description
                # values[3] -> type
                # values[4] -> long description
                # values[5] -> specific description
                values = row[1].values
                param_name = values[0]
                if values[4] is not None:
                    long_desc_dict[values[0]] = [
                        values[4],
                        f".. _desc_{builder}.{section}.{values[0]}:",
                        f"{builder}.{section}.{values[0]}",
                    ]
                    param_name = (
                        f":ref:`{values[0]} <desc_{builder}.{section}.{values[0]}>`"
                    )
                    # param_name = f":ref:`{values[0]} <{values[0]}>`"
                if values[0] in ["builders_paths", "module"]:
                    values[1] = "/path/to/iota2/sources"

                ref_lab = (
                    " " * 10
                    + f".. _{builder}.{section}.{values[0]}:\n"
                    + " " * 6
                    + f"* - {param_name}\n"
                    + " " * 8
                    + f"- {values[1]}\n"
                    + " " * 8
                    + f"- {values[2]}\n"
                    + " " * 8
                    + f"- {values[3]}\n"
                    + " " * 8
                    + f"- {values[5]}\n"
                    + " " * 8
                    + f"- {param_name}\n\n"
                    + "\n"
                )
                chain += ref_lab
                if values[0] not in current_check_list:
                    current_check_list[values[0]] = (
                        " " * 6
                        + f"* - {values[0]}\n"
                        + " " * 8
                        + f"- {values[1]}\n"
                        + " " * 8
                        + f"- {values[2]}\n"
                        + " " * 8
                        + f"- {values[3]}\n"
                        + " " * 8
                        + f"- {values[0]}\n\n"
                    )

            out_file.write(chain)
            out_file.write("\n" * 3)
            if long_desc_dict:
                out_file.write("Notes\n")
                out_file.write(f"{'=' * len('Notes')}\n\n")
                for param_name, param_value in long_desc_dict.items():
                    out_file.write(param_value[1] + "\n\n")
                    title = f":ref:`{param_name} <{param_value[2]}>`"
                    out_file.write(title + "\n")
                    out_file.write(f"{'-' * len(title)}\n")

                    out_file.write(param_value[0])
                    out_file.write("\n\n")
        return current_check_list


def build_doc_dataframe(
    params_desc: dict[int, dict[str, Any]]
) -> tuple[pd.DataFrame, list[str]]:
    """
    Build a DataFrame to store parameter documentation.

    Parameters
    ----------
    params_desc : dict
        The descriptions of the parameters.

    Returns
    -------
    tuple:
        - df_doc (DataFrame): DataFrame containing the documentation.
        - builders_list (list): List of builders found in the parameters.
    """
    df_doc = pd.DataFrame(
        columns=(
            "Builder",
            "Section",
            "Name",
            "Value",
            "Description",
            "Type_exp",
            "long_desc",
            "Mandatory",
        )
    )
    ind = 0
    builders_list = []
    for _, field_meta in params_desc.items():
        for builder in field_meta["available_on_builders"]:
            if builder not in builders_list:
                builders_list.append(builder)
            df_doc.loc[ind] = [
                builder,
                field_meta["section"],
                field_meta["field_name"],
                field_meta["default"],
                field_meta["short_desc"],
                field_meta["type"],
                field_meta.get("long_desc", None),
                builder in field_meta["mandatory_on_builders"],
            ]
            ind += 1
    return df_doc, builders_list


if __name__ == "__main__":

    generate_doc_files()
