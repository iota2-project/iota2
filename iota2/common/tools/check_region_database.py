#!/usr/bin/env python3

# =========================================================================
#   Program:   vector tools
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Tools to check the consistency of the database with iota2's requirements."""
import argparse
import string
import sys
from pathlib import Path

from osgeo import ogr

from iota2.common import service_error
from iota2.common.utils import str2bool
from iota2.typings.i2_types import ErrorsList, PathLike
from iota2.vector_tools.delete_duplicate_geometries_sqlite import (
    delete_duplicate_geometries_sqlite,
)
from iota2.vector_tools.vector_functions import (
    check_empty_geom,
    check_valid_geom,
    get_field_type,
    get_fields,
    get_vector_proj,
)


def get_geometries_by_area(
    input_vector: str, area: float, driver_name: str = "ESRI Shapefile"
) -> list[tuple[ogr.Geometry, float]]:
    """
    Get geometries smaller than the area.
    Sub geometries of a MULTIPOLYGONS are checked and returned either

    Parameters
    ----------
    input_vector:
        Path to the shapefile to be analyzed
    area:
        Area threshold
    driver_name:
        Name of the driver
    """

    driver = ogr.GetDriverByName(driver_name)
    data_src = driver.Open(input_vector, 0)
    layer_src = data_src.GetLayer()
    output_geoms = []
    for feature in layer_src:
        geom = feature.GetGeometryRef()
        for geom_part in geom:
            geom_area = geom_part.GetArea()
            if geom_area < area:
                output_geoms.append((geom_part.Clone(), geom_area))
    return output_geoms


def is_first_char_ascii(input_file: str) -> bool:
    """
    Check if first character of a file name is a letter

    Parameters
    ----------
    input_file:
        Path to the shapefile
    """
    avail_characters = string.ascii_letters
    first_character = Path(input_file).name[0]
    return first_character in avail_characters


def remove_invalid_features(shapefile: str, do_corrections: bool) -> int:
    """
    Return the sum of invalid features and delete them (inplace) if do_corrections is True.

    Parameters
    ----------
    shapefile:
        path to the shapefile to check
    do_corrections:
        whether to remove invalid features or not
    """
    none_geom = 0
    driver = ogr.GetDriverByName("ESRI Shapefile")
    data_source = driver.Open(shapefile, 1)
    if not data_source:
        raise FileNotFoundError(f"the file {shapefile} doest not exists or is empty")
    layer = data_source.GetLayer()
    count = layer.GetFeatureCount()
    for feature in range(count):
        feat = layer[feature]
        geom = feat.GetGeometryRef()
        if not geom:
            none_geom += 1
            if do_corrections:
                layer.DeleteFeature(feature)
    layer.ResetReading()
    return none_geom


def check_region_database_inplace(
    input_vector: str, field: str, epsg: int, do_corrections: bool
) -> ErrorsList:
    """Check multiple parameters of the shapefile representing regions data.

    Return corresponding errors if needed and applies the required correction (inplace)
    if do_corrections is True.

    Parameters
    ----------
    input_vector:
        path to shapefile to check
    field:
        field name
    epsg:
        projection code
    do_corrections:
        whether to apply corrections or not
    """
    area_threshold = 0.1
    input_vector_fields = get_fields(input_vector)
    errors: ErrorsList = []

    # check vector's projection
    check_projection(input_vector, epsg, errors)

    # check vector's name
    if not is_first_char_ascii(input_vector):
        error_msg = (
            f"{input_vector} file's name not correct, "
            "it must start with an ascii letter"
        )
        errors.append(service_error.NamingConvention(error_msg))

    # check field
    if field not in input_vector_fields:
        errors.append(service_error.MissingField(input_vector, field))

    # check field's type
    label_field_type = get_field_type(input_vector, field)
    if label_field_type is not str:
        errors.append(service_error.FieldType(input_vector, field, str))

    # Remove empty geometries
    shape_no_empty = check_empty_geometries(input_vector, errors, do_corrections)

    # remove duplicates features
    shape_no_duplicates = check_duplicates(
        str(input_vector), str(shape_no_empty), errors, do_corrections
    )

    # Check valid geometry
    if not Path(shape_no_duplicates).exists():
        shape_no_duplicates = input_vector

    shape_valid_geom, invalid_geom, _ = check_valid_geom(
        shape_no_duplicates, display=False, do_corrections=do_corrections
    )

    # Remove features with None geometries
    none_geoms = remove_invalid_features(
        str(shape_valid_geom), do_corrections=do_corrections
    )
    invalid_geom += none_geoms

    if invalid_geom != 0:
        error_msg = (
            f"'{input_vector}' contains {invalid_geom} invalid geometries, "
            "see checkRegionDataBase.py -h to correct the database"
        )
        errors.append(service_error.InvalidGeometry(error_msg))

    # Remove small geometries
    nb_too_small_geoms = len(
        get_geometries_by_area(
            input_vector, area=area_threshold, driver_name="ESRI Shapefile"
        )
    )
    if nb_too_small_geoms != 0:
        errors.append(
            service_error.TooSmallRegion(
                input_vector, area_threshold, nb_too_small_geoms
            )
        )
    return errors


def check_projection(input_vector: str, epsg: int, errors: ErrorsList) -> None:
    """
    Check a vector file's projection by reading it and checking if provided epsg is the same

    Parameters
    ----------
    input_vector:
        Path to the shapefile to be analyzed
    epsg:
        Projection code
    errors:
        List of I2Errors
    """
    vector_projection = get_vector_proj(input_vector)
    if vector_projection is None:
        error_msg = f"{input_vector} does not have projection"
        errors.append(service_error.InvalidProjection(error_msg))
    elif int(epsg) != int(vector_projection):
        error_msg = f"{input_vector} projection ({vector_projection}) incorrect"
        errors.append(service_error.InvalidProjection(error_msg))


def check_empty_geometries(
    input_vector: str, errors: ErrorsList, do_corrections: bool
) -> PathLike:
    """
    Check if a shapefile contains empty geometries and remove them if `do_corrections` is true.
    Return the path to the new shapefile

    Parameters
    ----------
    input_vector:
        Path to the input shapefile
    errors:
        List of I2 errors
    do_corrections:
        Flag to indicate whether to apply corrections or not
    """
    shape_no_empty_name = "no_empty.shp"
    shape_no_empty_dir = Path(input_vector).parent
    shape_no_empty = str(Path(shape_no_empty_dir) / shape_no_empty_name)
    shape_no_empty_process, empty_geom_number = check_empty_geom(
        input_vector, do_corrections=do_corrections, output_file=shape_no_empty
    )
    if empty_geom_number != 0:
        error_msg = (
            f"'{input_vector}' contains {empty_geom_number} empty geometries, "
            "see checkRegionDataBase.py -h to correct the database"
        )
        errors.append(service_error.EmptyGeometry(error_msg))
    return shape_no_empty_process


def check_duplicates(
    input_vector: str, shape_no_empty: str, errors: ErrorsList, do_corrections: bool
) -> str:
    """
    Check if a shapefile contains duplicated features and remove them if `do_corrections` is true.
    Return the path to the new shapefile

    Parameters
    ----------
    input_vector:
        Path to the input shapefile
    shape_no_empty:
        Path to the output shapefile
    errors:
        List of I2 errors
    do_corrections:
        Flag to indicate whether to apply corrections or not
    """
    shape_no_duplicates_name = "no_duplicates.shp"
    shape_no_duplicates_dir = Path(input_vector).parent
    shape_no_duplicates = str(Path(shape_no_duplicates_dir) / shape_no_duplicates_name)
    if not Path(shape_no_empty).exists():
        shape_no_empty = input_vector
    (
        shape_no_duplicates_process,
        duplicated_features,
    ) = delete_duplicate_geometries_sqlite(
        shape_no_empty,
        do_corrections=do_corrections,
        output_file=shape_no_duplicates,
        quiet_mode=True,
    )
    if duplicated_features != 0:
        error_msg = (
            f"'{input_vector}' contains {duplicated_features} duplicated features, "
            "see checkRegionDataBase.py -h to correct the database"
        )
        errors.append(service_error.DuplicatedFeatures(error_msg))
    return str(shape_no_duplicates_process)


def main() -> int:
    """Main entry point when using terminal"""
    parent = Path(__file__).resolve().parent.parent
    iota2_scripts_dir = str(parent.parent)

    if iota2_scripts_dir not in sys.path:
        sys.path.append(iota2_scripts_dir)

    description = (
        "check region database\n"
        "\t- remove empty geometries\n"
        "\t- remove duplicate geometries\n"
        "\t- split multi-polygons to polygons\n"
        "\t- check projection\n"
        "\t- check vector's name\n"
        "\t- check if the label field is integer type"
    )
    parser = argparse.ArgumentParser(description)
    parser.add_argument(
        "-in.vector",
        help="absolute path to the vector (mandatory)",
        dest="input_vector",
        required=True,
    )
    parser.add_argument(
        "-out.vector",
        help="output vector",
        dest="output_vector",
        required=False,
        default=None,
    )
    parser.add_argument(
        "-RegionField",
        help="field containing regions (mandatory)",
        dest="region_field",
        required=True,
    )
    parser.add_argument(
        "-epsg",
        help="expected EPSG's code (mandatory)",
        dest="epsg",
        required=True,
        type=int,
    )
    parser.add_argument(
        "-doCorrections",
        help="enable corrections (default = False)",
        dest="do_corrections",
        required=False,
        default=False,
        type=str2bool,
    )
    args = parser.parse_args()

    vector_copy = args.output_vector
    data_source = ogr.Open(args.input_vector)
    drv = data_source.GetDriver()
    drv.CopyDataSource(data_source, vector_copy)

    check_region_database_inplace(
        vector_copy, args.region_field, args.epsg, args.do_corrections
    )
    return 0


if __name__ == "__main__":
    sys.exit(main())
