#!/usr/bin/env python3

# =========================================================================
#   Program:   vector tools
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Tools to check the consistency of the database with iota2's requirements."""
import argparse
import string
import sys
from pathlib import Path

from osgeo import ogr

from iota2.common import service_error
from iota2.common.service_error import I2error
from iota2.common.utils import str2bool
from iota2.typings.i2_types import ErrorsList
from iota2.vector_tools.delete_duplicate_geometries_sqlite import (
    delete_duplicate_geometries_sqlite,
)
from iota2.vector_tools.multipoly_to_poly import multipoly2poly
from iota2.vector_tools.vector_functions import (
    check_empty_geom,
    check_valid_geom,
    get_fields,
    get_vector_proj,
    remove_shape,
)


def get_geometries_by_area(
    input_vector: str, area: float, driver_name: str = "ESRI Shapefile"
) -> list[tuple[ogr.Geometry, float]]:
    """get geometries smaller than the area

    sub geometries of a MULTIPOLYGONS are checked and returned either
    """

    driver = ogr.GetDriverByName(driver_name)
    data_src = driver.Open(input_vector, 0)
    layer_src = data_src.GetLayer()
    output_geoms = []
    for feature in layer_src:
        geom = feature.GetGeometryRef()
        for geom_part in geom:
            geom_area = geom_part.GetArea()
            if geom_area < area:
                output_geoms.append((geom_part.Clone(), geom_area))
    return output_geoms


def vector_name_check(input_vector: str, errors: list[I2error]) -> list[I2error]:
    """Check if first character is a letter."""
    avail_characters = string.ascii_letters
    first_character = Path(input_vector).name[0]
    if first_character not in avail_characters:
        error_msg = (
            f"{input_vector} file's name not correct, "
            "it must start with an ascii character"
        )
        errors.append(service_error.NamingConvention(error_msg))
    return errors


def remove_invalid_features(shapefile: str, do_corrections: bool) -> int:
    """
    Return the sum of invalid features and delete them (inplace) if do_corrections is True.

    Parameters
    ----------
    shapefile:
        path to the shapefile to check
    do_corrections:
        flag to remove invalid features
    """
    none_geom = 0
    driver = ogr.GetDriverByName("ESRI Shapefile")
    data_source = driver.Open(shapefile, 1)
    if not data_source:
        raise OSError(f"Could open the file '{shapefile}'")
    layer = data_source.GetLayer()
    count = layer.GetFeatureCount()
    for feature in range(count):
        feat = layer[feature]
        geom = feat.GetGeometryRef()
        if not geom:
            none_geom += 1
            if do_corrections:
                layer.DeleteFeature(feature)
    layer.ResetReading()
    return none_geom


def check_invalid_features(
    input_vector: str, do_corrections: bool, errors: ErrorsList
) -> tuple[ErrorsList, str]:
    """
    Wrapper for `check_valid_geom` used when checking for errors in a database

    Parameters
    ----------
    input_vector:
        Input shapefile
    do_corrections:
        Flag to remove invalid features
    errors:
        List of all errors
    """
    shape_valid_geometries, invalid_geom, _ = check_valid_geom(
        input_vector, display=False, do_corrections=do_corrections
    )
    if invalid_geom:
        error_msg = f"'{input_vector}' contains {invalid_geom} empty geometries"
        if do_corrections:
            error_msg += " and they were removed"
        errors.append(service_error.InvalidGeometry(error_msg))
    return errors, shape_valid_geometries


def check_vector_proj(input_vector: str, epsg: int, errors: ErrorsList) -> ErrorsList:
    """
    Check a shapefile's projection (not None and the same as the `epsg` parameter)

    Parameters
    ----------
    input_vector:
        Path to input file
    epsg:
        EPSG code for comparison
    errors:
        List of I2Error (used when checking for multiple errors)
    """
    vector_projection = get_vector_proj(input_vector)
    if vector_projection is None:
        error_msg = f"{input_vector} does not have projection"
        errors.append(service_error.InvalidProjection(error_msg))
    elif int(epsg) != int(vector_projection):
        error_msg = f"{input_vector} projection ({vector_projection}) incorrect"
        errors.append(service_error.InvalidProjection(error_msg))

    return errors


def remove_duplicated_features(
    input_vector: str,
    do_corrections: bool,
    errors: ErrorsList,
    output_file: str | None = None,
) -> tuple[ErrorsList, str]:
    """
    Wrapper for `delete_duplicate_geometries_sqlite` used when checking for errors in a database

    Parameters
    ----------
    input_vector:
        Input shapefile
    do_corrections:
        Flag to remove duplicates
    errors:
        List of all errors
    output_file:
        Output file
    """
    # remove duplicates features
    shape_no_duplicates, duplicated_features = delete_duplicate_geometries_sqlite(
        input_vector,
        do_corrections=do_corrections,
        quiet_mode=True,
        output_file=output_file,
    )

    if duplicated_features != 0:
        error_msg = (
            f"'{input_vector}' contains {duplicated_features} duplicated features, "
            "see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.DuplicatedFeatures(error_msg))
    return errors, shape_no_duplicates


def remove_multipolygons(
    input_vector: str,
    output_vector: str,
    do_corrections: bool,
    errors: ErrorsList,
) -> tuple[ErrorsList, str]:
    """
    Wrapper for `multipoly2poly` used when checking for errors in a database

    Parameters
    ----------
    input_vector:
        Input shapefile
    output_vector:
        Output shapefile
    do_corrections:
        Flag to remove multipolygons
    errors:
        List of all errors
    """
    multipolygons_number = multipoly2poly(input_vector, output_vector, do_corrections)
    if multipolygons_number != 0:
        error_msg = (
            f"'{input_vector}' contains {multipolygons_number} MULTIPOLYGON,"
            " see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.ContainsMultipolygon(error_msg))
    return errors, output_vector


def remove_none_geometries(
    input_vector: str, do_corrections: bool, errors: ErrorsList
) -> ErrorsList:
    """
    Wrapper for `remove_invalid_features` used when checking for errors in a database

    Parameters
    ----------
    input_vector:
        Input shapefile
    do_corrections:
        Flag to remove invalid features
    errors:
        List of all errors
    """
    none_geoms = remove_invalid_features(
        str(input_vector), do_corrections=do_corrections
    )
    if none_geoms:
        error_msg = (
            f"'{input_vector}' contains {none_geoms} None geometries, "
            "see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg += " and the were removed"
        errors.append(service_error.InvalidGeometry(error_msg))
    return errors


def check_ground_truth_inplace(
    input_vector: str,
    data_field: str,
    epsg: int,
    do_corrections: bool,
    display: bool = False,
) -> list[I2error]:
    """Check multiple parameters of the shapefile representing reference data.

    Return corresponding errors if needed and applies the required corrections (inplace)
    if do_corrections is True.

    Parameters
    ----------
    input_vector:
        path to shapefile to check
    data_field:
        field name
    epsg:
        projection code
    do_corrections:
        whether to apply corrections or not
    display:
        whether to display the errors or not
    """

    tmp_files = []

    errors: list = []
    # check vector's projection
    errors = check_vector_proj(input_vector, epsg, errors)

    # check vector's name
    errors = vector_name_check(input_vector, errors)

    # check field
    if data_field not in get_fields(input_vector):
        errors.append(service_error.MissingField(input_vector, data_field))

    # geometries checks
    shape_no_empty_in = str(Path(input_vector).parent / "no_empty.shp")
    shape_no_empty, empty_geom_number = check_empty_geom(
        str(input_vector), do_corrections=do_corrections, output_file=shape_no_empty_in
    )
    if empty_geom_number != 0:
        error_msg = (
            f"'{input_vector}' contains {empty_geom_number} empty geometries,"
            " see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.EmptyGeometry(error_msg))
    tmp_files.append(shape_no_empty)

    # remove duplicates features
    shape_no_duplicates = str(Path(input_vector).parent / "no_duplicates.shp")
    if not Path(shape_no_empty).exists():
        shape_no_empty = input_vector
    errors, _ = remove_duplicated_features(
        shape_no_empty, do_corrections, errors, shape_no_duplicates
    )
    tmp_files.append(shape_no_duplicates)

    # remove multipolygons

    shape_no_multi = str(Path(input_vector).parent / "no_multi.shp")
    if not Path(shape_no_duplicates).exists():
        shape_no_duplicates = input_vector

    errors, shape_no_multi_process = remove_multipolygons(
        shape_no_duplicates, shape_no_multi, do_corrections, errors
    )
    shape_no_multi_process = str(shape_no_multi_process)
    tmp_files.append(shape_no_multi_process)

    # Check valid geometry
    input_valid_geom_shape = (
        shape_no_multi_process if do_corrections else shape_no_duplicates
    )
    if not Path(input_valid_geom_shape).exists():
        input_valid_geom_shape = input_vector
    errors, shape_valid_geom = check_invalid_features(
        input_valid_geom_shape, do_corrections, errors
    )

    # remove features with None geometries
    errors = remove_none_geometries(shape_valid_geom, do_corrections, errors)

    tmp_files.append(str(shape_valid_geom))
    for tmp_file in tmp_files:
        if tmp_file is not input_vector and Path(tmp_file).exists():
            remove_shape(tmp_file)
    if display:
        print("\n".join([str(error) for error in errors]))
    return errors


def check_user_ground_truth(
    input_vector: str,
    data_field: str,
    epsg: int,
    do_corrections: bool,
    display: bool = True,
) -> list[I2error]:
    """Check multiple parameters of the shapefile representing reference data.

    return corresponding errors if needed. Also applies the required corrections (inplace)
    if do_corrections is True.

    Parameters
    ----------
    input_vector:
        path to shapefile to check
    data_field:
        field name
    epsg:
        projection code
    do_corrections:
        whether to apply corrections or not
    display:
        whether to display the errors or not
    """
    errors: list[I2error] = []
    # check vector's projection
    errors = check_vector_proj(input_vector, epsg, errors)

    # check vector's name
    errors = vector_name_check(input_vector, errors)

    # check field
    if data_field not in get_fields(input_vector):
        errors.append(service_error.MissingField(input_vector, data_field))

    # geometries checks
    _, empty_geom_number = check_empty_geom(input_vector, do_corrections=do_corrections)
    if empty_geom_number != 0:
        error_msg = (
            f"'{input_vector}' contains {empty_geom_number} empty geometries, "
            "see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.EmptyGeometry(error_msg))

    # remove duplicates features
    errors, shape_no_duplicates = remove_duplicated_features(
        input_vector, do_corrections, errors
    )

    # remove multipolygons
    shape_no_multi = str(Path(input_vector).parent / "no_multi.shp")
    multipolygons_number = multipoly2poly(input_vector, shape_no_multi, do_corrections)

    driver = ogr.GetDriverByName("ESRI shapefile")
    driver.DeleteDataSource(input_vector)
    data_source = ogr.Open(shape_no_multi)
    driver.CopyDataSource(data_source, input_vector)
    driver.DeleteDataSource(shape_no_multi)

    if multipolygons_number != 0:
        error_msg = (
            f"'{input_vector}' contains {multipolygons_number} MULTIPOLYGON, "
            "see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.ContainsMultipolygon(error_msg))

    # Check valid geometry
    input_valid_geom_shape = shape_no_multi if do_corrections else shape_no_duplicates
    if not Path(input_valid_geom_shape).exists():
        input_valid_geom_shape = input_vector
    errors, _ = check_invalid_features(input_valid_geom_shape, do_corrections, errors)

    # remove features with None geometries
    errors = remove_none_geometries(input_vector, do_corrections, errors)

    if display:
        print("\n".join([str(error) for error in errors]))
    return errors


def main() -> int:
    """Main entry point when using terminal"""
    parent = Path(__file__).resolve().parent.parent
    iota2_scripts_dir = str(parent.parent)

    if iota2_scripts_dir not in sys.path:
        sys.path.append(iota2_scripts_dir)

    description = (
        "\ncheck ground truth database\n"
        "\t- remove empty geometries\n"
        "\t- remove duplicate geometries\n"
        "\t- split multi-polygons to polygons\n"
        "\t- check projection\n"
        "\t- check vector's name\n"
        "\t- check if the label field is integer type\n"
    )
    parser = argparse.ArgumentParser(description)
    parser.add_argument(
        "-in.vector",
        help="absolute path to the vector (mandatory)",
        dest="input_vector",
        required=True,
    )
    parser.add_argument(
        "-out.vector",
        help="output vector",
        dest="output_vector",
        required=False,
        default=None,
    )
    parser.add_argument(
        "-dataField",
        help="field containing labels (mandatory)",
        dest="data_field",
        required=True,
    )
    parser.add_argument(
        "-epsg",
        help="expected EPSG's code (mandatory)",
        dest="epsg",
        required=True,
        type=int,
    )
    parser.add_argument(
        "-doCorrections",
        help="enable corrections (default = False)",
        dest="do_corrections",
        required=False,
        default=False,
        type=str2bool,
    )
    args = parser.parse_args()

    vector_copy = args.output_vector
    data_src = ogr.Open(args.input_vector)
    drv = data_src.GetDriver()
    drv.CopyDataSource(data_src, vector_copy)

    check_user_ground_truth(
        vector_copy, args.data_field, args.epsg, args.do_corrections
    )
    return 0


if __name__ == "__main__":
    sys.exit(main())
