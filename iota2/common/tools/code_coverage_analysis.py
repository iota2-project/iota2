# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Tool used to check if code coverage improved"""
import json
import sys
from argparse import ArgumentParser
from pathlib import Path

import numpy as np
import pandas as pd
from tabulate import tabulate

from iota2.typings.i2_types import PathLike, Percent

DEFAULT_NEW_FILE_THRESHOLD = 75
DEFAULT_IGNORE_THRESHOLD = 0.2


def get_cov_df(json_path: PathLike) -> pd.Series:
    """
    Parse a json file and return a dataframe containing file paths as keys and corresponding
    coverage statistics

    Parameters
    ----------
    json_path:
        Path to the json file
    """
    with open(json_path, encoding="utf-8") as json_in:
        data = json.load(json_in)
    file_names = data["files"].keys()
    # find the index of the last occurrence of "iota2" in paths
    iota2_occurences = [
        idx
        for idx, part in enumerate(Path(list(file_names)[0]).parts)
        if part == "iota2"
    ]
    idx_iota2 = iota2_occurences[-1]
    data_for_df = {}
    for file in file_names:
        cropped_path = Path(*Path(file).parts[idx_iota2:])
        file_summary = data["files"][file]["summary"]
        data_for_df[str(cropped_path)] = file_summary

    data_frame = pd.DataFrame(data_for_df, dtype=np.float64).T
    return data_frame["percent_covered"]


def color_values(
    percent_covered: float, merge_cov: float = 0, threshold: float = 75
) -> str:
    """
    Add an ANSI color code and an up/down arrow depending on the value
    """
    if pd.isna(percent_covered):
        if merge_cov < threshold:
            return f"\033[91m{merge_cov} %\033[0m"  # red + % symbol
        return f"\033[92m{merge_cov} %\033[0m"  # red + % symbol
    if percent_covered < 0:
        return f"\033[91m{percent_covered} \u2B07\033[0m"  # red and down arrow
    if percent_covered == 0:
        return f"{percent_covered}"
    return f"\033[92m{percent_covered} \u2B06\033[0m"  # green and up arrow


def convert_nans_diff(percent_covered: float, merge_cov: float = 0) -> float:
    """
    If a file is new, its difference is nan. This function converts these nan values to the actual
    file coverage instead.
    """
    if pd.isna(percent_covered):
        return merge_cov
    return percent_covered


def compute_coverage_diff(
    develop_coverage: PathLike,
    merge_coverage: PathLike,
    new_files_threshold: Percent = DEFAULT_NEW_FILE_THRESHOLD,
    ignore_threshold: Percent = DEFAULT_IGNORE_THRESHOLD,
    html_file_path: PathLike | None = None,
) -> tuple[int, int]:
    """
    Compute the difference of code coverage. Each json file is read into a pandas dataframe, then
    assembled into one dataframe, also containing the difference between code coverage of the
    develop branch and the branch to be merged. Files with unchanged coverage are ignored, as well
    as files with small changes (defined by `ignore_threshold`). Newly added files are accepted if
    their coverage is greater than `new_files_threshold`.

    Parameters
    ----------
    develop_coverage:
        Path to the develop's code coverage json file
    merge_coverage:
        Path to the branch's code coverage json file
    new_files_threshold:
        Threshold for new files' code coverage
    ignore_threshold:
        Threshold for ignoring small changes in files' coverage
    html_file_path:
        Path to the output HTML file (if not set, the file is not produced)

    Notes
    -----
    The function returns the number of files with incorrect code coverage (worse than on develop or
    new files with coverage lower than the accepted threshold)
    """
    # check if thresholds are valid
    assert 0 <= new_files_threshold <= 100
    assert 0 <= ignore_threshold <= 100

    # read json files
    develop_df = get_cov_df(develop_coverage)
    merge_df = get_cov_df(merge_coverage)

    # compute difference and convert to numbers
    coverage_difference_series = merge_df.subtract(develop_df)
    coverage_difference = coverage_difference_series.to_frame()
    coverage_difference.insert(1, "develop_cov", develop_df)
    coverage_difference.insert(2, "merge_cov", merge_df)
    coverage_difference.sort_values(by="percent_covered", inplace=True)
    # coverage_difference is a dataframe
    coverage_difference = coverage_difference.applymap(  # type: ignore
        lambda x: pd.to_numeric(x, errors="coerce")
    )

    # remove unchanged files + ignored ones, based on threshold
    coverage_difference.drop(
        coverage_difference[
            abs(coverage_difference["percent_covered"]) < ignore_threshold
        ].index,
        inplace=True,
    )

    # number of existing files with worse coverage or new files with coverage < threshold
    n_wrong_new_files: int = (
        (coverage_difference["percent_covered"].isna())
        & (coverage_difference["merge_cov"] < new_files_threshold)
    ).sum()
    total_wrong_files = (
        coverage_difference["percent_covered"] < 0
    ).sum() + n_wrong_new_files

    # number of existing files with better coverage and new files with coverage > threshold
    n_good_new_files: int = (
        (coverage_difference["percent_covered"].isna())
        & (coverage_difference["merge_cov"] > new_files_threshold)
    ).sum()
    total_good_files = (
        coverage_difference["percent_covered"] > 0
    ).sum() + n_good_new_files

    coverage_difference = coverage_difference.round(1)

    # copy df for later use to produce HTML file
    df_for_html = coverage_difference.copy()

    # format strings for a "beautiful" print
    coverage_difference["percent_covered"] = coverage_difference.apply(
        lambda x: color_values(
            x["percent_covered"], x["merge_cov"], new_files_threshold
        ),
        axis=1,
    )
    # coverage_difference is a dataframe
    coverage_difference[["develop_cov", "merge_cov"]] = coverage_difference[  # type: ignore
        ["develop_cov", "merge_cov"]
    ].applymap(
        lambda x: f"{x} %"
    )
    coverage_difference["develop_cov"] = np.where(
        coverage_difference["develop_cov"] == "nan %",
        "new file",
        coverage_difference["develop_cov"],
    )
    print(
        tabulate(
            coverage_difference,
            headers=[
                "Files",
                "Difference",
                "Develop coverage",
                "Incoming merge coverage",
            ],
            tablefmt="github",
            colalign=("left", "center", "center", "center"),
        )
    )

    if html_file_path:
        # HTML styling
        df_for_html.columns = [
            "Difference",
            "Develop coverage",
            "Incoming merge coverage",
        ]
        # create a dict for colors based on conditions
        style_dict = {}
        for index, row in df_for_html.iterrows():
            # if difference is nan i.e. the file is new
            if pd.isnull(row["Difference"]):
                if row["Incoming merge coverage"] < new_files_threshold:
                    style_dict[index] = "color: red"
                else:
                    style_dict[index] = "color: green"
            else:
                if row["Difference"] < 0:  # coverage is worse than before
                    style_dict[index] = "color: red"
                else:  # coverage is better than before
                    style_dict[index] = "color: green"

        # convert nan values to text
        df_for_html["Difference"] = df_for_html.apply(
            lambda x: convert_nans_diff(x["Difference"], x["Incoming merge coverage"]),
            axis=1,
        )
        df_for_html["Develop coverage"] = np.where(
            df_for_html["Develop coverage"].isna(), 0, df_for_html["Develop coverage"]
        )

        # apply style (colors and string format)
        def apply_style(row: pd.Series, styles: dict[str, str]) -> list[str]:
            return [styles[row.name]] * len(row)

        styled_df = df_for_html.style.apply(apply_style, styles=style_dict, axis=1)
        styled_df = styled_df.format(
            {
                "Difference": "{:.2f}%".format,
                "Develop coverage": "{:.2f}%".format,
                "Incoming merge coverage": "{:.2f}%".format,
            }
        )

        if Path(html_file_path).exists():
            Path(html_file_path).unlink()
        styled_df.to_html(html_file_path)
    print(f"\033[91m{total_wrong_files} files with invalid coverage\033[0m")
    print(f"\033[92m{total_good_files} files with valid coverage\033[0m")
    return total_wrong_files, total_good_files


def compare_global_coverage(develop_json: PathLike, merge_json: PathLike) -> None:
    """
    Print the global code coverage difference

    Parameters
    ----------
    develop_json:
        Path to the json file containing develop's code coverage
    merge_json
        Path to the json file containing the branch's code coverage
    """
    with open(develop_json, encoding="utf-8") as json_in:
        data = json.load(json_in)
    develop_coverage = data["totals"]["percent_covered"]
    with open(merge_json, encoding="utf-8") as json_in:
        data = json.load(json_in)
    merge_coverage = data["totals"]["percent_covered"]
    diff = merge_coverage - develop_coverage
    # print(f"Develop's global coverage: {develop_coverage:.2f}%")
    # print(f"Current branch's global coverage: {merge_coverage:.2f}%")
    if diff < 0:
        color = "\033[91m"
        sign = ""
    else:
        color = "\033[92m"
        sign = "+"
    print(
        f"{color}Coverage difference: {sign}{diff:.2f}%"
        f"\033[0m ({develop_coverage:.2f}% -> {merge_coverage:.2f}%)"
    )


def arg_parser() -> ArgumentParser:
    """
    Parse arguments for main function
    """
    parser = ArgumentParser(
        description="Compute the difference of code coverage. Each json file is read into a "
        "pandas dataframe, then assembled into one dataframe, also containing the "
        "difference between code coverage of the develop branch and the branch to be "
        "merged. Files with unchanged coverage are ignored, as well as files with "
        "small changes (defined by `ignore_threshold`). Newly added files are "
        "accepted if their coverage is greater than `new_files_threshold`."
    )
    parser.add_argument("-develop", help="Path to develop's json file", type=str)
    parser.add_argument("-merge", help="Path to branch's json file", type=str)
    parser.add_argument(
        "-new_file_threshold",
        help="Threshold for expected coverage of newly created files",
        type=float,
        default=DEFAULT_NEW_FILE_THRESHOLD,
    )
    parser.add_argument(
        "-ignore_threshold",
        help="Threshold for ignoring small changes in coverage",
        type=float,
        default=DEFAULT_IGNORE_THRESHOLD,
    )
    parser.add_argument(
        "-html_file",
        help="If set, save results to an HTML file",
        type=str,
        default=None,
    )
    return parser


def main() -> None:
    """
    Main CLI entry point for the tool
    """
    parser = arg_parser()
    args = parser.parse_args()
    develop_cov = args.develop
    merge_cov = args.merge
    new_file_cover_threshold = args.new_file_threshold
    ignore_threshold = args.ignore_threshold
    html_file = args.html_file
    n_errors, _ = compute_coverage_diff(
        develop_cov,
        merge_cov,
        new_file_cover_threshold,
        ignore_threshold,
        html_file,
    )
    compare_global_coverage(develop_cov, merge_cov)

    if n_errors:
        sys.exit(1)
    sys.exit(0)


if __name__ == "__main__":
    main()
