# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Create a shapefile from each region and tile's envelope"""
import logging
from pathlib import Path

from iota2.common import file_utils as fu
from iota2.common.utils import run
from iota2.typings.i2_types import ListPathLike, PathLike
from iota2.vector_tools import vector_functions as vf

logger = logging.getLogger("distributed.worker")


def split_vector_layer(
    shp_in: str,
    attribute: str,
    attribute_type: str,
    field_vals: list,
    output_path: PathLike,
) -> list[Path]:
    """Split a vector layer in function of its attribute.

    Parameters
    ----------
    shp_in
        input shapefile
    attribute
        columns name
    attribute_type
        attribute type which could be "string" or "int"

    Note
    ----
    return a list of shapeFiles
    """
    shp_out_list = []
    name = Path(shp_in).stem

    if attribute_type == "string":
        for val in field_vals:
            if val != "None":
                shp_out = Path(output_path) / f"{name}_region_{val}.shp"
                if not Path(shp_out).is_file():
                    cmd = (
                        f"ogr2ogr -where '{attribute} = \"{val}\"' {shp_out} {shp_in} "
                    )
                    run(cmd)
                shp_out_list.append(shp_out)

    elif attribute_type == "int":
        for val in field_vals:
            shp_out = Path(output_path) / f"{name}_region_{val}.shp"

            if not Path(shp_out).is_file():
                cmd = f"ogr2ogr -where '{attribute} = {val}' {shp_out} {shp_in}"
                run(cmd)
            shp_out_list.append(shp_out)
    else:
        raise Exception(
            f"Error for attribute_type {attribute_type}, ! Should be 'string' or 'int'"
        )

    return shp_out_list


def create_regions_by_tiles(
    region_shape: str,
    field_region: str,
    env_path: PathLike,
    output_path: PathLike,
    working_directory: PathLike | None = None,
) -> ListPathLike:
    """
    Create a shapeFile into tile's envelope for each regions in region_shape and for each tile.

    Parameters
    ----------
    region_shape
        the shape which contains all regions
    field_region
        the field into the region's shape which describes each tile belong to which model
    env_path
        path to the tile's envelope with priority
    output_path
        path to store all resulting shapeFile
    working_directory
        path to working directory (not mandatory)
    """

    if working_directory is None:
        path_name = output_path
    else:
        path_name = working_directory

    all_tiles = fu.file_search_and(env_path, True, ".shp")
    region_list = vf.get_field_element(region_shape, int, field_region, "unique")
    assert region_list
    shp_region_list = split_vector_layer(
        str(region_shape), field_region, "int", region_list, path_name
    )
    all_clip = []
    for shp in shp_region_list:
        for tile in all_tiles:
            path_to_clip = vf.clip_vector_data(shp, tile, path_name)
            all_clip.append(path_to_clip)

    if working_directory:
        for clip in all_clip:
            vf.copy_shapefile(str(clip), str(output_path))

    else:
        for shp in shp_region_list:
            vf.remove_shape(shp)

    return all_clip
