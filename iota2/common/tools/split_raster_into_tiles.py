#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""command line interface to split a large raster covering multiple tiles
and reproject onto these tiles"""

from pathlib import Path

from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.typings.i2_types import OtbPixType


def reproject(
    raster_data: Path,
    tile_folder: Path,
    output_folder: Path,
    pixel_type: OtbPixType,
) -> None:
    """
    Reproject using otb superimpose

    When using exogenous data in external features on multiple tiles,
    you might want to project your data on the individual tiles.

    Parameters
    ----------
    raster_data:
        raster data to reproject on tiles
    tile_folder:
        folder containing tiles images, names will be appended in output
    output_folder:
        output folder that will contain images names {raster_data}_{tile_name}.tif
    pixel_type:
        one of the following values
        complexDouble, complexFloat, double, float, int16, int32, uint16, uint32, uint8

    """
    # creates output directory
    output_folder.mkdir(exist_ok=True)

    # iterate over tiles
    for tile in tile_folder.iterdir():
        # check name validity
        if tile.suffix != ".tif":
            print(f"ignoring {tile.name}")
            continue
        output_image = output_folder / f"{raster_data.stem}_{tile.name}"
        superimpose_application, _ = create_application(
            AvailableOTBApp.SUPERIMPOSE,
            {
                "inr": str(tile),
                "inm": str(raster_data),
                "out": str(output_image),
                "pixType": pixel_type,
            },
        )
        # execute the app and write output (could be done in parallel)
        print(f"writing {output_image.name}")
        superimpose_application.ExecuteAndWriteOutput()
