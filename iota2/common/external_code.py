"""S2 tests utilities functions."""

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
from itertools import product

import numpy as np
import rasterio
from numpy import ma

from iota2.common.custom_numpy_features import DataContainer
from iota2.learning.utils import I2Label, I2TemporalLabel


def raise_label_exception(
    self: DataContainer,
) -> tuple[np.ndarray, list[I2TemporalLabel]]:
    """Utility function"""
    labels = [
        I2TemporalLabel(sensor_name="sentinel2", feat_name="new_b2", date=date)
        for date in self.interpolated_dates["Sentinel2"]
    ]
    return self.get_interpolated_Sentinel2_B2(), labels


def get_identity(self: DataContainer) -> tuple[np.ndarray, list[I2TemporalLabel]]:
    """Utility function"""
    labels = [
        I2TemporalLabel(sensor_name="sentinel2", feat_name="newb2", date=date)
        for date in self.interpolated_dates["Sentinel2"]
    ]
    return self.get_interpolated_Sentinel2_B2(), labels


def gen_2000_features_interp(
    self: DataContainer,
) -> tuple[np.ndarray, list[I2Label]]:
    """Function to generate 2000 external features"""
    x, y, _ = np.shape(self.get_interpolated_Sentinel2_B4())
    nb_date = len(self.interpolated_dates["Sentinel2"])
    coef = np.ones((x, y, (2000 // nb_date + 1) * nb_date), dtype=np.float32)
    feat_names = ["feat" + str(num) for num in np.arange(0, (2000 // nb_date + 1))]
    labels = [
        I2TemporalLabel(sensor_name="sentinel2", feat_name=feat_name, date=date)
        for feat_name, date in list(
            product(feat_names, self.interpolated_dates["Sentinel2"])
        )
    ]
    return coef, labels


def gen_2000_features(
    self: DataContainer,
) -> tuple[np.ndarray, list[I2TemporalLabel]]:
    """Function to generate 2000 external features"""
    masks, label_masks = self.get_filled_masks()
    x, y, nb_date = np.shape(masks)
    coef = np.ones((x, y, (2000 // nb_date + 1) * nb_date), dtype=np.float32)
    feat_names = ["feat" + str(num) for num in np.arange(0, (2000 // nb_date + 1))]
    labels = [
        I2TemporalLabel(sensor_name="sentinel2", feat_name=feat_name, date=label.date)
        for feat_name, label in list(product(feat_names, label_masks))
    ]
    return coef, labels


def get_mnt(self: DataContainer) -> tuple[np.ndarray, list[I2Label]]:
    """Utility function"""
    data = self.get_interpolated_userfeatures_mnt()
    _, _, mnt_bands = data.shape
    return data, [I2Label(sensor_name="mnt", feat_name=num) for num in range(mnt_bands)]


def test_get_band(
    self: DataContainer, band: int = 0, label: str = "none"
) -> tuple[np.ndarray, list[str]] | None:
    """
    Get the band defined by integer with a label given by the string

    Parameters
    ----------
    band:
        must be an integer among 2,3,4
    label:
        the string you want
    """
    if band == 2:
        return self.get_interpolated_Sentinel2_B2(), [label]
    if band == 3:
        return self.get_interpolated_Sentinel2_B3(), [label]
    if band == 4:
        return self.get_interpolated_Sentinel2_B4(), [label]
    raise ValueError(f"band must be 2, 3 or 4, not {band}")


def get_exception() -> None:
    """this function raises an exception
    it is used to simulate an external feature that fails"""
    raise Exception("I'm a bad external feature")


def test_index_sum(self: DataContainer) -> tuple[np.ndarray, list]:
    """
    Get a sum of test indexes
    """
    coef = self.get_interpolated_Sentinel2_B2() + self.get_interpolated_Sentinel2_B4()
    labels: list = []
    return np.sum(coef, axis=2), labels


def test_index(self: DataContainer) -> tuple[np.ndarray, list]:
    """
    Get a test index
    """
    coef = self.get_interpolated_Sentinel2_B2() + self.get_interpolated_Sentinel2_B4()
    labels: list = []
    return coef, labels


def duplicate_ndvi(self: DataContainer) -> tuple[np.ndarray, list[str]]:
    """Utility function"""
    ndvi = self.get_interpolated_Sentinel2_NDVI()
    labels = [f"dupndvi_{i+1}" for i in range(ndvi.shape[2])]
    return ndvi, labels


def get_ndvi(self: DataContainer) -> tuple[np.ndarray, list[I2Label]]:
    """Utility function"""
    coef = (
        self.get_interpolated_Sentinel2_B8() - self.get_interpolated_Sentinel2_B4()
    ) / (
        self.get_interpolated_Sentinel2_B4()
        + self.get_interpolated_Sentinel2_B8()
        + 1e-9
    )
    labels = [
        I2Label(sensor_name="ndvi", feat_name=i + 1) for i in range(coef.shape[2])
    ]
    return coef, labels


def custom_function(self: DataContainer) -> np.ndarray:
    """Utility function"""
    print(dir(self))
    print(self.get_interpolated_Sentinel2_b1(), self.get_interpolated_Sentinel2_b2())
    return self.get_interpolated_Sentinel2_b1() + self.get_interpolated_Sentinel2_b2()


def custom_function_inv(self: DataContainer) -> np.ndarray:
    """Utility function"""
    print(dir(self))
    print(self.get_interpolated_Sentinel2_b1(), self.get_interpolated_Sentinel2_b2())
    return self.get_interpolated_Sentinel2_b1() - self.get_interpolated_Sentinel2_b2()


def get_ndsi(self: DataContainer) -> tuple[np.ndarray, list[str]]:
    """Utility function"""
    coef = (
        self.get_interpolated_Sentinel2_B3() - self.get_interpolated_Sentinel2_B11()
    ) / (self.get_interpolated_Sentinel2_B3() + self.get_interpolated_Sentinel2_B11())
    labels = [f"ndsi_{i+1}" for i in range(coef.shape[2])]
    print("out custom features")
    return coef, labels


def get_cari(self: DataContainer) -> tuple[np.ndarray, list[str]]:
    """Utility function"""
    a = (  # pylint: disable=invalid-name
        (
            (
                self.get_interpolated_Sentinel2_B5()
                - self.get_interpolated_Sentinel2_B3()
            )
            / 150
        )
        * 670
        + self.get_interpolated_Sentinel2_B4()
        + (
            self.get_interpolated_Sentinel2_B3()
            - (
                (
                    self.get_interpolated_Sentinel2_B5()
                    - self.get_interpolated_Sentinel2_B3()
                )
                / 150
            )
            * 550
        )
    )
    b = (  # pylint: disable=invalid-name
        (self.get_interpolated_Sentinel2_B5() - self.get_interpolated_Sentinel2_B3())
        / (150 * 150)
    ) + 1
    coef = (
        self.get_interpolated_Sentinel2_B5() / self.get_interpolated_Sentinel2_B4()
    ) * ((np.sqrt(a * a)) / (np.sqrt(b)))
    labels = [f"cari_{i+1}" for i in range(coef.shape[2])]

    return coef, labels


def get_red_edge2(self: DataContainer) -> tuple[np.ndarray, list[str]]:
    """Utility function"""
    coef = (
        self.get_interpolated_Sentinel2_B5() - self.get_interpolated_Sentinel2_B4()
    ) / (self.get_interpolated_Sentinel2_B5() + self.get_interpolated_Sentinel2_B4())
    labels = [f"rededge2_{i+1}" for i in range(coef.shape[2])]
    return coef, labels


def get_gndvi(self: DataContainer) -> tuple[np.ndarray, list[str]]:
    """
    compute the Green Normalized Difference Vegetation Index
    DO NOT USE this except for test as Sentinel2 has not B9
    """
    coef = (
        self.get_interpolated_Sentinel2_B9() - self.get_interpolated_Sentinel2_B3()
    ) / (self.get_interpolated_Sentinel2_B9() + self.get_interpolated_Sentinel2_B3())
    labels = [f"gndvi_{i+1}" for i in range(coef.shape[2])]
    return coef, labels


def get_soi(self: DataContainer) -> tuple[np.ndarray, list[I2Label]]:
    """Compute the Soil Composition Index.

    Functions get_interpolated_Sentinel2_B2() returns numpy array
     of size N * M * D where:
    - N the number of row, computed by iota2
    - M the number of columns, computed by iota2
    - D the number of dimension == the number of dates (being masked or not)


    The output is a tuple of coef and labels.
    - Labels must be a list of I2Label or I2TemporalLabel (could be empty)
    - Coef is a numpy array, with an expected shape of N * M * X


    Where X is a dimension, which can be different to D, from 1 to infinite

    The user can force the output data type by converting it using `astype`
     built-in numpy array function.

    The user must manage numerically issues in their codes, such as
     division by 0 or type overflow.

    Parameters
    ----------
    self:
        self is the only parameter for external functions

    """
    coef = (
        self.get_interpolated_Sentinel2_B11() - self.get_interpolated_Sentinel2_B8()
    ) / (self.get_interpolated_Sentinel2_B11() + self.get_interpolated_Sentinel2_B8())
    labels = [I2Label(sensor_name="soi", feat_name=i + 1) for i in range(coef.shape[2])]
    return coef, labels


def get_raw_data(self: DataContainer) -> tuple[np.ndarray, list[I2TemporalLabel]]:
    """Utility function"""
    coef, labels_refl = self.get_filled_stack()
    masks, label_masks = self.get_filled_masks()
    coef = np.concatenate(  # pylint: disable=unexpected-keyword-arg
        (coef, masks), axis=2, dtype=np.float32
    )
    labels = labels_refl + label_masks
    return coef, labels


def use_raw_data(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = self.get_raw_Sentinel2_B2()
    labels: list = []
    return coef, labels


def use_exogeneous_data(self: DataContainer) -> tuple[np.ndarray, list[I2Label]]:
    """Utility function"""
    exo_data = self.get_exogeneous_data()
    _, _, nb_exo_bands = exo_data.shape
    labels = [
        I2Label(sensor_name="exo", feat_name=exo_ind) for exo_ind in range(nb_exo_bands)
    ]
    return exo_data, labels


def use_coordinates(self: DataContainer) -> tuple[np.ndarray, list[I2Label]]:
    """Utility function"""
    affine, y_size, x_size, _ = self.transform
    (pix_size_x, _, origin_x, _, pix_size_y, origin_y, _, _, _) = tuple(affine)
    lon_min, lon_max = origin_x, origin_x + x_size * pix_size_x
    lat_min, lat_max = origin_y, origin_y + y_size * pix_size_y
    lon = np.linspace(lon_min, lon_max, num=int(x_size))
    lat = np.linspace(lat_min, lat_max, num=int(y_size))

    lon_grid, lat_grid = np.meshgrid(lon, lat)
    coords = np.stack((lon_grid, lat_grid), axis=-1)
    labels = [
        I2Label(sensor_name="coord", feat_name="lon"),
        I2Label(sensor_name="coord", feat_name="lat"),
    ]
    return coords, labels


def use_raw_and_masks_data(self: DataContainer) -> tuple[np.ndarray, list]:
    """
    This function use raw data and masks

    This test function get the B2 from Sentinel2 and set to
    -1000 all pixels masked

    The output is converted to int16.
    """
    band = self.get_raw_Sentinel2_B2()
    mask = self.get_Sentinel2_binary_masks()
    coef: np.ndarray
    coef = ma.masked_array(band, mask=mask)
    coef = coef.filled(-1000)
    return coef.astype("int16"), []


def get_lat_lon_as_features(self):
    """
    Return a numpy array where the first band contains x-coordinates of the pixels and the second
    band contains the y-coordinates of the pixels. Adapted for deep learning.
    """
    transform, xsize, ysize = self.transform
    cols, rows = np.meshgrid(np.arange(xsize), np.arange(ysize))
    lons, lats = rasterio.transform.xy(transform, rows, cols)

    # band 1 contains latitude
    # band 2 contains longitude
    labels = [
        I2Label(sensor_name="latlon", feat_name="latitude"),
        I2Label(sensor_name="latlon", feat_name="longitude"),
    ]
    # xsize and ysize must be inverted to have the correct shape
    lon = np.reshape(lons, (ysize, xsize, 1))
    lat = np.reshape(lats, (ysize, xsize, 1))
    coef = np.concatenate((lat, lon), axis=2)

    return coef, labels


# ############################################################################
# DHI INDEX
# ############################################################################


def get_cumulative_productivity(
    self: DataContainer,
) -> tuple[np.ndarray, list[I2Label]]:
    """Utility function"""
    coef = np.sum(self.get_interpolated_Sentinel2_NDVI(), axis=2)
    return coef, [I2Label(sensor_name="cumul", feat_name="prod")]


def get_minimum_productivity(
    self: DataContainer,
) -> tuple[np.ndarray, list[I2Label]]:
    """Utility function"""
    coef = np.min(self.get_interpolated_Sentinel2_NDVI(), axis=2)
    return coef, [I2Label(sensor_name="min", feat_name="prod")]


def get_seasonal_variation(
    self: DataContainer,
) -> tuple[np.ndarray, list[I2Label]]:
    """Utility function"""
    coef = np.std(self.get_interpolated_Sentinel2_NDVI(), axis=2) / (
        np.mean(self.get_interpolated_Sentinel2_NDVI(), axis=2) + 1e-6
    )
    return coef, [I2Label(sensor_name="var", feat_name="prod")]


# ###########################################################################
# Functions for testing all sensors
# ###########################################################################


def test_index_sum_l5_old(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = (
        self.get_interpolated_Landsat5Old_B2() + self.get_interpolated_Landsat5Old_B4()
    )
    labels: list = []
    return np.sum(coef, axis=2), labels


def test_index_l5_old(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = (
        self.get_interpolated_Landsat5Old_B2() + self.get_interpolated_Landsat5Old_B4()
    )
    labels: list = []
    return coef, labels


def test_index_sum_l8_old(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = (
        self.get_interpolated_Landsat8Old_B2() + self.get_interpolated_Landsat8Old_B4()
    )
    labels: list = []
    return np.sum(coef, axis=2), labels


def test_index_l8_old(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = (
        self.get_interpolated_Landsat8Old_B2() + self.get_interpolated_Landsat8Old_B4()
    )
    labels: list = []
    return coef, labels


def test_index_sum_l8(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = self.get_interpolated_Landsat8_B2() + self.get_interpolated_Landsat8_B4()
    labels: list = []
    return np.sum(coef, axis=2), labels


def test_index_l8(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = self.get_interpolated_Landsat8_B2() + self.get_interpolated_Landsat8_B4()
    labels: list = []
    return coef, labels


def test_index_sum_s2_s2c(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = (
        self.get_interpolated_Sentinel2S2C_B02()
        + self.get_interpolated_Sentinel2S2C_B04()
    )
    labels: list = []
    return np.sum(coef, axis=2), labels


def test_index_s2_s2c(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = (
        self.get_interpolated_Sentinel2S2C_B02()
        + self.get_interpolated_Sentinel2S2C_B04()
    )
    labels: list = []
    return coef, labels


def test_index_sum_s2_l3a(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = (
        self.get_interpolated_Sentinel2L3A_B2()
        + self.get_interpolated_Sentinel2L3A_B4()
    )
    labels: list = []
    return np.sum(coef, axis=2), labels


def test_index_s2_l3a(self: DataContainer) -> tuple[np.ndarray, list]:
    """Utility function"""
    coef = (
        self.get_interpolated_Sentinel2L3A_B2()
        + self.get_interpolated_Sentinel2L3A_B4()
    )
    labels: list = []
    return coef, labels


def get_soi_s2(self: DataContainer) -> tuple[np.ndarray, list[I2TemporalLabel]]:
    """
    compute the Soil Composition Index
    """
    dates = self.get_interpolated_dates()
    coef = (
        self.get_interpolated_Sentinel2_B11() - self.get_interpolated_Sentinel2_B8()
    ) / (
        self.get_interpolated_Sentinel2_B11()
        + self.get_interpolated_Sentinel2_B8()
        + 1.0e-6
    )
    labels = [
        I2TemporalLabel(sensor_name="sentinel2", feat_name="soi", date=date)
        for date in dates["Sentinel2"]
    ]
    return coef, labels


# ##########################################################
# Coordinates
# ##########################################################


def get_s2_coord(self):
    """
    Return a numpy array where the first band contains x-coordinates of the pixels and the second
    band contains the y-coordinates of the pixels. Adapted for deep learning.
    """
    transform, nb_rows, nb_cols, _ = self.transform
    # xsize and ysize are inverted here, maybe the output of OTB
    cols, rows = np.meshgrid(np.arange(nb_cols), np.arange(nb_rows))
    x_geo, y_geo = rasterio.transform.xy(transform, rows, cols)

    # band 1 contains latitude
    # band 2 contains longitude
    labels = [
        I2Label(sensor_name="coord", feat_name="coordx"),
        I2Label(sensor_name="coord", feat_name="coordy"),
    ]
    lat = np.reshape(x_geo, (nb_rows, nb_cols, 1))
    lon = np.reshape(y_geo, (nb_rows, nb_cols, 1))
    coef = np.concatenate((lat, lon), axis=2)

    return coef, labels


def get_s2_coord_for_dl(self):
    """
    Return a numpy array where the first band contains x-coordinates of the pixels and the second
    band contains the y-coordinates of the pixels. Adapted for deep learning.
    """
    transform, nb_rows, nb_cols, _ = self.transform
    dates = self.get_interpolated_dates()
    cols, rows = np.meshgrid(np.arange(nb_cols), np.arange(nb_rows))
    x_geo, y_geo = rasterio.transform.xy(transform, rows, cols)

    # band 1 contains latitude
    # band 2 contains longitude
    lat = np.reshape(x_geo, (nb_rows, nb_cols, 1))
    lon = np.reshape(y_geo, (nb_rows, nb_cols, 1))
    labels = []
    list_coef = []
    for date in dates["Sentinel2"]:
        labels.append(I2Label(sensor_name="Sentinel2", feat_name=f"coordx{date}"))
        labels.append(I2Label(sensor_name="Sentinel2", feat_name=f"coordy{date}"))
        list_coef.append(lat)
        list_coef.append(lon)
    coef = np.dstack(list_coef)
    return coef, labels


def get_raw_indices(self: DataContainer) -> tuple[np.ndarray, list[I2Label]]:
    """
    Compute NDVI, NDWI and brightness from raw dates
    """
    coef, _ = self.get_filled_stack()
    _, labels = self.get_filled_masks()

    green = coef[:, :, 1::10].astype(float)  # 3
    red = coef[:, :, 2::10].astype(float)  # 4
    nir = coef[:, :, 6::10].astype(float)  # 8

    ndvi_coef = 1000 * ((nir - red) / (nir + red + 1e-6))

    ndvi_labels = []
    for label in labels:
        label_ndvi = I2TemporalLabel(
            sensor_name="sentinel2", feat_name="ndvi", date=label.date
        )
        ndvi_labels.append(label_ndvi)

    ndwi_coef = 1000 * ((green - nir) / (green + nir + 1e-6))
    ndwi_labels = []
    for label in labels:
        label_ndwi = I2TemporalLabel(
            sensor_name="sentinel2", feat_name="ndwi", date=label.date
        )
        ndwi_labels.append(label_ndwi)

    brightness_coef = np.sqrt(green**2 + red**2) / 2
    brightness_labels = []
    for label in labels:
        label_brightness = I2TemporalLabel(
            sensor_name="sentinel2", feat_name="brightness", date=label.date
        )
        brightness_labels.append(label_brightness)

    return (
        np.concatenate([ndvi_coef, ndwi_coef, brightness_coef], axis=2),
        ndvi_labels + ndwi_labels + brightness_labels,
    )
