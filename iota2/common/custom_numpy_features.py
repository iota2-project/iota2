#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""This module allows the use of python functions in an OTB pipeline"""
import inspect
import logging
from collections import namedtuple
from dataclasses import dataclass, field
from functools import partial

import affine
import numpy as np
import rasterio as rio
from osgeo import osr
from rasterio.plot import reshape_as_image
from rasterio.transform import Affine

from iota2.common import file_utils as fu
from iota2.common import raster_utils as ru
from iota2.common.i2_constants import Iota2Constants
from iota2.common.raster_utils import (
    OtbPipelineCarrier,
    PipelinesData,
    UserDefinedFunction,
    UserDefinedFunctionPayload,
    apply_udf_to_pipeline,
    gdal_transform_to_rio,
    pipelines_chunking,
    write_array,
)
from iota2.common.service_error import FeaturesShapeError
from iota2.configuration_files.check_config_parameters import (
    search_external_features_function,
)
from iota2.learning.utils import I2Label, I2TemporalLabel, i2_label_factory
from iota2.sensors.sensorscontainer import GenericSensor, SensorsContainer, SensorType
from iota2.sensors.sentinel1 import Sentinel1
from iota2.sensors.userfeatures import UserFeatures
from iota2.typings.i2_types import (
    FunctionNameWithParams,
    I2LabelAlias,
    OtbImage,
    PathLike,
    SensorsDates,
    SensorsParameters,
)

I2_CONST = Iota2Constants()

LOGGER = logging.getLogger("distributed.worker")
Transform = namedtuple("Transform", ["transform", "nb_rows", "nb_cols", "projection"])


@dataclass
class RawStructure:
    """
    Dataclass containing the structure of raw data in the stack (i.e. indices of relevant data)

    Attributes
    ----------
    list_of_bands: list[str]
        List of band names
    list_sensors: list[str]
        List of all sensors
    masks_indices: list[range]
        List of indices indicating the positions of the masks in the stack for each sensor
    masks_shift: int
        Index of the first mask array in the stack, i.e. the shift to apply to get the masks
    shift: int
        Index of the first raw array in the stack, i.e. the shift to apply to get the raw data
    time_series_indices: list[list[int]]
    """

    list_of_bands: list[str]
    list_sensors: list[str]
    masks_indices: list[range]
    masks_shift: int
    shift: int
    time_series_indices: list[list[int]]


@dataclass
class GapStructure:
    """
    Dataclass containing the structure of gapfilled data in the stack (indices of relevant data)

    Attributes
    ----------
    list_of_bands: list[str]
        List of band names
    list_sensors: list[str]
        List of all sensors
    shift: int
        Index of the first raw array in the stack (the shift to apply to get the gapfilled data)
    time_series_indices: list[list[int]]
    """

    list_of_bands: list[str]
    list_sensors: list[str]
    shift: int
    time_series_indices: list[list[int]]


def exogenous_data_tile(exogenous_data_tif: str | None, tile_name: str) -> str | None:
    """
    Interpolate tile in exogenous data path if possible

    Parameters
    ----------
    exogenous_data_tif: str | None
        Path to a Geotiff file containing additional data to be used in external features
    tile_name
        Name of the tile
    """
    if not exogenous_data_tif:
        # allow null value
        return exogenous_data_tif

    # if string exists, replace $TILE by actual tile name
    return exogenous_data_tif.replace("$TILE", tile_name)


@dataclass
class TimeSeriesStructure:
    """
    A class used to deal with all shifts and lists of bands, indices and sensors
    """

    gap: GapStructure
    raw: RawStructure


@dataclass
class CustomFeaturesConfiguration:
    """
    Multiple parameters to configure the custom features process
    """

    concat_mode: bool = True
    enabled_raw: bool = False
    enabled_gap: bool = True
    fill_missing_dates: bool = False
    missing_refl_values: float = I2_CONST.i2_missing_dates_no_data
    missing_masks_values: float = I2_CONST.i2_missing_dates_no_data_mask
    allow_nans: bool = False


class DataContainer:
    """
    A generic class that contains all time series from all enabled sensors.
    The actual data management is done in subclasses.
    """

    def __init__(
        self,
        sensors_parameters: SensorsParameters,
        configuration: CustomFeaturesConfiguration,
        all_dates_dict: SensorsDates | None = None,
        exogeneous_data_file: PathLike | None = None,
    ):
        if all_dates_dict is not None:
            self.all_dates = all_dates_dict
        else:
            self.all_dates = {}

        self.binary_masks: np.ndarray | None = None
        self.exogeneous_data_name = exogeneous_data_file
        self.exogenous_total_bands = 0
        self.configuration = configuration

        self.interpolated_dates: SensorsDates = {}
        self.raw_dates: dict[str, list[str] | SensorsDates] = {}
        self.structure = TimeSeriesStructure(
            GapStructure(
                list_of_bands=[],
                time_series_indices=[],
                list_sensors=[],
                shift=0,
            ),
            RawStructure(
                list_of_bands=[],
                time_series_indices=[],
                masks_indices=[],
                list_sensors=[],
                shift=0,
                masks_shift=0,
            ),
        )

        self.interpolated_data: np.ndarray | None = None
        self.raw_data: np.ndarray | None = None

        sensors_container = SensorsContainer(None, None, None, **sensors_parameters)

        all_data_containers: list[
            S1DataContainer | UserFeaturesDataContainer | OtherSensorsDataContainer
        ] = []
        sensor: SensorType
        for sensor in sensors_container.get_enabled_sensors():
            if isinstance(sensor, Sentinel1):
                all_data_containers.append(S1DataContainer(sensor, self))
            elif isinstance(sensor, UserFeatures):
                all_data_containers.append(UserFeaturesDataContainer(sensor, self))
            else:
                all_data_containers.append(OtherSensorsDataContainer(sensor, self))
        self.all_data_containers = all_data_containers
        compute_all_structures(
            all_data_containers, configuration.enabled_gap, configuration.enabled_raw
        )
        if self.exogeneous_data_name:
            self.exogenous_total_bands = rio.open(self.exogeneous_data_name).count

        def get_filled_stack() -> tuple[np.ndarray, list[I2TemporalLabel]]:
            filled_full_stack = None
            all_labels = []
            shift = 0
            for data_container in all_data_containers:
                filled_stack, labels, new_shift = data_container.manage_raw_data(shift)
                shift += new_shift
                all_labels += labels

                if filled_full_stack is None:
                    filled_full_stack = filled_stack
                else:
                    filled_full_stack = np.concatenate(
                        (filled_full_stack, filled_stack), axis=2
                    )
            assert filled_full_stack is not None
            return filled_full_stack, all_labels

        def get_filled_masks() -> tuple[np.ndarray, list[I2TemporalLabel]]:
            filled_full_mask = None
            all_labels = []
            shift = 0
            for data_container in all_data_containers:
                if isinstance(data_container, UserFeaturesDataContainer):
                    continue
                filled_mask, labels, new_shift = data_container.manage_raw_mask(shift)
                all_labels += labels
                if filled_full_mask is None:
                    filled_full_mask = filled_mask
                else:
                    filled_full_mask = np.concatenate(
                        (filled_full_mask, filled_mask), axis=2
                    )
                shift += new_shift
            assert filled_full_mask is not None
            return filled_full_mask, all_labels

        if configuration.enabled_raw:
            setattr(self, "get_filled_stack", get_filled_stack)
            setattr(self, "get_filled_masks", get_filled_masks)

        for band, indices, sensor_names in zip(
            self.structure.raw.list_of_bands,
            self.structure.raw.time_series_indices,
            self.structure.raw.list_sensors,
        ):

            def get_band_raw(list_indices: list[int] = indices) -> np.ndarray:
                data: np.ndarray = np.take(self.raw_data, list_indices, axis=2)
                return data

            function_name = f"get_raw_{sensor_names}_{band}"
            if not band:
                function_name = f"get_raw_{sensor_names}"
            setattr(self, function_name, get_band_raw)
        for band, indices, sensor_names in zip(
            self.structure.gap.list_of_bands,
            self.structure.gap.time_series_indices,
            self.structure.gap.list_sensors,
        ):

            def get_band(list_indices: list[int] = indices) -> np.ndarray:
                data: np.ndarray = np.take(self.interpolated_data, list_indices, axis=2)
                return data

            function_name = f"get_interpolated_{sensor_names}_{band}"
            if not band:
                function_name = f"get_interpolated_{sensor_names}"

            setattr(self, function_name, get_band)
        if self.exogeneous_data_name is not None:
            setattr(self, "get_exogeneous_data", self.get_exogeneous_data)

        for mask_indices, sensor_names in zip(
            self.structure.raw.masks_indices, self.structure.raw.list_sensors
        ):

            def get_binary_masks(list_indices: range = mask_indices) -> np.ndarray:
                """
                Retrieve the masks' numpy arrays

                Parameters
                ----------
                list_indices:
                    List of masks' indices

                Returns
                -------
                data: np.ndarray
                    The numpy arrays of the masks
                """
                data: np.ndarray = np.take(self.binary_masks, list_indices, axis=2)
                return data

            function_name = f"get_{sensor_names}_binary_masks"
            if configuration.enabled_raw:
                setattr(self, function_name, get_binary_masks)

        if configuration.enabled_raw:
            setattr(self, "get_raw_dates", self.get_raw_dates)

        if configuration.enabled_gap:
            setattr(self, "get_interpolated_dates", self.get_interpolated_dates)

    def get_exogeneous_data(self) -> np.ndarray | None:
        """
        Return the numpy array containing the exogenous data array.
        Implemented in the child class (CustomNumpyFeatures)
        """

    def get_raw_dates(self) -> dict[str, list[str] | SensorsDates]:
        """
        Return the raw dates dictionary
        """
        return self.raw_dates

    def get_interpolated_dates(self) -> SensorsDates:
        """
        Return the interpolated (gapfilled) dates dictionary
        """
        return self.interpolated_dates


class S1DataContainer:
    """
    DataContainer for the sentinel-1 sensor
    """

    def __init__(self, sensor: Sentinel1, data_container: DataContainer) -> None:
        self.sensor = sensor
        self.structure = data_container.structure
        self.raw_data = data_container.raw_data
        self.interpolated_data = data_container.interpolated_data
        self.data_container = data_container
        self.s1_orbits: list[str]
        self.all_orbits_dates: dict[str, list[str]]

    def compute_structure(self, enabled_gap: bool, enabled_raw: bool) -> None:
        """
        Create a TimeSeriesStructure instance for the sentinel-1 sensor, containing all indices and
        shifts of indices of all data types (raw and interpolated) in the stack.

        Parameters
        ----------
        enabled_gap: bool
            Flag used when gapfilling is enabled
        enabled_raw
            Flag used when raw data is used

        Returns
        -------
        None
        """
        s1_orbits = []
        s1_masks_indices = []
        if enabled_gap:
            orbit_dates = self.sensor.get_interpolated_dates()
            if "DES_vv" in orbit_dates:
                s1_orbits = ["DES_vv", "DES_vh"]

            if "ASC_vv" in orbit_dates:
                s1_orbits += ["ASC_vv", "ASC_vh"]

            for sensor_type in s1_orbits:
                gap_tmp_indices, self.structure.gap.shift = self.compute_indices(
                    orbit_dates[sensor_type], self.structure.gap.shift
                )
                sub_s1_sensor = f"{self.sensor.name}_{sensor_type}"
                self.structure.gap.list_of_bands.append("")
                self.structure.gap.time_series_indices.append(gap_tmp_indices)
                self.structure.gap.list_sensors.append(sub_s1_sensor)

        if enabled_raw:
            orbit_dates = self.sensor.get_available_dates()
            if "DES_vv" in orbit_dates:
                s1_orbits = ["DES_vv", "DES_vh"]
                s1_masks_indices = [
                    (
                        self.structure.raw.masks_shift,
                        self.structure.raw.masks_shift + len(orbit_dates["DES_vv"]),
                    ),
                    (
                        self.structure.raw.masks_shift,
                        self.structure.raw.masks_shift + len(orbit_dates["DES_vv"]),
                    ),
                ]
                self.structure.raw.masks_shift += len(orbit_dates["DES_vv"])
            if "ASC_vv" in orbit_dates:
                s1_orbits += ["ASC_vv", "ASC_vh"]
                s1_masks_indices += [
                    (
                        self.structure.raw.masks_shift,
                        self.structure.raw.masks_shift + len(orbit_dates["ASC_vv"]),
                    ),
                    (
                        self.structure.raw.masks_shift,
                        self.structure.raw.masks_shift + len(orbit_dates["ASC_vv"]),
                    ),
                ]
                self.structure.raw.masks_shift += len(orbit_dates["ASC_vv"])
            for sensor_type, masks_indices in zip(s1_orbits, s1_masks_indices):
                raw_tmp_s1_indices, self.structure.raw.shift = self.compute_indices(
                    orbit_dates[sensor_type], self.structure.raw.shift
                )
                sub_s1_sensor = f"{self.sensor.name}_{sensor_type}"
                self.data_container.raw_dates[sub_s1_sensor] = orbit_dates[sensor_type]
                self.structure.raw.list_of_bands.append("")
                self.structure.raw.time_series_indices.append(raw_tmp_s1_indices)
                self.structure.raw.list_sensors.append(sub_s1_sensor)

                self.structure.raw.masks_indices.append(
                    range(masks_indices[0], masks_indices[1])
                )
        self.s1_orbits = s1_orbits
        self.all_orbits_dates = orbit_dates

    @staticmethod
    def compute_indices(dates: list[str], shift: int = 0) -> tuple[list[int], int]:
        """
        Get the indices of the given dates, using the provided shift. Return the computed indices
        as well as the new shift.
        Parameters
        ----------
        dates : list[str]
            List of dates of which the indices are computed
        shift : int, optional
            Optional shift if dates aren't in the first position of the list

        Returns
        -------
        tuple:
            indices : int
                List of induces corresponding to the provided dates
            new_shift : int
                New shift representing the final general index of the provided dates
        """
        indices = list(range(shift, shift + len(dates)))
        new_shift = len(indices) + shift
        return indices, new_shift

    def manage_raw_mask(
        self, shift: int
    ) -> tuple[np.ndarray, list[I2TemporalLabel], int]:
        """
        Retrieves masks as numpy arrays, along with the corresponding labels and the total number
        of bands.

        Parameters
        ----------
        shift :
            Shift representing the place of the data to access

        Returns
        -------
        tuple:
            filled_mask : np.ndarray
                Numpy array containing the mask(s)
            labels : list[I2TemporalLabel]
                List of corresponding labels
            nb_raw_sensor_bands : int
                Total number of bands
        """
        s1_raw_dates = []
        s1_all_dates = []

        ordered_s1_orbits = [
            f"Sentinel1_{orbit}"
            for orbit in list(self.all_orbits_dates.keys())
            if "vh" not in orbit
        ]

        for ordered_s1_orbit in ordered_s1_orbits:
            for date in self.data_container.raw_dates[ordered_s1_orbit]:
                s1_raw_dates.append(f"{ordered_s1_orbit}_{date}")
            for date in self.data_container.all_dates[ordered_s1_orbit]:
                s1_all_dates.append(f"{ordered_s1_orbit}_{date}")
        if not self.data_container.configuration.fill_missing_dates:
            all_dates = s1_raw_dates
        else:
            all_dates = s1_all_dates
        existing_indices_mask = []
        labels = []
        s1_orbits_vv = [s1_orbit for s1_orbit in self.s1_orbits if "vh" not in s1_orbit]
        for date in all_dates:
            sensor_name, orbit, _, date = date.split(I2Label.separator)
            labels.append(
                I2TemporalLabel(
                    sensor_name=sensor_name, feat_name=f"{orbit}mask", date=date
                )
            )

        for orbit in s1_orbits_vv:
            raw_dates = self.data_container.raw_dates[f"Sentinel1_{orbit}"]
            for raw_date in raw_dates:
                expected_date = f"Sentinel1_{orbit}_{raw_date}"
                indices = [
                    index
                    for index, element in enumerate(all_dates)
                    if element == expected_date
                ]
                if len(indices) != 1:
                    raise ValueError(
                        "Something goes wrong with the Sentinel-1"
                        " date management in custom features, "
                        "a dates is duplicated"
                    )
                existing_indices_mask.append(indices[0])
        assert self.raw_data is not None
        row, col, _ = self.raw_data.shape
        filled_mask = np.full(
            (row, col, len(all_dates)),
            self.data_container.configuration.missing_masks_values,
        )
        nb_raw_sensor_bands = len(s1_raw_dates)
        assert self.data_container.binary_masks is not None
        filled_mask[:, :, existing_indices_mask] = self.data_container.binary_masks[
            :, :, shift : nb_raw_sensor_bands + shift
        ]
        return filled_mask, labels, nb_raw_sensor_bands

    def manage_raw_data(
        self, shift: int
    ) -> tuple[np.ndarray, list[I2TemporalLabel], int]:
        """
        Return the sentinel-1 data as a numpy array

        Parameters
        ----------
        shift : int
            Shift representing the first index of the raw data to access

        Returns
        -------
        tuple:
            filled_stack : np.ndarray
                Stack of the selected data
            labels : list[I2TemporalLabel]
                List of corresponding labels
            new_shift : int
                New shift to apply to indices
        """
        self.raw_data = self.data_container.raw_data

        s1_raw_dates = []
        s1_all_dates = []

        ordered_s1_orbits = [
            f"Sentinel1{I2Label.separator}{orbit}"
            for orbit in list(self.all_orbits_dates.keys())
        ]
        for ordered_s1_orbit in ordered_s1_orbits:
            for date in self.data_container.raw_dates[ordered_s1_orbit]:
                s1_raw_dates.append(f"{ordered_s1_orbit}{I2Label.separator}{date}")
            for date in self.data_container.all_dates[ordered_s1_orbit]:
                s1_all_dates.append(f"{ordered_s1_orbit}{I2Label.separator}{date}")

        if not self.data_container.configuration.fill_missing_dates:
            all_dates = s1_raw_dates
        else:
            all_dates = s1_all_dates

        labels = []
        for date in all_dates:
            sensor_name, orbit, pol, date = date.split(I2Label.separator)
            labels.append(
                I2TemporalLabel(
                    sensor_name=sensor_name, feat_name=f"{orbit}{pol}", date=date
                )
            )

        existing_indices_refl = []
        for orbit in self.s1_orbits:
            raw_dates = self.data_container.raw_dates[
                f"Sentinel1{I2Label.separator}{orbit}"
            ]
            for raw_date in raw_dates:
                expected_date = (
                    f"Sentinel1{I2Label.separator}"
                    f"{orbit}{I2Label.separator}{raw_date}"
                )
                indices = [
                    index
                    for index, element in enumerate(all_dates)
                    if element == expected_date
                ]
                if len(indices) != 1:
                    raise ValueError(
                        "Something went wrong with the Sentinel-1"
                        " date management in custom features: "
                        "a date is duplicated"
                    )
                existing_indices_refl.append(indices[0])
        assert self.raw_data is not None
        row, col, _ = self.raw_data.shape
        filled_data = np.full(
            (row, col, len(all_dates)),
            self.data_container.configuration.missing_refl_values,
        )
        nb_raw_sensor_bands = len(s1_raw_dates)
        filled_data[:, :, existing_indices_refl] = self.raw_data[
            :, :, shift : nb_raw_sensor_bands + shift
        ]
        return filled_data, labels, nb_raw_sensor_bands


class UserFeaturesDataContainer:
    """
    DataContainer for the UserFeatures sensor
    """

    def __init__(self, sensor: UserFeatures, data_container: DataContainer) -> None:
        self.sensor = sensor
        self.structure = data_container.structure
        self.raw_data = data_container.raw_data
        self.interpolated_data = data_container.interpolated_data

    def compute_structure(self, enabled_gap: bool, enabled_raw: bool) -> None:
        """
        Create a TimeSeriesStructure instance for the user_features sensor, containing all indices
        and shifts of indices of all data types (raw and interpolated) in the stack.

        Parameters
        ----------
        enabled_gap: bool
            Flag used when gapfilling is enabled
        enabled_raw
            Flag used when raw data is used

        Returns
        -------
        None
        """
        _, labels = self.sensor.get_features()
        if enabled_gap:
            (
                tmp_sensors,
                tmp_featnames,
                tmp_indices,
                _,
            ) = self.compute_indices(labels, self.structure.gap.shift)
            self.structure.gap.list_of_bands += tmp_featnames
            self.structure.gap.list_sensors += tmp_sensors
            self.structure.gap.time_series_indices += tmp_indices
        if enabled_raw:
            (
                tmp_sensors,
                tmp_featnames,
                tmp_indices,
                _,
            ) = self.compute_indices(labels, self.structure.raw.shift)
            self.structure.raw.list_of_bands += tmp_featnames
            self.structure.raw.list_sensors += tmp_sensors
            self.structure.raw.time_series_indices += tmp_indices

    @staticmethod
    def compute_indices(
        userfeatures_list: list[I2LabelAlias], shift: int
    ) -> tuple[list[str], list[str], list[list[int]], int]:
        """
        Get the indices of the given dates, using the provided shift. Return the computed indices
        as well as the new shift. user_features must be formatted as FEATURESNAME_band_INDEX.

        Parameters
        ----------
        dates : list[str]
            List of dates of which the indices are computed
        shift : int, optional
            Optional shift if dates aren't in the first position of the list

        Returns
        -------
        tuple:
            indices : int
                List of induces corresponding to the provided dates
            new_shift : int
                New shift representing the final general index of the provided dates
        """
        feat_list = [
            (userfeature.sensor_name, shift + index)
            for index, userfeature in enumerate(userfeatures_list)
        ]

        feat_ind = [elem[1] for elem in fu.sort_by_first_elem(feat_list)]
        feat_names = [elem[0] for elem in fu.sort_by_first_elem(feat_list)]
        sensors_list = ["userfeatures"] * len(feat_ind)
        return sensors_list, feat_names, feat_ind, shift + len(userfeatures_list)

    def manage_raw_data(
        self, shift: int, from_raw: bool = True
    ) -> tuple[np.ndarray, list[I2TemporalLabel], int]:
        """
        Return the user_features' sensor data as a numpy array

        Parameters
        ----------
        shift : int
            Shift representing the first index of the raw data to access
        from_raw : bool, defaults to True
            If true, return raw data (gapfilled data otherwise), True by default

        Returns
        -------
        tuple:
            filled_stack : np.ndarray
                Stack of the selected data
            labels : list[I2TemporalLabel]
                List of corresponding labels
            new_shift : int
                New shift to apply to indices
        """
        _, labels = self.sensor.get_features()
        assert self.raw_data is not None
        if from_raw:
            filled_stack = self.raw_data[:, :, shift : shift + len(labels)]
        else:
            assert self.interpolated_data is not None
            filled_stack = self.interpolated_data[:, :, shift : shift + len(labels)]
        new_shift = len(labels)
        return filled_stack, labels, new_shift


class OtherSensorsDataContainer:
    """
    DataContainer for all sensors, except sentinel1 and UserFeatures
    """

    def __init__(self, sensor: GenericSensor, data_container: DataContainer):
        self.sensor = sensor
        self.structure = data_container.structure
        self.interpolated_data = data_container.interpolated_data
        self.data_container = data_container
        self.raw_data: np.ndarray

    def compute_structure(self, enabled_gap: bool, enabled_raw: bool) -> None:
        """
        Create a TimeSeriesStructure instance for all sensors (except sentinel-1 and user
        features), containing all indices and shifts of indices of all data types (raw and
        interpolated) in the stack.

        Parameters
        ----------
        enabled_gap: bool
            Flag used when gapfilling is enabled
        enabled_raw: bool
            Flag used when raw data is used
        """
        spectral_bands = self.sensor.stack_band_position
        spectral_indices = self.sensor.features_names_list
        if enabled_gap:
            (
                _,
                self.data_container.interpolated_dates[self.sensor.name],
            ) = self.sensor.write_interpolation_dates_file(write=False)
            (
                gap_time_series_tmp_indices,
                self.structure.gap.shift,
            ) = self.compute_indices(
                spectral_bands,
                spectral_indices,
                self.data_container.interpolated_dates[self.sensor.name],
                self.structure.gap.shift,
            )

            self.structure.gap.list_of_bands += spectral_bands
            self.structure.gap.list_of_bands += spectral_indices
            self.structure.gap.time_series_indices += gap_time_series_tmp_indices
            self.structure.gap.list_sensors += [self.sensor.name] * (
                len(spectral_bands) + len(spectral_indices)
            )
        if enabled_raw:
            (
                _,
                self.data_container.raw_dates[self.sensor.name],
            ) = self.sensor.write_dates_file()
            raw_tmp_indices, self.structure.raw.shift = self.compute_indices(
                spectral_bands,
                [],
                self.data_container.raw_dates[self.sensor.name],
                self.structure.raw.shift,
            )
            self.structure.raw.list_of_bands += spectral_bands
            # raw data has no spectral features
            self.structure.raw.time_series_indices += raw_tmp_indices
            self.structure.raw.list_sensors += [self.sensor.name] * (
                len(spectral_bands)
            )
            self.structure.raw.masks_indices.append(
                range(
                    self.structure.raw.masks_shift,
                    self.structure.raw.masks_shift
                    + len(self.data_container.raw_dates[self.sensor.name]),
                )
            )
            self.structure.raw.masks_shift += len(
                self.data_container.raw_dates[self.sensor.name]
            )

    @staticmethod
    def compute_indices(
        spectral_bands: list[str],
        spectral_indices: list[str],
        dates: list[str] | SensorsDates,
        shift: int = 0,
    ) -> tuple[list[list[int]], int]:
        """
        Get the indices of the given dates, using the provided shift. Return the computed indices
        as well as the new shift. user_features must be formatted as FEATURESNAME_band_INDEX.

        Parameters
        ----------
        spectral_bands : list[str]
            List of names of spectral bands of the sensor
        spectral_indices : list[str]
            List of names of the spectral features available to the sensor (NDVI, NDWI ...)
        dates : list[str]
            List of dates of which the indices are computed
        shift : int, optional
            Optional shift if dates aren't in the first position of the list

        Returns
        -------
        tuple:
            indices : int
                List of induces corresponding to the provided dates
            new_shift : int
                New shift representing the final general index of the provided dates
        """
        indices = []
        for n_band, _ in enumerate(spectral_bands):
            ind = [
                shift + n_band + len(spectral_bands) * x for x, _ in enumerate(dates)
            ]
            indices.append(ind)

        len_spectral_band = len(dates) * len(spectral_bands) + shift
        for n_index, _ in enumerate(spectral_indices):
            spect_begin = len_spectral_band + n_index * len(dates)
            ind = list(range(spect_begin, spect_begin + len(dates)))
            indices.append(ind)

        new_shift = (
            len(dates) * len(spectral_bands)
            + len(dates) * len(spectral_indices)
            + shift
        )
        return indices, new_shift

    def manage_raw_data(
        self, shift: int
    ) -> tuple[np.ndarray, list[I2TemporalLabel], int]:
        """
        Return any sensor (except from sentinel-1 and user_features) data as a numpy array

        Parameters
        ----------
        shift : int
            Shift representing the first index of the raw data to access

        Returns
        -------
        tuple:
            filled_stack : np.ndarray
                Stack of the selected data
            labels : list[I2TemporalLabel]
                List of corresponding labels
            new_shift : int
                New shift to apply to indices
        """
        sensor_name = self.sensor.name
        sensor_bands = self.sensor.stack_band_position
        assert self.data_container.raw_data is not None
        self.raw_data = self.data_container.raw_data
        if not self.data_container.configuration.fill_missing_dates:
            all_dates = self.data_container.raw_dates[sensor_name]
        else:
            all_dates = self.data_container.all_dates[sensor_name]
        raw_dates = self.data_container.raw_dates[sensor_name]
        existing_indices_refl = []
        labels = []
        for ind_date, date in enumerate(all_dates):
            for ind_band, band in enumerate(sensor_bands):
                if date in raw_dates:
                    existing_indices_refl.append(
                        ind_date * len(sensor_bands) + ind_band
                    )
                labels.append(
                    I2TemporalLabel(sensor_name=sensor_name, feat_name=band, date=date)
                )

        if not existing_indices_refl:
            raise ValueError(
                "Something goes wrong with the date management"
                "Check the dates provided for gapfilling"
                "or the type provided to compute_custom_features"
            )
        row, col, _ = self.raw_data.shape
        filled_data = np.full(
            (row, col, len(all_dates) * len(sensor_bands)),
            self.data_container.configuration.missing_refl_values,
        )
        nb_raw_sensor_bands = len(self.data_container.raw_dates[sensor_name]) * len(
            sensor_bands
        )
        filled_data[:, :, existing_indices_refl] = self.raw_data[
            :, :, shift : nb_raw_sensor_bands + shift
        ]
        return filled_data, labels, nb_raw_sensor_bands

    def manage_raw_mask(
        self, shift: int
    ) -> tuple[np.ndarray, list[I2TemporalLabel], int]:
        """
        Retrieves masks as numpy arrays, along with the corresponding labels and the total number
        of bands.

        Parameters
        ----------
        shift :
            Shift representing the place of the data to access

        Returns
        -------
        tuple:
            filled_mask : np.ndarray
                Numpy array containing the mask(s)
            labels : list[I2TemporalLabel]
                List of corresponding labels
            nb_raw_sensor_bands : int
                Total number of bands
        """
        sensor_name = self.sensor.name

        binary_masks = self.data_container.binary_masks
        assert binary_masks is not None
        if not self.data_container.configuration.fill_missing_dates:
            all_dates = self.data_container.raw_dates[sensor_name]
        else:
            all_dates = self.data_container.all_dates[sensor_name]
        raw_dates = self.data_container.raw_dates[sensor_name]
        existing_indices_mask = []
        labels = []
        for ind_date, date in enumerate(all_dates):
            labels.append(
                I2TemporalLabel(sensor_name=sensor_name, feat_name="mask", date=date)
            )
            if date in raw_dates:
                existing_indices_mask.append(ind_date)
        row, col, _ = self.raw_data.shape
        filled_mask = np.full(
            (row, col, len(all_dates)),
            self.data_container.configuration.missing_masks_values,
        )
        nb_raw_sensor_bands = len(self.data_container.raw_dates[sensor_name])
        filled_mask[:, :, existing_indices_mask] = binary_masks[
            :, :, shift : nb_raw_sensor_bands + shift
        ]
        return filled_mask, labels, nb_raw_sensor_bands


GenericDataContainer = (
    S1DataContainer | UserFeaturesDataContainer | OtherSensorsDataContainer
)


def compute_all_structures(
    all_data_containers: list[GenericDataContainer],
    enabled_gap: bool,
    enabled_raw: bool,
) -> None:
    """
    Run the `compute_structure` method for all containers in the `all_data_containers` list

    Parameters
    ----------
    all_data_containers: list[GenericDataContainer]
        List of all data containers
    enabled_gap: bool
        Flag used when gapfilling is enabled
    enabled_raw
        Flag used when raw data is used
    """
    for container in all_data_containers:
        container.compute_structure(enabled_gap=enabled_gap, enabled_raw=enabled_raw)


class CustomNumpyFeatures(DataContainer):
    """This class add functions provided by a user.
    and concatenate the results to the original feature stack"""

    def __init__(
        self,
        sensors_params: SensorsParameters,
        module_name: str,
        list_functions: list[FunctionNameWithParams],
        configuration: CustomFeaturesConfiguration = CustomFeaturesConfiguration(),
        all_dates_dict: SensorsDates | None = None,
        exogeneous_data_file: str | None = None,
    ) -> None:
        """
        Parameters
        ----------
        sensors_params:
            A dictionary containing all required information to instantiate
            the enabled sensors
        module_name: str
            The user provided python code. The full path to a file is required
        list_functions: List[FunctionNameWithParams]
            A list of function name and parameters that will be applied
        """
        self.configuration = configuration
        self.func_param = list_functions
        self.exogeneous_data_file = exogeneous_data_file
        self.module_name = module_name
        self.out_data: np.ndarray
        self.exogeneous_data_array: np.ndarray | None = None
        self.transform: tuple[Affine, int, int, str] | None

        if configuration.fill_missing_dates and not configuration.enabled_raw:
            raise ValueError(
                "Ask for fill missing dates but not for access raw data."
                "Check your configuration"
            )
        super().__init__(
            sensors_params,
            configuration,
            all_dates_dict,
            exogeneous_data_file,
        )
        # load external functions
        self.external_functions = search_external_features_function(
            module_name, list_functions
        )

    @staticmethod
    def check_labels(features_labels: list[I2Label | I2TemporalLabel]) -> None:
        """
        Check if each label in the provided list is an instance of I2Label or I2TemporalLabel.

        Parameters
        ----------
        features_labels : list
            List of labels to be checked.

        Raises
        ------
        ValueError
            If any label in the list is not an instance of I2Label or I2TemporalLabel.
        """
        for label in features_labels:
            if not isinstance(label, (I2Label, I2TemporalLabel)):
                i2_lab_mod = fu.path_back_until(inspect.getfile(I2Label), "iota2")
                i2_temp_lab_mod = fu.path_back_until(
                    inspect.getfile(I2TemporalLabel), "iota2"
                )
                raise ValueError(
                    f"labels {label} is not an instance of I2Label or I2TemporalLabel. "
                    "Please use one of it.\n"
                    f"\tI2Label definition in {i2_lab_mod}\n"
                    f"\tI2TemporalLabel definition in {i2_temp_lab_mod}"
                )

    def get_exogeneous_data(self) -> np.ndarray | None:
        """
        Return the numpy array containing the exogenous data array
        """
        return self.exogeneous_data_array

    def process_features(
        self,
        feat: np.ndarray,
        n_feature: int,
        labels: list[I2LabelAlias],
    ) -> list[I2LabelAlias]:
        """
        Process the computed features and concatenate them along the feature dimension.

        This method takes the computed features ('feat') along with their associated labels
        ('labels') and appends the labels to the list of new labels. It ensures
        that the features have the correct shape, do not contain NaN or infinite values, and
        concatenates them along the feature dimension (axis=2). If 'feat' is a 2D array,
        it adds a dimension to represent the features.

        Parameters
        ----------
        feat:
            The computed features to be processed.
        n_feature:
            The index of the current feature.
        labels:
            The labels associated with the computed features.

        Returns
        -------
        list[I2LabelT]
            The list of new labels to which the labels were appended.

        Raises
        ------
        ValueError
            If the computed features have NaN or infinite values, or if their shape is invalid.
        """
        new_labels = []
        if isinstance(feat, np.ndarray) and len(feat.shape) in (2, 3):
            if not self.configuration.allow_nans and not np.isfinite(feat).all():
                raise ValueError(
                    "Error during custom_features computation: np.nan or infinite found. "
                    "Check if there is no division by zero"
                )
            if len(feat.shape) == 2:
                feat = feat[:, :, None]
            elif feat.any() and len(feat.shape) != 3:
                raise ValueError("The return feature must be a 2d or 3d array")
            if len(labels) != feat.shape[2]:
                labels = [
                    i2_label_factory(f"custFeat_num{n_feature + 1}b{j + 1}")
                    for j in range(feat.shape[2])
                ]
            new_labels += labels
            if n_feature == 0:
                self.out_data = feat[:]
            else:
                self.out_data = np.concatenate((self.out_data, feat), axis=2)
        else:
            raise FeaturesShapeError(feat.shape)
        return new_labels

    def concat_processed_features(self) -> None:
        """
        Concatenate the processed features with the interpolated or raw data.

        If the 'concat_mode' is enabled and the 'enabled_gap' flag is True, it concatenates
        the processed features with the interpolated data. Otherwise, it concatenates them
        with the raw data.

        If the processed features are already stored in a numpy array ('out_data'),
        it concatenates them along the feature dimension (axis=2). If 'out_data' is not
        a numpy array, it returns the interpolated or raw data based on the 'enabled_gap'
        flag.

        This method modifies the 'out_data' attribute of the object.
        """
        features_stack = (
            self.interpolated_data if self.configuration.enabled_gap else self.raw_data
        )
        self.out_data = (
            np.concatenate((features_stack, self.out_data), axis=2)
            if isinstance(self.out_data, np.ndarray)
            else features_stack
        )

    def process(
        self,
        data: PipelinesData,
        transform_in: tuple[Affine, int, int, str] | None = None,
    ) -> tuple[np.ndarray, list[I2LabelAlias]]:
        """
        Apply a set of functions to data and return the processed features.

        It iterates over each function, applies it to the data, and concatenates the resulting
        features along the feature dimension. It performs checks on the output features,
        ensuring they have the correct shape and do not contain NaN or infinite values.
        If concat_mode is enabled, it concatenates the processed features with the interpolated
        or raw data, depending on the enabled_gap flag.

        Parameters
        ----------
        data:
            An instance of PipelinesData containing the input data.
        transform_in:
            An optional transformation to apply to the input data. Defaults to None.

        Returns
        -------
        tuple[np.ndarray, list[I2LabelT]]
            A tuple containing the processed features as a numpy array and a list of labels.

        Raises
        ------
        ValueError
            If any label in the output is not an instance of I2Label or I2TemporalLabel,
            or if there are NaN or infinite values in the computed features.
        FeaturesShapeError
            If the shape of the computed features is invalid.
        """
        self.interpolated_data = data.interpolated
        self.binary_masks = data.binary_mask
        self.raw_data = data.raw
        self.exogeneous_data_array = data.exogenous
        self.transform = transform_in
        new_labels = []
        try:
            for n_feature, function in enumerate(self.func_param):
                fun_name = function[0]
                kwargs = function[1]
                func = self.external_functions[fun_name]
                feat, labels = func(self, **kwargs)
                self.check_labels(labels)
                features_labels = self.process_features(feat, n_feature, labels)
                new_labels += features_labels

            if self.configuration.concat_mode:
                self.concat_processed_features()
            return self.out_data, new_labels

        except AttributeError as err:
            print(
                "You try to access an non existing function. Check your configuration"
            )
            print("You probably trying to access raw data without enable it")
            raise err
        except Exception as err:
            print("Error during custom_features computation")
            raise err

    def test_user_feature_with_fake_data(self, epsg: int) -> Exception | None:
        """tests the external feature on fake data:
        1. creates fake data
        2. run the custom features
        3. intercept an error if there is
        returns:
          - success: (False, None)
          - failure: (True, Exception)
        """

        # creates fake data
        interpolated_data = np.ones((4, 3, self.structure.gap.shift), dtype=np.int16)
        raw_data = np.ones((4, 3, self.structure.raw.shift), dtype=np.int16)
        binary_masks = np.ones((4, 3, self.structure.raw.masks_shift), dtype=bool)
        exogeneous_data_array = None
        if self.exogenous_total_bands:
            if self.exogenous_total_bands == 1:
                exogeneous_data_array = np.ones(
                    (4, 3),
                    dtype=np.int16,
                )
            else:
                exogeneous_data_array = np.ones(
                    (4, 3, self.exogenous_total_bands),
                    dtype=np.int16,  # Change order to match reshape_as_image
                )

        # create a fake transform
        # origin 0,0 and 10m resolution
        geotransform = [0, 10, 0, 0, 0, -10]
        projection = osr.SpatialReference()
        projection.ImportFromEPSG(epsg)
        proj = projection.ExportToWkt()
        transform_in = (Affine.from_gdal(*geotransform), 4, 3, proj)
        # try to exec functions using fake data
        try:
            self.process(
                PipelinesData(
                    interpolated_data,
                    raw_data,
                    exogeneous_data_array,
                    binary_masks,
                ),
                transform_in=transform_in,
            )
        except Exception as e:  # pylint: disable=W0703
            return e
        return None


@dataclass
class ChunkParameters:
    """
    Represents the parameters related to a specific data chunk.

    Attributes:
    ----------
    chunk_config : ru.ChunkConfig
        Configuration for the data chunk, which includes various settings.

    targeted_chunk : int | None
        The index of the chunk being targeted. If `None`, no specific chunk is targeted.
    """

    chunk_config: ru.ChunkConfig
    targeted_chunk: int | None


@dataclass
class MaskingRaster:
    """
    Represents a raster with optional masking for data validation purposes.

    Attributes:
    ----------
    mask_valid_data : str | None, default=None
        A string representing a file path or identifier for a mask that defines valid data areas.
        If `None`, no mask is applied.

    mask_value : int | None, default=0
        The value to be used for masking invalid data. If `None`, no specific value is applied.
        Typically, this is set to 0 to indicate masked areas.
    """

    mask_valid_data: str | None = None
    mask_value: int = 0


@dataclass
class ComputeCustomFeaturesArgs:
    """Utility data class for carrying compute_custom_features arguments."""

    functions_builder: CustomNumpyFeatures | None = None
    otb_pipelines: OtbPipelineCarrier | None = None
    feat_labels: list[I2LabelAlias] | None = None
    chunk_params: ChunkParameters | None = None
    masking_raster: MaskingRaster = field(default_factory=MaskingRaster)
    mask_valid_data: str | None = None
    mask_value: int = 0
    exogeneous_data: str | None = None
    concatenate_features: bool | None = None
    output_name: str | None = None
    expected_output_bands: int = 0
    logger: logging.Logger | None = None


def compute_custom_features(
    functions_builder: CustomNumpyFeatures,
    otb_pipelines: OtbPipelineCarrier,
    feat_labels: list[I2LabelAlias],
    chunk_params: ChunkParameters,
    masking_raster: MaskingRaster = MaskingRaster(),
    exogeneous_data: str | None = None,
    concatenate_features: bool = True,
    output_name: str | None = None,
    expected_output_bands: int = 0,
    logger: logging.Logger = LOGGER,
) -> tuple[UserDefinedFunctionPayload, list[np.ndarray | None]]:
    """
    Compute custom features using a list of functions applied to OTB pipeline data.

    This function applies a list of functions to the OTB pipeline data and returns
    an OTB image object. It chunks the pipelines, applies the functions to each chunk,
    converts the resulting numpy array to an OTB image, and optionally writes the output
    to a file. The resulting features are concatenated if specified, and the payload
    object contains the labels, spatial metadata, and image data.

    Parameters
    ----------
    functions_builder:
        An instance of CustomNumpyFeatures containing the functions to apply to the data.
    otb_pipelines:
        The carrier object containing the OTB pipelines.
    feat_labels:
        The list of feature labels.
    chunk_params:
        Contain the configuration for chunking the data and The index of the targeted chunk.
    masking_raster:
        Contain the path to the mask valid data. Defaults to None and The value to use for masking.
    exogeneous_data:
        The path to the exogeneous data. Defaults to None.
    concatenate_features:
        Whether to concatenate features labels. Defaults to True.
    output_name:
        The output name for writing the array data. Defaults to None.
    expected_output_bands:
        The expected number of output bands. Defaults to 0.
    logger:
        The logger object for logging messages. Defaults to LOGGER.

    Returns
    -------
    tuple[UserDefinedFunctionPayload, list[np.ndarray | None]]
        A tuple containing the payload object with computed features and a list of masks.
    """
    pipelines_rois = pipelines_chunking(
        otb_pipelines,
        chunk_params.chunk_config,
        targeted_chunk=chunk_params.targeted_chunk,
        exogenous_data_file=exogeneous_data,
        spatial_mask=masking_raster.mask_valid_data,
    )
    (data, mask_list) = apply_udf_to_pipeline(
        otb_pipelines_list=pipelines_rois,
        function=UserDefinedFunction(partial(functions_builder.process), True),
        mask_value=masking_raster.mask_value,
        output_number_of_bands=expected_output_bands,
        logger=logger,
    )
    # feat_array is a raster array with shape
    # [band, rows, cols] but otb requires [rows, cols, bands]
    assert data.otb_img is not None and data.data is not None
    crop_otbimage = convert_numpy_array_to_otb_image(
        data.otb_img,
        data.data,
        gdal_transform_to_rio(data.spatial_meta.gdal_geo_transform),
    )
    if output_name:
        write_array(data.data, data.spatial_meta, output_name)

    if concatenate_features:
        feat_labels += data.labels
    else:
        feat_labels = data.labels
    return (
        UserDefinedFunctionPayload(
            labels=feat_labels,
            spatial_meta=data.spatial_meta,
            data=None,  # data are stored in crop_otbimage
            mask=None,
            otb_img=crop_otbimage,
            pipeline_dependencies=data.pipeline_dependencies,
        ),
        mask_list,
    )


def convert_numpy_array_to_otb_image(
    otbimage: OtbImage,
    array: np.ndarray,
    out_transform: affine.Affine,
    logger: logging.Logger = LOGGER,
) -> OtbImage:
    """
    This function allow to convert a numpy array to an otb image

    Parameters
    ----------
    otbimage:
        an otb image dictionary
    array:
        a rasterio array (shape is [bands, row, cols])
    out_transform:
        the geotransform corresponding to array
    logger:
        logger
    """
    otbimage["array"] = np.ascontiguousarray(reshape_as_image(array))
    # the next line cause zeros in the output array
    # otbimage["array"] = np.array(reshape_as_image(array), order="C")
    logger.debug(
        "Numpy image inserted in otb pipeline: " f"{fu.memory_usage_psutil()} MB"
    )
    # get the chunk size
    size = otbimage["array"].shape
    # get the chunk origin
    origin = out_transform * (0, 0)
    # add a demi pixel size to origin
    # offset between rasterio (gdal) and OTB
    otbimage["origin"].SetElement(0, origin[0] + (otbimage["spacing"][0] / 2))
    otbimage["origin"].SetElement(1, origin[1] + (otbimage["spacing"][1] / 2))
    # Set buffer image region
    # Mandatory to keep the projection in OTB pipeline
    otbimage["region"].SetIndex(0, 0)
    otbimage["region"].SetIndex(1, 0)
    otbimage["region"].SetSize(0, size[1])
    otbimage["region"].SetSize(1, size[0])
    return otbimage
