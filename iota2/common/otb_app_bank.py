#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" OTB applications interface"""
import configparser
import logging
import re
import warnings
from collections.abc import Callable, Generator
from dataclasses import asdict, dataclass
from enum import Enum
from functools import wraps
from pathlib import Path
from typing import Any

import numpy as np
import otbApplication as otb

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fut
from iota2.common.compression_options import compression_options
from iota2.sensors.sar import s1_processor as s1p
from iota2.sensors.sar.s1_filtering_processor import get_dates_in_otb_output_name
from iota2.typings.i2_types import (
    ListPathLike,
    OtbApp,
    OtbDep,
    OTBInputImage,
    OTBParamsTypes,
    PathLike,
    SARDatesDict,
)

LOGGER = logging.getLogger("distributed.worker")
S1_CONST = i2_const.S1Constants()


def add_compression_options(fun: Callable) -> Callable:
    """Add compression options to an otb application.

    Parameters
    ----------
    fun
        otb app

    Notes
    -----
    return
        otb app whose output parameter has been updated with compression options
    """

    @wraps(fun)
    def decorated(*args: Any) -> Any:
        out_label = None
        try:
            if isinstance(fun(*args), (list, tuple)):
                out_label = get_input_parameter_output(fun(*args)[0])
            else:
                out_label = get_input_parameter_output(fun(*args))
        except ValueError as err:
            if str(err) != "out parameter not recognize":
                raise err
        if out_label:
            if out_label in args[1].keys():
                if ".tif" in args[1][out_label]:
                    compression_str = (
                        f"?&gdal:co:COMPRESS={compression_options.algorithm}"
                        f"&gdal:co:PREDICTOR={compression_options.predictor}"
                        "&gdal:co:BIGTIFF=YES"
                    )
                    args[1][out_label] += compression_str
        return fun(*args)

    return decorated


def execute_app(otb_application: OtbApp) -> None:
    """function use to wrap otb's application.

    Usefull to purge RAM once ExecuteAndWriteOutput() is called.
    This function must
    be used with multiprocessing.Process(target=execute_app,
    args=[my_otb_application])
    """
    otb_application.ExecuteAndWriteOutput()


def get_input_parameter_output(otb_obj: OtbApp) -> str:
    """
    IN :
    otbObj [otb object]

    OUT :
    output parameter name

    Ex :
    otbBandMath = otb.CreateBandMathApplication(...)
    print getInputParameterOutput(otbBandMath)
    >> out

    This function is not complete, it must be filled up -> out.raster.filename
    """
    list_param = otb_obj.GetParametersKeys()
    # check out
    if "out" in list_param:
        return "out"
    # check io.out
    if "io.out" in list_param:
        return "io.out"
    # check mode.raster.out
    if "mode.raster.out" in list_param:
        return "mode.raster.out"
    if "outputstack" in list_param:
        return "outputstack"
    if "out.raster.filename" in list_param:
        return "out.raster.filename"
    raise ValueError("out parameter not recognize")


def un_pack_first(some_list_of_list: list[list[Any]]) -> Generator:
    """
    python generator
    return first element of a list of list

    Ex:
    myListOfList = [[1,2,3],[4,5,6]]

    for firstValue in unPackFirst(myListOfList):
        print firstValue
    >> 1
    >> 4
    """
    for values in some_list_of_list:
        if isinstance(values, (list, tuple)):
            yield values[0]
        else:
            yield values


def create_superimpose_application(
    otb_parameters: dict[str, Any]
) -> tuple[OtbApp, OtbDep]:
    """
    Create an application for superimposing one image on top of another.

    Parameters
    ----------
    otb_parameters : dict[str, Any]
        Dictionary containing OTB parameters.

    Returns
    -------
    tuple
        A tuple containing the superimpose application and the input image.

    Notes
    -----
    This function creates an application for superimposing one image on top of another
    using the provided OTB parameters. It returns a tuple containing the superimpose
    application and the input image, which can be used later for execution.

    Examples
    --------
    >>> params = {"inm": "main_image.tif", "inr": "ref_image.tif", "out": "output.tif"}
    >>> superimpose_app, input_img = create_superimpose_application(params)
    >>> superimpose_app.Execute()

    """
    superimpose_app = OtbAppFactory("Superimpose", otb_parameters).otb_app
    in_img = otb_parameters["inm"]
    return superimpose_app, [in_img]


def create_train_vector_classifier_application(
    otb_parameters: dict[str, Any]
) -> tuple[OtbApp, OtbDep | None]:
    """
    Creates and configures an OTB 'TrainVectorClassifier' application.

    Parameters
    ----------
    otb_parameters : dict[str, Any]
        Dictionary containing the parameters for the 'TrainVectorClassifier' OTB application.

    Returns
    -------
    tuple[OtbApp, OtbDep | None]
        A tuple containing the created 'TrainVectorClassifier' OTB application
        and its dependencies, if any.

    Raises
    ------
    AssertionError
        If the 'feat' parameter is not provided in the otb_parameters dictionary.
    """
    parameters = dict(otb_parameters)
    features_list = parameters.pop("feat", None)
    train_vect_app = OtbAppFactory(
        "TrainVectorClassifier", parameters, update_after={"io.vd"}
    ).otb_app
    # 'feat' is actually a ParameterType_Field with could not be a list.
    assert features_list
    train_vect_app.SetParameterStringList("feat", features_list)
    return train_vect_app, None


def create_vector_classifier_application(
    otb_parameters: dict,
) -> tuple[OtbApp, OtbDep | None]:
    """
    Create an instance of OTB's VectorClassifier application.

    Parameters
    ----------
    otb_parameters : dict
        Dictionary containing parameters for configuring the VectorClassifier application.

    Returns
    -------
    vector_classif_app : OtbApp
        An instance of the VectorClassifier application.

    Examples
    --------
    >>> parameters = {"in": "input_image.tif", "out": "output.shp"}
    >>> app = create_vector_classifier_application(parameters)
    """
    parameters = dict(otb_parameters)
    features_list = parameters.pop("feat", None)
    vector_classif_app = OtbAppFactory(
        "VectorClassifier", parameters, update_after={"in"}
    ).otb_app
    if features_list:
        vector_classif_app.SetParameterStringList("feat", features_list)
    return vector_classif_app, None


def create_iota2_feature_extraction_application(
    otb_parameters: dict[str, Any]
) -> tuple[OtbApp, OtbDep | None]:
    """Create an instance of the iota2FeatureExtraction application.

    This function binds to the iota2FeatureExtraction application of the Orfeo Toolbox (OTB).

    Parameters
    ----------
    otb_parameters : dict[str, Any]
        A dictionary containing OTB's parameter keys and their corresponding values.
    logger : logging.Logger, optional
        An optional logger object for logging messages, by default LOGGER.

    Returns
    -------
    class 'otbApplication.Application'
        An instance of the iota2FeatureExtraction application, ready to be executed.

    Notes
    -----
    If 'keep_duplicates' parameter is set to True, it will be changed to False due to
    its incompatibility with 'copyinput' parameter set to False.
    """
    parameters = dict(otb_parameters)
    if "keepduplicates" in parameters and parameters["keepduplicates"] is True:
        # 'not parameters["keepduplicates"]' due to opposite
        # signification of 'keepduplicates' in iota2FeaturesExtraction
        parameters["keepduplicates"] = not parameters["keepduplicates"]

    if (
        "keepduplicates" in parameters
        and parameters["keepduplicates"] is True
        and "copyinput" in parameters
        and parameters["copyinput"] is False
    ):
        raise ValueError(
            "'iota2_feature_extraction' cannot be use with the following parameters: "
            "'keepduplicates' : True and 'copyinput' : False."
        )
    if "relrefl" in parameters and parameters["relrefl"] is not True:
        parameters.pop("relrefl")
    if "acorfeat" in parameters and parameters["acorfeat"] is not True:
        parameters.pop("acorfeat")

    features_app = OtbAppFactory("iota2FeatureExtraction", parameters).otb_app
    return features_app, None


def create_sample_augmentation_application(
    otb_parameters: dict[str, Any]
) -> tuple[OtbApp, OtbDep | None]:
    """Create an instance of the SampleAugmentation application.

    This function binds to the SampleAugmentation application of the Orfeo Toolbox (OTB).

    Parameters
    ----------
    otb_parameters : dict[str, Any]
        A dictionary containing OTB's parameter keys and their corresponding values.

    Returns
    -------
    class 'otbApplication.Application'
        An instance of the SampleAugmentation application, ready to be executed.

    Note
    ----
    For complete documentation, see:
    http://www.orfeo-toolbox.org/Applications/SampleAugmentation.html
    """
    parameters = dict(otb_parameters)
    # 'exclude' has the wrong type inside the application (field instead of list of strings)
    exclude = parameters.pop("exclude", None)
    sample_augmentation = OtbAppFactory(
        "SampleAugmentation",
        parameters,
        update_after={"in"},
    ).otb_app
    if exclude:
        sample_augmentation.SetParameterStringList("exclude", exclude)
    return sample_augmentation, None


def create_ortho_rectification(otb_parameters: dict[str, Any]) -> tuple[OtbApp, OtbDep]:
    """
    Creates an application for orthorectification using Orfeo Toolbox (OTB).

    Parameters
    ----------
    otb_parameters : dict[str, Any]
        A dictionary containing OTB parameters.

    Returns
    -------
    tuple[OtbApp, str]
        A tuple containing an instance of the OTB application for orthorectification
        and the input image file path.

    Notes
    -----
    The orthorectification application is configured with the provided parameters.
    The input image file path is extracted from the parameters.

    """
    ortho = OtbAppFactory("OrthoRectification", otb_parameters).otb_app
    input_img = otb_parameters["in"]
    if "src_meta" in otb_parameters:
        ortho.SetImageMetadata(otb_parameters["src_meta"], "io.in")
    return ortho, [input_img]


def create_multitemp_filtering_filter(
    otb_parameters: dict[str, Any]
) -> tuple[OtbApp, OtbDep | None]:
    """
    Creates an application for multi-temporal filtering using Orfeo Toolbox (OTB).

    Parameters
    ----------
    otb_parameters : dict[str, Any]
        A dictionary containing OTB parameters.

    Returns
    -------
    tuple[OtbApp, OtbDep | None]
        A tuple containing an instance of the OTB application for multi-temporal filtering,
        the input image and the outcore image.
    """
    outcore = otb_parameters["oc"]
    in_img = otb_parameters["inl"]
    sar_filtering_f = OtbAppFactory("MultitempFilteringFilter", otb_parameters).otb_app
    return sar_filtering_f, [in_img, outcore]


@dataclass
class SetterHelper:
    """dataclass utility to store otb setter fonction and an associated formatter."""

    setter: Callable
    formatter: Callable


class OtbAppFactory:
    """Create an OTB application and set its parameters."""

    def __init__(
        self, app_name: str, dict_args: dict, update_after: set[str] | None = None
    ):
        self.app_name = app_name
        self.dict_args = dict(dict_args)
        self.was_executed = False
        self.update_after = update_after
        self.mandatory_otb_img_keys = [
            "array",
            "origin",
            "spacing",
            "size",
            "region",
            "metadata",
        ]
        self.otb_types = fut.import_otb_params_types(
            "_otbApplication", "ParameterType_"
        )
        self.__otb_app = self.create_otb_app(app_name, self.dict_args)

    @property
    def otb_app(self) -> OtbApp:
        """attribute to instance of the otb application."""
        return self.__otb_app

    @staticmethod
    def guess_output_params(parameters: dict[str, str], filter_val: str) -> list[str]:
        """
        Find keys in the input dictionary whose corresponding values match the
        specified filter value.

        Parameters
        ----------
        parameters : dict[str, str]
            Dictionary containing parameters with integer values.
        filter_val : str
            The value to match against in the dictionary values.

        Returns
        -------
        list[str]
            A list of keys from the input dictionary whose corresponding values match `output_val`.
        """
        return [key for key, value in parameters.items() if value == filter_val]

    @staticmethod
    def otb_type_to_setter_helper(
        types: OTBParamsTypes, app: OtbApp
    ) -> dict[int, SetterHelper]:
        """
        Helper function to map OTB parameter types to their respective setter methods.

        This function creates a mapping between OTB parameter types and their respective setter
        methods in the given OTB application. It returns a dictionary where keys are the integer
        values of the parameter types and values are instances of the SetterHelper class,
        containing the setter method and the expected data type for the parameter.

        Parameters
        ----------
        types : OTBParamsTypes
            a dataclass representing the various parameter types in OTB.
        app : OtbApp
            An instance of the OTB application to which the parameters will be set.

        Returns
        -------
        dict[int, SetterHelper]
            A dictionary mapping OTB parameter types to their corresponding setter methods.
        """
        return {
            types.ParameterType_Int: SetterHelper(app.SetParameterInt, int),
            types.ParameterType_Float: SetterHelper(app.SetParameterFloat, float),
            types.ParameterType_Double: SetterHelper(app.SetParameterDouble, float),
            types.ParameterType_String: SetterHelper(app.SetParameterString, str),
            types.ParameterType_StringList: SetterHelper(
                app.SetParameterStringList,
                list,
            ),
            types.ParameterType_InputFilename: SetterHelper(
                app.SetParameterString,
                str,
            ),
            types.ParameterType_InputFilenameList: SetterHelper(
                app.SetParameterStringList,
                list,
            ),
            types.ParameterType_OutputFilename: SetterHelper(
                app.SetParameterString,
                str,
            ),
            types.ParameterType_Directory: SetterHelper(app.SetParameterString, str),
            types.ParameterType_Choice: SetterHelper(app.SetParameterString, str),
            types.ParameterType_InputImage: SetterHelper(app.SetParameterString, str),
            types.ParameterType_InputImageList: SetterHelper(
                app.SetParameterString,
                str,
            ),  # SetParameterStringList ? -> il etc.
            types.ParameterType_InputVectorData: SetterHelper(
                app.SetParameterString,
                str,
            ),
            types.ParameterType_InputVectorDataList: SetterHelper(
                app.SetParameterStringList,
                list,
            ),
            types.ParameterType_OutputImage: SetterHelper(app.SetParameterString, str),
            types.ParameterType_OutputVectorData: SetterHelper(
                app.SetParameterString,
                str,
            ),
            types.ParameterType_Radius: SetterHelper(app.SetParameterString, str),
            types.ParameterType_Group: SetterHelper(app.SetParameterString, str),
            types.ParameterType_ListView: SetterHelper(app.SetParameterString, str),
            types.ParameterType_RAM: SetterHelper(app.SetParameterString, str),
            types.ParameterType_Bool: SetterHelper(app.SetParameterValue, bool),
            types.ParameterType_Field: SetterHelper(app.SetParameterString, str),
            types.ParameterType_Band: SetterHelper(app.SetParameterString, str),
        }

    def get_parameters_types(self, otb_app: OtbApp) -> dict:
        """
        Retrieve the parameter types for each parameter in an OTB application.

        This function queries the OTB application for its parameters and retrieves the type
        of each parameter. It returns a dictionary where keys are parameter names and values
        are strings representing the parameter types.

        Parameters
        ----------
        otb_app : OtbApp
            An instance of the OTB application.

        Returns
        -------
        dict
            A dictionary containing parameter names as keys and their corresponding
            parameter types as values.
        """
        tmp_params = {value: key for key, value in asdict(self.otb_types).items()}
        return {
            param_name: tmp_params[otb_app.GetParameterType(param_name)]
            for param_name in otb_app.GetParametersKeys()
        }

    @staticmethod
    def set_output_image_pixel_type(
        otb_app: OtbApp, outputs_img_params: list, pix_type: str
    ) -> None:
        """
        Set inplace the pixel type for output images in an OTB application.

        This function iterates over the list of parameter names corresponding to output images
        and sets the pixel type for each output image in the OTB application using the provided
        pixel type.

        Parameters
        ----------
        otb_app : OtbApp
            An instance of the OTB application.
        outputs_img_params : list
            A list of parameter names corresponding to output images.
        pix_type : str
            The pixel type to set for the output images.
        """
        for output_img_param in outputs_img_params:
            otb_app.SetParameterOutputImagePixelType(
                output_img_param, fut.common_pix_type_to_otb(pix_type)
            )

    def get_setter_and_formatter(
        self, otb_app: OtbApp, param_type: str
    ) -> tuple[Callable, Callable]:
        """
        Retrieve the setter method and formatter function for a given OTB parameter type.

        This function retrieves the setter method and formatter function corresponding to the
        specified OTB parameter type from the helper dictionary generated by
        `otb_type_to_setter_helper`. It returns a tuple containing the setter method and formatter
        function for the given parameter type.

        Parameters
        ----------
        otb_app : OtbApp
            An instance of the OTB application.
        param_type : str
            The string representing the parameter type.

        Returns
        -------
        tuple[Callable, Callable]
            A tuple containing the setter method and formatter function for the parameter type.
        """
        types_helpers = self.otb_type_to_setter_helper(self.otb_types, otb_app)
        return (
            types_helpers[getattr(self.otb_types, param_type)].setter,
            types_helpers[getattr(self.otb_types, param_type)].formatter,
        )

    @staticmethod
    def is_input_image_list(param_type: str) -> bool:
        """
        Check if a given parameter type represents an input image list in OTB.

        This function checks if the provided parameter type string matches the string
        representation of the ParameterType_InputImageList enumeration in OTB. It returns
        True if the parameter type represents an input image list, and False otherwise.

        Parameters
        ----------
        param_type : str
            The string representing the parameter type.

        Returns
        -------
        bool
            True if the parameter type represents an input image list, False otherwise.
        """
        return param_type == "ParameterType_InputImageList"

    def handle_input_image_list(
        self, param_name: str, value: Any, otb_app: OtbApp
    ) -> None:
        """
        Handle inplace the setting of an input image list parameter in an OTB application.

        This function handles different types of input for setting an input image list parameter:
            - If `value` is a single string or a list of strings, they are assumed to be paths
              to image files, and they will be set directly using `SetParameterStringList`.
            - If `value` is a list of tuples, each tuple is expected to contain an OTB application
              and its corresponding parameter in the first element. The output image of each
              application is added to the input image list parameter using
              `AddImageToParameterInputImageList`.
            - If `value` is a list of dictionaries, each dictionary is expected to represent a
              vector image. Each vector image is imported into the OTB application using
              `ImportVectorImage`.

        Parameters
        ----------
        param_name : str
            The name of the parameter to set.
        value : Any
            The value of the parameter. It can be a single string (path to an image file),
            a list of strings (paths to multiple image files), a list of tuples (each tuple
            contains an OTB application and its corresponding parameter), or a list of
            dictionaries (each dictionary represents a vector image).
        otb_app : OtbApp
            The OTB application instance to which the parameter will be set.
        """
        if not isinstance(value, list):
            value = [value]
        if isinstance(value[0], str):
            otb_app.SetParameterStringList(param_name, value)
        elif isinstance(value[0], otb.Application):
            for current_obj in value:
                in_out_param = get_input_parameter_output(current_obj)
                otb_app.AddImageToParameterInputImageList(
                    param_name,
                    current_obj.GetParameterOutputImage(in_out_param),
                )
        elif isinstance(value[0], tuple):
            for current_obj in un_pack_first(value):
                in_out_param = get_input_parameter_output(current_obj)
                otb_app.AddImageToParameterInputImageList(
                    param_name,
                    current_obj.GetParameterOutputImage(in_out_param),
                )
        elif isinstance(value[0], dict) and all(
            elem in value[0].keys() for elem in self.mandatory_otb_img_keys
        ):
            for img_num, image in enumerate(value):
                otb_app.ImportVectorImage(param_name, image, img_num)

    @staticmethod
    def is_pipeline(value: Any) -> bool:
        """
        Check if a given value represents a pipeline in OTB.

        This function checks if the provided value is an instance of an OTB application, a tuple,
        or a dictionary, which are common representations of pipelines in OTB. It returns True
        if the value represents a pipeline, and False otherwise.

        Parameters
        ----------
        value : Any
            The value to check.

        Returns
        -------
        bool
            True if the value represents a pipeline
            (an OTB application, a tuple, or a dictionary), False otherwise.
        """
        return isinstance(value, (otb.Application, tuple, dict))

    def handle_pipeline(
        self, param_name: str, param_value: Any, otb_app: OtbApp
    ) -> None:
        """
        Handle the setting of a pipeline parameter in an OTB application.

        This function handles different types of input for setting a pipeline parameter:
            - If `param_value` is a tuple, it is expected that the first element contain an
              OTB application and its corresponding parameter. The output image of the application
              is added to the input image parameter using `SetParameterInputImage`.
            - If `param_value` is an OTB application, its output image is added to the input image
              parameter using `SetParameterInputImage`.
            - If `param_value` is a dictionary, it is expected to represent a vector image, which
              is imported into the OTB application using `ImportVectorImage`.

        Parameters
        ----------
        param_name : str
            The name of the parameter to set.
        param_value : Any
            The value of the parameter, which represents a pipeline. It can be an OTB application,
            a tuple containing an OTB application and its corresponding parameter, or a dictionary
            representing a vector image.
        otb_app : OtbApp
            The OTB application instance to which the parameter will be set.


        """
        if isinstance(param_value, tuple):
            value = param_value[0].GetParameterOutputImage(
                get_input_parameter_output(param_value[0])
            )
            otb_app.SetParameterInputImage(param_name, value)
        elif isinstance(param_value, otb.Application):
            value = param_value.GetParameterOutputImage(
                get_input_parameter_output(param_value)
            )
            otb_app.SetParameterInputImage(param_name, value)
        elif isinstance(param_value, dict) and all(
            elem in param_value.keys() for elem in self.mandatory_otb_img_keys
        ):
            otb_app.ImportVectorImage(param_name, param_value)

    def create_otb_app(self, name: str, params: dict) -> OtbApp:
        """
        Create an instance of an Orfeo Toolbox (OTB) application with the provided parameters.

        This function handles various types of parameters, including input image lists
        and pipelines. For input image lists, it automatically handles adding images to
        the parameter. For pipeline parameters, it recursively handles setting parameters
        for nested pipelines. Other parameters are set directly using their setter methods.

        Parameters
        ----------
        name : str
            The name of the OTB application to create.
        params : dict
            A dictionary containing parameters for configuring the OTB application.

        Returns
        -------
        otb_app : OtbApp
            An instance of the OTB application.

        Raises
        ------
        ValueError
            If the specified OTB application cannot be created.

        Warnings
        --------
        Warns if a parameter in the provided dictionary does not exist in the application.
        """
        otb_app: OtbApp = otb.Registry.CreateApplication(name)
        if otb_app is None:
            raise ValueError(f"{name} can't be created")
        params_types = self.get_parameters_types(otb_app)
        outputs_img_params = self.guess_output_params(
            params_types, "ParameterType_OutputImage"
        )
        for param_name, param_value in params.items():
            if param_name == "pixType":
                self.set_output_image_pixel_type(
                    otb_app, outputs_img_params, param_value
                )
                continue
            if param_name not in params_types:
                warnings.warn(
                    f"Parameter {param_name} does not exist" f"in application {name}"
                )
                continue
            setter, formatter = self.get_setter_and_formatter(
                otb_app, params_types[param_name]
            )
            value = param_value
            if self.is_input_image_list(params_types[param_name]):
                self.handle_input_image_list(param_name, value, otb_app)
                continue
            if self.is_pipeline(value):
                self.handle_pipeline(param_name, param_value, otb_app)
                continue
            value = formatter(value)
            setter(param_name, value)
            if self.update_after and param_name in self.update_after:
                otb_app.UpdateParameters()
        return otb_app


def compute_user_features(
    stack: OTBInputImage, dates: list[str], nb_component: int, expressions: list[str]
) -> tuple[OtbApp, list[str], list[OtbApp], OTBInputImage]:
    """
    usage : from a multibands/multitemporal stack of image, compute features
            define by user into configuration file at field 'additionalFeatures'

    IN

    stack [string/otbApplications] : stack of images
    dates [int] : dates in stack
    nb_component [int] : number of components by dates
    expressions [string] : user feature

    OUT
    UserFeatures [otbApplication] : otb appli, ready to Execute()
    userFeatureDate : dependance
    stack : dependance
    """

    def transform_expr_to_list_string(expr: str) -> list[str]:
        """
        Transform a bandmath expression to a list of characters. Band names need to stay grouped in
        a single string.

        Parameters
        ----------
        expr: str
            Band math expression

        Example
        -------
        expr = "(b1+b2)/(b3+b10+b1)"
        print transform_expr_to_list_string(expr)
        >> ['(', 'b1', '+', 'b2', ')', '/', '(', 'b3', '+', 'b10', '+', 'b1', ')']
        """
        result = []
        char_index = 0
        length = len(expr)

        while char_index < length:
            # band name
            if expr[char_index] == "b":
                idx_n_band = char_index + 1
                # get band number
                while idx_n_band < length and expr[idx_n_band].isdigit():
                    idx_n_band += 1
                result.append(expr[char_index:idx_n_band])
                char_index = idx_n_band
            # operator, number etc...
            else:
                result.append(expr[char_index])
                char_index += 1

        return result

    def check_bands(all_bands: set[str], nb_comp: int) -> bool:
        """
        usage : check coherence between allBands in expression and number of component

        IN :
        allBands [set of all bands requested for expression] : example set(['b1', 'b2'])
        nbComp [int] : number of possible bands

        OUT
        ok [bool]
        """
        integer_bands = [int(current_band.split("b")[-1]) for current_band in all_bands]
        return bool(max(integer_bands) <= nb_comp)

    def compute_expression_dates(expr: str, nb_date: int, nb_comp: int) -> list[str]:
        """
        from an "bandMath like" expression return bandMath expressions to each date :

        IN :
        expr [string] : bandMath - like expression
        nb_date [int] : number of dates (in a raster stack)
        nbComp [int] : number of components by dates

        OUT :
        all_expression [list of strings] : bandMath expression by dates

        Example:
        nb_date = 3
        nbComp = 10
        expr = "(b1+b2)/(b3+b10+b1)"
        print compute_expression_dates(expr,nb_date,nbComp)
        >> ['(im1b1+im1b2)/(im1b3+im1b10+im1b1)', '(im1b11+im1b12)/(im1b13+im1b20+im1b11)',
        '(im1b21+im1b22)/(im1b23+im1b30+im1b21)']
        """
        all_bands = set(re.findall(r"[b]\d+", expr))
        expression_valid = check_bands(all_bands, nb_comp)
        if not expression_valid:
            raise Exception(
                "User features expression : '"
                + expr
                + "' is not consistent with \
                            sensor's component number : "
                + str(nb_comp)
            )
        expression = transform_expr_to_list_string(expr)
        all_expression = []
        for date in range(nb_date):
            expression_date = list(expression)
            for current_band in all_bands:
                indices = list(np.where(np.array(expression) == current_band)[0])
                if not indices:
                    raise Exception(
                        "Problem in parsing expression : band "
                        + current_band
                        + " not recognize"
                    )

                for ind in indices:
                    band_number = expression_date[ind]
                    band_date = int(band_number.split("b")[-1]) + nb_comp * date
                    expression_date[ind] = "b" + str(band_date)
            all_expression.append(("".join(expression_date)).replace("b", "im1b"))

        return all_expression

    nb_dates = len(dates)
    fields = [
        "USER_Features_" + str(cpt + 1) + "_" + date
        for cpt in range(len(expressions))
        for date in dates
    ]
    expression_date = [
        compute_expression_dates(currentExpression, nb_dates, nb_component)
        for currentExpression in expressions
    ]
    flat_expr_date = [
        currentExp for currentDate in expression_date for currentExp in currentDate
    ]

    user_feature_date = []
    for expression in flat_expr_date:
        band_math_app, _ = create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": stack,
                "exp": expression,
                "ram": "2000",
                "pixType": "int16",
                "out": "None",
            },
        )
        band_math_app.Execute()
        user_feature_date.append(band_math_app)
    user_features, _ = create_application(
        AvailableOTBApp.CONCATENATE_IMAGES,
        {"il": user_feature_date, "ram": "2000", "pixType": "int16", "out": ""},
    )
    return user_features, fields, user_feature_date, stack


def write_interpolate_date_file(
    interpolation_file: PathLike,
    all_dates_file: ListPathLike,
    time_res: int | str,
) -> None:
    """determine interpolation dates boundaries strategy"""
    all_first_dates = []
    all_last_dates = []
    for dates_file in all_dates_file:
        buff = []
        with open(dates_file, encoding="utf-8") as f_dates_file:
            for line in f_dates_file:
                date = (line.rstrip()).split("t")[0]
                buff.append(int(date))
        all_first_dates.append(buff[0])
        all_last_dates.append(buff[-1])

    all_first_dates = sorted(all_first_dates)
    all_last_dates = sorted(all_last_dates)

    mini_interpol = all_first_dates[-1]

    if mini_interpol != (maxi_interpol := all_last_dates[0]):

        out_inter_dates = "\n".join(
            [
                str(interpolDate).replace("-", "")
                for interpolDate in fut.date_interval(
                    str(mini_interpol), str(maxi_interpol), time_res
                )
            ]
        )
    else:
        out_inter_dates = str(mini_interpol)

    fut.write_new_file(str(interpolation_file), str(out_inter_dates))


def write_input_date_file(in_date_file: PathLike, out_date_file: PathLike) -> None:
    """
    Read input dates from a file and write them in another file

    Parameters
    ----------
    in_date_file:
        Input dates file
    out_date_file:
        Output dates file

    Notes
    -----
    Only used with SAR (sentinel1)
    """
    all_dates = []
    with open(in_date_file, encoding="utf-8") as f_in_date_file:
        for line in f_in_date_file:
            date = (line.rstrip()).split("t")[0]
            if date not in all_dates:
                all_dates.append(date)
    out_input_dates = "\n".join(all_dates)

    fut.write_new_file(str(out_date_file), str(out_input_dates))


def sort_s1_masks(masks_list: list[str]) -> list[list[str]]:
    """
    usage : sort masks by mode and sensors

    IN
    masks_list [list of strings] : names must contain sensor name (s1a or s1b)
                                  and sensor mode (DES or ASC)
    OUT
    sorted_masks [list of list] : masks sorted as : s1aDES,s1aASC,s1bDES,s1bASC
    """
    sorted_masks = []
    s1_des = [
        CMask for CMask in masks_list if CMask.split("/")[-1].split("_")[3] == "DES"
    ]
    s1_asc = [
        CMask for CMask in masks_list if CMask.split("/")[-1].split("_")[3] == "ASC"
    ]

    if s1_des:
        sorted_masks.append(sorted(s1_des, key=get_dates_in_otb_output_name))
        sorted_masks.append(sorted(s1_des, key=get_dates_in_otb_output_name))
    if s1_asc:
        sorted_masks.append(sorted(s1_asc, key=get_dates_in_otb_output_name))
        sorted_masks.append(sorted(s1_asc, key=get_dates_in_otb_output_name))

    return sorted_masks


def get_sar_stack(
    sar_config: str,
    tile_name: str,
    all_tiles: list[str],
    features_path: str,
    working_directory: str | None = None,
) -> tuple[OtbApp, list[list[str]], list[str], list[str]]:
    """
    function used to compute interpolation files
    """
    config = configparser.ConfigParser()
    config.read(sar_config)
    output_directory = config.get("Paths", "Output")
    time_res = config.get(
        "Processing",
        "TemporalResolution",
        fallback=S1_CONST.s1_temporal_res,
    )

    interp_date_files = []
    input_date_files = []

    all_filtered, all_masks = s1p.s1_preprocess(
        sar_config, tile_name, working_directory, get_filtered=True
    )
    # Input dates current tile
    try:
        in_date_files_s1_vv_asc = fut.file_search_and(
            str(Path(output_directory) / tile_name[1:]), True, "S1_vv_ASC_dates.txt"
        )[0]
        s1_vv_asc_i2 = str(
            Path(features_path)
            / tile_name
            / "tmp"
            / f"Sentinel1_ASC_{tile_name}_input_dates.txt"
        )
        write_input_date_file(in_date_files_s1_vv_asc, s1_vv_asc_i2)
    except IndexError:
        in_date_files_s1_vv_asc = []
    try:
        in_date_files_s1_vh_asc = fut.file_search_and(
            str(Path(output_directory) / tile_name[1:]), True, "S1_vh_ASC_dates.txt"
        )[0]
        s1_vh_asc_i2 = str(
            Path(features_path)
            / tile_name
            / "tmp"
            / f"Sentinel1_ASC_{tile_name}_input_dates.txt"
        )
        write_input_date_file(in_date_files_s1_vh_asc, s1_vh_asc_i2)
    except IndexError:
        in_date_files_s1_vh_asc = []
    try:
        in_date_files_s1_vv_des = fut.file_search_and(
            str(Path(output_directory) / tile_name[1:]), True, "S1_vv_DES_dates.txt"
        )[0]
        s1_vv_des_i2 = str(
            Path(features_path)
            / tile_name
            / "tmp"
            / f"Sentinel1_DES_{tile_name}_input_dates.txt"
        )
        write_input_date_file(in_date_files_s1_vv_des, s1_vv_des_i2)
    except IndexError:
        in_date_files_s1_vv_des = []
    try:
        in_date_files_s1_vh_des = fut.file_search_and(
            str(Path(output_directory, tile_name[1:])), True, "S1_vh_DES_dates.txt"
        )[0]
        s1_vh_des_i2 = str(
            Path(features_path)
            / tile_name
            / "tmp"
            / f"Sentinel1_DES_{tile_name}_input_dates.txt"
        )
        write_input_date_file(in_date_files_s1_vh_des, s1_vh_des_i2)
    except IndexError:
        in_date_files_s1_vh_des = []

    # Input dates all tiles
    all_in_date_file_s1_vv_des = []
    if in_date_files_s1_vv_des:
        all_in_date_file_s1_vv_des = [
            fut.file_search_and(
                str(Path(output_directory) / tile[1:]), True, "S1_vv_DES_dates.txt"
            )[0]
            for tile in all_tiles
        ]
        interp_date_files_s1_vv_des = str(
            Path(features_path)
            / tile_name
            / "tmp"
            / Path(in_date_files_s1_vv_des.replace(".txt", "_interpolation.txt")).name,
        )
        write_interpolate_date_file(
            interp_date_files_s1_vv_des, all_in_date_file_s1_vv_des, time_res
        )
        input_date_files.append(s1_vv_des_i2)
        interp_date_files.append(interp_date_files_s1_vv_des)

    all_in_date_file_s1_vh_des = []
    if in_date_files_s1_vh_des:
        all_in_date_file_s1_vh_des = [
            fut.file_search_and(
                str(Path(output_directory, tile[1:])), True, "S1_vh_DES_dates.txt"
            )[0]
            for tile in all_tiles
        ]
        interp_date_files_s1_vh_des = str(
            Path(features_path)
            / tile_name
            / "tmp"
            / Path(in_date_files_s1_vh_des.replace(".txt", "_interpolation.txt")).name,
        )
        write_interpolate_date_file(
            interp_date_files_s1_vh_des, all_in_date_file_s1_vh_des, time_res
        )
        input_date_files.append(s1_vh_des_i2)
        interp_date_files.append(interp_date_files_s1_vh_des)

    all_in_date_file_s1_vv_asc = []
    if in_date_files_s1_vv_asc:
        all_in_date_file_s1_vv_asc = [
            fut.file_search_and(
                str(Path(output_directory) / tile[1:]), True, "S1_vv_ASC_dates.txt"
            )[0]
            for tile in all_tiles
        ]
        interp_date_files_s1_vv_asc = str(
            Path(features_path)
            / tile_name
            / "tmp"
            / Path(in_date_files_s1_vv_asc.replace(".txt", "_interpolation.txt")).name,
        )
        write_interpolate_date_file(
            interp_date_files_s1_vv_asc, all_in_date_file_s1_vv_asc, time_res
        )
        input_date_files.append(s1_vv_asc_i2)
        interp_date_files.append(interp_date_files_s1_vv_asc)

    all_in_date_file_s1_vh_asc = []
    if in_date_files_s1_vh_asc:
        all_in_date_file_s1_vh_asc = [
            fut.file_search_and(
                str(Path(output_directory) / tile[1:]), True, "S1_vh_ASC_dates.txt"
            )[0]
            for tile in all_tiles
        ]
        interp_date_files_s1_vh_asc = str(
            Path(features_path)
            / tile_name
            / "tmp"
            / Path(in_date_files_s1_vh_asc.replace(".txt", "_interpolation.txt")).name,
        )
        write_interpolate_date_file(
            interp_date_files_s1_vh_asc, all_in_date_file_s1_vh_asc, time_res
        )
        input_date_files.append(s1_vh_asc_i2)
        interp_date_files.append(interp_date_files_s1_vh_asc)

    all_masks = sort_s1_masks(all_masks[0])

    return all_filtered, all_masks, interp_date_files, input_date_files


def generate_sar_feat_dates(
    sar_expressions: list[str],
    sar_dict: SARDatesDict,
    output_raster: str | None = None,
) -> tuple[OtbApp, list[str]]:
    """
    Generate SAR features

    Parameters
    ----------
    sar_expressions:
        List containing string features expression
    sar_dict:
        Dictionary containing SAR applications
    output_raster:
        Path of the output raster, None by default (if None, no raster is written)

    Notes
    -----
    The SAR dictionary should be as follows (with otb applications):
    sar_time_series = {
            "asc": {
                "vv": {"App": app, "availDates": app},
                "vh": {"App": app, "availDates": app},
            },
            "des": {
                "vv": {"App": app, "availDates": app},
                "vh": {"App": app, "availDates": app},
            },
        }
    """
    asc = bool(sar_dict["asc"]["vv"]["App"])
    des = bool(sar_dict["des"]["vv"]["App"])
    sar_labels = []

    if asc:
        asc_features_exp = compute_sar_features_dates_expressions(
            sar_expressions, len(sar_dict["asc"]["vv"]["availDates"])
        )
        input_features = [sar_dict["asc"]["vv"]["App"], sar_dict["asc"]["vh"]["App"]]
        expr = [elem for featuresDates in asc_features_exp for elem in featuresDates]
        sar_labels = [
            f"sentinel1_asc_userfeature{i + 1}_{date}"
            for i in range(len(asc_features_exp))
            for date in sar_dict["asc"]["vv"]["availDates"]
        ]
    if des:
        des_features_exp = compute_sar_features_dates_expressions(
            sar_expressions, len(sar_dict["des"]["vv"]["availDates"])
        )
        input_features = [sar_dict["des"]["vv"]["App"], sar_dict["des"]["vh"]["App"]]
        expr = [elem for featuresDates in des_features_exp for elem in featuresDates]
        sar_labels = [
            f"sentinel1_des_userfeature{i + 1}_{date}"
            for i in range(len(des_features_exp))
            for date in sar_dict["des"]["vv"]["availDates"]
        ]
    if asc and des:
        des_features_exp_tmp = list(des_features_exp)
        des_features_exp = []
        for feature_dates in des_features_exp_tmp:

            des_features_exp.append(
                [
                    feature_date.replace("im1", "im3").replace("im2", "im4")
                    for feature_date in feature_dates
                ]
            )
        input_features = [
            sar_dict["asc"]["vv"]["App"],
            sar_dict["asc"]["vh"]["App"],
            sar_dict["des"]["vv"]["App"],
            sar_dict["des"]["vh"]["App"],
        ]
        expr = []
        sar_labels = []
        feature_counter = 0
        for asc_feature, des_feature in zip(asc_features_exp, des_features_exp):
            for index_date, asc_expr in enumerate(asc_feature):
                expr.append(asc_expr)
                ascvv = sar_dict["asc"]["vv"]["availDates"][index_date]
                sar_labels.append(
                    f"sentinel1_asc_userfeature{feature_counter + 1}_{ascvv}"
                )
            for index_date, des_expr in enumerate(des_feature):
                expr.append(des_expr)
                desvv = sar_dict["des"]["vv"]["availDates"][index_date]
                sar_labels.append(
                    f"sentinel1_des_userfeature{feature_counter + 1}_{desvv}"
                )
            feature_counter += 1
    else:
        raise ValueError("At least one orbit type is required")

    return (
        create_application(
            AvailableOTBApp.BAND_MATH_X,
            {
                "il": input_features,
                "out": output_raster if output_raster else "",
                "exp": ";".join(expr),
            },
        )[0],
        sar_labels,
    )


def compute_sar_features_dates_expressions(
    sar_expressions: list[str], nb_dates: int
) -> list[list[str]]:
    """function use to compute sar features assuming VV is 'im1' and
    VH is 'im2'

    Parameters
    ----------
    sar_expressions : list
        list containing string features expression
    nb_dates : int
        number of dates

    Return
    ------
    list
    """
    out_expressions = []
    for sar_expr in sar_expressions:
        sar_expr = sar_expr.lower().replace(" ", "")
        sar_date = [
            sar_expr.replace("vv", f"im1b{i + 1}").replace("vh", f"im2b{i + 1}")
            for i in range(nb_dates)
        ]
        out_expressions.append(sar_date)
    return out_expressions


class AvailableOTBApp(str, Enum):
    """
    Enumeration of available applications in the Orfeo Toolbox (OTB).
    Each member of the enumeration represents an OTB application with its name.

    Attributes
    ----------
    BAND_MATH : str
        "BandMath"
    BAND_MATH_X : str
        "BandMathX"
    BINARY_MORPHOLOGICAL_OPERATION : str
        "BinaryMorphologicalOperation"
    CONFUSION_MATRIX : str
        "ComputeConfusionMatrix"
    CONCATENATE_IMAGES : str
        "ConcatenateImages"
    EXTRACT_ROI : str
        "ExtractROI"
    FUSION_OF_CLASSIFICATIONS : str
        "FusionOfClassifications"
    IOTA2_FEATURES_EXTRACTION : str
        "iota2FeatureExtraction"
    IMAGE_CLASSIFIER : str
        "ImageClassifier"
    IMAGE_TIME_SERIES_GAPFILLING : str
        "ImageTimeSeriesGapFilling"
    MULTITEMP_FILTERING_FILTER : str
        "MultitempFilteringFilter"
    MULTITEMP_FILTERING_OUTCORE : str
        "MultitempFilteringOutcore"
    ORTHO_RECTIFICATION : str
        "OrthoRectification"
    POLYGON_CLASS_STATISTICS : str
        "PolygonClassStatistics"
    RASTERIZATION : str
        "Rasterization"
    SAMPLE_AUGMENTATION : str
        "SampleAugmentation"
    SAMPLE_EXTRACTION : str
        "SampleExtraction"
    SAMPLE_SELECTION : str
        "SampleSelection"
    SAR_CALIBRATION : str
        "SARCalibration"
    SEGMENTATION : str
        "Segmentation"
    SLIC : str
        "SLIC"
    SUPERIMPOSE : str
        "Superimpose"
    TRAIN_VECTOR_CLASSIFIER : str
        "TrainVectorClassifier"
    VECTOR_CLASSIFIER : str
        "VectorClassifier"
    ZONAL_STATISTICS : str
        "ZonalStatistics"
    """

    BAND_MATH = "BandMath"
    BAND_MATH_X = "BandMathX"
    BINARY_MORPHOLOGICAL_OPERATION = "BinaryMorphologicalOperation"
    CONFUSION_MATRIX = "ComputeConfusionMatrix"
    CONCATENATE_IMAGES = "ConcatenateImages"
    EXTRACT_ROI = "ExtractROI"
    FUSION_OF_CLASSIFICATIONS = "FusionOfClassifications"
    IOTA2_FEATURES_EXTRACTION = "iota2FeatureExtraction"
    IMAGE_CLASSIFIER = "ImageClassifier"
    IMAGE_TIME_SERIES_GAPFILLING = "ImageTimeSeriesGapFilling"
    MULTITEMP_FILTERING_FILTER = "MultitempFilteringFilter"
    MULTITEMP_FILTERING_OUTCORE = "MultitempFilteringOutcore"
    ORTHO_RECTIFICATION = "OrthoRectification"
    POLYGON_CLASS_STATISTICS = "PolygonClassStatistics"
    RASTERIZATION = "Rasterization"
    SAMPLE_AUGMENTATION = "SampleAugmentation"
    SAMPLE_EXTRACTION = "SampleExtraction"
    SAMPLE_SELECTION = "SampleSelection"
    SAR_CALIBRATION = "SARCalibration"
    SEGMENTATION = "Segmentation"
    SLIC = "SLIC"
    SUPERIMPOSE = "Superimpose"
    TRAIN_VECTOR_CLASSIFIER = "TrainVectorClassifier"
    VECTOR_CLASSIFIER = "VectorClassifier"
    ZONAL_STATISTICS = "ZonalStatistics"


@dataclass
class OTBApplication:
    """
    Represents an Orfeo Toolbox (OTB) application configuration.

    Attributes
    ----------
    name : AvailableOTBApp
        The name of the OTB application.
    update_after : set[str] | None
        A set of parameters names that the application needs to be
        updated after these parameters
    builder : Callable[[dict[str, Any]], tuple[OtbApp, OtbDep | None]] | None
        A callable to build or configure the application, by default None.
    """

    name: AvailableOTBApp
    update_after: set[str] | None = None
    builder: Callable[[dict[str, Any]], tuple[OtbApp, OtbDep | None]] | None = None


OtbApplicationBuilder = {
    AvailableOTBApp.BAND_MATH: OTBApplication(AvailableOTBApp.BAND_MATH),
    AvailableOTBApp.BAND_MATH_X: OTBApplication(AvailableOTBApp.BAND_MATH_X),
    AvailableOTBApp.BINARY_MORPHOLOGICAL_OPERATION: OTBApplication(
        AvailableOTBApp.BINARY_MORPHOLOGICAL_OPERATION
    ),
    AvailableOTBApp.CONCATENATE_IMAGES: OTBApplication(
        AvailableOTBApp.CONCATENATE_IMAGES
    ),
    AvailableOTBApp.CONFUSION_MATRIX: OTBApplication(
        AvailableOTBApp.CONFUSION_MATRIX, update_after={"in"}
    ),
    AvailableOTBApp.EXTRACT_ROI: OTBApplication(
        AvailableOTBApp.EXTRACT_ROI, update_after={"in"}
    ),
    AvailableOTBApp.FUSION_OF_CLASSIFICATIONS: OTBApplication(
        AvailableOTBApp.FUSION_OF_CLASSIFICATIONS
    ),
    AvailableOTBApp.IOTA2_FEATURES_EXTRACTION: OTBApplication(
        AvailableOTBApp.IOTA2_FEATURES_EXTRACTION,
        builder=create_iota2_feature_extraction_application,
    ),
    AvailableOTBApp.IMAGE_CLASSIFIER: OTBApplication(AvailableOTBApp.IMAGE_CLASSIFIER),
    AvailableOTBApp.IMAGE_TIME_SERIES_GAPFILLING: OTBApplication(
        AvailableOTBApp.IMAGE_TIME_SERIES_GAPFILLING
    ),
    AvailableOTBApp.MULTITEMP_FILTERING_FILTER: OTBApplication(
        AvailableOTBApp.MULTITEMP_FILTERING_FILTER,
        builder=create_multitemp_filtering_filter,
    ),
    AvailableOTBApp.MULTITEMP_FILTERING_OUTCORE: OTBApplication(
        AvailableOTBApp.MULTITEMP_FILTERING_OUTCORE
    ),
    AvailableOTBApp.ORTHO_RECTIFICATION: OTBApplication(
        AvailableOTBApp.ORTHO_RECTIFICATION, builder=create_ortho_rectification
    ),
    AvailableOTBApp.POLYGON_CLASS_STATISTICS: OTBApplication(
        AvailableOTBApp.POLYGON_CLASS_STATISTICS
    ),
    AvailableOTBApp.RASTERIZATION: OTBApplication(AvailableOTBApp.RASTERIZATION),
    AvailableOTBApp.SAR_CALIBRATION: OTBApplication(AvailableOTBApp.SAR_CALIBRATION),
    AvailableOTBApp.SAMPLE_AUGMENTATION: OTBApplication(
        AvailableOTBApp.SAMPLE_AUGMENTATION,
        builder=create_sample_augmentation_application,
    ),
    AvailableOTBApp.SAMPLE_EXTRACTION: OTBApplication(
        AvailableOTBApp.SAMPLE_EXTRACTION, update_after={"vec"}
    ),
    AvailableOTBApp.SAMPLE_SELECTION: OTBApplication(
        AvailableOTBApp.SAMPLE_SELECTION, update_after={"vec", "instats"}
    ),
    AvailableOTBApp.SEGMENTATION: OTBApplication(AvailableOTBApp.SEGMENTATION),
    AvailableOTBApp.SLIC: OTBApplication(AvailableOTBApp.SLIC),
    AvailableOTBApp.SUPERIMPOSE: OTBApplication(
        AvailableOTBApp.SUPERIMPOSE, builder=create_superimpose_application
    ),
    AvailableOTBApp.TRAIN_VECTOR_CLASSIFIER: OTBApplication(
        AvailableOTBApp.TRAIN_VECTOR_CLASSIFIER,
        builder=create_train_vector_classifier_application,
    ),
    AvailableOTBApp.VECTOR_CLASSIFIER: OTBApplication(
        AvailableOTBApp.VECTOR_CLASSIFIER, builder=create_vector_classifier_application
    ),
    AvailableOTBApp.ZONAL_STATISTICS: OTBApplication(AvailableOTBApp.ZONAL_STATISTICS),
}


@add_compression_options
def create_application(
    name: AvailableOTBApp, otb_parameters: dict[str, Any]
) -> tuple[OtbApp, OtbDep | None]:
    """
    Creates an OTB application based on the given name and parameters.

    Parameters
    ----------
    name : AvailableOTBApp
        The name of the OTB application to create.
    otb_parameters : dict[str, Any]
        A dictionary of parameters to configure the OTB application.

    Returns
    -------
    tuple[OtbApp, OtbDep | None]
        A tuple containing the created OTB application and optionally its dependencies.

    Raises
    ------
    ValueError
        If the application name is not handled.

    Examples
    --------
    >>> app, deps = create_application(AvailableOTBApp.BAND_MATH,
                                       {"in": "input.tif", "out": "output.tif"})
    """
    if name not in OtbApplicationBuilder:
        raise ValueError(
            f"Application '{name}' is currently not handled. "
            f"Available application list : {', '.join(list(OtbApplicationBuilder.keys()))}"
        )
    if OtbApplicationBuilder[name].builder:
        return OtbApplicationBuilder[name].builder(dict(otb_parameters))  # type: ignore
    return (
        OtbAppFactory(
            name.value,
            dict(otb_parameters),
            update_after=OtbApplicationBuilder[name].update_after,
        ).otb_app,
        None,
    )
