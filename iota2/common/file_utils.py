#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" This module contains a lot of useful file manipulation functions"""

import csv
import datetime
import errno
import glob
import hashlib
import logging
import os
import random
import re
import resource
import shutil
from collections import defaultdict
from collections.abc import Callable, Generator
from dataclasses import dataclass
from datetime import timedelta
from importlib.util import module_from_spec, spec_from_file_location
from pathlib import Path
from types import ModuleType

import numpy as np
import osgeo
import otbApplication as otb
from numpy._typing import ArrayLike
from osgeo import gdal, osr

import iota2.common.raster_utils as r_utils
from config import Config
from iota2.common.utils import RemoveInStringList, run
from iota2.typings.i2_types import (
    CoordXY,
    InputRaster,
    LabelsCoords,
    ListPathLike,
    OTBParamsTypes,
    OTBType,
    PathLike,
    SortedLabelsCoords,
)

LOGGER = logging.getLogger("distributed.worker")


@dataclass
class TileMergeOptions:
    """
    Dataclass containing options for the `assemble_tile_merge` function.

    Attributes
    ----------
    spatial_resolution
        x,y spatial resolution in meters
    output_pixel_type
        (optional) output pixel type (gdal format). Goes to `ot` gdal parameter
    creation_options:
        (optional) gdal rasters creation options. Goes to `co` gdal parameter
    output_no_data:
        (optional) value representing absence of data in output raster if set.
        Goes to `a_nodata` gdal parameter if not None

    """

    spatial_resolution: CoordXY
    output_pixel_type: str = "Int16"
    creation_options: dict | None = None
    output_no_data: int | None = None
    available_ram: int | None = None


def is_dir_empty(dir_path: PathLike) -> bool:
    """
    Check if a directory is empty.

    Parameters
    ----------
    dir_path: PathLike

    Returns
    -------
    bool:
        True if the directory is empty; False otherwise
    """
    return not any(Path(dir_path).iterdir())


def import_otb_params_types(module_name: str, regex_pattern: str) -> OTBParamsTypes:
    """
    Import items from the specified module whose names match the given regular expression pattern.

    Parameters
    ----------
    module_name : str
        Name of the module to import items from.
    regex_pattern : str
        Regular expression pattern to match the item names.

    Returns
    -------
    dataclass
        dataclass containing the imported items.

    Raises
    ------
    ImportError
        If the module cannot be imported.
    """
    imported_module = __import__(module_name)
    module_dict = imported_module.__dict__
    matching_items = {}
    regex = re.compile(regex_pattern)
    for name, item in module_dict.items():
        if re.match(regex, name):
            matching_items[name] = item
    return OTBParamsTypes(**matching_items)


def walk(path_like: str | Path) -> Generator:
    """Pathlib equivalent to os.walk()

    Parameters
    ----------
    path_like:
        path to walk (str or pathlib.Path)
    """
    if not Path(path_like).is_dir():
        return
    for path in Path(path_like).iterdir():
        if path.is_dir():
            yield from walk(path)
            continue
        yield path.resolve()


def path_back_until(path_like: str | Path, until: str) -> None | Path:
    """
    Return the path of a file back until a given parent directory (None if it doesn't exist)

    Parameters
    ----------
    path_like
        Path of the desired file/directory
    until
        Parent directory

    Examples
    --------
    >>> path_to_a_file = "dir1/dir2/dir3/file.txt"
    >>> print(path_back_until("dir1/dir2/dir3/file.txt", "dir2"))
    "dir2/dir3/file.txt"
    """
    tree_list = Path(path_like).parts
    try:
        index = tree_list.index(until)
    except ValueError:
        return None
    return Path(*tree_list[index:])


def verify_import(module_path: str | None) -> ModuleType:
    """Check if a module can be imported

    Parameters
    ----------
    module_path:
        the path to module

    """
    assert module_path is not None
    spec = spec_from_file_location(Path(module_path).stem, module_path)
    assert spec is not None
    loader = spec.loader
    assert loader is not None
    module = module_from_spec(spec)

    loader.exec_module(module)
    return module


def md5(file_name: str) -> str:
    """
    return the md5 of a file
    Parameters
    ----------
    file_name:
        the file name
    """
    hash_md5 = hashlib.md5()
    with open(file_name, "rb") as fil:
        for chunk in iter(lambda: fil.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def detect_csv_delimiter(csv_file: PathLike) -> str:
    """detect csv delimiter"""
    sniffer = csv.Sniffer()
    with open(csv_file, encoding="utf-8") as csvfile:
        first_line = next(csvfile).rstrip()
    dialect = sniffer.sniff(first_line)
    delimiter = dialect.delimiter
    return delimiter


def find_and_replace_file(
    input_file: str, output_file: str, str_to_find: str, new_str: str
) -> None:
    """search string in file and replace by the new one into an output file"""
    with open(input_file, encoding="utf-8") as in_file:
        with open(output_file, "w", encoding="utf-8") as out_file:
            for line in in_file:
                out_file.write(line.replace(str_to_find, new_str))


def is_writable_directory(directory_path: str) -> bool:
    """
    ensure a directory is writable
    Parameters
    ----------
    directory_path:
        the directory to be tested
    """
    out = True
    try:
        Path(directory_path).mkdir(parents=True, exist_ok=True)
    except OSError:
        out = False
    return out


def get_iota2_project_dir() -> Path:
    """Return iota2's installation directory"""
    parent = Path(__file__).resolve().parent.parent
    iota2dir = parent.parent
    return iota2dir


def ensure_dir(dirname: str, raise_exe: bool = True) -> None:
    """
    Ensure that a named directory exists; if it does not, attempt to create it.
    """
    try:
        os.makedirs(dirname)
    except OSError as err:
        if err.errno != errno.EEXIST:
            if raise_exe:
                raise


def write_new_file(new_file: str, file_content: str) -> None:
    """
    write a new file
    """
    with open(new_file, "w", encoding="utf-8") as new_f:
        new_f.write(file_content)


def memory_usage_psutil(unit: str = "MB") -> float:
    """Return the memory usage
    Parameters
    ----------
    unit:
        the expect unit for return ram (MB or GB)
    """
    # return the memory usage in MB

    if unit == "MB":
        coeff = 1000.0
    elif unit == "GB":
        coeff = 1000.0 * 1000.0
    else:
        raise ValueError(f"Invalid unit: '{unit}'")
    mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / coeff
    return mem


def date_interval(
    date_min: str, date_max: str, temp_res: int | str
) -> Generator[datetime.date, datetime.date, None]:
    """
    dateMin [string] : Ex -> 20160101
    dateMax [string] > dateMin
    tr [int/string] -> temporal resolution
    """
    start = datetime.date(int(date_min[0:4]), int(date_min[4:6]), int(date_min[6:8]))
    end = datetime.date(int(date_max[0:4]), int(date_max[4:6]), int(date_max[6:8]))
    delta = timedelta(days=int(temp_res))
    curr = start
    while curr <= end:
        yield curr
        curr += delta


def update_directory(src: PathLike, dst: PathLike) -> None:
    """
    Copy all content of source directory to destination directory if it doesn't already exist

    Parameters
    ----------
    src:
        Source directory
    dst:
        Destination directory
    """
    src, dst = Path(src), Path(dst)
    for current_content in src.iterdir():
        if (src / current_content).is_file():
            if not (dst / current_content).exists():
                shutil.copy(src / current_content, dst / current_content)
        if (src / current_content).is_dir():
            if not (dst / current_content).exists():
                try:
                    shutil.copytree(src / current_content, dst / current_content)
                # python >2.5
                except OSError as exc:
                    if exc.errno == errno.ENOTDIR:
                        shutil.copy(src, dst)
                    else:
                        raise


def get_date_landsat(path_landsat: str, tiles: list[str]) -> tuple[str, str]:
    """
    Get the min and max dates for the given tile.
    """
    date_min = 30000000000
    date_max = 0
    all_tiles_path = [Path(path_landsat) / tile for tile in tiles]
    for tile_path in all_tiles_path:
        for file in walk(tile_path):
            date = int(file.name.split("_")[3])
            date_max = max(date_max, date)
            date_min = min(date_min, date)

    return str(date_min), str(date_max)


def get_date_l8usgs(path_l8: str, tiles: list[str]) -> tuple[str, str]:
    """
    Get the min and max dates for the given tile.
    """
    date_pos = 3
    date_min = 30000000000
    date_max = 0
    for tile in tiles:
        folder = [element.name for element in (Path(path_l8) / tile).iterdir()]
        for file_name in folder:
            if (
                file_name.count(".tgz") == 0
                and file_name.count(".jpg") == 0
                and file_name.count(".xml") == 0
            ):
                date = int(file_name.split("_")[date_pos])
                date_max = max(date_max, date)
                date_min = min(date_min, date)

    return str(date_min), str(date_max)


def get_date_s2(path_s2: str, tiles: list[str]) -> tuple[str, str]:
    """
    Get the min and max dates for the given tile.
    """
    date_pos = 2
    if "T" in tiles[0]:
        date_pos = 1
    date_min = 30000000000
    date_max = 0
    for tile in tiles:
        folder = [element.name for element in (Path(path_s2) / tile).iterdir()]
        for file_name in folder:
            if (
                file_name.count(".tgz") == 0
                and file_name.count(".jpg") == 0
                and file_name.count(".xml") == 0
            ):
                date = int(file_name.split("_")[date_pos].split("-")[0])
                date_max = max(date_max, date)
                date_min = min(date_min, date)

    return str(date_min), str(date_max)


def get_date_s2_s2c(path_s2: str, tiles: list[str]) -> tuple[str, str]:
    """
    Get the min and max dates for the given tile.
    """
    date_pos = 2
    date_min = 30000000000
    date_max = 0
    for tile in tiles:
        folder = [element.name for element in (Path(path_s2) / tile).iterdir()]
        for file_name in folder:
            if (
                file_name.count(".tgz") == 0
                and file_name.count(".jpg") == 0
                and file_name.count(".xml") == 0
            ):
                date = int(file_name.split("_")[date_pos].split("T")[0])
                date_max = max(date_max, date)
                date_min = min(date_min, date)

    return str(date_min), str(date_max)


def common_pix_type_to_otb(string: str) -> OTBType:
    """Convert a name to an otb pixel type
    Parameters
    ----------
    string:
        the name to convert
    """

    dico: dict[str, OTBType] = {
        "complexDouble": otb.ComplexImagePixelType_double,
        "complexFloat": otb.ComplexImagePixelType_float,
        "double": otb.ImagePixelType_double,
        "float": otb.ImagePixelType_float,
        "int16": otb.ImagePixelType_int16,
        "int32": otb.ImagePixelType_int32,
        "uint16": otb.ImagePixelType_uint16,
        "uint32": otb.ImagePixelType_uint32,
        "uint8": otb.ImagePixelType_uint8,
    }
    try:
        return dico[string]
    except KeyError as k_err:
        raise KeyError(
            "Error in common_pix_type_to_otb function input parameter : "
            + string
            + " not available, choices are :"
            "'complexDouble','complexFloat','double','float',"
            "'int16','int32','uint16','uint32','uint8'"
        ) from k_err


def split_list(input_list: list, nb_split: int) -> list:
    """
    IN :
    input_list [list]
    nb_split [int] : number of output fold

    OUT :
    split_list [list of nbSplit list]

    Examples :
        foo = ['a', 'b', 'c', 'd', 'e']
        print split_list(foo,4)
        >> [['e', 'c'], ['d'], ['a'], ['b']]

        print split_list(foo,8)
        >> [['b'], ['d'], ['c'], ['e'], ['a'], ['d'], ['a'], ['b']]
    """

    def chunk(xss: list, nb_fold: int) -> Generator:
        yss = list(xss)
        random.shuffle(yss)
        size = len(yss) // nb_fold
        leftovers = yss[size * nb_fold :]
        for fold_num in range(nb_fold):
            if leftovers:
                extra = [leftovers.pop()]
            else:
                extra = []
            yield yss[fold_num * size : (fold_num + 1) * size] + extra

    split_list_in = list(chunk(input_list, nb_split))

    # check empty content (if nbSplit > len(Inlist))
    all_elem = []
    for splits in split_list_in:
        for split in splits:
            if split not in all_elem:
                all_elem.append(split)

    for _, item in enumerate(split_list_in):
        if len(item) == 0:
            random_choice = random.sample(all_elem, 1)[0]
            item.append(random_choice)

    return split_list_in


def sort_by_first_elem(my_list: list) -> list:
    """
    Example 1:
        MyList = [(1,2), (1,1), (6,1), (1,4), (6,7)]
        print sortByElem(MyList)
        >> [(1, [2, 1, 4]), (6, [1, 7])]

    Example 2:
        MyList = [((1,6),2), ((1,6),1), ((1,2),1), ((1,6),4), ((1,2),7)]
        print sortByElem(MyList)
        >> [((1, 2), [1, 7]), ((1, 6), [2, 1, 4])]
    """
    dic_temp = defaultdict(list)
    for key, value in my_list:
        dic_temp[key].append(value)
    return list(dic_temp.items())


def array_to_raster(
    array: np.ndarray, output: str, model: str, driver: str = "GTiff"
) -> None:
    """convert a numpy array to a gdal raster

    Parameters:
    array:
        numpy nd-array
    output:
        output image name
    model:
        a reference raster to provide metadata
    driver:
        the expected driver
    """
    used_driver = gdal.GetDriverByName(driver)
    raster_metadata = r_utils.read_raster_metadata(model)

    assert raster_metadata.img_size is not None
    assert raster_metadata.pix_size is not None
    assert raster_metadata.img_origin is not None
    assert raster_metadata.projection is not None
    cols = raster_metadata.img_size.x_size
    rows = raster_metadata.img_size.y_size
    out_raster = used_driver.Create(output, cols, rows, 1, gdal.GDT_Byte)
    out_raster.SetGeoTransform(
        (
            raster_metadata.img_origin.x_origin,
            raster_metadata.pix_size.pixel_width,
            0,
            raster_metadata.img_origin.y_origin,
            0,
            raster_metadata.pix_size.pixel_height,
        )
    )
    outband = out_raster.GetRasterBand(1)
    outband.WriteArray(array)
    out_raster.SetProjection(raster_metadata.projection.ExportToWkt())
    outband.FlushCache()


def get_raster_resolution(raster_in: InputRaster) -> tuple[float, float] | None:
    """
    Return pixel size X and y from geotransform (and None if file doesn't exist)

    Parameters
    ----------
        raster_in: path to raster
    """
    try:
        raster = open_raster(raster_in)
    except FileExistsError:
        return None
    geotransform = raster.GetGeoTransform()
    spacing_x = geotransform[1]
    spacing_y = geotransform[5]
    return spacing_x, spacing_y


def report_band_description(input_raster_path: str, output_raster_path: str) -> None:
    """
    Parameters
    ----------

    input_raster_path:
        path to a raster of the band descriptions to copy
    output_raster_path:
        path to a raster without the band description
    """
    output_raster = gdal.Open(output_raster_path, gdal.GA_Update)
    input_raster = gdal.Open(input_raster_path)
    if input_raster.RasterCount == output_raster.RasterCount:
        for band in range(1, input_raster.RasterCount + 1):
            output_band = output_raster.GetRasterBand(band)
            input_band = input_raster.GetRasterBand(band)
            label = input_band.GetDescription()
            output_band.SetDescription(label)
    else:
        LOGGER.warning(
            "the rasters do not have the same number of bands, "
            "so the band descriptions of %s will not be copied to the bands of %s",
            input_raster,
            output_raster,
        )
    del output_raster
    del input_raster


def assemble_tile_merge(
    raster_list: list[str],
    output_path: str,
    merge_options: TileMergeOptions,
    input_no_data: int = 0,
) -> None:
    """
    Wrapper around gdal_merge utility. Merges multiple raster into a single one

    Parameters
    ----------
    raster_list:
        path of rasters to merge
    output_path
        path of output raster
    merge_options:
        options given for the gdal merge command
    input_no_data:
        value of input raster ignored in merge. Goes to `n` gdal parameter

    Notes
    -----
    Create a mosaic file at output_path composed of all images in raster_list
    """

    # explicit overwrite behavior
    Path(output_path).unlink(missing_ok=True)

    # spatial resolution
    spx = float(merge_options.spatial_resolution[0])
    spy = -abs(float(merge_options.spatial_resolution[1]))

    # creation options parameter only when non empty
    if creation_options := merge_options.creation_options:
        gdal_co = " -co " + " -co ".join(
            [
                f"{co_name}={co_value}"
                for co_name, co_value in list(creation_options.items())
            ]
        )
    else:
        gdal_co = ""

    # use no_data option only if output_no_data is set
    if merge_options.output_no_data is None:
        a_nodata = " "
    else:
        a_nodata = f" -dstnodata {merge_options.output_no_data} "

    # build command
    if (merge_options.available_ram is not None) and merge_options.available_ram > 2:
        cache_ram = "30%"
    else:
        cache_ram = "500"
    cmd = (
        f"gdalwarp {gdal_co} --config GDAL_CACHEMAX {cache_ram} "
        f"-srcnodata {input_no_data} -tr {spx} {spy}"
        f"{a_nodata} -wo OPTIMIZE_SIZE=YES -wm 500 "
        f"-ot {merge_options.output_pixel_type} -cvmd '' "
        f"{' '.join(raster_list)} {output_path}"
    )

    run(cmd, desc="gdalwarp")
    report_band_description(raster_list[0], output_path)


def get_date_from_string(vardate: str) -> tuple[int, int, int]:
    """
    get date from string date
    Parameters
    ----------
    vardate:
        input date ex : 20160101
    """
    year = int(vardate[0:4])
    month = int(vardate[4:6])
    day = int(vardate[6 : len(vardate)])
    return year, month, day


def get_dates_in_tile(date_in_file: PathLike, display: bool = True) -> list[str]:
    """
    Get available dates in file

    Parameters
    ----------
    date_in_file:
        path to txt containing one date per line
    display:
        flag to print messages
    raw_dates:
        flag used to return all available dates
    """
    all_dates = []
    with open(date_in_file, encoding="utf-8") as ffile:
        for line in ffile:
            vardate = line.rstrip()
            try:
                year, month, day = get_date_from_string(vardate)
                valid_date = datetime.datetime(int(year), int(month), int(day))
                all_dates.append(vardate)
                if display:
                    print(valid_date)
            except ValueError as v_err:
                raise ValueError(
                    f"invalid date in : {date_in_file} -> '{vardate}'"
                ) from v_err
    return all_dates


def get_raster_projection_epsg(file_name: PathLike) -> str:
    """
    Get raster EPSG projection code

    Parameters
    ----------
    file_name:
        Path to raster file
    """
    source_ds = gdal.Open(file_name, gdal.GA_ReadOnly)
    projection = osr.SpatialReference()
    projection.ImportFromWkt(source_ds.GetProjectionRef())
    projection_code: str = projection.GetAttrValue("AUTHORITY", 1)
    return projection_code


def get_raster_n_bands(raster: InputRaster) -> int:
    """
    usage get raster's number of bands
    """
    src_ds = open_raster(raster)
    return int(src_ds.RasterCount)


def get_image_pixel_type(raster_in: InputRaster) -> str:
    """
    Get raster pixel type of raster_in from GetDataTypeName()

    Parameters
    ----------
    raster_in:
        gdal raster datatse or  osgeo.gdal.Dataset object
    """
    raster = open_raster(raster_in)
    band = raster.GetRasterBand(1)
    data_type: str = gdal.GetDataTypeName(band.DataType)
    return data_type


def get_raster_extent(raster_in: InputRaster) -> list[float]:
    """
    Get raster extent of raster_in from GetGeoTransform()

    Parameters
    ----------
    raster_in:
        input raster

    Notes
    -----
    Return the extent with [minX,maxX,minY,maxY]
    """
    raster = open_raster(raster_in)
    geotransform = raster.GetGeoTransform()
    origin_x = geotransform[0]
    origin_y = geotransform[3]
    spacing_x = geotransform[1]
    spacing_y = geotransform[5]
    rows, cols = raster.RasterYSize, raster.RasterXSize

    min_x = origin_x
    max_y = origin_y
    max_x = min_x + cols * spacing_x
    min_y = max_y + rows * spacing_y

    return [min_x, max_x, min_y, max_y]


def pix_to_geo(raster: PathLike, col: int, row: int) -> tuple[float, float]:
    """
    return geographical coordinates of col, row position
    """
    dsrast = gdal.Open(raster)
    originx, xres, rot1, originy, rot2, yres = dsrast.GetGeoTransform()
    xcoord = xres * col + rot1 * row + originx
    ycoord = rot2 * col + yres * row + originy
    return xcoord, ycoord


def gen_confusion_matrix(
    flat_matrix: SortedLabelsCoords, all_classes: list[int]
) -> ArrayLike:
    """
    Generate a numpy array representing a confusion matrix

    Parameters
    ----------
    flat_matrix:
        list of label's values and coordinates, coming from conf_coordinates_csv() function and
        sorted by sort_by_first_element()
    all_classes:
        list of all classes (ints)
    """
    n_classes = len(all_classes)
    output_matrix = np.zeros([n_classes, n_classes])
    for class_ref, class_ref_data in flat_matrix:
        for class_prod, score in class_ref_data:
            if class_ref in all_classes and class_prod in all_classes:
                row_index = all_classes.index(class_ref)
                col_index = all_classes.index(class_prod)
                output_matrix[row_index][col_index] += score
    return output_matrix


def parse_csv(csv_file: PathLike) -> LabelsCoords:
    """
    Parse a CSV file (used in `conf_coordinates_csv`) and return the expected list of tuples

    Parameters
    ----------
    csv_file:
        path to the CSV file

    Example
    -------
    If the file contains the following:
        ex : file1.csv
        #Reference labels (rows):11
        #Produced labels (columns):11,12
        14258,52

    list_labels_tuples = [(11, (11, 14258.0)), (11, (12, 52.0))]
    """
    with open(csv_file, encoding="utf-8") as data_file:
        lines = data_file.readlines()
        ref = lines[0].rstrip("\n\r").split(":")[-1].split(",")
        prod = lines[1].rstrip("\n\r").split(":")[-1].split(",")

        data_lines = lines[2:]
        list_labels_tuples = []
        for n_row, ref_label in enumerate(ref):
            for n_col, prod_label in enumerate(prod):
                score = float(data_lines[n_row].rstrip("\n\r").split(",")[n_col])
                list_labels_tuples.append((int(ref_label), (int(prod_label), score)))
    return list_labels_tuples


def conf_coordinates_csv(csv_paths: ListPathLike) -> LabelsCoords:
    """
    Return a list of tuple containing labels coordinates and values parsed from csv files

    Parameters
    ----------
    csv_paths:
        list of path to csv files (ex: ["/path/to/file1.csv","/path/to/file2.csv"])

    ex : file1.csv
        #Reference labels (rows):11
        #Produced labels (columns):11,12
        14258,52

         file2.csv
        #Reference labels (rows):12
        #Produced labels (columns):11,12
        38,9372

    out = [(11, (11, 14258.0)), (11, (12, 52.0)), (12, (11, 38.0)), (12, (12, 9372.0))]
    """
    all_scores = []
    for csv_file in csv_paths:
        all_scores.extend(parse_csv(csv_file))
    return all_scores


def get_list_tile_from_model(model_in: str, path_to_config: str) -> list[str] | None:
    """
    Export a list of tiles uses to built "modelIN"

    Parameters
    ----------
    model_in:
        model name (generally an integer)
    path_to_config :
        path to the configuration file which links a model and all tiles used to build it

    Example
    -------
    $cat /path/to/myConfigFile.cfg
    AllModel:
    [
        {
        modelName:'1'
        tilesList:'D0005H0001 D0005H0002'
        }
        {
        modelName:'22'
        tilesList:'D0004H0004 D0005H0008'
        }
    ]
    tiles = get_list_tile_from_model('22',/path/to/myConfigFile.cfg)
    print tiles
    >>tiles = ['D0004H0004','D0005H0008']
    """
    with open(path_to_config, encoding="utf-8") as conf_file:
        cfg = Config(conf_file)
        all_model = cfg.AllModel
        for model in all_model.data:
            if model.modelName == model_in:
                list_tiles: list[str] = model.tiles_list.split("_")
                return list_tiles
    return None


@RemoveInStringList(".aux.xml", ".sqlite-journal", ".pyc", "~")
def file_search_reg_ex(path_file: str) -> list[str]:
    """
    get files by regEx
    """
    return list(glob.glob(path_file.replace("[", "[[]")))


def get_feat_stack_name(
    list_indices: list[str], user_feat_pattern: str | None = None
) -> str:
    """
    usage : get Feature Stack name
    Parameters
    ----------
    list_indices: list(string)
        the features list
    user_feat_pattern: string
        contains features name separated by comma
    Return
    ------
    string: the image stack name
    """
    if user_feat_pattern is None:
        user_feat_pattern = ""
    else:
        user_feat_pattern = "_".join(user_feat_pattern.split(","))

    stack_ind = f"SL_MultiTempGapF{user_feat_pattern}.tif"
    return_list_feat = True
    list_feat = ""
    if len(list_indices) > 1:
        list_indices = list(list_indices)
        list_indices = sorted(list_indices)
        list_feat = "_".join(list_indices)
    elif len(list_indices) == 1:
        list_feat = list_indices[0]
    else:
        return_list_feat = False

    if return_list_feat and list_feat:
        stack_ind = f"SL_MultiTempGapF_{list_feat}_{user_feat_pattern}_.tif"
    return stack_ind


def write_cmds(path: PathLike, cmds: list[str], mode: str = "w") -> None:
    """
    Write commands in a text file
    Parameters
    ----------
    path:
        Path to the file the commands will be written to
    cmds:
        List of the commands to write
    mode:
        Open mode of the file ('w' by default)
    """
    with open(path, mode, encoding="utf-8") as cmd_file:
        for cmd_num, cmd in enumerate(cmds):
            if cmd_num == 0:
                cmd_file.write(cmd)
            else:
                cmd_file.write(f"\n{cmd}")


@RemoveInStringList(".aux.xml", ".sqlite-journal", ".pyc", "~")
def file_search_and(path_to_folder: str, all_path: bool, *names: str) -> list[str]:
    """
    search all files in a folder or sub folder which contains all names in their name

    IN :
        - path_to_folder : target folder
                ex : /xx/xxx/xx/xxx
        - all_path : flag to toggle return value to provide folder path
        - *names : target names
                ex : "target1","target2"
    OUT :
        - out : a list containing all file name (without extension) which are containing all name
    """
    return file_search(path_to_folder, all_path, all, *names)


@RemoveInStringList(".aux.xml", ".sqlite-journal", ".pyc", "~")
def file_search_or(path_to_folder: str, all_path: bool, *names: str) -> list[str]:
    """
    search all files in a folder or sub folder which contains any of the names in their name

    IN :
        - path_to_folder : target folder
                ex : /xx/xxx/xx/xxx
        - all_path : flag to toggle return value to provide folder path
        - *names : target names
                ex : "target1","target2"
    OUT :
        - out : a list containing all file name (without extension) which have any of the names
    """
    return file_search(path_to_folder, all_path, any, *names)


def file_search(
    path_to_folder: str, all_path: bool, condition: Callable, *names: str
) -> list[str]:
    """
    Utility function used by `file_search_or` and `file_search_and`

    Parameters
    ----------
    path_to_folder:
        Target folder
    all_path:
        flag to toggle return value to provide folder path
    condition:
        Any or all
    names:
        Target names
    """
    out = []
    for file in walk(path_to_folder):
        if condition(name in file.name for name in names):
            out.append(str(file.parent / file) if all_path else file.stem)

    return out


def open_raster(raster_in: InputRaster) -> osgeo.gdal.Dataset:
    """
    Open the given raster
    """
    if isinstance(raster_in, str):
        if not Path(raster_in).is_file():
            raise FileExistsError(f"File {raster_in} doesn't exist")
        raster = gdal.Open(raster_in, gdal.GA_ReadOnly)
    elif isinstance(raster_in, osgeo.gdal.Dataset):
        raster = raster_in
    else:
        raise TypeError(
            f"Raster type should be str or str, osgeo.gdal.Dataset not {type(raster_in)}"
        )
    return raster


def delete_all_files(files_list: ListPathLike) -> None:
    """
    Delete all files in the given list of files

    Parameters
    ----------
    files_list:
        List of paths

    Returns
    -------
    None
    """
    for file_path in files_list:
        Path(file_path).unlink(missing_ok=True)
