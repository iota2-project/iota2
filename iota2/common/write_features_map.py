"""
Functions used to write features maps
"""

import logging
from logging import Logger
from pathlib import Path
from typing import Any

import rasterio as rio
from osgeo import gdal

import iota2.common.file_utils as fu
import iota2.common.raster_utils as ru
from iota2.common import generate_features as gen_features
from iota2.common.compression_options import compression_options
from iota2.common.custom_numpy_features import (
    ChunkParameters,
    CustomFeaturesConfiguration,
    CustomNumpyFeatures,
    MaskingRaster,
    compute_custom_features,
    exogenous_data_tile,
)
from iota2.common.generate_features import FeaturesMapParameters
from iota2.learning.utils import I2Label
from iota2.typings.i2_types import PathLike, SpatialResolution

SensorsParamsType = dict[str, str | list[str] | int]

FunctionNameWithParams = tuple[str, dict[str, Any]]

LOGGER = logging.getLogger("distributed.worker")


def add_label_on_raster_band(
    raster: str, list_bands: list[int], feat_labels: list[I2Label]
) -> None:
    """
    Add labels as description to bands of a raster

    Parameters
    ----------
    raster
        Name of the raster file
    list_bands
        List of bands to label
    feat_labels
        List of labels to add as descriptions of the bands of the raster.
    """
    ds = gdal.Open(raster, gdal.GA_Update)
    for band, label in zip(list_bands, feat_labels):
        raster_band = ds.GetRasterBand(band)
        raster_band.SetDescription(str(label))
    del ds


def write_features_by_chunk(
    features_map_parameters: FeaturesMapParameters,
    chunk_config: ru.ChunkConfig,
    module_path: str,
    targeted_chunk: int,
    list_functions: list[FunctionNameWithParams],
    concat_mode: bool = False,
    multi_param_cust: dict[str, Any] | None = None,
    logger: Logger = LOGGER,
) -> None:
    """
    Compute from a stack of data -> gapFilling -> features computation -> sampleExtractions
    using OTB applications

    Parameters
    ----------
    features_map_parameters:
        Parameters used to compute features maps
    chunk_config:
        kind of "dataclass" with chunk size / padding
    module_path:
        optional custom module path, if not set, searches iota2 'external_code.py'
    list_functions:
        list of function names with their keyword arguments as a dict
        as outputted by the dcp.convert_functions
    targeted_chunk:
        number of the current chunk to compute
    concat_mode:
        (de)activate concatenation with input data
    multi_param_cust:
        # DOCME
    logger:
        logger
    """
    if not features_map_parameters.working_directory:
        working_directory = str(features_map_parameters.output_path)
    else:
        working_directory = str(features_map_parameters)
    working_directory_features = str(
        Path(working_directory) / features_map_parameters.tile
    )

    try:
        Path(working_directory_features).mkdir(exist_ok=True, parents=True)
    except OSError:
        logger.warning(f"{working_directory_features} already exists")

    (otb_pipelines, labs_dict) = gen_features.generate_features(
        features_map_parameters, logger=logger
    )
    opath = str(Path(features_map_parameters.output_path) / "customF")
    Path(opath).mkdir(exist_ok=True, parents=True)
    output_name = str(
        Path(opath) / f"{features_map_parameters.tile}_chunk_{targeted_chunk}.tif"
    )
    assert isinstance(multi_param_cust, dict)
    exogeneous_data = exogenous_data_tile(
        multi_param_cust["exogeneous_data"], features_map_parameters.tile
    )
    cust = CustomNumpyFeatures(
        sensors_params=features_map_parameters.sensors_parameters,
        module_name=module_path,
        list_functions=list_functions,
        configuration=CustomFeaturesConfiguration(
            concat_mode=concat_mode,
            enabled_raw=multi_param_cust["enable_raw"],
            enabled_gap=multi_param_cust["enable_gap"],
            fill_missing_dates=multi_param_cust["fill_missing_dates"],
        ),
        all_dates_dict=multi_param_cust["all_dates_dict"],
        exogeneous_data_file=exogeneous_data,
    )
    otb_pipelines = ru.turn_off_otb_pipelines(
        otb_pipelines,
        turn_off_raw=multi_param_cust["enable_raw"] is False,
        turn_off_interpolated=multi_param_cust["enable_gap"] is False,
    )
    data, _ = compute_custom_features(
        functions_builder=cust,
        otb_pipelines=otb_pipelines,
        feat_labels=labs_dict.interp,
        chunk_params=ChunkParameters(chunk_config, targeted_chunk),
        masking_raster=MaskingRaster(
            multi_param_cust["mask_valid_data"], multi_param_cust["mask_value"]
        ),
        exogeneous_data=exogeneous_data,
        concatenate_features=concat_mode,
        output_name=output_name,
    )
    feat_labels = data.labels

    add_label_on_raster_band(
        output_name, list(range(1, len(feat_labels) + 1)), feat_labels
    )


def merge_features_by_tiles(
    iota2_directory: PathLike,
    tile: str,
    res: SpatialResolution,
    compress: dict | None = None,
) -> None:
    """merge all chunks of a tile"""
    list_tile = fu.file_search_and(
        str(Path(iota2_directory) / "customF"), True, tile, ".tif"
    )
    features_map = str(Path(iota2_directory) / "by_tiles" / f"{tile}.tif")
    raster = rio.open(list_tile[0])
    output_type = raster.meta["dtype"]
    if compress is None:
        compress_options = {
            "COMPRESS": compression_options.algorithm,
            "PREDICTOR": compression_options.predictor,
            "BIGTIFF": "YES",
        }
    else:
        compress_options = compress
    merge_options = fu.TileMergeOptions(
        spatial_resolution=res,
        output_pixel_type=output_type,
        creation_options=compress_options,
    )
    fu.assemble_tile_merge(list_tile, features_map, merge_options)


def merge_features_maps(
    iota2_directory: PathLike,
    output_name: str | None = None,
    res: SpatialResolution | None = None,
    compress: dict | None = None,
    no_data_value: int = -10000,
) -> None:
    """
    Merge tiles to a final mosaic

    Parameters
    ----------
    iota2_directory:
        Path to the output directory
    output_name:
        Path to the output map
    res:
        Spatial resolution
    compress:
        Compression options
    no_data_value:
        Value to use for nodata
    """
    if res is None:
        res = (10.0, 10.0)
    list_tile = fu.file_search_and(
        str(Path(iota2_directory) / "by_tiles"), True, ".tif"
    )
    if output_name is None:
        feature_map = str(Path(iota2_directory) / "final" / "features_map.tif")
    else:
        feature_map = str(Path(iota2_directory) / "final" / output_name)
    if compress is None:
        compress_options = {
            "COMPRESS": compression_options.algorithm,
            "PREDICTOR": compression_options.predictor,
            "BIGTIFF": "YES",
        }
    else:
        compress_options = compress

    # get output type
    raster = rio.open(list_tile[0])
    output_type = raster.meta["dtype"]

    merge_options = fu.TileMergeOptions(
        spatial_resolution=res,
        output_pixel_type=output_type,
        creation_options=compress_options,
        output_no_data=no_data_value,
    )
    fu.assemble_tile_merge(list_tile, feature_map, merge_options, no_data_value)
