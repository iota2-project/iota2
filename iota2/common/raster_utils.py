#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provides some functions to manipulate rasters."""
import logging
import math
from dataclasses import dataclass
from functools import partial
from pathlib import Path
from typing import Literal

import geopandas as gpd
import numpy as np
import pandas as pd
import pyproj
import rasterio
import torch
import torch.nn.functional as F
from osgeo import gdal, gdalconst, osr
from pyproj import Transformer
from rasterio import mask, warp
from rasterio.coords import BoundingBox
from rasterio.io import MemoryFile
from rasterio.merge import merge
from rasterio.transform import Affine
from shapely.geometry import box
from shapely.ops import transform
from sklearn.preprocessing import LabelEncoder, binarize

import iota2.common.file_utils as fut
import iota2.common.otb_app_bank as otb_app
from iota2.common.utils import run
from iota2.typings.i2_types import (
    AvailCompressions,
    GdalGeoTranform,
    GdalType,
    I2LabelAlias,
    InputRaster,
    OtbApp,
    OtbDep,
    OtbImage,
    OtbLikeBoundingBox,
    PaddingBox,
    PathLike,
    RasterioSpatialMetadata,
    SpatialMetadata,
)

LOGGER = logging.getLogger("distributed.worker")


@dataclass
class MaskData:
    """Data class dedicated to contains masking information

    Parameters
    ----------
    mask_arr
        mask as 2D numpy array
    mask_value
        every pixel under 'mask_value' will be ignored
    """

    mask_arr: np.ndarray
    mask_value: int = 0

    def __post_init__(self) -> None:
        if len(self.mask_arr.shape) > 2:
            raise ValueError("MaskData.mask_arr must be a 2D array (row, col)")


@dataclass
class SpatialBounds:
    """Dataclass containing spatial bounds information padding and stream box."""

    padding: PaddingBox | None = None
    stream_bbox: OtbLikeBoundingBox | None = None


@dataclass
class ImageSize:
    """Image size in x and y dims."""

    x_size: int
    y_size: int


@dataclass
class ImageOrigin:
    """Spatial image origin."""

    x_origin: float
    y_origin: float


@dataclass
class PixelSize:
    """Spatial resolution."""

    pixel_width: float
    pixel_height: float


@dataclass
class PixelRotation:
    """Pixels rotation in x and y dims."""

    x_rotation: float
    y_rotation: float


@dataclass
class RasterMetadata:
    """
    Dataclass for raster metadata
    """

    img_size: ImageSize | None = None
    projection: osr.SpatialReference | None = None
    img_origin: ImageOrigin | None = None
    pix_size: PixelSize | None = None
    pix_rot: PixelRotation | None = None

    def __eq__(self, other: object) -> bool:
        """
        Overload the `==` method to compare two RasterMetadata objects

        Parameters
        ----------
        other:
            Other metadata object to compare

        Returns
        -------
        True if the tw objects are equivalent, False otherwise
        """
        if not isinstance(other, RasterMetadata):
            return NotImplemented

        return (
            self.img_size == other.img_size
            and self.img_origin == other.img_origin
            and self.pix_size == other.pix_size
            and self.pix_rot == other.pix_rot
            and compare_projections(self.projection, other.projection)
        )


def compare_projections(
    proj1: osr.SpatialReference | None, proj2: osr.SpatialReference | None
) -> bool:
    """
    Check if OSR projections are the same
    """
    if proj1 is None or proj2 is None:
        return proj1 == proj2
    return bool(proj1.IsSame(proj2))


@dataclass
class PipelinesData:
    """Dataclass to carry most of otb pipeline data."""

    interpolated: np.ndarray | None = None
    raw: np.ndarray | None = None
    exogenous: np.ndarray | None = None
    binary_mask: np.ndarray | None = None


def extract_raster_metadata_from_otb(otb_application: OtbApp) -> RasterMetadata:
    """
    Extracts raster metadata from an Orfeo Toolbox (OTB) application.

    Parameters
    ----------
    otb_application:
        An instance of the Orfeo Toolbox application.

    Notes
    -----
    returned type: RasterMetadata
                        A dataclass containing metadata information extracted from
                        the OTB application, including image size, projection,
                        mage origin, and pixel size.
    """
    otb_application.Execute()
    param_name = otb_app.get_input_parameter_output(otb_application)
    x_size, y_size = otb_application.GetImageSize(param_name)
    proj = otb_application.GetImageProjection(param_name)
    x_origin, y_origin = otb_application.GetImageOrigin(param_name)
    pix_width, pix_height = otb_application.GetImageSpacing(param_name)
    projection = osr.SpatialReference()
    projection.ImportFromWkt(proj)
    return RasterMetadata(
        ImageSize(x_size, y_size),
        projection,
        ImageOrigin(x_origin, y_origin),
        PixelSize(pix_width, pix_height),
    )


@dataclass
class OtbPipelineCarrier:
    """Dataclass to carry most of otb pipeline."""

    interpolated_pipeline: OtbApp | None = None
    raw_pipeline: OtbApp | None = None
    binary_mask_pipeline: OtbApp | None = None
    exogenous_data_pipeline: OtbApp | None = None
    spatial_mask: OtbApp | None = None
    pipeline_dependencies: OtbDep | None = None

    spatial_bounds: SpatialBounds | None = None

    def get_nonempty_pipeline(self) -> OtbApp | None:
        """
        Get the first non-empty pipeline from the available pipelines.

        This method checks each available pipeline attribute in the following order:
        interpolated_pipeline, raw_pipeline, binary_mask_pipeline,
        exogenous_data_pipeline, spatial_mask. It returns the first non-empty pipeline
        found, or None if all pipelines are empty.

        Returns
        -------
        OtbApp | None
            The first non-empty pipeline found, or None if all pipelines are empty.
        """
        pipelines = [
            self.interpolated_pipeline,
            self.raw_pipeline,
            self.binary_mask_pipeline,
            self.exogenous_data_pipeline,
            self.spatial_mask,
        ]
        return next((pipeline for pipeline in pipelines if pipeline is not None), None)

    def __post_init__(self) -> None:
        if self.interpolated_pipeline:
            self.interpolated_pipeline.Execute()
        if self.raw_pipeline:
            self.raw_pipeline.Execute()
        if self.binary_mask_pipeline:
            self.binary_mask_pipeline.Execute()
        if self.exogenous_data_pipeline:
            self.exogenous_data_pipeline.Execute()
        if self.spatial_mask:
            self.spatial_mask.Execute()


def turn_off_otb_pipelines(
    otb_pipelines: OtbPipelineCarrier, turn_off_raw: bool, turn_off_interpolated: bool
) -> OtbPipelineCarrier:
    """
    Turn off specific OTB pipelines based on the specified parameters.

    Parameters
    ----------
    otb_pipelines : OtbPipelineCarrier
        The OTB pipelines object to modify.
    turn_off_raw : bool
        Whether to turn off the raw pipeline or not.
    turn_off_interpolated : bool
        Whether to turn off the interpolated pipeline or not.

    Returns
    -------
    OtbPipelineCarrier
        The modified OTB pipelines object.

    Notes
    -----
    This function modifies the input OTB pipelines object based on the provided parameters.
    If `turn_off_raw` is True, the raw pipeline will be turned off by setting it to None.
    If `turn_off_interpolated` is True, the interpolated pipeline will be turned off
    by setting it to None.

    Examples
    --------
    >>> otb_pipelines = OtbPipelineCarrier(...)
    >>> otb_pipelines = turn_off_otb_pipelines(otb_pipelines, True, False)

    """
    if turn_off_raw:
        otb_pipelines = OtbPipelineCarrier(
            interpolated_pipeline=otb_pipelines.interpolated_pipeline,
            raw_pipeline=None,
            binary_mask_pipeline=otb_pipelines.binary_mask_pipeline,
            exogenous_data_pipeline=otb_pipelines.exogenous_data_pipeline,
            spatial_mask=otb_pipelines.spatial_mask,
            pipeline_dependencies=otb_pipelines.pipeline_dependencies,
            spatial_bounds=otb_pipelines.spatial_bounds,
        )
    if turn_off_interpolated:
        otb_pipelines = OtbPipelineCarrier(
            interpolated_pipeline=None,
            raw_pipeline=otb_pipelines.raw_pipeline,
            binary_mask_pipeline=otb_pipelines.binary_mask_pipeline,
            exogenous_data_pipeline=otb_pipelines.exogenous_data_pipeline,
            spatial_mask=otb_pipelines.spatial_mask,
            pipeline_dependencies=otb_pipelines.pipeline_dependencies,
            spatial_bounds=otb_pipelines.spatial_bounds,
        )
    return otb_pipelines


def write_array(
    array: np.ndarray, spatial_meta: SpatialMetadata, output_file: PathLike
) -> None:
    """Save array as raster file (overwrite if exist).

    array must be 3D [bands, rows, cols]

    Parameters
    ----------
    array
        data to save
    spatial_meta
        spatial information
    output_file
        output file
    """
    if len(array.shape) != 3:
        raise ValueError("input array must be 3D shape as (bands, rows, cols).")

    epsg_code = spatial_meta.projection.GetAttrValue("AUTHORITY", 1)
    trans = gdal_transform_to_rio(spatial_meta.gdal_geo_transform)
    with rasterio.open(
        output_file,
        "w",
        driver="GTiff",
        height=array.shape[1],
        width=array.shape[2],
        count=array.shape[0],
        crs=f"EPSG:{epsg_code}",
        transform=trans,
        dtype=array.dtype,
    ) as dest:
        dest.write(array)


def gdal_transform_to_rio(gdal_transform: GdalGeoTranform) -> Affine:
    """formatter gdal to rasterio transform

    Parameters
    ----------
    gdal_transform
        transform from gdal
    """
    return Affine(
        gdal_transform[1],
        gdal_transform[2],
        gdal_transform[0],
        gdal_transform[4],
        gdal_transform[5],
        gdal_transform[3],
    )


@dataclass
class UserDefinedFunctionPayload:
    """Represent data returned by the udf.

    labels: list[I2LabelT]
    spatial_meta: SpatialMetadata
    data: np.ndarray | None
    mask: np.ndarray | None
    otb_img : OtbImage | None
    pipeline_dependencies: list[OtbDep] | None
    """

    labels: list[I2LabelAlias]
    spatial_meta: SpatialMetadata
    data: np.ndarray | None = None
    mask: np.ndarray | None = None
    otb_img: OtbImage | None = None
    pipeline_dependencies: OtbDep | None = None


def traduct_array(in_array: np.ndarray, enc: LabelEncoder) -> np.ndarray:
    """Translate a 2D array based on a LabelEncoder.

    This function takes a 2D numpy array and a LabelEncoder and translates
    the values in the array according to the encoding provided by the LabelEncoder.
    It returns the translated array.

    Parameters:
    -----------
    in_array:
        The input 2D numpy array to be translated.
    enc:
        The LabelEncoder used for translation.

    Notes:
    ------
    returned value, np.ndarray:
                        The translated 2D numpy array.
    """
    dic_converter = dict(enumerate(enc.classes_))
    traducted_array: np.ndarray = np.copy(in_array)
    for torch_label, i2_label in dic_converter.items():
        traducted_array[in_array == torch_label] = i2_label
    return traducted_array


def str_to_gdal_pix_type(type_as_str: str) -> GdalType:
    """Convert a string type to a GDAL data type.

    This function takes a string representing a data type and converts it
    to the corresponding GDAL data type.

    Parameters:
    -----------
    type_as_str:
        The string representation of the data type.

    Notes:
    ------
    return GdalType, The corresponding GDAL data type.

    Raises:
    -------
    KeyError:
        If the input string does not match any known data type.
    """
    converter: dict[str, int] = {
        "uint8": gdalconst.GDT_Byte,
        "uint16": gdalconst.GDT_UInt16,
        "int16": gdalconst.GDT_Int16,
        "uint32": gdalconst.GDT_UInt32,
        "int32": gdalconst.GDT_Int32,
        "float": gdalconst.GDT_Float32,
        "double": gdalconst.GDT_Float64,
    }
    return converter[type_as_str]


def reproj_bbox(bounds: BoundingBox, in_crs: str, out_crs: str | int) -> BoundingBox:
    """
    Reproject a bounding box.

    Parameters
    ----------
    bounds:
        Bounding box to reproject
    in_crs:
        Projection of input bounding box
    out_crs:
        Projection of output bounding box
    """
    ul_from = (bounds.left, bounds.top)
    ur_from = (bounds.right, bounds.top)
    ll_from = (bounds.left, bounds.bottom)
    lr_from = (bounds.right, bounds.bottom)
    x_from = [p[0] for p in [ul_from, ur_from, ll_from, lr_from]]
    y_from = [p[1] for p in [ul_from, ur_from, ll_from, lr_from]]
    transformer = Transformer.from_crs(in_crs, out_crs, always_xy=True)
    x_to, y_to = transformer.transform(x_from, y_from)  # pylint: disable=E0633
    return BoundingBox(np.min(x_to), np.min(y_to), np.max(x_to), np.max(y_to))


def rasters_footprints(rasters_list: list[Path], target_epsg: int) -> gpd.GeoDataFrame:
    """Generate footprints of rasters as a GeoDataFrame.

    Parameters:
    -----------
    rasters_list:
        A list of file paths to the raster files.
    target_epsg:
        The EPSG code of the target coordinate reference system (CRS).

    Notes:
    ------
    return gpd.GeoDataFrame, containing footprints of the rasters.

    The returned GeoDataFrame will contain two columns:
    - 'path': containing the file path of the raster.
    - 'geometry': containing the footprint geometry as polygons.

    The function reads each raster file to extract its bounding box and then
    transforms it to the target CRS if necessary. The resulting footprints
    are stored in the GeoDataFrame along with their corresponding file paths.
    """
    geometries = []
    paths = []
    for raster_file in rasters_list:
        with rasterio.open(raster_file) as raster:
            epsg = raster.read_crs().to_epsg()
            raster_bounds = raster.bounds
            geom = box(*raster_bounds)
            project = pyproj.Transformer.from_proj(
                pyproj.Proj(init=f"epsg:{epsg}"),
                pyproj.Proj(init=f"epsg:{target_epsg}"),
            )
            if epsg != target_epsg:
                geom = transform(project.transform, geom)
            geometries.append(geom)
            paths.append(str(raster_file))
    df_grid = pd.DataFrame({"path": paths, "geometry": geometries})
    gdf_grid = gpd.GeoDataFrame(df_grid, geometry="geometry", crs=f"EPSG:{target_epsg}")
    return gdf_grid


def img_as_patches(in_img: torch.Tensor | np.ndarray, radius: int) -> torch.Tensor:
    """
    Convert an image into patches regarding a given radius.

    Parameters:
    -----------
    in_img
        The input image data. It should be shaped as (nb_bands, size_x, size_y).
    radius
        The radius used to create patches. The kernel size will be 2 * radius + 1.

    Notes:
    ------
    return type torch.Tensor
        A tensor containing image patches shaped as (1, nb_bands, size_x, size_y, kernel, kernel),
        where kernel = 2 * radius + 1.

    This function converts the input image into patches with a specified radius.
    It pads the image, unfolds it into patches, and then rearranges the dimensions
    to match the output tensor shape. If the input is a NumPy array, it will be converted
    to a PyTorch tensor before processing.
    """
    kernel = 2 * radius + 1
    stride = 1
    pad = (radius, radius, radius, radius)
    if isinstance(in_img, torch.Tensor):
        img = in_img.unsqueeze(0)
        img = F.pad(input=img, pad=pad, mode="constant", value=0)
        img = (
            img.unfold(3, kernel, stride)
            .unfold(2, kernel, stride)
            .permute(0, 1, 2, 3, 5, 4)
        )

    elif isinstance(in_img, np.ndarray):
        img = torch.from_numpy(in_img).unsqueeze(0)  # pylint: disable=E1101
        img = F.pad(input=img, pad=pad, mode="constant", value=0)
        img = np.array(
            img.unfold(3, kernel, stride)
            .unfold(2, kernel, stride)
            .permute(0, 1, 2, 3, 5, 4)
        )
    else:
        raise ValueError(f"type : {type(in_img)} not supported")
    return img


def reorder_proba_map(
    proba_map_path_in: str,
    proba_map_path_out: str,
    class_model: list[int],
    all_class: list[int],
    pix_type: str,
) -> None:
    """Reorder the probability map.

    in order to merge proability raster containing a different number of
    effective class it is needed to reorder them according to a reference

    Parameters
    ----------

    proba_map_path_in :
        input probability map
    proba_map_path_out :
        output probability map
    class_model :
        list containing labels in the model used to classify
    all_class :
        list containing all possible labels
    pix_type
        output pixel type
    """

    class_model_copy = class_model[:]

    if len(class_model) != len(all_class):
        in_nb_bands = fut.get_raster_n_bands(proba_map_path_in)
        nodata_label_idx = len(all_class)
        in_reorder = proba_map_path_in
        if in_nb_bands != len(all_class):
            proba_new_band, _ = otb_app.create_application(
                otb_app.AvailableOTBApp.BAND_MATH_X,
                {
                    "il": proba_map_path_in,
                    "ram": "1000",
                    "pixType": pix_type,
                    "exp": "im1; 0 * im1b1",
                    "out": proba_map_path_out,
                },
            )
            proba_new_band.ExecuteAndWriteOutput()
            # last band contains only 0 values now
            nodata_label_idx = len(class_model) + 1
            in_reorder = proba_map_path_out

        for index_label, expected_label in enumerate(all_class):
            if expected_label not in class_model:
                class_model_copy.insert(index_label, -1)
        index_vector = [
            class_model.index(x) + 1 if x in class_model else nodata_label_idx
            for x in class_model_copy
        ]
        reorder_app, _ = otb_app.create_application(
            otb_app.AvailableOTBApp.BAND_MATH_X,
            {
                "il": in_reorder,
                "ram": "1000",
                "exp": f"bands(im1, {'{' + ','.join(map(str, index_vector)) + '}'})",
                "out": proba_map_path_out,
                "pixType": pix_type,
            },
        )
        reorder_app.ExecuteAndWriteOutput()


def roi_raster(
    in_raster: str, out_raster: str, roi_coord: list[tuple[float, float]]
) -> None:
    """use rasterio to extract raster region of interest

    The ROI does not have to be square

    Parameters
    ----------

    in_raster:
        raster to crop
    out_raster:
        raster cropped
    roi_coord:
        list of points which define a polygon [[x1, y1], ..., [xn, yn]]
    """
    src = rasterio.open(in_raster)
    roi_polygon_src_coords = warp.transform_geom(
        {"init": str(src.crs).lower()},
        src.crs,
        {"type": "Polygon", "coordinates": [roi_coord]},
    )
    out_image, out_transform = mask.mask(src, [roi_polygon_src_coords], crop=True)
    with rasterio.open(
        out_raster,
        "w",
        driver="GTiff",
        height=out_image.shape[1],
        width=out_image.shape[2],
        count=out_image.shape[0],
        crs=src.crs,
        dtype=out_image.dtype,
        transform=out_transform,
    ) as dst:
        dst.write(out_image)


def re_encode_raster(
    in_raster: str,
    out_raster: str,
    conversion_table: dict,
    no_data_values: int = 0,
    logger: logging.Logger = LOGGER,
) -> tuple[bool, str]:
    """According to a conversion table, re_encode the monoband raster values.

    in_raster
        input monoband raster
    out_raster
        output raster
    conversion_table
        conversion_table[old_pixel_value] = new_pixel_value

    Notes
    -----
    return True if success.
    """
    conv_list = []
    new_labels = []
    all_castable = []
    for old_label, new_label in conversion_table.items():
        conv_list.append(f"im1b1=={new_label}?{old_label}")
        try:
            casted = int(old_label)
            new_labels.append(casted)
            all_castable.append(True)
        except ValueError:
            new_labels.append(int(new_label))
            all_castable.append(False)

    out_format = "uint8" if max(new_labels) < 255 else "uint32"
    join_conv_list = ":".join(conv_list)
    join_conv_list = f"{join_conv_list}:{no_data_values}"
    re_encoder, _ = otb_app.create_application(
        otb_app.AvailableOTBApp.BAND_MATH,
        {
            "il": [in_raster],
            "exp": join_conv_list,
            "out": out_raster,
            "ram": "1024",
            "pixType": "uint8" if max(new_labels) < 255 else "uint32",
        },
    )

    if all(all_castable):
        re_encoder.ExecuteAndWriteOutput()
        return True, out_format
    logger.warning(
        f"the raster {in_raster} can not be encode with the rules "
        f"{conversion_table}, values to encode are strings"
    )
    return False, "uint8"


def extract_raster_bands(
    in_raster: str, out_raster: str, bands_of_interest: list[int]
) -> None:
    """use gdal_translate to extract bands of interest, band's index
    start at 1.

    Parameters
    ----------

    in_raster:
        raster to crop
    out_raster:
        raster cropped
    bands_of_interest:
        list of bands to copy in output raster
    """
    data_src = gdal.Open(in_raster)
    gdal.Translate(out_raster, data_src, bandList=bands_of_interest)


def compress_raster(
    raster_in: str, raster_out: str, compress_mode: AvailCompressions = "LZW"
) -> bool:
    """
    Compress a raster file using GDAL translation.

    This function compresses a raster file using GDAL's translation utility (gdal_translate).
    It applies the specified compression mode to the output raster file.
    If compression is successful, the function returns True; otherwise, it returns False.

    Parameters:
    -----------
    raster_in:
        The input raster file path.
    raster_out:
        The output raster file path.
    compress_mode:
        The compression mode to be applied. Default is 'LZW'.
        Supported compression modes: 'NONE', 'LZW', 'DEFLATE', 'PACKBITS', 'JPEG', 'CCITTRLE',
        'CCITTFAX3', 'CCITTFAX4', 'LZMA', 'ZSTD', 'WEBP', 'NONE', 'AUTO'.

    Notes:
    ------
    return type : bool
        True if compression is successful, False otherwise.
    """
    success = True
    command = (
        f"gdal_translate -co 'COMPRESS={compress_mode}' "
        f"-co 'BIGTIFF=YES' {raster_in} {raster_out}"
    )
    try:
        run(command)
    except Exception:  # pylint: disable=broad-except; run raises a generic Exception
        success = False
    return success


def raster_to_array(in_raster: InputRaster, band: int | None = None) -> np.ndarray:
    """
    Convert a raster file to a NumPy array.

    This function reads the raster data from the input file or GDAL dataset
    and converts it to a NumPy array. If the 'band' parameter is specified,
    only the data from the corresponding band is read; otherwise, data from
    all bands is read.

    Parameters:
    -----------
    in_raster:
        The input raster file path or GDAL dataset.
    band:
        The band number to read from the raster file. Default is None,
        which reads all bands.

    Notes:
    ------
    returned type : np.ndarray
                        The NumPy array representing the raster data.

    """
    dataset = fut.open_raster(in_raster)
    array_out: np.ndarray
    if band:
        raster_band = dataset.GetRasterBand(band)
        array_out = raster_band.ReadAsArray()
    else:
        array_out = dataset.ReadAsArray()
    return array_out


def read_raster_metadata(in_raster: InputRaster) -> RasterMetadata:
    """
    Read metadata from a raster file or GDAL dataset.

    This function opens the raster file or GDAL dataset specified by 'in_raster'
    and reads its metadata properties, including projection, geotransform, and
    size. It returns a 'RasterMetadata' object containing these metadata values.

    Parameters:
    -----------
    in_raster:
        The input raster file path or GDAL dataset.

    Notes:
    ------
    returned type : RasterMetadata
                        An object containing metadata information such as the size, projection,
                        and geotransform of the raster.
    """
    in_raster = fut.open_raster(in_raster)

    # property of raster
    projection = in_raster.GetProjectionRef()
    geo_transform = in_raster.GetGeoTransform()
    xsize = in_raster.RasterXSize
    ysize = in_raster.RasterYSize

    proj = osr.SpatialReference()
    proj.ImportFromWkt(projection)

    return RasterMetadata(
        img_size=ImageSize(xsize, ysize),
        projection=proj,
        img_origin=ImageOrigin(geo_transform[0], geo_transform[3]),
        pix_size=PixelSize(geo_transform[1], geo_transform[5]),
        pix_rot=PixelRotation(geo_transform[2], geo_transform[4]),
    )


@dataclass
class ChunkConfig:
    """Class used to store chunk parameters"""

    chunk_size_mode: Literal["user_fixed", "split_number"]
    number_of_chunks: int | None = None
    chunk_size_x: int | None = None
    chunk_size_y: int | None = None
    padding_size_x: int = 0
    padding_size_y: int = 0

    def __post_init__(self) -> None:
        if self.chunk_size_mode == "split_number":
            if self.number_of_chunks is None:
                raise ValueError(
                    "if chunk_size_mode = 'split_number' then 'number_of_chunks' must be set"
                )
        elif self.chunk_size_mode == "user_fixed":
            if self.chunk_size_x is None or self.chunk_size_y is None:
                raise ValueError(
                    "if chunk_size_mode = 'user_fixed' then "
                    "'chunk_size_x' and 'chunk_size_x' must be set"
                )


def pipelines_chunking(
    otb_pipelines: OtbPipelineCarrier,
    chunk_config: ChunkConfig,
    spatial_mask: str | None = None,
    exogenous_data_file: str | None = None,
    targeted_chunk: int | None = None,
) -> list[OtbPipelineCarrier]:
    """
    Chunk pipelines based on the specified configuration.

    Parameters
    ----------
    otb_pipelines
        The input OTB pipelines to be chunked.
    chunk_config
        The configuration object specifying chunking parameters.
    spatial_mask
        The path to the spatial mask file used for chunking (e.g., region file).
    exogenous_data_file
        The path to the exogenous data file used for chunking (e.g., meteorological data file).
    targeted_chunk
        Specify a specific chunk to target.

    Notes
    -----
    returned type: list[OtbPipelineCarrier]
                        A list of chunked OTB pipeline carriers.
    """

    def create_roi_application(
        pipeline: str | OtbApp | None, roi_boundary: dict[str, int]
    ) -> OtbApp | None:
        """
        Create a Region of Interest (ROI) application based on the given pipeline and boundary.

        Parameters
        ----------
        pipeline:
            The pipeline from which the ROI application will be created.
        roi_boundary:
            A dictionary containing boundary information with keys:
            'startx', 'sizex', 'starty', 'sizey'.

        Notes
        -----
        returned value: OtbApp | None
            The ROI application if the pipeline is provided, otherwise None.
        """
        if pipeline:
            application: OtbApp = otb_app.create_application(
                otb_app.AvailableOTBApp.EXTRACT_ROI,
                {
                    "in": pipeline,
                    "startx": roi_boundary["startx"],
                    "sizex": roi_boundary["sizex"],
                    "starty": roi_boundary["starty"],
                    "sizey": roi_boundary["sizey"],
                },
            )[0]
            return application
        return None

    otb_app_pipeline = otb_pipelines.get_nonempty_pipeline()
    assert otb_app_pipeline
    raster_meta = extract_raster_metadata_from_otb(otb_app_pipeline)
    assert raster_meta.img_size
    boundaries, paddings = get_chunks_boundaries(
        shape=(raster_meta.img_size.x_size, raster_meta.img_size.y_size),
        chunk_config=chunk_config,
    )

    chunks: list[OtbPipelineCarrier] = []
    if targeted_chunk is not None:
        assert targeted_chunk < len(boundaries)
        boundaries = [boundaries[targeted_chunk]]
        paddings = [paddings[targeted_chunk]]
    for padding, boundary in zip(paddings, boundaries):
        chunks.append(
            OtbPipelineCarrier(
                create_roi_application(otb_pipelines.interpolated_pipeline, boundary),
                create_roi_application(otb_pipelines.raw_pipeline, boundary),
                create_roi_application(otb_pipelines.binary_mask_pipeline, boundary),
                create_roi_application(exogenous_data_file, boundary),
                create_roi_application(spatial_mask, boundary),
                spatial_bounds=SpatialBounds(
                    (
                        padding["startx"],
                        padding["endx"],
                        padding["starty"],
                        padding["endy"],
                    ),
                    (
                        boundary["startx"],
                        boundary["sizex"],
                        boundary["starty"],
                        boundary["sizey"],
                    ),
                ),
            )
        )

    return chunks


@dataclass
class UserDefinedFunction:
    """Store user defined function."""

    udf: partial
    is_custom_features: bool = False


def process_pipeline(
    otb_pipelines: OtbPipelineCarrier,
    mask_value: int,
    function: UserDefinedFunction,
    logger: logging.Logger,
) -> UserDefinedFunctionPayload:
    """Apply a Python function to the outputs of one or more OTB pipelines.

    This function takes one or more OTB pipelines along with their associated data,
    a Python function to apply on this data.
    It returns a payload containing the resulting data from the function application,
    along with other associated information.

    Parameters:
    -----------
    function:
        The Python function to apply on the data.
    otb_pipelines:
        The OTB pipelines containing the input data.
    remove_padding:
        A boolean indicating whether to remove padding from the output data.

    Notes
    -----
    return a UserDefinedFunctionPayload
        A payload containing the resulting data, mask, labels, spatial metadata,
        OTB image, and pipeline dependencies.
    """
    assert otb_pipelines.spatial_bounds and otb_pipelines.spatial_bounds.stream_bbox
    region_info = (
        f"processing region start_x : {otb_pipelines.spatial_bounds.stream_bbox[0]} size_x :"
        f" {otb_pipelines.spatial_bounds.stream_bbox[1]} "
        f"start_y : {otb_pipelines.spatial_bounds.stream_bbox[2]} "
        f"size_y : {otb_pipelines.spatial_bounds.stream_bbox[3]}"
    )
    logger.info(region_info)
    spatial_mask = None
    if otb_pipelines.spatial_mask:
        spatial_mask = MaskData(
            np.squeeze(otb_pipelines.spatial_mask.ExportImage("out")["array"]),
            mask_value,
        )
    udf_payload = process_function(
        function=function, otb_pipelines=otb_pipelines, spatial_mask=spatial_mask
    )
    return udf_payload


def merge_results(
    arrays_transform: list[tuple[np.ndarray | None, SpatialMetadata]],
    rio_data_sets: list[rasterio.io.DatasetReader],
    output_number_of_bands: int,
    udf_payload: UserDefinedFunctionPayload,
) -> tuple[np.ndarray, Affine]:
    """Merge arrays and their transform into a single numpy array.

    This function takes a list of arrays along with their corresponding spatial
    metadata and rasterio dataset readers. It merges these arrays into a single
    numpy array, considering the number of output bands. If there are multiple
    rasterio datasets, it merges them into a single mosaic. Otherwise, it repeats
    the mask array if the first array is None.

    Parameters:
    -----------
    arrays_transform:
        A list of tuples containing numpy arrays and their spatial metadata.
    rio_data_sets:
        A list of rasterio dataset readers.
    output_number_of_bands:
        The number of bands in the output array.
    udf_payload:
        The user-defined function payload containing additional metadata.

    Notes:
    ------
    return a tuple containing the merged numpy array and its transformation information.
    """
    mosaic: np.ndarray
    if len(rio_data_sets) > 1:
        mosaic, out_trans = merge(rio_data_sets)
    else:
        if arrays_transform[0][0] is None:
            assert udf_payload.mask is not None
            mosaic = np.repeat(
                udf_payload.mask[np.newaxis, :, :], output_number_of_bands, axis=0
            )
        else:
            mosaic = np.moveaxis(arrays_transform[0][0], -1, 0)
        out_trans = rio_data_sets[0].transform
    return mosaic, out_trans


def apply_udf_to_pipeline(
    otb_pipelines_list: list[OtbPipelineCarrier],
    function: UserDefinedFunction,
    mask_value: int = 0,
    output_number_of_bands: int = 1,
    logger: logging.Logger = LOGGER,
) -> tuple[UserDefinedFunctionPayload, list[np.ndarray | None]]:
    """Apply a function to an otb pipeline list and merge outputs.

    If pipelines contains a 'spatial_mask' attribute, this mask will be binarised
    then the resulting output could be defined as the following :
    output = output * mask_value

    Parameters
    ----------
    otb_pipelines_list
        list of OtbPipelineCarrier
    function
        user defined function
    mask_value
        masking value
    output_number_of_bands
        force output shape
    logger
        logging.logger
    """
    assert otb_pipelines_list

    new_arrays = []
    chunks_mask = []

    for otb_pipelines in otb_pipelines_list:
        assert otb_pipelines.spatial_bounds and otb_pipelines.spatial_bounds.stream_bbox
        size_x = otb_pipelines.spatial_bounds.stream_bbox[1]
        size_y = otb_pipelines.spatial_bounds.stream_bbox[3]
        udf_payload = process_pipeline(otb_pipelines, mask_value, function, logger)
        new_arrays.append((udf_payload.data, udf_payload.spatial_meta))
        chunks_mask.append(udf_payload.mask)

    all_data_sets = get_rasterio_datasets(
        new_arrays,
        mask_value,
        force_output_shape=(size_y, size_x, output_number_of_bands),
    )

    mosaic, out_trans = merge_results(
        new_arrays, all_data_sets, output_number_of_bands, udf_payload
    )

    assert udf_payload.labels is not None

    return (
        UserDefinedFunctionPayload(
            data=mosaic,
            spatial_meta=SpatialMetadata(
                udf_payload.spatial_meta.projection, out_trans.to_gdal()
            ),
            labels=udf_payload.labels,
            otb_img=udf_payload.otb_img,
            pipeline_dependencies=udf_payload.pipeline_dependencies,
        ),
        chunks_mask,
    )


def get_expected_arr_shapes(
    array_proj: list[tuple[np.ndarray | None, SpatialMetadata]],
    force_output_shape: tuple[int, int, int],
) -> set[tuple[int, int, int]]:
    """Get shape from arrays.

    Parameters
    ----------
    array_proj
        list of possible arrays
    force_output_shape
        if array is None, use this shape
    Notes
    -----
        return set of tuples representing shapes
    """
    expected_arr_shapes = {
        arr[0].shape for arr in array_proj if isinstance(arr[0], np.ndarray)
    }
    if not expected_arr_shapes:
        expected_arr_shapes = {force_output_shape}
    return expected_arr_shapes


def get_expected_arr_types(
    array_proj: list[tuple[np.ndarray | None, SpatialMetadata]],
    force_output_type: str,
) -> set[str]:
    """Get types from arrays.

    Parameters
    ----------
    array_proj
        list of possible arrays
    force_output_type
        if array is None, use this type
    Notes
    -----
        return set of tuples representing shapes
    """
    expected_arr_types = {
        arr[0].dtype for arr in array_proj if isinstance(arr[0], np.ndarray)
    }
    if not expected_arr_types:
        expected_arr_types = {force_output_type}
    return expected_arr_types


def create_rasterio_dataset(
    array: np.ndarray, geo_transform: GdalGeoTranform, epsg_code: int
) -> rasterio.io.DatasetReader:
    """Create in-memory rasterio raster

    Parameters
    ----------
    array
        input array containing data
    geo_transform
        transform
    epsg_code
        epsg code

    Notes
    -----
        return an in-memory rasterio object
    """
    trans = Affine.from_gdal(*geo_transform)
    array_ordered = np.moveaxis(array, -1, 0)
    if len(array_ordered.shape) != 3:
        raise ValueError("array's must be 3D")
    count, height, width = array_ordered.shape

    with MemoryFile() as memfile:
        with memfile.open(
            driver="GTiff",
            height=height,
            width=width,
            count=count,
            dtype=array.dtype,
            crs=f"EPSG:{epsg_code}",
            transform=trans,
        ) as dataset:
            dataset.write(array_ordered)
        return memfile.open()


def get_rasterio_datasets(
    array_proj: list[tuple[np.ndarray | None, SpatialMetadata]],
    mask_value: int = 0,
    force_output_shape: tuple[int, int, int] = (1, 1, 1),
    force_output_type: str = "int32",
) -> list[rasterio.io.DatasetReader]:
    """Transforms numpy arrays containing projection data into rasterio datasets.

    This function takes a list of tuples, where each tuple contains a numpy array
    and its corresponding spatial metadata, and transforms them into rasterio datasets.
    It can handle arrays with different shapes and data types by forcing the output shape
    and data type if necessary.

    Parameters:
    -----------
    array_proj:
        A list of tuples containing numpy arrays and their spatial metadata.
    mask_value:
        The value used for filling empty cells in case of None arrays.
    force_output_shape:
        A tuple representing the desired output shape of the arrays.
    force_output_type:
        The desired data type of the arrays.

    Notes:
    ------
    return a list of rasterio dataset readers containing the transformed arrays.
    """
    expected_arr_shapes = get_expected_arr_shapes(array_proj, force_output_shape)
    expected_arr_shape = list(expected_arr_shapes)[0]
    expected_arr_types = get_expected_arr_types(array_proj, force_output_type)
    expected_arr_type = list(expected_arr_types)[0]
    epsg_code = array_proj[0][-1].projection.GetAttrValue("AUTHORITY", 1)

    all_data_sets = []
    for new_array in array_proj:
        array = new_array[0]
        geo_transform = new_array[-1].gdal_geo_transform
        if array is None:
            array = np.full(expected_arr_shape, mask_value, dtype=expected_arr_type)
        dataset = create_rasterio_dataset(array, geo_transform, epsg_code)
        all_data_sets.append(dataset)

    return all_data_sets


def get_mask_roi(
    spatial_mask: MaskData | None = None,
) -> tuple[np.ndarray | None, bool, bool]:
    """Extracts information from a spatial mask.

    This function takes a MaskData object representing a spatial mask and extracts
    relevant information such as the mask itself, whether to ignore the region of interest (ROI),
    and whether the ROI contains parts of the mask.

    Parameters:
    -----------
    spatial_mask:
        A MaskData object representing the spatial mask.

    Notes
    -----
    return a tuple:
        A tuple containing the mask, a boolean indicating whether to ignore the ROI,
        and a boolean indicating whether the ROI contains parts of the mask.
    """
    mask_roi = None
    roi_to_ignore = False
    roi_contains_mask_part = False

    if spatial_mask is not None:
        mask_roi = binarize(spatial_mask.mask_arr)
        unique_mask_values = np.unique(mask_roi)

        if (
            len(unique_mask_values) == 1
            and unique_mask_values[0] == spatial_mask.mask_value
        ):
            roi_to_ignore = True
        elif (
            len(unique_mask_values) > 1
            and spatial_mask.mask_value in unique_mask_values
        ):
            roi_contains_mask_part = True

    return mask_roi, roi_to_ignore, roi_contains_mask_part


def remove_padding_from_array(
    output_arr: np.ndarray, bounds: SpatialBounds
) -> np.ndarray:
    """Remove padding from a numpy array.

    This function removes padding from a 3D numpy array based on the spatial bounds provided.

    Parameters:
    -----------
    output_arr:
        The input 3D numpy array.
    bounds:
        SpatialBounds object containing padding and stream bounding box information.

    Notes:
    ------
    return np.ndarray:
        The numpy array with padding removed.
    """
    assert bounds.padding and bounds.stream_bbox
    output_arr = output_arr[
        bounds.padding[2] : (bounds.stream_bbox[3] - bounds.padding[3]),
        bounds.padding[0] : (bounds.stream_bbox[1] - bounds.padding[1]),
        :,
    ]
    return output_arr


def offset_without_padding(
    bounds: SpatialBounds, raster_meta: RasterMetadata
) -> tuple[float, float]:
    """Calculate offset without padding.

    This function calculates the offset without padding based on the spatial bounds
    and raster metadata provided.

    Parameters:
    -----------
    bounds:
        SpatialBounds object containing padding and stream bounding box information.
    raster_meta:
        RasterMetadata object containing pixel width and height information.

    Notes:
    ------
    return a tuple
        A tuple containing the x and y offsets without padding.
    """

    assert (
        bounds.padding
        and raster_meta.pix_size
        and raster_meta.pix_size.pixel_width
        and raster_meta.pix_size.pixel_height
    )
    offset_x = (bounds.padding[0] - 0.5) * raster_meta.pix_size.pixel_width
    offset_y = (bounds.padding[2] - 0.5) * raster_meta.pix_size.pixel_height
    offset_xy = (offset_x, offset_y)
    return offset_xy


def process_function(
    function: UserDefinedFunction,
    otb_pipelines: OtbPipelineCarrier,
    spatial_mask: MaskData | None = None,
    remove_padding: bool = True,
) -> UserDefinedFunctionPayload:
    """Apply a Python function to the outputs of one or more OTB pipelines.

    This function takes one or more OTB pipelines along with their associated data,
    a Python function to apply on this data, and optionally a spatial mask.
    It returns a payload containing the resulting data from the function application,
    along with other associated information.

    Parameters:
    -----------
    function:
        The Python function to apply on the data.
    otb_pipelines:
        The OTB pipelines containing the input data.
    spatial_mask:
        Optional spatial mask.
    remove_padding:
        A boolean indicating whether to remove padding from the output data.

    Notes
    -----
    This is the place where data is extracted from pipelines.

    return a UserDefinedFunctionPayload
        A payload containing the resulting data, mask, labels, spatial metadata,
        OTB image, and pipeline dependencies.
    """
    assert otb_pipelines.spatial_bounds
    mask_roi, roi_to_ignore, roi_contains_mask_part = get_mask_roi(spatial_mask)
    (
        pipelines_data,
        raster_meta,
        otbimage,
        _,
    ) = get_pipelines_data(otb_pipelines)

    output_arr = None
    new_labels = []
    offset_xy = (0.0, 0.0)
    if not roi_to_ignore:
        if not function.is_custom_features:
            output_arr, new_labels = function.udf(pipelines_data.interpolated)
        else:
            offset_xy = offset_without_padding(
                otb_pipelines.spatial_bounds, raster_meta
            )
            assert (
                raster_meta.img_size
                and raster_meta.img_origin
                and raster_meta.pix_size
                and raster_meta.projection
            )
            geo_transform = (
                raster_meta.img_origin.x_origin + offset_xy[0],
                raster_meta.pix_size.pixel_width,
                0,
                raster_meta.img_origin.y_origin + offset_xy[1],
                0,
                raster_meta.pix_size.pixel_height,
            )

            output_arr, new_labels = function.udf(
                data=PipelinesData(
                    pipelines_data.interpolated,
                    pipelines_data.raw,
                    pipelines_data.exogenous,
                    pipelines_data.binary_mask,
                ),
                transform_in=(
                    Affine.from_gdal(*geo_transform),
                    raster_meta.img_size.y_size,
                    raster_meta.img_size.x_size,
                    raster_meta.projection,
                ),
            )

        if roi_contains_mask_part:
            assert mask_roi is not None
            output_arr = output_arr * mask_roi[:, :, np.newaxis]

    if remove_padding:
        offset_xy = offset_without_padding(otb_pipelines.spatial_bounds, raster_meta)
        if not roi_to_ignore and output_arr is not None:
            output_arr = remove_padding_from_array(
                output_arr, otb_pipelines.spatial_bounds
            )
    assert raster_meta and raster_meta.img_origin and raster_meta.pix_size
    geo_transform = (
        raster_meta.img_origin.x_origin + offset_xy[0],
        raster_meta.pix_size.pixel_width,
        0,
        raster_meta.img_origin.y_origin + offset_xy[1],
        0,
        raster_meta.pix_size.pixel_height,
    )
    return UserDefinedFunctionPayload(
        data=output_arr,
        mask=mask_roi,
        labels=new_labels,
        spatial_meta=SpatialMetadata(raster_meta.projection, geo_transform),
        otb_img=otbimage,
        pipeline_dependencies=_,
    )


def get_pipelines_data(
    otb_pipelines: OtbPipelineCarrier,
) -> tuple[PipelinesData, RasterMetadata, OtbImage, OtbDep]:
    """
    Retrieve data from OTB pipelines along with their parameters and dependencies.

    This function retrieves data from the input OTB pipelines, including interpolated image,
    raw image, exogenous data array, binary mask image, raster metadata, OTB image, and
    pipeline dependencies. It executes each pipeline if it exists, retrieves the necessary
    information, and returns them as a tuple.

    Parameters
    ----------
    otb_pipelines:
        The carrier object containing OTB pipelines.

    Notes
    -----
    returned type: tuple[PipelinesData, RasterMetadata, OtbImage, OtbDep]
                    A tuple containing pipelines data, raster metadata,
                    OTB image, and dependencies.
    """
    otbimage = None
    exo_roi_array = None
    interpolated_image = {"array": None}
    raw_image = {"array": None}
    binary_mask_image = {"array": None}

    x_resolution = y_resolution = o_x = o_y = pipeline_proj = cols = rows = None

    deps = []
    if otb_pipelines.interpolated_pipeline is not None:
        otb_pipelines.interpolated_pipeline.Execute()
        deps.append(otb_pipelines.interpolated_pipeline)
        proj = otb_pipelines.interpolated_pipeline.GetImageProjection("out")
        pipeline_proj = osr.SpatialReference()
        pipeline_proj.ImportFromWkt(proj)
        o_x, o_y = otb_pipelines.interpolated_pipeline.GetImageOrigin("out")
        (
            x_resolution,
            y_resolution,
        ) = otb_pipelines.interpolated_pipeline.GetImageSpacing("out")
        cols, rows = otb_pipelines.interpolated_pipeline.GetImageSize("out")

        # remove this call to GetVectorImageAsNumpyArray
        # allow a gain of few seconds required to initialise otbimage object
        # This step is very time consuming as the whole pipeline is computed here
        interpolated_image = otb_pipelines.interpolated_pipeline.ExportImage("out")
        if otbimage is None:
            otbimage = interpolated_image
    if otb_pipelines.raw_pipeline is not None:
        otb_pipelines.raw_pipeline.Execute()
        deps.append(otb_pipelines.raw_pipeline)
        proj = otb_pipelines.raw_pipeline.GetImageProjection("out")
        pipeline_proj = osr.SpatialReference()
        pipeline_proj.ImportFromWkt(proj)
        o_x, o_y = otb_pipelines.raw_pipeline.GetImageOrigin("out")
        x_resolution, y_resolution = otb_pipelines.raw_pipeline.GetImageSpacing("out")
        cols, rows = otb_pipelines.raw_pipeline.GetImageSize("out")
        raw_image = otb_pipelines.raw_pipeline.ExportImage("out")
        if otbimage is None:
            otbimage = raw_image
    if otb_pipelines.binary_mask_pipeline is not None:
        otb_pipelines.binary_mask_pipeline.Execute()
        deps.append(otb_pipelines.binary_mask_pipeline)
        proj = otb_pipelines.binary_mask_pipeline.GetImageProjection("out")
        pipeline_proj = osr.SpatialReference()
        pipeline_proj.ImportFromWkt(proj)
        o_x, o_y = otb_pipelines.binary_mask_pipeline.GetImageOrigin("out")
        x_resolution, y_resolution = otb_pipelines.binary_mask_pipeline.GetImageSpacing(
            "out"
        )
        binary_mask_image = otb_pipelines.binary_mask_pipeline.ExportImage("out")
        cols, rows = otb_pipelines.binary_mask_pipeline.GetImageSize("out")
        if otbimage is None:
            otbimage = binary_mask_image
    if otb_pipelines.exogenous_data_pipeline is not None:
        otb_pipelines.exogenous_data_pipeline.Execute()
        deps.append(otb_pipelines.exogenous_data_pipeline)
        exo_roi = otb_pipelines.exogenous_data_pipeline.ExportImage("out")
        exo_roi_array = exo_roi["array"]

    return (
        PipelinesData(
            interpolated_image["array"],
            raw_image["array"],
            exo_roi_array,
            binary_mask_image["array"],
        ),
        # mypy can't infer data type from OTB methods
        RasterMetadata(
            pix_size=PixelSize(x_resolution, y_resolution),  # type: ignore
            img_origin=ImageOrigin(o_x, o_y),  # type: ignore
            projection=pipeline_proj,
            img_size=ImageSize(cols, rows),  # type: ignore
        ),
        otbimage,
        deps,
    )


def get_padding(
    chunk_config: ChunkConfig, boundary: dict, size_x: int, size_y: int
) -> dict:
    """
    Return a dictionary similar to `boundary` containing the coordinates of the padding.
    For each coordinate, keep the minimum between:
        - The given padding contained in the `chunk_config` argument
        - The new padding, calculated using the `boundary` argument

    Parameters
    ----------
    chunk_config:
        Chunk parameters
    boundary:
        Boundary used to compute the padding
    size_x:
        Image's size in x-coordinates (in pixels)
    size_y
        Image's size in y-coordinates (in pixels)
    """
    padding_start_x = min(chunk_config.padding_size_x, boundary["startx"])
    padding_start_y = min(chunk_config.padding_size_y, boundary["starty"])
    padding_end_x = min(
        chunk_config.padding_size_x, size_x - boundary["startx"] - boundary["sizex"]
    )
    padding_end_y = min(
        chunk_config.padding_size_y, size_y - boundary["starty"] - boundary["sizey"]
    )
    return {
        "startx": padding_start_x,
        "endx": padding_end_x,
        "starty": padding_start_y,
        "endy": padding_end_y,
    }


def get_boundary_with_padding(
    boundary: dict[str, int], padding: dict[str, int]
) -> dict[str, int]:
    """
    Return a dictionary containing the coordinates of boundaries with an added padding

    Parameters
    ----------
    boundary:
        Dictionary containing the original coordinates
    padding:
        Dictionary containing the padding to apply to the image
    """
    return {
        "startx": boundary["startx"] - padding["startx"],
        "sizex": boundary["sizex"] + padding["startx"] + padding["endx"],
        "starty": boundary["starty"] - padding["starty"],
        "sizey": boundary["sizey"] + padding["starty"] + padding["endy"],
    }


def get_raster_chunks_boundaries_carto(
    raster_file: Path, chunk_config: ChunkConfig
) -> list[dict[str, int]]:
    """
    Return chunks boundaries and paddings (see `get_chunks_boundaries_carto` for more details).
    The function uses rasterio to get the image size.

    Parameters
    ----------
    raster_file:
        Path to raster
    chunk_config:
        Chunks parameters
    """
    with rasterio.open(raster_file, "r") as rio_file:
        sizex = rio_file.meta["width"]
        sizey = rio_file.meta["height"]
        return get_chunks_boundaries_carto(
            (sizex, sizey), rio_file.transform, chunk_config
        )


def get_chunks_boundaries_carto(
    shape: tuple[int, int], in_transform: rasterio.transform, chunk_config: ChunkConfig
) -> list[dict[str, int]]:
    """
    Transform chunks boundaries from pixel numbers to a coordinate system.

    Parameters
    ----------
    shape:
        Size of the image (in pixels)
    in_transform
        Rasterio transform to use
    chunk_config
        Chunks parameters
    """
    boundaries, _ = get_chunks_boundaries(shape, chunk_config)
    boundaries_carto = []
    for boundary in boundaries:
        llx = boundary["startx"]
        lrx = llx + boundary["sizex"]
        uly = boundary["starty"]
        lly = uly + boundary["sizey"]

        llx_carto, lly_carto = rasterio.transform.xy(in_transform, lly, llx)
        lrx_carto, uly_carto = rasterio.transform.xy(in_transform, uly, lrx)
        boundaries_carto.append(
            {
                "lowerleft_x": llx_carto,
                "lowerleft_y": lly_carto,
                "upperright_x": lrx_carto,
                "upperright_y": uly_carto,
            }
        )
    return boundaries_carto


def get_raster_chunks_boundaries(
    raster_file: PathLike, chunk_config: ChunkConfig
) -> tuple[list, list]:
    """
    Return chunks boundaries and paddings (see `get_chunks_boundaries` for more details).
    The function uses rasterio to get the image size.

    Parameters
    ----------
    raster_file:
        Path to raster
    chunk_config:
        Chunks parameters
    """
    with rasterio.open(raster_file, "r") as rio_file:
        sizex = rio_file.meta["width"]
        sizey = rio_file.meta["height"]
    return get_chunks_boundaries((sizex, sizey), chunk_config)


def get_chunks_boundaries(
    shape: tuple[int, int], chunk_config: ChunkConfig
) -> tuple[list[dict[str, int]], list[dict[str, int]]]:
    """
    Create a grid of chunks of size defined by `chunk_config` and return a list of all chunks'
    boundaries and paddings (as dictionaries).

    Parameters
    ----------
    shape:
        Size of the image (x and y coordinates)
    chunk_config:
        Parameters of the chunks
    """
    boundaries = []
    paddings = []
    size_x, size_y = shape[0], shape[1]

    if (
        2 * chunk_config.padding_size_x >= size_x
        or 2 * chunk_config.padding_size_y >= size_y
    ):
        raise ValueError(
            f"Error: padding size ({chunk_config.padding_size_x},{chunk_config.padding_size_y}) "
            f"exceeds the image size ({size_x}, {size_y})"
        )

    if chunk_config.chunk_size_mode == "user_fixed":
        for start_y in np.arange(0, size_y, chunk_config.chunk_size_y):
            for start_x in np.arange(0, size_x, chunk_config.chunk_size_x):
                boundary = {
                    "startx": start_x,
                    "sizex": min(chunk_config.chunk_size_x, size_x - start_x),
                    "starty": start_y,
                    "sizey": min(chunk_config.chunk_size_y, size_y - start_y),
                }
                padding = get_padding(chunk_config, boundary, size_x, size_y)
                paddings.append(padding)
                boundaries.append(get_boundary_with_padding(boundary, padding))

    elif chunk_config.chunk_size_mode == "split_number":
        assert chunk_config.number_of_chunks
        if chunk_config.number_of_chunks > size_x + size_y:
            raise ValueError(
                f"Error: number of chunks ({chunk_config.number_of_chunks})"
                f"vastly exceeds the image size ({size_x * size_y} pixels)"
            )
        if chunk_config.number_of_chunks > size_y:
            unused_chunks = chunk_config.number_of_chunks - size_y
            split_x = np.linspace(0, size_x, unused_chunks + 1)
            split_y = np.linspace(0, size_y, size_y + 1)
        else:
            split_x = np.linspace(0, size_x, 2)
            split_y = np.linspace(0, size_y, chunk_config.number_of_chunks + 1)

        split_y = [math.floor(x) for x in split_y]
        split_x = [math.floor(x) for x in split_x]

        for cpt1, start_y in enumerate(split_y[:-1]):
            for cpt2, start_x in enumerate(split_x[:-1]):
                boundary = {
                    "startx": start_x,
                    "sizex": split_x[cpt2 + 1] - start_x,
                    "starty": start_y,
                    "sizey": split_y[cpt1 + 1] - start_y,
                }
                padding = get_padding(chunk_config, boundary, size_x, size_y)
                paddings.append(padding)
                boundaries.append(get_boundary_with_padding(boundary, padding))
    else:
        raise ValueError(
            f"Unknow split method {chunk_config.chunk_size_mode}, only split_number"
            " and user_fixed are handled"
        )
    return boundaries, paddings


def merge_rasters(
    rasters: list[InputRaster],
    output_path: str,
    output_type: str = "Int16",
    creation_options: dict | None = None,
) -> None:
    """Merge geo-referenced rasters thanks to rasterio.merge.

    Parameters
    ----------
    rasters
        list of raster's path
    output_path
        output path
    """
    if rasters is not None:
        resolution = fut.get_raster_resolution(rasters[0])
    else:
        raise ValueError("raster can't be empty")
    if resolution is not None:
        res_x, res_y = resolution

        merge_options = fut.TileMergeOptions(
            spatial_resolution=(res_x, -res_y),
            output_pixel_type=output_type,
            creation_options=creation_options,
        )
        fut.assemble_tile_merge(rasters, output_path, merge_options)

    else:
        raise ValueError("Resolutions can't be empty")


def gdal_to_rio_metadata(spatial_meta: SpatialMetadata) -> RasterioSpatialMetadata:
    """
    Convert spatial metadata from gdal style to rasterio

    Parameters
    ----------
    spatial_meta
        Spatial metadata as used by gdal

    Returns
    -------
    Spatial metadata as used by rasterio
    """
    raster_transform = gdal_transform_to_rio(spatial_meta.gdal_geo_transform)
    epsg = spatial_meta.projection.GetAttrValue("AUTHORITY", 1)
    return RasterioSpatialMetadata(epsg, raster_transform)
