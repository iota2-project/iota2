#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Generate iota² output tree."""
import logging
import shutil
from pathlib import Path

LOGGER = logging.getLogger("distributed.worker")


def generate_directories(
    root: str,
    merge_final_classifications: bool,
    tile_list: list[str],
    enable_boundary_fusion: bool = False,
    comparison_mode: bool = False,
) -> None:
    """
    Generate iota2 classification workflow output directories.

    Parameters
    ----------
    root:
        iota2 output path
    merge_final_classifications
        flag to generate the directory dedicated to receive the
        fusion of final classifications

    Notes
    -----
    the removal of directories is managed in
     iota.sequence_builders.i2_classification.generate_output_directories
    """
    Path(root).mkdir(parents=True, exist_ok=True)
    (Path(root) / "samplesSelection").mkdir(parents=True, exist_ok=True)
    (Path(root) / "model").mkdir(parents=True, exist_ok=True)
    (Path(root) / "formattingVectors").mkdir(parents=True, exist_ok=True)
    (Path(root) / "config_model").mkdir(parents=True, exist_ok=True)
    (Path(root) / "envelope").mkdir(parents=True, exist_ok=True)
    (Path(root) / "classif").mkdir(parents=True, exist_ok=True)
    if enable_boundary_fusion:
        (Path(root) / "boundary").mkdir(parents=True, exist_ok=True)
    (Path(root) / "shapeRegion").mkdir(parents=True, exist_ok=True)
    (Path(root) / "final").mkdir(parents=True, exist_ok=True)
    if comparison_mode:
        (Path(root) / "final" / "standard" / "TMP").mkdir(parents=True, exist_ok=True)
        (Path(root) / "final" / "boundary" / "TMP").mkdir(parents=True, exist_ok=True)

    (Path(root) / "final" / "TMP").mkdir(parents=True, exist_ok=True)
    (Path(root) / "features").mkdir(parents=True, exist_ok=True)
    (Path(root) / "dataRegion").mkdir(parents=True, exist_ok=True)
    (Path(root) / "learningSamples").mkdir(parents=True, exist_ok=True)
    (Path(root) / "dataAppVal").mkdir(parents=True, exist_ok=True)
    (Path(root) / "stats").mkdir(parents=True, exist_ok=True)
    (Path(root) / "logs").mkdir(parents=True, exist_ok=True)

    if merge_final_classifications:
        (Path(root) / "final" / "merge_final_classifications").mkdir(
            parents=True, exist_ok=True
        )
    for tile in tile_list:
        (Path(root) / "features" / tile / "tmp").mkdir(parents=True, exist_ok=True)


def generate_features_maps_directories(root: str, tile_list: list[str]) -> None:
    """
    Generate output directories for write features maps.

    Parameters
    ----------
    root : str
        iota2 output path
    """
    Path(root).mkdir(parents=True, exist_ok=True)
    if (Path(root) / "final").exists():
        shutil.rmtree(Path(root) / "final")
    (Path(root) / "final").mkdir(parents=True, exist_ok=True)
    if (Path(root) / "features").exists():
        shutil.rmtree(Path(root) / "features")
    (Path(root) / "features").mkdir(parents=True, exist_ok=True)
    for tile in tile_list:
        (Path(root) / "features" / tile / "tmp").mkdir(parents=True, exist_ok=True)
    if (Path(root) / "customF").exists():
        shutil.rmtree(Path(root) / "customF")
    (Path(root) / "customF").mkdir(parents=True, exist_ok=True)
    if (Path(root) / "by_tiles").exists():
        shutil.rmtree(Path(root) / "by_tiles")
    (Path(root) / "by_tiles").mkdir(parents=True, exist_ok=True)


def generate_directories_obia(
    root: str, merge_final_classifications: bool, tile_list: list[str]
) -> None:
    """
    Generate iota2 classification workflow output directories.

    Parameters
    ----------
    root:
        iota2 output path
    merge_final_classifications
        flag to generate the directory dedicated to receive the
        fusion of final classifications

    Notes
    -----
    the removal of directories is managed in
     iota.sequence_builders.i2_classification.generate_output_directories
    """
    Path(root).mkdir(parents=True, exist_ok=True)
    (Path(root) / "samplesSelection").mkdir(parents=True, exist_ok=True)
    (Path(root) / "model").mkdir(parents=True, exist_ok=True)
    (Path(root) / "formattingVectors").mkdir(parents=True, exist_ok=True)
    (Path(root) / "config_model").mkdir(parents=True, exist_ok=True)
    (Path(root) / "envelope").mkdir(parents=True, exist_ok=True)
    (Path(root) / "classif" / "zonal_stats").mkdir(parents=True, exist_ok=True)
    (Path(root) / "classif" / "classif_tmp").mkdir(parents=True, exist_ok=True)
    (Path(root) / "classif" / "reduced").mkdir(parents=True, exist_ok=True)
    (Path(root) / "shapeRegion").mkdir(parents=True, exist_ok=True)
    (Path(root) / "final/TMP").mkdir(parents=True, exist_ok=True)
    if merge_final_classifications:
        (Path(root) / "final" / "merge_final_classifications").mkdir(
            parents=True, exist_ok=True
        )
    (Path(root) / "features").mkdir(parents=True, exist_ok=True)
    (Path(root) / "dataRegion").mkdir(parents=True, exist_ok=True)
    (Path(root) / "learningSamples" / "zonal_stats").mkdir(parents=True, exist_ok=True)
    (Path(root) / "dataAppVal").mkdir(parents=True, exist_ok=True)
    (Path(root) / "stats").mkdir(parents=True, exist_ok=True)
    (Path(root) / "segmentation" / "tmp").mkdir(parents=True, exist_ok=True)
    (Path(root) / "segmentation" / "grid_split").mkdir(parents=True, exist_ok=True)
    (Path(root) / "segmentation" / "grid_split_learn").mkdir(
        parents=True, exist_ok=True
    )
    for tile in tile_list:
        (Path(root) / "features" / tile / "tmp").mkdir(parents=True, exist_ok=True)


def generate_directories_external_classification(
    root: str, boundary_sizes: list[int]
) -> None:
    """Generate additional folders in case of external classification."""

    (Path(root) / "classif" / "full_pred").mkdir(parents=True, exist_ok=True)
    (Path(root) / "boundary_vectors").mkdir(parents=True, exist_ok=True)
    (Path(root) / "boundary_vectors" / "boundary_files").mkdir(
        parents=True, exist_ok=True
    )
    (Path(root) / "boundary_vectors" / "boundary_points").mkdir(
        parents=True, exist_ok=True
    )
    (Path(root) / "boundary_vectors" / "validation_files").mkdir(
        parents=True, exist_ok=True
    )
    if boundary_sizes:
        for buff_size in boundary_sizes:
            (Path(root) / "boundary_vectors" / f"boundary_files/{buff_size}").mkdir(
                parents=True, exist_ok=True
            )
            (Path(root) / "boundary_vectors" / f"boundary_points/{buff_size}").mkdir(
                parents=True, exist_ok=True
            )
            (Path(root) / "boundary_vectors" / f"validation_files/{buff_size}").mkdir(
                parents=True, exist_ok=True
            )
