#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provides class, instance and functions related to the compression options"""
from dataclasses import dataclass
from enum import Enum
from pathlib import Path

from iota2.typings.i2_types import PathLike


@dataclass()
class CompressionOptions:
    """Class used for compression options"""

    class AlgorithmEnum(Enum):
        """
        Enum class for gdal compression algorithms
        """

        NONE = "NONE"
        LZW = "LZW"
        ZSTD = "ZSTD"

    _algorithm: str = "ZSTD"
    _predictor: int = 2
    _algorithm_set: bool = False
    _predictor_set: bool = False

    @property
    def algorithm(self) -> str:
        """Get algorithm"""
        return self._algorithm

    @algorithm.setter
    def algorithm(self, algorithm: str) -> None:
        """Set algorithm if not already set"""
        if self._algorithm_set:
            pass
        else:
            self._algorithm = algorithm
            self._algorithm_set = True

    @property
    def predictor(self) -> int:
        """Get predictor"""
        return self._predictor

    @predictor.setter
    def predictor(self, predictor: int) -> None:
        """Set predictor if not already set"""
        if self._predictor_set:
            pass
        else:
            self._predictor = predictor
            self._predictor_set = True


compression_options = CompressionOptions()


def delete_compression_suffix(output_file_path: PathLike) -> str:
    """
    Remove compression options from the string corresponding to
     the out parameter of an otb application
    :param output_file_path: string corresponding to the out parameter of an otb application
    :return: output_file_path without the compression options
    """
    output_file = Path(output_file_path).name.split("?", 1)[0]
    folder_path = Path(output_file_path).parent.absolute()
    return str(folder_path / output_file)
