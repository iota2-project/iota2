#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provides the Iota2Constants dataclass."""
from dataclasses import dataclass


@dataclass(frozen=True)
class Iota2Constants:
    """Define some constant values for iota2 runs."""

    # general
    i2_tasks_status_filename: str = "IOTA2_tasks_status.txt"
    i2_builders_section: str = "builders"

    # classification builder
    re_encoding_label_name: str = "i2label"
    tile_origin_field: str = "tileorigin"
    re_encoding_label_file: str = "reference_data.shp"
    i2_database_ext: str = "nc"

    # obia builder
    i2_segmentation_field_name: str = "i2seg"

    # fill missing dates
    i2_missing_dates_no_data: float = 0.0
    i2_missing_dates_no_data_mask: float = 2.0

    # regression
    fake_class: str = "split"  # fake class used for sample selection

    # maximum number of column in a sqlite table
    sqlite_max_column: int = 2000


@dataclass(frozen=True)
class S1Constants:
    """Constant values used with S1 processing"""

    s1_window_radius: int = 2
    s1_ram_per_process: float = 5000.0
    s1_border_threshold: float = 1e-3
    s1_temporal_res: int = 10
    s1_interp_method: str = "linear"


@dataclass(frozen=True)
class NodeColors:
    """Hex codes for nodes' colors"""

    done_node_color = "#648FFF"
    unlaunchable_node_color = "#FFB000"
    fail_node_color = "#DC267F"
