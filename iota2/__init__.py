"""Iota2 init"""
import sys
from pathlib import Path

IOTA2DIR = str(Path(__file__).parent)
if IOTA2DIR not in sys.path:
    sys.path.append(IOTA2DIR)
