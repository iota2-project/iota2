#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Tests dedicated to launch iota2 runs."""
import logging
import os
import pickle as cp
import shutil
import sqlite3
import sys
from pathlib import Path

import numpy as np
import pandas as pd
import pytest
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import HuberRegressor
from sklearn.preprocessing import StandardScaler

import iota2.tests.utils.tests_utils_files as TUF
import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.classification.py_classifiers import reorder_features
from iota2.common import i2_constants as i2_const
from iota2.common.raster_utils import raster_to_array
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_launcher import run_cmd
from iota2.vector_tools.vector_functions import get_all_fields_in_shape

I2_CONST = i2_const.Iota2Constants()

IOTA2DIR = Path(os.environ.get("IOTA2DIR"))

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")

IOTA2_SCRIPTS = IOTA2DIR / "iota2"
sys.path.append(IOTA2_SCRIPTS)

LOGGER = logging.getLogger("distributed.worker")


def train_model(learning_samples_file, out_path, mode):
    """Train a scikit RF classifier and store it in a serializable object."""
    all_columns = get_all_fields_in_shape(learning_samples_file, driver_name="SQLite")

    # filter to get only the features
    all_feat = [name for name in all_columns if "sentinel2" in name]
    feat_reo = sorted(all_feat)

    conn = sqlite3.connect(learning_samples_file)

    # Define a model
    if mode == "classif":
        model = RandomForestClassifier(n_estimators=20, n_jobs=-10)
        df_labels = pd.read_sql_query("select i2label from output", conn).to_numpy()
    elif mode == "regression":
        model = HuberRegressor()
        df_labels = pd.read_sql_query("select code from output", conn).to_numpy()
    else:
        raise ValueError(
            f"Pretrained mode '{mode}' is invalid. Mode can only be either 'classif'"
            f" or 'regression'"
        )

    df_feat = pd.read_sql_query(f"select {','.join(feat_reo)} from output", conn)

    scaler = StandardScaler()
    scaler.fit(df_feat)

    model.fit(scaler.transform(df_feat), df_labels.ravel())

    object_to_save = {"model": model, "scaler": scaler, "features_order": feat_reo}
    with open(out_path + "/model.pickle", "wb") as out:
        cp.dump(object_to_save, out)
    print(out_path + "/model.pickle")


def inference_skrf(
    data_stack_ori,
    feat_labels,
    model,
):
    """Predict using scikit RF model."""
    with open(model, "rb") as in_file:
        # load from training mandatory information
        object_to_load = cp.load(in_file)
        model = object_to_load["model"]
        scaler = object_to_load["scaler"]
        labels_order = object_to_load["features_order"]
        # compute the expected feature order w.r.t iota2 pipeline
        new_feat_order = reorder_features(labels_order, feat_labels)
        # return None if order are the same
        if new_feat_order is not None:
            feat_stack = data_stack_ori[:, :, new_feat_order]
        else:
            feat_stack = data_stack_ori
        # the original array is an image (3D) but in sqlite the shape is 2D
        data_stack = np.reshape(
            feat_stack, (feat_stack.shape[0] * feat_stack.shape[1], feat_stack.shape[2])
        )
        # apply the scaler on reshaped data
        data_stack = scaler.transform(data_stack)
        # Then predict
        all_pred_labels = model.predict(data_stack)

        # Reshape the labels to an image shape (3D)
        # if a label encoder is used, apply it before reshaping
        all_pred_labels = np.reshape(
            all_pred_labels, (data_stack_ori.shape[0], data_stack_ori.shape[1], 1)
        )
    # Return only the labels, the confidence and probas are set to None
    return all_pred_labels, None, None


@pytest.mark.classification
class Iota2PreTrainedRunCases(AssertsFilesUtils):
    # pylint: disable=too-many-public-methods
    """Tests dedicated to launch iota2 runs."""

    # before launching tests
    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        # input data
        cls.config_ref = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_config.cfg"
        )
        cls.config_ref_keep = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_builder.cfg"
        )
        cls.config_ref_scikit = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_config_scikit.cfg"
        )
        cls.config_ref_scikit_regression = str(
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "i2_config_regression_scikit.cfg"
        )
        cls.ground_truth_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "ground_truth.shp"
        )
        cls.ground_truth_points_path = str(
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "ground_truth_points.shp"
        )
        cls.region_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "region.shp"
        )
        cls.nomenclature_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "nomenclature.txt"
        )
        cls.color_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "color.txt"
        )

    def test_i2_pretrained_classification(self, i2_tmpdir):
        """Tests iota2's pretrained classification model."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        data_generator = TUR.FakeS2DataGenerator(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        data_generator.generate_data()
        config_test = str(i2_tmpdir / "i2_config_generate_samples.cfg")
        shutil.copy(self.config_ref, config_test)

        TUF.copy_and_modify_config(
            self.config_ref,
            config_test,
            {
                "chain": {
                    "output_path": running_output_path,
                    "s2_path": fake_s2_theia_dir,
                    "data_field": "code",
                    "spatial_resolution": 10,
                    "list_tile": tile_name,
                    "ground_truth": self.ground_truth_path,
                    "nomenclature_path": self.nomenclature_path,
                    "color_table": self.color_path,
                    "first_step": "init",
                    "last_step": "sampling",
                },
                "python_data_managing": {
                    "number_of_chunks": 1,
                    "data_mode_access": "both",
                },
                "external_features": {"functions": "get_soi_s2"},
            },
        )

        # Launch the chain to generate samples
        cmd = f"Iota2.py -config {config_test} -scheduler_type debug"
        run_cmd(cmd)

        # Train custom model outside iota2
        path_to_samples = str(
            i2_tmpdir
            / "test_results"
            / "learningSamples"
            / "Samples_region_1_seed0_learn.sqlite"
        )
        train_model(
            path_to_samples, str(i2_tmpdir / "test_results" / "model"), mode="classif"
        )

        print("End of train")
        # Launch the chain to produce the classification with the model
        config_test = str(i2_tmpdir / "i2_config_pretrained_classification.cfg")
        TUF.copy_and_modify_config(
            self.config_ref,
            config_test,
            {
                "chain": {
                    "output_path": running_output_path,
                    "s2_path": fake_s2_theia_dir,
                    "data_field": "code",
                    "spatial_resolution": 10,
                    "list_tile": tile_name,
                    "ground_truth": self.ground_truth_path,
                    "nomenclature_path": self.nomenclature_path,
                    "color_table": self.color_path,
                },
                "pretrained_model": {
                    "module": __file__,
                    "function": "inference_skrf",
                    "model": str(i2_tmpdir / "test_results" / "model" / "model.pickle"),
                    "boundary_buffer": [100],
                    "mode": "classif",
                },
                "builders": {"builders_class_name": ["I2PreTrainedModel"]},
                "python_data_managing": {
                    "number_of_chunks": 1,
                    "data_mode_access": "both",
                },
                "external_features": {"functions": "get_soi_s2"},
            },
        )

        cmd = f"Iota2.py -config {config_test} -scheduler_type debug"
        run_cmd(cmd)

        # asserts
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=True,
            classif_color=True,
            confidence=False,
            pixel_val=True,
            diff_seed=True,
        )
        assert expected_files, details
        classif = raster_to_array(
            str(Path(running_output_path) / "final" / "Classif_Seed_0.tif")
        )
        print(np.sum(classif))
        assert np.sum(classif) > 0

    def test_i2_pretrained_regression(self, i2_tmpdir):
        """Tests iota2's pretrained classification model."""
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        data_generator = TUR.FakeS2DataGenerator(
            fake_s2_theia_dir, tile_name, ["20200101", "20200112", "20200127"], res=10.0
        )
        data_generator.generate_data()
        config_test = str(i2_tmpdir / "i2_config_generate_samples_regression.cfg")

        TUF.copy_and_modify_config(
            self.config_ref_scikit_regression,
            config_test,
            {
                "chain": {
                    "output_path": running_output_path,
                    "s2_path": fake_s2_theia_dir,
                    "data_field": "code",
                    "spatial_resolution": 10,
                    "list_tile": tile_name,
                    "ground_truth": self.ground_truth_points_path,
                    "first_step": "init",
                    "last_step": "sampling",
                    "nomenclature_path": self.nomenclature_path,
                    "color_table": self.color_path,
                    "remove_output_path": False,
                },
                "arg_train": {"runs": 2},
                "multi_run_fusion": {"merge_run": True},
                "python_data_managing": {
                    "number_of_chunks": 2,
                    "data_mode_access": "both",
                },
            },
        )
        # Launch the chain to generate samples
        cmd = f"Iota2.py -config {config_test} -scheduler_type debug"
        run_cmd(cmd)
        #
        # Train custom model outside iota2
        path_to_samples = str(
            i2_tmpdir
            / "test_results"
            / "learningSamples"
            / "Samples_region_1_seed0_learn.sqlite"
        )
        train_model(path_to_samples, str(i2_tmpdir), mode="regression")

        # Launch the chain to produce the regression with the model
        config_test = str(i2_tmpdir / "i2_config_pretrained_regression.cfg")

        TUF.copy_and_modify_config(
            self.config_ref_scikit_regression,
            config_test,
            {
                "chain": {
                    "output_path": running_output_path,
                    "s2_path": fake_s2_theia_dir,
                    "data_field": "code",
                    "spatial_resolution": 10,
                    "list_tile": tile_name,
                    "ground_truth": self.ground_truth_path,
                },
                "arg_train": {"runs": 2},
                "multi_run_fusion": {"merge_run": True},
                "python_data_managing": {"number_of_chunks": 2},
                "pretrained_model": {
                    "module": __file__,
                    "function": "inference_skrf",
                    "model": str(i2_tmpdir / "model.pickle"),
                    "boundary_buffer": [100],
                    "mode": "regression",
                },
                "builders": {"builders_class_name": ["I2PreTrainedModel"]},
            },
        )
        cmd = f"Iota2.py -config {config_test} -scheduler_type debug"
        run_cmd(cmd)

        self.assert_file_exist(
            Path(running_output_path)
            / "classif"
            / "Regression_T31TCJ_model_1_seed_1.tif"
        )
        # tiles are well mosaiced ?
        self.assert_file_exist(
            Path(running_output_path) / "final" / "Regression_Seed_0.tif"
        )
        self.assert_file_exist(
            Path(running_output_path) / "final" / "Regressions_fusion.tif"
        )
        # metrics are well created ?
        self.assert_file_exist(
            Path(running_output_path) / "final" / "T31TCJ_seed0_metrics.csv"
        )
