#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Integration tests for regression builder

"""

import logging
import shutil
from pathlib import Path

import numpy as np
import pytest
import xarray as xr

import iota2
import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.configuration_files import read_config_file as RCF
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_launcher import run_cmd

I2_CONST = iota2.common.i2_constants.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")


@pytest.mark.regression
class Iota2TestRegression(AssertsFilesUtils):
    """
    test iota2 regression builder
    """

    @classmethod
    def setup_class(cls):
        """prepare variable and data for tests
        initialize class variables
        creates test directory
        generates test data
        """

        cls.test_name = "TestRegression"
        cls.test_dir = IOTA2DIR / "data" / cls.test_name
        cls.tests_ok = []

        # reference config files
        cls.config_ref_pytorch = (
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "i2_config_regression_pytorch.cfg"
        )
        cls.config_ref_scikit = (
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "i2_config_regression_scikit.cfg"
        )
        cls.external_code = (
            IOTA2DIR / "data" / "references" / "running_iota2" / "external_code.py"
        )
        cls.external_module = (
            IOTA2DIR / "data" / "references" / "running_iota2" / "custom_nn.py"
        )

        # input data
        cls.template_pytorch = cls.test_dir / "template_pytorch.cfg"
        cls.template_scikit = cls.test_dir / "template_scikit.cfg"
        cls.pytorch_module = cls.test_dir / "custom.py"
        cls.ground_truth_path = (
            IOTA2DIR / "data" / "references" / "running_iota2" / "ground_truth.shp"
        )
        cls.ground_truth_points_path = (
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "ground_truth_points.shp"
        )
        cls.region_path = (
            IOTA2DIR / "data" / "references" / "running_iota2" / "region.shp"
        )
        cls.list_tile = ["T31TCJ", "T31TDJ"]
        cls.list_dates = ["20200101", "20200112", "20200127"]
        cls.s2_path = cls.test_dir / "s2_data"

        if not cls.test_dir.exists():
            print("generating test data")
            # makes test directory
            shutil.rmtree(cls.test_dir, ignore_errors=True)
            cls.test_dir.mkdir()

            # generates test data in class folder
            for tile in cls.list_tile:
                data_generator = TUR.FakeS2DataGenerator(
                    cls.s2_path,
                    tile,
                    cls.list_dates,
                    res=10.0,
                )
                data_generator.generate_data()

        # copies reference config and update with generated data path
        shutil.copy(cls.config_ref_pytorch, cls.template_pytorch)
        shutil.copy(cls.config_ref_scikit, cls.template_scikit)
        shutil.copy(cls.external_module, cls.pytorch_module)
        for template in [cls.template_pytorch, cls.template_scikit]:
            # read without validation
            config = RCF.ReadInternalConfigFile(template)
            chain = config.cfg_as_dict["chain"]  # chain section
            # set common variables
            chain["s2_path"] = str(cls.s2_path)
            chain["ground_truth"] = str(cls.ground_truth_path)
            chain["data_field"] = "code"
            chain["spatial_resolution"] = 10
            # chain["region_path"] = str(cls.region_path)
            # chain["region_field"] = "region"
            python_data_managing = config.cfg_as_dict["python_data_managing"]
            python_data_managing["number_of_chunks"] = 5

            config.save(template)

    def test_pytorch(self, i2_tmpdir: Path) -> None:
        """test pytorch regression workflow"""

        config_test = i2_tmpdir / "config.cfg"
        # copies template and modify it
        shutil.copy(self.template_pytorch, config_test)
        # get sections
        config = RCF.ReadInternalConfigFile(config_test)
        chain = config.cfg_as_dict["chain"]
        # set fields
        output_path = i2_tmpdir / "Output"
        chain["output_path"] = str(output_path)
        chain["list_tile"] = " ".join(self.list_tile)
        config.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20"
            " -scheduler_type debug"
        )
        run_cmd(cmd)

        # learning samples are well extracted ?
        self.assert_file_exist(
            output_path / "learningSamples" / "Samples_region_1_seed0_learn.nc"
        )
        # models are well trained ?
        self.assert_file_exist(output_path / "model" / "model_1_seed_0.txt")
        # predictions are well merged
        self.assert_file_exist(
            output_path / "classif" / "Regression_T31TCJ_model_1_seed_0.tif"
        )
        # tiles are well mosaiced ?
        self.assert_file_exist(output_path / "final" / "Regression_Seed_0.tif")
        # metrics are well created ?
        self.assert_file_exist(output_path / "final" / "T31TCJ_seed0_metrics.csv")
        self.assert_file_exist(output_path / "final" / "T31TCJ_seed0_metrics.csv")
        self.assert_file_exist(output_path / "final" / "mosaic_seed0_metrics.csv")

    def test_pytorch_external_features(self, i2_tmpdir: Path) -> None:
        """test external features with pytorch"""

        config_test = i2_tmpdir / "config.cfg"
        shutil.copy(self.template_pytorch, config_test)
        # get sections
        config = RCF.ReadInternalConfigFile(config_test)
        chain = config.cfg_as_dict["chain"]
        config.cfg_as_dict["external_features"] = {}
        exfeat = config.cfg_as_dict["external_features"]
        # set fields
        output_path = i2_tmpdir / "Output"
        chain["output_path"] = str(output_path)
        chain["list_tile"] = " ".join(self.list_tile)
        # exfeat["module"] = str(self.external_code)
        exfeat["functions"] = "get_soi_s2"

        config.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 "
            "-scheduler_type debug"
        )
        run_cmd(cmd)
        self.assert_file_exist(output_path / "final" / "Regression_Seed_0.tif")

    def test_scikit_external_features(self, i2_tmpdir: Path) -> None:
        """test external features with scikit learn"""

        config_test = i2_tmpdir / "config.cfg"
        shutil.copy(self.template_scikit, config_test)
        # get sections
        config = RCF.ReadInternalConfigFile(config_test)
        chain = config.cfg_as_dict["chain"]
        config.cfg_as_dict["external_features"] = {}
        exfeat = config.cfg_as_dict["external_features"]
        # set fields
        output_path = i2_tmpdir / "Output"
        chain["output_path"] = str(output_path)
        chain["list_tile"] = " ".join(self.list_tile)
        # exfeat["module"] = str(self.external_code)
        exfeat["functions"] = "get_soi_s2"

        config.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 "
            "-scheduler_type debug"
        )
        run_cmd(cmd)
        self.assert_file_exist(output_path / "final" / "Regression_Seed_0.tif")

    def test_scikit(self, i2_tmpdir: Path) -> None:
        """test scikit regression workflow"""
        output_path = i2_tmpdir / "Output"
        config_test = i2_tmpdir / "config.cfg"
        shutil.copy(self.template_scikit, config_test)
        config = RCF.ReadInternalConfigFile(config_test)
        chain = config.cfg_as_dict["chain"]
        chain["output_path"] = str(output_path)
        chain["list_tile"] = " ".join(self.list_tile)
        arg_train = config.cfg_as_dict["arg_train"]
        arg_train["runs"] = 1
        multi_run_fusion = config.cfg_as_dict["multi_run_fusion"]
        multi_run_fusion["merge_run"] = False
        # chain["region_path"] = None

        config.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 19 "
            "-scheduler_type debug"
        )
        run_cmd(cmd)

        # learning samples are well extracted
        # self.assert_file_exist(output / "learningSamples" / "Samples_region_1_seed0_learn.nc")
        # models are well trained
        # self.assert_file_exist(output / "model" / "model_1_seed_0.txt")
        # predictions are well merged
        self.assert_file_exist(
            output_path / "classif" / "Regression_T31TCJ_model_1_seed_0.tif"
        )
        # tiles are well mosaiced
        self.assert_file_exist(output_path / "final" / "Regression_Seed_0.tif")
        # metrics are well created
        self.assert_file_exist(output_path / "final" / "T31TCJ_seed0_metrics.csv")
        self.assert_file_exist(output_path / "final" / "mosaic_seed0_metrics.csv")

    def test_ground_truth_points(self, i2_tmpdir: Path) -> None:
        """test regression workflow with a file of points as ground truth"""
        output_path = i2_tmpdir / "Output"
        config_test = i2_tmpdir / "config.cfg"
        shutil.copy(self.template_scikit, config_test)
        config = RCF.ReadInternalConfigFile(config_test)
        chain = config.cfg_as_dict["chain"]
        chain["output_path"] = str(output_path)
        chain["ground_truth"] = str(self.ground_truth_points_path)
        chain["list_tile"] = " ".join(self.list_tile)
        arg_train = config.cfg_as_dict["arg_train"]
        arg_train["runs"] = 1
        multi_run_fusion = config.cfg_as_dict["multi_run_fusion"]
        multi_run_fusion["merge_run"] = False

        config.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 19 "
            "-scheduler_type debug"
        )
        run_cmd(cmd)

        # learning samples are well extracted
        # self.assert_file_exist(output / "learningSamples" / "Samples_region_1_seed0_learn.nc")
        # models are well trained
        # self.assert_file_exist(output / "model" / "model_1_seed_0.txt")
        # predictions are well merged
        self.assert_file_exist(
            output_path / "classif" / "Regression_T31TCJ_model_1_seed_0.tif"
        )
        # tiles are well mosaiced
        self.assert_file_exist(output_path / "final" / "Regression_Seed_0.tif")
        # metrics are well created
        self.assert_file_exist(output_path / "final" / "T31TCJ_seed0_metrics.csv")
        self.assert_file_exist(output_path / "final" / "mosaic_seed0_metrics.csv")

    def test_multi_run(self, i2_tmpdir: Path) -> None:
        """test scikit regression workflow"""
        output_path = i2_tmpdir / "Output"
        config_test = i2_tmpdir / "config.cfg"
        shutil.copy(self.template_scikit, config_test)
        config = RCF.ReadInternalConfigFile(config_test)
        chain = config.cfg_as_dict["chain"]
        chain["output_path"] = str(output_path)
        chain["list_tile"] = " ".join(self.list_tile)

        config.save(config_test)
        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 21 "
            "-scheduler_type debug"
        )
        run_cmd(cmd)

        # tiles are well mosaiced and fusionned
        self.assert_file_exist(output_path / "final" / "Regressions_fusion.tif")
        # a confidence map is built
        self.assert_file_exist(output_path / "final" / "Confidence.tif")
        # metrics are well created
        self.assert_file_exist(output_path / "final" / "fusion_metrics.csv")
        self.assert_file_exist(output_path / "final" / "T31TCJ_metrics.csv")
        self.assert_file_exist(output_path / "final" / "mosaic_metrics.csv")

    def test_external_pytorch_module(self, i2_tmpdir: Path) -> None:
        """test the usage of an external pytorch module in regression workflow"""
        output_path = i2_tmpdir / "Output"
        config_test = i2_tmpdir / "config.cfg"
        shutil.copy(self.template_pytorch, config_test)
        config = RCF.ReadInternalConfigFile(config_test)
        chain = config.cfg_as_dict["chain"]
        chain["output_path"] = str(output_path)
        chain["list_tile"] = " ".join(self.list_tile)
        deep_learning_parameters = config.cfg_as_dict["arg_train"][
            "deep_learning_parameters"
        ]
        deep_learning_parameters["dl_name"] = "CustomANN"
        deep_learning_parameters["dl_module"] = str(self.pytorch_module)

        config.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 16 "
            "-scheduler_type debug"
        )
        run_cmd(cmd)

        self.assert_file_exist(
            output_path / "classif" / "Regression_T31TCJ_model_1_seed_0_SUBREGION_0.tif"
        )
        self.assert_file_exist(output_path / "model" / "model_1_seed_0_hyp_0.txt")

    def test_data_aug_pytorch(self, i2_tmpdir: Path) -> None:
        """test the usage of data augmentation in regression workflow"""
        output_path = i2_tmpdir / "Output"
        config_test = i2_tmpdir / "config.cfg"
        shutil.copy(self.template_pytorch, config_test)
        config = RCF.ReadInternalConfigFile(config_test)
        chain = config.cfg_as_dict["chain"]
        chain["output_path"] = str(output_path)
        chain["list_tile"] = " ".join(self.list_tile)
        config.cfg_as_dict["arg_train"]["sample_augmentation"] = {
            "activate": True,
            "strategy": "jitter",
            "strategy.jitter.stdfactor": 10,
            "target_models": ["1"],
            "bins": [1, 2.5, 5],
            "samples.strategy": "balance",
        }
        config.cfg_as_dict["arg_train"]["ratio"] = 1

        config.save(config_test)

        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 16 "
            f"-scheduler_type debug"
        )
        run_cmd(cmd)

        learning_samples = xr.load_dataarray(
            output_path / "learningSamples" / "Samples_region_1_seed0_learn.nc"
        )

        _, counts = np.unique(
            learning_samples.sel(bands="code").to_numpy(), return_counts=True
        )

        assert (counts[0] + counts[1]) == (counts[2] + counts[3])

        self.assert_file_exist(output_path / "model" / "model_1_seed_0_hyp_0.txt")

    def test_2000_features(self, i2_tmpdir):
        """run using more than 2000 features"""

        config_test = i2_tmpdir / "config.cfg"
        shutil.copy(self.template_pytorch, config_test)
        # get sections
        config = RCF.ReadInternalConfigFile(config_test)
        chain = config.cfg_as_dict["chain"]
        config.cfg_as_dict["external_features"] = {}
        exfeat = config.cfg_as_dict["external_features"]
        # set fields
        output_path = i2_tmpdir / "Output"
        chain["output_path"] = str(output_path)
        chain["list_tile"] = " ".join(self.list_tile)
        exfeat["concat_mode"] = False
        config.cfg_as_dict["python_data_managing"]["data_mode_access"] = "raw"
        config.cfg_as_dict["python_data_managing"]["fill_missing_dates"] = True
        exfeat["functions"] = "get_raw_data gen_2000_features"

        config.cfg_as_dict["arg_train"]["learning_samples_extension"] = "csv"
        config.save(config_test)
        # Launch the chain
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 "
            "-scheduler_type debug"
        )
        run_cmd(cmd)
        self.assert_file_exist(output_path / "final" / "Regression_Seed_0.tif")
