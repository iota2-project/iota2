#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Tests dedicated to launch iota2 runs."""

from pathlib import Path

import rasterio as rio

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_files import copy_and_modify_config
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_launcher import run_cmd


class Iota2TestTiler(AssertsFilesUtils):
    """
    Tests dedicated to launch iota2 runs
    """

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_ref = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_config_tiler.cfg"
        )

    def test_raster_as_grid(self, i2_tmpdir: Path) -> None:
        """Test tiling builder with rasters as ref."""
        tile_name = "T31TCJ"
        res = 10
        feat_file_name = "feat.tif"
        feat_dir = i2_tmpdir / "feat_data"
        feat_file = str(Path(feat_dir) / feat_file_name)
        Path.mkdir(feat_dir)
        origin_x, origin_y = (566347.7, 6284062.4)
        TUR.array_to_raster(
            TUR.fun_array("iota2_binary"),
            feat_file,
            origin_x=origin_x + res * 20,
            origin_y=origin_y,
            pixel_size=res,
        )
        grid_dir = i2_tmpdir / "grid"
        Path.mkdir(grid_dir)
        tile_file = str(Path(grid_dir) / f"{tile_name}.tif")
        TUR.array_to_raster(
            TUR.fun_array("iota2_binary"),
            tile_file,
            origin_x=origin_x,
            origin_y=origin_y,
            pixel_size=res,
        )

        running_output_path = str(i2_tmpdir / "test_results")
        config_test = str(i2_tmpdir / "i2_config_tiler_raster_as_grid.cfg")
        copy_and_modify_config(
            self.config_ref,
            config_test,
            {
                "chain": {
                    "output_path": running_output_path,
                    "list_tile": tile_name,
                    "rasters_grid_path": str(grid_dir),
                    "spatial_resolution": 30,
                    "features_path": str(feat_dir),
                }
            },
        )

        final_grid = Path(running_output_path) / tile_name / feat_file_name
        if Path.exists(final_grid):
            Path.unlink(final_grid)
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 1"
            " -scheduler_type debug"
        )
        run_cmd(cmd)
        self.assert_file_exist(final_grid)
        with rio.open(final_grid, "r") as rio_file:
            (
                test_res,
                _,
                test_origin_x,
                _,
                _,
                test_origin_y,
                _,
                _,
                _,
            ) = rio_file.transform
            assert TUR.fun_array("iota2_binary").shape == (
                rio_file.height,
                rio_file.width,
            )
            assert res == test_res
            assert origin_x == test_origin_x
            assert origin_y == test_origin_y

    def test_vector_db_as_grid(self, i2_tmpdir: Path) -> None:
        """Test tiling builder with vector db as ref."""
        tiles = ["1", "2"]
        res = 10
        output_res = 10
        feat_file_name = "feat.tif"
        feat_dir = i2_tmpdir / "feat_data"
        Path.mkdir(feat_dir)
        origin_x, origin_y = (566347.7, 6284062.4)
        TUR.array_to_raster(
            TUR.fun_array("iota2_binary"),
            str(Path(feat_dir) / feat_file_name),
            origin_x=origin_x + res * 20,
            origin_y=origin_y,
            pixel_size=res,
        )

        running_output_path = str(i2_tmpdir / "test_results_grid_shp_as_db")
        config_test = str(i2_tmpdir / "i2_config_tiler_vector_as_grid.cfg")

        copy_and_modify_config(
            self.config_ref,
            config_test,
            {
                "chain": {
                    "output_path": running_output_path,
                    "list_tile": " ".join(tiles),
                    "spatial_resolution": output_res,
                    "grid": str(
                        IOTA2DIR
                        / "data"
                        / "references"
                        / "running_iota2"
                        / "region.shp"
                    ),
                    "tile_field": "region",
                    "features_path": str(feat_dir),
                }
            },
        )

        for tile_name in tiles:
            final_grid = Path(running_output_path) / tile_name / feat_file_name
            if Path.exists(final_grid):
                Path.unlink(final_grid)
        cmd = (
            f"Iota2.py -config {config_test} -starting_step 1 -ending_step 1"
            " -scheduler_type debug"
        )
        run_cmd(cmd)

        # asserts
        expected_transforms = [
            ((566370, 6284040), (19, 40)),
            ((566740, 6284040), (19, 50)),
        ]
        for tile_name, expected_transform in zip(tiles, expected_transforms):
            final_grid = Path(running_output_path) / tile_name / feat_file_name
            self.assert_file_exist(final_grid)
            with rio.open(final_grid, "r") as rio_file:
                (
                    test_res,
                    _,
                    test_origin_x,
                    _,
                    _,
                    test_origin_y,
                    _,
                    _,
                    _,
                ) = rio_file.transform
                assert expected_transform == (
                    (test_origin_x, test_origin_y),
                    (rio_file.height, rio_file.width),
                )
                assert output_res == test_res
