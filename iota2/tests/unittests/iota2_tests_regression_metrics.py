#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to check if metrics are well computed and stored as csv"""
import os
import shutil
from pathlib import Path

import pandas as pd

from iota2.typings.i2_types import RegressionMetricsParameters
from iota2.validation.regression_metrics import (
    compute_metrics,
    generate_metrics,
    generate_multi_tile_metrics,
    merge_metrics,
)

IOTA2DIR_AS_STR = os.environ.get("IOTA2DIR")
if not IOTA2DIR_AS_STR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR_AS_STR)


class Iota2TestRegressionMetrics:
    """check metrics are well computed and stored as csv"""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_data = IOTA2DIR / "data" / "references" / "Validation"
        cls.raster = IOTA2DIR / "data" / "references" / "Fusion" / "Input"

    def test_compute_metrics(self, i2_tmpdir: Path) -> None:
        """Test generate confusion matrix."""
        # Prepare data
        path_input = str(self.ref_data / "predict_output.sqlite")
        path_output = str(i2_tmpdir / "results_test_compute_metrics.csv")

        assert not Path(path_output).exists()

        compute_metrics(path_input, path_output, "ndvi")

        assert Path(path_output).exists()

        df_results = pd.read_csv(path_output)

        assert df_results["max_error"][0] == 3
        assert df_results["mean_absolute_error"][0] == 2
        assert df_results["mean_squared_error"][0] == 6
        assert df_results["median_absolute_error"][0] == 3
        assert round(df_results["r2_score"][0], 3) == -1.077

    def test_generate_metrics(self, i2_tmpdir: Path) -> None:
        """Test 'generate_metrics' method"""
        raster = str(Path(i2_tmpdir) / "T31TCJ_seed_0.tif")
        samples_poly = str(Path(i2_tmpdir) / "samples_poly.sqlite")
        samples_points = str(Path(i2_tmpdir) / "samples_points.sqlite")

        shutil.copy(str(self.raster / "T31TCJ_seed_0.tif"), raster)
        shutil.copy(str(self.ref_data / "samples_poly.sqlite"), samples_poly)
        shutil.copy(str(self.ref_data / "samples_points.sqlite"), samples_points)

        path_output_poly = str(i2_tmpdir / "results_test_generate_metrics_poly.csv")
        path_output_points = str(i2_tmpdir / "results_test_generate_metrics_points.csv")
        stats_poly = str(i2_tmpdir / "samples_poly.xml")
        stats_points = str(i2_tmpdir / "samples_points.xml")
        points_poly = str(i2_tmpdir / "samples_poly_points.sqlite")
        points_points = str(i2_tmpdir / "samples_points_points.sqlite")
        predicted_path_poly = str(
            Path(i2_tmpdir) / "samples_poly_predicted_T31TCJ_seed_0.sqlite"
        )
        predicted_path_points = str(
            Path(i2_tmpdir) / "samples_points_predicted_T31TCJ_seed_0.sqlite"
        )

        assert not Path(predicted_path_poly).exists()
        assert not Path(predicted_path_points).exists()
        assert not Path(predicted_path_poly).exists()
        assert not Path(predicted_path_points).exists()
        metrics_parameters = RegressionMetricsParameters(data_field="code")
        generate_metrics(
            raster, samples_poly, "split", path_output_poly, metrics_parameters
        )
        generate_metrics(
            raster, samples_points, "split", path_output_points, metrics_parameters
        )

        assert Path(stats_poly).exists()
        assert not Path(stats_points).exists()
        assert Path(points_poly).exists()
        assert not Path(points_points).exists()
        assert Path(predicted_path_poly).exists()
        assert Path(predicted_path_points).exists()
        assert Path(path_output_poly).exists()
        assert Path(path_output_points).exists()

    def test_generate_multi_tile_metrics(self, i2_tmpdir: Path) -> None:
        """Test 'generate_multi_tile_metrics' method"""
        raster = str(Path(i2_tmpdir) / "T31TCJ_seed_0.tif")
        sample_1 = str(Path(i2_tmpdir) / "samples_1_poly.sqlite")
        sample_2 = str(Path(i2_tmpdir) / "samples_2_poly.sqlite")

        shutil.copy(str(self.raster / "T31TCJ_seed_0.tif"), raster)
        shutil.copy(str(self.ref_data / "samples_1_poly.sqlite"), sample_1)
        shutil.copy(str(self.ref_data / "samples_2_poly.sqlite"), sample_2)

        path_output = str(i2_tmpdir / "results_test_generate_metrics.csv")
        predicted_path_1 = str(
            Path(i2_tmpdir) / "samples_1_poly_predicted_T31TCJ_seed_0.sqlite"
        )
        predicted_path_2 = str(
            Path(i2_tmpdir) / "samples_2_poly_predicted_T31TCJ_seed_0.sqlite"
        )

        assert not Path(path_output).exists()
        assert not Path(predicted_path_1).exists()
        assert not Path(predicted_path_2).exists()

        generate_multi_tile_metrics(
            raster,
            [sample_1, sample_2],
            "split",
            path_output,
            RegressionMetricsParameters(data_field="code"),
        )

        assert Path(path_output).exists()
        assert Path(predicted_path_1).exists()
        assert Path(predicted_path_2).exists()

    def test_merge_metrics(self, i2_tmpdir: Path) -> None:
        """Test merging metrics"""
        metrics_0 = str(Path(i2_tmpdir) / "final" / "T31TCJ_seed0_metrics.csv")
        metrics_1 = str(Path(i2_tmpdir) / "final" / "T31TCJ_seed1_metrics.csv")
        metrics_2 = str(Path(i2_tmpdir) / "final" / "T31TCJ_seed2_metrics.csv")

        (Path(i2_tmpdir) / "final").mkdir()

        shutil.copy(str(self.ref_data / "T31TCJ_seed0_metrics.csv"), metrics_0)
        shutil.copy(str(self.ref_data / "T31TCJ_seed1_metrics.csv"), metrics_1)
        shutil.copy(str(self.ref_data / "T31TCJ_seed2_metrics.csv"), metrics_2)
        tile_metrics = str(Path(i2_tmpdir) / "final" / "T31TCJ_metrics.csv")

        assert not Path(tile_metrics).exists()

        merge_metrics(str(Path(i2_tmpdir)), 3, "T31TCJ")

        assert Path(tile_metrics).exists()

        df_metrics = pd.read_csv(tile_metrics, index_col=0)

        assert df_metrics["max_error"][0] == 0.5
        assert df_metrics["mean_absolute_error"][0] == 0.2
        assert round(df_metrics["mean_squared_error"][0], 3) == 0.002
        assert df_metrics["median_absolute_error"][0] == 0.03
        assert df_metrics["r2_score"][0] == 0.96

        assert df_metrics["max_error"][3] == 0.5
        assert df_metrics["mean_absolute_error"][3] == 0.1
        assert round(df_metrics["mean_squared_error"][3], 3) == 0.021
        assert df_metrics["median_absolute_error"][3] == 0.02
        assert round(df_metrics["r2_score"][3], 3) == 0.967
