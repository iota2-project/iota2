#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test some numpy workflow in iota2 related to scikit ML."""
import pickle
import shutil
from functools import partial
from pathlib import Path

import numpy as np
from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestClassifier

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.common import raster_utils as rasterU
from iota2.common.file_utils import file_search_and
from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.common.raster_utils import (
    ChunkConfig,
    OtbPipelineCarrier,
    UserDefinedFunction,
    pipelines_chunking,
)
from iota2.learning.train_sklearn import (
    CrossValidationParameters,
    SciKitModelParams,
    sk_learn,
)
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_rasters import array_to_raster
from iota2.typings.i2_types import DBInfo


class Iota2TestsNumpyWorkflow(AssertsFilesUtils):
    """Test some numpy workflow in iota2 related to scikit ML."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.features_dataBase = str(IOTA2DIR / "data" / "reduced_output_samples.sqlite")
        cls.ref_cross_validation = ["Best Parameters:"]
        cls.ref_scale = np.array(
            [
                2.26379081,
                2.26379081,
                2.26379081,
                2.26379081,
                2.26379081,
                2.26379081,
                2.26379081,
                2.26379081,
                2.26379081,
                2.26379081,
                2.26379081,
                2.26379081,
                2.2770817,
                2.2770817,
                2.2770817,
                2.2770817,
                2.2770817,
                2.2770817,
                2.3113216,
                2.3113216,
                2.3113216,
                2.3113216,
                2.3113216,
                2.3113216,
                2.42691728,
                2.42691728,
                2.42691728,
                2.42691728,
                2.42691728,
                2.42691728,
                2.45312165,
                2.45312165,
                2.45312165,
                2.45312165,
                2.45312165,
                2.45312165,
                2.47527844,
                2.47527844,
                2.47527844,
                2.47527844,
                2.47527844,
                2.47527844,
                2.47947365,
                2.47947365,
                2.47947365,
                2.47947365,
                2.47947365,
                2.47947365,
                2.46829782,
                2.46829782,
                2.46829782,
                2.46829782,
                2.46829782,
                2.46829782,
                2.45930121,
                2.45930121,
                2.45930121,
                2.45930121,
                2.45930121,
                2.45930121,
                2.48103485,
                2.48103485,
                2.48103485,
                2.48103485,
                2.48103485,
                2.48103485,
                2.42499156,
                2.42499156,
                2.42499156,
                2.42499156,
                2.42499156,
                2.42499156,
                2.38322473,
                2.38322473,
                2.38322473,
                2.38322473,
                2.38322473,
                2.38322473,
                2.35705107,
                2.35705107,
                2.35705107,
                2.35705107,
                2.35705107,
                2.35705107,
                2.34456355,
                2.34456355,
                2.34456355,
                2.34456355,
                2.34456355,
                2.34456355,
                2.27672512,
                2.27672512,
                2.27672512,
                2.27672512,
                2.27672512,
                2.27672512,
                2.28776289,
                2.28776289,
                2.28776289,
                2.28776289,
                2.28776289,
                2.28776289,
                2.28240157,
                2.28240157,
                2.28240157,
                2.28240157,
                2.28240157,
                2.28240157,
                2.34223592,
                2.34223592,
                2.34223592,
                2.34223592,
                2.34223592,
                2.34223592,
                2.33568571,
                2.33568571,
                2.33568571,
                2.33568571,
                2.33568571,
                2.33568571,
                2.30312608,
                2.30312608,
                2.30312608,
                2.30312608,
                2.30312608,
                2.30312608,
                2.24433067,
                2.24433067,
                2.24433067,
                2.24433067,
                2.24433067,
                2.24433067,
                2.16757937,
                2.16757937,
                2.16757937,
                2.16757937,
                2.16757937,
                2.16757937,
            ]
        )
        cls.ref_var = np.array(
            [
                5.12474884,
                5.12474884,
                5.12474884,
                5.12474884,
                5.12474884,
                5.12474884,
                5.12474884,
                5.12474884,
                5.12474884,
                5.12474884,
                5.12474884,
                5.12474884,
                5.18510106,
                5.18510106,
                5.18510106,
                5.18510106,
                5.18510106,
                5.18510106,
                5.34220754,
                5.34220754,
                5.34220754,
                5.34220754,
                5.34220754,
                5.34220754,
                5.88992751,
                5.88992751,
                5.88992751,
                5.88992751,
                5.88992751,
                5.88992751,
                6.01780582,
                6.01780582,
                6.01780582,
                6.01780582,
                6.01780582,
                6.01780582,
                6.12700335,
                6.12700335,
                6.12700335,
                6.12700335,
                6.12700335,
                6.12700335,
                6.14778957,
                6.14778957,
                6.14778957,
                6.14778957,
                6.14778957,
                6.14778957,
                6.09249414,
                6.09249414,
                6.09249414,
                6.09249414,
                6.09249414,
                6.09249414,
                6.04816243,
                6.04816243,
                6.04816243,
                6.04816243,
                6.04816243,
                6.04816243,
                6.15553393,
                6.15553393,
                6.15553393,
                6.15553393,
                6.15553393,
                6.15553393,
                5.88058406,
                5.88058406,
                5.88058406,
                5.88058406,
                5.88058406,
                5.88058406,
                5.67976011,
                5.67976011,
                5.67976011,
                5.67976011,
                5.67976011,
                5.67976011,
                5.55568973,
                5.55568973,
                5.55568973,
                5.55568973,
                5.55568973,
                5.55568973,
                5.49697822,
                5.49697822,
                5.49697822,
                5.49697822,
                5.49697822,
                5.49697822,
                5.18347728,
                5.18347728,
                5.18347728,
                5.18347728,
                5.18347728,
                5.18347728,
                5.23385904,
                5.23385904,
                5.23385904,
                5.23385904,
                5.23385904,
                5.23385904,
                5.20935694,
                5.20935694,
                5.20935694,
                5.20935694,
                5.20935694,
                5.20935694,
                5.48606908,
                5.48606908,
                5.48606908,
                5.48606908,
                5.48606908,
                5.48606908,
                5.45542772,
                5.45542772,
                5.45542772,
                5.45542772,
                5.45542772,
                5.45542772,
                5.30438975,
                5.30438975,
                5.30438975,
                5.30438975,
                5.30438975,
                5.30438975,
                5.03702016,
                5.03702016,
                5.03702016,
                5.03702016,
                5.03702016,
                5.03702016,
                4.69840032,
                4.69840032,
                4.69840032,
                4.69840032,
                4.69840032,
                4.69840032,
            ]
        )

    # Tests definitions
    def test_apply_function(self, i2_tmpdir: Path) -> None:
        """TEST : check the whole workflow."""

        def custom_features(array):
            return array + array, []

        # First create some dummy data on disk
        dummy_raster_path = str(i2_tmpdir / "DUMMY.tif")
        array_to_rasterize = TUR.fun_array("iota2_binary")
        array_to_raster(array_to_rasterize, dummy_raster_path)

        # Get it in a otbApplication (simulating full iota2 features pipeline)
        band_math, _ = create_application(
            AvailableOTBApp.BAND_MATH_X,
            {"il": [dummy_raster_path], "exp": "im1b1;im1b1"},
        )
        band_math.Execute()

        # Then apply function
        function_partial = partial(custom_features)
        fake_pipeline = OtbPipelineCarrier(band_math, band_math, band_math)
        otb_pipelines = pipelines_chunking(
            fake_pipeline,
            chunk_config=ChunkConfig(
                chunk_size_mode="user_fixed",
                number_of_chunks=None,
                chunk_size_x=5,
                chunk_size_y=5,
            ),
        )

        (data, _,) = rasterU.apply_udf_to_pipeline(
            otb_pipelines_list=otb_pipelines,
            function=UserDefinedFunction(function_partial),
        )
        test_array = data.data
        new_labels = data.labels
        # asserts

        # check array' shape consistency
        # concerning np.array there is different convention between OTB(GDAL?)
        # and rasterio
        # OTB : [row, cols, bands]
        # rasterio : [bands, row, cols]
        band_math.Execute()
        pipeline_shape = band_math.GetVectorImageAsNumpyArray("out").shape
        pipeline_shape = (pipeline_shape[2], pipeline_shape[0], pipeline_shape[1])
        assert pipeline_shape == test_array.shape

        # check if the input function is well applied
        pipeline_array = band_math.GetVectorImageAsNumpyArray("out")
        ref_array, _ = custom_features(pipeline_array)
        assert np.allclose(np.moveaxis(ref_array, -1, 0), test_array)
        assert new_labels is not None

    def test_apply_function_padding(self, i2_tmpdir: Path) -> None:
        """
        TEST : check the whole workflow
        """

        padding_size_x, padding_size_y = 2, 1

        # The padding is used for contextual approach.
        # In this case 2 chunks overlap each other (on the padding) and we need
        # to check these 2 chunks results are well merged
        # So here we need a test function which returns a value calculated with
        # the context of the pixel, otherwise the 2 chunks results would be
        # identical and the test irrelevant
        def custom_features(array):
            result = np.copy(array)
            for x in range(padding_size_x, array.shape[1] - padding_size_x):
                patch_boundaries_x = (x - padding_size_x, x + padding_size_x)
                for y in range(padding_size_y, array.shape[0] - padding_size_y):
                    patch_boundaries_y = (y - padding_size_y, y + padding_size_y)
                    result[y, x, :] = np.mean(
                        array[
                            patch_boundaries_y[0] : patch_boundaries_y[1],
                            patch_boundaries_x[0] : patch_boundaries_x[1],
                            :,
                        ]
                    )
            return result, []

        # First create some dummy data on disk
        dummy_raster_path = str(i2_tmpdir / "DUMMY.tif")
        array_to_rasterize = TUR.fun_array("iota2_binary")
        array_to_raster(array_to_rasterize, dummy_raster_path)

        # Get it in a otbApplication (simulating full iota2 features pipeline)
        band_math, _ = create_application(
            AvailableOTBApp.BAND_MATH_X,
            {"il": [dummy_raster_path], "exp": "im1b1;im1b1"},
        )
        band_math.Execute()
        fake_pipelines = OtbPipelineCarrier(band_math, band_math, band_math)
        otb_pipeline_chunks = pipelines_chunking(
            fake_pipelines,
            ChunkConfig(
                chunk_size_mode="user_fixed",
                number_of_chunks=None,
                chunk_size_x=5,
                chunk_size_y=5,
                padding_size_x=padding_size_x,
                padding_size_y=padding_size_y,
            ),
        )
        # Then apply function
        function_partial = partial(custom_features)
        (data, _) = rasterU.apply_udf_to_pipeline(
            otb_pipelines_list=otb_pipeline_chunks,
            function=UserDefinedFunction(function_partial),
        )
        test_array = data.data
        new_labels = data.labels
        band_math.Execute()
        pipeline_shape = band_math.GetVectorImageAsNumpyArray("out").shape
        pipeline_shape = (pipeline_shape[2], pipeline_shape[0], pipeline_shape[1])
        assert pipeline_shape == test_array.shape

        # check if the input function is well applied
        pipeline_array = band_math.GetVectorImageAsNumpyArray("out")
        ref_array, _ = custom_features(pipeline_array)
        assert np.allclose(np.moveaxis(ref_array, -1, 0), test_array)
        assert new_labels is not None

        # test "targeted_chunk" behaviour
        number_of_chunks = 5

        def get_mosaic(targeted_chunk):
            otb_pipeline_chunks = pipelines_chunking(
                fake_pipelines,
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=number_of_chunks,
                    chunk_size_x=5,
                    chunk_size_y=5,
                    padding_size_x=padding_size_x,
                    padding_size_y=padding_size_y,
                ),
                targeted_chunk=targeted_chunk,
            )
            (data, _) = rasterU.apply_udf_to_pipeline(
                otb_pipelines_list=otb_pipeline_chunks,
                function=UserDefinedFunction(function_partial),
            )
            return data.data

        test_array = get_mosaic(targeted_chunk=0)
        assert (pipeline_shape[0], 3, pipeline_shape[2]) == test_array.shape

        test_array = get_mosaic(targeted_chunk=number_of_chunks - 1)
        assert (pipeline_shape[0], 4, pipeline_shape[2]) == test_array.shape

    def test_sk_cross_validation(self, i2_tmpdir: Path) -> None:
        """Test cross validation."""
        db_file_name = Path(self.features_dataBase).name

        features_db_test = str(i2_tmpdir / db_file_name)
        shutil.copy(self.features_dataBase, features_db_test)

        test_model_path = str(i2_tmpdir / "test_model.rf")
        scikit_parameters = SciKitModelParams(
            DBInfo(
                "code",
                features_db_test,
                features_fields=[f"reduced_{cpt}" for cpt in range(138)],
            ),
            model_name="RandomForestClassifier",
            model_kwargs={"min_samples_split": 25},
            output_model=test_model_path,
        )
        sk_learn(
            scikit_parameters,
            False,
            cross_validation_params=CrossValidationParameters(
                parameters={"n_estimators": [50, 100]}
            ),
        )

        # asserts
        self.assert_file_exist(Path(test_model_path))
        test_cross_val_results = file_search_and(
            str(i2_tmpdir), True, "cross_val_param.cv"
        )[0]
        with open(test_cross_val_results, encoding="UTF-8") as cross_val_f:
            test_cross_val = [line.rstrip() for line in cross_val_f]

        test_cv_val = all(
            val_to_find in test_cross_val for val_to_find in self.ref_cross_validation
        )
        assert test_cv_val, "cross validation failed"

    def test_sk_standardization(self, i2_tmpdir: Path) -> None:
        """test standardization"""

        db_file_name = Path(self.features_dataBase).name

        features_db_test = str(i2_tmpdir / db_file_name)
        shutil.copy(self.features_dataBase, features_db_test)

        test_model_path = str(i2_tmpdir / "test_model.rf")

        scikit_parameters = SciKitModelParams(
            DBInfo(
                "code",
                features_db_test,
                features_fields=[f"reduced_{cpt}" for cpt in range(138)],
            ),
            model_name="RandomForestClassifier",
            output_model=test_model_path,
        )

        sk_learn(
            scikit_parameters,
            apply_standardization=True,
        )

        self.assert_file_exist(Path(test_model_path))

        with open(test_model_path, "rb") as model_obj:
            _, scaler_features, _ = pickle.load(model_obj)

        assert np.allclose(self.ref_scale, scaler_features.scale_)
        assert np.allclose(self.ref_var, scaler_features.var_)

    def test_machine_learning(self, i2_tmpdir: Path) -> None:
        """Use sci-kit learn machine learning algorithm."""

        def do_predict(array, model, dtype="int32"):
            def wrapper(*args, **kwargs):  # pylint: disable=unused-argument
                return model.predict([args[0]])

            predicted_array = np.apply_along_axis(func1d=wrapper, axis=-1, arr=array)
            return predicted_array.astype(dtype), []

        # build data to learn RF model
        X, y = make_classification(
            n_samples=16 * 86,
            n_features=2,
            n_informative=2,
            n_redundant=0,
            random_state=0,
            shuffle=True,
        )
        # learning
        clf = RandomForestClassifier(n_estimators=100, max_depth=2, random_state=0)
        clf.fit(X, y)
        subsamples = np.squeeze(X.reshape(16, 86, 2)[:, :, 0])
        # create some data on disk in order to predict them
        dummy_raster_path = str(i2_tmpdir / "DUMMY.tif")
        array_to_raster(subsamples, dummy_raster_path, output_format="float")
        labels = np.unique(y)

        # Get it in a otbApplication (simulating full iota2 features pipeline)
        band_math, _ = create_application(
            AvailableOTBApp.BAND_MATH_X,
            {"il": [dummy_raster_path], "exp": "im1b1;im1b1"},
        )
        fake_pipelines = OtbPipelineCarrier(band_math, band_math, band_math)
        otb_pipelines_chunks = pipelines_chunking(
            fake_pipelines,
            ChunkConfig(
                chunk_size_mode="user_fixed",
                number_of_chunks=None,
                chunk_size_x=5,
                chunk_size_y=5,
            ),
        )
        # prediction
        function_partial = partial(do_predict, model=clf)
        (data, _) = rasterU.apply_udf_to_pipeline(
            otb_pipelines_list=otb_pipelines_chunks,
            function=UserDefinedFunction(function_partial),
        )
        inference_labels = np.unique(data.data)

        # asserts
        assert data.data.shape == (1, 16, 86)
        assert all(ref_lab in inference_labels for ref_lab in labels)
