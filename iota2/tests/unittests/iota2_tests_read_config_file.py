#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test different config file reading"""
import shutil
from pathlib import Path

import pytest
from pydantic import ValidationError

from iota2.configuration_files import read_config_file as rcf
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestReadConfigFile:
    """Class to test different config file reading"""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.configuration_file = str(
            IOTA2DIR / "config" / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        cls.cfg_file_bad1 = str(
            IOTA2DIR / "data" / "config" / "test_bad_config_boolean.cfg"
        )
        cls.cfg_file_bad2 = str(
            IOTA2DIR / "data" / "config" / "test_bad_config_int.cfg"
        )
        cls.cfg_file_bad3 = str(
            IOTA2DIR / "data" / "config" / "test_bad_config_resolution.cfg"
        )
        cls.test_working_directory = None

        cls.config_ref = str(
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "i2_config_feat_map.cfg"
        )

    def test_init_config_file(self, i2_tmpdir: Path) -> None:
        """Check setParam feature."""
        # the class is instantiated with self.configuration_file config file
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        shutil.copy(self.configuration_file, config_path_test)
        cfg_test = rcf.ReadInternalConfigFile(config_path_test)
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["l8_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["region_path"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.save(config_path_test)

        cfg = rcf.ReadConfigFile(config_path_test)
        cfg.set_param("arg_train", "runs", 2)
        cfg.set_param(
            "chain",
            "region_path",
            str(IOTA2DIR / "data" / "references" / "region_need_To_env.shp"),
        )
        cfg.set_param("chain", "region_field", "DN_char")
        cfg.set_param("arg_classification", "classif_mode", "separate")

        # we get output_path variable
        assert cfg.get_param("chain", "output_path") == "../../../data/tmp/"

        # we check if bad section is detected
        pytest.raises(Exception, cfg.get_param, "BADchain", "output_path")

        # we check if bad param is detected
        pytest.raises(Exception, cfg.get_param, "chain", "BADoutput_path")

    def test_init_config_file_bad1(self, i2_tmpdir: Path) -> None:
        """Parameter can't be converted as a boolean."""
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        shutil.copy(self.cfg_file_bad1, config_path_test)
        cfg_test = rcf.ReadInternalConfigFile(config_path_test)
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["data_field"] = "whatever"
        cfg_test.save(config_path_test)

        pytest.raises(ValueError, rcf.ReadConfigFile, config_path_test)

    def test_init_config_file_bad2(self, i2_tmpdir: Path) -> None:
        """Parameter can't be converted as an integer."""
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        shutil.copy(self.cfg_file_bad2, config_path_test)
        cfg_test = rcf.ReadInternalConfigFile(config_path_test)
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["data_field"] = "whatever"
        cfg_test.save(config_path_test)

        pytest.raises(ValueError, rcf.ReadConfigFile, config_path_test)

    def test_init_config_file_bad3(self, i2_tmpdir: Path) -> None:
        """Resolution parameter bad formatted."""
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        shutil.copy(self.cfg_file_bad3, config_path_test)
        cfg_test = rcf.ReadInternalConfigFile(config_path_test)
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["data_field"] = "whatever"
        cfg_test.save(config_path_test)

        pytest.raises(ValueError, rcf.ReadConfigFile, self.cfg_file_bad3)

    def test_config_builders_compatibility(self, i2_tmpdir: Path) -> None:
        """Check builders compatibilty."""

        # same builder twice
        config_test_vecto_vecto = str(i2_tmpdir / "i2_config_vecto_vecto.cfg")
        shutil.copy(self.config_ref, config_test_vecto_vecto)
        cfg_test = rcf.ReadInternalConfigFile(config_test_vecto_vecto)
        cfg_test.cfg_as_dict["builders"]["builders_class_name"] = [
            "I2Classification",
            "I2Classification",
        ]
        cfg_test.save(config_test_vecto_vecto)
        pytest.raises(ValueError, rcf.ReadConfigFile, config_test_vecto_vecto)

        # features map and classification
        config_test_featuresmap_classif = str(i2_tmpdir / "i2_config_feat_classif.cfg")
        shutil.copy(self.config_ref, config_test_featuresmap_classif)
        cfg_test = rcf.ReadInternalConfigFile(config_test_featuresmap_classif)
        cfg_test.cfg_as_dict["builders"]["builders_class_name"] = [
            "I2FeaturesMap",
            "I2Classification",
        ]
        cfg_test.save(config_test_featuresmap_classif)
        pytest.raises(ValueError, rcf.ReadConfigFile, config_test_featuresmap_classif)

    def test_external_features_kwargs(self, i2_tmpdir: Path) -> None:
        """Checks the conversion of external features with keyword arguments."""
        # generate config file
        config_test = str(i2_tmpdir / "i2_config_external_feature_with_kwargs.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.ReadInternalConfigFile(config_test)

        cfg_test.cfg_as_dict["chain"]["s2_path"] = config_test
        cfg_test.cfg_as_dict["external_features"]["module"] = None
        cfg_test.cfg_as_dict["external_features"]["functions"] = [
            ["test_get_band", {"label": "red", "band": 2}]
        ]
        cfg_test.save(config_test)
        # read it
        config = rcf.ReadConfigFile(config_test)
        converted_functions = config.get_param("external_features", "functions")
        assert converted_functions == [("test_get_band", {"label": "red", "band": 2})]

    def test_external_features_kwargs_and_function_name(self, i2_tmpdir):
        """Checks the conversion of external features with keyword argument."""
        # generate config file
        config_test = str(
            i2_tmpdir / "i2_config_external_feature_with_kwargs_and_function_name.cfg"
        )
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.ReadInternalConfigFile(config_test)
        cfg_test.cfg_as_dict["chain"]["s2_path"] = config_test
        cfg_test.cfg_as_dict["external_features"]["module"] = None
        cfg_test.cfg_as_dict["external_features"]["functions"] = [
            "test_get_band",
            ["test_get_band", {"label": "red", "band": 2}],
        ]
        cfg_test.save(config_test)
        # read it
        config = rcf.ReadConfigFile(config_test)
        converted_functions = config.get_param("external_features", "functions")
        assert converted_functions == [
            ("test_get_band", {}),
            ("test_get_band", {"label": "red", "band": 2}),
        ]

    def test_external_features_no_kwargs(self, i2_tmpdir: Path) -> None:
        """Checks the conversion of external features without keyword arguments."""
        # generate config file
        config_test = str(i2_tmpdir / "i2_config_external_feature_no_kwargs.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.ReadInternalConfigFile(config_test)
        cfg_test.cfg_as_dict["chain"]["s2_path"] = config_test
        cfg_test.cfg_as_dict["external_features"]["module"] = None
        cfg_test.cfg_as_dict["external_features"]["functions"] = "test_get_band"
        cfg_test.save(config_test)
        # read it
        config = rcf.ReadConfigFile(config_test)
        converted_functions = config.get_param("external_features", "functions")
        assert converted_functions == [("test_get_band", {})]

    def test_external_features_no_kwargs_other(self, i2_tmpdir: Path) -> None:
        """Checks the conversion of external features without keyword arguments, other format."""
        # generate config file
        config_test = str(i2_tmpdir / "i2_config_external_feature_no_kwargs_other.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.ReadInternalConfigFile(config_test)
        cfg_test.cfg_as_dict["chain"]["s2_path"] = config_test
        cfg_test.cfg_as_dict["external_features"]["module"] = None
        cfg_test.cfg_as_dict["external_features"]["functions"] = [["test_get_band"]]
        cfg_test.save(config_test)
        # read it
        config = rcf.ReadConfigFile(config_test)
        converted_functions = config.get_param("external_features", "functions")
        assert converted_functions == [("test_get_band", {})]

    def test_external_features_nonexistant(self, i2_tmpdir: Path) -> None:
        """Checks read config file for nonexistant external feature."""
        # generate config file
        config_test = str(i2_tmpdir / "i2_config_external_feature_nonexistant.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.ReadInternalConfigFile(config_test)
        cfg_test.cfg_as_dict["chain"]["s2_path"] = config_test
        cfg_test.cfg_as_dict["external_features"]["module"] = None
        cfg_test.cfg_as_dict["external_features"]["functions"] = "test_get_baand"
        cfg_test.save(config_test)
        # read it
        pytest.raises(AttributeError, rcf.ReadConfigFile, config_test)

    def test_external_features_corrupted_kwargs(self, i2_tmpdir: Path) -> None:
        """Checks the conversion of external features with invalid keyword arguments."""
        # generate config file
        config_test = str(i2_tmpdir / "i2_config_external_feature_corrupted_kwargs.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.ReadInternalConfigFile(config_test)
        cfg_test.cfg_as_dict["chain"]["s2_path"] = config_test
        cfg_test.cfg_as_dict["external_features"]["module"] = None
        cfg_test.cfg_as_dict["external_features"]["functions"] = [
            {"function": "test_get_band", "label": "red", "band": 2}
        ]
        cfg_test.save(config_test)
        pytest.raises(ValueError, rcf.ReadConfigFile, config_test)

    def test_external_features_invalid_type_kwargs(self, i2_tmpdir):
        """Checks the conversion of external features with invalid type keyword arguments."""
        # generate config file
        config_test = str(
            i2_tmpdir / "i2_config_external_feature_invalid_type_kwargs.cfg"
        )
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.ReadInternalConfigFile(config_test)
        cfg_test.cfg_as_dict["chain"]["s2_path"] = config_test
        cfg_test.cfg_as_dict["external_features"]["module"] = None
        cfg_test.cfg_as_dict["external_features"]["functions"] = [
            ["test_get_band", {"label": "red", "band": "red"}]
        ]
        cfg_test.save(config_test)

        pytest.raises(ValidationError, rcf.ReadConfigFile, config_test)
