# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
tests I2TemporalLabel, I2Label, i2_label_factory usage.
"""
import pytest
from pydantic import ValidationError

from iota2.learning.utils import I2Label, I2TemporalLabel, i2_label_factory


@pytest.mark.parametrize(
    "label, expect_type",
    [("sentinelx_feat_20210101", I2TemporalLabel), ("dem_slope", I2Label)],
)
def test_i2labelfactory(
    label: str, expect_type: type[I2TemporalLabel] | type[I2Label]
) -> None:
    """test i2_label_factory nominal behavior."""
    assert isinstance(i2_label_factory(label), expect_type)


@pytest.mark.parametrize(
    "label, exception",
    [("badnaming", ValueError), ("sentinelx_feat_20210231", ValidationError)],
)
def test_i2labelfactory_raise(label: str, exception: type[Exception]) -> None:
    """test i2_label_factory raise behavior."""
    with pytest.raises(exception):
        i2_label_factory(label)


@pytest.mark.parametrize(
    "label, label_class",
    [
        (
            {"sensor_name": "sentinelx", "feat_name": "feat", "date": "20210101"},
            I2TemporalLabel,
        ),
        ({"sensor_name": "dem", "feat_name": "slope"}, I2Label),
    ],
)
def test_labels(
    label: dict, label_class: type[I2TemporalLabel] | type[I2Label]
) -> None:
    """test I2Label nominal behavior."""
    assert label_class(**label)


@pytest.mark.parametrize(
    "label, exception",
    [
        (
            {"sensor_name": "sentinel_x", "feat_name": "feat", "date": "20210101"},
            ValidationError,
        ),
        (
            {"sensor_name": "sentinelx", "feat_name": "feat_1", "date": "20210101"},
            ValidationError,
        ),
    ],
)
def test_labels_raise(label: dict, exception: type[Exception]) -> None:
    """test i2_label_factory raise behavior."""
    with pytest.raises(exception):
        I2TemporalLabel(**label)
