#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test external features workflow."""
import configparser
import shutil
from functools import partial
from pathlib import Path

import numpy as np
import otbApplication as otb
import pytest
from osgeo import gdal
from scipy import ndimage

import iota2.tests.utils.tests_utils_rasters as TUR
from config import Config
from iota2.common import iota2_directory
from iota2.common import raster_utils as rasterU
from iota2.common.custom_numpy_features import (
    ChunkParameters,
    CustomFeaturesConfiguration,
    CustomNumpyFeatures,
    DataContainer,
    MaskingRaster,
    compute_custom_features,
    exogenous_data_tile,
)
from iota2.common.file_utils import report_band_description
from iota2.common.generate_features import FeaturesMapParameters, generate_features
from iota2.common.i2_constants import Iota2Constants
from iota2.common.raster_utils import (
    ChunkConfig,
    PipelinesData,
    UserDefinedFunction,
    extract_raster_metadata_from_otb,
    turn_off_otb_pipelines,
)
from iota2.common.tools.split_raster_into_tiles import reproject
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.sections.cfg_utils import ConfigError
from iota2.learning.utils import I2TemporalLabel
from iota2.sensors.sensorscontainer import SensorsContainer
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_files import copy_and_modify_config
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_launcher import get_large_i2_data_test
from iota2.typings.i2_types import EmptyList

I2_CONST = Iota2Constants()


@pytest.mark.external_features
class Iota2TestCustomNumpyFeatures(AssertsFilesUtils):
    """Check external features workflow."""

    @classmethod
    def setup_class(cls) -> None:
        """Variables shared between tests."""
        # Download i2 large data set
        get_large_i2_data_test()

        cls.config_test = str(
            IOTA2DIR / "data" / "numpy_features" / "config_plugins.cfg"
        )
        cls.sar_config_test = str(
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "large_i2_data_test"
            / "s1_data"
            / "i2_config_sar.cfg"
        )
        cls.srtm_db = str(
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "large_i2_data_test"
            / "s1_data"
            / "srtm.shp"
        )
        cls.tiles_grid_db = str(
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "large_i2_data_test"
            / "s1_data"
            / "Features.shp"
        )
        cls.srtm_dir = str(
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "large_i2_data_test"
            / "s1_data"
            / "SRTM"
        )
        cls.geoid_file = str(
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "large_i2_data_test"
            / "s1_data"
            / "egm96.grd"
        )
        cls.acquisitions = str(
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "large_i2_data_test"
            / "s1_data"
            / "acquisitions"
        )

    @pytest.mark.config
    def test_check_custom_features_valid_inputs(self, i2_tmpdir: Path) -> None:
        """Test the behaviour of check custom features."""
        s2st_data = str(i2_tmpdir)
        test_path = str(i2_tmpdir / "RUN")
        config_path_test = str(i2_tmpdir / "Config_TEST_valid.cfg")

        shutil.copy(self.config_test, config_path_test)
        cfg_test = Config(config_path_test).as_dict()
        cfg_test["chain"]["output_path"] = test_path

        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = s2st_data
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"]["use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["external_features"]["functions"] = "get_identity get_ndvi"

        with open(config_path_test, "w", encoding="UTF-8") as file_cfg:
            file_cfg.write(str(cfg_test))

        cfg = rcf.ReadConfigFile(config_path_test)
        assert cfg.get_param("external_features", "external_features_flag")
        del cfg

    @pytest.mark.config
    def test_check_invalid_module_name(self, i2_tmpdir: Path) -> None:
        """Test the behaviour of check custom features."""
        s2st_data = str(i2_tmpdir)
        test_path = str(i2_tmpdir / "RUN")

        # Invalid module name
        config_path_test = str(i2_tmpdir / "Config_TEST_invalidmodule.cfg")

        shutil.copy(self.config_test, config_path_test)
        cfg_test = Config(config_path_test).as_dict()
        cfg_test["chain"]["output_path"] = test_path

        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = s2st_data
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"]["use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["external_features"]["module"] = str(
            Path(IOTA2DIR) / "data" / "numpy_features" / "dummy.py"
        )

        cfg_test["external_features"]["functions"] = "get_identity get_ndvi"
        with open(config_path_test, "w", encoding="UTF-8") as file_cfg:
            file_cfg.write(str(cfg_test))
        pytest.raises(ConfigError, rcf.ReadConfigFile, config_path_test)

    @pytest.mark.config
    def test_check_invalid_function_name(self, i2_tmpdir: Path) -> None:
        """Test the behaviour of check custom features."""
        s2st_data = str(i2_tmpdir)
        test_path = str(i2_tmpdir / "RUN")

        # Invalid function name
        config_path_test = str(i2_tmpdir / "Config_TEST_invalid_functions.cfg")

        shutil.copy(self.config_test, config_path_test)
        cfg_test = Config(config_path_test).as_dict()
        cfg_test["chain"]["output_path"] = test_path

        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = s2st_data
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["sensors_data_interpolation"]["use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["external_features"]["module"] = str(
            Path(IOTA2DIR) / "data" / "numpy_features" / "user_custom_function.py"
        )
        cfg_test["external_features"]["functions"] = "dummy_function"  # " get_ndvi"

        with open(config_path_test, "w", encoding="UTF-8") as file_cfg:
            file_cfg.write(str(cfg_test))
        pytest.raises(AttributeError, rcf.ReadConfigFile, config_path_test)

    def test_apply_function_with_custom_features(self, i2_tmpdir: Path) -> None:
        """Test the whole workflow."""
        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        data_generator.generate_data()
        config_path_test = i2_tmpdir / "Config_TEST.cfg"
        copy_and_modify_config(
            self.config_test,
            config_path_test,
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": str(i2_tmpdir),
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "external_features": {"functions": "get_identity"},
            },
        )

        cfg = rcf.ReadConfigFile(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False, cfg.get_param("chain", "list_tile").split(" ")
        )
        sensors_param = rcf.Iota2Parameters(
            rcf.ReadConfigFile(config_path_test)
        ).get_sensors_parameters("T31TCJ")
        sensors = SensorsContainer("T31TCJ", None, str(i2_tmpdir), **sensors_param)
        sensors.sensors_preprocess()
        sensor = sensors.get_enabled_sensors()[0]
        (
            (time_s_app, app_dep),  # pylint: disable=unused-variable
            _,
        ) = sensor.get_time_series()

        time_s_app.ExecuteAndWriteOutput()
        time_s = sensor.get_time_series_masks()
        time_s_app, app_dep, _ = time_s
        time_s_app.ExecuteAndWriteOutput()

        # only one sensor for test
        features_map_parameters = FeaturesMapParameters(
            working_directory=None,
            tile="T31TCJ",
            output_path=str(i2_tmpdir),
            sensors_parameters=sensors_param,
        )
        (otb_pipelines, _) = generate_features(features_map_parameters)
        list_functions = [
            ("get_cumulative_productivity", {}),
            ("get_seasonal_variation", {}),
        ]
        cust = CustomNumpyFeatures(
            sensors_param,
            cfg.get_param("external_features", "module"),
            list_functions,
        )
        chunking_config = rasterU.ChunkConfig(
            chunk_size_mode="user_fixed",
            number_of_chunks=None,
            chunk_size_x=5,
            chunk_size_y=5,
        )
        pipelines_rois = rasterU.pipelines_chunking(
            otb_pipelines,
            chunking_config,
            targeted_chunk=0,
        )
        (data, _) = rasterU.apply_udf_to_pipeline(
            otb_pipelines_list=pipelines_rois,
            function=UserDefinedFunction(partial(cust.process), True),
        )
        test_array = data.data
        new_labels = data.labels
        assert new_labels is not None
        assert test_array.shape == (366, 5, 5)
        del cfg

    def test_custom_features_fake_data(self, i2_tmpdir: Path) -> None:
        """
        TEST run custom feature with fake data
        this allows to test a feature before the whole chain starts
        """
        # instantiating custom_external_features needs a full directory, even
        # if we do not intend to use it because we inject fake data

        # generate data and config
        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        data_generator.generate_data()
        confpath = str(i2_tmpdir / "Config_TEST.cfg")
        shutil.copy(self.config_test, confpath)
        s2_st_data = str(i2_tmpdir)
        test_path = str(i2_tmpdir / "RUN")
        cfg_test = Config(confpath).as_dict()
        cfg_test["chain"]["output_path"] = test_path

        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = s2_st_data
        cfg_test["chain"]["nomenclature_path"] = confpath
        cfg_test["chain"]["color_table"] = confpath
        cfg_test["chain"]["ground_truth"] = confpath

        with open(confpath, "w", encoding="UTF-8") as file_cfg:
            file_cfg.write(str(cfg_test))

        # read param
        cfg = rcf.ReadConfigFile(confpath)
        param = rcf.Iota2Parameters(cfg)

        # variable used to instantiate custom_numpy_features
        tile_name = "T31TCJ"
        sensors_param = param.get_sensors_parameters(tile_name)
        module_name = cfg.get_param("external_features", "module")

        # should fail
        data = CustomNumpyFeatures(
            sensors_param,
            module_name,
            [("get_exception", {})],
        )
        error = data.test_user_feature_with_fake_data(2154)
        assert error is not None

        # should succeed
        data = CustomNumpyFeatures(
            sensors_param,
            module_name,
            [("get_seasonal_variation", {})],
        )
        error = data.test_user_feature_with_fake_data(2154)
        assert error is None

        # should succeed
        # testing get_filled_stack method
        data = CustomNumpyFeatures(
            sensors_param,
            module_name,
            [("get_raw_data", {})],
            configuration=CustomFeaturesConfiguration(enabled_raw=True),
        )
        error = data.test_user_feature_with_fake_data(2154)
        assert error is None
        del cfg

    def test_compute_custom_features(self, i2_tmpdir: Path) -> None:
        """TEST : check the whole workflow."""

        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        data_generator.generate_data()
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        copy_and_modify_config(
            self.config_test,
            config_path_test,
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": str(i2_tmpdir),
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "external_features": {"functions": "get_identity"},
            },
        )

        cfg = rcf.ReadConfigFile(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False, cfg.get_param("chain", "list_tile").split(" ")
        )
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters(tile_name)
        sensors = SensorsContainer(
            tile_name, working_dir, str(i2_tmpdir), **sensors_param
        )
        sensors.sensors_preprocess()
        list_sensors = sensors.get_enabled_sensors()
        sensor = list_sensors[0]
        (
            (time_s_app, _),
            labels,
        ) = sensor.get_time_series()
        assert all(isinstance(label, I2TemporalLabel) for label in labels)

        time_s_app.ExecuteAndWriteOutput()
        time_s = sensor.get_time_series_masks()
        time_s_app, _, _ = time_s
        time_s_app.ExecuteAndWriteOutput()

        features_map_parameters = FeaturesMapParameters(
            working_directory=working_dir,
            tile="T31TCJ",
            output_path=str(i2_tmpdir),
            sensors_parameters=sensors_param,
        )

        (otb_pipelines, _) = generate_features(features_map_parameters)

        # Then apply function
        list_functions = [
            ("get_cumulative_productivity", {}),
            ("get_seasonal_variation", {}),
        ]  # type: list[tuple[str, dict]]

        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=cfg.get_param("external_features", "module"),
            list_functions=list_functions,
            configuration=CustomFeaturesConfiguration(
                enabled_raw=False, enabled_gap=True, fill_missing_dates=False
            ),
            all_dates_dict=None,
        )
        otb_pipelines = turn_off_otb_pipelines(
            otb_pipelines,
            turn_off_raw=True,
            turn_off_interpolated=False,
        )
        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=[],
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="user_fixed",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
            masking_raster=MaskingRaster(None, 0),
        )

        assert data.labels is not None
        del cfg

    def test_manage_raw_data(
        self,
        i2_tmpdir: Path,
    ) -> None:
        """TEST : check the whole workflow."""
        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        data_generator.generate_data()
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)

        copy_and_modify_config(
            self.config_test,
            config_path_test,
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": str(i2_tmpdir),
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "external_features": {
                    "functions": "get_identity",
                    "module": str(
                        Path(IOTA2DIR)
                        / "data"
                        / "numpy_features"
                        / "user_custom_function.py"
                    ),
                },
            },
        )

        cfg = rcf.ReadConfigFile(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False, cfg.get_param("chain", "list_tile").split(" ")
        )
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters(tile_name)
        sensors = SensorsContainer(
            tile_name, working_dir, str(i2_tmpdir), **sensors_param
        )
        sensors.sensors_preprocess()
        list_sensors = sensors.get_enabled_sensors()
        sensor = list_sensors[0]
        (
            (time_s_app, _),
            labels,
        ) = sensor.get_time_series()

        assert all(isinstance(label, I2TemporalLabel) for label in labels)

        time_s_app.ExecuteAndWriteOutput()
        time_s = sensor.get_time_series_masks()
        time_s_app, _, time_series_length = time_s
        assert time_series_length == 4
        time_s_app.ExecuteAndWriteOutput()

        # Then apply function
        module_path = str(
            Path(IOTA2DIR) / "data" / "numpy_features" / "user_custom_function.py"
        )
        list_functions = [("get_raw_data", {})]  # type: list[tuple[str, dict]]
        # , "get_ndvi", "duplicate_ndvi"]
        all_dates_dict = {
            "Sentinel2": [
                "20200101",
                "20200212",
                "20200512",
                "20200702",
                "20201002",
                "20201102",
            ]
        }
        cust = CustomNumpyFeatures(
            sensors_param,
            module_path,
            list_functions,
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=True,
                enabled_gap=False,
                fill_missing_dates=True,
            ),
            all_dates_dict=all_dates_dict,
        )
        arr, labels = cust.process(
            PipelinesData(
                interpolated=np.ones((5, 4, 3)) * 6,
                raw=np.ones((5, 4, 40)),
                binary_mask=np.ones((5, 4, 4)) * 5,
            )
        )

        assert arr.shape == (5, 4, 66)

        assert [str(label) for label in labels] == [
            "sentinel2_b2_20200101",
            "sentinel2_b3_20200101",
            "sentinel2_b4_20200101",
            "sentinel2_b5_20200101",
            "sentinel2_b6_20200101",
            "sentinel2_b7_20200101",
            "sentinel2_b8_20200101",
            "sentinel2_b8a_20200101",
            "sentinel2_b11_20200101",
            "sentinel2_b12_20200101",
            "sentinel2_b2_20200212",
            "sentinel2_b3_20200212",
            "sentinel2_b4_20200212",
            "sentinel2_b5_20200212",
            "sentinel2_b6_20200212",
            "sentinel2_b7_20200212",
            "sentinel2_b8_20200212",
            "sentinel2_b8a_20200212",
            "sentinel2_b11_20200212",
            "sentinel2_b12_20200212",
            "sentinel2_b2_20200512",
            "sentinel2_b3_20200512",
            "sentinel2_b4_20200512",
            "sentinel2_b5_20200512",
            "sentinel2_b6_20200512",
            "sentinel2_b7_20200512",
            "sentinel2_b8_20200512",
            "sentinel2_b8a_20200512",
            "sentinel2_b11_20200512",
            "sentinel2_b12_20200512",
            "sentinel2_b2_20200702",
            "sentinel2_b3_20200702",
            "sentinel2_b4_20200702",
            "sentinel2_b5_20200702",
            "sentinel2_b6_20200702",
            "sentinel2_b7_20200702",
            "sentinel2_b8_20200702",
            "sentinel2_b8a_20200702",
            "sentinel2_b11_20200702",
            "sentinel2_b12_20200702",
            "sentinel2_b2_20201002",
            "sentinel2_b3_20201002",
            "sentinel2_b4_20201002",
            "sentinel2_b5_20201002",
            "sentinel2_b6_20201002",
            "sentinel2_b7_20201002",
            "sentinel2_b8_20201002",
            "sentinel2_b8a_20201002",
            "sentinel2_b11_20201002",
            "sentinel2_b12_20201002",
            "sentinel2_b2_20201102",
            "sentinel2_b3_20201102",
            "sentinel2_b4_20201102",
            "sentinel2_b5_20201102",
            "sentinel2_b6_20201102",
            "sentinel2_b7_20201102",
            "sentinel2_b8_20201102",
            "sentinel2_b8a_20201102",
            "sentinel2_b11_20201102",
            "sentinel2_b12_20201102",
            "sentinel2_mask_20200101",
            "sentinel2_mask_20200212",
            "sentinel2_mask_20200512",
            "sentinel2_mask_20200702",
            "sentinel2_mask_20201002",
            "sentinel2_mask_20201102",
        ]
        del cfg

    def test_pipeline_raise(self, i2_tmpdir: Path) -> None:
        """TEST : check the whole workflow."""
        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        data_generator.generate_data()
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        s2_st_data = str(i2_tmpdir)
        test_path = str(i2_tmpdir / "RUN")
        cfg_test = Config(config_path_test).as_dict()
        cfg_test["chain"]["output_path"] = test_path

        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = s2_st_data
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["sensors_data_interpolation"]["use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["external_features"]["module"] = str(
            Path(IOTA2DIR) / "data" / "numpy_features" / "user_custom_function.py"
        )
        cfg_test["external_features"]["functions"] = "get_identity"  # " get_ndvi"

        with open(config_path_test, "w", encoding="UTF-8") as file_cfg:
            file_cfg.write(str(cfg_test))
        cfg = rcf.ReadConfigFile(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False, cfg.get_param("chain", "list_tile").split(" ")
        )
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters(tile_name)
        features_map_parameters = FeaturesMapParameters(
            working_directory=working_dir,
            tile="T31TCJ",
            output_path=str(i2_tmpdir),
            sensors_parameters=sensors_param,
        )
        otb_pipelines, labs_dict = generate_features(features_map_parameters)
        # Then apply function
        module_path = str(
            Path(IOTA2DIR) / "data" / "numpy_features" / "user_custom_function.py"
        )
        list_functions = [("get_raw_data", {})]  # type: list[tuple[str, dict]]
        # , "get_ndvi", "duplicate_ndvi"]
        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101",
            "20200212",
            "20200512",
            "20200702",
            "20201002",
        ]

        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=module_path,
            list_functions=list_functions,
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=False,
                enabled_gap=True,
                fill_missing_dates=False,
            ),
            all_dates_dict=None,
        )
        otb_pipelines = turn_off_otb_pipelines(
            otb_pipelines,
            turn_off_raw=True,
            turn_off_interpolated=False,
        )

        pytest.raises(
            AttributeError,
            compute_custom_features,
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
        )

        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=cfg.get_param("external_features", "module"),
            list_functions=[("raise_label_exception", {})],
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=False,
                enabled_gap=True,
                fill_missing_dates=False,
            ),
            all_dates_dict=None,
        )
        otb_pipelines = turn_off_otb_pipelines(
            otb_pipelines,
            turn_off_raw=True,
            turn_off_interpolated=False,
        )
        pytest.raises(
            ValueError,
            compute_custom_features,
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
        )
        del cfg

    def test_pipeline_use_exogeneous_data(
        self,
        i2_tmpdir: Path,
    ) -> None:
        """TEST : check the whole workflow."""
        # generate multiple tiles
        for tile in ["T31TCJ", "T31TDJ"]:
            data_generator = TUR.FakeS2DataGenerator(
                str(i2_tmpdir),
                tile,
                ["20200101", "20200512", "20200702", "20201002"],
            )
            data_generator.generate_data()
        # make symbolic link in one folder for tiling tool
        tile_folder = str(i2_tmpdir / "sample_tiles")
        tiled_exogenous_data = str(i2_tmpdir / "tiled_exogenous_data")
        Path(tile_folder).mkdir()
        for src, dst in (
            (
                (
                    "T31TCJ/SENTINEL2B_20200101-000000-000_L2A_T31TCJ_D_V1-7/"
                    "SENTINEL2B_20200101-000000-000_L2A_T31TCJ_D_V1-7_FRE_B2.tif"
                ),
                "T31TCJ.tif",
            ),
            (
                (
                    "T31TDJ/SENTINEL2B_20200101-000000-000_L2A_T31TDJ_D_V1-7/"
                    "SENTINEL2B_20200101-000000-000_L2A_T31TDJ_D_V1-7_FRE_B2.tif"
                ),
                "T31TDJ.tif",
            ),
        ):
            Path(tile_folder / Path(dst)).symlink_to(i2_tmpdir / Path(src))
        # reproject fake exogenous data on multiple tiles
        reproject(
            IOTA2DIR / "data" / "numpy_features" / "fake_exo_multi_band.tif",
            Path(tile_folder),
            Path(tiled_exogenous_data),
            "float",
        )

        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        copy_and_modify_config(
            self.config_test,
            config_path_test,
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": str(i2_tmpdir),
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "external_features": {
                    "functions": "use_exogeneous_data get_ndvi",
                    "exogeneous_data": str(
                        Path(tiled_exogenous_data) / "fake_exo_multi_band_$TILE.tif"
                    ),
                },
            },
        )

        cfg = rcf.ReadConfigFile(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False, cfg.get_param("chain", "list_tile").split(" ")
        )

        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters("T31TCJ")
        features_map_parameters = FeaturesMapParameters(
            working_directory=None,
            tile="T31TCJ",
            output_path=str(i2_tmpdir),
            sensors_parameters=sensors_param,
        )
        (otb_pipelines, labs_dict) = generate_features(features_map_parameters)

        all_dates_dict = {
            "Sentinel2": [
                "20200101",
                "20200212",
                "20200512",
                "20200702",
                "20201002",
            ]
        }

        exogeneous_data = exogenous_data_tile(
            cfg.get_param("external_features", "exogeneous_data"), "T31TCJ"
        )
        functions = cfg.get_param("external_features", "functions")
        functions.append(("use_coordinates", {}))
        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=cfg.get_param("external_features", "module"),
            list_functions=functions,
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=True,
                enabled_gap=True,
                fill_missing_dates=True,
            ),
            all_dates_dict=all_dates_dict,
            exogeneous_data_file=exogeneous_data,
        )
        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
            exogeneous_data=exogeneous_data,
            concatenate_features=False,
        )
        assert len(data.labels) == 33
        assert data.otb_img["array"].shape == (16, 86, 33)
        del cfg

    def test_raw_data_s1_s2(
        self,
        i2_tmpdir: Path,
    ) -> None:
        """TEST the whole workflow."""

        def assert_all_dates(udf_data, pipelines):
            custom_array = udf_data.otb_img["array"]
            expected_shape = (
                16,
                86,
                10 * 2 + 2 + 6 + 3,  # (1 DES mask + 2 ASC masks)
            )  # s2_nb_bands * s2_dates + s2_dates + s1_nb_bands + s1_nb_masks
            assert custom_array.shape == expected_shape
            raw_arr = pipelines.raw_pipeline.GetVectorImageAsNumpyArray("out")
            assert np.allclose(raw_arr, custom_array[:, :, 0:26])
            masks_arr = pipelines.binary_mask_pipeline.GetVectorImageAsNumpyArray("out")
            assert np.allclose(masks_arr, custom_array[:, :, 26:])

        def assert_not_all_dates(udf_data, pipelines):
            cust_arr = udf_data.otb_img["array"]
            # asserts
            expected_shape = (
                16,
                86,
                10 * 3 + 3 + 8 + 4,
            )  # s2_nb_bands * s2_dates + s2_dates + s1_nb_bands + s1_nb_masks
            assert cust_arr.shape == expected_shape
            raw_arr = pipelines.raw_pipeline.GetVectorImageAsNumpyArray("out")
            fake_bands = np.ones((16, 86, 1)) * I2_CONST.i2_missing_dates_no_data
            # s1 data then s2 data
            bands_to_insert = [0, 1, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16]
            test_otbimage = np.insert(
                raw_arr, obj=bands_to_insert, values=fake_bands, axis=2
            )
            assert np.allclose(
                test_otbimage, cust_arr[:, :, 0:38]
            ), "fake spectral bands are at the wrong position"
            masks_arr = pipelines.binary_mask_pipeline.GetVectorImageAsNumpyArray("out")
            fake_bands = np.ones((16, 86, 1)) * I2_CONST.i2_missing_dates_no_data_mask
            # s1 data then s2 data
            test_mask = np.insert(masks_arr, obj=[0, 4], values=fake_bands, axis=2)
            assert np.allclose(
                test_mask, cust_arr[:, :, 38:]
            ), "fake mask bands are at the wrong position"

        # prepare data
        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512"],
        )
        data_generator.generate_data()
        config_path_test_s1 = str(i2_tmpdir / "Config_TEST_SAR.cfg")

        s1_output_data = str(i2_tmpdir / "tilled_s1")
        Path(s1_output_data).mkdir(parents=True, exist_ok=True)
        config = configparser.ConfigParser()
        config.read(self.sar_config_test)

        config["Paths"] = {
            **config["Paths"],
            **{
                "output": s1_output_data,
                "srtm": self.srtm_dir,
                "geoidfile": self.geoid_file,
                "s1images": self.acquisitions,
            },
        }

        config["Processing"]["referencesfolder"] = str(i2_tmpdir)
        config["Processing"]["srtmshapefile"] = self.srtm_db
        config["Processing"]["tilesshapefile"] = self.tiles_grid_db
        config["Processing"]["TemporalResolution"] = "1"

        with open(config_path_test_s1, "w", encoding="UTF-8") as configfile:
            config.write(configfile)

        copy_and_modify_config(
            self.config_test,
            str(i2_tmpdir / "Config_TEST.cfg"),
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": str(i2_tmpdir),
                    "region_field": "region",
                    "s1_path": config_path_test_s1,
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "external_features": {
                    "functions": "get_identity",
                    "module": str(
                        Path(IOTA2DIR)
                        / "data"
                        / "numpy_features"
                        / "user_custom_function.py"
                    ),
                },
                "python_data_managing": {
                    "number_of_chunks": 1,
                    "chunk_size_mode": "split_number",
                },
            },
        )

        cfg = rcf.ReadConfigFile(str(i2_tmpdir / "Config_TEST.cfg"))

        iota2_directory.generate_directories(
            str(i2_tmpdir / "RUN"),
            False,
            cfg.get_param("chain", "list_tile").split(" "),
        )
        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters("T31TCJ")
        features_map_parameters = FeaturesMapParameters(
            working_directory=None,
            tile="T31TCJ",
            output_path=str(i2_tmpdir),
            sensors_parameters=sensors_param,
        )
        (otb_pipelines, labs_dict) = generate_features(features_map_parameters)

        # available dates == all dates

        functions = [("get_raw_data", {})]  # type: list[tuple[str, dict]]
        module_path = cfg.get_param("external_features", "module")

        # launch function
        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=module_path,
            list_functions=functions,
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=True,
                enabled_gap=True,
                fill_missing_dates=True,
            ),
            all_dates_dict={
                "Sentinel2": ["20200101", "20200512"],
                "Sentinel1_DES_vv": ["20151231"],
                "Sentinel1_DES_vh": ["20151231"],
                "Sentinel1_ASC_vv": ["20170518", "20170519"],
                "Sentinel1_ASC_vh": ["20170518", "20170519"],
            },
        )

        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
        )
        assert_all_dates(data, otb_pipelines)

        # prepare data
        # available dates != all dates : 1 fake s2 and s1 bands required
        # launch function
        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=module_path,
            list_functions=functions,
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=True,
                enabled_gap=True,
                fill_missing_dates=True,
            ),
            all_dates_dict={
                "Sentinel2": ["20200101", "20200201", "20200512"],
                "Sentinel1_DES_vv": ["20151201", "20151231"],
                "Sentinel1_DES_vh": ["20151201", "20151231"],
                "Sentinel1_ASC_vv": ["20170518", "20170519"],
                "Sentinel1_ASC_vh": ["20170518", "20170519"],
            },
        )
        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
            concatenate_features=False,
        )

        assert_not_all_dates(data, otb_pipelines)
        del cfg

    def test_s2_with_padding(
        self,
        i2_tmpdir: Path,
    ) -> None:
        """Check custom feature with padding"""
        # prepare data
        s2_dates = ["20200101", "20200512"]
        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir),
            "T31TCJ",
            s2_dates,
        )
        data_generator.generate_data()

        copy_and_modify_config(
            self.config_test,
            str(i2_tmpdir / "Config_TEST.cfg"),
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": str(i2_tmpdir),
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "python_data_managing": {
                    "number_of_chunks": 1,
                    "chunk_size_mode": "split_number",
                },
                "userFeat": {"patterns": "mnt"},
            },
        )

        cfg = rcf.ReadConfigFile(str(i2_tmpdir / "Config_TEST.cfg"))

        iota2_directory.generate_directories(
            str(i2_tmpdir / "RUN"),
            False,
            cfg.get_param("chain", "list_tile").split(" "),
        )
        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters("T31TCJ")
        (otb_pipelines, labs_dict) = generate_features(
            FeaturesMapParameters(
                working_directory=None,
                tile="T31TCJ",
                output_path=str(i2_tmpdir),
                sensors_parameters=sensors_param,
            )
        )

        # available dates == all dates
        kernel = np.array([[1, 1, 1], [1, 1, 0], [1, 0, 0]])
        functions = [
            ("convolve_raw_data", {"kernel": kernel, "mode": "constant", "cval": 0.0})
        ]

        raw_ts = otb_pipelines.raw_pipeline
        ref_data = extract_raster_metadata_from_otb(raw_ts)
        raw_arr = raw_ts.GetVectorImageAsNumpyArray("out")

        # launch function
        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=__file__,
            list_functions=functions,
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=True,
                enabled_gap=True,
                fill_missing_dates=True,
            ),
            all_dates_dict={"Sentinel2": s2_dates},
        )

        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=3,
                    chunk_size_x=5,
                    chunk_size_y=5,
                    padding_size_x=3,
                    padding_size_y=3,
                ),
                None,
            ),
            concatenate_features=False,
        )
        # uncomment the next lines led to an error
        # raw_arr = raw_ts.GetVectorImageAsNumpyArray("out")
        cust_arr = data.otb_img["array"]

        # asserts
        s2_nb_bands = 10
        expected_shape = (16, 86, s2_nb_bands * len(s2_dates))
        assert cust_arr.shape == expected_shape
        assert np.allclose(
            ndimage.convolve(
                raw_arr,
                np.repeat(kernel[:, :, np.newaxis], raw_arr.shape[-1], axis=2),
                mode="constant",
                cval=0.0,
            ),
            cust_arr,
        ), "padding workflow broken"

        bandmath = otb.Registry.CreateApplication("BandMath")
        bandmath.ImportVectorImage("il", data.otb_img)
        bandmath.Execute()

        test_data = extract_raster_metadata_from_otb(bandmath)

        assert ref_data == test_data

        del cfg

    def test_s2_with_padding_small_chunks(
        self,
        i2_tmpdir: Path,
    ) -> None:
        """Check custom feature with padding and 'user_fixed' sized chunks"""
        # prepare data
        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512"],
        )
        data_generator.generate_data()
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        copy_and_modify_config(
            self.config_test,
            config_path_test,
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": str(i2_tmpdir),
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "python_data_managing": {
                    "number_of_chunks": 1,
                    "chunk_size_mode": "split_number",
                },
            },
        )

        cfg = rcf.ReadConfigFile(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir / "RUN"),
            False,
            cfg.get_param("chain", "list_tile").split(" "),
        )
        working_dir = None
        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters("T31TCJ")
        features_map_parameters = FeaturesMapParameters(
            working_directory=working_dir,
            tile="T31TCJ",
            output_path=str(i2_tmpdir),
            sensors_parameters=sensors_param,
        )
        (otb_pipelines, labs_dict) = generate_features(features_map_parameters)

        # available dates == all dates
        all_dates_dict = {"Sentinel2": ["20200101", "20200512"]}
        module_path = __file__
        kernel = np.array([[1, 1, 1], [1, 1, 0], [1, 0, 0]])
        functions = [
            ("convolve_raw_data", {"kernel": kernel, "mode": "constant", "cval": 0.0})
        ]

        # launch function

        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=module_path,
            list_functions=functions,
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=True,
                enabled_gap=True,
                fill_missing_dates=True,
            ),
            all_dates_dict=all_dates_dict,
        )

        size_x, size_y = (5, 5)
        padding_size_x, padding_size_y = (2, 2)
        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="user_fixed",
                    number_of_chunks=3,
                    chunk_size_x=size_x,
                    chunk_size_y=size_y,
                    padding_size_x=padding_size_x,
                    padding_size_y=padding_size_y,
                ),
                20,
            ),
            concatenate_features=False,
        )

        # asserts
        assert data.otb_img["array"].shape == (size_x, size_y, 20)

    def test_pipeline_use_raw_and_masks(
        self,
        i2_tmpdir: Path,
    ) -> None:
        """TEST : check the whole workflow."""

        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        data_generator.generate_data()
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)

        copy_and_modify_config(
            self.config_test,
            str(i2_tmpdir / "Config_TEST.cfg"),
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": str(i2_tmpdir),
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "external_features": {
                    "functions": "use_raw_and_masks_data",
                },
            },
        )

        cfg = rcf.ReadConfigFile(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False, cfg.get_param("chain", "list_tile").split(" ")
        )
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters(tile_name)
        features_map_parameters = FeaturesMapParameters(
            working_directory=working_dir,
            tile="T31TCJ",
            output_path=str(i2_tmpdir),
            sensors_parameters=sensors_param,
        )
        (otb_pipelines, labs_dict) = generate_features(features_map_parameters)

        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101",
            "20200212",
            "20200512",
            "20200702",
            "20201002",
        ]
        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=cfg.get_param("external_features", "module"),
            list_functions=cfg.get_param("external_features", "functions"),
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=True,
                enabled_gap=True,
                fill_missing_dates=True,
            ),
            all_dates_dict=all_dates_dict,
            exogeneous_data_file=None,
        )

        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
            exogeneous_data=None,
            concatenate_features=False,
        )
        otbimage = data.otb_img
        feat_labels = data.labels
        unique, counts = np.unique(otbimage["array"], return_counts=True)
        dico = dict(zip(unique, counts))
        assert dico[-1000] == 2752
        assert otbimage["array"].dtype == "int16"
        assert len(feat_labels) == 4
        assert otbimage["array"].shape == (16, 86, 4)
        del cfg

    def test_interpolated_data_s1_s2(
        self,
        i2_tmpdir: Path,
    ) -> None:
        """Check custom feature with S1 data"""
        # prepare data
        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200111"],
        )
        data_generator.generate_data()
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        config_path_test_s1 = str(i2_tmpdir / "Config_TEST_SAR.cfg")
        s2_data = str(i2_tmpdir)

        s1_output_data = str(i2_tmpdir / "tilled_s1")
        Path(s1_output_data).mkdir(parents=True, exist_ok=True)
        config = configparser.ConfigParser()
        config.read(self.sar_config_test)

        paths = config["Paths"]
        paths["output"] = s1_output_data
        paths["srtm"] = self.srtm_dir
        paths["geoidfile"] = self.geoid_file
        paths["s1images"] = self.acquisitions

        processing = config["Processing"]
        processing["referencesfolder"] = s2_data
        processing["srtmshapefile"] = self.srtm_db
        processing["tilesshapefile"] = self.tiles_grid_db
        processing["TemporalResolution"] = "1"

        with open(config_path_test_s1, "w", encoding="UTF-8") as configfile:
            config.write(configfile)

        copy_and_modify_config(
            self.config_test,
            config_path_test,
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": str(i2_tmpdir),
                    "s1_path": config_path_test_s1,
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "python_data_managing": {
                    "number_of_chunks": 1,
                    "chunk_size_mode": "split_number",
                },
            },
        )

        cfg = rcf.ReadConfigFile(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir / "RUN"),
            False,
            cfg.get_param("chain", "list_tile").split(" "),
        )
        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters("T31TCJ")
        otb_pipelines, labs_dict = generate_features(
            FeaturesMapParameters(
                working_directory=None,
                tile="T31TCJ",
                output_path=str(i2_tmpdir),
                sensors_parameters=sensors_param,
            )
        )
        functions = [("get_interp_data", {})]  # type: list[tuple[str, dict]]
        # launch function
        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=__file__,
            list_functions=functions,
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=True,
                enabled_gap=True,
                fill_missing_dates=True,
            ),
            all_dates_dict={
                "Sentinel2": ["20200101", "20200111"],
                "Sentinel1_DES_vv": ["20151231"],
                "Sentinel1_DES_vh": ["20151231"],
                "Sentinel1_ASC_vv": ["20170518", "20170519"],
                "Sentinel1_ASC_vh": ["20170518", "20170519"],
            },
        )
        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
            concatenate_features=False,
        )
        ref_interp_app = otb_pipelines.interpolated_pipeline
        ref_interp_app.Execute()
        ref_interp_arr = ref_interp_app.GetVectorImageAsNumpyArray("out")
        assert np.allclose(
            ref_interp_arr, data.otb_img["array"]
        ), "interpolated data s1 + s2 fail"
        del cfg

    def test_external_features_with_userfeatures(self, i2_tmpdir: Path) -> None:
        """Check if external features can handle userfeatures."""

        s2_dir = str(i2_tmpdir / "s2_data")
        mnt_dir = str(i2_tmpdir / "mnt_data")
        mnt_content = 42
        for tile in ["T31TCJ"]:
            data_generator = TUR.FakeS2DataGenerator(
                s2_dir,
                tile,
                ["20200101"],
            )
            data_generator.generate_data()

            TUR.generate_fake_user_features_data(
                mnt_dir, tile, ["mnt"], force_value=mnt_content
            )

        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        copy_and_modify_config(
            self.config_test,
            config_path_test,
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": str(i2_tmpdir / "s2_data"),
                    "user_feat_path": mnt_dir,
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "external_features": {
                    "functions": "get_mnt",
                },
                "userFeat": {"patterns": "mnt"},
            },
        )
        cfg = rcf.ReadConfigFile(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False, cfg.get_param("chain", "list_tile").split(" ")
        )
        working_dir = None

        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters("T31TCJ")

        features_map_parameters = FeaturesMapParameters(
            working_directory=working_dir,
            tile="T31TCJ",
            output_path=str(i2_tmpdir),
            sensors_parameters=sensors_param,
        )

        otb_pipelines, labs_dict = generate_features(features_map_parameters)
        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=cfg.get_param("external_features", "module"),
            list_functions=cfg.get_param("external_features", "functions"),
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=False,
                enabled_gap=True,
                fill_missing_dates=False,
            ),
            all_dates_dict={"Sentinel2": ["20200101"]},
        )
        otb_pipelines = turn_off_otb_pipelines(
            otb_pipelines,
            turn_off_raw=True,
            turn_off_interpolated=False,
        )
        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
            concatenate_features=False,
        )
        otbimage = data.otb_img
        feat_labels = data.labels
        assert len(np.unique(otbimage["array"])) == 1
        assert np.unique(otbimage["array"])[0] == mnt_content
        assert len(feat_labels) == 1
        assert otbimage["array"].shape == (16, 86, 1)
        del cfg

    def test_external_features_with_userfeatures_only(self, i2_tmpdir: Path) -> None:
        """Check if external features can handle userfeatures."""

        s2_dir = str(i2_tmpdir / "s2_data")
        mnt_dir = str(i2_tmpdir / "mnt_data")
        mnt_content = 42
        for tile in ["T31TCJ"]:
            data_generator = TUR.FakeS2DataGenerator(
                s2_dir,
                tile,
                ["20200101"],
            )
            data_generator.generate_data()
            TUR.generate_fake_user_features_data(
                mnt_dir, tile, ["mnt"], force_value=mnt_content
            )

        copy_and_modify_config(
            self.config_test,
            str(i2_tmpdir / "Config_TEST.cfg"),
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "user_feat_path": mnt_dir,
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "external_features": {
                    "functions": "get_mnt",
                },
                "userFeat": {"patterns": "mnt"},
            },
        )

        cfg = rcf.ReadConfigFile(str(i2_tmpdir / "Config_TEST.cfg"))

        iota2_directory.generate_directories(
            str(i2_tmpdir), False, cfg.get_param("chain", "list_tile").split(" ")
        )
        tile_name = "T31TCJ"
        working_dir = None

        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters(tile_name)
        features_map_parameters = FeaturesMapParameters(
            working_directory=working_dir,
            tile="T31TCJ",
            output_path=str(i2_tmpdir),
            sensors_parameters=sensors_param,
        )
        otb_pipelines, labs_dict = generate_features(features_map_parameters)
        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101",
        ]
        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=cfg.get_param("external_features", "module"),
            list_functions=cfg.get_param("external_features", "functions"),
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=True,
                enabled_gap=True,
                fill_missing_dates=False,
            ),
            all_dates_dict=all_dates_dict,
        )
        otb_pipelines = turn_off_otb_pipelines(
            otb_pipelines,
            turn_off_raw=False,
            turn_off_interpolated=False,
        )
        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
            concatenate_features=False,
        )
        otbimage = data.otb_img
        feat_labels = data.labels
        assert len(np.unique(otbimage["array"])) == 1
        assert np.unique(otbimage["array"])[0] == mnt_content
        assert len(feat_labels) == 1
        assert otbimage["array"].shape == (16, 86, 1)
        del cfg

    def test_external_features_with_userfeatures_s1_s2(self, i2_tmpdir: Path) -> None:
        """Check if external features can handle userfeatures."""

        s2_dir = str(i2_tmpdir / "s2_data")
        mnt_dir = str(i2_tmpdir / "mnt_data")
        mnt_content = 42
        data_generator = TUR.FakeS2DataGenerator(
            s2_dir,
            "T31TCJ",
            ["20200101"],
        )
        data_generator.generate_data()

        TUR.generate_fake_user_features_data(
            mnt_dir, "T31TCJ", ["mnt"], force_value=mnt_content
        )

        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        config_path_test_s1 = str(i2_tmpdir / "Config_TEST_SAR.cfg")
        s1_output_data = str(i2_tmpdir / "tilled_s1")
        Path(s1_output_data).mkdir(parents=True, exist_ok=True)
        config = configparser.ConfigParser()
        config.read(self.sar_config_test)
        paths = config["Paths"]
        paths["output"] = s1_output_data
        paths["srtm"] = self.srtm_dir
        paths["geoidfile"] = self.geoid_file
        paths["s1images"] = self.acquisitions
        processing = config["Processing"]

        processing["referencesfolder"] = s2_dir
        processing["srtmshapefile"] = self.srtm_db
        processing["tilesshapefile"] = self.tiles_grid_db
        processing["TemporalResolution"] = "1"

        with open(config_path_test_s1, "w", encoding="UTF-8") as configfile:
            config.write(configfile)

        copy_and_modify_config(
            self.config_test,
            config_path_test,
            {
                "chain": {
                    "output_path": str(i2_tmpdir / "RUN"),
                    "check_inputs": False,
                    "s2_path": s2_dir,
                    "s1_path": config_path_test_s1,
                    "user_feat_path": mnt_dir,
                    "region_field": "region",
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": False,
                },
                "external_features": {
                    "functions": "get_mnt",
                },
                "userFeat": {"patterns": "mnt"},
            },
        )

        cfg = rcf.ReadConfigFile(str(i2_tmpdir / "Config_TEST.cfg"))

        iota2_directory.generate_directories(
            str(i2_tmpdir / "RUN"),
            False,
            cfg.get_param("chain", "list_tile").split(" "),
        )
        sensors_param = rcf.Iota2Parameters(cfg).get_sensors_parameters("T31TCJ")
        otb_pipelines, labs_dict = generate_features(
            FeaturesMapParameters(
                working_directory=None,
                tile="T31TCJ",
                output_path=str(i2_tmpdir),
                sensors_parameters=sensors_param,
            )
        )
        cust = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=cfg.get_param("external_features", "module"),
            list_functions=cfg.get_param("external_features", "functions"),
            configuration=CustomFeaturesConfiguration(
                concat_mode=False,
                enabled_raw=True,
                enabled_gap=True,
                fill_missing_dates=False,
            ),
            all_dates_dict={"Sentinel2": ["20200101"]},
        )
        otb_pipelines = turn_off_otb_pipelines(
            otb_pipelines,
            turn_off_raw=False,
            turn_off_interpolated=False,
        )
        data, _ = compute_custom_features(
            functions_builder=cust,
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=1,
                    chunk_size_x=5,
                    chunk_size_y=5,
                ),
                0,
            ),
            concatenate_features=False,
        )
        otbimage = data.otb_img
        assert len(np.unique(otbimage["array"])) == 1
        assert np.unique(otbimage["array"])[0] == mnt_content
        assert len(data.labels) == 1
        assert otbimage["array"].shape == (16, 86, 1)
        del cfg

    def test_report_band_description(self, i2_tmpdir: Path) -> None:
        """
        TEST report_band_description fonction
        """
        # generate data and config
        input_raster_path = str(i2_tmpdir / "input_raster.tif")
        TUR.generate_fake_raster_multi_band(
            input_raster_path, array_names=["iota2_binary", "iota2_binary"]
        )

        output_raster_path = str(i2_tmpdir / "output_raster.tif")
        TUR.generate_fake_raster_multi_band(
            output_raster_path, array_names=["iota2_binary", "iota2_binary"]
        )

        raster = gdal.Open(input_raster_path, gdal.GA_Update)
        raster_band = raster.GetRasterBand(1)
        raster_band.SetDescription("label_bande_1")
        raster_band = raster.GetRasterBand(2)
        raster_band.SetDescription("label_bande_2")
        del raster

        report_band_description(input_raster_path, output_raster_path)

        raster = gdal.Open(output_raster_path)
        raster_band = raster.GetRasterBand(1)
        label_bande_1 = raster_band.GetDescription()
        raster_band = raster.GetRasterBand(2)
        label_bande_2 = raster_band.GetDescription()
        del raster

        assert label_bande_1 == "label_bande_1"
        assert label_bande_2 == "label_bande_2"

        TUR.generate_fake_raster_multi_band(
            output_raster_path, array_names=["iota2_binary"]
        )

        report_band_description(input_raster_path, output_raster_path)

        raster = gdal.Open(output_raster_path)
        raster_band = raster.GetRasterBand(1)
        label_bande_1 = raster_band.GetDescription()
        del raster

        assert label_bande_1 == ""


def convolve_raw_data(
    self: DataContainer,
    kernel: np.ndarray,
    mode: str = "constant",
    cval: float = 0.0,
) -> tuple[np.ndarray, list[I2TemporalLabel]]:
    """Perform a convolution to raw data, using a given kernel"""
    raw, labels = self.get_filled_stack()
    kernel = np.repeat(kernel[:, :, np.newaxis], raw.shape[-1], axis=2)
    conv = ndimage.convolve(raw, kernel, mode=mode, cval=cval)
    return conv, labels


def get_interp_data(self: DataContainer) -> tuple[np.ndarray, EmptyList]:
    """must be use only with test_interpolated_data_s1_s2
    due to re-shape
    """
    des_vv = self.get_interpolated_Sentinel1_DES_vv()
    des_vh = self.get_interpolated_Sentinel1_DES_vh()
    asc_vv = self.get_interpolated_Sentinel1_ASC_vv()
    asc_vh = self.get_interpolated_Sentinel1_ASC_vh()

    s2_b2 = self.get_interpolated_Sentinel2_B2()
    s2_b3 = self.get_interpolated_Sentinel2_B3()
    s2_b4 = self.get_interpolated_Sentinel2_B4()
    s2_b5 = self.get_interpolated_Sentinel2_B5()
    s2_b6 = self.get_interpolated_Sentinel2_B6()
    s2_b7 = self.get_interpolated_Sentinel2_B7()
    s2_b8 = self.get_interpolated_Sentinel2_B8()
    s2_b8a = self.get_interpolated_Sentinel2_B8A()
    s2_b11 = self.get_interpolated_Sentinel2_B11()
    s2_b12 = self.get_interpolated_Sentinel2_B12()
    s2_ndvi = self.get_interpolated_Sentinel2_NDVI()
    s2_ndwi = self.get_interpolated_Sentinel2_NDWI()
    s2_brightness = self.get_interpolated_Sentinel2_Brightness()

    interp_stack = np.concatenate(
        (
            des_vv,
            des_vh,
            asc_vv,
            asc_vh,
            np.expand_dims(s2_b2[:, :, 0], axis=2),
            np.expand_dims(s2_b3[:, :, 0], axis=2),
            np.expand_dims(s2_b4[:, :, 0], axis=2),
            np.expand_dims(s2_b5[:, :, 0], axis=2),
            np.expand_dims(s2_b6[:, :, 0], axis=2),
            np.expand_dims(s2_b7[:, :, 0], axis=2),
            np.expand_dims(s2_b8[:, :, 0], axis=2),
            np.expand_dims(s2_b8a[:, :, 0], axis=2),
            np.expand_dims(s2_b11[:, :, 0], axis=2),
            np.expand_dims(s2_b12[:, :, 0], axis=2),
            np.expand_dims(s2_b2[:, :, 1], axis=2),
            np.expand_dims(s2_b3[:, :, 1], axis=2),
            np.expand_dims(s2_b4[:, :, 1], axis=2),
            np.expand_dims(s2_b5[:, :, 1], axis=2),
            np.expand_dims(s2_b6[:, :, 1], axis=2),
            np.expand_dims(s2_b7[:, :, 1], axis=2),
            np.expand_dims(s2_b8[:, :, 1], axis=2),
            np.expand_dims(s2_b8a[:, :, 1], axis=2),
            np.expand_dims(s2_b11[:, :, 1], axis=2),
            np.expand_dims(s2_b12[:, :, 1], axis=2),
            np.expand_dims(s2_ndvi[:, :, 0], axis=2),
            np.expand_dims(s2_ndvi[:, :, 1], axis=2),
            np.expand_dims(s2_ndwi[:, :, 0], axis=2),
            np.expand_dims(s2_ndwi[:, :, 1], axis=2),
            np.expand_dims(s2_brightness[:, :, 0], axis=2),
            np.expand_dims(s2_brightness[:, :, 1], axis=2),
        ),
        axis=2,
    )
    labels: EmptyList = []
    return interp_stack, labels
