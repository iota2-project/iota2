#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test if sensors object can be instantiated."""
import os
from pathlib import Path

import pytest

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.common.compression_options import delete_compression_suffix
from iota2.sensors.landsat5old import Landsat5Old
from iota2.sensors.landsat8 import Landsat8
from iota2.sensors.landsat8old import Landsat8Old
from iota2.sensors.sensorscontainer import SensorsContainer
from iota2.sensors.sentinel2 import Sentinel2
from iota2.sensors.sentinel2_l3a import Sentinel2L3a
from iota2.sensors.sentinel2_s2c import Sentinel2S2c
from iota2.sensors.userfeatures import UserFeatures
from iota2.tests.utils.asserts_utils import AssertsFilesUtils

IOTA2DIR = os.environ.get("IOTA2DIR")
if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")


@pytest.mark.config
@pytest.mark.sensors
class Iota2TestSensors(AssertsFilesUtils):
    """Check if sensors object can be instantiated."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.expected_s2_labels = [
            "Sentinel2_B2_20200101",
            "Sentinel2_B3_20200101",
            "Sentinel2_B4_20200101",
            "Sentinel2_B5_20200101",
            "Sentinel2_B6_20200101",
            "Sentinel2_B7_20200101",
            "Sentinel2_B8_20200101",
            "Sentinel2_B8A_20200101",
            "Sentinel2_B11_20200101",
            "Sentinel2_B12_20200101",
            "Sentinel2_B2_20200111",
            "Sentinel2_B3_20200111",
            "Sentinel2_B4_20200111",
            "Sentinel2_B5_20200111",
            "Sentinel2_B6_20200111",
            "Sentinel2_B7_20200111",
            "Sentinel2_B8_20200111",
            "Sentinel2_B8A_20200111",
            "Sentinel2_B11_20200111",
            "Sentinel2_B12_20200111",
            "Sentinel2_NDVI_20200101",
            "Sentinel2_NDVI_20200111",
            "Sentinel2_NDWI_20200101",
            "Sentinel2_NDWI_20200111",
            "Sentinel2_Brightness_20200101",
            "Sentinel2_Brightness_20200111",
        ]

        cls.expected_s2_s2c_labels = [
            "Sentinel2S2C_B02_20190501",
            "Sentinel2S2C_B03_20190501",
            "Sentinel2S2C_B04_20190501",
            "Sentinel2S2C_B05_20190501",
            "Sentinel2S2C_B06_20190501",
            "Sentinel2S2C_B07_20190501",
            "Sentinel2S2C_B08_20190501",
            "Sentinel2S2C_B8A_20190501",
            "Sentinel2S2C_B11_20190501",
            "Sentinel2S2C_B12_20190501",
            "Sentinel2S2C_B02_20190504",
            "Sentinel2S2C_B03_20190504",
            "Sentinel2S2C_B04_20190504",
            "Sentinel2S2C_B05_20190504",
            "Sentinel2S2C_B06_20190504",
            "Sentinel2S2C_B07_20190504",
            "Sentinel2S2C_B08_20190504",
            "Sentinel2S2C_B8A_20190504",
            "Sentinel2S2C_B11_20190504",
            "Sentinel2S2C_B12_20190504",
            "Sentinel2S2C_NDVI_20190501",
            "Sentinel2S2C_NDVI_20190504",
            "Sentinel2S2C_NDWI_20190501",
            "Sentinel2S2C_NDWI_20190504",
            "Sentinel2S2C_Brightness_20190501",
            "Sentinel2S2C_Brightness_20190504",
        ]

        cls.expected_l8_labels = [
            "Landsat8_B1_20200101",
            "Landsat8_B2_20200101",
            "Landsat8_B3_20200101",
            "Landsat8_B4_20200101",
            "Landsat8_B5_20200101",
            "Landsat8_B6_20200101",
            "Landsat8_B7_20200101",
            "Landsat8_B1_20200111",
            "Landsat8_B2_20200111",
            "Landsat8_B3_20200111",
            "Landsat8_B4_20200111",
            "Landsat8_B5_20200111",
            "Landsat8_B6_20200111",
            "Landsat8_B7_20200111",
            "Landsat8_NDVI_20200101",
            "Landsat8_NDVI_20200111",
            "Landsat8_NDWI_20200101",
            "Landsat8_NDWI_20200111",
            "Landsat8_Brightness_20200101",
            "Landsat8_Brightness_20200111",
        ]

        cls.expected_s2_l3a_labels = [
            "Sentinel2L3A_B2_20200101",
            "Sentinel2L3A_B3_20200101",
            "Sentinel2L3A_B4_20200101",
            "Sentinel2L3A_B5_20200101",
            "Sentinel2L3A_B6_20200101",
            "Sentinel2L3A_B7_20200101",
            "Sentinel2L3A_B8_20200101",
            "Sentinel2L3A_B8A_20200101",
            "Sentinel2L3A_B11_20200101",
            "Sentinel2L3A_B12_20200101",
            "Sentinel2L3A_B2_20200120",
            "Sentinel2L3A_B3_20200120",
            "Sentinel2L3A_B4_20200120",
            "Sentinel2L3A_B5_20200120",
            "Sentinel2L3A_B6_20200120",
            "Sentinel2L3A_B7_20200120",
            "Sentinel2L3A_B8_20200120",
            "Sentinel2L3A_B8A_20200120",
            "Sentinel2L3A_B11_20200120",
            "Sentinel2L3A_B12_20200120",
            "Sentinel2L3A_NDVI_20200101",
            "Sentinel2L3A_NDVI_20200120",
            "Sentinel2L3A_NDWI_20200101",
            "Sentinel2L3A_NDWI_20200120",
            "Sentinel2L3A_Brightness_20200101",
            "Sentinel2L3A_Brightness_20200120",
        ]
        cls.expected_user_features_labels = ["numberofthings_0", "lai_0"]

        cls.expected_l8_old_labels = [
            "Landsat8Old_B1_20200101",
            "Landsat8Old_B2_20200101",
            "Landsat8Old_B3_20200101",
            "Landsat8Old_B4_20200101",
            "Landsat8Old_B5_20200101",
            "Landsat8Old_B6_20200101",
            "Landsat8Old_B7_20200101",
            "Landsat8Old_B1_20200111",
            "Landsat8Old_B2_20200111",
            "Landsat8Old_B3_20200111",
            "Landsat8Old_B4_20200111",
            "Landsat8Old_B5_20200111",
            "Landsat8Old_B6_20200111",
            "Landsat8Old_B7_20200111",
            "Landsat8Old_NDVI_20200101",
            "Landsat8Old_NDVI_20200111",
            "Landsat8Old_NDWI_20200101",
            "Landsat8Old_NDWI_20200111",
            "Landsat8Old_Brightness_20200101",
            "Landsat8Old_Brightness_20200111",
        ]
        cls.expected_l5_old_labels = [
            "Landsat5Old_B1_20200101",
            "Landsat5Old_B2_20200101",
            "Landsat5Old_B3_20200101",
            "Landsat5Old_B4_20200101",
            "Landsat5Old_B5_20200101",
            "Landsat5Old_B6_20200101",
            "Landsat5Old_B1_20200111",
            "Landsat5Old_B2_20200111",
            "Landsat5Old_B3_20200111",
            "Landsat5Old_B4_20200111",
            "Landsat5Old_B5_20200111",
            "Landsat5Old_B6_20200111",
            "Landsat5Old_NDVI_20200101",
            "Landsat5Old_NDVI_20200111",
            "Landsat5Old_NDWI_20200101",
            "Landsat5Old_NDWI_20200111",
            "Landsat5Old_Brightness_20200101",
            "Landsat5Old_Brightness_20200111",
        ]

        cls.expected_sensors = [
            "Landsat5Old",
            "Landsat8",
            "Landsat8Old",
            "Sentinel2",
            "Sentinel2S2C",
            "Sentinel2L3A",
        ]

    # Tests definitions
    def test_instance_s2(self, i2_tmpdir: Path) -> None:
        """Tests if the class sentinel_2 can be instantiated."""
        tile_name = "T31TCJ"
        data_generator = TUR.FakeS2DataGenerator(
            str(i2_tmpdir), tile_name, ["20200101", "20200120"], res=10
        )
        data_generator.generate_data()
        args = {
            "tile_name": "T31TCJ",
            "target_proj": 2154,
            "all_tiles": "T31TCJ",
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None,
        }

        s2_sensor = Sentinel2(**args)
        (features_app, _), features_labels = s2_sensor.get_features()

        features_app.ExecuteAndWriteOutput()

        assert [elem.lower() for elem in self.expected_s2_labels] == [
            str(elem) for elem in features_labels
        ], "Sentinel-2 class broken, wrong features' labels"

        expected_output = Path(
            delete_compression_suffix(features_app.GetParameterString("out"))
        )
        self.assert_file_exist(expected_output)

    def test_instance_s2_s2c(self, i2_tmpdir: Path) -> None:
        """Tests if the class sentinel_2_s2c can be instantiated."""
        tile_name = "T31TCJ"
        # generate fake input data
        mtd_files = [
            str(Path(IOTA2DIR) / "data" / "MTD_MSIL2A_20190501.xml"),
            str(Path(IOTA2DIR) / "data" / "MTD_MSIL2A_20190506.xml"),
        ]

        TUR.generate_fake_s2_s2c_data(
            str(i2_tmpdir),
            tile_name,
            mtd_files,
            origin_x=566377,
            origin_y=6284029,
            epsg_code=2154,
        )
        args = {
            "tile_name": tile_name,
            "target_proj": 2154,
            "all_tiles": tile_name,
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": "",
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 3,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None,
        }

        s2_sensor = Sentinel2S2c(**args)
        (features_app, _), features_labels = s2_sensor.get_features()
        features_app.ExecuteAndWriteOutput()

        assert [elem.lower() for elem in self.expected_s2_s2c_labels] == [
            str(elem) for elem in features_labels
        ], "Sentinel_2_S2C class broken, wrong features' labels"

        expected_output = Path(
            delete_compression_suffix(features_app.GetParameterString("out"))
        )
        self.assert_file_exist(expected_output)

    def test_instance_l8(self, i2_tmpdir: Path) -> None:
        """Tests if the class landsat_8 can be instantiated."""
        tile_name = "T31TCJ"
        data_generator = TUR.FakeL8DataGenerator(
            str(i2_tmpdir), tile_name, ["20200101", "20200120"]
        )
        data_generator.generate_data()

        args = {
            "tile_name": "T31TCJ",
            "target_proj": 2154,
            "all_tiles": "T31TCJ",
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None,
        }

        l8_sensor = Landsat8(**args)
        (features_app, _), features_labels = l8_sensor.get_features()
        features_app.ExecuteAndWriteOutput()

        assert [label.lower() for label in self.expected_l8_labels] == [
            str(feat) for feat in features_labels
        ], "Landsat_8 class broken, wrong features' labels"

        expected_output = Path(
            delete_compression_suffix(features_app.GetParameterString("out"))
        )
        self.assert_file_exist(expected_output)

    def test_instance_user_features(self, i2_tmpdir: Path) -> None:
        """Tests if the class user_features can be instantiated."""
        tile_name = "T31TCJ"
        patterns = ["NUMBEROFTHINGS", "LAI"]
        TUR.generate_fake_user_features_data(str(i2_tmpdir), tile_name, patterns)
        args = {
            "tile_name": tile_name,
            "target_proj": 2154,
            "all_tiles": "T31TCJ",
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": None,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": None,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": [],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "patterns": patterns,
            "working_resolution": None,
        }
        user_feat_sensor = UserFeatures(**args)
        (user_feat_stack, _), features_labels = user_feat_sensor.get_features()
        user_feat_stack.ExecuteAndWriteOutput()
        assert self.expected_user_features_labels == [
            str(elem) for elem in features_labels
        ], "user_features class broken, wrong features' labels"

        expected_output = Path(
            delete_compression_suffix(user_feat_stack.GetParameterString("out"))
        )
        self.assert_file_exist(expected_output)

    def test_instance_s2_l3a(self, i2_tmpdir: Path) -> None:
        """Tests if the class sentinel_2_l3a can be instantiated."""
        tile_name = "T31TCJ"
        data_generator = TUR.FakeS2L3ADataGenerator(
            str(i2_tmpdir),
            tile_name,
            ["20200101", "20200120"],
            res=10.0,
            array_name="ones",
        )
        data_generator.generate_data()
        args = {
            "tile_name": "T31TCJ",
            "target_proj": 2154,
            "all_tiles": "T31TCJ",
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None,
        }

        s2_l3a_sensor = Sentinel2L3a(**args)
        (features_app, _), features_labels = s2_l3a_sensor.get_features()
        features_app.ExecuteAndWriteOutput()

        assert [label.lower() for label in self.expected_s2_l3a_labels] == [
            str(feat) for feat in features_labels
        ], "Sentinel 2 L3A class broken, wrong features' labels"

        expected_output = Path(
            delete_compression_suffix(features_app.GetParameterString("out"))
        )
        self.assert_file_exist(expected_output)

    def test_instance_l8_old(self, i2_tmpdir: Path) -> None:
        """Tests if the class landsat_8_old can be instantiated."""
        tile_name = "France-MetropoleD0005H0002"
        data_generator = TUR.FakeL8OldDataGenerator(
            str(i2_tmpdir), tile_name, ["20200101", "20200120"]
        )
        data_generator.generate_data()
        args = {
            "tile_name": tile_name,
            "target_proj": 2154,
            "all_tiles": tile_name,
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None,
        }

        l8_sensor = Landsat8Old(**args)
        (features_app, _), features_labels = l8_sensor.get_features()
        features_app.ExecuteAndWriteOutput()
        assert [label.lower() for label in self.expected_l8_old_labels] == [
            str(feat) for feat in features_labels
        ], "Landsat_ 8 old class broken, wrong features' labels"

        expected_output = Path(
            delete_compression_suffix(features_app.GetParameterString("out"))
        )
        self.assert_file_exist(expected_output)

    def test_instance_l5_old(self, i2_tmpdir: Path) -> None:
        """Tests if the class landsat_5_old can be instantiated."""
        tile_name = "France-MetropoleD0005H0002"
        data_generator = TUR.FakeL5OldDataGenerator(
            str(i2_tmpdir), tile_name, ["20200101", "20200120"]
        )
        data_generator.generate_data()
        args = {
            "tile_name": tile_name,
            "target_proj": 2154,
            "all_tiles": tile_name,
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None,
        }

        l5_sensor = Landsat5Old(**args)
        (features_app, _), features_labels = l5_sensor.get_features()
        features_app.ExecuteAndWriteOutput()
        assert [label.lower() for label in self.expected_l5_old_labels] == [
            str(feat) for feat in features_labels
        ], "Landsat_ 5 old class broken, wrong features' labels"

        expected_output = Path(
            delete_compression_suffix(features_app.GetParameterString("out"))
        )
        self.assert_file_exist(expected_output)

    def test_sensors_container(self, i2_tmpdir: Path) -> None:
        """Test if the sensors_container class enable all required sensors."""
        tile_name = "T31TCJ"

        # sentinel_2_l3a object needs data to be instantiate.
        data_generator = TUR.FakeS2L3ADataGenerator(
            str(i2_tmpdir), tile_name, ["20200101"], res=10.0
        )
        data_generator.generate_data()
        args = {
            "Sentinel_2": {
                "tile_name": tile_name,
                "target_proj": 2154,
                "all_tiles": tile_name,
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": "",
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None,
            },
            "Sentinel_2_S2C": {
                "tile_name": "T31TCJ",
                "target_proj": 2154,
                "all_tiles": "T31TCJ",
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": None,
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None,
            },
            "Landsat8": {
                "tile_name": "T31TCJ",
                "target_proj": 2154,
                "all_tiles": "T31TCJ",
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": None,
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None,
            },
            "Sentinel_2_L3A": {
                "tile_name": "T31TCJ",
                "target_proj": 2154,
                "all_tiles": "T31TCJ",
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": None,
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None,
            },
            "Landsat5_old": {
                "tile_name": "T31TCJ",
                "target_proj": 2154,
                "all_tiles": "T31TCJ",
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": None,
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None,
            },
            "Landsat8_old": {
                "tile_name": "T31TCJ",
                "target_proj": 2154,
                "all_tiles": "T31TCJ",
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": None,
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None,
            },
        }
        sensors = SensorsContainer(tile_name, str(i2_tmpdir), str(i2_tmpdir), **args)
        enabled_sensors = sensors.get_enabled_sensors()
        sensors_name = [sensor.name for sensor in enabled_sensors]

        assert sensors_name == self.expected_sensors
