#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test the confusion_fusion function."""

import filecmp
import os
import shutil
from pathlib import Path

from iota2.validation import confusion_fusion as confFus

IOTA2DIR_AS_STR = os.environ.get("IOTA2DIR")
if not IOTA2DIR_AS_STR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR_AS_STR)


class Iota2TestConfFusion:
    """Test confusion_fusion function."""

    ref_data: str

    @classmethod
    def setup_class(cls) -> None:
        """Variables shared between tests."""
        cls.ref_data = str(IOTA2DIR / "data" / "references" / "ConfFusion")

    def test_confusion_fusion(self, i2_tmpdir: Path) -> None:
        """Test confusion_fusion function."""
        # Prepare data
        final = i2_tmpdir / "final"
        final_tmp = final / "TMP"

        final_tmp.mkdir(parents=True, exist_ok=True)

        # copy input data
        src_files = (Path(self.ref_data) / "Input" / "final" / "TMP").iterdir()
        for file_name in src_files:
            shutil.copy(file_name, final_tmp)

        # run test
        datafield = "CODE"
        shape_data = str(
            IOTA2DIR / "data" / "references" / "D5H2_groundTruth_samples.shp"
        )
        confFus.confusion_fusion(shape_data, datafield, str(final / "TMP"), 1)

        file1 = str(Path(final) / "TMP" / "Results_seed_0.txt")
        referencefile1 = str(Path(self.ref_data) / "Output" / "Results_seed_0.txt")
        assert filecmp.cmp(file1, referencefile1)
