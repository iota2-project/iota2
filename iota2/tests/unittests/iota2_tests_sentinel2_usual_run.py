#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest running_Tests
"""
Tests dedicated to launch iota2 runs
"""
import logging
import os
import shutil
from pathlib import Path

import pytest

import iota2.tests.utils.tests_utils_rasters as TUR
from config import Config
from iota2.Iota2 import Iota2JobParameters, run
from iota2.tests.utils.tests_utils_files import check_expected_i2_results
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR

LOGGER = logging.getLogger("distributed.worker")


class Iota2FullS2Run:
    """Tests dedicated to launch iota2 runs."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_ref = str(
            Path(IOTA2DIR) / "data" / "references" / "running_iota2" / "i2_config.cfg"
        )
        cls.config_ref_scikit = str(
            Path(IOTA2DIR)
            / "data"
            / "references"
            / "running_iota2"
            / "i2_config_scikit.cfg"
        )
        cls.ground_truth_path = str(
            Path(IOTA2DIR)
            / "data"
            / "references"
            / "running_iota2"
            / "ground_truth.shp"
        )
        cls.nomenclature_path = str(
            Path(IOTA2DIR)
            / "data"
            / "references"
            / "running_iota2"
            / "nomenclature.txt"
        )
        cls.color_path = str(
            Path(IOTA2DIR) / "data" / "references" / "running_iota2" / "color.txt"
        )

    def test_too_few_s2_data(self, i2_tmpdir: Path) -> None:
        """
        Tests iota2's run using s2 (theia format)
        and perform data augmentation algorithm
        """
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        data_generator = TUR.FakeS2DataGenerator(
            fake_s2_theia_dir, "T31TCJ", ["20200101", "20200112", "20200127"], res=10.0
        )
        data_generator.generate_data()
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(config_test).as_dict()
        cfg_test["chain"]["output_path"] = running_output_path
        cfg_test["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test["chain"]["data_field"] = "code"
        cfg_test["chain"]["spatial_resolution"] = 10
        cfg_test["chain"]["list_tile"] = tile_name
        cfg_test["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test["chain"]["minimum_required_dates"] = 666
        cfg_test["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test["chain"]["color_table"] = self.color_path

        with open(config_test, "w", encoding="UTF-8") as file_cfg:
            file_cfg.write(str(cfg_test))
        job_parameters = Iota2JobParameters(
            config_path=config_test,
            scheduler_type="debug",
            starting_step=1,
            ending_step=20,
            restart=False,
        )
        # Launch the chain
        pytest.raises(
            ValueError,
            run,
            job_parameters,
            [],
            False,
            1,
            None,
        )

    def test_usual_s2_theia_run(self, i2_tmpdir: Path) -> None:
        """
        Tests iota2's run using s2 (theia format)
        and perform data augmentation algorithm
        """
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        data_generator = TUR.FakeS2DataGenerator(
            fake_s2_theia_dir, "T31TCJ", ["20200101", "20200112", "20200127"], res=10.0
        )
        data_generator.generate_data()
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(config_test).as_dict()
        cfg_test["chain"]["output_path"] = running_output_path
        cfg_test["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test["chain"]["data_field"] = "code"
        cfg_test["chain"]["spatial_resolution"] = 10
        cfg_test["chain"]["list_tile"] = tile_name
        cfg_test["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test["chain"]["color_table"] = self.color_path

        with open(config_test, "w", encoding="UTF-8") as file_cfg:
            file_cfg.write(str(cfg_test))

        env_var_name = "WDIR"
        working_directory = str(i2_tmpdir / "working_dir")
        Path(working_directory).mkdir()
        os.environ[env_var_name] = working_directory
        # Launch the chain
        job_parameters = Iota2JobParameters(
            config_path=config_test,
            scheduler_type="debug",
            starting_step=1,
            ending_step=20,
            restart=False,
        )
        run(job_parameters, [], False, 1, env_var_name)
        i2_results, _ = check_expected_i2_results(Path(running_output_path) / "final")
        assert i2_results

        assert Path.exists(
            Path(running_output_path)
            / "learningSamples"
            / "class_statistics_seed0_learn.csv"
        )
