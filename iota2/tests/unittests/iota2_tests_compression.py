#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Test otb compression methods
"""
import importlib
import shutil
from pathlib import Path

from osgeo import gdal

from iota2.common import compression_options, otb_app_bank
from iota2.common.file_utils import (
    TileMergeOptions,
    assemble_tile_merge,
    get_raster_resolution,
)
from iota2.common.otb_app_bank import AvailableOTBApp
from iota2.configuration_files import read_config_file as rcf
from iota2.sequence_builders.i2_features_map import I2FeaturesMap
from iota2.tests.utils import tests_utils_rasters as TUR
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestCompression:
    """Test compression steps."""

    def test_set_compression_dico(self, i2_tmpdir: Path) -> None:
        """test_set_compression_dico."""
        config_ref = str(
            Path(IOTA2DIR)
            / "data"
            / "references"
            / "running_iota2"
            / "i2_config_feat_map.cfg"
        )
        tile_name = "T31TCJ"
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        data_generator = TUR.FakeS2DataGenerator(
            fake_s2_theia_dir, tile_name, ["20200101"], res=10.0
        )
        data_generator.generate_data()
        config_test = str(i2_tmpdir / "i2_config_compression.cfg")
        shutil.copy(config_ref, config_test)
        cfg_test = rcf.ReadInternalConfigFile(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = str(i2_tmpdir)
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["compression_algorithm"] = "LZW"
        cfg_test.cfg_as_dict["chain"]["compression_predictor"] = 1
        cfg_test.cfg_as_dict["external_features"]["module"] = str(
            Path(IOTA2DIR) / "data" / "numpy_features" / "user_custom_function.py"
        )
        cfg_test.save(config_test)
        chain = I2FeaturesMap(config_test, " ", " ")

        assert compression_options.compression_options.algorithm == "ZSTD"
        assert compression_options.compression_options.predictor == 2

        chain.set_compression_options()

        assert compression_options.compression_options.algorithm == "LZW"
        assert compression_options.compression_options.predictor == 1

    def test_compression_options_class(self) -> None:
        """test_compression_options_class."""
        compression_options_test = compression_options.CompressionOptions()

        assert compression_options_test.algorithm == "ZSTD"
        assert compression_options_test.predictor == 2

        compression_options_test.algorithm = "LZW"
        compression_options_test.predictor = 1

        assert compression_options_test.algorithm == "LZW"
        assert compression_options_test.predictor == 1

        compression_options_test.algorithm = "ZSTD"
        compression_options_test.predictor = 2

        assert compression_options_test.algorithm == "LZW"
        assert compression_options_test.predictor == 1

    def test_compression_otb_decorator(self, i2_tmpdir: Path) -> None:
        """test_compression_otb_decorator."""
        importlib.reload(compression_options)
        importlib.reload(otb_app_bank)

        compression_options.compression_options.algorithm = "ZSTD"
        compression_options.compression_options.predictor = 2

        raster_test = str(i2_tmpdir / "test.tif")
        output_raster = str(i2_tmpdir / "fake_output_path.tif")
        TUR.generate_fake_raster(raster_test, res=10)

        band_math_app, _ = otb_app_bank.create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": [raster_test],
                "out": output_raster,
                "exp": "im1b1",
                "pixType": "uint8",
            },
        )

        expected_output = (
            f"{output_raster}?&gdal:co:COMPRESS=ZSTD&gdal:co:PREDICTOR=2"
            "&gdal:co:BIGTIFF=YES"
        )

        assert (
            band_math_app.GetParameterAsString(
                otb_app_bank.get_input_parameter_output(band_math_app)
            )
            == expected_output
        )

        otb_app_bank.execute_app(band_math_app)
        compress_raster = gdal.Open(output_raster)
        metadata = compress_raster.GetMetadata("IMAGE_STRUCTURE")

        assert metadata.get("COMPRESSION") == "ZSTD"

        assert metadata.get("PREDICTOR") == "2"

        band_math_app1, _ = otb_app_bank.create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": [raster_test],
                "exp": "im1b1",
                "pixType": "uint8",
            },
        )

        band_math_app1.Execute()

        band_math_app2, _ = otb_app_bank.create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": band_math_app1,
                "out": output_raster,
                "exp": "im1b1",
                "pixType": "uint8",
            },
        )

        assert (
            band_math_app2.GetParameterAsString(
                otb_app_bank.get_input_parameter_output(band_math_app)
            )
            == expected_output
        )

        otb_app_bank.execute_app(band_math_app)
        compress_raster = gdal.Open(output_raster)
        metadata = compress_raster.GetMetadata("IMAGE_STRUCTURE")

        assert metadata.get("COMPRESSION") == "ZSTD"

        assert metadata.get("PREDICTOR") == "2"

        importlib.reload(compression_options)
        importlib.reload(otb_app_bank)

        compression_options.compression_options.algorithm = "NONE"
        compression_options.compression_options.predictor = 2

        raster_test = str(i2_tmpdir / "test.tif")
        TUR.generate_fake_raster(raster_test, res=10)

        band_math_app, _ = otb_app_bank.create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": [raster_test],
                "out": output_raster,
                "exp": "im1b1",
                "pixType": "uint8",
            },
        )

        expected_output = (
            f"{output_raster}?&gdal:co:COMPRESS=NONE&gdal:co:PREDICTOR=2"
            "&gdal:co:BIGTIFF=YES"
        )

        assert (
            band_math_app.GetParameterAsString(
                otb_app_bank.get_input_parameter_output(band_math_app)
            )
            == expected_output
        )

        otb_app_bank.execute_app(band_math_app)
        compress_raster = gdal.Open(output_raster)
        metadata = compress_raster.GetMetadata("IMAGE_STRUCTURE")

        assert metadata.get("COMPRESSION") is None
        assert metadata.get("PREDICTOR") is None

    def test_delete_compression_suffix(self) -> None:
        """test_delete_compression_suffix."""
        path_with_compression_options = (
            "/jjjd?/ouioi/pp.-pp/fake_output_path.tif?&gdal:co:COMPRESS=NONE&gdal:co:PREDICTOR=2"
            "&gdal:co:BIGTIFF=YES"
        )
        path_without_compression_options = "/jjjd?/ouioi/pp.-pp/fake_output_path.tif"

        assert (
            compression_options.delete_compression_suffix(path_with_compression_options)
            == path_without_compression_options
        )
        assert (
            compression_options.delete_compression_suffix(
                path_without_compression_options
            )
            == path_without_compression_options
        )

    def test_mosaic_compression(self, i2_tmpdir: Path) -> None:
        """test_compression_in_function_assembleTile_Merge."""
        raster_to_merge_1 = str(i2_tmpdir / "raster_to_merge_1.tif")
        raster_to_merge_2 = str(i2_tmpdir / "raster_to_merge_2.tif")
        mosaic = str(i2_tmpdir / "mosaic.tif")
        shutil.copy(
            str(
                IOTA2DIR
                / "data"
                / "references"
                / "ClassificationShaping"
                / "Input"
                / "Classif"
                / "classif"
                / "Classif_D0005H0002_model_1_seed_0.tif"
            ),
            raster_to_merge_1,
        )
        shutil.copy(
            str(
                IOTA2DIR
                / "data"
                / "references"
                / "ClassificationShaping"
                / "Input"
                / "Classif"
                / "classif"
                / "Classif_D0005H0003_model_1_seed_0.tif"
            ),
            raster_to_merge_2,
        )

        importlib.reload(compression_options)

        compression_options.compression_options.algorithm = "ZSTD"
        compression_options.compression_options.predictor = 2

        spatial_resolution = get_raster_resolution(raster_to_merge_1)

        creation_options = {
            "COMPRESS": compression_options.compression_options.algorithm,
            "PREDICTOR": compression_options.compression_options.predictor,
            "BIGTIFF": "YES",
        }

        merge_options = TileMergeOptions(
            spatial_resolution=spatial_resolution,
            creation_options=creation_options,
        )
        assemble_tile_merge(
            [raster_to_merge_1, raster_to_merge_2], mosaic, merge_options
        )

        compress_raster = gdal.Open(mosaic)
        metadata = compress_raster.GetMetadata("IMAGE_STRUCTURE")

        assert metadata.get("COMPRESSION") == "ZSTD"
        assert metadata.get("PREDICTOR") == "2"
