#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Check class iota2_parameters"""
import os
from pathlib import Path

import pytest

import iota2.configuration_files.read_config_file as rcf

RM_IF_ALL_OK = True
IOTA2DIR_AS_STR = os.environ.get("IOTA2DIR")

if IOTA2DIR_AS_STR is None:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR_AS_STR)


@pytest.mark.config
class Iota2TestConfig:
    """Check rcf.iota2_parameters."""

    group_test_name: str
    config_ref: str
    config_test: str

    @classmethod
    def setup_class(cls) -> None:
        """Variables shared between tests."""
        cls.group_test_name = "iota2_test_config"
        cls.config_ref = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_config.cfg"
        )
        cls.config_test = str(
            IOTA2DIR / "config" / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )

    # Tests definitions
    def test_config_dictionaries(self, i2_tmpdir: Path) -> None:
        """Check iota2_parameters class."""
        i2_cfg = rcf.ReadInternalConfigFile(self.config_test)
        i2_cfg.cfg_as_dict["chain"]["nomenclature_path"] = self.config_test
        i2_cfg.cfg_as_dict["chain"]["color_table"] = self.config_test
        i2_cfg.cfg_as_dict["chain"]["region_path"] = self.config_test
        i2_cfg.cfg_as_dict["chain"]["ground_truth"] = self.config_test
        i2_cfg.cfg_as_dict["chain"]["user_feat_path"] = str(IOTA2DIR / "data")
        i2_cfg.cfg_as_dict["chain"]["l8_path"] = str(IOTA2DIR / "data")
        i2_cfg.cfg_as_dict["chain"]["l8_path_old"] = str(IOTA2DIR / "data")
        i2_cfg.cfg_as_dict["arg_train"]["sample_management"] = self.config_test
        i2_cfg.cfg_as_dict["arg_train"]["prev_features"] = self.config_test
        i2_cfg.cfg_as_dict["arg_train"][
            "annual_classes_extraction_source"
        ] = self.config_test
        i2_cfg.cfg_as_dict["arg_train"]["output_prev_features"] = self.config_test

        test_cfg = str(i2_tmpdir / "config_test.cfg")
        i2_cfg.save(test_cfg)
        sensors_parameters = rcf.Iota2Parameters(
            rcf.ReadConfigFile(test_cfg)
        ).get_sensors_parameters("T31TCJ")
        assert len(sensors_parameters.keys()) == 3
