#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Test of indecision_management for post classification fusion."""

import shutil
from pathlib import Path

import pytest

from iota2.classification import fusion as FUS
from iota2.classification import indecision_management as UM
from iota2.common import file_utils as fut
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_rasters import generate_custom_fake_raster
from iota2.typings.i2_types import DBInfo


class Iota2TestIndecisionManagement(AssertsFilesUtils):
    """Test indecision management in post classification fusion."""

    @pytest.mark.parametrize(
        "no_label_management",
        [
            "learningPriority",
            "maxConfidence",
        ],
    )
    def test_indecision_management(
        self, i2_tmpdir: Path, no_label_management: str
    ) -> None:
        """Test indecision_management."""

        shutil.copytree(
            str(
                IOTA2DIR
                / "data"
                / "references"
                / "NoData"
                / "Input"
                / "Classif"
                / "classif"
            ),
            i2_tmpdir / "classif",
        )
        classif_dir = i2_tmpdir / "classif"
        classif_mask_dir = classif_dir / "MASK"
        shutil.copytree(
            str(IOTA2DIR / "data" / "references" / "NoData" / "Input" / "config_model"),
            str(i2_tmpdir / "config_model"),
        )

        Path.mkdir(i2_tmpdir / "cmd" / "fusion", parents=True, exist_ok=True)
        Path.mkdir(classif_mask_dir, parents=True, exist_ok=True)

        shutil.copy(
            str(classif_dir / "Classif_D0005H0002_model_1_seed_0.tif"),
            str(classif_dir / "Classif_D0005H0002_model_1f2_seed_0.tif"),
        )
        shutil.copy(
            str(classif_dir / "Classif_D0005H0002_model_1_seed_0.tif"),
            str(classif_dir / "Classif_D0005H0002_model_1f1_seed_0.tif"),
        )

        shutil.copy(
            str(classif_dir / "D0005H0002_model_1_confidence_seed_0.tif"),
            str(classif_dir / "D0005H0002_model_1f2_confidence_seed_0.tif"),
        )
        shutil.copy(
            str(classif_dir / "D0005H0002_model_1_confidence_seed_0.tif"),
            str(classif_dir / "D0005H0002_model_1f1_confidence_seed_0.tif"),
        )

        generate_custom_fake_raster(
            (570420, 571920),
            (6262890, 6264390),
            30,
            classif_mask_dir / "MASK_region_1_D0005H0002.tif",
        )
        generate_custom_fake_raster(
            (570420, 571920),
            (6262890, 6264390),
            30,
            classif_mask_dir / "MASK_region_2_D0005H0002.tif",
        )

        in_classif = [
            str(classif_dir / "Classif_D0005H0002_model_1f1_seed_0.tif"),
            str(classif_dir / "Classif_D0005H0002_model_1f2_seed_0.tif"),
        ]

        out_classif = str(classif_dir / "D0005H0002_FUSION_model_1_seed_0.tif")
        out_pix_type = "uint8"
        fusion_options = "-nodatalabel 0 -method majorityvoting"
        FUS.fusion(in_classif, fusion_options, out_classif, out_pix_type)

        fusion_files = fut.file_search_and(str(classif_dir), True, "_FUSION_")
        pixtype = "uint8"

        for fusionpath in fusion_files:
            UM.indecision_management(
                path_test=str(i2_tmpdir),
                path_fusion=fusionpath,
                region_db=DBInfo(
                    "",
                    str(
                        IOTA2DIR
                        / "data"
                        / "references"
                        / "running_iota2"
                        / "region.shp"
                    ),
                    region_field="region",
                ),
                path_to_img=str(IOTA2DIR / "data" / "references" / "features"),
                no_label_management=no_label_management,
                path_wd="",
                list_indices=["NDVI", "NDWI", "Brightness"],
                pix_type=pixtype,
                user_feat_pattern="ALT,ASP,SLP",
            )

        self.assert_file_equal(
            str(classif_dir / "D0005H0002_FUSION_model_1_seed_0.tif"),
            str(
                IOTA2DIR
                / "data"
                / "references"
                / "NoData"
                / "Output"
                / "D0005H0002_FUSION_model_1_seed_0.tif"
            ),
        )
