#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module testing iota2's utils functions"""
from pathlib import Path

from iota2.common.file_utils import is_dir_empty


def test_is_empty(i2_tmpdir: Path):
    """Test the 'is_empty()' method"""

    test_dir = i2_tmpdir / "test_dir"
    test_dir.mkdir()
    assert is_dir_empty(test_dir)

    test_file_path = test_dir / "test_file.txt"

    with open(test_file_path, "w", encoding="UTF-8") as file:
        file.write("test")

    assert test_file_path.is_file()
    assert is_dir_empty(test_dir) is False
