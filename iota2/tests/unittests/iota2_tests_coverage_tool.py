#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test the code coverage tool."""
from pathlib import Path
from unittest.mock import patch

import pandas as pd
import pytest

from iota2.common.tools.code_coverage_analysis import (
    arg_parser,
    color_values,
    compute_coverage_diff,
    convert_nans_diff,
    get_cov_df,
    main,
)
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_files import generate_code_coverage_file
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.typings.i2_types import PathLike


class Iota2TestCoverageTool(AssertsFilesUtils):
    """Test the code coverage tool"""

    @classmethod
    def setup_class(cls) -> None:
        """Variables shared between tests."""
        cls.ref_json = IOTA2DIR / "data" / "references" / "code_coverage_reference.json"

    @staticmethod
    def generate_test_files(dir_path: PathLike) -> tuple[Path, Path]:
        """Generate required json test files"""
        dir_path.mkdir(exist_ok=True)
        develop_file = Path(dir_path) / "develop_file.json"
        merge_file = Path(dir_path) / "merge_file.json"
        generate_code_coverage_file(develop_file)
        generate_code_coverage_file(merge_file)
        return develop_file, merge_file

    @staticmethod
    @pytest.mark.parametrize(
        "percent_covered, merge_cov, threshold, expected_string",
        [
            (float("nan"), 50, 75, "\033[91m50 %\033[0m"),
            (float("nan"), 75, 50, "\033[92m75 %\033[0m"),
            (-10, 50, 50, "\033[91m-10 \u2B07\033[0m"),
            (0, 50, 50, "0"),
            (10, 50, 50, "\033[92m10 \u2B06\033[0m"),
        ],
    )
    def test_get_color(
        percent_covered: float, merge_cov: float, threshold: float, expected_string: str
    ) -> None:
        """Test the `get_color` function"""
        assert color_values(percent_covered, merge_cov, threshold) == expected_string

    @staticmethod
    def test_convert_nans() -> None:
        """Test the `convert_nans_diff` function"""
        assert convert_nans_diff(percent_covered=float("nan"), merge_cov=50) == 50
        assert convert_nans_diff(percent_covered=10, merge_cov=50) == 10

    def test_get_cov_df(self) -> None:
        """Test the `get_cov_df` function"""
        ref_df = pd.Series(
            data=[50.0, 77.5, 25.0],
            index=[
                "iota2/file1.py",
                "iota2/file2.py",
                "iota2/file3.py",
            ],
            name="percent_covered",
        )
        df_to_compare = get_cov_df(self.ref_json)
        pd.testing.assert_series_equal(ref_df, df_to_compare)

    @staticmethod
    def test_arg_parser_defaults() -> None:
        """Test the `arg_parser` function"""
        parser = arg_parser()
        args = parser.parse_args([])
        assert args.develop is None
        assert args.merge is None
        assert args.new_file_threshold == 75.0
        assert args.ignore_threshold == 0.2
        assert args.html_file is None

    @staticmethod
    def test_arg_parser_custom_values() -> None:
        """Test `arg_parser` function"""
        parser = arg_parser()
        test_args = [
            "-develop",
            "path/to/develop.json",
            "-merge",
            "path/to/merge.json",
            "-new_file_threshold",
            "80",
            "-ignore_threshold",
            "0.1",
            "-html_file",
            "results.html",
        ]
        args = parser.parse_args(test_args)
        assert args.develop == "path/to/develop.json"
        assert args.merge == "path/to/merge.json"
        assert args.new_file_threshold == 80.0
        assert args.ignore_threshold == 0.1
        assert args.html_file == "results.html"

    @staticmethod
    @pytest.mark.parametrize(
        "develop_coverage, merge_coverage, "
        "new_files_threshold, ignore_threshold, "
        "html_file_path, expected_exception",
        [
            ("", "", 50, 50, "", FileNotFoundError),  # wrong json file(s)
            ("", "", -50, 50, "", AssertionError),  # wrong new file threshold
            ("", "", 50, 1000, "", AssertionError),  # wrong ignore threshold
            ("", "", -50, 1000, "", AssertionError),  # wrong both threshold
        ],
    )
    def test_wrong_inputs(
        develop_coverage: PathLike,
        merge_coverage: PathLike,
        new_files_threshold: float,
        ignore_threshold: float,
        html_file_path: PathLike,
        expected_exception: type[Exception],
    ) -> None:
        """Test the compute_coverage_diff when wrong inputs are used"""
        with pytest.raises(expected_exception):
            compute_coverage_diff(
                develop_coverage,
                merge_coverage,
                new_files_threshold,
                ignore_threshold,
                html_file_path,
            )

    def test_compute_diff(self, i2_tmpdir) -> None:
        """Test the `compute_coverage_diff` function"""
        develop_file, merge_file = self.generate_test_files(i2_tmpdir)
        html_file = Path(i2_tmpdir) / "results.html"
        bad_files, good_files = compute_coverage_diff(
            develop_file, merge_file, html_file_path=html_file
        )
        assert bad_files >= 0
        assert good_files >= 0
        assert html_file.exists()

    def test_run_main(self, i2_tmpdir) -> None:
        """Test running the main process of the tool, including parsing from the terminal"""
        develop_file, merge_file = self.generate_test_files(i2_tmpdir)
        html_file = Path(i2_tmpdir) / "results.html"
        with patch(
            "sys.argv",
            [
                "script_name",
                "-develop",
                str(develop_file),
                "-merge",
                str(merge_file),
                "-new_file_threshold",
                "75",
                "-ignore_threshold",
                "0.2",
                "-html_file",
                str(html_file),
            ],
        ), patch(
            "iota2.common.tools.code_coverage_analysis.compute_coverage_diff",
            side_effect=compute_coverage_diff,
        ) as mock_compute_coverage_diff:
            with pytest.raises(SystemExit):
                main()

            mock_compute_coverage_diff.assert_called_once_with(
                str(develop_file),
                str(merge_file),
                75.0,
                0.2,
                str(html_file),
            )
