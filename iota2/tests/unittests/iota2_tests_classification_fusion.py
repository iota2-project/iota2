#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module used to merge different types of files"""
import shutil
from pathlib import Path

from iota2.classification.fusion import (
    compute_confusion_of_fusion,
    get_parameters_for_fusion,
    perform_fusion,
)
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_rasters import generate_custom_fake_raster
from iota2.validation.results_utils import gen_confusion_matrix_fig


class Iota2TestClassificationsFusion:
    """Test the classification fusion workflow."""

    @classmethod
    def setup_class(cls) -> None:
        """Variables shared between tests."""
        cls.ref_vector = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "ground_truth.shp"
        )
        cls.confusion_matrix = str(
            IOTA2DIR / "data" / "references" / "opt_confusion.csv"
        )
        cls.nomenclature_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "nomenclature.txt"
        )

    def test_fusion(self, i2_tmpdir: Path):
        """
        Test the fusion workflow (fusion of classifications and producing the corresponding
        confusion matrix)
        """
        list_iota_dirs = []
        output_dir = str(i2_tmpdir / "fusion")
        Path(output_dir).mkdir()
        # Generate 3 classifications and confusion matrices
        for nb_classif in range(3):
            # Generate classification
            iota2_dir = i2_tmpdir / f"result_{nb_classif}"
            list_iota_dirs.append(str(iota2_dir))
            final_dir = iota2_dir / "final"
            final_dir.mkdir(parents=True)
            classif_path = final_dir / "Classif_Seed_0.tif"
            x_coords = (566377, 567237)
            y_coords = (6283869, 6284029)
            generate_custom_fake_raster(
                x_coords, y_coords, resolution=10, output_file_path=classif_path
            )

            # Copy confusion matrix
            final_tmp_dir = final_dir / "TMP"
            final_tmp_dir.mkdir()
            confusion_matrix_path = final_tmp_dir / "Classif_Seed_0.csv"
            shutil.copy(self.confusion_matrix, confusion_matrix_path)
        list_classifs, list_confusion_matrices = get_parameters_for_fusion(
            list_iota_dirs
        )
        final_classif = perform_fusion(
            list_classifs=list_classifs,
            list_confusion_matrices=list_confusion_matrices,
            output_dir=output_dir,
            method="dempstershafer",
        )
        assert Path(final_classif).exists()
        confusion_matrix = compute_confusion_of_fusion(
            ref_vector=self.ref_vector,
            classification=final_classif,
            data_field="code",
            output_dir=output_dir,
        )
        assert Path(confusion_matrix).exists()
        output_image = f"{output_dir}/final_confusion_matrix.png"
        gen_confusion_matrix_fig(
            confusion_matrix,
            self.nomenclature_path,
            output_image,
        )
        assert Path(output_image).exists()
