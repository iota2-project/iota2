# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test sensors container"""
import os
import shutil
from pathlib import Path

import pytest

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.common import iota2_directory
from iota2.common.file_utils import file_search_and
from iota2.common.raster_utils import raster_to_array
from iota2.configuration_files import read_config_file as rcf
from iota2.sensors.sensorscontainer import SensorsContainer
from iota2.tests.utils.tests_utils_rasters import compute_brightness_from_vector

IOTA2DIR_AS_STR = os.environ.get("IOTA2DIR")
if not IOTA2DIR_AS_STR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR_AS_STR)


@pytest.mark.sensors
class Iota2TestS2STSensor:
    """Check sensors container."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_test = str(
            IOTA2DIR / "config" / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        cls.MTD_files = [
            str(IOTA2DIR / "data" / "MTD_MSIL2A_20190506.xml"),
            str(IOTA2DIR / "data" / "MTD_MSIL2A_20190501.xml"),
        ]

    def test_sensor(self, i2_tmpdir: Path) -> None:
        """Test sensors container.

        Check if the sensors container is fully operational
        by writing on disk the features stack.
        """
        # s2 sen2cor data
        TUR.generate_fake_s2_s2c_data(str(i2_tmpdir), "T31TCJ", self.MTD_files)

        # config file
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        shutil.copy(self.config_test, config_path_test)

        s2st_data = str(i2_tmpdir)
        test_path = str(i2_tmpdir / "RUN")
        cfg_test = rcf.ReadInternalConfigFile(config_path_test)
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = str(
            IOTA2DIR / "data" / "references" / "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["chain"]["color_table"] = config_path_test
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["chain"]["output_path"] = test_path
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = None
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["s2_path"] = None
        cfg_test.cfg_as_dict["chain"]["s2_s2c_path"] = s2st_data
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test.save(config_path_test)

        iota2_directory.generate_directories(test_path, False, ["T31TCJ"])

        # Launch test
        iota2_dico = rcf.Iota2Parameters(
            rcf.ReadConfigFile(config_path_test)
        ).get_sensors_parameters("T31TCJ")
        sensors = SensorsContainer("T31TCJ", None, str(i2_tmpdir), **iota2_dico)
        sensors.sensors_preprocess()

        # produce the time series
        time_s = sensors.get_sensors_time_series()
        for _, ((time_s_app, __), _) in time_s:
            time_s_app.ExecuteAndWriteOutput()
        # produce the time series gapFilled
        time_s_g = sensors.get_sensors_time_series_gapfilling()
        for _, ((time_s_g_app, __), _) in time_s_g:
            time_s_g_app.ExecuteAndWriteOutput()
        # produce features
        features = sensors.get_sensors_features()
        for _, ((features_app, __), _) in features:
            features_app.ExecuteAndWriteOutput()

        feature_array = raster_to_array(
            file_search_and(str(test_path), True, "_Features.tif")[0]
        )
        data_value, brightness_value = feature_array[:, 0, 2][0:-1], int(
            feature_array[:, 0, 2][-1]
        )
        theorical_brightness = int(compute_brightness_from_vector(data_value))
        assert theorical_brightness == brightness_value
