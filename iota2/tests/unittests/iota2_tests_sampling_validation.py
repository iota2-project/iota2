#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test non-regression sampling (stats, samples selection and extraction)"""
import shutil
import sqlite3
from pathlib import Path

import geopandas as gpd
import xmltodict

from iota2.common.custom_numpy_features import (
    ChunkParameters,
    ComputeCustomFeaturesArgs,
    CustomFeaturesConfiguration,
    CustomNumpyFeatures,
)
from iota2.common.raster_utils import ChunkConfig
from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import (
    samples_selection,
    samples_stat,
    vector_formatting,
    vector_sampler,
)
from iota2.sampling.vector_sampler import SampleFlagsParameters, build_output_file
from iota2.tests.utils import tests_utils_vectors as TUV
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.typings.i2_types import DBInfo
from iota2.vector_tools.vector_functions import copy_shapefile, list_value_fields


class Iota2TestSamplingWorkflow(AssertsFilesUtils):
    """Non regression tests which check stats, samples selection/extraction."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_data_folder = str(
            IOTA2DIR
            / "data"
            / "references"
            / "Runs_standards"
            / "Usual_s2_theia_run"
            / "test_results"
        )
        cls.s2ref_data_folder = str(
            IOTA2DIR
            / "data"
            / "references"
            / "Runs_standards"
            / "Usual_s2_theia_run"
            / "s2_data"
        )

        cls.ref_formattingVector_data_folder = str(
            IOTA2DIR
            / "data"
            / "references"
            / "formatting_vectors"
            / "Input"
            / "formattingVectors"
        )

        cls.config_ref = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_config.cfg"
        )
        cls.ground_truth_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "ground_truth.shp"
        )
        # cls.ground_truth_ndvi_path = str(
        #     IOTA2DIR / "data" / "references" / "running_iota2" / "ground_truth_ndvi.shp"
        # )

        cls.ground_truth_ndvi_path = str(
            IOTA2DIR
            / "data"
            / "references"
            / "formatting_vectors"
            / "Input"
            / "formattingVectors"
            / "T31TCJ.shp"
        )

        cls.nomenclature_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "nomenclature.txt"
        )
        cls.color_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "color.txt"
        )

    def test_compute_stats(self, i2_tmpdir: Path) -> None:
        """Check stats from otb."""
        shutil.copytree(self.ref_data_folder, i2_tmpdir, dirs_exist_ok=True)

        stats_file_name = "T31TCJ_region_1_seed_0_stats_val.xml"
        shutil.rmtree(
            i2_tmpdir / "samplesSelection" / stats_file_name,
            ignore_errors=True,
        )
        samples_stat.samples_stats(
            region_seed_tile=["1", "0", "T31TCJ"],
            iota2_directory=str(i2_tmpdir),
            data_field="i2label",
            sampling_validation=True,
            working_directory=None,
        )

        self.assert_file_exist(i2_tmpdir / "samplesSelection" / stats_file_name)
        with open(
            str(i2_tmpdir / "samplesSelection" / stats_file_name), encoding="UTF-8"
        ) as xml_run:
            dict_run = xmltodict.parse(xml_run.read())
        with open(
            str(Path(self.ref_data_folder) / "samplesSelection" / stats_file_name),
            encoding="UTF-8",
        ) as xml_ref:
            dict_ref = xmltodict.parse(xml_ref.read())
        assert dict_ref == dict_run

    def test_selection(self, i2_tmpdir: Path) -> None:
        """Samples_selection.samples_selection non regression test."""
        shutil.copytree(self.ref_data_folder, i2_tmpdir, dirs_exist_ok=True)
        shutil.rmtree(
            i2_tmpdir
            / "samplesSelection"
            / "T31TCJ_samples_region_1_val_seed_0_selection.sqlite",
            ignore_errors=True,
        )
        samples_selection.samples_selection(
            model=str(i2_tmpdir / "samplesSelection" / "samples_region_1_seed_0.shp"),
            working_directory=str(i2_tmpdir),
            output_path=str(i2_tmpdir),
            runs=1,
            masks_name="MaskCommunSL.tif",
            otb_parameters={"sampler": "random", "strategy": "all", "rand": 1},
            data_field="i2label",
            sampling_validation=True,
            parameters_validation={"sampler": "random", "strategy": "all"},
        )
        con = sqlite3.connect(
            str(
                i2_tmpdir
                / "samplesSelection"
                / "T31TCJ_samples_region_1_val_seed_0_selection.sqlite"
            )
        )
        query = "SELECT * FROM T31TCJ_samples_region_1_val_seed_0_selection"
        df_run = gpd.read_postgis(
            sql=query, con=con, geom_col="geometry", crs={"init": "EPSG:2154"}
        )
        con2 = sqlite3.connect(
            str(
                Path(self.ref_data_folder)
                / "samplesSelection"
                / "T31TCJ_samples_region_1_val_seed_0_selection.sqlite"
            )
        )

        df_ref = gpd.read_postgis(
            sql=query, con=con2, geom_col="geometry", crs={"init": "EPSG:2154"}
        )

        assert len(df_ref.index) == len(df_run.index)

    def test_validation_extraction(self, i2_tmpdir: Path) -> None:
        """Check extraction."""
        shutil.copytree(self.ref_data_folder, i2_tmpdir, dirs_exist_ok=True)
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg = rcf.ReadInternalConfigFile(config_test)
        cfg.cfg_as_dict["chain"]["output_path"] = str(i2_tmpdir)
        cfg.cfg_as_dict["chain"]["s2_path"] = self.s2ref_data_folder
        cfg.cfg_as_dict["chain"]["data_field"] = "code"
        cfg.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg.save(config_test)

        folder_sample = str(i2_tmpdir / "learningSamples")
        shutil.rmtree(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_learn.sqlite",
            ignore_errors=True,
        )
        shutil.rmtree(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_val.sqlite",
            ignore_errors=True,
        )
        samples = build_output_file(
            False,
            "sqlite",
            str(i2_tmpdir / "samplesSelection" / "T31TCJ_selection_merge.sqlite"),
            folder_sample,
        )
        vector_sampler.generate_samples_simple(
            DBInfo(
                "i2label",
                str(i2_tmpdir / "samplesSelection" / "T31TCJ_selection_merge.sqlite"),
                region_field="region",
            ),
            samples,
            path_wd=None,
            runs=1,
            sensors_parameters=rcf.Iota2Parameters(
                rcf.ReadConfigFile(config_test)
            ).get_sensors_parameters("T31TCJ"),
            flags_parameters=SampleFlagsParameters(
                force_standard_labels=False,
                features_from_raw_dates=False,
                sampling_validation=True,
            ),
            ram=128,
            custom_features=None,
        )
        self.assert_file_exist(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_val.sqlite"
        )

        self.assert_file_exist(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_learn.sqlite"
        )

        assert TUV.compare_sqlite(
            str(Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_val.sqlite"),
            str(
                Path(self.ref_data_folder)
                / "learningSamples"
                / "T31TCJ_region_1_seed0_Samples_val.sqlite"
            ),
            cmp_mode="table",
        )

    def test_validation_extraction_csv(self, i2_tmpdir):
        """Check extraction."""
        shutil.copytree(self.ref_data_folder, i2_tmpdir, dirs_exist_ok=True)
        shutil.copytree(
            self.ref_formattingVector_data_folder, i2_tmpdir, dirs_exist_ok=True
        )

        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg = rcf.ReadInternalConfigFile(config_test)
        cfg.cfg_as_dict["chain"]["output_path"] = str(i2_tmpdir)
        cfg.cfg_as_dict["chain"]["s2_path"] = self.s2ref_data_folder
        cfg.cfg_as_dict["chain"]["data_field"] = "code"
        cfg.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg.cfg_as_dict["external_features"]["functions"] = "gen_2000_features"

        cfg.save(config_test)

        folder_sample = str(i2_tmpdir / "learningSamples")

        shutil.rmtree(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_learn.sqlite",
            ignore_errors=True,
        )
        shutil.rmtree(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_val.sqlite",
            ignore_errors=True,
        )
        samples = build_output_file(
            False,
            "csv",
            str(i2_tmpdir / "formattingVectors" / "T31TCJ.shp"),
            folder_sample,
        )

        module_name = str(IOTA2DIR / "iota2" / "common" / "external_code.py")

        multi_param_cust = {
            "enable_raw": False,
            "enable_gap": True,
            "fill_missing_dates": False,
            "all_dates_dict": dict(
                [("Sentinel2", ["20200101", "20200111", "20200121"])]
            ),
            "mask_valid_data": None,
            "mask_value": 0,
            "exogeneous_data": None,
        }

        vector_sampler.generate_samples_simple(
            DBInfo("i2label", None, region_field="region"),
            samples,
            path_wd=None,
            runs=1,
            sensors_parameters=rcf.Iota2Parameters(
                rcf.ReadConfigFile(config_test)
            ).get_sensors_parameters("T31TCJ"),
            flags_parameters=SampleFlagsParameters(
                force_standard_labels=False,
                features_from_raw_dates=False,
                sampling_validation=True,
            ),
            ram=128,
            custom_features=ComputeCustomFeaturesArgs(
                chunk_params=ChunkParameters(
                    ChunkConfig(
                        chunk_size_mode="split_number",
                        number_of_chunks=1,
                        chunk_size_x=None,
                        chunk_size_y=None,
                    ),
                    None,
                ),
                functions_builder=CustomNumpyFeatures(
                    sensors_params=rcf.Iota2Parameters(
                        rcf.ReadConfigFile(config_test)
                    ).get_sensors_parameters("T31TCJ"),
                    module_name=module_name,
                    list_functions=[("gen_2000_features_interp", {})],
                    configuration=CustomFeaturesConfiguration(
                        enabled_raw=multi_param_cust["enable_raw"],
                        enabled_gap=multi_param_cust["enable_gap"],
                        fill_missing_dates=multi_param_cust["fill_missing_dates"],
                        concat_mode=False,
                    ),
                    all_dates_dict=multi_param_cust["all_dates_dict"],
                    exogeneous_data_file=multi_param_cust["exogeneous_data"],
                ),
                concatenate_features=False,
            ),
        )
        self.assert_file_exist(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_val.csv"
        )

        self.assert_file_exist(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_learn.csv"
        )

    def test_extract_maj_vote_samples(self, i2_tmpdir):
        """Test 'extract_maj_vote_samples' method"""
        ground_truth_ndvi_temp = str(i2_tmpdir / "ground_truth_ndvi.shp")
        copy_shapefile(
            self.ground_truth_ndvi_path,
            ground_truth_ndvi_temp,
        )
        data_field = "code"
        region_field = "region"
        extraction_ratio = 0.5
        vector_formatting.extract_maj_vote_samples(
            ground_truth_ndvi_temp,
            str(i2_tmpdir / "test_majvote_samples.sqlite"),
            extraction_ratio,
            data_field,
            region_field,
            random_seed=1,
        )

        # vector_formatting.extract_maj_vote_samples(
        #     ground_truth_ndvi_temp,
        #     str(i2_tmpdir / "test_majvote_samples.sqlite"),
        #     0.5,
        #     random_seed=1,
        #     data_field="split",
        #     region_field=0,
        # )

        listid_input = list_value_fields(ground_truth_ndvi_temp, "originfid")
        listid_val = list_value_fields(
            str(i2_tmpdir / "test_majvote_samples.sqlite"),
            "originfid",
            driver="SQLite",
        )
        assert set(listid_input).isdisjoint(
            listid_val
        ), "merge validation set intersects training set"
