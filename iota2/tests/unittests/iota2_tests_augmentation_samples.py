# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test samples otb samples augmentation."""
import shutil
from collections import Counter
from pathlib import Path

from iota2.sampling import data_augmentation
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.typings.i2_types import AugmentationParams
from iota2.vector_tools import vector_functions as vf


class Iota2TestSamplesAugmentation:
    """Test samples otb samples augmentation."""

    vector: str
    class_count: dict[int, int]
    csvFile: str

    @classmethod
    def setup_class(cls) -> None:
        """Variables shared between tests."""
        cls.vector = str(
            Path(IOTA2DIR)
            / "data"
            / "references"
            / "sampler"
            / "D0005H0002_polygons_To_Sample_Samples_ref_bindings.sqlite"
        )
        cls.vector_csv = str(
            Path(IOTA2DIR)
            / "data"
            / "references"
            / "sampler"
            / "D0005H0002_polygons_To_Sample_Samples_ref_bindings.csv"
        )
        cls.class_count = {51: 147, 11: 76, 12: 37, 42: 19}
        cls.csvFile = str(
            Path(IOTA2DIR) / "data" / "references" / "sampleAugmentation.csv"
        )

    def test_iota2_augmentation_counter(self) -> None:
        """Test how many samples must be added to the sample set est the 3 different strategies."""
        balance_expected = {42: 128, 11: 71, 12: 110}
        atleast_expected = {42: 101, 11: 44, 12: 83}
        byclass_expected = {42: 11, 51: 33, 12: 1}
        class_augmentation_balance = data_augmentation.samples_augmentation_counter(
            self.class_count, mode="balance", min_number=None, by_class=None
        )
        assert class_augmentation_balance == balance_expected

        class_augmentation_atleast = data_augmentation.samples_augmentation_counter(
            self.class_count, mode="minNumber", min_number=120, by_class=None
        )
        assert class_augmentation_atleast == atleast_expected

        class_augmentation_byclass = data_augmentation.samples_augmentation_counter(
            self.class_count, mode="byClass", min_number=None, by_class=self.csvFile
        )
        assert class_augmentation_byclass == byclass_expected

    def test_iota2_augmentation(self, i2_tmpdir: Path) -> None:
        """Test data augmentation workflow."""
        vector_test = str(i2_tmpdir / "vector_TEST_1_seed0.sqlite")
        shutil.copyfile(self.vector, vector_test)
        class_augmentation_balance = data_augmentation.samples_augmentation_counter(
            self.class_count, mode="balance", min_number=None, by_class=None
        )
        data_augmentation.do_augmentation(
            vector_test,
            class_augmentation_balance,
            sample_augmentation_params=AugmentationParams(
                "replicate",
                "code",
                [f"value_{elem}" for elem in range(2, 20)],
                10,
                None,
                1,
            ),
            working_directory=None,
        )
        class_count_test = Counter(
            vf.get_field_element(vector_test, elem_type=int, field="code", mode="all")
        )
        samples_number = self.class_count[
            max(self.class_count, key=lambda key: self.class_count[key])
        ]
        assert all(samples_number == v for k, v in list(class_count_test.items()))

    def test_iota2_augmentation_csv(self, i2_tmpdir):
        """Test data augmentation workflow."""
        vector_test = str(i2_tmpdir / "vector_TEST_1_seed0.csv")
        shutil.copyfile(self.vector_csv, vector_test)
        class_augmentation_balance = data_augmentation.samples_augmentation_counter(
            self.class_count, mode="balance", min_number=None, by_class=None
        )
        data_augmentation.do_augmentation(
            vector_test,
            class_augmentation_balance,
            sample_augmentation_params=AugmentationParams(
                "replicate",
                "code",
                [f"value_{elem}" for elem in range(2, 20)],
                10,
                None,
                1,
            ),
            working_directory=None,
        )
        class_count_test = Counter(
            vf.get_field_element(vector_test, elem_type=int, field="code", mode="all")
        )
        samples_number = self.class_count[
            max(self.class_count, key=lambda key: self.class_count[key])
        ]
        assert all(samples_number == v for k, v in list(class_count_test.items()))
