#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Test sampler applications
"""
import logging
import os
import shutil
from pathlib import Path

from osgeo import ogr

from iota2.common import file_utils as fut
from iota2.common.utils import TaskConfig
from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import vector_sampler
from iota2.sampling.vector_sampler import SampleFlagsParameters
from iota2.tests.utils import tests_utils_vectors as TUV
from iota2.tests.utils.tests_utils_files import copy_and_modify_config
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.typings.i2_types import DBInfo

IOTA2_DATATEST = str(IOTA2DIR / "data")


class Iota2TestSamplerApplications:
    """Class dedicated to launch several sampler tests"""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""

        cls.referenceShape = str(
            Path(IOTA2_DATATEST)
            / "references"
            / "sampler"
            / "D0005H0002_polygons_To_Sample.shp"
        )
        cls.referenceShape_test = str(
            Path(IOTA2_DATATEST) / "references" / "sampler" / "D0005H0002.shp"
        )
        cls.configSimple_NO_bindings = str(
            Path(IOTA2_DATATEST) / "config" / "test_config.cfg"
        )
        cls.configSimple_bindings = str(
            Path(IOTA2_DATATEST) / "config" / "test_config_bindings.cfg"
        )
        cls.configSimple_bindings_uDateFeatures = str(
            Path(IOTA2_DATATEST) / "config" / "test_config_bindings_uDateFeatures.cfg"
        )
        cls.MNT = str(Path(IOTA2_DATATEST) / "references" / "MNT")
        cls.expectedFeatures = {11: 74, 12: 34, 42: 19, 51: 147}
        cls.SensData = str(Path(IOTA2_DATATEST) / "L8_50x50")
        cls.iota2_directory = os.environ.get("IOTA2DIR")

        cls.selection_test = str(
            Path(IOTA2_DATATEST) / "references" / "sampler" / "D0005H0002.sqlite"
        )

        cls.multi_param_cust = {
            "enable_raw": False,
            "enable_gap": True,
            "fill_missing_dates": False,
            "all_dates_dict": {},
            "mask_valid_data": None,
            "mask_value": 0,
            "exogeneous_data": None,
        }

    @staticmethod
    def prepare_tests_folder(
        output_directory: str, working_directory: bool = False
    ) -> tuple:
        """
        Prepares and organizes the necessary test directories, creating and clearing any
        old files/directories as needed.

        Parameters
        ----------
        output_directory : str
            The directory where the test and output folders will be created.
            Test data and feature outputs will be stored here.
        working_directory : bool, optional
            If True, a temporary working directory will be created and returned.
            Default is False.

        Returns
        -------
        tuple
            A tuple containing three elements:

            - test_path : str
                Path to the test directory (simpleSampler_vector_bindings).
            - features_outputs : str
                Path to the feature outputs directory (simpleSampler_features_bindings).
            - w_d : str or None
                Path to the temporary working directory if `working_directory` is True,
                otherwise None.
        """
        w_d = None
        Path(output_directory).mkdir(exist_ok=True)
        test_path = str(Path(output_directory) / "simpleSampler_vector_bindings")
        if Path(test_path).exists():
            shutil.rmtree(test_path)
        Path(test_path).mkdir()
        (Path(test_path) / "features").mkdir(exist_ok=True, parents=True)
        features_outputs = str(
            Path(output_directory) / "simpleSampler_features_bindings"
        )
        if Path(features_outputs).exists():
            shutil.rmtree(features_outputs)
        Path(features_outputs).mkdir()
        if working_directory:
            w_d = str(Path(output_directory) / "simpleSampler_bindingsTMP")
            if Path(w_d).exists():
                shutil.rmtree(w_d)
            Path(w_d).mkdir()
        return test_path, features_outputs, w_d

    def test_sampler_simple_bindings(self, i2_tmpdir: Path) -> None:
        """
        Integration test for the sample generation process using the binding sampler.

        This test validates the sample generation from polygons and compares the output
        with a reference SQLite database. It uses a specific configuration file and modifies
        key parameters such as output paths and sampling parameters before generating the samples.
        """
        # load configuration file
        test_path, features_outputs, w_d = self.prepare_tests_folder(
            str(i2_tmpdir), True
        )
        config_path = str(
            Path(self.iota2_directory)
            / "config"
            / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )

        config_path_test = str(Path(w_d) / "Config_TEST.cfg")

        copy_and_modify_config(
            config_path,
            config_path_test,
            {
                "chain": {
                    "output_path": test_path,
                    "ground_truth": str(
                        Path(IOTA2DIR)
                        / "data"
                        / "references"
                        / "D5H2_groundTruth_samples.shp"
                    ),
                    "nomenclature_path": config_path_test,
                    "color_table": config_path_test,
                    "list_tile": "D0005H0002",
                    "l8_path_old": str(
                        Path(self.iota2_directory) / "data" / "L8_50x50"
                    ),
                    "l8_path": None,
                    "user_feat_path": None,
                    "region_field": "region",
                    "region_path": None,
                },
                "sensors_data_interpolation": {"use_additional_features": False},
                "arg_train": {"sample_management": None},
            },
        )
        (Path(features_outputs) / "D0005H0002" / "tmp").mkdir(
            exist_ok=True, parents=True
        )

        config = rcf.ReadConfigFile(config_path_test)
        data_field = config.get_param("chain", "data_field")
        output_path = config.get_param("chain", "output_path")
        region_field = (config.get_param("chain", "region_field")).lower()
        runs = config.get_param("arg_train", "runs")

        ram = 128
        sensors_params = rcf.Iota2Parameters(
            rcf.ReadConfigFile(config_path_test)
        ).get_sensors_parameters("D0005H0002")

        ds = ogr.Open(self.referenceShape_test)
        drv_region = ds.GetDriver()
        test_ref_file = str(i2_tmpdir / "ref_shape.shp")
        drv_region.CopyDataSource(ds, test_ref_file)

        test_selection = str(i2_tmpdir / "D0005H0002.sqlite")
        shutil.copy(self.selection_test, test_selection)

        vector_sampler.generate_samples(
            DBInfo(data_field, test_ref_file, region_field=region_field),
            output_path,
            None,
            runs,
            sensors_params,
            SampleFlagsParameters(sampling_validation=False),
            TaskConfig(logging.getLogger(), ram),
            test_selection,
        )

        # assert
        test_vector = fut.file_search_reg_ex(test_path + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(
            test_vector,
            str(
                Path(IOTA2_DATATEST)
                / "references"
                / "sampler"
                / "D0005H0002_polygons_To_Sample_Samples_ref_bindings.sqlite"
            ),
            cmp_mode="coordinates",
        )
        assert compare
        del config

    def test_sampler_simple_bindings_temporary_data(self, i2_tmpdir: Path) -> None:
        """
        Integration test for the sample generation process using the binding sampler
        and writing temporary data.

        """
        config_path = str(
            Path(self.iota2_directory)
            / "config"
            / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        reference = str(
            Path(IOTA2_DATATEST)
            / "references"
            / "sampler"
            / "D0005H0002_polygons_To_Sample_Samples_ref_bindings.sqlite"
        )
        test_path, features_outputs, _ = self.prepare_tests_folder(
            str(i2_tmpdir),
        )
        config_path_test = str(Path(test_path) / "Config_TEST.cfg")
        copy_and_modify_config(
            config_path,
            config_path_test,
            {
                "chain": {
                    "ground_truth": str(
                        Path(IOTA2DIR)
                        / "data"
                        / "references"
                        / "D5H2_groundTruth_samples.shp"
                    ),
                    "output_path": test_path,
                    "nomenclature_path": config_path_test,
                    "color_table": config_path_test,
                    "list_tile": "D0005H0002",
                    "l8_path_old": str(
                        Path(self.iota2_directory) / "data" / "L8_50x50"
                    ),
                    "l8_path": None,
                    "user_feat_path": None,
                    "region_field": "region",
                    "region_path": None,
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": True,
                },
                "arg_train": {"sample_management": None},
            },
        )

        (Path(features_outputs) / "D0005H0002" / "tmp").mkdir(
            exist_ok=True, parents=True
        )

        # launch test case
        config_test = rcf.ReadConfigFile(config_path_test)
        data_field = config_test.get_param("chain", "data_field")
        output_path = config_test.get_param("chain", "output_path")

        region_field = (config_test.get_param("chain", "region_field")).lower()

        runs = config_test.get_param("arg_train", "runs")
        ram = 128

        sensors_params = rcf.Iota2Parameters(
            rcf.ReadConfigFile(config_path_test)
        ).get_sensors_parameters("D0005H0002")

        ds = ogr.Open(self.referenceShape_test)
        drv_region = ds.GetDriver()
        test_ref_file = str(i2_tmpdir / "ref_shape.shp")
        drv_region.CopyDataSource(ds, test_ref_file)

        test_selection = str(i2_tmpdir / "D0005H0002.sqlite")
        shutil.copy(self.selection_test, test_selection)

        vector_sampler.generate_samples(
            DBInfo(data_field, test_ref_file, region_field=region_field),
            output_path,
            None,
            runs,
            sensors_params,
            SampleFlagsParameters(sampling_validation=False),
            TaskConfig(logging.getLogger(), ram),
            test_selection,
        )

        # assert
        test_vector = fut.file_search_reg_ex(test_path + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(test_vector, reference, cmp_mode="coordinates")
        assert compare
        del config_test

    def test_sampler_simple_bindings_temporary_data_workingdir(
        self, i2_tmpdir: Path
    ) -> None:
        """
        Integration test for the sample generation process using the binding sampler
        and writing temporary data and use a working directory
        """
        config_path = str(
            Path(self.iota2_directory)
            / "config"
            / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        test_path, features_outputs, w_d = self.prepare_tests_folder(
            str(i2_tmpdir),
        )
        config_path_test = str(Path(test_path) / "Config_TEST.cfg")
        l8_rasters = str(Path(self.iota2_directory) / "data" / "L8_50x50")
        copy_and_modify_config(
            config_path,
            config_path_test,
            {
                "chain": {
                    "ground_truth": str(
                        Path(IOTA2DIR)
                        / "data"
                        / "references"
                        / "D5H2_groundTruth_samples.shp"
                    ),
                    "output_path": test_path,
                    "nomenclature_path": config_path_test,
                    "color_table": config_path_test,
                    "list_tile": "D0005H0002",
                    "l8_path_old": l8_rasters,
                    "l8_path": None,
                    "user_feat_path": None,
                    "region_field": "region",
                    "region_path": None,
                },
                "sensors_data_interpolation": {
                    "use_additional_features": False,
                    "write_outputs": True,
                },
                "arg_train": {"sample_management": None},
            },
        )

        (Path(features_outputs) / "D0005H0002" / "tmp").mkdir(
            exist_ok=True, parents=True
        )
        config_test = rcf.ReadConfigFile(config_path_test)
        data_field = config_test.get_param("chain", "data_field")
        output_path = config_test.get_param("chain", "output_path")
        region_field = (config_test.get_param("chain", "region_field")).lower()
        runs = config_test.get_param("arg_train", "runs")
        sensors_params = rcf.Iota2Parameters(config_test).get_sensors_parameters(
            "D0005H0002"
        )

        ds = ogr.Open(self.referenceShape_test)
        drv_region = ds.GetDriver()
        test_ref_file = str(i2_tmpdir / "ref_shape.shp")
        drv_region.CopyDataSource(ds, test_ref_file)

        test_selection = str(i2_tmpdir / "D0005H0002.sqlite")
        shutil.copy(self.selection_test, test_selection)

        vector_sampler.generate_samples(
            DBInfo(data_field, test_ref_file, region_field=region_field),
            output_path,
            w_d,
            runs,
            sensors_params,
            SampleFlagsParameters(sampling_validation=False),
            TaskConfig(logging.getLogger(), 128),
            test_selection,
        )

        test_vector = fut.file_search_reg_ex(test_path + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(
            test_vector,
            str(
                Path(IOTA2_DATATEST)
                / "references"
                / "sampler"
                / "D0005H0002_polygons_To_Sample_Samples_ref_bindings.sqlite"
            ),
            cmp_mode="coordinates",
        )
        assert compare
        del config_test

    def test_sampler_simple_bindings_userfeatures_temporary_data(
        self, i2_tmpdir: Path
    ) -> None:
        """
        Integration test for the sample generation process using the binding sampler
        and user features and writing temporary data.
        """
        config_path = str(
            Path(self.iota2_directory)
            / "config"
            / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        reference = str(
            Path(IOTA2_DATATEST)
            / "references"
            / "sampler"
            / "D0005H0002_polygons_To_Sample_Samples_UserFeat_UserExpr.sqlite"
        )
        test_path, features_outputs, w_d = self.prepare_tests_folder(
            str(i2_tmpdir), working_directory=False
        )
        config_path_test = str(Path(test_path) / "Config_TEST.cfg")
        copy_and_modify_config(
            config_path,
            config_path_test,
            {
                "chain": {
                    "ground_truth": str(
                        Path(IOTA2DIR)
                        / "data"
                        / "references"
                        / "D5H2_groundTruth_samples.shp"
                    ),
                    "region_path": None,
                    "nomenclature_path": config_path_test,
                    "color_table": config_path_test,
                    "output_path": test_path,
                    "list_tile": "D0005H0002",
                    "l8_path_old": str(
                        Path(self.iota2_directory) / "data" / "L8_50x50"
                    ),
                    "l8_path": None,
                    "user_feat_path": str(
                        Path(self.iota2_directory) / "data" / "references" / "MNT"
                    ),
                    "region_field": "region",
                },
                "arg_train": {"sample_management": None},
                "userFeat": {"arbo": "/*", "patterns": "MNT"},
                "sensors_data_interpolation": {
                    "write_outputs": True,
                    "use_additional_features": True,
                },
                "Landsat8_old": {"additionalFeatures": "b1+b2,(b1-b2)/(b1+b2)"},
            },
        )

        (Path(features_outputs) / "D0005H0002" / "tmp").mkdir(
            exist_ok=True, parents=True
        )
        config_test = rcf.ReadConfigFile(config_path_test)
        data_field = config_test.get_param("chain", "data_field")
        output_path = config_test.get_param("chain", "output_path")
        region_field = (config_test.get_param("chain", "region_field")).lower()
        runs = config_test.get_param("arg_train", "runs")
        sensors_params = rcf.Iota2Parameters(config_test).get_sensors_parameters(
            "D0005H0002"
        )

        ds = ogr.Open(self.referenceShape_test)
        drv_region = ds.GetDriver()
        test_ref_file = str(i2_tmpdir / "ref_shape.shp")
        drv_region.CopyDataSource(ds, test_ref_file)

        test_selection = str(i2_tmpdir / "D0005H0002.sqlite")
        shutil.copy(self.selection_test, test_selection)

        vector_sampler.generate_samples(
            DBInfo(data_field, test_ref_file, region_field=region_field),
            output_path,
            w_d,
            runs,
            sensors_params,
            SampleFlagsParameters(sampling_validation=False),
            TaskConfig(logging.getLogger(), 128),
            test_selection,
        )

        test_vector = fut.file_search_reg_ex(test_path + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(test_vector, reference, cmp_mode="coordinates")
        assert compare
        del config_test

    def test_sampler_simple_bindings_userfeatures_temporary_data_workingdir(
        self, i2_tmpdir: Path
    ) -> None:
        """
        Integration test for the sample generation process using the binding sampler
        and user features and writing temporary data and use a working directory
        """
        config_path = str(
            Path(self.iota2_directory)
            / "config"
            / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        reference = str(
            Path(IOTA2_DATATEST)
            / "references"
            / "sampler"
            / "D0005H0002_polygons_To_Sample_Samples_UserFeat_UserExpr.sqlite"
        )
        test_path, features_outputs, w_d = self.prepare_tests_folder(
            str(i2_tmpdir), working_directory=True
        )
        config_path_test = str(Path(test_path) / "Config_TEST.cfg")
        copy_and_modify_config(
            config_path,
            config_path_test,
            {
                "chain": {
                    "ground_truth": str(
                        Path(IOTA2DIR)
                        / "data"
                        / "references"
                        / "D5H2_groundTruth_samples.shp"
                    ),
                    "region_path": None,
                    "nomenclature_path": config_path_test,
                    "color_table": config_path_test,
                    "output_path": test_path,
                    "list_tile": "D0005H0002",
                    "l8_path_old": str(
                        Path(self.iota2_directory) / "data" / "L8_50x50"
                    ),
                    "l8_path": None,
                    "user_feat_path": str(
                        Path(self.iota2_directory) / "data" / "references" / "MNT"
                    ),
                    "region_field": "region",
                },
                "arg_train": {"sample_management": None},
                "userFeat": {"arbo": "/*", "patterns": "MNT"},
                "sensors_data_interpolation": {
                    "write_outputs": True,
                    "use_additional_features": True,
                },
                "Landsat8_old": {"additionalFeatures": "b1+b2,(b1-b2)/(b1+b2)"},
            },
        )

        (Path(features_outputs) / "D0005H0002" / "tmp").mkdir(
            exist_ok=True, parents=True
        )
        config_test = rcf.ReadConfigFile(config_path_test)
        data_field = config_test.get_param("chain", "data_field")
        output_path = config_test.get_param("chain", "output_path")
        region_field = (config_test.get_param("chain", "region_field")).lower()

        runs = config_test.get_param("arg_train", "runs")
        sensors_params = rcf.Iota2Parameters(config_test).get_sensors_parameters(
            "D0005H0002"
        )

        ds = ogr.Open(self.referenceShape_test)
        drv_region = ds.GetDriver()
        test_ref_file = str(i2_tmpdir / "ref_shape.shp")
        drv_region.CopyDataSource(ds, test_ref_file)

        test_selection = str(i2_tmpdir / "D0005H0002.sqlite")
        shutil.copy(self.selection_test, test_selection)

        vector_sampler.generate_samples(
            DBInfo(data_field, test_ref_file, region_field=region_field),
            output_path,
            w_d,
            runs,
            sensors_params,
            SampleFlagsParameters(sampling_validation=False),
            TaskConfig(logging.getLogger(), 128),
            test_selection,
        )

        test_vector = fut.file_search_reg_ex(test_path + "/learningSamples/*sqlite")[0]
        TUV.delete_useless_fields(test_vector)
        compare = TUV.compare_sqlite(test_vector, reference, cmp_mode="coordinates")
        assert compare
        del config_test
