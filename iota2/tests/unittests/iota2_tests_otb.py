"""Module dedicated to test the otb application factory"""

from enum import Enum

import pytest

import iota2.common.otb_app_bank as otb


def test_nothandled_app() -> None:
    """
    Test whether an exception is thrown for an unmanaged application
    """
    new_values = {"TILE_FUSION": "TileFusion"}
    all_values = {**otb.AvailableOTBApp.__members__, **new_values}
    available_otb_app = Enum("AvailableOTBApp", all_values)
    with pytest.raises(ValueError):
        _ = otb.create_application(available_otb_app.TILE_FUSION, {})
