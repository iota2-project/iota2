#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test merging runs"""
import shutil
from pathlib import Path

import numpy as np
import rasterio

from iota2.regression.merge_final_regressions import fusion_regression
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.typings.i2_types import RegressionMetricsParameters
from iota2.validation.regression_metrics import generate_multi_tile_metrics

IOTA2_DATATEST = str(Path(IOTA2DIR) / "data")


class Iota2MergeRunTest:
    """Class to test merging runs"""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.path_input = IOTA2_DATATEST + "/references/Fusion/Input/"
        cls.path_validation = IOTA2_DATATEST + "/references/Validation/"

    def test_fusion_regression(self, i2_tmpdir: Path) -> None:
        """Test fusion for regression"""
        final_regressions = [
            self.path_input + "T31TCJ_seed_" + str(i) + ".tif" for i in range(3)
        ]
        fusion_regression(
            final_regressions,
            str(Path(i2_tmpdir) / "fusion.tif"),
            str(Path(i2_tmpdir) / "confidence.tif"),
            "median",
            "float",
        )

        seed_0 = rasterio.open(self.path_input + "T31TCJ_seed_0.tif")
        seed_1 = rasterio.open(self.path_input + "T31TCJ_seed_1.tif")
        seed_2 = rasterio.open(self.path_input + "T31TCJ_seed_2.tif")
        fusion = rasterio.open(str(Path(i2_tmpdir) / "fusion.tif"))
        confidence = rasterio.open(str(Path(i2_tmpdir) / "confidence.tif"))

        fusion_arr = fusion.read(1)
        confidence_arr = confidence.read(1)
        seed_0_arr = seed_0.read(1)
        seed_1_arr = seed_1.read(1)
        seed_2_arr = seed_2.read(1)

        run_median = np.median(
            [seed_0_arr.flatten(), seed_1_arr.flatten(), seed_2_arr.flatten()], axis=0
        )
        assert np.array_equal(fusion_arr.flatten(), run_median)
        assert np.allclose(
            confidence_arr.flatten(),
            np.median(
                [
                    np.abs(seed_0_arr.flatten() - run_median),
                    np.abs(seed_1_arr.flatten() - run_median),
                    np.abs(seed_2_arr.flatten() - run_median),
                ],
                axis=0,
            ),
        )

        fusion_regression(
            final_regressions,
            str(Path(i2_tmpdir) / "fusion.tif"),
            str(Path(i2_tmpdir) / "confidence.tif"),
            "mean",
            "float",
        )
        fusion = rasterio.open(str(Path(i2_tmpdir) / "fusion.tif"))
        confidence = rasterio.open(str(Path(i2_tmpdir) / "confidence.tif"))
        fusion_arr = fusion.read(1)
        confidence_arr = confidence.read(1)

        assert np.allclose(
            fusion_arr.flatten(),
            np.mean(
                [seed_0_arr.flatten(), seed_1_arr.flatten(), seed_2_arr.flatten()],
                axis=0,
            ),
        )
        assert np.allclose(
            confidence_arr.flatten(),
            np.std(
                [seed_0_arr.flatten(), seed_1_arr.flatten(), seed_2_arr.flatten()],
                axis=0,
            ),
        )

    def test_generate_multi_run_metrics(self, i2_tmpdir: Path) -> None:
        """Test generation of metrics for multi run"""
        shutil.copy(
            self.path_validation + "T31TCJ_majvote.sqlite",
            str(Path(i2_tmpdir) / "T31TCJ_majvote.sqlite"),
        )
        shutil.copy(
            self.path_validation + "T31TDJ_majvote.sqlite",
            str(Path(i2_tmpdir) / "T31TDJ_majvote.sqlite"),
        )
        shutil.copy(
            self.path_validation + "Regressions_fusion.tif",
            str(Path(i2_tmpdir) / "Regressions_fusion.tif"),
        )

        validation_data = [
            str(Path(i2_tmpdir) / "T31TCJ_majvote.sqlite"),
            str(Path(i2_tmpdir) / "T31TDJ_majvote.sqlite"),
        ]

        generate_multi_tile_metrics(
            str(Path(i2_tmpdir) / "Regressions_fusion.tif"),
            validation_data,
            "split",
            str(Path(i2_tmpdir) / "result.csv"),
            RegressionMetricsParameters(
                data_field="ndvi",
                mode="w",
                column=True,
            ),
        )

        assert (Path(i2_tmpdir) / "result.csv").exists()
