# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""check inputs tests."""
import os
from pathlib import Path

from iota2.common import service_error, verify_inputs
from iota2.common.tools import check_database, check_region_database
from iota2.tests.utils.tests_utils_rasters import FakeS2DataGenerator
from iota2.tests.utils.tests_utils_vectors import random_ground_truth_generator
from iota2.vector_tools.vector_functions import copy_shapefile

IOTA2DIR_AS_STR = os.environ.get("IOTA2DIR")
if not IOTA2DIR_AS_STR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR_AS_STR)


class Iota2CheckInputs:
    """Check the iota2's ability to detect anomaly in user-provided data."""

    config_test: str
    ground_truth: str
    region_target_miss: str
    region_target: str
    gt_target_miss: str
    gt_target: str
    gt_target_miss_region: str
    gt_target_miss_one_region: str

    @classmethod
    def setup_class(cls) -> None:
        """Variables shared between tests."""
        references_directory = IOTA2DIR / "data" / "references"
        cls.config_test = str(
            IOTA2DIR / "config" / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        cls.ground_truth = str(references_directory / "ZONAGE_BV_ALL.shp")
        cls.region_target_miss = str(references_directory / "region_target_miss.shp")
        cls.region_target = str(references_directory / "region_target.shp")
        cls.gt_target_miss = str(references_directory / "gt_target_miss.shp")
        cls.gt_target = str(references_directory / "gt_target.shp")
        cls.gt_target_miss_region = str(
            Path(references_directory) / "gt_target_miss_region.shp"
        )
        cls.gt_target_miss_one_region = str(
            Path(references_directory) / "gt_target_miss_one_region.shp"
        )

    def test_check_database(self, i2_tmpdir: Path) -> None:
        """TEST : check the database."""
        ground_truth_name = Path(self.ground_truth).name
        test_ground_truth = str(i2_tmpdir / ground_truth_name)
        copy_shapefile(self.ground_truth, test_ground_truth)
        data_field = "code"
        epsg = 2154
        gt_errors = check_database.check_ground_truth_inplace(
            test_ground_truth, data_field, epsg, do_corrections=False, display=False
        )
        # multipolygons detected
        assert len(gt_errors) == 1
        assert isinstance(
            type(gt_errors[0]), service_error.ContainsMultipolygon.__class__
        ), "multipolygons undetected"
        # wrong projection
        epsg = 2155
        gt_errors = check_database.check_ground_truth_inplace(
            test_ground_truth, data_field, epsg, do_corrections=False, display=False
        )
        assert len(gt_errors) == 2
        assert isinstance(
            type(gt_errors[0]), service_error.InvalidProjection.__class__
        ), "invalid projection miss detected"
        assert isinstance(
            type(gt_errors[1]), service_error.ContainsMultipolygon.__class__
        ), "multipolygons undetected"
        # no field detected
        data_field = "error"
        epsg = 2154
        gt_errors = check_database.check_ground_truth_inplace(
            test_ground_truth, data_field, epsg, do_corrections=False, display=False
        )
        assert len(gt_errors) == 2
        assert isinstance(
            type(gt_errors[0]), service_error.MissingField.__class__
        ), "missing field undetected"
        assert isinstance(
            type(gt_errors[1]), service_error.ContainsMultipolygon.__class__
        ), "multipolygons undetected"
        assert isinstance(
            type(gt_errors[0]), service_error.FieldType.__class__
        ), "integer field Type undetected"
        assert isinstance(
            type(gt_errors[1]), service_error.ContainsMultipolygon.__class__
        ), "multipolygons undetected"
        # invalid geometry
        no_geom_shape = str(i2_tmpdir / "no_geom_shape.shp")
        random_ground_truth_generator(no_geom_shape, data_field, 3, set_geom=False)
        gt_errors = check_database.check_ground_truth_inplace(
            no_geom_shape, data_field, epsg, do_corrections=False, display=False
        )
        assert len(gt_errors) == 1
        assert isinstance(
            type(gt_errors[0]), service_error.InvalidGeometry.__class__
        ), "invalid geometries undetected"

        # duplicated features
        dupli_feat_path = str(i2_tmpdir / "duplicate_features.shp")
        random_ground_truth_generator(dupli_feat_path, data_field, 3)
        gt_errors = check_database.check_ground_truth_inplace(
            dupli_feat_path, data_field, epsg, do_corrections=False, display=False
        )
        assert len(gt_errors) == 1
        assert isinstance(
            type(gt_errors[0]), service_error.DuplicatedFeatures.__class__
        ), "duplicated features undetected"

    def test_check_region_shape(self, i2_tmpdir: Path) -> None:
        """TEST : check the region shape database."""
        ground_truth_name = Path(self.ground_truth).name
        test_ground_truth = str(i2_tmpdir / ground_truth_name)
        copy_shapefile(self.ground_truth, test_ground_truth)
        data_field = "region"
        epsg = 2154
        region_errors = check_region_database.check_region_database_inplace(
            test_ground_truth, data_field, epsg, do_corrections=False
        )
        assert len(region_errors) == 1
        assert isinstance(
            type(region_errors[0]), service_error.TooSmallRegion.__class__
        ), "too small regions undetected"

    def test_check_intersections(self, i2_tmpdir: Path) -> None:
        """TEST : check if no intersection between geo-reference data is detected."""
        s2_data = str(i2_tmpdir / "s2_data")
        data_generator = FakeS2DataGenerator(
            s2_data, "T31TCJ", ["20190909", "20190919", "20190929"]
        )
        data_generator.generate_data()

        # usual use case
        intersections_errors = verify_inputs.check_data_intersection(
            self.gt_target, None, "region", 2154, s2_data, ["T31TCJ"]
        )
        assert (
            len(intersections_errors) == 0
        ), "no intersections detected, but there is intersections"

        # no intersections between input rasters and the ground truth
        intersections_errors = verify_inputs.check_data_intersection(
            self.gt_target_miss, None, "region", 2154, s2_data, ["T31TCJ"]
        )

        assert len(intersections_errors) == 1
        assert isinstance(
            type(intersections_errors[0]), service_error.IntersectionError.__class__
        ), "no intersections undetected"

        # no intersections between the ground truth and the region shape
        intersections_errors = verify_inputs.check_data_intersection(
            self.gt_target_miss_region,
            self.region_target,
            "region",
            2154,
            s2_data,
            ["T31TCJ"],
        )

        assert len(intersections_errors) == 1
        assert isinstance(
            type(intersections_errors[0]), service_error.IntersectionError.__class__
        ), "no intersections undetected"

        # intersections between the ground truth and the region shape
        intersections_errors = verify_inputs.check_data_intersection(
            self.gt_target, self.region_target, "region", 2154, s2_data, ["T31TCJ"]
        )

        assert (
            len(intersections_errors) == 0
        ), "no intersections detected, but there is intersections"

        # no intersections between the ground truth and ONE region
        intersections_errors = verify_inputs.check_data_intersection(
            self.gt_target_miss_one_region,
            self.region_target,
            "region",
            2154,
            s2_data,
            ["T31TCJ"],
        )

        assert len(intersections_errors) == 1
        assert isinstance(
            type(intersections_errors[0]), service_error.IntersectionError.__class__
        ), "no intersections undetected"
