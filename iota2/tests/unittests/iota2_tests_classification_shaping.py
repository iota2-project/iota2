#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test the classification shaping workflow."""

import shutil
from pathlib import Path

from osgeo import gdal

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.common.compression_options import compression_options
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.typings.i2_types import MosaicInputPaths, MosaicParameters
from iota2.validation import classification_shaping as CS


class Iota2TestClassificationShaping:
    """Test the classification shaping workflow."""

    ref_data: str

    @classmethod
    def setup_class(cls) -> None:
        """Variables shared between tests."""
        cls.ref_data = str(IOTA2DIR / "data" / "references" / "ClassificationShaping")

    def test_classification_shaping(self, i2_tmpdir: Path) -> None:
        """Test 'classification_shaping' method"""
        # Prepare data
        path_classif = i2_tmpdir / "classif"
        classif_final_path = i2_tmpdir / "final"

        Path.mkdir(path_classif / "MASK", parents=True, exist_ok=True)
        Path.mkdir(path_classif / "tmpClassif", parents=True, exist_ok=True)
        Path.mkdir(classif_final_path / "TMP", parents=True, exist_ok=True)
        # copy input file
        src_files = (Path(self.ref_data) / "Input" / "Classif" / "MASK").iterdir()
        for file in src_files:
            shutil.copy(file, path_classif / "MASK")

        src_files = (Path(self.ref_data) / "Input" / "Classif" / "classif").iterdir()
        for file in src_files:
            shutil.copy(file, path_classif)

        # run test
        region_file = str(IOTA2DIR / "data" / "regionShape" / "regionTiles.shp")
        features_ref = str(IOTA2DIR / "data" / "references" / "features")
        features_ref_test = str(i2_tmpdir / "features")
        Path(features_ref_test).mkdir()
        shutil.copytree(features_ref + "/D0005H0002", features_ref_test + "/D0005H0002")
        shutil.copytree(features_ref + "/D0005H0003", features_ref_test + "/D0005H0003")

        nb_run = 1
        colortable = str(IOTA2DIR / "data" / "references" / "color.txt")

        mosaic_input_paths = MosaicInputPaths(
            path_classif=str(path_classif),
            final_folder=str(i2_tmpdir / "final"),
            final_tmp_folder=str(i2_tmpdir / "final" / "TMP"),
            validation_input_folder=str(i2_tmpdir / "dataAppVal"),
            nb_view_folder=str(i2_tmpdir / "features"),
            region_shape=region_file,
            color_path=colortable,
        )
        mosaic_parameters = MosaicParameters(
            runs=nb_run,
            classif_mode="separate",
            data_field="code",
            labels_conversion={"12": 12},
            tiles_from_cfg=["D0005H0002", "D0005H0003"],
            flags=MosaicParameters.Flags(proba_map_flag=False, output_statistics=False),
        )
        CS.classification_shaping(
            mosaic_input_paths=mosaic_input_paths,
            mosaic_parameters=mosaic_parameters,
            spatial_resolution=(30, 30),
        )

        # file comparison to ref file
        src_files = (Path(self.ref_data) / "Output").iterdir()
        for file in src_files:
            file_name = file.name
            file1 = str(classif_final_path / file_name)
            reference_file1 = str(Path(self.ref_data) / "Output" / file_name)
            nbdiff = TUR.gdal_file_compare(file1, reference_file1)
            assert nbdiff == 0, f"{file1}, {reference_file1}"

            compress_raster = gdal.Open(file1)
            metadata = compress_raster.GetMetadata("IMAGE_STRUCTURE")
            assert metadata.get("COMPRESSION") == compression_options.algorithm
            assert metadata.get("PREDICTOR") == str(compression_options.predictor)
