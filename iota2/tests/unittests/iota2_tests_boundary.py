#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module for testing vector formattings functions."""
import os
import shutil
from pathlib import Path

import numpy as np

import iota2.validation.boundary_tile_fusion as btf
import iota2.vector_tools.compute_boundary_regions as cbr
from iota2.common.raster_utils import raster_to_array
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.typings.i2_types import (
    BoundariesProbaFusionParameters,
    DistanceMapParameters,
)

IOTA2DIR_AS_STR = os.environ.get("IOTA2DIR")
if not IOTA2DIR_AS_STR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR_AS_STR)
IOTA2_DATATEST = IOTA2DIR / "data"


class Iota2TestBoundaryFusion(AssertsFilesUtils):
    """Test the vector formatting functions."""

    # before launching tests
    @classmethod
    def setup_class(cls) -> None:
        """Shared variables between tests."""
        # definition of local variables

        cls.common_mask = str(
            IOTA2_DATATEST
            / "references"
            / "Runs_standards"
            / "Usual_s2_theia_run"
            / "test_results"
            / "features"
            / "T31TCJ"
            / "tmp"
            / "MaskCommunSL.tif"
        )
        cls.ref_region = str(
            IOTA2_DATATEST / "references" / "running_iota2" / "region.shp"
        )

        cls.proba_map_folder = (
            IOTA2_DATATEST / "references" / "Runs_standards" / "Probability_maps"
        )
        # References
        cls.config_test = str(
            IOTA2_DATATEST / "config" / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        cls.nomenclature_path = str(
            IOTA2_DATATEST / "references" / "running_iota2" / "nomenclature.txt"
        )

    def test_compute_distance(self, i2_tmpdir: Path) -> None:
        # pylint: disable=unused-argument
        """Test compute distance function."""
        distance_map_params = DistanceMapParameters(
            exterior_buffer_size=40,
            tile="T31TCJ",
            region_field="region",
            epsilon=0.001,
            interior_buffer_size=50,
            spatial_res=(10, 10),
            paths=DistanceMapParameters.Paths(
                region_file=self.ref_region,
                common_mask=self.common_mask,
                output_path=str(i2_tmpdir),
                masks_region_path=str(i2_tmpdir),
            ),
        )
        cbr.compute_distance_map(
            distance_map_params=distance_map_params,
        )
        # Ensure that the merge of distance works correctly
        self.assert_file_exist(i2_tmpdir / "T31TCJ_distance_map.tif")
        # Ensure that each distance map for regions is not empty (itk filter)
        distance_map_1 = raster_to_array(
            str(i2_tmpdir / "tile_T31TCJ_region_1_distance.tif")
        )
        assert np.sum(distance_map_1) > 0
        distance_map_2 = raster_to_array(
            str(i2_tmpdir / "tile_T31TCJ_region_2_distance.tif")
        )
        assert np.sum(distance_map_2) > 0
        # Ensure that buffered region are not empty
        mask_1 = raster_to_array(str(i2_tmpdir / "Boundary_MASK_region_1_T31TCJ.tif"))
        assert np.sum(mask_1) > 0
        mask_2 = raster_to_array(str(i2_tmpdir / "Boundary_MASK_region_2_T31TCJ.tif"))
        assert np.sum(mask_2) > 0

    def test_proba_fusion(self, i2_tmpdir: Path) -> None:
        # pylint: disable=unused-argument
        """Test proba fusion function."""
        fusion_parameters = BoundariesProbaFusionParameters(
            tile="T31TCJ",
            seed=0,
            region_field="region",
            number_of_chunks=1,
            nb_class=4,
            target_chunk=0,  # only one chunk
        )
        btf.proba_fusion_at_boundaries(
            distance_map_folder=str(self.proba_map_folder),
            probabilities_map_folder=str(self.proba_map_folder),
            boundary_folder_out=str(i2_tmpdir),
            original_region_file=self.ref_region,
            proba_fusion_params=fusion_parameters,
        )
        fused_map = raster_to_array(
            str(i2_tmpdir / "classif_boundary_T31TCJ_seed_0_0.tif")
        )
        assert np.sum(fused_map) > 0

    def test_metrics_boundary(self, i2_tmpdir: Path) -> None:
        # pylint: disable=unused-argument
        """Test if metrics are well computed."""
        # copy input data
        shutil.copytree(str(self.proba_map_folder / "final"), i2_tmpdir / "final")
        btf.merge_metrics_matrices(i2_tmpdir, "code", 1, self.nomenclature_path)
        self.assert_file_exist(
            i2_tmpdir
            / "final"
            / "Confusion_conditional_classification_boundary_seed_0.csv"
        )
        self.assert_file_exist(
            i2_tmpdir
            / "final"
            / "Confusion_conditional_classification_standard_seed_0.csv"
        )
        self.assert_file_exist(
            i2_tmpdir / "final" / "Confusion_classification_boundary_seed_0.csv"
        )
        self.assert_file_exist(
            i2_tmpdir / "final" / "Confusion_classification_standard_seed_0.csv"
        )
