#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test the merge of by-tiles samples to a unique one"""
import shutil
from pathlib import Path

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.common import file_utils as fu
from iota2.sampling import vector_samples_merge as VSM
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestsVectorSamplesMerge:
    """Check the merge of by-tiles samples to a unique one"""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_data = str(
            Path(IOTA2DIR) / "data" / "references" / "VectorSamplesMerge"
        )

    def test_vector_samples_merge(self, i2_tmpdir: Path) -> None:
        """Test the 'vector_samples_merge' method"""
        learning_samples = i2_tmpdir / "learningSamples"
        # test and creation of learningSamples
        Path.mkdir(learning_samples, exist_ok=True)
        learning_samples = str(learning_samples)

        # copy input data
        shutil.copy(
            self.ref_data + "/Input/D0005H0003_region_1_seed0_learn_Samples.sqlite",
            learning_samples,
        )

        # Start test
        vector_list = fu.file_search_and(learning_samples, True, ".sqlite")
        VSM.vector_samples_merge(vector_list, str(i2_tmpdir), "code")

        # file comparison to ref file
        file1 = str(Path(learning_samples) / "Samples_region_1_seed0_learn.sqlite")
        reference_file1 = str(
            Path(self.ref_data) / "Output" / "Samples_region_1_seed0_learn.sqlite"
        )
        assert TUV.compare_sqlite(
            file1, reference_file1, cmp_mode="coordinates", ignored_fields=[]
        )

    def test_get_tile_vector_list(self):
        """Test the 'get_tile_vector_list' method"""
        folder_path = Path("results/learningSamples/")
        folder_path_tile = Path("results_T31TCJ/learningSamples/")
        suffix = "_Samples_learn"
        tiles_list = ["T31TGJ", "T06TOP"]
        vectors_list = []
        for tile in tiles_list:
            for chunk in range(4):
                vectors_list.append(
                    folder_path.joinpath(
                        tile + "_chunk_" + str(chunk) + suffix + ".csv"
                    )
                )

        vectors_list_filtered = VSM.get_tile_vector_list(vectors_list)

        assert len(vectors_list_filtered) == 2

        vectors_list = []
        for tile in tiles_list:
            for chunk in range(4):
                vectors_list.append(
                    folder_path_tile.joinpath(
                        tile + "_chunk_" + str(chunk) + suffix + ".sqlite"
                    )
                )

        vectors_list_filtered.append(folder_path_tile.joinpath("test.sqlite"))

        vectors_list_filtered = VSM.get_tile_vector_list(vectors_list)

        assert len(vectors_list_filtered) == 2
