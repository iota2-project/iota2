#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test envelope file generation."""
import os
import shutil
from pathlib import Path

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.sampling import tile_envelope as env

IOTA2DIR_AS_STR = os.environ.get("IOTA2DIR")
if not IOTA2DIR_AS_STR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR_AS_STR)
IOTA2_DATATEST = str(IOTA2DIR / "data")


class Iota2TestGenerateShapeTile:
    """Class to test envelope file generation."""

    tiles: list[str]
    path_tiles_feat: str

    @classmethod
    def setup_class(cls) -> None:
        """Variables shared between tests."""
        cls.tiles = ["D0005H0002"]
        cls.path_tiles_feat = IOTA2_DATATEST + "/references/features/"

    def test_generate_shape_tile(self, i2_tmpdir: Path) -> None:
        """Test envelope file generation."""
        # Test de création des enveloppes
        features_path = str(i2_tmpdir / "features")

        masks_references = str(IOTA2DIR / "data" / "references" / "features")
        if Path(features_path).exists():
            shutil.rmtree(features_path)
        shutil.copytree(masks_references, features_path)

        # Test de création des enveloppes
        # Launch function
        env.generate_shape_tile(self.tiles, None, str(i2_tmpdir), 2154, 0)

        # For each tile test if the shapefile is ok
        for n_tile in self.tiles:
            # generate filename
            reference_shape_file = (
                IOTA2_DATATEST + "/references/GenerateShapeTile/" + n_tile + ".shp"
            )
            shape_file = str(i2_tmpdir / "envelope" / f"{n_tile}.shp")
            # Launch shapefile comparison
            assert TUV.test_same_shapefiles(reference_shape_file, shape_file)
