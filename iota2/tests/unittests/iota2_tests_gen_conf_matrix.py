#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test the generation of confusion matrix."""
import os
import shutil
from pathlib import Path

from iota2.typings.i2_types import ConfusionMatrixAppParameters
from iota2.validation import gen_confusion_matrix as GCM

IOTA2DIR_AS_STR = os.environ.get("IOTA2DIR")
if not IOTA2DIR_AS_STR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR_AS_STR)


class Iota2TestGenConfMatrix:
    """check confusion matrix gen as csv"""

    ref_data: Path

    @classmethod
    def setup_class(cls) -> None:
        """Variables shared between tests."""
        cls.ref_data = IOTA2DIR / "data" / "references" / "GenConfMatrix"

    def test_gen_conf_matrix(self, i2_tmpdir: Path) -> None:
        """Test generate confusion matrix."""
        # Prepare data
        pathappval = str(i2_tmpdir / "dataAppVal")
        pathclassif = str(i2_tmpdir / "classif")
        final = str(i2_tmpdir / "final")

        # test and creation of pathClassif
        Path(pathclassif).mkdir(exist_ok=True, parents=True)
        (Path(pathclassif) / "MASK").mkdir(exist_ok=True, parents=True)
        (Path(pathclassif) / "tmpClassif").mkdir(exist_ok=True, parents=True)

        # test and creation of Final
        Path(final).mkdir(exist_ok=True, parents=True)
        (Path(final) / "TMP").mkdir(exist_ok=True, parents=True)

        # test and creation of pathAppVal
        Path(pathappval).mkdir(exist_ok=True, parents=True)

        # copy input data
        src_files = (self.ref_data / "Input" / "dataAppVal").iterdir()
        for file_name in src_files:
            shutil.copy(file_name, pathappval)

        src_files = (self.ref_data / "Input" / "Classif" / "MASK").iterdir()
        for file_name in src_files:
            shutil.copy(file_name, pathclassif + "/MASK")
        src_files = (self.ref_data / "Input" / "Classif" / "classif").iterdir()
        for file_name in src_files:
            shutil.copy(file_name, pathclassif)
        src_files = (self.ref_data / "Input" / "final" / "TMP").iterdir()
        for file_name in src_files:
            shutil.copy(file_name, final + "/TMP")

        # Run test
        data_field = "CODE"
        in_classif = str(
            i2_tmpdir / "classif" / "D0005H0003_model_1_confidence_seed_0.tif"
        )

        ref_vec = str(Path(pathappval) / "D0005H0003_region_1_seed0_val.shp")
        out_csv = str(i2_tmpdir / "csv_test.csv")
        conf_matrix_app_params = ConfusionMatrixAppParameters(
            in_classif=in_classif,
            out_csv=out_csv,
            data_field=data_field,
            ref_vector=ref_vec,
            ram=128,
        )
        GCM.gen_conf_matrix(
            conf_matrix_params=conf_matrix_app_params,
            labels_table={"class": "class"},
        )

        # Write in the CSV:
        # Reference labels (rows):1
        # Produced labels (columns):1
        # 4
        csv_content = "#Reference labels (rows):1\n#Produced labels (columns):1\n4\n"

        with open(out_csv, encoding="UTF-8") as file_csv:
            file_csv_content = file_csv.read()

        assert file_csv_content == csv_content, "csv file generation failed"

    def test_gen_conf_matrix_int_nomen(self, i2_tmpdir: Path) -> None:
        """Test generate confusion matrix."""
        pathappval = str(i2_tmpdir / "dataAppVal")
        pathclassif = str(i2_tmpdir / "classif")
        final = str(i2_tmpdir / "final")
        cmdpath = str(i2_tmpdir / "cmd")

        # test and creation of pathClassif
        Path(pathclassif).mkdir(exist_ok=True, parents=True)
        (Path(pathclassif) / "MASK").mkdir(exist_ok=True, parents=True)
        (Path(pathclassif) / "tmpClassif").mkdir(exist_ok=True, parents=True)

        # test and creation of Final
        Path(final).mkdir(exist_ok=True, parents=True)
        (Path(final) / "TMP").mkdir(exist_ok=True, parents=True)

        # test and creation of cmdPath
        Path(cmdpath).mkdir(exist_ok=True, parents=True)
        (Path(cmdpath) / "confusion").mkdir(exist_ok=True, parents=True)

        # test and creation of pathAppVal
        Path(pathappval).mkdir(exist_ok=True, parents=True)

        # copy input data
        src_files = (self.ref_data / "Input" / "dataAppVal").iterdir()
        for file_name in src_files:
            shutil.copy(file_name, pathappval)

        src_files = (self.ref_data / "Input" / "Classif" / "MASK").iterdir()
        for file_name in src_files:
            shutil.copy(file_name, pathclassif + "/MASK")

        src_files = (self.ref_data / "Input" / "Classif" / "classif").iterdir()
        for file_name in src_files:
            shutil.copy(file_name, pathclassif)

        src_files = (self.ref_data / "Input" / "final" / "TMP").iterdir()
        for file_name in src_files:
            shutil.copy(file_name, final + "/TMP")

        data_field = "CODE"
        in_classif = str(
            i2_tmpdir / "classif" / "D0005H0003_model_1_confidence_seed_0.tif"
        )

        ref_vec = str(Path(pathappval) / "D0005H0003_region_1_seed0_val.shp")
        out_csv = str(i2_tmpdir / "csv_test.csv")

        conf_matrix_app_params = ConfusionMatrixAppParameters(
            in_classif=in_classif,
            out_csv=out_csv,
            data_field=data_field,
            ref_vector=ref_vec,
            ram=128,
        )
        GCM.gen_conf_matrix(
            conf_matrix_params=conf_matrix_app_params, labels_table={1: 12}
        )

        # Write in the CSV:
        # Reference labels (rows):12
        # Produced labels (columns):1
        # 4
        csv_content = "#Reference labels (rows):12\n#Produced labels (columns):1\n4\n"
        with open(out_csv, encoding="utf-8") as file_csv:
            file_csv_content = file_csv.read()
        assert file_csv_content == csv_content, "csv file generation failed"
