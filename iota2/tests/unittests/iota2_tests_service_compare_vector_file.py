#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test the vector file comparing tool"""
import pytest

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestComparingVectorFile:
    """Class to test the vector file comparing tool"""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_data = IOTA2DIR / "data" / "references" / "compare_vector_files"

    def test_same_vector(self):
        """Check if it is the same file."""
        file1 = str(self.ref_data / "vector1.shp")
        assert TUV.test_same_shapefiles(file1, file1)

    def test_different_vector(self):
        """Check if differences are detected."""
        file1 = str(self.ref_data / "vector1.shp")
        file2 = str(self.ref_data / "vector2.shp")
        assert TUV.test_same_shapefiles(file1, file2) is False

    def test_error_vector(self):
        """Check if an error is detected."""
        file1 = str(self.ref_data / "vectorNotHere.shp")
        file2 = str(self.ref_data / "vector2.shp")
        pytest.raises(Exception, TUV.test_same_shapefiles, file1, file2)
