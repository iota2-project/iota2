#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to test some vector tools features."""

import os
import shutil
from pathlib import Path

import numpy as np
import pytest

from iota2.typings.i2_types import IntersectionDatabaseParameters
from iota2.vector_tools import add_field_perimeter as afp
from iota2.vector_tools import buffer_ogr as bfo
from iota2.vector_tools import change_name_field as cnf
from iota2.vector_tools import spatial_operations as so
from iota2.vector_tools import vector_functions as vf

IOTA2DIR_AS_STR = os.environ.get("IOTA2DIR")
if not IOTA2DIR_AS_STR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR_AS_STR)


@pytest.mark.vecto
class Iota2TestVectorTools:
    """Test some vector tools features."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.classif = str(
            IOTA2DIR / "data" / "references" / "vectortools" / "classif.shp"
        )
        cls.inter = str(IOTA2DIR / "data" / "references" / "vectortools" / "region.shp")
        cls.classifout = str(
            IOTA2DIR / "data" / "references" / "vectortools" / "classifout.shp"
        )
        cls.fake_region = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "fake_region.shp"
        )
        cls.classif_raster_file = str(
            IOTA2DIR
            / "data"
            / "references"
            / "Runs_standards"
            / "Usual_s2_theia_run"
            / "test_results"
            / "final"
            / "Classif_Seed_0.tif"
        )
        cls.classif_vector_file = str(
            IOTA2DIR / "data" / "references" / "vectortools" / "i2_classif.sqlite"
        )
        cls.shp_points_path = str(
            IOTA2DIR
            / "data"
            / "references"
            / "running_iota2"
            / "ground_truth_points.shp"
        )
        cls.sqlite_points_path = str(
            IOTA2DIR
            / "data"
            / "references"
            / "Validation"
            / "T31TCJ_seed_0_val_predicted.sqlite"
        )

    def test_iota2_vectortools(self, i2_tmpdir: Path) -> None:
        """Test how many samples must be added to the sample set."""
        outinter = str(i2_tmpdir / "inter.shp")
        classifwd = i2_tmpdir / "out" / "classif.shp"
        if not classifwd.parent.exists():
            classifwd.parent.mkdir(parents=True, exist_ok=True)
        classifwd = str(classifwd)
        # Add Field
        for ext in [".shp", ".dbf", ".shx", ".prj"]:
            shutil.copyfile(
                Path(self.classif).with_suffix(ext),
                Path(classifwd).with_suffix(ext),
            )

        afp.add_field_perimeter(classifwd)
        tmpbuff = str(i2_tmpdir / "tmpbuff.shp")
        bfo.buffer_poly(classifwd, tmpbuff, -10)
        for ext in [".shp", ".dbf", ".shx", ".prj"]:
            shutil.copyfile(
                Path(tmpbuff).with_suffix(ext), Path(classifwd).with_suffix(ext)
            )

        cnf.change_name(classifwd, "Classe", "class")
        assert vf.get_nb_feat(classifwd) == 143, "Number of features does not fit"
        assert vf.get_fields(classifwd) == [
            "Validmean",
            "Validstd",
            "Confidence",
            "Hiver",
            "Ete",
            "Feuillus",
            "Coniferes",
            "Pelouse",
            "Landes",
            "UrbainDens",
            "UrbainDiff",
            "ZoneIndCom",
            "Route",
            "PlageDune",
            "SurfMin",
            "Eau",
            "GlaceNeige",
            "Prairie",
            "Vergers",
            "Vignes",
            "Perimeter",
            "class",
        ], "List of fields does not fit"
        assert vf.list_value_fields(classifwd, "class") == [
            "11",
            "12",
            "211",
            "222",
            "31",
            "32",
            "36",
            "42",
            "43",
            "51",
        ], "Values of field 'class' do not fit"
        assert vf.get_field_type(classifwd, "class") == str, (
            f"Type of field 'class' ({vf.get_field_type(classifwd, 'class')}) do not fit,"
            f" 'str' expected"
        )

        so.intersection_sqlite(
            IntersectionDatabaseParameters(
                data_base1=classifwd,
                data_base2=self.inter,
                fields_to_keep=[
                    "class",
                    "Validmean",
                    "Validstd",
                    "Confidence",
                    "ID",
                    "Perimeter",
                    "Aire",
                    "mask",
                ],
            ),
            output=outinter,
            epsg=2154,
        )

    def test_get_geometry(self):
        """Test vector_functions.get_geometry"""

        assert vf.get_geometry(self.classifout) == "POLYGON"
        assert (
            vf.get_geometry(self.classif_vector_file, ogr_driver="SQLite") == "POLYGON"
        )

        assert vf.get_geometry(self.shp_points_path) == "POINT"
        assert vf.get_geometry(self.sqlite_points_path, ogr_driver="SQLite") == "POINT"

    def test_categorize_continuous_field(self):
        """Test vector_functions.categorize_continuous_field"""

        field_arr = np.array([1, 9, 2, 2, 3, 6, 8, 5])

        categorized_arr = vf.categorize_continuous_field(field_arr, 9)
        assert np.array_equal(categorized_arr, np.array([1, 9, 2, 2, 3, 6, 8, 5]))

        categorized_arr = vf.categorize_continuous_field(field_arr, [1, 5, 6, 8, 10])
        assert np.array_equal(categorized_arr, np.array([1, 4, 1, 1, 1, 3, 4, 2]))
