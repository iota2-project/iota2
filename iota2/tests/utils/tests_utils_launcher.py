#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""This module offers some tools for writing tests."""
import os
import shutil
from pathlib import Path
from subprocess import PIPE, Popen

import requests

from iota2.common.file_utils import get_iota2_project_dir, md5
from iota2.typings.i2_types import PathLike


def download_url(url: str, save_path: PathLike, chunk_size: int = 128) -> None:
    """Download and save file from a given URL"""
    req = requests.get(url, stream=True, timeout=30)
    with open(save_path, "wb") as file:
        for chunk in req.iter_content(chunk_size=chunk_size):
            file.write(chunk)


def get_large_i2_data_test() -> None:
    """Get large iota2 reference data test."""
    html_archive = "https://docs.iota2.net/data/large_i2_data_test.tar.gz"
    html_checksum = "https://docs.iota2.net/data/large_i2_data_test_md5sum.txt"

    checksum = requests.get(html_checksum, timeout=30).text.rstrip().split(" ")[0]

    expected_huge_date_dir_storage = str(
        Path(get_iota2_project_dir()) / "data" / "references" / "running_iota2"
    )
    if not Path(expected_huge_date_dir_storage).exists():
        raise ValueError(
            f"the directory '{expected_huge_date_dir_storage}'"
            " needed to store large iota2 test dataset doesn't"
            " exists"
        )
    large_i2_data_file = str(
        Path(expected_huge_date_dir_storage) / "large_i2_data_test.tar.gz"
    )
    dl_try = 0
    max_try = 5
    success = False

    while dl_try < max_try:
        if not Path(large_i2_data_file).exists():
            print("Download large iota2 data test in progress")
            download_url(html_archive, large_i2_data_file, 10000)
            print("Downloaded")
        file_checksum = md5(large_i2_data_file)
        if file_checksum == checksum:
            success = True
            shutil.unpack_archive(
                large_i2_data_file, expected_huge_date_dir_storage, format="gztar"
            )
            break
        download_url(html_archive, large_i2_data_file, 10000)
        dl_try += 1
    if not success:
        raise ValueError(
            f"Can't download iota2 data test archive : max try {max_try} reached "
        )


def run_cmd(cmd: str) -> None:
    """Run cmd in a subprocess and raise and Exception if the command fails."""
    with Popen(
        cmd,
        stdout=PIPE,
        stderr=PIPE,
        bufsize=1,
        universal_newlines=True,
        env=os.environ,
        shell=True,
    ) as process:
        if process.stdout:
            for line in process.stdout:
                print(line, end="")  # process line here
            err = ""
            if process.stderr:
                for line in process.stderr:
                    err += line
    if process.returncode != 0:
        raise Exception(err)
