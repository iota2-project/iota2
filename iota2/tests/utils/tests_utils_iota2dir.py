# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""refac some test utils"""

import os
from pathlib import Path

IOTA2DIR_AS_STR = os.environ.get("IOTA2DIR")
if IOTA2DIR_AS_STR is None:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR_AS_STR)
