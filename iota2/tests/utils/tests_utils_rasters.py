#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
This module offers some tools for writing tests
"""

import math
import random
import shutil
import xml.dom.minidom
from pathlib import Path
from typing import Literal

import numpy as np
import rasterio
from osgeo import gdal, osr
from rasterio.transform import from_origin

import iota2.common.file_utils as fut
from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.typings.i2_types import CoordXY, PathLike


def generate_custom_fake_raster(
    x_coords: CoordXY,
    y_coords: CoordXY,
    resolution: int,
    output_file_path: PathLike,
    values_range: tuple[int, int] | None = None,
) -> None:
    """
    Generate a raster with random values

    Parameters
    ----------
    x_coords: tuple[int | float]
        Tuple containing the min and max x-coordinate of the desired raster
    y_coords: tuple[int | float]
        Tuple containing the min and max y-coordinate of the desired raster
    resolution: int
        Resolution of the raster
    output_file_path:
        Path to the output raster
    values_range:
        Range for the random values (0-100 if not set)

    Returns
    -------
    None
    """
    if values_range is None:
        values_range = (0, 100)

    x_min, x_max = x_coords
    y_min, y_max = y_coords

    width = int((x_max - x_min) / resolution)
    height = int((y_max - y_min) / resolution)
    val_min, val_max = values_range
    data = np.random.randint(val_min, val_max, (height, width), dtype=np.int32)
    transform = from_origin(x_min, y_max, resolution, resolution)

    meta = {
        "driver": "GTiff",
        "dtype": "int32",
        "nodata": 0.0,
        "width": width,
        "height": height,
        "count": 1,
        "crs": "EPSG:2154",
        "transform": transform,
    }

    with rasterio.open(output_file_path, "w", **meta) as dst:
        dst.write(data, 1)


def generate_fake_classif(
    output_raster: str, label_list: list[int] | None = None
) -> None:
    """Generate a fake classification raster."""
    if label_list is None:
        label_list = [1, 2, 3]
    classif = []
    array = fun_array("iota2_binary")
    y_size, x_size = array.shape
    for _ in range(y_size):
        line = []
        for _ in range(x_size):
            line.append(np.random.choice(label_list))
        classif.append(np.asarray(line))
    classif_arr = np.asarray(classif) * array
    array_to_raster(classif_arr, output_raster)


def generate_range_raster(
    output_raster: str, min_range: int = 0, max_range: int = 100
) -> None:
    """generate a raster with bounded values"""
    arr = []
    array = fun_array("iota2_binary")
    y_size, x_size = array.shape
    for _ in range(y_size):
        line = []
        for _ in range(x_size):
            line.append(np.random.randint(min_range, max_range))
        arr.append(np.asarray(line))
    arr = np.asarray(arr) * array
    array_to_raster(arr, output_raster)


def compute_brightness_from_vector(input_vector: list[float]) -> float:
    """compute brightness from a vector of values

    Parameters
    ----------
    input_vector : list
        input vector
    Return
    ------
    brightness : float
        output brightness
    """
    brightness = None
    brightness = math.sqrt(sum(val**2 for val in input_vector))
    return brightness


def array_to_raster(
    input_array: np.ndarray | list[np.ndarray],
    output_raster_path: str,
    output_format: Literal["int", "float"] = "int",
    output_driver: str = "GTiff",
    pixel_size: float = 30.0,
    origin_x: float = 777225.58,
    origin_y: float = 6825084.53,
    epsg_code: int = 2154,
) -> None:
    """usage : from an array of a list of array, create a raster

    Parameters
    ----------
    input_array : list
        list of numpy.array sorted by bands (fisrt array = band 1,
                                             N array = band N)
    output_raster_path : str
        output path
    output_format : str
        'int' or 'float'
    output_driver: str
        gdal outut format
    pixel_size: float
        pixel resolution
    origin_x: float
        x origin raster coordinate
    origin_y: float
        y origin raster coordinate
    epsg_code: int
        epsg code
    """

    if not isinstance(input_array, list):
        input_array = [input_array]
    nb_bands = len(input_array)
    rows = input_array[0].shape[0]
    cols = input_array[0].shape[1]

    driver = gdal.GetDriverByName(output_driver)
    if output_format == "int":
        output_raster = driver.Create(
            output_raster_path, cols, rows, len(input_array), gdal.GDT_UInt16
        )
    elif output_format == "float":
        output_raster = driver.Create(
            output_raster_path, cols, rows, len(input_array), gdal.GDT_Float32
        )
    else:
        raise TypeError("Output format can only be 'int' or 'float'")
    if not output_raster:
        raise Exception("can not create : " + output_raster)
    output_raster.SetGeoTransform((origin_x, pixel_size, 0, origin_y, 0, -pixel_size))

    for n_band in range(nb_bands):
        outband = output_raster.GetRasterBand(n_band + 1)
        outband.WriteArray(input_array[n_band])

    output_raster_srs = osr.SpatialReference()
    output_raster_srs.ImportFromEPSG(epsg_code)
    output_raster.SetProjection(output_raster_srs.ExportToWkt())
    outband.FlushCache()


def fun_array(fun: str, oversized: bool = False) -> np.ndarray:
    """arrays used in unit tests"""

    if fun == "iota2_binary":
        values = [
            [
                0,
                0,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
            ],
            [
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
            ],
            [
                0,
                0,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
            ],
            [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
            ],
            [
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
            ],
            [
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
            ],
            [
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
            ],
            [
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
            ],
            [
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
            ],
            [
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ],
            [
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ],
            [
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ],
            [
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
            ],
            [
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
            ],
            [
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
            ],
            [
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
            ],
        ]
        array = np.array(values)

    elif fun == "ones":
        array = np.ones((16, 86))

    elif fun == "zeros":
        array = np.zeros((16, 86))
    else:
        raise ValueError(
            "Incorrect 'fun' value. Must be: 'iota2_binary', 'ones' or 'zeros'"
        )

    if oversized:
        array = np.concatenate((array, array), axis=0)
        array = np.concatenate((array, array), axis=1)
    assert isinstance(array, np.ndarray)
    return array


def generate_fake_raster(
    output_file: str, res: float = 30.0, array_name: str = "iota2_binary"
) -> None:
    """generate a mono-band raster"""
    origin_x = 566377
    origin_y = 6284029
    array_to_raster(
        fun_array(array_name),
        output_file,
        pixel_size=res,
        origin_x=origin_x,
        origin_y=origin_y,
    )


def generate_fake_raster_multi_band(
    output_file: str, res: float = 30.0, array_names: list[str] | None = None
) -> None:
    """generate a multi-band raster"""
    if array_names is None:
        array_names = []
    origin_x = 566377
    origin_y = 6284029
    input_arrays = []
    for array_name in array_names:
        input_arrays.append(fun_array(array_name))
    array_to_raster(
        input_arrays, output_file, pixel_size=res, origin_x=origin_x, origin_y=origin_y
    )


class FakeDataGenerator:
    """
    Class used to generate fake data for all sensors
    """

    origin_x = 566377
    origin_y = 6284029

    masks_of_interest = [""]
    bands_of_interest = [""]

    masks_dir_name = "MASK"

    empty_mask_flag = True

    def __init__(
        self,
        root_directory: str,
        tile_name: str,
        dates: list[str],
        res: float = 30.0,
        array_name: str = "iota2_binary",
        seed: int | None = None,
        oversized: bool = False,
    ):
        """
        Initialization method and set seed if needed.

        Parameters
        ----------
        root_directory: str
            Path to store the generated rasters
        tile_name: str
            Tile name (ex:T31TCJ)
        dates: list
            List of strings representing dates format : YYYYMMDD
        res: float
            Output rasters resolution
        array_name: string
            Pattern to build rasters
        seed: int | None
            Random seed
        oversized: bool
            If True, create a bigger raster (useful when envelope is eroded)
        """
        self.root_directory = root_directory
        self.tile_name = tile_name
        self.dates = dates
        self.res = res
        self.array_name = array_name
        self.seed = seed
        self.oversized = oversized

        if seed:
            random.seed(seed)

    def generate_data(self) -> None:
        """
        Generic function to generate all fake data
        """
        tile_dir = Path(self.root_directory) / self.tile_name
        tile_dir.mkdir(parents=True, exist_ok=True)

        for cpt, date in enumerate(self.dates):
            date_dir = self.get_date_dir_name(date, tile_dir)
            date_dir.mkdir(parents=True, exist_ok=True)

            self.generate_masks(cpt, date, date_dir)
            self.generate_data_rasters(date, date_dir)

    def generate_masks(self, cpt: int, date: str, date_dir: Path) -> None:
        """
        Generic function to generate mask rasters

        Parameters
        ----------
        cpt: int
            Number of the date
        date: str
            String representing the date
        date_dir:
            Directory where to store rasters for the given date
        """
        mask_date_dir = date_dir / self.masks_dir_name
        mask_date_dir.mkdir(parents=True, exist_ok=True)

        if self.empty_mask_flag:
            edge_mask_path = self.get_empty_mask_path(date, mask_date_dir)
            array_to_raster(
                (fun_array("zeros", oversized=self.oversized) + 1) * (cpt % 2),
                edge_mask_path,
                pixel_size=self.res,
                origin_x=self.origin_x,
                origin_y=self.origin_y,
            )

        # masks are defined in all children classes
        for mask_type in self.masks_of_interest:  # type: ignore
            mask_path = self.get_data_file_path(date, mask_type, mask_date_dir)
            array_to_raster(
                fun_array(self.array_name, oversized=self.oversized) * cpt % 2,
                mask_path,
                pixel_size=self.res,
                origin_x=self.origin_x,
                origin_y=self.origin_y,
            )

    def generate_data_rasters(self, date: str, date_dir: Path) -> None:
        """
        Generic function to generate data rasters

        Parameters
        ----------
        date: str
            String representing the date
        date_dir:
            Directory where to store rasters for the given date
        """
        all_bands = []
        # bands are defined in all children classes
        for band_name in self.bands_of_interest:  # type: ignore
            band_path = self.get_data_file_path(date, band_name, date_dir)
            all_bands.append(band_path)
            array = fun_array(self.array_name, oversized=self.oversized)

            random_array = array * np.random.random(array.shape) * 1000

            array_to_raster(
                random_array,
                band_path,
                pixel_size=self.res,
                origin_x=self.origin_x,
                origin_y=self.origin_y,
            )
        stack_path = self.get_stack_path(date, date_dir)
        stack_app, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES, {"il": all_bands, "out": stack_path}
        )
        stack_app.ExecuteAndWriteOutput()

    def get_date_dir_name(self, date: str, tile_dir: Path) -> Path:
        """
        Return the path to the date directory

        Parameters
        ----------
        date:
            String representing the date
        tile_dir
            Parent directory

        Raises
        ------
        NotImplementedError: only implemented for true sensors classes inherited from this one
        """
        raise NotImplementedError("Use a sensor specific method")

    def get_data_file_path(
        self, date: str, file_suffix: str, file_directory: Path
    ) -> str:
        """
        Return the path to a given data file, using a suffix in the file name representing the
        type of file (band number, mask type ...)

        Parameters
        ----------
        date: str
            String representing the date
        file_suffix: str
            String suffix specific to a sensor and file name/type
        file_directory: Path
            Directory where to store the raster

        Raises
        ------
        NotImplementedError: only implemented for true sensors classes inherited from this one
        """
        raise NotImplementedError("Use a sensor specific method")

    def get_stack_path(self, date: str, file_directory: Path) -> str:
        """
        Return the path to a stack

        Parameters
        ----------
        date: str
            String representing the date
        file_directory: Path
            Directory where to store the raster

        Raises
        ------
        NotImplementedError: only implemented for true sensors classes inherited from this one
        """
        raise NotImplementedError("Use a sensor specific method")

    def get_empty_mask_path(self, date: str, mask_date_dir: Path) -> str:
        """
        Some sensors require a special mask, filled with zeros. Return the path to this mask, if
        needed.

        Parameters
        ----------
        date: str
            String representing the date
        mask_date_dir: Path
            Directory where to store the raster

        Raises
        ------
        NotImplementedError: only implemented for true sensors classes inherited from this one
        """
        raise NotImplementedError("Use a sensor specific method")


class FakeS2DataGenerator(FakeDataGenerator):
    """
    Sentinel 2 fake data generator class
    """

    bands_of_interest = ["B2", "B3", "B4", "B5", "B6", "B7", "B8", "B8A", "B11", "B12"]
    masks_of_interest = ["CLM_R1", "SAT_R1"]
    masks_dir_name = "MASKS"

    empty_mask_flag = True

    def __init__(
        self,
        root_directory: str,
        tile_name: str,
        dates: list[str],
        res: float = 30.0,
        array_name: str = "iota2_binary",
        seed: int | None = None,
        oversized: bool = False,
    ):
        super().__init__(
            root_directory,
            tile_name,
            dates,
            res=res,
            array_name=array_name,
            seed=seed,
            oversized=oversized,
        )

    def get_date_dir_name(self, date: str, tile_dir: Path) -> Path:
        return (
            Path(tile_dir) / f"SENTINEL2B_{date}-000000-000_L2A_{self.tile_name}_D_V1-7"
        )

    def get_data_file_path(
        self, date: str, file_suffix: str, file_directory: Path
    ) -> str:
        return str(
            Path(file_directory)
            / f"SENTINEL2B_{date}-000000-000_L2A_{self.tile_name}_D_V1-7_FRE_{file_suffix}.tif"
        )

    def get_empty_mask_path(self, date: str, mask_date_dir: Path) -> str:
        return str(
            mask_date_dir
            / f"SENTINEL2B_{date}-000000-000_L2A_{self.tile_name}_D_V1-7_FRE_EDG_R1.tif"
        )

    def get_stack_path(self, date: str, file_directory: Path) -> str:
        return self.get_data_file_path(date, "STACK", file_directory)


class FakeL8DataGenerator(FakeDataGenerator):
    """
    Landsat 8 fake data generator class
    """

    bands_of_interest = ["B1", "B2", "B3", "B4", "B5", "B6", "B7"]
    masks_of_interest = ["BINARY_MASK", "CLM_XS", "SAT_XS"]
    masks_dir_name = "MASKS"

    empty_mask_flag = True

    def __init__(
        self,
        root_directory: str,
        tile_name: str,
        dates: list[str],
        res: float = 30.0,
        array_name: str = "iota2_binary",
    ):
        super().__init__(
            root_directory, tile_name, dates, res=res, array_name=array_name
        )

    def get_date_dir_name(self, date: str, tile_dir: Path) -> Path:
        return (
            Path(tile_dir)
            / f"LANDSAT8-OLITIRS-XS_{date}-000000-000_L2A_{self.tile_name}_D_V1-7"
        )

    def get_data_file_path(
        self, date: str, file_suffix: str, file_directory: Path
    ) -> str:
        return str(
            Path(file_directory)
            / f"LANDSAT8-OLITIRS-XS_{date}-000000-000_L2A_{self.tile_name}"
            f"_D_V1-7_FRE_{file_suffix}.tif"
        )

    def get_empty_mask_path(self, date: str, mask_date_dir: Path) -> str:
        return str(
            mask_date_dir
            / f"LANDSAT8-OLITIRS-XS_{date}-000000-000_L2A_{self.tile_name}_D_V1-7_FRE_EDG_XS.tif"
        )

    def get_stack_path(self, date: str, file_directory: Path) -> str:
        return self.get_data_file_path(date, "STACK", file_directory)


class FakeS2L3ADataGenerator(FakeDataGenerator):
    """
    Sentinel 2 L3A fake data generator class
    """

    bands_of_interest = [
        "FRC_B2",
        "FRC_B3",
        "FRC_B4",
        "FRC_B5",
        "FRC_B6",
        "FRC_B7",
        "FRC_B8",
        "FRC_B8A",
        "FRC_B11",
        "FRC_B12",
    ]
    masks_of_interest = ["BINARY_MASK", "FLG_R1"]
    empty_mask_flag = False
    masks_dir_name = "MASKS"

    stack_prefix = "FRC_"

    def __init__(
        self,
        root_directory: str,
        tile_name: str,
        dates: list[str],
        res: float = 30.0,
        array_name: str = "iota2_binary",
    ) -> None:
        super().__init__(
            root_directory, tile_name, dates, res=res, array_name=array_name
        )

    def get_date_dir_name(self, date: str, tile_dir: Path) -> Path:
        return (
            Path(tile_dir) / f"SENTINEL2X_{date}-000000-000_L3A_{self.tile_name}_D_V1-7"
        )

    def get_data_file_path(
        self, date: str, file_suffix: str, file_directory: Path
    ) -> str:
        return str(
            Path(file_directory)
            / f"SENTINEL2X_{date}-000000-000_L3A_{self.tile_name}_D_V1-7_{file_suffix}.tif",
        )

    def get_empty_mask_path(self, date: str, mask_date_dir: Path) -> str:
        raise NotImplementedError("No edge mask for S2 L3A")

    def get_stack_path(self, date: str, file_directory: Path) -> str:
        return str(
            file_directory
            / f"SENTINEL2X_{date}-000000-000_L3A_{self.tile_name}_D_V1-7_FRC_STACK.tif"
        )


class FakeL5OldDataGenerator(FakeDataGenerator):
    """
    Old Landsat 5 fake data generator class
    """

    bands_of_interest = ["B1", "B2", "B3", "B4", "B5", "B6"]
    masks_of_interest = ["BINARY_MASK", "NUA", "SAT"]
    masks_dir_name = "MASK"
    empty_mask_flag = True

    def __init__(
        self,
        root_directory: str,
        tile_name: str,
        dates: list[str],
        res: float = 30.0,
        array_name: str = "iota2_binary",
    ):
        super().__init__(
            root_directory, tile_name, dates, res=res, array_name=array_name
        )

    def get_date_dir_name(self, date: str, tile_dir: Path) -> Path:
        return tile_dir / f"LANDSAT5_TM_XS_{date}_N2A_{self.tile_name}"

    def get_data_file_path(
        self, date: str, file_suffix: str, file_directory: Path
    ) -> str:
        return str(
            Path(file_directory)
            / f"LANDSAT5_TM_XS_{date}_N2A_{self.tile_name}_{file_suffix}.TIF"
        )

    def get_empty_mask_path(self, date: str, mask_date_dir: Path) -> str:
        return str(
            Path(mask_date_dir) / f"LANDSAT5_TM_XS_{date}_N2A_{self.tile_name}_DIV.TIF"
        )

    def get_stack_path(self, date: str, file_directory: Path) -> str:
        return str(
            file_directory
            / f"LANDSAT5_TM_XS_{date}_N2A_ORTHO_SURF_CORR_PENTE_{self.tile_name}.TIF"
        )


class FakeL8OldDataGenerator(FakeDataGenerator):
    """
    Old Landsat 8 fake data generator class
    """

    bands_of_interest = ["B1", "B2", "B3", "B4", "B5", "B6", "B7"]
    masks_of_interest = ["BINARY_MASK", "NUA", "SAT"]
    masks_dir_name = "MASK"
    empty_mask_flag = True

    def __init__(
        self,
        root_directory: str,
        tile_name: str,
        dates: list[str],
        res: float = 30.0,
        array_name: str = "iota2_binary",
    ):
        super().__init__(
            root_directory, tile_name, dates, res=res, array_name=array_name
        )

    def get_date_dir_name(self, date: str, tile_dir: Path) -> Path:
        return tile_dir / f"LANDSAT8_OLITIRS_XS_{date}_N2A_{self.tile_name}"

    def get_data_file_path(
        self, date: str, file_suffix: str, file_directory: Path
    ) -> str:
        return str(
            Path(file_directory)
            / f"LANDSAT8_OLITIRS_XS_{date}_N2A_{self.tile_name}_{file_suffix}.TIF"
        )

    def get_empty_mask_path(self, date: str, mask_date_dir: Path) -> str:
        return str(
            Path(mask_date_dir)
            / f"LANDSAT8_OLITIRS_XS_{date}_N2A_{self.tile_name}_DIV.TIF"
        )

    def get_stack_path(self, date: str, file_directory: Path) -> str:
        return str(
            file_directory
            / f"LANDSAT8_OLITIRS_XS_{date}_N2A_ORTHO_SURF_CORR_PENTE_{self.tile_name}.TIF"
        )


def generate_s2c_data_tree(
    directory: PathLike, mtd_s2st_date: str, s2st_ext: str = "jp2"
) -> list[str]:
    """generate a fake Sen2Cor data
    TODO : replace this function by downloading a Sen2Cor data from PEPS.

    Return
    ------
    products : list
        list of data ready to be generated
    """
    Path(directory).mkdir(parents=True, exist_ok=True)

    dom_tree = xml.dom.minidom.parse(mtd_s2st_date)
    collection = dom_tree.documentElement
    general_info_node = collection.getElementsByTagName("n1:General_Info")
    date_dir = (
        general_info_node[0].getElementsByTagName("PRODUCT_URI")[0].childNodes[0].data
    )

    products = []
    for product_organisation_nodes in general_info_node[0].getElementsByTagName(
        "Product_Organisation"
    ):
        img_list_nodes = product_organisation_nodes.getElementsByTagName("IMAGE_FILE")
        for img_list in img_list_nodes:
            new_prod = str(
                Path(directory)
                / date_dir
                / f"{img_list.childNodes[0].data}.{s2st_ext}",
            )
            new_prod_dir = Path(new_prod).parent
            Path(new_prod_dir).mkdir(parents=True, exist_ok=True)
            products.append(new_prod)
    return products


def generate_fake_s2_s2c_data(
    output_directory: str,
    tile_name: str,
    mtd_files: list[str],
    fake_raster: list[np.ndarray] | None = None,
    fake_scene_classification: list[np.ndarray] | None = None,
    origin_x: float = 300000.0,
    origin_y: float = 4900020.0,
    epsg_code: int = 32631,
) -> None:
    """Generate fake s2_s2c data."""
    if fake_raster is None:
        fake_raster = [np.array([[10, 55, 61], [100, 56, 42], [1, 42, 29]])]
    if fake_scene_classification is None:
        fake_scene_classification = [np.array([[2, 0, 4], [0, 4, 2], [1, 1, 10]])]

    for mtd in mtd_files:
        prod_list = generate_s2c_data_tree(str(Path(output_directory) / tile_name), mtd)
        for prod in prod_list:
            pix_size = 60
            if "10m.jp2" in prod:
                pix_size = 10
            if "20m.jp2" in prod:
                pix_size = 20
            if "60m.jp2" in prod:
                pix_size = 60
            if "_SCL_" in prod:
                array_raster = fake_scene_classification
            else:
                array_raster = fake_raster
            # output_driver has to be 'GTiff' even if S2ST are jp2
            array_to_raster(
                array_raster,
                prod,
                output_driver="GTiff",
                output_format="int",
                pixel_size=pix_size,
                origin_x=origin_x,
                origin_y=origin_y,
                epsg_code=epsg_code,
            )


def generate_fake_l8_usgs_data(
    root_directory: str,
    dates: list[str],
    res: float = 30.0,
    seed: int | None = None,
    array_name: str = "iota2_binary",
    oversized: bool = False,
    path_row: str = "200029",
) -> None:
    """
    Parameters
    ----------
    root_directory : string
        path to generate Landsat8 dates
    tile_name : string
        USGS tile name (ex:200029)
    dates : list
        list of strings representing dates format : YYYYMMDD
    res : float
        raster's resolution
    array_name : string
        pattern to build rasters
    """
    tile_dir = str(Path(root_directory) / f"tile{path_row}")
    Path(tile_dir).mkdir(parents=True, exist_ok=True)

    bands_of_interest = [
        "SR_B1",
        "SR_B2",
        "SR_B3",
        "SR_B4",
        "SR_B5",
        "SR_B6",
        "SR_B7",
        "ST_B10",
    ]

    origin_x = 566377
    origin_y = 6284029
    if seed:
        random.seed(seed)
    for cpt, date in enumerate(dates):
        date_dir = str(Path(tile_dir) / f"LC08_L2SP_{path_row}_{date}_{date}_02_T1")
        Path(date_dir).mkdir(parents=True, exist_ok=True)
        Path(date_dir).mkdir(parents=True, exist_ok=True)
        all_bands = []

        array_to_raster(
            (fun_array("zeros", oversized=oversized) + 1) * (cpt % 2),
            str(Path(date_dir) / f"LC08_L2SP_{path_row}_{date}_{date}_02_T1_EDGE.TIF"),
            pixel_size=res,
            origin_x=origin_x,
            origin_y=origin_y,
        )
        # masks
        for mask in ["QA_PIXEL", "QA_RADSAT"]:
            array = fun_array(array_name, oversized=oversized)
            random_array = array * np.random.random(array.shape) * 1000
            array_to_raster(
                np.array(random_array) + 10,
                str(
                    Path(date_dir)
                    / f"LC08_L2SP_{path_row}_{date}_{date}_02_T1_{mask}.TIF"
                ),
                pixel_size=res,
                origin_x=origin_x,
                origin_y=origin_y,
            )

        for band in bands_of_interest:
            new_band = str(
                Path(date_dir) / f"LC08_L2SP_{path_row}_{date}_{date}_02_T1_{band}.TIF",
            )
            all_bands.append(new_band)
            array = fun_array(array_name, oversized=oversized)
            random_array = array * np.random.random(array.shape) * 1000

            array_to_raster(
                np.array(random_array),
                new_band,
                pixel_size=res,
                origin_x=origin_x,
                origin_y=origin_y,
            )

        # level 1 bands
        for band in ["B10", "B11"]:
            new_band = str(
                Path(date_dir) / f"LC08_L1TP_{path_row}_{date}_{date}_02_T1_{band}.TIF",
            )
            all_bands.append(new_band)
            array = fun_array(array_name, oversized=oversized)
            random_array = array * np.random.random(array.shape) * 1000
            array_to_raster(
                np.array(random_array),
                new_band,
                pixel_size=res,
                origin_x=origin_x,
                origin_y=origin_y,
            )


def generate_fake_user_features_data(
    root_directory: str,
    tile_name: str,
    patterns: list[str],
    array_name: str = "iota2_binary",
    force_value: int | float | None = None,
) -> None:
    """
    Parameters
    ----------
    root_directory : string
        path to generate Sentinel-2 dates
    tile_name : string
        THEIA tile name (ex:T31TCJ)
    dates : list
        List of raster's name
    """

    tile_dir = str(Path(root_directory) / tile_name)
    Path(tile_dir).mkdir(parents=True, exist_ok=True)

    origin_x = 566377
    origin_y = 6284029

    array = fun_array(array_name)
    for pattern in patterns:
        user_features_path = str(Path(tile_dir) / f"{pattern}.tif")
        random_values = []
        if force_value is None:
            for val in array:
                val_tmp = []
                for pix_val in val:
                    val_tmp.append(pix_val * random.random() * 1000)
                    random_values.append(val_tmp)
            random_array = np.array(random_values)
        else:
            random_array = np.ones(array.shape) * force_value
        array_to_raster(
            random_array,
            user_features_path,
            origin_x=origin_x,
            origin_y=origin_y,
        )


def prepare_annual_features(
    working_directory: str,
    reference_directory: PathLike,
    pattern: str,
    rename: tuple[str, str],
) -> None:
    """
    double all raster's pixels
    rename must be a tuple
    """

    for path in fut.walk(Path(reference_directory)):
        new_path = Path(
            str(path)
            .replace(str(reference_directory), str(working_directory))
            .replace(rename[0], rename[1])
        )

        if path.is_dir():
            new_path.mkdir(parents=True, exist_ok=True)
        else:
            new_path.parent.mkdir(parents=True, exist_ok=True)
            shutil.copy(path, new_path)

    rasters_path = fut.file_search_and(working_directory, True, pattern)
    for raster in rasters_path:
        app, _ = create_application(
            AvailableOTBApp.BAND_MATH_X,
            {
                "il": [raster],
                "out": raster,
                "exp": "im1+im1",
            },
        )
        app.ExecuteAndWriteOutput()


def compare_metadata(
    file1_md: dict[str, str], file2_md: dict[str, str], id_metadata: str
) -> int:
    """
    Compare files metadata

    Parameters
    ----------
    file1_md: dict[str, str]
        Gdal metadata of the first file
    file2_md: dict[str, str]
        Gdal metadata of the second file
    id_metadata: str
        Metadata identifier

    Returns
    -------
    found_diff: int
        Number of differences found
    """
    if file1_md is None and file2_md is None:
        return 0

    found_diff = 0

    if len(list(file1_md.keys())) != len(list(file2_md.keys())):
        print(f"Difference in {id_metadata} metadata key count")
        print("  file1 Keys: " + str(list(file1_md.keys())))
        print("  file2 Keys: " + str(list(file2_md.keys())))
        found_diff += 1

    for key in list(file1_md.keys()):
        if key not in file2_md:
            print(f'file2 {id_metadata} metadata lacks key "{key}"')
            found_diff += 1
        elif file2_md[key] != file1_md[key]:
            print('Metadata value difference for key "' + key + '"')
            print('  file1: "' + file1_md[key] + '"')
            print('  file2:    "' + file2_md[key] + '"')
            found_diff += 1

    return found_diff


def compare_image_pixels(file1_band: gdal.Band, file2_band: gdal.Band) -> None:
    """
    Review and report on the actual image pixels that differ.

    Parameters
    ----------
    file1_band: gdal.Band
        First file to compare
    file2_band: gdal.Band
        Second file to compare

    Returns
    -------
    None
    """
    diff_count = 0
    max_diff = 0

    for line in range(file1_band.YSize):
        file1_line = file1_band.ReadAsArray(0, line, file1_band.XSize, 1)[0]
        file2_line = file2_band.ReadAsArray(0, line, file1_band.XSize, 1)[0]
        diff_line = file1_line.astype(float) - file2_line.astype(float)
        max_diff = max(max_diff, abs(diff_line).max())
        diff_count += len(diff_line.nonzero()[0])

    print("  Pixels Differing: " + str(diff_count))
    print("  Maximum Pixel Difference: " + str(max_diff))


def compare_band(
    file1_band: gdal.Band,
    file2_band: gdal.Band,
    id_band: str,
    options: list | None = None,
) -> int:
    """
    Compare two GDAL bands and report differences.

    Parameters
    ----------
    file1_band: gdal.Band
        The first GDAL band to compare
    file2_band: gdal.Band
        The second GDAL band to compare
    id_band: str
        Band identifier
    options: list, optional
        List of comparison options

    Returns
    -------
    int
        The number of differences found between the two bands.
    """
    if options is None:
        options = []
    found_diff = 0

    if file1_band.DataType != file2_band.DataType:
        print(f"Band {id_band} pixel types differ.")
        print("  file1: " + gdal.GetDataTypeName(file1_band.DataType))
        print("  file2:    " + gdal.GetDataTypeName(file2_band.DataType))
        found_diff += 1

    if file1_band.GetNoDataValue() != file2_band.GetNoDataValue():
        print(f"Band {id_band} nodata values differ.")
        print("  file1: " + str(file1_band.GetNoDataValue()))
        print("  file2:    " + str(file2_band.GetNoDataValue()))
        found_diff += 1

    if file1_band.GetColorInterpretation() != file2_band.GetColorInterpretation():
        print(f"Band {id_band} color interpretation values differ.")
        print(
            "  file1: "
            + gdal.GetColorInterpretationName(file1_band.GetColorInterpretation())
        )
        print(
            "  file2:    "
            + gdal.GetColorInterpretationName(file2_band.GetColorInterpretation())
        )
        found_diff += 1

    if file1_band.Checksum() != file2_band.Checksum():
        print(f"Band {id_band} checksum difference:")
        print("  file1: " + str(file1_band.Checksum()))
        print("  file2:    " + str(file2_band.Checksum()))
        found_diff += 1
        compare_image_pixels(file1_band, file2_band)

    # Check overviews
    if file1_band.GetOverviewCount() != file2_band.GetOverviewCount():
        print(f"Band {id_band} overview count difference:")
        print("  file1: " + str(file1_band.GetOverviewCount()))
        print("  file2:    " + str(file2_band.GetOverviewCount()))
        found_diff += 1
    else:
        for n_overview in range(file1_band.GetOverviewCount()):
            found_diff += compare_band(
                file1_band.GetOverview(n_overview),
                file2_band.GetOverview(n_overview),
                id_band + " overview " + str(n_overview),
                options,
            )

    # Metadata
    if "SKIP_METADATA" not in options:
        found_diff += compare_metadata(
            file1_band.GetMetadata(),
            file2_band.GetMetadata(),
            "Band " + id_band,
        )
    return found_diff


def compare_srs(file1_wkt: str, file2_wkt: str) -> int:
    """
    Compare two spatial reference systems (SRS) in WKT format.

    Parameters
    ----------
    file1_wkt : str
        SRS of the first file
    file2_wkt : str
        SRS of the first file

    Returns
    -------
    int
        Returns 0 if the SRS are identical, 1 if they differ.
    """
    retour = 1
    if file1_wkt == file2_wkt:
        retour = 0
    else:
        print("Difference in SRS!")

        file1_srs = osr.SpatialReference(file1_wkt)
        file2_srs = osr.SpatialReference(file2_wkt)

        if file1_srs.IsSame(file2_srs):
            print("  * IsSame() reports them as equivalent.")
        else:
            print("  * IsSame() reports them as different.")

        print("  file1:")
        print("  " + file1_srs.ExportToPrettyWkt())
        print("  file2:")
        print("  " + file2_srs.ExportToPrettyWkt())

    return retour


def compare_gdal(
    file1_gdal: gdal.Dataset,
    file2_gdal: gdal.Dataset,
    options: list | None = None,
) -> int:
    """
    Compare two GDAL datasets and report differences.

    Parameters
    ----------
    file1_gdal: gdal.Dataset
        The first GDAL dataset to compare
    file2_gdal: gdal.Dataset
        The second GDAL dataset to compare
    options : list | None
        List of options to skip comparisons. Possible values are:
        - "SKIP_SRS": Skip comparison of spatial reference systems.
        - "SKIP_GEOTRANSFORM": Skip comparison of geotransform matrices.
        - "SKIP_METADATA": Skip comparison of dataset metadata.

    Returns
    -------
    found_diff: int
        The number of differences found.
    """
    if options is None:
        options = []
    found_diff = 0

    # SRS
    if "SKIP_SRS" not in options:
        found_diff += compare_srs(
            file1_gdal.GetProjection(), file2_gdal.GetProjection()
        )

    # GeoTransform
    if "SKIP_GEOTRANSFORM" not in options:
        file1_gt = file1_gdal.GetGeoTransform()
        file2_gt = file2_gdal.GetGeoTransform()
        if file1_gt != file2_gt:
            print("GeoTransforms Differ:")
            print("  file1: " + str(file1_gt))
            print("  file2:    " + str(file2_gt))
            found_diff += 1

    # Metadata
    if "SKIP_METADATA" not in options:
        found_diff += compare_metadata(
            file1_gdal.GetMetadata(), file2_gdal.GetMetadata(), "Dataset"
        )

    # Bands
    if file1_gdal.RasterCount != file2_gdal.RasterCount:
        print(
            f"Band count mismatch (file1={file1_gdal.RasterCount}, "
            f"file2={file2_gdal.RasterCount})"
        )
        found_diff += 1

    # Dimensions
    for n_raster in range(file1_gdal.RasterCount):
        file1_size_x = file1_gdal.GetRasterBand(n_raster + 1).XSize
        file2_size_x = file2_gdal.GetRasterBand(n_raster + 1).XSize
        file1_size_y = file1_gdal.GetRasterBand(n_raster + 1).YSize
        file2_size_y = file2_gdal.GetRasterBand(n_raster + 1).YSize

        if file1_size_x != file2_size_x or file1_size_y != file2_size_y:
            print(
                f"Band size mismatch (band={n_raster} file1=[{file1_size_x},{file1_size_y}], "
                f"file2=[{file2_size_x},{file2_size_y}])"
            )
            found_diff += 1

    # If so-far-so-good, then compare pixels
    if found_diff == 0:
        for n_raster in range(file1_gdal.RasterCount):
            found_diff += compare_band(
                file1_gdal.GetRasterBand(n_raster + 1),
                file2_gdal.GetRasterBand(n_raster + 1),
                str(n_raster + 1),
                options,
            )

    return found_diff


def compare_gdal_sds(
    file1_db: gdal.Dataset,
    file2_db: gdal.Dataset,
    options: list | None = None,
) -> int:
    """
    Compare subdatasets within two GDAL datasets and report any differences.

    Parameters
    ----------
    file1_db: gdal.Dataset
        The first GDAL dataset containing subdatasets to compare.
    file2_db: gdal.Dataset
        The second GDAL dataset containing subdatasets to compare.
    options: list | None
        List of comparison options.

    Returns
    -------
    int
        The total number of differences found between the subdatasets.
    """
    if options is None:
        options = []
    found_diff = 0

    file1_sds = file1_db.GetMetadata("SUBDATASETS")
    file2_sds = file2_db.GetMetadata("SUBDATASETS")

    count = int(len(list(file1_sds.keys())) / 2)
    for n_subdataset in range(count):
        key = f"SUBDATASET_{n_subdataset + 1}_NAME"

        sub_file1_db = gdal.Open(file1_sds[key])
        sub_file2_db = gdal.Open(file2_sds[key])

        sds_diff = compare_gdal(sub_file1_db, sub_file2_db, options)
        found_diff += sds_diff
        if sds_diff > 0:
            print(
                f"{sds_diff} differences found between:\n  {file1_sds[key]}\n  "
                f"{file2_sds[key]}"
            )

    return found_diff


def gdal_file_compare(file1: PathLike, file2: PathLike) -> int:
    """
    Compare two GDAL-compatible files and return the number of differences.

    Parameters
    ----------
    file1: PathLike
        Path to the first file.
    file2: PathLike
        Path to the second file.

    Returns
    -------
    difference: int
        The total number of differences found between the two files.

    Raises
    ------
    Exception
        If either file cannot be opened.

    """
    try:
        Path(file1).stat()
    except Exception as exc:
        raise Exception(f"Could not open  {file1}") from exc
    try:
        Path(file2).stat()
    except Exception as exc:
        raise Exception(f"Could not open {file2}") from exc

    file1_gdal = gdal.Open(file1)
    file2_gdal = gdal.Open(file2)

    check_sub_data_set = False

    difference = 0

    difference += compare_gdal(file1_gdal, file2_gdal)

    if check_sub_data_set:
        difference += compare_gdal_sds(file1_gdal, file2_gdal)

    return difference
