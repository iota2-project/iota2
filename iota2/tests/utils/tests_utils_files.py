#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Utility tools for testing, dedicated to deal with files"""
import json
import random
import shutil
import xml.etree.ElementTree as ET
from collections import Counter
from datetime import datetime
from pathlib import Path

import rasterio

from iota2.configuration_files import read_config_file as rcf
from iota2.typings.i2_types import PathLike


def check_color_table(raster_file: str) -> bool:
    """
    Checks if the color table of the first band in a raster file contains only
    (255, 255, 255, 255), (0, 0, 0, 255).

    The function opens a raster file, retrieves the color table of the first band, and compares
    the occurrence of specific color entries . The function returns
    `True` if the color table contains only (255, 255, 255, 255), (0, 0, 0, 255)., and `False`
    otherwise.

    Parameters
    ----------
    raster_file : str
        Path to the raster file to be checked.

    Returns
    -------
    bool
        `True` if the color table contains only (255, 255, 255, 255), (0, 0, 0, 255).
    """
    with rasterio.open(raster_file) as src:
        if src.colormap(1):
            colormap = src.colormap(1)
        else:
            return False
    return list(dict(Counter(list(colormap.values()))).keys()) != [
        (255, 255, 255, 255),
        (0, 0, 0, 255),
    ]


def check_expected_i2_results(
    i2_final_dir: Path,
    report: bool = True,
    classif: bool = True,
    classif_color: bool = True,
    confidence: bool = True,
    pixel_val: bool = True,
    confusion_fig: bool = True,
    diff_seed: bool = True,
    multi_seed: bool = False,
) -> tuple[bool, dict]:
    """Check expected final results."""
    content = [str(elem.name) for elem in Path.iterdir(i2_final_dir)]
    _ = {}
    if report:
        _["RESULTS.txt"] = "RESULTS.txt" in content
    if classif:
        _["Classif_Seed_0.tif"] = "Classif_Seed_0.tif" in content
    if classif_color:
        _["Classif_Seed_0_ColorIndexed.tif"] = (
            "Classif_Seed_0_ColorIndexed.tif" in content
        )
        if _["Classif_Seed_0_ColorIndexed.tif"]:
            _["Classif_Seed_0_ColorIndexed.tif"] = check_color_table(
                str(i2_final_dir / "Classif_Seed_0_ColorIndexed.tif")
            )

    if confidence:
        _["Confidence_Seed_0.tif"] = "Confidence_Seed_0.tif" in content
    if pixel_val:
        _["PixelsValidity.tif"] = "PixelsValidity.tif" in content
    if confusion_fig:
        _["Confusion_Matrix_Classif_Seed_0.png"] = (
            "Confusion_Matrix_Classif_Seed_0.png" in content
        )
    if diff_seed:
        _["diff_seed_0.tif"] = "diff_seed_0.tif" in content
    if multi_seed:
        _["Classifications_fusion_ColorIndexed.tif"] = (
            "Classifications_fusion_ColorIndexed.tif" in content
        )
        _["Classifications_fusion.tif"] = "Classifications_fusion.tif" in content
    if report and multi_seed:
        _["RESULTS_seeds.txt"] = "RESULTS_seeds.txt" in content
    if confusion_fig and multi_seed:
        _["fusionConfusion.png"] = "fusionConfusion.png" in content

    return all(val for key, val in _.items()), _


def cmp_xml_stat_files(xml_1: str, xml_2: str) -> bool:
    """compare statistics xml files

    samplesPerClass and samplesPerVector tags from input files are
    compared without considering line's order

    Parameters
    ----------
    xml_1 : string
        statistics file from otbcli_PolygonClassStatistics
    xml_2 : string
        statistics file from otbcli_PolygonClassStatistics

    Return
    ------
    bool
        True if content are equivalent
    """
    xml_1_stats = {}
    tree_1 = ET.parse(xml_1)
    root_1 = tree_1.getroot()

    xml_2_stats = {}
    tree_2 = ET.parse(xml_2)
    root_2 = tree_2.getroot()

    xml_1_stats["samplesPerClass"] = {
        (samplesPerClass.attrib["key"], samplesPerClass.attrib["value"])
        for samplesPerClass in root_1[0]
    }
    xml_1_stats["samplesPerVector"] = {
        (samplesPerClass.attrib["key"], samplesPerClass.attrib["value"])
        for samplesPerClass in root_1[1]
    }

    xml_2_stats["samplesPerClass"] = {
        (samplesPerClass.attrib["key"], samplesPerClass.attrib["value"])
        for samplesPerClass in root_2[0]
    }
    xml_2_stats["samplesPerVector"] = {
        (samplesPerClass.attrib["key"], samplesPerClass.attrib["value"])
        for samplesPerClass in root_2[1]
    }

    return xml_1_stats == xml_2_stats


def generate_code_coverage_file(filepath: PathLike, num_files: int = 15) -> dict:
    """
    Function used to generate fake coverage json files
    Parameters
    ----------
    filepath:
        Path to save the file
    num_files:
        Number of fake files to use
    """
    fake_data = {
        "meta": {
            "format": 2,
            "version": "7.4.3",
            "timestamp": datetime.now().isoformat(),
            "branch_coverage": "false",
            "show_contexts": "false",
        },
        "files": {},
        "totals": {
            "covered_lines": 0,
            "num_statements": 0,
            "percent_covered": 0,
            "percent_covered_display": "0",
            "missing_lines": 0,
            "excluded_lines": 0,
        },
    }

    total_covered_lines = 0
    total_num_statements = 0
    total_missing_lines = 0

    for n_file in range(num_files):
        num_statements = random.randint(20, 50)
        covered_lines = random.randint(10, num_statements)
        missing_lines_count = num_statements - covered_lines
        percent_covered = round((covered_lines / num_statements) * 100, 2)

        executed_lines = sorted(
            random.sample(range(1, num_statements + 1), covered_lines)
        )
        missing_lines = sorted(set(range(1, num_statements + 1)) - set(executed_lines))

        fake_data["files"][f"/fake/path/to/iota2/file{n_file+1}.py"] = {
            "executed_lines": executed_lines,
            "summary": {
                "covered_lines": covered_lines,
                "num_statements": num_statements,
                "percent_covered": percent_covered,
                "percent_covered_display": str(round(percent_covered)),
                "missing_lines": missing_lines_count,
                "excluded_lines": 0,
            },
            "missing_lines": missing_lines,
            "excluded_lines": [],
        }

        total_covered_lines += covered_lines
        total_num_statements += num_statements
        total_missing_lines += len(missing_lines)

    overall_percent_covered = round(
        (total_covered_lines / total_num_statements) * 100, 2
    )
    fake_data["totals"] = {
        "covered_lines": total_covered_lines,
        "num_statements": total_num_statements,
        "percent_covered": overall_percent_covered,
        "percent_covered_display": str(round(overall_percent_covered)),
        "missing_lines": total_missing_lines,
        "excluded_lines": 0,
    }

    with open(filepath, "w", encoding="UTF-8") as file:
        json.dump(fake_data, file, indent=4)

    return fake_data


def copy_and_modify_config(
    config_source: str, config_dest: str, modifications: dict
) -> None:
    """Helper function to copy and modify config."""
    shutil.copy(config_source, config_dest)
    cfg = rcf.ReadInternalConfigFile(config_dest)
    config_dict = cfg.cfg_as_dict

    for section, params in modifications.items():
        if section not in config_dict:
            config_dict[section] = {}

        for key, value in params.items():
            config_dict[section][key] = value

    cfg.save(config_dest)
