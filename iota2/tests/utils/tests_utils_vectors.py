# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""This module offers some tools for writing tests."""
import random
import sqlite3 as lite
from itertools import zip_longest
from pathlib import Path
from typing import Any

import numpy as np
import pandas as pad
from osgeo import ogr, osr

from iota2.common import otb_app_bank as otb
from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.common.raster_utils import raster_to_array
from iota2.common.utils import run
from iota2.sampling.tile_envelope import get_shape_extent
from iota2.typings.i2_types import CoordXY, PathLike
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.add_field import add_field
from iota2.vector_tools.delete_field import delete_field


def rename_table(vect_file: PathLike, old_table_name: str, new_table_name: str) -> None:
    """Use in test_split_selection Test."""
    sql_clause = f"ALTER TABLE {old_table_name} RENAME TO {new_table_name}"
    conn = lite.connect(vect_file)
    cursor = conn.cursor()
    cursor.execute(sql_clause)
    conn.commit()


def compare_vector_raster(
    in_vec: str, vec_field: str, field_values: list[int], in_img: str
) -> list[int]:
    """Return img values at vector positions where field is equal to a given values."""
    rasterization, _ = create_application(
        AvailableOTBApp.RASTERIZATION,
        {
            "in": in_vec,
            "im": in_img,
            "mode": "attribute",
            "mode.attribute.field": vec_field,
        },
    )
    rasterization.Execute()

    vec_array = rasterization.GetImageAsNumpyArray("out")
    y_coords, x_coords = np.where(np.isin(vec_array, field_values))
    classif_array = raster_to_array(in_img)
    values = []
    for y_coord, x_coord in zip(y_coords, x_coords):
        values.append(classif_array[y_coord][x_coord])
    return values


def random_features_database_generator(
    output_db: str, features_fields: list[str], features_numbers: int = 2
) -> None:
    """Generate SQLite  ile with random integer values in appropriate fields."""
    driver = ogr.GetDriverByName("SQLite")
    Path(output_db).unlink(missing_ok=True)

    data_source = driver.CreateDataSource(output_db)

    layer_name = Path(output_db).stem
    layer = data_source.CreateLayer(layer_name, geom_type=ogr.wkbPoint)
    for field in features_fields:
        data_field_name = ogr.FieldDefn(field, ogr.OFTInteger)
        data_field_name.SetWidth(10)
        layer.CreateField(data_field_name)

    for _ in range(features_numbers):
        feature = ogr.Feature(layer.GetLayerDefn())
        for feature_field in features_fields:
            feature.SetField(feature_field, random.randrange(1, 100))
        layer.CreateFeature(feature)
    data_source = None


def random_ground_truth_generator(
    output_shape: PathLike,
    data_field: str,
    number_of_class: int,
    region_field: str | None = None,
    min_cl_samples: int = 10,
    max_cl_samples: int = 100,
    epsg_code: int = 2154,
    set_geom: bool = True,
) -> None:
    """Generate a shape file with random integer values in appropriate field.

    Parameters
    ----------
    output_shape : string
        output shapeFile
    data_field : string
        data field
    number_of_class : int
        number of class
    region_field : string
        region field
    min_cl_samples : int
        minimum samples per class
    max_cl_samples : int
        maximum samples per class
    epsg_code : int
        epsg code
    set_geom : bool
        set a fake geometry
    """
    assert (
        max_cl_samples > min_cl_samples
    ), "max_cl_samples must be superior to min_cl_samples"

    label_number = []
    for class_label in range(number_of_class):
        label_number.append(
            (class_label + 1, random.randrange(min_cl_samples, max_cl_samples))
        )

    driver = ogr.GetDriverByName("ESRI Shapefile")
    if Path(output_shape).exists():
        driver.DeleteDataSource(output_shape)

    data_source = driver.CreateDataSource(output_shape)

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg_code)

    layer_name = Path(output_shape).stem
    layer = data_source.CreateLayer(layer_name, srs, geom_type=ogr.wkbPolygon)
    data_field_name = ogr.FieldDefn(data_field, ogr.OFTInteger)
    data_field_name.SetWidth(10)
    layer.CreateField(data_field_name)
    if region_field:
        region_field_name = ogr.FieldDefn(region_field, ogr.OFTString)
        region_field_name.SetWidth(10)
        layer.CreateField(region_field_name)

    for class_label, features_num in label_number:
        for _ in range(features_num):
            feature = ogr.Feature(layer.GetLayerDefn())
            feature.SetField(data_field, class_label)
            if region_field:
                feature.SetField(region_field, "1")
            point = ogr.CreateGeometryFromWkt("POLYGON ((1 2, 2 2, 2 1, 1 1, 1 2))")
            if set_geom:
                feature.SetGeometry(point)
            layer.CreateFeature(feature)
            feature = None
    data_source = None


def shape_reference_vector(ref_vector: str, output_name: str) -> str:
    """Modify reference vector (add field, rename...).

    Parameters
    ----------
    ref_vector : string
    output_name : string

    Return
    ------
    string
    """
    path = str(Path(ref_vector).parent)

    tmp_shapefile = str(Path(path) / f"{output_name}_TMP.shp")
    vf.copy_shapefile(ref_vector, tmp_shapefile)
    add_field(tmp_shapefile, "region", "1", str)
    add_field(tmp_shapefile, "seed_0", "learn", str)
    cmd = (
        f"ogr2ogr -dialect 'SQLite' -sql 'select GEOMETRY,seed_0, "
        f"region, CODE as code from {output_name}_TMP' "
        f"{path}/{output_name}.shp {tmp_shapefile}"
    )
    run(cmd)
    vf.remove_shape(tmp_shapefile)
    return path + "/" + output_name + ".shp"


def prepare_test_selection(
    vector: str,
    raster_ref: str,
    output_selection: str,
    working_directory: str,
    data_field: str,
) -> None:
    """Prepare vector selection files.

    Parameters
    ----------
    vector: string
    raster_ref: string
    output_selection: string
    working_directory: string
    data_field: string

    Return
    ------
    None
    """
    stats_path = str(Path(working_directory) / "stats.xml")
    Path(stats_path).unlink(missing_ok=True)
    stats, _ = otb.create_application(
        AvailableOTBApp.POLYGON_CLASS_STATISTICS,
        {
            "in": raster_ref,
            "vec": vector,
            "field": data_field.lower(),
            "out": stats_path,
        },
    )
    stats.ExecuteAndWriteOutput()
    sample_sel, _ = otb.create_application(
        AvailableOTBApp.SAMPLE_SELECTION,
        {
            "in": raster_ref,
            "vec": vector,
            "out": output_selection,
            "instats": stats_path,
            "sampler": "random",
            "strategy": "all",
            "field": data_field.lower(),
        },
    )
    Path(output_selection).unlink(missing_ok=True)
    sample_sel.ExecuteAndWriteOutput()
    Path(stats_path).unlink()


def delete_useless_fields(test_vector: str, field_to_rm: str = "region") -> None:
    """Delete fields.

    Parameters
    ----------
    test_vector: string
    field_to_rm: string

    Return
    ------
    None
    """
    fields = vf.get_all_fields_in_shape(test_vector, driver_name="SQLite")
    rm_field = [field for field in fields if field_to_rm in field]
    for field_to_remove in rm_field:
        delete_field(test_vector, field_to_remove)


def compare_sqlite(
    vect_1: str,
    vect_2: str,
    cmp_mode: str = "table",
    ignored_fields: list[str] | None = None,
) -> bool:
    """Compare SQLite, table mode is faster but does not work with connected OTB applications.

    return true if vectors are the same
    """
    if ignored_fields is None:
        ignored_fields = []

    fields_1 = vf.get_all_fields_in_shape(vect_1, "SQLite")
    fields_2 = vf.get_all_fields_in_shape(vect_2, "SQLite")

    if len([field for field in fields_1 if field not in ignored_fields]) != len(
        [field for field in fields_2 if field not in ignored_fields]
    ):
        return False

    if cmp_mode == "table":
        connection_1 = lite.connect(vect_1)
        df_1 = pad.read_sql_query("SELECT * FROM output", connection_1)

        connection_2 = lite.connect(vect_2)
        df_2 = pad.read_sql_query("SELECT * FROM output", connection_2)

        try:
            table = (df_1 != df_2).any(axis=1)
            out = True
            if True in table.tolist():
                out = False
            return out
        except ValueError:
            return False

    elif cmp_mode == "coordinates":
        values_1 = get_values_sorted_by_coordinates(
            vect_1,
            ignored_fields_list=ignored_fields,
            geometry_type="point",
            driver_name="SQLite",
        )
        values_2 = get_values_sorted_by_coordinates(
            vect_2,
            ignored_fields_list=ignored_fields,
            geometry_type="point",
            driver_name="SQLite",
        )
        same_feat = []
        for val_1, val_2 in zip(values_1, values_2):
            for (key_1, v_1), (key_2, v_2) in zip(
                list(val_1[1].items()), list(val_2[1].items())
            ):
                if key_1 not in ignored_fields and key_2 not in ignored_fields:
                    # same_feat.append(cmp(v_1, v_2) == 0)
                    same_feat.append(v_1 == v_2)
        if False in same_feat:
            return False
        return True
    else:
        raise Exception("CmpMode parameter must be 'table' or 'coordinates'")


def multi_polygons_search(shp: str, ogr_driver: str = "ESRI Shapefile") -> bool:
    """Return true if shp contains one or more 'MULTIPOLYGON'.

    Parameters
    ----------
    shp :
        path to a shapeFile
    ogr_driver :
        ogr driver name

    """
    driver = ogr.GetDriverByName(ogr_driver)
    in_ds = driver.Open(shp, 0)
    in_lyr = in_ds.GetLayer()

    retour = False
    for in_feat in in_lyr:
        geom = in_feat.GetGeometryRef()
        if geom.GetGeometryName() == "MULTIPOLYGON":
            retour = True
    return retour


def test_same_shapefiles(
    vector1: PathLike, vector2: PathLike, driver_name: str = "ESRI Shapefile"
) -> bool:
    """

    Parameters
    ----------
    vector1: PathLike
        path to shapefile 1
    vector2: PathLike
        path to shapefile 2
    driver_name: str
        gdal driver

    Returns
    -------
    bool:
        True if same file False if different
    """

    def is_equal(in1: Any, in2: Any) -> None:
        if in1 != in2:
            raise DifferenceError("Files are not identical")

    # Output of the function
    retour = False

    try:
        driver = ogr.GetDriverByName(driver_name)
        # Opening of files
        data1 = driver.Open(vector1, 0)
        data2 = driver.Open(vector2, 0)

        if data1 is None:
            raise Exception(f"Could not open {vector1}")
        if data2 is None:
            raise Exception(f"Could not open {vector2}")

        layer1 = data1.GetLayer()
        layer2 = data2.GetLayer()
        feature_count1 = layer1.GetFeatureCount()
        feature_count2 = layer2.GetFeatureCount()
        # check if number of element is equal
        is_equal(feature_count1, feature_count2)

        # check if type of geometry is same
        is_equal(layer1.GetGeomType(), layer2.GetGeomType())

        # check features
        for n_feature in range(feature_count1):
            feature1 = layer1.GetFeature(n_feature)
            feature2 = layer2.GetFeature(n_feature)

            geom1 = feature1.GetGeometryRef()
            geom2 = feature2.GetGeometryRef()
            # check if coordinates are equal
            is_equal(str(geom1), str(geom2))

        layer_definition1 = layer1.GetLayerDefn()
        layer_definition2 = layer2.GetLayerDefn()
        # check if number of fiels is equal
        is_equal(layer_definition1.GetFieldCount(), layer_definition2.GetFieldCount())

        # check fields for layer definition
        for n_feature in range(layer_definition1.GetFieldCount()):
            is_equal(
                layer_definition1.GetFieldDefn(n_feature).GetName(),
                layer_definition2.GetFieldDefn(n_feature).GetName(),
            )
            field_type_code = layer_definition1.GetFieldDefn(n_feature).GetType()
            is_equal(
                layer_definition1.GetFieldDefn(n_feature).GetFieldTypeName(
                    field_type_code
                ),
                layer_definition2.GetFieldDefn(n_feature).GetFieldTypeName(
                    field_type_code
                ),
            )
            is_equal(
                layer_definition1.GetFieldDefn(n_feature).GetWidth(),
                layer_definition2.GetFieldDefn(n_feature).GetWidth(),
            )
            is_equal(
                layer_definition1.GetFieldDefn(n_feature).GetPrecision(),
                layer_definition2.GetFieldDefn(n_feature).GetPrecision(),
            )

    except DifferenceError:
        # DifferenceError : retour set to false
        retour = False
    except Exception:
        # other error : retour set to false and raise
        retour = False
        raise
    else:
        # no error : files are identical retour set to true
        retour = True

    return retour


class DifferenceError(Exception):
    """Exception class to indicate an error in a difference"""

    def __init__(self, value: Any):
        self.value = value

    def __str__(self) -> str:
        return repr(self.value)


def get_field_value(feat: ogr.Feature, fields: list[str]) -> dict[str, Any]:
    """
    Get all field's values in input feature.

    Parameters
    ----------
    feat: ogr.Feature
        Feature to use as input
    fields: list[str]
        List of field names

    Returns
    -------
    dict:
        Dictionary mapping field names to values
    """
    return {currentField: feat.GetField(currentField) for currentField in fields}


def get_values_sorted_by_coordinates(
    vector: str,
    ignored_fields_list: list[str] | None = None,
    geometry_type: str = "point",
    driver_name: str = "SQLite",
) -> list[tuple[CoordXY, dict[str, Any]]]:
    """
    Extract values from a vector file and return them, sorted by their coordinates.

    Parameters
    ----------
    vector: str
        Path to the vector file
    ignored_fields_list: list[str] = None
        List of field to ignore
    geometry_type: str = "point"
        Type of geometry ("point" or "polygon")
    driver_name: str = "SQLite"
        Name of the OGR driver to use

    Returns
    -------
    values: list[tuple[CoordXY, dict[str, Any]]]
        A list of tuples, each containing:
        - The extracted coordinates as (x, y)
        - Dictionary mapping field names to corresponding values
        The list is sorted by coordinates (x, y).
    """

    def priority(item: tuple[CoordXY, dict[str, Any]]) -> CoordXY:
        return item[0]

    if ignored_fields_list is None:
        ignored_fields_list = []
    values = []

    driver = ogr.GetDriverByName(driver_name)
    data_src = driver.Open(vector, 0)
    layer = data_src.GetLayer()

    fields = [
        field
        for field in vf.get_all_fields_in_shape(vector, driver_name)
        if field not in ignored_fields_list
    ]

    for feature in layer:
        if geometry_type == "point":
            x_val = feature.GetGeometryRef().GetX()
            y_val = feature.GetGeometryRef().GetY()
        elif geometry_type == "polygon":
            x_val = feature.GetGeometryRef().Centroid().GetX()
            y_val = feature.GetGeometryRef().Centroid().GetY()
        else:
            raise ValueError(f"Cannot use the following geometry type: {geometry_type}")
        fields_val = get_field_value(feature, fields)
        values.append(((x_val, y_val), fields_val))
    values = sorted(values, key=priority)
    return values


def compare_vector_file(
    vect_1: str,
    vect_2: str,
    mode: str = "table",
    typegeom: str = "point",
    drivername: str = "SQLite",
) -> bool:
    """Use to compare two SQLite vector files.

    mode=='table' is faster but does not work with connected OTB applications.

    Parameters
    ----------
    vect_1 : string
        path to a vector file
    vect_2 : string
        path to a vector file
    mode : string
        'table' or 'coordinates'
        -> table : compare sqlite tables
        -> 'coordinates' : compare features geo-referenced at the same
                           coordinates
    typegeom : string
        'point' or 'polygon'
    drivername : string
        ogr driver's name

    Return
    ------
    bool
        True if vectors are the same
    """
    fields_1 = vf.get_all_fields_in_shape(vect_1, drivername)
    fields_2 = vf.get_all_fields_in_shape(vect_2, drivername)

    for field_1, field_2 in zip_longest(fields_1, fields_2, fillvalue=None):
        if not field_1 == field_2:
            return False

    if mode == "table":
        connection_1 = lite.connect(vect_1)
        df_1 = pad.read_sql_query("SELECT * FROM output", connection_1)

        connection_2 = lite.connect(vect_2)
        df_2 = pad.read_sql_query("SELECT * FROM output", connection_2)

        try:
            table = (df_1 != df_2).any(axis=1)
            if True in table.tolist():
                return False
            return True
        except ValueError:
            return False

    elif mode == "coordinates":
        values_1 = get_values_sorted_by_coordinates(
            vect_1, geometry_type=typegeom, driver_name=drivername
        )
        values_2 = get_values_sorted_by_coordinates(
            vect_2, geometry_type=typegeom, driver_name=drivername
        )
        same_feat = [val_1 == val_2 for val_1, val_2 in zip(values_1, values_2)]
        if False in same_feat:
            return False
        return True
    else:
        raise Exception("mode parameter must be 'table' or 'coordinates'")


def gen_geometries(
    origin: tuple[int, int], size: float, x_size: int, y_size: int, overlap: int
) -> list[list[ogr.Geometry]]:
    """
    Generate a grid of polygons
    Parameters
    ----------
    origin:
        Origin of the image
    size:
        Size of the polygons
    x_size:
        Number of columns
    y_size:
        Number of lines
    overlap:
        Overlap of the polygons
    """
    geom_grid = []
    for y in range(y_size):
        raw = []
        for x in range(x_size):
            if x == 0:
                min_x = float(origin[0])
            else:
                min_x = float((origin[0] + x * size * 1000) - 1000 * overlap * x)
            max_x = min_x + size * 1000
            if y == 0:
                min_y = float(origin[1])
            else:
                min_y = float((origin[1] + y * size * 1000) - 1000 * overlap * y)
            max_y = min_y + size * 1000

            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(min_x, min_y)
            ring.AddPoint(max_x, min_y)
            ring.AddPoint(max_x, max_y)
            ring.AddPoint(min_x, max_y)
            ring.AddPoint(min_x, min_y)

            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)

            raw.append(poly)
        geom_grid.append(raw)
    return geom_grid


def generate_tif(vector_file: str, pix_size: int) -> None:
    """Create a raster from a shapefile"""
    min_x, max_x, min_y, max_y = get_shape_extent(vector_file)
    cmd = (
        "gdal_rasterize -te "
        + str(min_x)
        + " "
        + str(min_y)
        + " "
        + str(max_x)
        + " "
        + str(max_y)
        + " -a Tile -tr "
        + str(pix_size)
        + " "
        + str(pix_size)
        + " "
        + vector_file
        + " "
        + vector_file.replace(".shp", ".tif")
    )
    run(cmd)


def gen_grid(
    output_directory: str,
    x_size: int = 10,
    y_size: int = 10,
    overlap: int = 10,
    size: int = 100,
    raster: bool = True,
    pix_size: int = 100,
) -> None:
    """
    Generate a grid of tiles
    Parameters
    ----------
    output_directory:
        Output directory
    x_size:
        Number of columns
    y_size:
        Number of lines
    overlap:
        Overlap of the polygons
    size:
        Size of the tiles
    raster:
        If True, also generate a raster file
    pix_size:
        Pixel size, if a raster is generated
    """
    origin = (500100, 6211230)  # lower left
    geom_grid = gen_geometries(origin, size, x_size, y_size, overlap)
    driver = ogr.GetDriverByName("ESRI Shapefile")
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(2154)
    tile = 1
    for raw in geom_grid:
        for col in raw:
            out_tile = output_directory + "/Tile" + str(tile) + ".shp"
            if Path(out_tile).exists():
                driver.DeleteDataSource(out_tile)
            data_source = driver.CreateDataSource(out_tile)
            layer_name = out_tile.split("/")[-1].split(".")[0]
            layer = data_source.CreateLayer(layer_name, srs, geom_type=ogr.wkbPolygon)
            field_tile = ogr.FieldDefn("Tile", ogr.OFTInteger)
            field_tile.SetWidth(5)
            layer.CreateField(field_tile)
            feature = ogr.Feature(layer.GetLayerDefn())
            feature.SetField("Tile", tile)
            feature.SetGeometry(col)
            layer.CreateFeature(feature)
            tile += 1
            feature = None
            data_source = None

            if raster:
                generate_tif(out_tile, pix_size)


def check_same_envelope(ev_ref: str, ev_test: str) -> bool:
    """
    usage get input extent and compare them. Return true if same, else false

    IN
    ev_ref [string] : path to a vector file
    ev_test [string] : path to a vector file

    OUT
    [bool]
    """
    min_x_ref, max_x_ref, min_y_ref, max_y_ref = get_shape_extent(ev_ref)
    min_x_test, max_x_test, min_y_test, max_y_test = get_shape_extent(ev_test)

    if (
        (min_x_ref == min_x_test)
        and (min_y_test == min_y_ref)
        and (max_x_ref == max_x_test)
        and (max_y_ref == max_y_test)
    ):
        return True
    return False


def random_update(
    vect_file: PathLike, table_name: str, field: str, value: str, nb_update: int
) -> None:
    """
    use in test_split_selection Test
    """
    sql_clause = (
        f"UPDATE {table_name} SET {field}='{value}' WHERE ogc_fid in "
        f"(SELECT ogc_fid FROM {table_name} ORDER BY RANDOM() LIMIT {nb_update})"
    )

    conn = lite.connect(vect_file)
    cursor = conn.cursor()
    cursor.execute(sql_clause)
    conn.commit()
