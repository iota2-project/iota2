#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Wrong builder"""

from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.sequence_builders.i2_sequence_builder import I2Builder


class I2WrongBuilder(I2Builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """

    def __init__(
        self,
        cfg: str,
        config_ressources: Path | None,
        hpc_working_directory: str = "TMPDIR",
    ):
        super().__init__(
            cfg,
            config_ressources,
            "localCluster",
            hpc_working_directory=hpc_working_directory,
        )
        # steps definitions
        self.steps_group: dict[str, dict] = {}
        self.steps_group["init"] = {}
        self.steps_group["writting"] = {}

        # build steps
        self.build_steps(self.cfg)
        self.sort_step()

        # pickle's path
        self.iota2_pickle = str(
            Path(rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path"))
            / "logs"
            / "iota2.txt"
        )

    @classmethod
    def parameters_validator(cls, i2_parameters):  # type: ignore  # fake function
        """Pass"""

    def get_dir(self) -> list[str]:
        """
        usage : return iota2_directories
        """
        directories = ["final", "features", "customF"]

        iota2_outputs_dir = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "output_path"
        )

        return [str(Path(iota2_outputs_dir) / d) for d in directories]
