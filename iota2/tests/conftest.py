#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Available fixtures through all iota2 tests directories."""

import shutil
from pathlib import Path, PurePath

import pytest
from _pytest.fixtures import SubRequest


@pytest.fixture(scope="function")
def i2_tmpdir(request: SubRequest) -> Path:
    """Make test temporary directory."""
    test_name = request.node.nodeid.split("::")[-1]
    i2_tests_tmp_path = Path(__file__).parent
    Path.mkdir(i2_tests_tmp_path, exist_ok=True)
    test_working_dir = Path(PurePath(i2_tests_tmp_path, test_name))
    if test_working_dir.exists():
        # Prevent that previous results interfere with current test
        shutil.rmtree(test_working_dir)
    Path.mkdir(test_working_dir, exist_ok=True)

    # Delete test directory when test is over
    def cleanup() -> None:
        # Only delete if test passed
        if request.node.rep_setup.passed and request.node.rep_call.passed:
            shutil.rmtree(test_working_dir)

    request.addfinalizer(cleanup)

    return test_working_dir


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item):  # type: ignore
    """Make test result information available in fixtures.

    from https://docs.pytest.org/en/latest/example/simple.html#making-test-result-information-available-in-fixtures. # noqa: E501, pylint: disable=line-too-long
    """
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"
    setattr(item, "rep_" + rep.when, rep)
