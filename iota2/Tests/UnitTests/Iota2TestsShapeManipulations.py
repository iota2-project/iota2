#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsShapeManipulations

import os
import sys
import shutil
import unittest
import iota2.Common.FileUtils as fu
import iota2.Tests.UnitTests.tests_utils.tests_utils_vectors as TUV
import iota2.VectorTools.vector_functions as vf
from iota2.Sampling import TileEnvelope
IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class Iota2TestShapeManipulations(unittest.TestCase):
    """
    
    """
    @classmethod
    def setUpClass(cls):
        cls.group_test_name = "Iota2TestsShapeManipulations"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        cls.reference_shape = os.path.join(IOTA2DIR, "data", "references",
                                           "D5H2_groundTruth_samples.shp")
        cls.nb_features = 28
        cls.fields = ['ID', 'LC', 'CODE', 'AREA_HA']
        cls.data_field = 'CODE'
        cls.epsg = 2154
        cls.type_shape = os.path.join(IOTA2DIR, "data", "references",
                                      "typo.shp")
        cls.region_field = "DN"

        cls.priority_envelope_ref = os.path.join(IOTA2DIR, "data",
                                                 "references", "priority_ref")
        cls.split_ratio = 0.5

        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    def test_CountFeatures(self):
        features = vf.getFieldElement(self.reference_shape,
                                      driverName="ESRI Shapefile",
                                      field="CODE",
                                      mode="all",
                                      elemType="int")
        self.assertTrue(len(features) == self.nb_features)

    def test_MultiPolygons(self):
        detect_multi = TUV.multiSearch(self.reference_shape)
        single = os.path.join(IOTA2DIR, "data", "test_MultiToSinglePoly.shp")
        vf.multiPolyToPoly(self.reference_shape, single)

        detect_no_multi = TUV.multiSearch(single)
        self.assertTrue(detect_multi)
        self.assertFalse(detect_no_multi)

        test_files = fu.fileSearchRegEx(
            os.path.join(IOTA2DIR, "data", "test_*"))

        for test_file in test_files:
            if os.path.isfile(test_file):
                os.remove(test_file)

    def test_getField(self):
        all_fields = vf.get_all_fields_in_shape(self.reference_shape,
                                                "ESRI Shapefile")
        self.assertTrue(self.fields == all_fields)

    def test_Envelope(self):

        self.priority_envelope_test = os.path.join(self.iota2_tests_directory,
                                                   "priority_test")
        if os.path.exists(self.priority_envelope_test):
            shutil.rmtree(self.priority_envelope_test)
        os.mkdir(self.priority_envelope_test)

        # Create a 3x3 grid (9 vectors shapes). Each tile are 110.010 km
        # with 10 km overlaping to fit L8 datas.
        TUV.genGrid(self.iota2_tests_directory,
                    X=3,
                    Y=3,
                    overlap=10,
                    size=110.010,
                    raster="True",
                    pixSize=30)

        tilesPath = fu.fileSearchRegEx(self.iota2_tests_directory + "/*.tif")

        ObjListTile = [
            TileEnvelope.Tile(
                currentTile,
                currentTile.split("/")[-1].split(".")[0].split("_")[0])
            for currentTile in tilesPath
        ]
        ObjListTile_sort = sorted(ObjListTile, key=TileEnvelope.priorityKey)

        TileEnvelope.genTileEnvPrio(ObjListTile_sort,
                                    self.priority_envelope_test,
                                    self.priority_envelope_test, self.epsg)

        envRef = fu.fileSearchRegEx(self.priority_envelope_ref + "/*.shp")

        comp = []
        for eRef in envRef:
            tile_number = os.path.split(eRef)[-1].split("_")[1]
            comp.append(
                fu.FileSearch_AND(self.priority_envelope_test, True,
                                  "Tile" + tile_number + "_PRIO.shp")[0])

        cmpEnv = [
            TUV.checkSameEnvelope(currentRef, test_env)
            for currentRef, test_env in zip(envRef, comp)
        ]
        self.assertTrue(all(cmpEnv))

    def test_regionsByTile(self):
        from iota2.Common.Tools import CreateRegionsByTiles as RT
        self.test_regions_by_tiles = os.path.join(self.iota2_tests_directory,
                                                  "test_regionsByTiles")
        if os.path.exists(self.test_regions_by_tiles):
            shutil.rmtree(self.test_regions_by_tiles)
        os.mkdir(self.test_regions_by_tiles)

        RT.createRegionsByTiles(self.type_shape, self.region_field,
                                self.priority_envelope_ref,
                                self.test_regions_by_tiles, None)
