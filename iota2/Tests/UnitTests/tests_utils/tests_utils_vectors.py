#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
This module offers some tools for writing tests
"""
import os
from typing import Optional, List, Tuple
from osgeo import ogr, osr
from iota2.VectorTools import vector_functions as vf
from iota2.Tests.UnitTests.tests_utils.tests_utils_rasters import rasterToArray


def rename_table(vect_file, old_table_name, new_table_name="output"):
    """
    use in test_split_selection Test
    """
    import sqlite3 as lite

    sql_clause = "ALTER TABLE {} RENAME TO {}".format(old_table_name,
                                                      new_table_name)

    conn = lite.connect(vect_file)
    cursor = conn.cursor()
    cursor.execute(sql_clause)
    conn.commit()


def compare_vector_raster(in_vec: str, vec_field: str, field_values: List[int],
                          in_img: str) -> List[int]:
    """return img values at vector positions where field is equal to a given values
    """
    import numpy as np
    from iota2.Common.OtbAppBank import CreateRasterizationApplication

    rasterization = CreateRasterizationApplication({
        "in":
        in_vec,
        "im":
        in_img,
        "mode":
        "attribute",
        "mode.attribute.field":
        vec_field
    })
    rasterization.Execute()

    vec_array = rasterization.GetImageAsNumpyArray("out")
    y_coords, x_coords = np.where(np.isin(vec_array, field_values))
    classif_array = rasterToArray(in_img)
    values = []
    for y_coord, x_coord in zip(y_coords, x_coords):
        values.append(classif_array[y_coord][x_coord])
    return values


def random_features_database_generator(output_db: str,
                                       features_fields: List[str],
                                       features_numbers: Optional[int] = 2
                                       ) -> None:
    """generate SQLite  ile with random integer values in appropriate fields
    """
    import random
    import osgeo.ogr as ogr
    import osgeo.osr as osr

    driver = ogr.GetDriverByName("SQLite")
    if os.path.exists(output_db):
        os.remove(output_db)

    data_source = driver.CreateDataSource(output_db)

    layer_name, _ = os.path.splitext(os.path.basename(output_db))
    layer = data_source.CreateLayer(layer_name, geom_type=ogr.wkbPoint)
    for field in features_fields:
        data_field_name = ogr.FieldDefn(field, ogr.OFTInteger)
        data_field_name.SetWidth(10)
        layer.CreateField(data_field_name)

    for _ in range(features_numbers):
        feature = ogr.Feature(layer.GetLayerDefn())
        for feature_field in features_fields:
            feature.SetField(feature_field, random.randrange(1, 100))
        layer.CreateFeature(feature)
    data_source = None


def random_ground_truth_generator(output_shape,
                                  data_field,
                                  number_of_class,
                                  region_field=None,
                                  min_cl_samples=10,
                                  max_cl_samples=100,
                                  epsg_code=2154,
                                  set_geom=True):
    """generate a shape file with random integer values in appropriate field

    Parameters
    ----------
    output_shape : string
        output shapeFile
    data_field : string
        data field
    number_of_class : int
        number of class
    region_field : string
        region field
    min_cl_samples : int
        minimum samples per class
    max_cl_samples : int
        maximum samples per class
    epsg_code : int
        epsg code
    set_geom : bool
        set a fake geometry
    """
    message = "max_cl_samples must be superior to min_cl_samples"
    assert max_cl_samples > min_cl_samples, message

    import random
    import osgeo.ogr as ogr
    import osgeo.osr as osr

    label_number = []
    for class_label in range(number_of_class):
        label_number.append(
            (class_label + 1, random.randrange(min_cl_samples,
                                               max_cl_samples)))

    driver = ogr.GetDriverByName("ESRI Shapefile")
    if os.path.exists(output_shape):
        driver.DeleteDataSource(output_shape)

    data_source = driver.CreateDataSource(output_shape)

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg_code)

    layer_name, _ = os.path.splitext(os.path.basename(output_shape))
    layer = data_source.CreateLayer(layer_name, srs, geom_type=ogr.wkbPolygon)
    data_field_name = ogr.FieldDefn(data_field, ogr.OFTInteger)
    data_field_name.SetWidth(10)
    layer.CreateField(data_field_name)
    if region_field:
        region_field_name = ogr.FieldDefn(region_field, ogr.OFTString)
        region_field_name.SetWidth(10)
        layer.CreateField(region_field_name)

    for class_label, features_num in label_number:
        for feat_cpt in range(features_num):
            feature = ogr.Feature(layer.GetLayerDefn())
            feature.SetField(data_field, class_label)
            if region_field:
                feature.SetField(region_field, "1")
            wkt = "POLYGON ((1 2, 2 2, 2 1, 1 1, 1 2))"
            point = ogr.CreateGeometryFromWkt(wkt)
            if set_geom:
                feature.SetGeometry(point)
            layer.CreateFeature(feature)
            feature = None
    data_source = None


def shape_reference_vector(ref_vector: str, output_name: str) -> str:
    """
    modify reference vector (add field, rename...)
    Parameters
    ----------
    ref_vector : string
    output_name : string
    Return
    ------
    string
    """
    import os
    from iota2.Common.Utils import run
    from iota2.Common import FileUtils as fut
    from iota2.VectorTools.AddField import addField
    path, _ = os.path.split(ref_vector)

    tmp = os.path.join(path, output_name + "_TMP")
    vf.cpShapeFile(ref_vector.replace(".shp", ""), tmp,
                   [".prj", ".shp", ".dbf", ".shx"])
    addField(tmp + ".shp", "region", "1", str)
    addField(tmp + ".shp", "seed_0", "learn", str)
    cmd = (f"ogr2ogr -dialect 'SQLite' -sql 'select GEOMETRY,seed_0, "
           f"region, CODE as code from {output_name}_TMP' "
           f"{path}/{output_name}.shp {tmp}.shp")
    run(cmd)

    os.remove(tmp + ".shp")
    os.remove(tmp + ".shx")
    os.remove(tmp + ".prj")
    os.remove(tmp + ".dbf")
    return path + "/" + output_name + ".shp"


def prepare_test_selection(vector: str, raster_ref: str, output_selection: str,
                           working_directory: str, data_field: str) -> None:
    """
    Prepare vector selection files
    Parameters
    ----------
    vector: string
    raster_ref: string
    output_selection: string
    working_directory: string
    data_field: string
    Return
    ------
    None
    """
    import os
    from iota2.Common import OtbAppBank as otb
    stats_path = os.path.join(working_directory, "stats.xml")
    if os.path.exists(stats_path):
        os.remove(stats_path)
    stats = otb.CreatePolygonClassStatisticsApplication({
        "in": raster_ref,
        "vec": vector,
        "field": data_field,
        "out": stats_path
    })
    stats.ExecuteAndWriteOutput()
    sample_sel = otb.CreateSampleSelectionApplication({
        "in": raster_ref,
        "vec": vector,
        "out": output_selection,
        "instats": stats_path,
        "sampler": "random",
        "strategy": "all",
        "field": data_field
    })
    if os.path.exists(output_selection):
        os.remove(output_selection)
    sample_sel.ExecuteAndWriteOutput()
    os.remove(stats_path)


def delete_useless_fields(test_vector: str,
                          field_to_rm: Optional[str] = "region") -> None:
    """
    Parameters
    ----------
    test_vector: string
    field_to_rm: string
    Return
    ------
    None
    """
    from iota2.Common import FileUtils as fut
    from iota2.VectorTools.DeleteField import deleteField
    # const

    fields = vf.get_all_fields_in_shape(test_vector, driver='SQLite')

    rm_field = [field for field in fields if field_to_rm in field]

    for field_to_remove in rm_field:
        deleteField(test_vector, field_to_remove)


def compare_sqlite(vect_1: str,
                   vect_2: str,
                   cmp_mode: Optional[str] = 'table',
                   ignored_fields: Optional[List[str]] = None):
    """
    compare SQLite, table mode is faster but does not work with
    connected OTB applications.

    return true if vectors are the same
    """

    from collections import OrderedDict
    from iota2.Common import FileUtils as fut
    if ignored_fields is None:
        ignored_fields = []

    def get_field_value(feat, fields):
        """
        usage : get all fields's values in input feature

        IN
        feat [gdal feature]
        fields [list of string] : all fields to inspect

        OUT
        [dict] : values by fields
        """
        return OrderedDict([(currentField, feat.GetField(currentField))
                            for currentField in fields])

    def priority(item):
        """
        priority key
        """
        return (item[0], item[1])

    def get_values_sorted_by_coordinates(vector: str
                                         ) -> List[Tuple[float, float]]:
        """
        usage return values sorted by coordinates (x,y)

        IN
        vector [string] path to a vector of points

        OUT
        values [list of tuple] : [(x,y,[val1,val2]),()...]
        """
        import ogr
        values = []
        driver = ogr.GetDriverByName("SQLite")
        dataset = driver.Open(vector, 0)
        lyr = dataset.GetLayer()
        fields = vf.get_all_fields_in_shape(vector, 'SQLite')
        for feature in lyr:
            x_coord = feature.GetGeometryRef().GetX()
            y_coord = feature.GetGeometryRef().GetY()
            fields_val = get_field_value(feature, fields)
            values.append((x_coord, y_coord, fields_val))

        values = sorted(values, key=priority)
        return values

    fields_1 = vf.get_all_fields_in_shape(vect_1, 'SQLite')
    fields_2 = vf.get_all_fields_in_shape(vect_2, 'SQLite')

    if len(fields_1) != len(fields_2):
        return False

    if cmp_mode == 'table':
        import sqlite3 as lite
        import pandas as pad
        connection_1 = lite.connect(vect_1)
        df_1 = pad.read_sql_query("SELECT * FROM output", connection_1)

        connection_2 = lite.connect(vect_2)
        df_2 = pad.read_sql_query("SELECT * FROM output", connection_2)

        try:
            table = (df_1 != df_2).any(1)
            out = True
            if True in table.tolist():
                out = False
            return out
        except ValueError:
            return False

    elif cmp_mode == 'coordinates':
        values_1 = get_values_sorted_by_coordinates(vect_1)
        values_2 = get_values_sorted_by_coordinates(vect_2)
        same_feat = []
        for val_1, val_2 in zip(values_1, values_2):
            for (key_1, v_1), (key_2, v_2) in zip(list(val_1[2].items()),
                                                  list(val_2[2].items())):
                if key_1 not in ignored_fields and key_2 in ignored_fields:
                    # same_feat.append(cmp(v_1, v_2) == 0)
                    same_feat.append(v_1 == v_2)
        if False in same_feat:
            return False
        return True
    else:
        raise Exception("CmpMode parameter must be 'table' or 'coordinates'")


def multiSearch(shp: str, ogrDriver: Optional[str] = 'ESRI Shapefile') -> bool:
    """
    Return true if shp contains one or more 'MULTIPOLYGON'
    Parameters
    ----------
    shp :
        path to a shapeFile
    ogrDriver : 
        ogr driver name

    """
    driver = ogr.GetDriverByName(ogrDriver)
    in_ds = driver.Open(shp, 0)
    in_lyr = in_ds.GetLayer()

    retour = False
    for in_feat in in_lyr:
        geom = in_feat.GetGeometryRef()
        if geom.GetGeometryName() == 'MULTIPOLYGON':
            retour = True
    return retour


class service_compare_vector_file:
    """
    The class serviceCompareShapeFile provides methods to compare
    two vector file
    """
    def testSameShapefiles(self, vector1, vector2, driver='ESRI Shapefile'):
        """
            IN :
                vector [string] : path to shapefile 1
                vector [string] : path to shapefile 2
                driver [string] : gdal driver
            OUT :
                retour [bool] : True if same file False if different
        """
        def isEqual(in1, in2):
            if in1 != in2:
                raise DifferenceError("Files are not identical")

        # Output of the function
        retour = False

        try:
            driver = ogr.GetDriverByName(driver)
            # Openning of files
            data1 = driver.Open(vector1, 0)
            data2 = driver.Open(vector2, 0)

            if data1 is None:
                raise Exception("Could not open " + vector1)
            if data2 is None:
                raise Exception("Could not open " + vector2)

            layer1 = data1.GetLayer()
            layer2 = data2.GetLayer()
            featureCount1 = layer1.GetFeatureCount()
            featureCount2 = layer2.GetFeatureCount()
            # check if number of element is equal
            isEqual(featureCount1, featureCount2)

            # check if type of geometry is same
            isEqual(layer1.GetGeomType(), layer2.GetGeomType())

            # check features
            for i in range(featureCount1):
                feature1 = layer1.GetFeature(i)
                feature2 = layer2.GetFeature(i)

                geom1 = feature1.GetGeometryRef()
                geom2 = feature2.GetGeometryRef()
                print(geom1)
                print(geom2)
                # check if coordinates are equal
                isEqual(str(geom1), str(geom2))

            layerDefinition1 = layer1.GetLayerDefn()
            layerDefinition2 = layer2.GetLayerDefn()
            # check if number of fiels is equal
            isEqual(layerDefinition1.GetFieldCount(),
                    layerDefinition2.GetFieldCount())

            # check fields for layer definition
            for i in range(layerDefinition1.GetFieldCount()):
                isEqual(
                    layerDefinition1.GetFieldDefn(i).GetName(),
                    layerDefinition2.GetFieldDefn(i).GetName())
                fieldTypeCode = layerDefinition1.GetFieldDefn(i).GetType()
                isEqual(
                    layerDefinition1.GetFieldDefn(i).GetFieldTypeName(
                        fieldTypeCode),
                    layerDefinition2.GetFieldDefn(i).GetFieldTypeName(
                        fieldTypeCode))
                isEqual(
                    layerDefinition1.GetFieldDefn(i).GetWidth(),
                    layerDefinition2.GetFieldDefn(i).GetWidth())
                isEqual(
                    layerDefinition1.GetFieldDefn(i).GetPrecision(),
                    layerDefinition2.GetFieldDefn(i).GetPrecision())

        # TODO Voir si ces tests sont suffisants.

        except DifferenceError:
            # DifferenceError : retour set to false
            retour = False
        except:
            # other error : retour set to false and raise
            retour = False
            raise
        else:
            # no error : files are identical retour set to true
            retour = True

        return retour


# Error class definition
class DifferenceError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def compareVectorFile(vect_1,
                      vect_2,
                      mode='table',
                      typegeom='point',
                      drivername="SQLite"):
    """used to compare two SQLite vector files

    mode=='table' is faster but does not work with connected OTB applications.

    Parameters
    ----------
    vect_1 : string
        path to a vector file
    vect_2 : string
        path to a vector file
    mode : string
        'table' or 'coordinates'
        -> table : compare sqlite tables
        -> 'coordinates' : compare features geo-referenced at the same
                           coordinates
    typegeom : string
        'point' or 'polygon'
    drivername : string
        ogr driver's name

    Return
    ------
    bool
        True if vectors are the same
    """
    import ogr
    from itertools import zip_longest
    import sqlite3 as lite
    import pandas as pad

    def getFieldValue(feat, fields):
        return dict([(currentField, feat.GetField(currentField))
                     for currentField in fields])

    def priority(item):
        return (item[0], item[1])

    def getValuesSortedByCoordinates(vector):
        values = []
        driver = ogr.GetDriverByName(drivername)
        ds = driver.Open(vector, 0)
        lyr = ds.GetLayer()
        fields = vf.get_all_fields_in_shape(vector, drivername)
        for feature in lyr:
            if typegeom == "point":
                x = feature.GetGeometryRef().GetX(),
                y = feature.GetGeometryRef().GetY()
            elif typegeom == "polygon":
                x = feature.GetGeometryRef().Centroid().GetX()
                y = feature.GetGeometryRef().Centroid().GetY()
            fields_val = getFieldValue(feature, fields)
            values.append((x, y, fields_val))

        values = sorted(values, key=priority)
        return values

    fields_1 = vf.get_all_fields_in_shape(vect_1, drivername)
    fields_2 = vf.get_all_fields_in_shape(vect_2, drivername)

    for field_1, field_2 in zip_longest(fields_1, fields_2, fillvalue=None):
        if not field_1 == field_2:
            return False

    if mode == 'table':
        connection_1 = lite.connect(vect_1)
        df_1 = pad.read_sql_query("SELECT * FROM output", connection_1)

        connection_2 = lite.connect(vect_2)
        df_2 = pad.read_sql_query("SELECT * FROM output", connection_2)

        try:
            table = (df_1 != df_2).any(1)
            if True in table.tolist():
                return False
            else:
                return True
        except ValueError:
            return False

    elif mode == 'coordinates':
        values_1 = getValuesSortedByCoordinates(vect_1)
        values_2 = getValuesSortedByCoordinates(vect_2)
        sameFeat = [val_1 == val_2 for val_1, val_2 in zip(values_1, values_2)]
        if False in sameFeat:
            return False
        return True
    else:
        raise Exception("mode parameter must be 'table' or 'coordinates'")


# #############################################################################
# Create data for test_envelope
# #############################################################################


def genGeometries(origin, size, X, Y, overlap):
    geom_grid = []
    for y in range(Y):
        raw = []
        for x in range(X):
            if x == 0: minX = origin[0]
            else:
                minX = float((origin[0] + x * size * 1000) -
                             1000 * overlap * x)
            maxX = minX + size * 1000
            if y == 0: minY = origin[1]
            else:
                minY = float((origin[1] + y * size * 1000) -
                             1000 * overlap * y)
            maxY = minY + size * 1000

            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(minX, minY)
            ring.AddPoint(maxX, minY)
            ring.AddPoint(maxX, maxY)
            ring.AddPoint(minX, maxY)
            ring.AddPoint(minX, minY)

            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)

            raw.append(poly)
        geom_grid.append(raw)
    return geom_grid


def generateTif(vectorFile, pixSize):

    minX, minY, maxX, maxY = vf.getShapeExtent(vectorFile)
    cmd = "gdal_rasterize -te " + str(minX) + " " + str(minY) + " " + str(
        maxX) + " " + str(maxY) + " -a Tile -tr " + str(pixSize) + " " + str(
            pixSize) + " " + vectorFile + " " + vectorFile.replace(
                ".shp", ".tif")
    print(cmd)
    os.system(cmd)


def genGrid(outputDirectory,
            X=10,
            Y=10,
            overlap=10,
            size=100,
            raster="True",
            pixSize=100):

    origin = (500100, 6211230)  # lower left
    geom_grid = genGeometries(origin, size, X, Y, overlap)
    driver = ogr.GetDriverByName("ESRI Shapefile")
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(2154)

    tile = 1
    for raw in geom_grid:
        for col in raw:
            outTile = outputDirectory + "/Tile" + str(tile) + ".shp"
            if os.path.exists(outTile): driver.DeleteDataSource(outTile)
            data_source = driver.CreateDataSource(outTile)
            layerName = outTile.split("/")[-1].split(".")[0]
            layer = data_source.CreateLayer(layerName,
                                            srs,
                                            geom_type=ogr.wkbPolygon)
            field_tile = ogr.FieldDefn("Tile", ogr.OFTInteger)
            field_tile.SetWidth(5)
            layer.CreateField(field_tile)
            feature = ogr.Feature(layer.GetLayerDefn())
            feature.SetField("Tile", tile)
            feature.SetGeometry(col)
            layer.CreateFeature(feature)
            tile += 1
            feature = None
            data_source = None

            if raster == "True":
                generateTif(outTile, pixSize)
                #if os.path.exists(outTile):
                #    driver.DeleteDataSource(outTile)


def checkSameEnvelope(EvRef, EvTest):
    """
    usage get input extent and compare them. Return true if same, else false

    IN
    EvRef [string] : path to a vector file
    EvTest [string] : path to a vector file

    OUT
    [bool]
    """
    miX_ref, miY_ref, maX_ref, maY_ref = vf.getShapeExtent(EvRef)
    miX_test, miY_test, maX_test, maY_test = vf.getShapeExtent(EvTest)

    if ((miX_ref == miX_test) and (miY_test == miY_ref)
            and (maX_ref == maX_test) and (maY_ref == maY_test)):
        return True
    return False


def random_update(vect_file, table_name, field, value, nb_update):
    """
    use in test_split_selection Test
    """
    import sqlite3 as lite

    sql_clause = "UPDATE {} SET {}='{}' WHERE ogc_fid in (SELECT ogc_fid FROM {} ORDER BY RANDOM() LIMIT {})".format(
        table_name, field, value, table_name, nb_update)

    conn = lite.connect(vect_file)
    cursor = conn.cursor()
    cursor.execute(sql_clause)
    conn.commit()
