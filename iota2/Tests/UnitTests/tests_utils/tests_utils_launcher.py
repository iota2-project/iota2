#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import shutil
import urllib.request
import tarfile
from subprocess import Popen, PIPE, CalledProcessError

from iota2.Common.FileUtils import md5
from iota2.Common.FileUtils import get_iota2_project_dir


def get_large_i2_data_test() -> None:
    """get large iota2 reference data test
    """
    html_archive = "http://osr-cesbio.ups-tlse.fr/oso/donneeswww_TheiaOSO/large_i2_data_test.tar.gz"
    html_checksum = "http://osr-cesbio.ups-tlse.fr/oso/donneeswww_TheiaOSO/large_i2_data_test_md5sum.txt"

    checksum = next(urllib.request.urlopen(html_checksum)).decode(
        "utf-8").rstrip().split(" ")[0]

    expected_huge_date_dir_storage = os.path.join(get_iota2_project_dir(),
                                                  "data", "references",
                                                  "running_iota2")
    if not os.path.exists(expected_huge_date_dir_storage):
        raise ValueError(f"the directory '{expected_huge_date_dir_storage}'"
                         " needed to store large iota2 test dataset doesn't"
                         " exists")
    large_i2_data_file = os.path.join(expected_huge_date_dir_storage,
                                      "large_i2_data_test.tar.gz")
    dl_try = 0
    max_try = 5
    success = False

    while dl_try < max_try:
        if not os.path.exists(large_i2_data_file):
            print("Download large iota2 data test in progress")
            with urllib.request.urlopen(html_archive) as response, open(
                    large_i2_data_file, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)
            print("Downloaded")
        file_checksum = md5(large_i2_data_file)
        if file_checksum == checksum:
            success = True
            large_i2_data_dir = os.path.join(expected_huge_date_dir_storage,
                                             "large_i2_data_test")
            shutil.unpack_archive(large_i2_data_file,
                                  expected_huge_date_dir_storage,
                                  format="gztar")
            break
    if not success:
        raise ValueError(
            f"Can't download iota2 data test archive : max try {max_try} reached "
        )


def run_cmd(cmd: str) -> int:
    """run cmd in a subprocess and raise and Exception if the command fails
    """
    # p = subprocess.Popen(cmd,
    #                      env=os.environ,
    #                      shell=True,
    #                      stdout=subprocess.PIPE,
    #                      stderr=subprocess.PIPE)
    # # Get output as strings
    # out, err = p.communicate()
    # if p.returncode != 0:
    #     raise Exception(err.decode("UTF-8"))

    with Popen(cmd,
               stdout=PIPE,
               stderr=PIPE,
               bufsize=1,
               universal_newlines=True,
               env=os.environ,
               shell=True) as p:
        for line in p.stdout:
            print(line, end='')  # process line here
        err = ""
        for line in p.stderr:
            err += line

    if p.returncode != 0:
        raise Exception(err)
