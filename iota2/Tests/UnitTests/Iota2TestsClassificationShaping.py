#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsClassificationShaping

import os
import sys
import shutil
import unittest
from iota2.configuration_files import read_config_file as rcf
import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class iota_test_classification_shaping(unittest.TestCase):
    """ Test the classification shaping function"""
    @classmethod
    def setUpClass(cls):
        # definition of local variables
        cls.group_test_name = "Iota2TestsClassificationShaping"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # definition of local variables
        cls.fichier_config = os.path.join(
            IOTA2DIR, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg")
        cls.path_tiles_feat = os.path.join(IOTA2DIR, "data", "references",
                                           "features")
        cls.ref_data = os.path.join(IOTA2DIR, "data", "references",
                                    "ClassificationShaping")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    # after launching all tests
    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, "_outcomeForDoCleanups",
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    def test_classification_shaping(self):
        from iota2.Validation import ClassificationShaping as CS

        # Prepare data
        path_classif = os.path.join(self.test_working_directory, "classif")
        classif_final = os.path.join(self.test_working_directory, "final")

        # test and creation of pathClassif
        if not os.path.exists(path_classif):
            os.mkdir(path_classif)
        if not os.path.exists(path_classif + "/MASK"):
            os.mkdir(path_classif + "/MASK")
        if not os.path.exists(path_classif + "/tmpClassif"):
            os.mkdir(path_classif + "/tmpClassif")
        # test and creation of classifFinal
        if not os.path.exists(classif_final):
            os.mkdir(classif_final)

        # copy input file
        src_files = os.listdir(self.ref_data + "/Input/Classif/MASK")
        for file_name in src_files:
            full_file_name = os.path.join(
                self.ref_data + "/Input/Classif/MASK", file_name)
            shutil.copy(full_file_name, path_classif + "/MASK")

        src_files = os.listdir(self.ref_data + "/Input/Classif/classif")
        for file_name in src_files:
            full_file_name = os.path.join(
                self.ref_data + "/Input/Classif/classif/", file_name)
            shutil.copy(full_file_name, path_classif)

        # run test

        rcf.clearConfig()
        cfg = rcf.read_config_file(self.fichier_config)
        features_ref = os.path.join(IOTA2DIR, "data", "references", "features")
        features_ref_test = os.path.join(self.test_working_directory,
                                         "features")
        os.mkdir(features_ref_test)
        shutil.copytree(features_ref + "/D0005H0002",
                        features_ref_test + "/D0005H0002")
        shutil.copytree(features_ref + "/D0005H0003",
                        features_ref_test + "/D0005H0003")

        nb_run = 1
        colortable = os.path.join(IOTA2DIR, "data", "references", "color.txt")
        nomenclature_path = os.path.join(IOTA2DIR, "data", "references",
                                         "nomenclature.txt")
        CS.classification_shaping(path_classif,
                                  nb_run,
                                  classif_final,
                                  None,
                                  "separate",
                                  self.test_working_directory,
                                  False,
                                  2154,
                                  nomenclature_path,
                                  False, [30, 30],
                                  False,
                                  cfg.getParam("chain", "region_path"),
                                  colortable,
                                  "code", ["D0005H0002", "D0005H0003"],
                                  labels_conversion={"12": 12})

        # file comparison to ref file
        service_compare_image_file = TUR.service_compare_image_file()
        src_files = os.listdir(self.ref_data + "/Output/")
        for file_name in src_files:
            file1 = os.path.join(classif_final, file_name)
            referencefile1 = os.path.join(self.ref_data + "/Output/",
                                          file_name)
            nbdiff = service_compare_image_file.gdalFileCompare(
                file1, referencefile1)
            self.assertEqual(nbdiff, 0)
