#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsVectorSamplesMerge

import os
import sys
import shutil
import unittest
from iota2.configuration_files import read_config_file as rcf
import iota2.Tests.UnitTests.tests_utils.tests_utils_vectors as TUV
from iota2.Common import FileUtils as fu
IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class Iota2TestsVectorSamplesMerge(unittest.TestCase):
    # Test ok
    @classmethod
    def setUpClass(cls):

        cls.group_test_name = "Iota2TestsVectorSamplesMerge"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # definition of local variables
        cls.fichier_config = os.path.join(
            IOTA2DIR, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg")

        cls.ref_data = os.path.join(IOTA2DIR, "data", "references",
                                    "VectorSamplesMerge")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    # after launching all tests
    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, "_outcomeForDoCleanups",
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    def test_VectorSamplesMerge(self):
        from iota2.Sampling import VectorSamplesMerge as VSM
        from iota2.Sampling import VectorSampler as vs

        # Prepare data
        learning_samples = os.path.join(self.test_working_directory,
                                        "learningSamples")
        cmd_path = os.path.join(self.test_working_directory, "cmd")

        # test and creation of learningSamples
        if not os.path.exists(learning_samples):
            os.mkdir(learning_samples)
        # test and creation of cmdpath
        if not os.path.exists(cmd_path):
            os.mkdir(cmd_path)

        # copy input data
        shutil.copy(
            self.ref_data +
            "/Input/D0005H0003_region_1_seed0_learn_Samples.sqlite",
            learning_samples)

        # Start test
        rcf.clearConfig()
        cfg = rcf.read_config_file(self.fichier_config)
        cfg.setParam('chain', 'output_path', self.test_working_directory)

        vector_list = fu.FileSearch_AND(learning_samples, True, ".sqlite")
        VSM.vector_samples_merge(vector_list, self.test_working_directory)

        # file comparison to ref file
        file1 = os.path.join(learning_samples,
                             "Samples_region_1_seed0_learn.sqlite")
        reference_file1 = os.path.join(self.ref_data, "Output",
                                       "Samples_region_1_seed0_learn.sqlite")
        self.assertTrue(
            TUV.compare_sqlite(file1,
                               reference_file1,
                               cmp_mode='coordinates',
                               ignored_fields=[]))
