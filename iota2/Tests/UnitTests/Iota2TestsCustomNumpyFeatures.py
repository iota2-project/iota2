#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsCustomNumpyFeatures

import os
import sys
import shutil
import unittest
import numpy as np

from iota2.Common.i2_constants import iota2_constants
from iota2.Common.FileUtils import ensure_dir
from iota2.Tests.UnitTests.tests_utils.tests_utils_launcher import get_large_i2_data_test

I2_CONST = iota2_constants()
IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class Iota2TestsCustomNumpyFeatures(unittest.TestCase):

    # before launching tests
    @classmethod
    def setUpClass(cls):
        # Download i2 large data set
        get_large_i2_data_test()

        # definition of local variables
        cls.group_test_name = "Iota2TestsCustomNumpyFeatures"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []
        cls.config_test = os.path.join(IOTA2DIR, "data", "numpy_features",
                                       "config_plugins.cfg")
        cls.sar_config_test = os.path.join(IOTA2DIR, "data", "references",
                                           "running_iota2",
                                           "large_i2_data_test", "s1_data",
                                           "i2_config_sar.cfg")
        cls.srtm_db = os.path.join(IOTA2DIR, "data", "references",
                                   "running_iota2", "large_i2_data_test",
                                   "s1_data", "srtm.shp")
        cls.tiles_grid_db = os.path.join(IOTA2DIR, "data", "references",
                                         "running_iota2", "large_i2_data_test",
                                         "s1_data", "Features.shp")
        cls.srtm_dir = os.path.join(IOTA2DIR, "data", "references",
                                    "running_iota2", "large_i2_data_test",
                                    "s1_data", "SRTM")
        cls.geoid_file = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "large_i2_data_test",
                                      "s1_data", "egm96.grd")
        cls.acquisitions = os.path.join(IOTA2DIR, "data", "references",
                                        "running_iota2", "large_i2_data_test",
                                        "s1_data", "acquisitions")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    # after launching all tests
    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, "_outcomeForDoCleanups",
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    # Tests definitions
    def test_check_custom_features_valid_inputs(self):
        """Test the behaviour of check custom features"""
        from config import Config
        from iota2.configuration_files import read_config_file as rcf
        s2st_data = self.test_working_directory
        test_path = os.path.join(self.test_working_directory, "RUN")

        # Valid test
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST_valid.cfg")

        shutil.copy(self.config_test, config_path_test)
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = test_path
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"

        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = s2st_data
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = "get_identity get_ndvi"
        cfg_test.save(open(config_path_test, "w"))
        cfg = rcf.read_config_file(config_path_test)
        self.assertTrue(
            cfg.getParam("external_features", "external_features_flag"))

    def test_check_invalid_module_name(self):
        """Test the behaviour of check custom features"""
        from config import Config
        from iota2.configuration_files import read_config_file as rcf
        s2st_data = self.test_working_directory
        test_path = os.path.join(self.test_working_directory, "RUN")

        # Invalid module name
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST_invalidmodule.cfg")

        shutil.copy(self.config_test, config_path_test)
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = test_path
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"
        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = s2st_data
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False
        cfg_test.external_features.module = os.path.join(
            IOTA2DIR, "data", "numpy_features", "dummy.py")

        cfg_test.external_features.functions = "get_identity get_ndvi"
        cfg_test.save(open(config_path_test, "w"))
        #cfg = rcf.read_config_file(config_path_test)
        self.assertRaises(ValueError, rcf.read_config_file, config_path_test)

    def test_check_invalid_function_name(self):
        """Test the behaviour of check custom features"""
        from config import Config
        from iota2.configuration_files import read_config_file as rcf
        s2st_data = self.test_working_directory
        test_path = os.path.join(self.test_working_directory, "RUN")

        # Invalid function name
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST_invalid_functions.cfg")

        shutil.copy(self.config_test, config_path_test)
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = test_path
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"
        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = s2st_data
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False
        cfg_test.external_features.module = os.path.join(
            IOTA2DIR, "data", "numpy_features", "user_custom_function.py")

        cfg_test.external_features.functions = "dummy_function"  # " get_ndvi"
        cfg_test.save(open(config_path_test, "w"))
        # cfg = rcf.read_config_file(config_path_test)
        self.assertRaises(AttributeError, rcf.read_config_file,
                          config_path_test)

    def test_apply_function_with_custom_features(self):
        """
        TEST : check the whole workflow
        """
        from functools import partial
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        from iota2.Common import rasterUtils as rasterU
        from iota2.Common.customNumpyFeatures import custom_numpy_features
        from iota2.configuration_files import read_config_file as rcf
        from iota2.Common import IOTA2Directory
        from config import Config
        from iota2.Sensors.Sensors_container import sensors_container
        from iota2.Common.GenerateFeatures import generate_features
        TUR.generate_fake_s2_data(
            self.test_working_directory,
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = self.test_working_directory
        testPath = os.path.join(self.test_working_directory, "RUN")
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = testPath
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"
        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = S2ST_data
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False
        #cfg_test.external_features.module = os.path.join(
        #    IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = "get_identity"  # " get_ndvi"
        cfg_test.save(open(config_path_test, "w"))
        cfg = rcf.read_config_file(config_path_test)

        IOTA2Directory.generate_directories(
            self.test_working_directory, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(
            config_path_test).get_sensors_parameters(tile_name)
        sensors = sensors_container(tile_name, working_dir,
                                    self.test_working_directory,
                                    **sensors_param)
        sensors.sensors_preprocess()
        list_sensors = sensors.get_enabled_sensors()
        sensor = list_sensors[0]
        ((time_s_app, app_dep), features_labels) = sensor.get_time_series()

        time_s_app.ExecuteAndWriteOutput()
        time_s = sensor.get_time_series_masks()
        time_s_app, app_dep, nbdates = time_s
        time_s_app.ExecuteAndWriteOutput()

        ((time_s_app, app_dep),
         features_labels) = sensor.get_time_series_gapfilling()
        # only one sensor for test
        # sensor_name, ((time_s_app, app_dep), features_labels) = time_s[0]
        # ( (time_s_app, app_dep), features_labels) = time_s
        apps_dict, labs_dict, ori_dep = generate_features(
            working_dir, "T31TCJ", False, self.test_working_directory,
            sensors_param)
        apps_dict["enable_interp"] = True
        apps_dict["enable_masks"] = False
        apps_dict["enable_raw"] = False
        # ori_features = apps_dict["interp"]
        # ori_feat_labels = labs_dict["interp"]
        # Then apply function
        # module_path = os.path.join(IOTA2DIR, "data", "numpy_features",
        #                            "user_custom_function.py")
        module_path = None
        # list_functions = ["get_identity", "get_ndvi", "duplicate_ndvi"]
        list_functions = [
            "get_cumulative_productivity", "get_seasonal_variation"
        ]
        cust = custom_numpy_features(
            tile_name, self.test_working_directory, sensors_param,
            cfg.getParam('external_features', "module"), list_functions)
        function_partial = partial(cust.process)

        labels_features_name = ["NDVI_20200101", "NDVI_20200102"]
        new_features_path = os.path.join(self.test_working_directory,
                                         "DUMMY_test.tif")
        (test_array, new_labels, _, _, _,
         _) = rasterU.insert_external_function_to_pipeline(
             otb_pipelines=apps_dict,
             labels=labels_features_name,
             working_dir=self.test_working_directory,
             function=function_partial,
             output_path=new_features_path,
             chunk_size_x=5,
             chunk_size_y=5,
             ram=128,
             is_custom_feature=True)

        self.assertTrue(os.path.exists(new_features_path))
        self.assertTrue(new_labels is not None)

    def test_compute_custom_features(self):
        """
        TEST : check the whole workflow
        """
        from functools import partial
        import numpy as np
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        from iota2.Common.customNumpyFeatures import custom_numpy_features
        from iota2.Common.customNumpyFeatures import compute_custom_features
        from iota2.configuration_files import read_config_file as rcf
        from iota2.Common import IOTA2Directory
        from config import Config
        from iota2.Sensors.Sensors_container import sensors_container
        from iota2.Common.GenerateFeatures import generate_features
        TUR.generate_fake_s2_data(
            self.test_working_directory,
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = self.test_working_directory
        testPath = os.path.join(self.test_working_directory, "RUN")
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = testPath
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"
        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = S2ST_data
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = "get_identity"  # " get_ndvi"
        cfg_test.save(open(config_path_test, "w"))
        cfg = rcf.read_config_file(config_path_test)

        IOTA2Directory.generate_directories(
            self.test_working_directory, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(
            config_path_test).get_sensors_parameters(tile_name)
        sensors = sensors_container(tile_name, working_dir,
                                    self.test_working_directory,
                                    **sensors_param)
        sensors.sensors_preprocess()
        list_sensors = sensors.get_enabled_sensors()
        sensor = list_sensors[0]
        ((time_s_app, app_dep), features_labels) = sensor.get_time_series()

        time_s_app.ExecuteAndWriteOutput()
        time_s = sensor.get_time_series_masks()
        time_s_app, app_dep, nbdates = time_s
        time_s_app.ExecuteAndWriteOutput()

        ((time_s_app, app_dep),
         features_labels) = sensor.get_time_series_gapfilling()
        # only one sensor for test
        # sensor_name, ((time_s_app, app_dep), features_labels) = time_s[0]
        # ( (time_s_app, app_dep), features_labels) = time_s
        apps_dict, labs_dict, ori_dep = generate_features(
            working_dir, "T31TCJ", False, self.test_working_directory,
            sensors_param)
        apps_dict["enable_interp"] = True
        apps_dict["enable_raw"] = False
        apps_dict["enable_masks"] = False
        # ori_features = apps_dict["interp"]
        # ori_feat_labels = labs_dict["interp"]
        # Then apply function
        # module_path = os.path.join(IOTA2DIR, "data", "numpy_features",
        #                            "user_custom_function.py")
        # list_functions = ["get_identity", "get_ndvi", "duplicate_ndvi"]
        list_functions = [
            "get_cumulative_productivity", "get_seasonal_variation"
        ]
        cust = custom_numpy_features(
            tile_name, self.test_working_directory, sensors_param,
            cfg.getParam("external_features", "module"), list_functions)
        function_partial = partial(cust.process)

        labels_features_name = ["NDVI_20200101", "NDVI_20200102"]
        new_features_path = os.path.join(self.test_working_directory,
                                         "DUMMY_test.tif")

        crop_image, new_labels, out_transform, _ = compute_custom_features(
            "T31TCJ",
            self.test_working_directory,
            sensors_param,
            cfg.getParam("external_features", "module"),
            list_functions,
            apps_dict, [],
            self.test_working_directory,
            "user_fixed",
            0,
            1,
            chunk_size_x=5,
            chunk_size_y=5,
            enabled_raw=False,
            enabled_gap=True,
            fill_missing_dates=False,
            all_dates_dict=None,
            mask_valid_data=None,
            mask_value=0)

        self.assertTrue(new_labels is not None)

    def test_manage_raw_data(self):
        """
        TEST : check the whole workflow
        """
        import numpy as np
        from functools import partial
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        from iota2.Common import rasterUtils as rasterU
        from iota2.Common.customNumpyFeatures import custom_numpy_features
        from iota2.configuration_files import read_config_file as rcf
        from iota2.Common import IOTA2Directory
        from config import Config
        from iota2.Sensors.Sensors_container import sensors_container
        from iota2.Common.GenerateFeatures import generate_features
        TUR.generate_fake_s2_data(
            self.test_working_directory,
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = self.test_working_directory
        testPath = os.path.join(self.test_working_directory, "RUN")
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = testPath
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"
        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = S2ST_data
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False
        cfg_test.external_features.module = os.path.join(
            IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = "get_identity"  # " get_ndvi"
        cfg_test.save(open(config_path_test, "w"))
        cfg = rcf.read_config_file(config_path_test)

        IOTA2Directory.generate_directories(
            self.test_working_directory, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(
            config_path_test).get_sensors_parameters(tile_name)
        sensors = sensors_container(tile_name, working_dir,
                                    self.test_working_directory,
                                    **sensors_param)
        sensors.sensors_preprocess()
        list_sensors = sensors.get_enabled_sensors()
        sensor = list_sensors[0]
        ((time_s_app, app_dep), features_labels) = sensor.get_time_series()

        time_s_app.ExecuteAndWriteOutput()
        time_s = sensor.get_time_series_masks()
        time_s_app, app_dep, nbdates = time_s
        time_s_app.ExecuteAndWriteOutput()

        ((time_s_app, app_dep),
         features_labels) = sensor.get_time_series_gapfilling()
        # only one sensor for test
        # sensor_name, ((time_s_app, app_dep), features_labels) = time_s[0]
        # ( (time_s_app, app_dep), features_labels) = time_s
        # apps_dict, labs_dict, ori_dep = generate_features(
        #     working_dir, "T31TCJ", False, self.test_working_directory,
        #     sensors_param)
        # ori_features = apps_dict["interp"]
        # ori_feat_labels = labs_dict["interp"]

        # Then apply function
        module_path = os.path.join(IOTA2DIR, "data", "numpy_features",
                                   "user_custom_function.py")
        list_functions = ["get_raw_data"]  #, "get_ndvi", "duplicate_ndvi"]
        # list_functions = [
        #     "get_cumulative_productivity", "get_seasonal_variation"
        # ]
        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101", "20200212", "20200512", "20200702", "20201002",
            "20201102"
        ]
        print("start init")
        cust = custom_numpy_features(tile_name,
                                     self.test_working_directory,
                                     sensors_param,
                                     module_path,
                                     list_functions,
                                     concat_mode=False,
                                     enabled_raw=True,
                                     enabled_gap=False,
                                     fill_missing_dates=True,
                                     all_dates_dict=all_dates_dict,
                                     working_dir=None)
        arr, labels = cust.process(np.ones((5, 4, 3)) * 6,
                                   binary_masks=np.ones((5, 4, 4)) * 5,
                                   raw_data=np.ones((5, 4, 40)))

        self.assertTrue(arr.shape == (5, 4, 66))

        self.assertTrue(labels == [
            'Sentinel2_B2_20200101', 'Sentinel2_B3_20200101',
            'Sentinel2_B4_20200101', 'Sentinel2_B5_20200101',
            'Sentinel2_B6_20200101', 'Sentinel2_B7_20200101',
            'Sentinel2_B8_20200101', 'Sentinel2_B8A_20200101',
            'Sentinel2_B11_20200101', 'Sentinel2_B12_20200101',
            'Sentinel2_B2_20200212', 'Sentinel2_B3_20200212',
            'Sentinel2_B4_20200212', 'Sentinel2_B5_20200212',
            'Sentinel2_B6_20200212', 'Sentinel2_B7_20200212',
            'Sentinel2_B8_20200212', 'Sentinel2_B8A_20200212',
            'Sentinel2_B11_20200212', 'Sentinel2_B12_20200212',
            'Sentinel2_B2_20200512', 'Sentinel2_B3_20200512',
            'Sentinel2_B4_20200512', 'Sentinel2_B5_20200512',
            'Sentinel2_B6_20200512', 'Sentinel2_B7_20200512',
            'Sentinel2_B8_20200512', 'Sentinel2_B8A_20200512',
            'Sentinel2_B11_20200512', 'Sentinel2_B12_20200512',
            'Sentinel2_B2_20200702', 'Sentinel2_B3_20200702',
            'Sentinel2_B4_20200702', 'Sentinel2_B5_20200702',
            'Sentinel2_B6_20200702', 'Sentinel2_B7_20200702',
            'Sentinel2_B8_20200702', 'Sentinel2_B8A_20200702',
            'Sentinel2_B11_20200702', 'Sentinel2_B12_20200702',
            'Sentinel2_B2_20201002', 'Sentinel2_B3_20201002',
            'Sentinel2_B4_20201002', 'Sentinel2_B5_20201002',
            'Sentinel2_B6_20201002', 'Sentinel2_B7_20201002',
            'Sentinel2_B8_20201002', 'Sentinel2_B8A_20201002',
            'Sentinel2_B11_20201002', 'Sentinel2_B12_20201002',
            'Sentinel2_B2_20201102', 'Sentinel2_B3_20201102',
            'Sentinel2_B4_20201102', 'Sentinel2_B5_20201102',
            'Sentinel2_B6_20201102', 'Sentinel2_B7_20201102',
            'Sentinel2_B8_20201102', 'Sentinel2_B8A_20201102',
            'Sentinel2_B11_20201102', 'Sentinel2_B12_20201102',
            'Sentinel2_MASK_20200101', 'Sentinel2_MASK_20200212',
            'Sentinel2_MASK_20200512', 'Sentinel2_MASK_20200702',
            'Sentinel2_MASK_20201002', 'Sentinel2_MASK_20201102'
        ])

    def test_pipeline_raise_raw_data(self):
        """
        TEST : check the whole workflow
        """
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        from iota2.Common.customNumpyFeatures import compute_custom_features
        from iota2.configuration_files import read_config_file as rcf
        from iota2.Common import IOTA2Directory
        from config import Config
        from iota2.Common.GenerateFeatures import generate_features

        TUR.generate_fake_s2_data(
            self.test_working_directory,
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = self.test_working_directory
        testPath = os.path.join(self.test_working_directory, "RUN")
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = testPath
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"
        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = S2ST_data
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False
        cfg_test.external_features.module = os.path.join(
            IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = "get_identity"  # " get_ndvi"
        cfg_test.save(open(config_path_test, "w"))
        cfg = rcf.read_config_file(config_path_test)

        IOTA2Directory.generate_directories(
            self.test_working_directory, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(
            config_path_test).get_sensors_parameters(tile_name)

        apps_dict, labs_dict, deps = generate_features(
            working_dir, "T31TCJ", False, self.test_working_directory,
            sensors_param)
        # Then apply function
        module_path = os.path.join(IOTA2DIR, "data", "numpy_features",
                                   "user_custom_function.py")
        list_functions = ["get_raw_data"]  #, "get_ndvi", "duplicate_ndvi"]

        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101", "20200212", "20200512", "20200702", "20201002"
        ]

        apps_dict["enable_raw"] = False
        apps_dict["enable_masks"] = False
        apps_dict["enable_interp"] = True
        self.assertRaises(AttributeError,
                          compute_custom_features,
                          tile=tile_name,
                          output_path=cfg.getParam("chain", "output_path"),
                          sensors_parameters=sensors_param,
                          module_path=module_path,
                          list_functions=list_functions,
                          otb_pipelines=apps_dict,
                          feat_labels=labs_dict["interp"],
                          path_wd=self.test_working_directory,
                          targeted_chunk=0,
                          number_of_chunks=1,
                          chunk_size_x=5,
                          chunk_size_y=5,
                          chunk_size_mode="split_number",
                          enabled_gap=True,
                          enabled_raw=False,
                          fill_missing_dates=False,
                          all_dates_dict=None,
                          concat_mode=False)

        # self.assertTrue(os.path.exists(new_features_path))
        # self.assertTrue(new_labels is not None)
        # self.assertTrue(False)

    def test_pipeline_use_exogeneous_data(self):
        """
        TEST : check the whole workflow
        """
        import numpy as np
        from functools import partial
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        from iota2.Common import rasterUtils as rasterU
        from iota2.Common.customNumpyFeatures import compute_custom_features
        from iota2.configuration_files import read_config_file as rcf
        from iota2.Common import IOTA2Directory
        from config import Config
        from iota2.Sensors.Sensors_container import sensors_container
        from iota2.Common.GenerateFeatures import generate_features

        TUR.generate_fake_s2_data(
            self.test_working_directory,
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = self.test_working_directory
        testPath = os.path.join(self.test_working_directory, "RUN")
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = testPath
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"
        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = S2ST_data
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False

        cfg_test.external_features.functions = "use_exogeneous_data get_ndvi"  # " get_ndvi"
        cfg_test.external_features.exogeneous_data = (os.path.join(
            IOTA2DIR, "data", "numpy_features", "fake_exogeneous_data.tif"))

        # multiband image
        # cfg_test.external_features.exogeneous_data = (os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "fake_exo_multi_band.tif"))

        cfg_test.save(open(config_path_test, "w"))
        cfg = rcf.read_config_file(config_path_test)

        IOTA2Directory.generate_directories(
            self.test_working_directory, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(
            config_path_test).get_sensors_parameters(tile_name)
        apps_dict, labs_dict, deps = generate_features(
            working_dir, "T31TCJ", False, self.test_working_directory,
            sensors_param)

        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101", "20200212", "20200512", "20200702", "20201002"
        ]

        apps_dict["enable_raw"] = True
        apps_dict["enable_masks"] = True
        apps_dict["enable_interp"] = True

        otbimage, feat_labels, out_transform, _ = compute_custom_features(
            tile=tile_name,
            output_path=cfg.getParam("chain", "output_path"),
            sensors_parameters=sensors_param,
            module_path=cfg.getParam("external_features", "module"),
            list_functions=cfg.getParam("external_features", 'functions'),
            otb_pipelines=apps_dict,
            feat_labels=labs_dict["interp"],
            path_wd=self.test_working_directory,
            targeted_chunk=0,
            number_of_chunks=1,
            chunk_size_x=5,
            chunk_size_y=5,
            chunk_size_mode="split_number",
            enabled_gap=True,
            enabled_raw=True,
            fill_missing_dates=True,
            all_dates_dict=all_dates_dict,
            exogeneous_data=cfg.getParam("external_features",
                                         "exogeneous_data"),
            concat_mode=False)

        self.assertTrue(len(feat_labels) == 56)
        self.assertTrue(otbimage["array"].shape == (16, 86, 56))

    def test_raw_data_s1_s2(self):
        """
        TEST : check the whole workflow
        """
        import numpy as np
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        from iota2.Common import rasterUtils as rasterU
        from iota2.Common.customNumpyFeatures import compute_custom_features
        from iota2.configuration_files import read_config_file as rcf
        from iota2.Common import IOTA2Directory
        from config import Config
        from iota2.Sensors.Sensors_container import sensors_container
        from iota2.Common.GenerateFeatures import generate_features

        # prepare data
        TUR.generate_fake_s2_data(
            self.test_working_directory,
            "T31TCJ",
            ["20200101", "20200512"],
        )
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST.cfg")
        config_path_test_s1 = os.path.join(self.test_working_directory,
                                           "Config_TEST_SAR.cfg")

        shutil.copy(self.config_test, config_path_test)

        S2_data = self.test_working_directory
        testPath = os.path.join(self.test_working_directory, "RUN")
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = testPath
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"
        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = S2_data
        cfg_test.chain.s1_path = config_path_test_s1
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.python_data_managing.chunk_size_mode = "split_number"

        cfg_test.save(open(config_path_test, "w"))
        cfg = rcf.read_config_file(config_path_test)

        import configparser
        s1_output_data = os.path.join(self.test_working_directory, "tilled_s1")
        ensure_dir(s1_output_data)
        config = configparser.ConfigParser()
        config.read(self.sar_config_test)
        paths = config["Paths"]
        paths["output"] = s1_output_data
        paths["srtm"] = self.srtm_dir
        paths["geoidfile"] = self.geoid_file
        paths["s1images"] = self.acquisitions
        processing = config["Processing"]

        processing["referencesfolder"] = S2_data
        processing["srtmshapefile"] = self.srtm_db
        processing["tilesshapefile"] = self.tiles_grid_db
        processing["TemporalResolution"] = "1"

        with open(config_path_test_s1, 'w') as configfile:
            config.write(configfile)

        IOTA2Directory.generate_directories(
            testPath, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(
            config_path_test).get_sensors_parameters(tile_name)
        apps_dict, labs_dict, deps = generate_features(
            working_dir, "T31TCJ", False, self.test_working_directory,
            sensors_param)

        # availables dates == all dates
        all_dates_dict = {
            "Sentinel2": ["20200101", "20200512"],
            'Sentinel1_DES_vv': ['20151231'],
            'Sentinel1_DES_vh': ['20151231'],
            'Sentinel1_ASC_vv': ['20170518', '20170519'],
            'Sentinel1_ASC_vh': ['20170518', '20170519']
        }

        apps_dict["enable_raw"] = True
        apps_dict["enable_masks"] = True
        apps_dict["enable_interp"] = True

        functions = ["get_raw_data"]
        module_path = cfg.getParam('external_features', "module")

        # launch function
        otbimage, feat_labels, out_transform, _ = compute_custom_features(
            tile=tile_name,
            output_path=cfg.getParam("chain", "output_path"),
            sensors_parameters=sensors_param,
            module_path=module_path,
            list_functions=functions,
            otb_pipelines=apps_dict,
            feat_labels=labs_dict["interp"],
            path_wd=self.test_working_directory,
            targeted_chunk=0,
            number_of_chunks=1,
            chunk_size_x=5,
            chunk_size_y=5,
            chunk_size_mode="split_number",
            enabled_gap=True,
            enabled_raw=True,
            fill_missing_dates=True,
            all_dates_dict=all_dates_dict,
            concat_mode=False)
        cust_arr = otbimage["array"]

        # asserts
        s2_nb_bands = 10
        s2_dates = 2
        s1_nb_bands = 6
        s1_nb_masks = 3  #(1 DES mask + 2 ASC masks)
        expected_shape = (16, 86, s2_nb_bands * s2_dates + s2_dates +
                          s1_nb_bands + s1_nb_masks)
        self.assertTrue(cust_arr.shape == expected_shape)
        data_bands = cust_arr[:, :, 0:26]
        masks_bands = cust_arr[:, :, 26:]
        raw_ts = apps_dict["raw"]
        raw_arr = raw_ts.GetVectorImageAsNumpyArray("out")
        self.assertTrue(np.allclose(raw_arr, data_bands))

        masks_ts = apps_dict["masks"]
        masks_arr = masks_ts.GetVectorImageAsNumpyArray("out")
        self.assertTrue(np.allclose(masks_arr, masks_bands))

        # prepare data
        # availables dates != all dates : 1 fake s2 and s1 bands required
        all_dates_dict = {
            "Sentinel2": ["20200101", "20200201", "20200512"],
            'Sentinel1_DES_vv': ["20151201", '20151231'],
            'Sentinel1_DES_vh': ["20151201", '20151231'],
            'Sentinel1_ASC_vv': ['20170518', '20170519'],
            'Sentinel1_ASC_vh': ['20170518', '20170519']
        }
        # launch function
        otbimage, feat_labels, out_transform, _ = compute_custom_features(
            tile=tile_name,
            output_path=cfg.getParam("chain", "output_path"),
            sensors_parameters=sensors_param,
            module_path=module_path,
            list_functions=functions,
            otb_pipelines=apps_dict,
            feat_labels=labs_dict["interp"],
            path_wd=self.test_working_directory,
            targeted_chunk=0,
            number_of_chunks=1,
            chunk_size_x=5,
            chunk_size_y=5,
            chunk_size_mode="split_number",
            enabled_gap=True,
            enabled_raw=True,
            fill_missing_dates=True,
            all_dates_dict=all_dates_dict,
            concat_mode=False)

        cust_arr = otbimage["array"]

        # asserts
        s2_nb_bands = 10
        s2_dates = 3
        s1_nb_bands = 8
        s1_nb_masks = 4  #(1 DES mask + 2 ASC masks)
        expected_shape = (16, 86, s2_nb_bands * s2_dates + s2_dates +
                          s1_nb_bands + s1_nb_masks)

        data_bands = cust_arr[:, :, 0:38]
        masks_bands = cust_arr[:, :, 38:]
        raw_ts = apps_dict["raw"]
        raw_arr = raw_ts.GetVectorImageAsNumpyArray("out")

        self.assertTrue(cust_arr.shape == expected_shape)
        fake_bands = np.ones((16, 86, 1)) * I2_CONST.i2_missing_dates_no_data
        # s1 data then s2 data
        bands_to_insert = [0, 1, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16]
        test_otbimage = np.insert(raw_arr,
                                  obj=bands_to_insert,
                                  values=fake_bands,
                                  axis=2)
        self.assertTrue(np.allclose(test_otbimage, data_bands),
                        msg="fake spectal bands are at the wrong position")

        masks_ts = apps_dict["masks"]
        masks_arr = masks_ts.GetVectorImageAsNumpyArray("out")
        fake_bands = np.ones(
            (16, 86, 1)) * I2_CONST.i2_missing_dates_no_data_mask
        # s1 data then s2 data
        bands_to_insert = [0, 4]
        test_mask = np.insert(masks_arr,
                              obj=bands_to_insert,
                              values=fake_bands,
                              axis=2)
        self.assertTrue(np.allclose(test_mask, masks_bands),
                        msg="fake mask bands are at the wrong position")

    def test_pipeline_use_raw_and_masks(self):
        """
        TEST : check the whole workflow
        """
        import numpy as np
        import rasterio as rio
        from functools import partial
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        from iota2.Common import rasterUtils as rasterU
        from iota2.Common.customNumpyFeatures import compute_custom_features
        from iota2.configuration_files import read_config_file as rcf
        from iota2.Common import IOTA2Directory
        from config import Config
        from iota2.Sensors.Sensors_container import sensors_container
        from iota2.Common.GenerateFeatures import generate_features

        TUR.generate_fake_s2_data(
            self.test_working_directory,
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = self.test_working_directory
        testPath = os.path.join(self.test_working_directory, "RUN")
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = testPath
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"
        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = S2ST_data
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False

        cfg_test.external_features.functions = "use_raw_and_masks_data"  # " get_ndvi"
        # cfg_test.external_features.exogeneous_data = (os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "fake_exogeneous_data.tif"))

        # multiband image
        # cfg_test.external_features.exogeneous_data = (os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "fake_exo_multi_band.tif"))

        cfg_test.save(open(config_path_test, "w"))
        cfg = rcf.read_config_file(config_path_test)

        IOTA2Directory.generate_directories(
            self.test_working_directory, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(
            config_path_test).get_sensors_parameters(tile_name)
        apps_dict, labs_dict, deps = generate_features(
            working_dir, "T31TCJ", False, self.test_working_directory,
            sensors_param)

        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101", "20200212", "20200512", "20200702", "20201002"
        ]

        apps_dict["enable_raw"] = True
        apps_dict["enable_masks"] = True
        apps_dict["enable_interp"] = True

        otbimage, feat_labels, out_transform, _ = compute_custom_features(
            tile=tile_name,
            output_path=cfg.getParam("chain", "output_path"),
            sensors_parameters=sensors_param,
            module_path=cfg.getParam("external_features", "module"),
            list_functions=cfg.getParam("external_features", 'functions'),
            otb_pipelines=apps_dict,
            feat_labels=labs_dict["interp"],
            path_wd=self.test_working_directory,
            targeted_chunk=0,
            number_of_chunks=1,
            chunk_size_x=5,
            chunk_size_y=5,
            chunk_size_mode="split_number",
            enabled_gap=True,
            enabled_raw=True,
            fill_missing_dates=True,
            all_dates_dict=all_dates_dict,
            exogeneous_data=None,
            concat_mode=False)

        unique, counts = np.unique(otbimage["array"], return_counts=True)
        dico = dict(zip(unique, counts))
        self.assertTrue(dico[-1000] == 2752)
        self.assertTrue(otbimage["array"].dtype == "int16")
        self.assertTrue(len(feat_labels) == 4)
        self.assertTrue(otbimage["array"].shape == (16, 86, 4))

    def test_interpolated_data_s1_s2(self):
        """
        """
        import numpy as np
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        from iota2.Common import rasterUtils as rasterU
        from iota2.Common.customNumpyFeatures import compute_custom_features
        from iota2.configuration_files import read_config_file as rcf
        from iota2.Common import IOTA2Directory
        from config import Config
        from iota2.Sensors.Sensors_container import sensors_container
        from iota2.Common.GenerateFeatures import generate_features

        # prepare data
        TUR.generate_fake_s2_data(
            self.test_working_directory,
            "T31TCJ",
            ["20200101", "20200111"],
        )
        config_path_test = os.path.join(self.test_working_directory,
                                        "Config_TEST.cfg")
        config_path_test_s1 = os.path.join(self.test_working_directory,
                                           "Config_TEST_SAR.cfg")

        shutil.copy(self.config_test, config_path_test)

        S2_data = self.test_working_directory
        testPath = os.path.join(self.test_working_directory, "RUN")
        cfg_test = Config(open(config_path_test))
        cfg_test.chain.output_path = testPath
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.l8_path_old = "None"
        cfg_test.chain.l8_path = "None"
        cfg_test.chain.check_inputs = False
        cfg_test.chain.s2_path = S2_data
        cfg_test.chain.s1_path = config_path_test_s1
        cfg_test.chain.user_feat_path = "None"
        cfg_test.chain.region_field = "region"
        cfg_test.arg_train.crop_mix = False
        cfg_test.arg_train.samples_classif_mix = False
        cfg_test.arg_train.annual_classes_extraction_source = None
        cfg_test.sensors_data_interpolation.use_additional_features = False
        cfg_test.sensors_data_interpolation.write_outputs = False
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.python_data_managing.chunk_size_mode = "split_number"

        cfg_test.save(open(config_path_test, "w"))
        cfg = rcf.read_config_file(config_path_test)

        import configparser
        s1_output_data = os.path.join(self.test_working_directory, "tilled_s1")
        ensure_dir(s1_output_data)
        config = configparser.ConfigParser()
        config.read(self.sar_config_test)
        paths = config["Paths"]
        paths["output"] = s1_output_data
        paths["srtm"] = self.srtm_dir
        paths["geoidfile"] = self.geoid_file
        paths["s1images"] = self.acquisitions
        processing = config["Processing"]

        processing["referencesfolder"] = S2_data
        processing["srtmshapefile"] = self.srtm_db
        processing["tilesshapefile"] = self.tiles_grid_db
        processing["TemporalResolution"] = "1"

        with open(config_path_test_s1, 'w') as configfile:
            config.write(configfile)

        IOTA2Directory.generate_directories(
            testPath, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(
            config_path_test).get_sensors_parameters(tile_name)
        apps_dict, labs_dict, deps = generate_features(
            working_dir, "T31TCJ", False, self.test_working_directory,
            sensors_param)
        apps_dict["enable_raw"] = True
        apps_dict["enable_masks"] = True
        apps_dict["enable_interp"] = True
        functions = ["get_interp_data"]
        module_path = __file__
        all_dates_dict = {
            "Sentinel2": ["20200101", "20200111"],
            'Sentinel1_DES_vv': ['20151231'],
            'Sentinel1_DES_vh': ['20151231'],
            'Sentinel1_ASC_vv': ['20170518', '20170519'],
            'Sentinel1_ASC_vh': ['20170518', '20170519']
        }
        # launch function
        otbimage, feat_labels, out_transform, _ = compute_custom_features(
            tile=tile_name,
            output_path=cfg.getParam("chain", "output_path"),
            sensors_parameters=sensors_param,
            module_path=module_path,
            list_functions=functions,
            otb_pipelines=apps_dict,
            feat_labels=labs_dict["interp"],
            path_wd=self.test_working_directory,
            targeted_chunk=0,
            number_of_chunks=1,
            chunk_size_x=5,
            chunk_size_y=5,
            chunk_size_mode="split_number",
            enabled_gap=True,
            enabled_raw=True,
            fill_missing_dates=True,
            all_dates_dict=all_dates_dict,
            concat_mode=False)
        test_interp_arr = otbimage["array"]
        ref_interp_app = apps_dict["interp"]
        ref_interp_app.Execute()
        ref_interp_arr = ref_interp_app.GetVectorImageAsNumpyArray("out")
        self.assertTrue(np.allclose(ref_interp_arr, test_interp_arr),
                        msg="interpolated data s1 + s2 fail")


def get_interp_data(self):
    """must be use only with test_interpolated_data_s1_s2
    due to re-shape
    """
    des_vv = self.get_interpolated_Sentinel1_DES_vv()
    des_vh = self.get_interpolated_Sentinel1_DES_vh()
    asc_vv = self.get_interpolated_Sentinel1_ASC_vv()
    asc_vh = self.get_interpolated_Sentinel1_ASC_vh()

    s2_b2 = self.get_interpolated_Sentinel2_B2()
    s2_b3 = self.get_interpolated_Sentinel2_B3()
    s2_b4 = self.get_interpolated_Sentinel2_B4()
    s2_b5 = self.get_interpolated_Sentinel2_B5()
    s2_b6 = self.get_interpolated_Sentinel2_B6()
    s2_b7 = self.get_interpolated_Sentinel2_B7()
    s2_b8 = self.get_interpolated_Sentinel2_B8()
    s2_b8a = self.get_interpolated_Sentinel2_B8A()
    s2_b11 = self.get_interpolated_Sentinel2_B11()
    s2_b12 = self.get_interpolated_Sentinel2_B12()
    s2_ndvi = self.get_interpolated_Sentinel2_NDVI()
    s2_ndwi = self.get_interpolated_Sentinel2_NDWI()
    s2_brightness = self.get_interpolated_Sentinel2_Brightness()

    interp_stack = np.concatenate(
        (des_vv, des_vh, asc_vv, asc_vh, np.expand_dims(
            s2_b2[:, :, 0], axis=2), np.expand_dims(s2_b3[:, :, 0], axis=2),
         np.expand_dims(s2_b4[:, :, 0],
                        axis=2), np.expand_dims(s2_b5[:, :, 0], axis=2),
         np.expand_dims(s2_b6[:, :, 0],
                        axis=2), np.expand_dims(s2_b7[:, :, 0], axis=2),
         np.expand_dims(s2_b8[:, :, 0],
                        axis=2), np.expand_dims(s2_b8a[:, :, 0], axis=2),
         np.expand_dims(s2_b11[:, :, 0],
                        axis=2), np.expand_dims(s2_b12[:, :, 0], axis=2),
         np.expand_dims(s2_b2[:, :, 1],
                        axis=2), np.expand_dims(s2_b3[:, :, 1], axis=2),
         np.expand_dims(s2_b4[:, :, 1],
                        axis=2), np.expand_dims(s2_b5[:, :, 1], axis=2),
         np.expand_dims(s2_b6[:, :, 1],
                        axis=2), np.expand_dims(s2_b7[:, :, 1], axis=2),
         np.expand_dims(s2_b8[:, :, 1],
                        axis=2), np.expand_dims(s2_b8a[:, :, 1], axis=2),
         np.expand_dims(s2_b11[:, :, 1],
                        axis=2), np.expand_dims(s2_b12[:, :, 1], axis=2),
         np.expand_dims(s2_ndvi[:, :, 0],
                        axis=2), np.expand_dims(s2_ndvi[:, :, 1], axis=2),
         np.expand_dims(s2_ndwi[:, :, 0],
                        axis=2), np.expand_dims(s2_ndwi[:, :, 1], axis=2),
         np.expand_dims(s2_brightness[:, :, 0], axis=2),
         np.expand_dims(s2_brightness[:, :, 1], axis=2)),
        axis=2)
    labels = []
    return interp_stack, labels
