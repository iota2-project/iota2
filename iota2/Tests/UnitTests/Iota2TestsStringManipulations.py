#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsStringManipulations

import os
import sys
import shutil
import unittest
import random
import string
import iota2.Common.FileUtils as fu
IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


def generate_random_string(size):
    """
    usage : generate a random string of 'size' character

    IN
    size [int] : size of output string

    OUT
    a random string of 'size' character
    """
    return ''.join(
        random.SystemRandom().choice(string.ascii_uppercase + string.digits +
                                     string.ascii_lowercase)
        for _ in range(size))


class Iota2TestsStringManipulations(unittest.TestCase):
    """
    Test iota2 string manipulations
    """
    @classmethod
    def setUpClass(cls):
        cls.group_test_name = "Iota2TestsStringManipulations"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []
        cls.AllL8Tiles = (
            "D00005H0010 D0002H0007 D0003H0006 D0004H0004 "
            "D0005H0002 D0005H0009 D0006H0006 D0007H0003 "
            "D0007H0010 D0008H0008 D0009H0007 D0010H0006 "
            "D0000H0001 D0002H0008 D0003H0007 D0004H0005 "
            "D0005H0003 D0005H0010 D0006H0007 D0007H0004 "
            "D0008H0002 D0008H0009 D0009H0008 D0010H0007 "
            "D0000H0002 D0003H0001 D0003H0008 D0004H0006 "
            "D0005H0004 D0006H0001 D0006H0008 D0007H0005 "
            "D0008H0003 D0009H0002 D0009H0009 D0010H0008 "
            "D00010H0005 D0003H0002 D0003H0009 D0004H0007 "
            "D0005H0005 D0006H0002 D0006H0009 D0007H0006 "
            "D0008H0004 D0009H0003 D0010H0002 D0001H0007 "
            "D0003H0003 D0004H0001 D0004H0008 D0005H0006 "
            "D0006H0003 D0006H0010 D0007H0007 D0008H0005 "
            "D0009H0004 D0010H0003 D0001H0008 D0003H0004 "
            "D0004H0002 D0004H0009 D0005H0007 D0006H0004 "
            "D0007H0001 D0007H0008 D0008H0006 D0009H0005 "
            "D0010H0004 D0002H0006 D0003H0005 D0004H0003 "
            "D0005H0001 D0005H0008 D0006H0005 D0007H0002 "
            "D0007H0009 D0008H0007 D0009H0006 D0010H0005".split())
        cls.AllS2Tiles = ("T30TVT T30TXP T30TYN T30TYT T30UWU T30UYA T31TCK "
                          "T31TDJ T31TEH T31TEN T31TFM T31TGL T31UCR T31UDS "
                          "T31UFP T32TLP T32TMN T32ULV T30TWP T30TXQ T30TYP "
                          "T30UUU T30UWV T30UYU T31TCL T31TDK T31TEJ T31TFH "
                          "T31TFN T31TGM T31UCS T31UEP T31UFQ T32TLQ T32TNL "
                          "T32UMU T30TWS T30TXR T30TYQ T30UVU T30UXA T30UYV "
                          "T31TCM T31TDL T31TEK T31TFJ T31TGH T31TGN T31UDP "
                          "T31UEQ T31UFR T32TLR T32TNM T32UMV T30TWT T30TXS "
                          "T30TYR T30UVV T30UXU T31TCH T31TCN T31TDM T31TEL "
                          "T31TFK T31TGJ T31UCP T31UDQ T31UER T31UGP T32TLT "
                          "T32TNN T30TXN T30TXT T30TYS T30UWA T30UXV T31TCJ "
                          "T31TDH T31TDN T31TEM T31TFL T31TGK T31UCQ T31UDR "
                          "T31UES T31UGQ T32TMM T32ULU".split())
        cls.dateFile = os.path.join(IOTA2DIR, "data", "references",
                                    "dates.txt")
        cls.fakeDateFile = os.path.join(IOTA2DIR, "data", "references",
                                        "fakedates.txt")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, "_outcomeForDoCleanups",
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    def test_getTile(self):
        """
        get tile name in random string
        """
        rstring_head = generate_random_string(100)
        rstring_tail = generate_random_string(100)

        S2 = True
        for current_tile in self.AllS2Tiles:
            try:
                fu.findCurrentTileInString(
                    rstring_head + current_tile + rstring_tail,
                    self.AllS2Tiles)
            except Exception:
                S2 = False
        self.assertTrue(S2)
        L8 = True
        for current_tile in self.AllL8Tiles:
            try:
                fu.findCurrentTileInString(
                    rstring_head + current_tile + rstring_tail,
                    self.AllL8Tiles)
            except Exception:
                L8 = False
        self.assertTrue(L8)

    def test_getDates(self):
        """
        get number of dates
        """
        try:
            nbDates = fu.getNbDateInTile(self.dateFile, display=False)
            self.assertTrue(nbDates == 35)
        except Exception:
            self.assertTrue(False)

        try:
            fu.getNbDateInTile(self.fakeDateFile, display=False)
            self.assertTrue(False)
        except:
            self.assertTrue(True)
