#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Vector operations used in segmentation"""
import logging
import warnings
from pathlib import Path
from typing import Any

import fiona
import geopandas as gpd
import numpy as np
import pandas as pd
import rtree
import xmltodict
from fiona.collection import Collection
from fiona.crs import from_epsg  # pylint: disable=no-name-in-module
from osgeo import ogr
from rtree import Index
from shapely.geometry import Polygon, mapping, shape

from iota2.common.service_error import DataBaseError
from iota2.common.utils import run
from iota2.learning.utils import I2LabelAlias
from iota2.typings.i2_types import PathLike
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def compute_intersection_with_regions(
    in_seg: PathLike,
    seg_field: str,
    out_seg: Path,
    region_path: PathLike,
    region_priority: list[str] | None,
    region_field: str,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Find the first intersection between a shapefile and a region shapefile.
    Create a new column in output with the region found.
    There is no clip in this function

    Parameters
    ----------

    in_seg:
        the shapefile to be intersected, the geometry of this file is kept
    seg_field:
        the column ID in first shapefile
    out_seg:
        the resulting shapefile with the shape of in_seg and a new column
    region_path:
        the region shapefile
    region_priority:
        all regions are looked in numerical order.
        If not None this priority list is used instead of.
    region_field:
        the region field in second shapefile.
        This name is also used to add a new column to output
    logger:
        Logger
    """
    df_seg = gpd.GeoDataFrame.from_file(in_seg)
    df_seg[seg_field] = range(1, len(df_seg.index) + 1)
    df_seg.to_file(out_seg)
    ds_seg = ogr.Open(str(out_seg), 1)
    print("Segmentation loaded")
    ds_reg = ogr.Open(region_path)
    layer_seg = ds_seg.GetLayer()
    layer_reg = ds_reg.GetLayer()
    new_field = ogr.FieldDefn(region_field, ogr.OFTString)
    if region_priority is None:
        region_priority = []
        for reg in layer_reg:
            region_priority.append(reg.GetField(region_field))
    region_geom = {}
    layer_reg.ResetReading()
    for reg in layer_reg:
        region_geom[reg.GetField(region_field)] = reg
    layer_seg.CreateField(new_field)
    total = layer_seg.GetFeatureCount()
    logger.info(f"Clone done, iterate over {total} features")
    for feat in layer_seg:
        found = False
        index = 0
        while index < len(region_priority) and not found:
            logger.debug(f"{index}/{len(region_priority)}")
            reg = region_geom[region_priority[index]]
            index += 1
            region = reg.GetField(region_field)
            logger.debug(f"REGION: {region}")
            if feat.GetGeometryRef().Intersects(reg.GetGeometryRef()):
                layer_seg.SetFeature(feat)
                feat.SetField(region_field, region)
                layer_seg.SetFeature(feat)
                found = True
            if not found:
                if index > len(region_priority):
                    logger.debug("No intersection found")


def segmentation_regions_intersection(
    iota2_directory: str,
    tile: str,
    seg_field: str,
    region_priority: list[str] | None,
    region_field: str = "region",
) -> None:
    """
    This function handle the case when only one region is used.
    If several regions are available, then it call
    :meth:compute_intersection_segmentation_regions_using_ogr

    Parameters
    ----------

    iota2_directory:
        the output iota2 directories root
    tile:
        the tile name
    region_field:
        the name of region field in region_path
    region_priority:
        list of all region ordered by priority
        by default the increase order is used.
        first region will have more polygons
    seg_field:
        a custom segmentation column name. Used to relabel each segment
        after cropping the initial segmentation.
    """
    in_seg = str(Path(iota2_directory) / "segmentation" / "tmp" / f"seg_{tile}.shp")
    out_seg_path = Path(iota2_directory) / "segmentation"
    out_seg = out_seg_path / f"seg_{tile}.shp"

    in_region = str(Path(iota2_directory, "segmentation", "tmp", f"regions_{tile}.shp"))
    gdf = gpd.GeoDataFrame().from_file(in_region)

    regions = gdf[region_field].unique()
    if len(regions) == 1:
        df_seg = gpd.GeoDataFrame().from_file(in_seg)
        df_seg[region_field] = 1
        df_seg[seg_field] = range(1, len(df_seg.index) + 1)
        df_seg.to_file(out_seg)
    else:
        compute_intersection_with_regions(
            in_seg, seg_field, out_seg, in_region, region_priority, region_field
        )


def generate_grid(bsize: int, df_in: gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    """
    Take a geodataframe and a buffer size and return a dataframe
    containing a regular grid corresponding to the extent

    Parameters
    ----------
    bsize:
        the buffer size in meters (buffer * resolution)
    df_in:
        a geodataframe opened
    """
    # create the buffer grid
    xmin, ymin, xmax, ymax = df_in.total_bounds
    cols = list(range(int(np.floor(xmin)), int(np.ceil(xmax)), bsize))
    rows = [np.ceil(ymax)]
    while rows[-1] - bsize > ymin:
        rows.append(rows[-1] - bsize)
    polygons = []
    for x in cols:
        for y in rows:
            polygons.append(
                Polygon(
                    [(x, y), (x + bsize, y), (x + bsize, y - bsize), (x, y - bsize)]
                )
            )

    grid = gpd.GeoDataFrame(
        {"geometry": polygons, "gridid": range(1, len(polygons) + 1)}
    )

    return grid


def segmentation_and_samples_intersection(
    iota2_directory: str,
    tile: str,
    seed: int,
    region_field: str,
    seg_field: str,
    spatial_resolution: list[float | int],
    full_segment: bool,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """Step designed function to compute intersection between the
    learning polygons and the segmentation in OBIA workflow

    Parameters
    ----------
    iota2_directory:
        iota2 output path
    tile:
        tile name
    seed:
        seed number
    region_field:
        region field name
    seg_field:
        segments field name
    spatial_resolution:
        spatial resolution as list with two elements
        ex: [10,-10] for S2
    full_segment:
        If True, the segments are not clipped to the learning shape
        If False only the intersection between learning polygon and segment are
        kept
    working_directory:
        working directory if different than iota2_directory

    """
    work_dir = str(Path(iota2_directory) / "segmentation" / "tmp")
    if working_directory:
        work_dir = working_directory
    # replace old format_sample_to_segmentation
    data_app_val_folder = str(Path(iota2_directory) / "dataAppVal")

    samples_file_sql = str(
        Path(data_app_val_folder, f"{tile}_seed_{seed}_learn.sqlite")
    )
    samples_file = str(Path(data_app_val_folder) / f"{tile}_seed_{seed}_learn.shp")
    # Convert to shape to simply use geopandas
    cmd = f"ogr2ogr -f 'ESRI Shapefile' {samples_file} {samples_file_sql}"
    logger.info(cmd)
    run(cmd, logger=logger)
    print("open segs")
    seg_folder = str(Path(iota2_directory) / "segmentation")
    segmentation_file = str(Path(seg_folder) / f"seg_{tile}.shp")
    out_name = str(
        Path(
            iota2_directory, "segmentation", f"learning_samples_{tile}_seed_{seed}.shp"
        )
    )
    find_intersections_between_samples_and_segments(
        samples_file,
        segmentation_file,
        work_dir,
        region_field,
        tile,
        seg_field,
        full_segment,
        spatial_resolution,
        out_name,
    )


def find_intersections_between_samples_and_segments(
    samples_file: str,
    segmentation_file: str,
    work_dir: str,
    region_field: str,
    tile: str,
    seg_field: str,
    full_segment: bool,
    spatial_resolution: list[float | int],
    out_name: str,
) -> None:
    """
    Function which compute the intersection between two shapefiles.

    Parameters
    ----------
    samples_file:
        the first shapefile
    segmentation_file:
        the second shapefile
    work_dir:
        the working directory
    tile:
        tile name
    seed:
        seed number
    region_field:
        region field name
    seg_field:
        segments field name
    spatial_resolution:
        spatial resolution as list with two elements
        ex: [10,-10] for S2
    full_segment:
        If True, the second shapefile geometry is kept
        If False only the intersection between learning polygon and segment are
       kept
    out_name:
        The output shapefile resulting of the intersection
        Only polygons which intersects are kept
    """
    # segmentation_file has a column named region
    # df_seg = gpd.GeoDataFrame.from_file(segmentation_file)
    df_samples = gpd.GeoDataFrame.from_file(samples_file)
    # remove region field as it not corresponding to OBIA spec
    df_samples = df_samples.drop(region_field, axis="columns")
    samples_without_region = str(Path(work_dir, f"{tile}_samples_without_region.shp"))
    df_samples.to_file(samples_without_region)
    intersection = str(Path(work_dir) / f"intersect_{tile}_learning_samples.shp")
    epsg = int(df_samples.crs.to_epsg())
    # find all intersections between the two shapes
    intersect_keep_duplicates(
        data_base1=segmentation_file,
        data_base2=samples_without_region,
        output=intersection,
        epsg=epsg,
        fields_to_keep=[seg_field, "region", "i2label"],
    )
    intersections = gpd.GeoDataFrame.from_file(intersection)
    # If duplicates on seg_field that means at least two learning
    # polygons intersect the same segment. Then removes them.
    intersections.drop_duplicates(seg_field, keep=False, inplace=True)
    # Use overlay to cut learning samples
    if full_segment:
        intersections["idlearn"] = range(1, len(intersections.index) + 1)
        intersections.to_file(out_name)
    else:
        print("start intersection")
        # remove lazy columns
        for col in ["originfid", "i2label", "index_right", "grid_id"]:
            if col in intersections.columns:
                intersections = intersections.drop(col, axis="columns")
        overlay = gpd.overlay(intersections, df_samples, how="intersection")
        # geopandas considers that multipolygon and polygon are the same
        overlay = overlay.explode()
        # remove to small polygons ?
        overlay["area"] = overlay.area
        # arbitrary choice of minimum resolution
        # observed from issue when run with polygons < 200m2
        overlay = overlay[
            overlay["area"]
            > 2 * abs(spatial_resolution[0]) * abs(spatial_resolution[1])
        ]
        if not overlay.empty:
            overlay["idlearn"] = range(1, len(overlay.index) + 1)
            overlay.to_file(out_name)


def intersect_keep_duplicates(
    data_base1: str,
    data_base2: str,
    output: str,
    epsg: int,
    fields_to_keep: list[str],
    data_base1_fid_field_name: str | None = None,
    data_base2_fid_field_name: str | None = None,
) -> bool:
    """Perform vector intersection using fiona. If there is intersection return True, else False.

    Parameters
    ----------
    data_base1 :
        input vector path
    data_base2 :
        input vector path
    output:
        output vector path
    epsg:
        epsg code
    fields_to_keep:
        list of fields to keep
    data_base1_fid_field_name:
        add FID column to first input database
    data_base2_fid_field_name
        add FID column to second input database
    """
    output_suffix = Path(output).suffix
    if output_suffix == ".sqlite":
        output = str(Path(output).with_suffix(".shp"))

    db1_as_shape = convert_sqlite_to_shapefile(data_base1)
    db2_as_shape = convert_sqlite_to_shapefile(data_base2)

    with fiona.open(db1_as_shape, "r") as layer1:
        with fiona.open(db2_as_shape, "r") as layer2:
            new_schema = filter_fields(
                layer1, layer2, fields_to_keep, db1_as_shape, db2_as_shape
            )

            if data_base1_fid_field_name:
                new_schema["properties"][data_base1_fid_field_name] = "int:9"
            if data_base2_fid_field_name:
                new_schema["properties"][data_base2_fid_field_name] = "int:9"

            write_intersection_to_file(
                output,
                layer1,
                layer2,
                fields_to_keep,
                epsg,
                new_schema,
                data_base1_fid_field_name,
                data_base2_fid_field_name,
            )

    output_field = vf.get_fields(output)[0]
    features = vf.get_field_element(output, elem_type=str, field=output_field)

    if output_suffix == ".sqlite":
        expected_out = Path(output).with_suffix(".sqlite")
        run(f"ogr2ogr -f 'SQLite' {expected_out} {output}")
        vf.remove_shape(output)
    if Path(data_base1).suffix == ".sqlite":
        vf.remove_shape(data_base1)
    if Path(data_base2).suffix == ".sqlite":
        vf.remove_shape(data_base2)
    return bool(features)


def filter_fields(
    layer1: Collection,
    layer2: Collection,
    fields_to_keep: list[str],
    db1_as_shape: str,
    db2_as_shape: str,
) -> dict:
    """
    Filter the fields to keep and creates a new schema based on the selected fields.

    Parameters
    ----------
    layer1 : Collection
        First layer (database 1).
    layer2 : Collection
        Second layer (database 2).
    fields_to_keep : list[str]
        List of fields to keep in the output file.
    db1_as_shape : str
        Path to the shapefile corresponding to the first database.
    db2_as_shape : str
        Path to the shapefile corresponding to the second database.

    Returns
    -------
    new_schema : dict
        The new schema with the filtered fields.
    """
    new_schema: dict = layer2.schema.copy()
    if fields_to_keep:
        properties = {}
        for field in fields_to_keep:
            if field in layer1.schema["properties"]:
                properties[field] = layer1.schema["properties"][field]
            elif field in layer2.schema["properties"]:
                properties[field] = layer2.schema["properties"][field]
            else:
                warnings.warn(
                    f"Can't find field '{field}' in input databases :"
                    f" {vf.get_fields(db1_as_shape)} and {vf.get_fields(db2_as_shape)}"
                )
        new_schema = {"properties": properties, "geometry": "Polygon"}
    else:
        for field_name, field_value in layer1.schema["properties"].items():
            new_schema["properties"][field_name] = field_value
    return new_schema


def write_intersection_to_file(
    output: str,
    layer1: Collection,
    layer2: Collection,
    field_to_keep: list[str],
    epsg: int,
    new_schema: dict[str, str | dict],
    data_base1_fid_field_name: str | None,
    data_base2_fid_field_name: str | None,
) -> None:
    """
    Iterate over all features, check for intersection and write them in an output file

    Parameters
    ----------
    output : str
        Path to the output shapefile
    layer1 : Collection
        First layer to scan
    layer2 : Collection
        Second layer to scan
    field_to_keep : list[str]
        List of fields to keep
    epsg : int
        Projection code
    new_schema : dict[str, str | dict]
        Schema for writing the output shapefile
    data_base1_fid_field_name :
        Field name of the first shapefile
    data_base2_fid_field_name
        Field name of the second shapefile

    Returns
    -------
    None
    """
    crs = from_epsg(int(epsg))
    with fiona.open(
        output, "w", crs=crs, driver="ESRI Shapefile", schema=new_schema
    ) as layer_out:
        index = rtree.index.Index()
        for feat1 in layer1:
            fid = int(feat1["id"])
            geom1 = shape(feat1["geometry"])
            index.insert(fid, geom1.bounds)
        for feat2 in layer2:
            geom2 = shape(feat2["geometry"])
            for fid in list(index.intersection(geom2.bounds)):
                feat1 = layer1[fid]
                process_feature_pair(
                    feat1,
                    feat2,
                    field_to_keep,
                    data_base1_fid_field_name,
                    data_base2_fid_field_name,
                    layer_out,
                )


def convert_sqlite_to_shapefile(database: PathLike) -> str:
    """
    Check the type of the database and depending on its type:
        - .sqlite: convert to a shapefile (if a shapefile with the same path doesn't already exist)
        - .shp: return the file
        - other: raise an OSError

    Parameters
    ----------
    database: PathLike
        Path to the database

    Returns
    -------
    output_database: str
        Path to the converted database
    """
    if Path(database).suffix == ".sqlite":
        db_as_shapefile = Path(database).with_suffix(".shp")
        if db_as_shapefile.exists():
            raise FileExistsError(f"{db_as_shapefile} already exits, please remove it")
        run(f"ogr2ogr {db_as_shapefile} {database}")
        output_database = str(db_as_shapefile)
    elif Path(database).suffix == ".shp":
        output_database = str(database)
    else:
        raise DataBaseError("The file extension is not valid")

    return output_database


def write_intersection_to_layer(
    index: Index,
    keepfields: list[str],
    layer1: fiona.Collection,
    layer2: fiona.Collection,
    layer_out: fiona.Collection,
    data_base1_fid_field_name: str | None,
    data_base2_fid_field_name: str | None,
) -> None:
    """
    Write intersections to the output layer.

    Parameters
    ----------
    index : rtree.index.Index
        Spatial index for quick lookup of feature bounds.
    keepfields : list of str
        List of field names to keep.
    layer1 : fiona.Collection
        The first input layer collection.
    layer2 : fiona.Collection
        The second input layer collection.
    layer_out : fiona.Collection
        The output layer collection.
    data_base1_fid_field_name : str, optional
        Field name for the FID of the first database.
    data_base2_fid_field_name : str, optional
        Field name for the FID of the second database.
    """
    for feat2 in layer2:
        geom2 = shape(feat2["geometry"])
        for fid in list(index.intersection(geom2.bounds)):
            feat1 = layer1[fid]
            process_feature_pair(
                feat1,
                feat2,
                keepfields,
                data_base1_fid_field_name,
                data_base2_fid_field_name,
                layer_out,
            )


def do_intersect_with_grid(
    in_seg: str,
    seg_field: str,
    tile: str,
    prefix: str,
    work_dir: str,
    buffer_size: int,
    spatial_res: int,
    out_folder: str,
    region_field: str,
    keepfields: list[str],
) -> dict[str, list[str]]:
    """
    Divides a shapefile into a grid based on the specified buffer size (in pixels) and spatial
    resolution. Geometries are not split: each geometry is assigned to the first grid cell it
    intersects. The function generates one shapefile for each grid cell.

    Parameters
    ----------
    in_seg:
        the input shapefile
    seg_field:
        the polygons ID field
    tile:
        tile name
    prefix:
        empty string or not. To add to output file name before the extension
    work_dir:
        the working directory
    buffer_size:
        buffer size in pixel unit
    spatial_res:
        spatial resolution in image resolution unit (meter for example)
    out_folder:
        output directory
    region_field:
        region column name
    keepfields:
        list of fields to retrieve in the resulting shapefiles

    Returns
    -------
    dict_reg_parts : dict[str, list[str]]
        Dictionary mapping region names to produced shapefiles
    """
    inter_seg = str(Path(work_dir) / f"grid_intersect_seg_{tile}{prefix}.shp")
    if not Path(inter_seg).exists():
        divide_shapefile_by_grid(
            buffer_size * spatial_res, in_seg, inter_seg, keepfields, out_folder, tile
        )
    # Split the intersections
    df_inter = gpd.GeoDataFrame().from_file(inter_seg)
    df_inter.drop_duplicates(seg_field, keep="first", inplace=True)
    regions = df_inter[region_field].unique()
    dict_reg_parts: dict[str, list[str]] = {}
    for region in regions:
        dict_reg_parts[region] = []
        df_split = df_inter[df_inter[region_field] == region]
        grid_sub = df_split["gridid"].unique()
        for grid_id in grid_sub:
            grid_file = str(
                Path(
                    out_folder, f"seg_{tile}_region_{region}_grid_{grid_id}{prefix}.shp"
                )
            )
            dict_reg_parts[region].append(grid_file)
            if not Path(grid_file).exists():
                df_write = df_split[df_split["gridid"] == grid_id]
                df_write.to_file(grid_file)

    return dict_reg_parts


def divide_shapefile_by_grid(
    buffer_size: int,
    in_seg: str,
    inter_seg: str,
    fields_to_keep: list[str],
    out_folder: str,
    tile: str,
) -> None:
    """
    Compute a grid based on the buffer size and spatial resolution and divide a shapefile according
    to this grid.
    The geometries are assigned to the first grid cell they intersect, without being split.

    Parameters
    ----------
    buffer_size : int
        Size of the grid buffer (grid cell size)
    in_seg : str
        Path to the input shapefile.
    inter_seg : str
        Path to save the shapefile with grid intersections.
    fields_to_keep : list[str]
        Fields to retain in the resulting shapefiles.
    out_folder : str
        Directory to save the output shapefiles.
    tile : str
        Name of the tile to include in output filenames.

    Returns
    -------
    None
    """
    # Compute grid
    df_seg = gpd.GeoDataFrame.from_file(in_seg)
    epsg = df_seg.crs.to_epsg()
    grid = generate_grid(buffer_size, df_seg)
    grid_tile = str(Path(out_folder) / f"seg_{tile}_grid.shp")
    grid.to_file(grid_tile)
    # Compute intersection between segmentation and grid
    intersect_keep_duplicates(
        data_base1=in_seg,
        data_base2=grid_tile,
        output=inter_seg,
        epsg=epsg,
        fields_to_keep=fields_to_keep,
    )


def extract_properties(
    feat1: dict[str, Any],
    feat2: dict[str, Any],
    keepfields: list[str],
    data_base1_fid_field_name: str | None,
    data_base2_fid_field_name: str | None,
) -> dict[str, Any]:
    """
    Extract properties from features based on keepfields and FID field names.

    Parameters
    ----------
    feat1 : dict
        The first feature dictionary.
    feat2 : dict
        The second feature dictionary.
    keepfields : list of str
        List of field names to keep.
    data_base1_fid_field_name : str, optional
        Field name for the FID of the first database.
    data_base2_fid_field_name : str, optional
        Field name for the FID of the second database.

    Returns
    -------
    dict
        A dictionary of properties.
    """
    prop = {}
    if keepfields:
        for field in keepfields:
            try:
                if field in feat1["properties"]:
                    prop[field] = feat1["properties"][field]
                else:
                    prop[field] = feat2["properties"][field]
            except KeyError:
                pass
    else:
        prop.update(feat1["properties"])
        prop.update(feat2["properties"])

    if data_base1_fid_field_name:
        prop[data_base1_fid_field_name] = feat1["id"]
    if data_base2_fid_field_name:
        prop[data_base2_fid_field_name] = feat2["properties"]["tile_grid_"]

    return prop


def convert_xml_to_shape(
    out_directory: str,
    xml_files: list[str],
    labels: list[list[I2LabelAlias]],
    shape_file: str,
    seed: int,
    merge_field: str,
    stat_used: list[str],
) -> tuple[list[str], str]:
    """
    This function associates statistics produced by OTB in xml to the shapefile containing the
    geometry. The result is stored as shapefile as required for the VectorClassifier.

    Parameters
    ----------

    out_directory:
        the path to output directory created by iota2
    xml_files:
        the sensor ordered list of statistics files
    labels:
        a list of list of string labels generated by sensors_container
    shape_file:
        the file containing the geometry corresponding to the statistics
    merge_field:
        the merge along which the statistics and geometry will be merged
    stat_used:
        a list of which statistics are kept for classification
        Allowed values are: mean, max, min, std, count
    """
    allowed_stats = ["mean", "max", "min", "std", "count"]
    stat_used = [stat for stat in stat_used if stat in allowed_stats]
    # Init the final dataframe to None
    df_full = pd.DataFrame()
    final_labels: list[str] = []
    # Parse the xml file using external lib xmltodict
    # Fill a pandas dataframe with the stats values
    for id_sens, (xml_file, label) in enumerate(zip(xml_files, labels)):
        df_full, final_labels = compute_stats_dataframe(
            xml_file,
            stat_used,
            id_sens,
            df_full,
            [str(clab) for clab in label],
            final_labels,
        )
    out_file = write_stats_to_file(
        out_directory, shape_file, df_full, final_labels, merge_field, seed
    )
    return final_labels, out_file


def compute_stats_dataframe(
    xml_file: str,
    stat_used: list[str],
    id_sens: int,
    df_full: pd.DataFrame,
    label: list[str],
    final_labels: list[str],
) -> tuple[pd.DataFrame, list[str]]:
    """
    Compute a dataframe from a given xml file produced by OTB.

    Parameters
    ----------
    xml_file : str
        Path to the XML file containing the statistics.
    stat_used : list of str
        List of statistics to extract from the XML file.
    id_sens : int
        The sensor index used to generate unique labels for the statistics.
    df_full : pd.DataFrame
        The general DataFrame containing all stats from all files.
    label : list of str
        List of labels corresponding to the statistics.
    final_labels : list of str
        The final list of labels which will be updated with new labels for each statistic.

    Returns
    -------
    tuple[pd.DataFrame, list[str]]
        - The updated dataframe with the added stats
        - The updated list of labels
    """
    with open(xml_file, encoding="utf-8") as cfi:
        doc = xmltodict.parse(cfi.read())
        list_stats = doc["GeneralStatistics"]["Statistic"]
        names = {elem["@name"]: elem["StatisticMap"] for elem in list_stats}
        for stat_use in stat_used:
            if isinstance(names[stat_use], list):
                value_dict = {
                    stat["@key"]: [float(i) for i in stat["@value"][1:-1].split(",")]
                    for stat in names[stat_use]
                }
            else:
                # if not list type means that only one polygon in file
                # then the split is different
                stat = names[stat_use]
                value_dict = {
                    stat["@key"]: [float(i) for i in stat["@value"][1:-1].split(",")]
                }
            label_sens = [f"S{id_sens}{stat_use}{i}" for i, _ in enumerate(label)]
            final_labels += label_sens
            if df_full is None:
                df_full = pd.DataFrame.from_dict(
                    value_dict, orient="index", columns=label_sens
                )
            else:
                df = pd.DataFrame.from_dict(
                    value_dict, orient="index", columns=label_sens
                )
                df_full = pd.concat([df_full, df], axis=1)
    return df_full, final_labels


def write_stats_to_file(
    out_directory: str,
    shape_file: str,
    df_full: pd.DataFrame,
    final_labels: list[str],
    merge_field: str,
    seed: int,
) -> str:
    """
    Write statistics from a DataFrame and merge these statistics into a shapefile.

    Parameters
    ----------
    out_directory : str
        Path to the output directory.
    shape_file : str
        Path to the shapefile containing the geometry to merge with the statistics.
    df_full : pd.DataFrame
        DataFrame containing the statistics.
    final_labels : list[str]
        List of labels corresponding to the statistics columns in the DataFrame.
    merge_field : str
        Column name used to merge the statistics with the shapefile.
    seed : int
        Fixed seed.

    Returns
    -------
    str
        The path to the generated shapefile.
    """

    labels_to_write = "\n".join(final_labels)
    with open(
        str(Path(out_directory) / "Stats_labels.txt"), "w", encoding="utf-8"
    ) as fid:
        fid.write(labels_to_write)
    bname = Path(shape_file).stem
    df_full[merge_field] = [int(i) for i in df_full.index]
    df_full.to_csv(str(Path(out_directory) / f"{bname}_seed_{seed}.csv"))
    df_shape = gpd.GeoDataFrame.from_file(shape_file)
    inter = df_shape.merge(df_full, on=merge_field)
    out_file = f"{Path(out_directory) / bname}_seed_{seed}.shp"
    if not Path(out_file).exists():
        inter.to_file(out_file)
    else:
        print("skipped : ", out_file)
    return out_file


def process_feature_pair(
    feat1: dict[str, Any],
    feat2: dict[str, Any],
    keepfields: list[str],
    data_base1_fid_field_name: str | None,
    data_base2_fid_field_name: str | None,
    layer_out: fiona.Collection,
) -> None:
    """
    Process a pair of features and write the intersection to the output layer if they intersect.

    Parameters
    ----------
    feat1 : dict
        The first feature dictionary.
    feat2 : dict
        The second feature dictionary.
    keepfields : list of str
        List of field names to keep.
    data_base1_fid_field_name : str, optional
        Field name for the FID of the first database.
    data_base2_fid_field_name : str, optional
        Field name for the FID of the second database.
    layer_out : fiona.Collection
        The output layer collection.
    """
    geom1 = shape(feat1["geometry"])
    geom2 = shape(feat2["geometry"])
    if geom1.intersects(geom2):
        prop = extract_properties(
            feat1,
            feat2,
            keepfields,
            data_base1_fid_field_name,
            data_base2_fid_field_name,
        )
        layer_out.write(
            {
                "geometry": mapping(geom1),
                "properties": prop,
            }
        )
