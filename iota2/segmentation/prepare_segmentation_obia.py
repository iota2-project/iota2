#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to prepare for segmentation"""
import inspect
import logging
import shutil
from pathlib import Path

import geopandas as gpd

import iota2.common.file_utils as fu
from iota2.common import otb_app_bank as otb
from iota2.common.utils import run
from iota2.segmentation.obia_metrics import split_classify_tiles
from iota2.segmentation.vector_operations import convert_xml_to_shape
from iota2.sensors.sensorscontainer import SensorsContainer
from iota2.typings.i2_types import (
    ClassifierOptions,
    I2FeaturesPipeline,
    I2LabelAlias,
    PathLike,
    SensorsParameters,
)
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def prepare_initial_segmentation(
    in_seg: str,
    tile: str,
    iota2_directory: PathLike,
    region_path: str | None = None,
    region_field: str = "region",
    seg_field: str = "DN",
    is_slic: bool = False,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    The raster cover all tiles.
    Use the common mask to extract ROI then vectorize if raster.
    If vector, clip using common mask

    Parameters
    ----------
    in_seg:
        raster (tiff) or shapefile (shp, sqlite)
    tile:
        the name of the tile processed
    iota2_directory:
        path to iota2 output path. Used to access data iota2 produce.
    region_path:
        optional path to a region shape. If not given all tiles are
         considered as a unique region.
    region_field:
        the name for the region field in shape.
    seg_field:
        the segment ID field name
    is_slic:
        If slic, just copy the tif images and then vectorize.
        If not slic, clip the segmentation to the tile footprint then vectorize
    """
    seg_path = str(Path(iota2_directory) / "segmentation")
    if region_path is not None:
        out_segmentation_raster = str(Path(seg_path) / "tmp" / f"seg_{tile}_tmp.tif")
    else:
        out_segmentation_raster = str(Path(seg_path) / "tmp" / f"seg_{tile}.tif")

    # Vector file is stored in tmp as intersection with regions must be computed
    out_segmentation_vector = str(Path(seg_path) / "tmp" / f"seg_{tile}.shp")

    reference_vector = str(
        Path(iota2_directory, "features", tile, "tmp", "MaskCommunSL.shp")
    )
    raster = False
    reference_raster = str(
        Path(iota2_directory, "features", tile, "tmp", "MaskCommunSL.tif")
    )
    if in_seg.endswith("tif"):
        raster = True
        crop_by_tile(in_seg, is_slic, out_segmentation_raster, reference_raster)

    elif in_seg.endswith("shp") or in_seg.endswith("sqlite"):
        # GML is excluded as we lost projection information
        # clip segmentation to tile extent

        cmd = (
            f"ogr2ogr -clipsrc {reference_vector} "
            f"{out_segmentation_vector} {in_seg}"
            " -nlt POLYGON -skipfailures"
        )
        print(cmd)
        run(cmd, logger=logger)
        # Rasterize for later
    else:
        raise ValueError(
            f"{in_seg} not correspond to allowed format"
            "only tif, shp or sqlite files are allowed"
        )

    # Handle region
    region_out = str(
        Path(iota2_directory, "segmentation", "tmp", f"regions_{tile}.shp")
    )
    if region_path is None:
        # create a shape for region == common mask
        df = gpd.GeoDataFrame.from_file(reference_vector)
        df[region_field] = 1
        df.to_file(region_out)
    else:
        df_reg = gpd.GeoDataFrame.from_file(region_path)
        if seg_field in df_reg.columns:
            df_reg = df_reg.drop(seg_field, axis="columns")
        df_ref = gpd.GeoDataFrame.from_file(reference_vector)
        if seg_field in df_ref.columns:
            df_ref = df_ref.drop(seg_field, axis="columns")
        inter = gpd.overlay(df_reg, df_ref, how="intersection")
        inter.to_file(region_out)
        if raster:
            out_segmentation_raster = apply_region_mask_to_segmentation(
                inter,
                iota2_directory,
                out_segmentation_raster,
                reference_raster,
                seg_path,
                tile,
            )

    # Vectorize the segmentation
    if Path(out_segmentation_vector).exists() and raster:
        vf.remove_shape(out_segmentation_vector)

    if raster:
        cmd = (
            f"gdal_polygonize.py -f 'ESRI Shapefile' {out_segmentation_raster}"
            f" {out_segmentation_vector}"
        )
        logging.info("Vectorize segmentation")
        run(cmd, logger=logger)


def crop_by_tile(
    in_seg: PathLike, is_slic: bool, out_segmentation_raster: str, reference_raster: str
) -> None:
    """
    Crop the input segmentation by tiles (unless it comes from slic)

    Parameters
    ----------
    in_seg: PathLike
        Input segmentation (as raster)
    is_slic: bool
        Flag indicating if the segmentation was done using slic
    out_segmentation_raster: str
        Path to output cropped raster
    reference_raster: str
        Raster to use for cropping

    Returns
    -------
    None
    """
    roi, _ = otb.create_application(
        otb.AvailableOTBApp.SUPERIMPOSE,
        {
            "inm": in_seg,
            "out": out_segmentation_raster,
            "interpolator": "nn",
            "inr": reference_raster,
        },
    )
    if is_slic:
        # if the segmentation comes from slic per tile
        # no needs to crop it
        # Just copy to segmentation folder
        # This file is used later for zonal stats
        shutil.copy(in_seg, out_segmentation_raster)

    else:
        roi.ExecuteAndWriteOutput()


def apply_region_mask_to_segmentation(
    inter: gpd.GeoDataFrame,
    iota2_directory: PathLike,
    out_segmentation_raster: PathLike,
    reference_raster: PathLike,
    seg_path: PathLike,
    tile: str,
) -> str:
    """
    Apply region mask to the segmentation raster

    Parameters
    ----------
    inter: gpd.GeoDataFrame
        Dataframe representing the intersection between common mask and regions
    iota2_directory: PathLike
        Path to the iota2 root dir
    out_segmentation_raster: PathLike
        Path to the segmentation raster
    reference_raster: PathLike
        Path to a reference raster (common mask)
    seg_path: PathLike
        Path to the segmentation directory
    tile: str
        Tile name

    Returns
    -------
    final_seg: PathLike
        Path to the masked raster
    """
    # Masking the raster
    # Avoid to rasterize as it can lose segments
    inter["fake_id"] = 1
    inter = inter.dissolve(by="fake_id")
    dissolved = str(
        Path(iota2_directory, "segmentation", "tmp", f"dissolved_{tile}.shp")
    )
    inter.to_file(dissolved)
    region_raster = str(
        Path(iota2_directory, "segmentation", "tmp", f"regions_{tile}.tif")
    )
    rasterize_app, _ = otb.create_application(
        otb.AvailableOTBApp.RASTERIZATION,
        {
            "in": dissolved,
            "out": region_raster,
            "im": reference_raster,
            "mode": "binary",
        },
    )
    rasterize_app.ExecuteAndWriteOutput()
    final_seg = str(Path(seg_path) / "tmp" / f"seg_{tile}.tif")
    bandmath, _ = otb.create_application(
        otb.AvailableOTBApp.BAND_MATH,
        {
            "il": [region_raster, out_segmentation_raster],
            "out": final_seg,
            "exp": "im1b1==0?0:im2b1",
        },
    )
    bandmath.ExecuteAndWriteOutput()
    return final_seg


def compute_zonal_stats_by_tiles(
    iota2_directory: str,
    tile: str,
    sensors_parameters: SensorsParameters,
    seed: int,
    spatial_res: int,
    seg_field: str,
    buffer_size: int,
    region_field: str,
    stat_used: list[str],
    working_dir: str,
) -> None:
    """
    Compute zonal stats for training samples in a specific tile.

    Parameters
    ----------
    iota2_directory:
        the output path of iota2
    tile:
        tile name
    sensors_parameters:
        dictionary containing all parameters about sensors
    seed:
        the seed number
    spatial_res:
        the working resolution (to compute buffer real size)
    seg_field:
        segments field name
    buffer_size:
        buffer size in number of pixels
    region_field:
        region field name
    stat_used:
        list of stats which can be used
        choices are: mean, count, min, max, std
    working_dir:
        Working directory

    Returns
    -------
    None
    """
    # Access to spectral features for each sensor
    sensor_tile_container = SensorsContainer(
        tile, working_dir, iota2_directory, **sensors_parameters
    )
    sensors_features = sensor_tile_container.get_sensors_features(available_ram=1000)
    in_seg = str(
        Path(
            iota2_directory, "segmentation", f"learning_samples_{tile}_seed_{seed}.shp"
        )
    )
    # split_tile_regions_subgrid
    keepfields = ["i2label", "region", seg_field, "gridid", "idlearn"]
    dico_reg_parts = split_classify_tiles(
        iota2_directory,
        in_seg,
        tile,
        int(spatial_res),
        seg_field,
        region_field,
        int(buffer_size),
        keepfields,
        seed,
    )
    mask = str(Path(iota2_directory) / "features" / tile / "tmp" / "MaskCommunSL.tif")

    compute_region_stats(
        dico_reg_parts, stat_used, sensors_features, mask, tile, seed, iota2_directory
    )


def compute_region_stats(
    dico_reg_parts: dict[str, list[str]],
    stat_used: list[str],
    sensors_features: list[tuple[str, I2FeaturesPipeline]],
    mask: str,
    tile: str,
    seed: int,
    iota2_directory: str,
) -> None:
    """
    Compute zonal stats for all regions

    Parameters
    ----------
    dico_reg_parts: dict[str, list[str]]
        Dictionary mapping region names to shapefiles from a grid
    stat_used: list[str]
        List of stats names to use
    sensors_features: list[tuple[str, I2FeaturesPipeline]]
        Each sensor's features
    mask: str
        Common mask
    tile: str
        Tile name
    seed: int
        Seed number
    iota2_directory: str
        Iota2 output directory

    Returns
    -------
    None
    """
    for region, regions_vals in dico_reg_parts.items():
        for shape_part in regions_vals:
            part = Path(shape_part).stem.split("_")[-3]

            seg = prepare_sub_tile_to_stats(shape_part, mask, "idlearn")
            xml_files, labels = do_zonal_stats(
                str(Path(iota2_directory) / "learningSamples"),
                seg,
                sensors_features,
                tile,
                seed,
                region,
                part,
            )
            convert_xml_to_shape(
                str(Path(iota2_directory) / "learningSamples"),
                xml_files,
                labels,
                shape_part,
                seed,
                "idlearn",
                stat_used,
            )


def learning_models_by_region(
    iota2_directory: str,
    region: str,
    seed: int,
    data_field: str,
    classifier_name: str,
    enable_stats: bool,
    classifier_options: ClassifierOptions,
) -> None:
    """
    This function trains a classifier for each region

    Parameters
    ----------
    iota2_directory:
        iota2 output directory
    region:
        region value
    seed:
        seed number
    data_field:
        label column name used for train the classifier
    classifier_name:
        classifier name. Only OTB classifiers allowed
    enable_stats:
        compute mean and std stats for normalization.
        Not working for now.
    classifier_options:
        dictionary of OTB allowed parameters for classifier
        ex: {"classifier.sharkrf.nbtrees" : 100}
    """
    print(
        "\n" * 2
        + "*" * 30
        + "\n"
        + f"{inspect.stack()[0][3]} start"
        + "\n" * 2
        + "*" * 30
    )
    # Get all samples file for the target region and seed
    samples_files = fu.file_search_and(
        str(Path(iota2_directory) / "learningSamples"),
        True,
        f"_region_{region}_",
        f"_seed_{seed}.shp",
    )

    model = str(
        Path(iota2_directory, "model", f"model_region_{region}_seed_{seed}.txt")
    )
    # Get features name from text file
    feature_file = str(Path(iota2_directory) / "learningSamples" / "Stats_labels.txt")
    with open(feature_file, encoding="utf-8") as cont_file:
        feats = cont_file.read().split("\n")

    # if samples  normalization is enable get the file
    learning_parameters = {
        "io.vd": samples_files if isinstance(samples_files, list) else [samples_files],
        "cfield": data_field,
        "feat": feats,
        "classifier": classifier_name,
        "io.out": model,
    }
    if enable_stats:
        features_stats = str(
            Path(
                iota2_directory,
                "stats",
                f"learn_samples_region_{region}_seed_{seed}_stats_label.txt",
            )
        )
        learning_parameters["io.stats"] = features_stats

    # samples_file is a list of shapefile, where each one have the same
    # columns name
    assert classifier_options
    app_learning, _ = otb.create_application(
        otb.AvailableOTBApp.TRAIN_VECTOR_CLASSIFIER,
        {**learning_parameters, **classifier_options},
    )
    app_learning.ExecuteAndWriteOutput()


def prepare_sub_tile_to_stats(shape_file: str, mask: str, seg_field: str) -> str:
    """
    Rasterize the shapefile according to the mask for zonal stats efficiency

    Parameters
    ----------
    shape_file:
        the shapefile to rasterize
    mask:
        the mask used for reference
    seg_field:
        the field used to rasterize
    """
    # Create Ref
    out_roi = str(Path(shape_file).parent / f"{Path(shape_file).stem}_mask.tif")
    roi, _ = otb.create_application(
        otb.AvailableOTBApp.EXTRACT_ROI,
        {"in": mask, "out": out_roi, "mode": "fit", "mode.fit.vect": shape_file},
    )
    roi.ExecuteAndWriteOutput()
    out = str(Path(shape_file).parent / f"{Path(shape_file).stem}.tif")
    raster, _ = otb.create_application(
        otb.AvailableOTBApp.RASTERIZATION,
        {
            "in": shape_file,
            "out": out,
            "im": out_roi,
            "mode": "attribute",
            "mode.attribute.field": seg_field,
            "ram": 2000,
        },
    )
    raster.ExecuteAndWriteOutput()
    return out


def do_zonal_stats(
    out_directory: str,
    segmentation: str,
    sensors_features: list[tuple[str, I2FeaturesPipeline]],
    tile: str,
    seed: int,
    region: str,
    part: int | str,
) -> tuple[list[str], list[list[I2LabelAlias]]]:
    """
    Compute the zonal stats. Tile are splitted before for handling
     memory issues.

    Parameters
    ----------
    out_directory:
        the working directory
    segmentation:
        the rasterized segmentation
    sensors_features:
        otb pipeline containing spectral features
    tile:
        tile name
    seed:
        seed number
    region:
        the region id
    part:
        the part number (for indexing results)
    """

    classif_dir = str(Path(out_directory) / "zonal_stats")
    labels = []
    outputs_list = []
    for sensor_name, (
        (sensor_features, _),
        features_labels,
    ) in sensors_features:
        # Stack every feature
        sensor_features.Execute()
        labels.append(features_labels)
        output_xml = str(
            Path(
                classif_dir,
                f"{sensor_name}_{tile}_"
                f"samples_seed_{seed}_region_{region}_part_{part}_stats.xml",
            )
        )
        extract_roi, _ = otb.create_application(
            otb.AvailableOTBApp.EXTRACT_ROI,
            {"in": sensor_features, "mode": "fit", "mode.fit.im": segmentation},
        )

        extract_roi.Execute()
        zonal_stats_app, _ = otb.create_application(
            otb.AvailableOTBApp.ZONAL_STATISTICS,
            {
                "in": extract_roi,
                "inbv": "0",
                "inzone.labelimage.in": segmentation,
                "inzone": "labelimage",
                "inzone.labelimage.nodata": 0,
                "out.xml.filename": output_xml,
                "out": "xml",
                "ram": 2000,
            },
        )
        if not Path(output_xml).exists():
            zonal_stats_app.ExecuteAndWriteOutput()
        else:
            print("Stats found skipping", output_xml)
        outputs_list.append(output_xml)
    return outputs_list, labels


def do_vector_classification(
    output_path: str,
    shape_file: str,
    model: str,
    features: list[str],
    kept_field: list[str],
) -> None:
    """
    Launch OTB vector classification.
    The input `shape_file` is updated while processing.
    Look at OTB documentation about VectorClassifier application.

    Parameters
    ----------

    output_path:
        path for resulting shape (without features)
    shape_file:
        the shapefile containing the features. A new column will be added.
    model:
        the OTB model. Output of TrainVectorClassifier
    features:
        the list of features used for classification
    kept_field:
        list of field to be kept in the final result
    """
    bname = Path(shape_file).name
    classif_shape = shape_file
    # Call vector classifier
    vector_app, _ = otb.create_application(
        otb.AvailableOTBApp.VECTOR_CLASSIFIER,
        {"in": shape_file, "model": model, "feat": features, "confmap": True},
    )
    vector_app.ExecuteAndWriteOutput()
    # Remove all columns to final classif
    gdf = gpd.GeoDataFrame.from_file(classif_shape)
    gdf = gdf[kept_field + ["predicted", "confidence", "geometry"]]
    gdf.to_file(str(Path(output_path) / bname))


def classify_zonal_stats(
    iota2_directory: str,
    tile: str,
    sensors_parameters: SensorsParameters,
    seed: int,
    spatial_res: int,
    stats: list[str],
    seg_field: str,
    region_field: str,
    buffer_size: int,
    working_dir: str,
) -> None:
    """
    Compute Zonal statistics over a tile
    Depending on a segmentation input and a stack of spectral features

    Parameters
    ----------
    iota2_directory:
        iota2 output path
    tile:
        tile name
    sensors_parameters:
        dictionary containing all parameters for sensors
    seed:
        seed number
    spatial_res:
        working resolution. Used for compute buffer can be different that
        real resolution
    stats:
        list of statistics used for classification.
        Must be the same as used for training.
    seg_field:
        segmentation field name
    region_field:
        region field name
    buffer_size:
        buffer size in pixel unit
    working_dir:
        working directory
    """
    # Access to spectral features for each sensor
    sensor_tile_container = SensorsContainer(
        tile, working_dir, iota2_directory, **sensors_parameters
    )
    sensors_features = sensor_tile_container.get_sensors_features(available_ram=1000)
    in_seg = str(Path(iota2_directory) / "segmentation" / f"seg_{tile}.shp")
    keepfields = ["DN", "region", seg_field, "gridid"]
    dico_reg_parts = split_classify_tiles(
        iota2_directory,
        in_seg,
        tile,
        int(spatial_res),
        seg_field,
        region_field,
        int(buffer_size),
        keepfields,
        seed=None,
    )

    mask = str(Path(iota2_directory) / "features" / tile / "tmp" / "MaskCommunSL.tif")
    zonal_stats_by_region(
        dico_reg_parts,
        stats,
        sensors_features,
        mask,
        tile,
        seed,
        seg_field,
        iota2_directory,
    )


def zonal_stats_by_region(
    dico_reg_parts: dict[str, list[str]],
    stats_to_use: list[str],
    sensors_features: list[tuple[str, I2FeaturesPipeline]],
    mask: str,
    tile: str,
    seed: int,
    seg_field: str,
    iota2_directory: str,
) -> None:
    """
    Compute the zonal stats and start classification for all regions

    Parameters
    ----------
    dico_reg_parts: dict[str, list[str]]
        Dictionary mapping region names to shapefiles from a grid
    stats_to_use: list[str]
        List of stats names to use
    sensors_features: list[tuple[str, I2FeaturesPipeline]]
        Each sensor's features
    mask: str
        Common mask
    tile: str
        Tile name
    seed: int
        Seed number
    seg_field:
        Segmentation field name
    iota2_directory: str
        Iota2 output directory

    Returns
    -------

    """
    for region, regions_vals in dico_reg_parts.items():
        model = str(
            Path(iota2_directory, "model", f"model_region_{region}_seed_{seed}.txt")
        )
        for shape_part in regions_vals:
            part = Path(shape_part).stem.split("_")[-1]
            seg = prepare_sub_tile_to_stats(shape_part, mask, seg_field)
            xml_files, labels = do_zonal_stats(
                str(Path(iota2_directory) / "classif"),
                seg,
                sensors_features,
                tile,
                seed,
                region,
                part,
            )

            features, shape_stats = convert_xml_to_shape(
                str(Path(iota2_directory) / "classif" / "zonal_stats"),
                xml_files,
                labels,
                shape_part,
                seed,
                seg_field,
                stats_to_use,
            )
            output_path = str(Path(iota2_directory) / "classif" / "reduced")
            do_vector_classification(
                output_path, shape_stats, model, features, [seg_field]
            )


def merge_clipped_tiles(
    iota2_directory: str, seed: int, logger: logging.Logger = LOGGER
) -> None:
    """
    Get all classification in sqlite format and merge them in a unique layer

    Parameters
    ----------
    iota2_directory:
        iota2 output directory
    seed:
        seed number

    """
    list_shapes = fu.file_search_and(
        str(Path(iota2_directory) / "classif"), True, "clipped.sqlite"
    )

    out_name = str(Path(iota2_directory) / "final" / f"Classif_Seed_{seed}.sqlite")

    # Init the file
    cmd = f"ogr2ogr -f sqlite -nln layer {out_name} {list_shapes[0]}"
    print(cmd)
    logger.info(cmd)
    run(cmd, logger=logger)
    for shape_in in list_shapes[1:]:
        cmd = f"ogr2ogr -f sqlite -nln layer -append -update " f"{out_name} {shape_in}"
        print(cmd)
        logger.info(cmd)
        run(cmd, logger=logger)
