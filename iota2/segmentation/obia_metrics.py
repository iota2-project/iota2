#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module used to compute and manage metrics after an OBIA classification"""

import logging
from pathlib import Path

import geopandas as gpd
import pandas as pd

from iota2.common import file_utils as fu
from iota2.common.utils import run
from iota2.segmentation.vector_operations import do_intersect_with_grid
from iota2.validation import results_utils as ru

LOGGER = logging.getLogger("distributed.worker")


def compute_confusion_matrix_from_vector(
    classif_dir: str,
    validation_samples_dir: str,
    tile: str,
    output_dir: str,
    seed: int,
    ref_label_name: str,
    pred_label_name: str,
    seg_field: str,
) -> None:
    """
    Iota2 step desinged function to:
     Compute the confusion matrix of a vector file according to a validation
     shapefile
    Parameters
    ----------

    classif_dir:
        path where classification are stored
    validation_samples_dir:
        path where validation files are stored
    tile:
        tile name
    output_dir:
        output directory
    seed:
        seed number
    ref_label_name:
        label name in reference data
    pred_label_name:
        predicted class name
    seg_field:
        segment id name
    """
    classif_shape = fu.file_search_and(
        classif_dir, True, tile, f"{seed}", "clipped.shp"
    )[0]
    validation_shape = fu.file_search_and(
        validation_samples_dir, True, tile, f"{seed}", "val.sqlite"
    )[0]
    # Convert to shape for geopandas
    val_shape = Path(validation_shape).name + ".shp"
    if not Path(val_shape).exists():
        run(f"ogr2ogr -f 'ESRI Shapefile' " f"{val_shape} {validation_shape}")

    extract_matrix_from_vector(
        classif_shape,
        val_shape,
        tile,
        str(Path(output_dir) / "TMP"),
        seed,
        ref_label_name,
        pred_label_name,
        seg_field,
    )


def extract_matrix_from_vector(
    classif_shape: str,
    val_shape: str,
    tile: str,
    output_dir: str,
    seed: int,
    ref_label_name: str,
    pred_label_name: str,
    seg_field: str,
) -> None:
    """
    Compute the confusion matrix of a vector file according to a
     validation shapefile

    Parameters
    ----------
    classif_shape:
        the classification shapefile
    val_shape:
        the validation shapefile
    tile:
        tile name
    output_dir:
        output directory
    seed:
        seed number
    ref_label_name:
        label name in reference data
    pred_label_name:
        predicted class name
    seg_field:
        segment id name
    """
    # Start
    df_classif = gpd.GeoDataFrame.from_file(classif_shape)

    df_val = gpd.GeoDataFrame.from_file(val_shape)
    df_val["i2valid"] = range(1, len(df_val.index) + 1)
    df_join = gpd.sjoin(df_val, df_classif, how="inner", predicate="intersects")
    # duplicates on seg field means that at least two validation polygons
    # intersects one segment
    df_join.drop_duplicates(seg_field, keep=False, inplace=True)
    df = pd.DataFrame(df_join, columns=[ref_label_name, pred_label_name])
    confusion_matrix = pd.crosstab(
        df[ref_label_name],
        df[pred_label_name],
        rownames=[ref_label_name],
        colnames=[pred_label_name],
    )
    confusion_matrix.to_csv(
        str(Path(output_dir) / f"{tile}_confusion_matrix_seed_{seed}.csv")
    )


def compute_metrics_obia(
    iota2_directory: str,
    tile: str,
    seed: int,
    labels_vector_table: dict[int, int],
    data_field: str,
    ref_label_name: str,
    seg_field: str,
    pred_label_name: str = "predicted",
) -> None:
    """
    Iota2 step designed function to:
    - reassemble a vector classification for parts
    - produce the confusion matrix

    Parameters
    ----------

    iota2_directory:
        iota2 output path
    tile:
        tile name
    seed:
        seed number
    label_vector_table:
        dictionary for label encoding
    data_field:
        column name for decoded labels in final classification
    ref_label_name:
        label name in reference data
    pred_label_name:
        predicted class name
    seg_field:
        segment id name
    """
    reassemble_vectors_classification(
        iota2_directory, tile, seed, data_field, labels_vector_table
    )
    compute_confusion_matrix_from_vector(
        str(Path(iota2_directory) / "classif"),
        str(Path(iota2_directory) / "dataAppVal"),
        tile,
        str(Path(iota2_directory) / "final"),
        seed,
        ref_label_name,
        pred_label_name,
        seg_field,
    )


def cumulate_confusion_matrix(
    out_directory: str,
    seed: int,
    data_field: str,
    nomenclature_file: str,
    label_vector_table: dict[int, int],
    tmp_dir: str,
) -> None:
    """
    Get all confusion_matrix_seed_x.csv files in tmp_dir, and accumulate values for each class

    Parameters
    ----------

    out_directory:
        output path
    seed:
        seed number
    data_field:
        label name
    nomenclature_file:
        path to nomenclature file
    label_vector_table:
        dictionary of label encoding
    tmp_dir:
        path where are stored the confusion matrix of each tile
    """
    matrix_files = fu.file_search_and(
        tmp_dir, True, "confusion_matrix", f"seed_{seed}.csv"
    )
    # Handle encoding
    df_nomen = pd.read_csv(
        nomenclature_file, sep=":", names=["name", "label"], header=None
    )

    labels_nomen = df_nomen.label.values
    dico_lab = {}
    labels = []
    for label in labels_nomen:
        if label in label_vector_table:
            dico_lab[label_vector_table[label]] = label
            labels.append(str(label_vector_table[label]))
        else:
            print(f"{label} in nomenclature not in reference data")

    dict_matrix = build_matrix_dictionary(labels, matrix_files, data_field)

    matrix_file = write_matrix_to_file(dict_matrix, dico_lab, labels, seed, tmp_dir)

    report_txt = str(Path(out_directory) / "RESULTS.txt")
    report_fig = str(Path(out_directory, f"Confusion_Matrix_Classif_Seed_{seed}.png"))
    ru.gen_confusion_matrix_fig(matrix_file, nomenclature_file, out_png=report_fig)
    ru.stats_report([matrix_file], nomenclature_file, report_txt)


def write_matrix_to_file(
    dict_matrix: dict[str, dict[str, int]],
    dico_lab: dict[int, int],
    labels: list[str],
    seed: int,
    tmp_dir: str,
) -> str:
    """
    Write the complet matrix to a csv file

    Parameters
    ----------
    dict_matrix:
        Dictionary representing the confusion matrix
    dico_lab:
        Dictionary containing the labels
    labels:
        List of labels
    seed:
        Seed number
    tmp_dir
        path where are stored the confusion matrix of each tile

    Returns
    -------
    matrix_file : str
        Path to the output file
    """
    l_temp = []
    for label in labels:
        # if label in labels_seen:
        l_temp.append(str(dico_lab[int(label)]))

    matrix_file = str(Path(tmp_dir) / f"Classif_Seed_{seed}.csv")
    with open(matrix_file, "w", encoding="utf-8") as file:
        file.write("#Reference labels (rows):" + ",".join(l_temp) + "\n")
        file.write("#Produced labels (columns):" + ",".join(l_temp) + "\n")
        for ref in labels:
            line = []
            for pred in labels:
                line.append(f"{dict_matrix[ref][pred]}")
            file.write(",".join(line) + "\n")
    return matrix_file


def build_matrix_dictionary(
    labels: list[str], matrix_files: list[str], data_field: str
) -> dict[str, dict[str, int]]:
    """
    Build the dictionary representing the confusion matrix

    Parameters
    ----------
    labels :
        List of labels
    matrix_files:
        List of paths to confusion matrices
    data_field:
        Column name of the labels

    Returns
    -------
    dict_matrix dict[str, dict[str, int]]
        Dictionary representing the compilation of the confusion matrices
    """
    # init dictionary
    dict_matrix: dict[str, dict[str, int]] = {}
    for ref in labels:
        dict_matrix[ref] = {}
        for pred in labels:
            dict_matrix[ref][pred] = 0
    # iterate and fill the dictionary
    labels_seen = []
    for matrix in matrix_files:
        df_mat = pd.read_csv(matrix)
        df_labels = df_mat[data_field]
        for label in df_labels.values:
            if str(label) not in labels_seen:
                labels_seen.append(str(label))
        mat_vals = df_mat.drop(data_field, axis=1)
        dict_labs = df_labels.to_dict()
        dict_vals = mat_vals.to_dict()
        for column in dict_vals.keys():
            # keys are columns ie predicted class
            d_t = dict_vals[column]
            for index in d_t.keys():
                # mypy can't infer the exact type of columns
                dict_matrix[str(dict_labs[index])][column] += d_t[index]  # type: ignore
    return dict_matrix


def split_classify_tiles(
    iota2_directory: str,
    in_seg: str,
    tile: str,
    spatial_res: int,
    seg_field: str,
    region_field: str,
    buffer_size: int,
    fields_to_keep: list[str],
    seed: None | int = None,
) -> dict[str, list[str]]:
    """
    Split a shapefile according to a grid

    Parameters
    ----------
    iota2_directory:
        iota2 output directory
    in_seg:
        the input shapefile
    seg_field:
        the polygons ID field
    tile:
        tile name
    buffer_size:
        buffer size in pixel unit
    spatial_res:
        spatial resolution in image resolution unit (meter for example)
    region_field:
        region column name
    fields_to_keep:
        list of fields to retrieve in the resulting shapefiles
    seed: int
        Fixed seed number
    """

    work_dir = str(Path(iota2_directory) / "segmentation" / "tmp")

    if seed is not None:
        out_folder = str(
            Path(iota2_directory, "segmentation", "grid_split_learn", tile)
        )
        prefix = f"_seed_{seed}"
    else:
        out_folder = str(Path(iota2_directory) / "segmentation" / "grid_split" / tile)
        prefix = ""
    if not Path(out_folder).exists():
        Path(out_folder).mkdir()
    regions_dict: dict[str, list[str]] = do_intersect_with_grid(
        in_seg,
        seg_field,
        tile,
        prefix,
        work_dir,
        buffer_size,
        spatial_res,
        out_folder,
        region_field,
        fields_to_keep,
    )
    return regions_dict


def reassemble_vectors_classification(
    iota2_directory: str,
    tile: str,
    seed: int,
    data_field: str,
    labels_vector_table: dict[int, int],
    logger: logging.Logger = LOGGER,
) -> None:
    """
    This function gets all vector classification produced in
    classif/reduced path and merge them as a big shapefile

    Parameters
    ----------
    iota2_directory:
        iota2 output path
    tile:
        tile name
    seed:
        seed number
    data_field:
        output label field (after decoding)
    labels_vector_table:
        the encoding label dictionary

    """
    classif_dir = str(Path(iota2_directory) / "classif")
    shp_parts = fu.file_search_and(
        str(Path(classif_dir) / "reduced"), True, tile, ".shp"
    )
    crs_env = gpd.read_file(shp_parts[0]).crs
    merged_parts = gpd.GeoDataFrame(
        pd.concat([gpd.read_file(i) for i in shp_parts], ignore_index=True), crs=crs_env
    )
    vector_base = str(Path(classif_dir) / f"Vectorized_map_tile_{tile}_seed_{seed}")
    # Re encode labels
    reverse_labels_encode = {v: k for k, v in labels_vector_table.items()}
    # Verifier cohérence
    merged_parts[data_field] = merged_parts["predicted"].map(reverse_labels_encode)

    # used for validation
    # no overlap with other tiles as validation samples are clipped by envelope
    merged_parts.to_file(vector_base + ".shp")

    # used for the final product
    envelope = str(Path(iota2_directory) / "envelope" / f"{tile}.shp")
    cmd = (
        f"ogr2ogr -clipsrc {envelope} "
        f"{vector_base}_clipped.shp {vector_base}.shp "
        "-nlt POLYGON -skipfailures"
    )
    print(cmd)
    logger.info(cmd)
    run(cmd, logger=logger)
    cmd = (
        f"ogr2ogr -f sqlite {vector_base}_clipped.sqlite" f" {vector_base}_clipped.shp"
    )
    logger.info(cmd)
    run(cmd, logger=logger)
