# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Segmentation module"""
import logging
import math
import shutil
from pathlib import Path

from iota2.common.generate_features import FeaturesMapParameters, generate_features
from iota2.common.otb_app_bank import (
    AvailableOTBApp,
    create_application,
    get_input_parameter_output,
)
from iota2.typings.i2_types import OtbApp, SensorsParameters

LOGGER = logging.getLogger("distributed.worker")
LOGGER.addHandler(logging.NullHandler())


def slic_segmentation(
    tile_name: str,
    output_path: str,
    sensors_parameters: SensorsParameters,
    ram: int = 128,
    working_dir: str | None | None = None,
    force_spw: int | None | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """generate segmentation using SLIC algorithm

    Parameters
    ----------
    tile_name : string
        tile's name
    output_path : string
        iota2 output path
    sensors_parameters : dict
        sensors parameters description
    ram : int
        available ram
    working_dir : string
        directory to store temporary data
    force_spw : int
        force segments' spatial width
    logger : logging
        root logger
    """

    slic_name = f"SLIC_{tile_name}.tif"
    features_map_parameters = FeaturesMapParameters(
        working_directory=working_dir,
        tile=tile_name,
        output_path=output_path,
        sensors_parameters=sensors_parameters,
    )
    otb_pipelines, _ = generate_features(
        features_map_parameters,
        logger=logger,
    )
    assert otb_pipelines.interpolated_pipeline
    all_features: OtbApp = otb_pipelines.interpolated_pipeline
    all_features.Execute()

    spx, _ = all_features.GetImageSpacing(get_input_parameter_output(all_features))

    tmp_dir = working_dir
    if working_dir is None:
        tmp_dir = str(
            Path(output_path) / "features" / tile_name / "tmp" / "SLIC_TMPDIR"
        )
    else:
        tmp_dir = str(Path(working_dir) / tile_name)

    Path(tmp_dir).mkdir(parents=True, exist_ok=True)

    slic_seg_path = str(Path(output_path) / "features" / tile_name / "tmp" / slic_name)

    features_ram_estimation = all_features.PropagateRequestedRegion(
        key="out", region=all_features.GetImageRequestedRegion("out")
    )
    # increase estimation...
    features_ram_estimation = features_ram_estimation * 1.5
    xy_tiles = math.ceil(
        math.sqrt(float(features_ram_estimation) / (float(ram) * 1024**2))
    )
    slic_parameters = {
        "in": all_features,
        "tmpdir": tmp_dir,
        "spw": force_spw if force_spw else int(spx),
        "tiling": "manual",
        "tiling.manual.ny": int(xy_tiles),
        "tiling.manual.nx": int(xy_tiles),
        "out": slic_seg_path,
    }
    slic_seg, _ = create_application(AvailableOTBApp.SLIC, slic_parameters)

    if not Path(slic_seg_path).exists():
        logger.info(
            f"Processing SLIC segmentation : {tile_name}\n\t\t"
            f"with parameters : {slic_parameters}"
        )
        slic_seg.ExecuteAndWriteOutput()

    if working_dir is None:
        shutil.rmtree(tmp_dir, ignore_errors=True)
