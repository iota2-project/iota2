#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the learning task"""

import logging
from itertools import product
from pathlib import Path

import iota2.common.i2_constants as i2_const
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.learning.launch_learning import (
    learn_otb_model,
    learn_scikitlearn_model,
    learn_torch_model,
)
from iota2.learning.pytorch.train_pytorch_model import (
    DataLoaderParameters,
    ModelBuilder,
)
from iota2.learning.train_sklearn import (
    CrossValidationParameters,
    SciKitModelParams,
    cast_config_cv_parameters,
)
from iota2.steps import iota2_step
from iota2.typings.i2_types import DBInfo, ModelHyperParameters

I2_CONST = i2_const.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")


class LearnModel(iota2_step.Step):
    """Class for launching the learning step"""

    resources_block_name = "training"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.output_path = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "output_path"
        )

        self.runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")

        self.classifier = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "classifier"
        )

        sample_management = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "sample_management"
        )
        self.sk_model_parameters = dict(
            rcf.ReadConfigFile(self.cfg).get_section("scikit_models_parameters")
        )

        self.model_type = self.sk_model_parameters["model_type"]
        del self.sk_model_parameters["model_type"]
        del self.sk_model_parameters["standardization"]
        del self.sk_model_parameters["cross_validation_grouped"]
        del self.sk_model_parameters["cross_validation_folds"]
        del self.sk_model_parameters["cross_validation_parameters"]

        features_from_raw_dates = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "features_from_raw_dates"
        )
        self.neural_network = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "deep_learning_parameters"
        )
        deep_learning_enabled = "dl_name" in self.neural_network

        self.db_ext = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "learning_samples_extension"
        )
        if deep_learning_enabled:
            self.db_ext = f"{I2_CONST.i2_database_ext}"
        for model_name, _ in self.spatial_models_distribution.items():
            for seed in range(self.runs):
                target_model = f"model_{model_name}_seed_{seed}"
                task_name = f"learning_model_{model_name}_seed_{seed}"
                vector_file = str(
                    Path(self.output_path)
                    / "learningSamples"
                    / f"Samples_region_{model_name}_seed{seed}_learn.{self.db_ext}"
                )
                output_model = str(
                    Path(self.output_path)
                    / "model"
                    / f"model_{model_name}_seed_{seed}.txt"
                )
                if features_from_raw_dates or deep_learning_enabled:

                    hyperparameters_couples = list(
                        product(
                            self.neural_network["hyperparameters_solver"]["batch_size"],
                            self.neural_network["hyperparameters_solver"][
                                "learning_rate"
                            ],
                        )
                    )
                    for hyper_num, (batch_size, learning_rate) in enumerate(
                        hyperparameters_couples
                    ):

                        task = self.I2Task(
                            task_name=f"{task_name}_hyp_{hyper_num}",
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters=self.get_learning_pytorch_parameters(
                                vector_file,
                                f"{output_model.replace('.txt', '')}_hyp_{hyper_num}.txt",
                                batch_size,
                                learning_rate,
                                working_directory,
                            ),
                            task_resources=self.get_resources(),
                        )
                        dep_key = "region_tasks"
                        dep_values = [target_model]
                        if sample_management:
                            dep_key = "seed_tasks"
                            dep_values = [str(seed)]
                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="region_tasks",
                            task_sub_group=f"{target_model}_hyp_{hyper_num}",
                            task_dep_dico={dep_key: dep_values},
                        )
                else:
                    task = self.I2Task(
                        task_name=task_name,
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters=self.get_learning_i2_task_parameters(
                            vector_file, output_model, model_name, seed
                        ),
                        task_resources=self.get_resources(),
                    )
                    dep_key = "region_tasks"
                    dep_values = [target_model]
                    if sample_management:
                        dep_key = "seed_tasks"
                        dep_values = [str(seed)]

                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="region_tasks",
                        task_sub_group=f"{target_model}",
                        task_dep_dico={dep_key: dep_values},
                    )

    def get_learning_pytorch_parameters(
        self,
        vector_file: str,
        output_model: str,
        batch_size: int,
        learning_rate: float,
        working_directory: str | None,
    ) -> dict:
        """Return task parameters to learn pytorch model"""
        data_field = self.i2_const.re_encoding_label_name
        database = DBInfo(data_field, vector_file)
        model_builder = ModelBuilder(
            dl_name=self.neural_network["dl_name"],
            model_path=output_model,
            model_parameters_file=output_model.replace(".txt", "_parameters.pickle"),
            encoder_convert_file=output_model.replace(".txt", "_encoder.pickle"),
            dl_module=self.neural_network["dl_module"],
        )
        model_hyperparameters = ModelHyperParameters(
            dl_parameters=self.neural_network["dl_parameters"],
            restart_from_checkpoint=self.neural_network["restart_from_checkpoint"],
            weighted_labels=self.neural_network["weighted_labels"],
            learning_rate=learning_rate,
            epochs=self.neural_network["epochs"],
            batch_size=batch_size,
            model_optimization_criterion=self.neural_network[
                "model_optimization_criterion"
            ],
            additional_statistics_percentage=self.neural_network[
                "additional_statistics_percentage"
            ],
            enable_early_stop=self.neural_network["enable_early_stop"],
            epoch_to_trigger=self.neural_network["epoch_to_trigger"],
            early_stop_patience=self.neural_network["early_stop_patience"],
            early_stop_metric=self.neural_network["early_stop_metric"],
            early_stop_tol=self.neural_network["early_stop_tol"],
            adaptive_lr=self.neural_network["adaptive_lr"],
        )
        dataloader_params = DataLoaderParameters(
            self.neural_network["num_workers"], self.neural_network["dataloader_mode"]
        )
        task_params = {
            "f": learn_torch_model,
            "database": database,
            "model_builder": model_builder,
            "model_hyperparameters": model_hyperparameters,
            "dataloader_params": dataloader_params,
            "working_dir": working_directory,
        }
        return task_params

    def get_learning_i2_task_parameters(
        self,
        vector_file: str,
        output_model: str,
        model_name: str,
        seed: int,
    ) -> dict:
        """Return task parameters to learn scikit model"""
        data_field = self.i2_const.re_encoding_label_name
        use_scikitlearn = (
            rcf.ReadConfigFile(self.cfg).get_param(
                "scikit_models_parameters", "model_type"
            )
            is not None
        )
        if use_scikitlearn is True:
            scikit_parameters = SciKitModelParams(
                DBInfo(data_field, vector_file),
                self.model_type,
                output_model,
                self.sk_model_parameters.get("keyword_arguments", None),
            )
            cross_validation = CrossValidationParameters(
                grouped=rcf.ReadConfigFile(self.cfg).get_param(
                    "scikit_models_parameters", "cross_validation_grouped"
                ),
                folds=rcf.ReadConfigFile(self.cfg).get_param(
                    "scikit_models_parameters", "cross_validation_folds"
                ),
                parameters=cast_config_cv_parameters(
                    rcf.ReadConfigFile(self.cfg).get_param(
                        "scikit_models_parameters", "cross_validation_parameters"
                    )
                ),
            )
            task_params = {
                "f": learn_scikitlearn_model,
                "model_parameters": scikit_parameters,
                "cross_validation_parameters": cross_validation,
                "apply_standardization": rcf.ReadConfigFile(self.cfg).get_param(
                    "scikit_models_parameters", "standardization"
                ),
                "available_ram": 1024.0 * get_ram(self.resources.ram),
            }
        else:
            training_parameters = {
                "io.vd": (
                    vector_file if isinstance(vector_file, list) else [vector_file]
                ),
                "classifier": self.classifier,
                "io.out": output_model,
                "cfield": data_field,
            }
            if "svm" in self.classifier.lower():
                learning_stats_file = str(
                    Path(self.output_path)
                    / "stats"
                    / f"Model_{model_name}_seed_{seed}.xml"
                )
                training_parameters["io.stats"] = learning_stats_file
                training_parameters["classifier.libsvm.prob"] = True
            task_params = {
                "f": learn_otb_model,
                "train_params": training_parameters,
                "classifier_options": rcf.ReadConfigFile(self.cfg).get_param(
                    "arg_train", "otb_classifier_options"
                ),
                "region_field": rcf.ReadConfigFile(self.cfg).get_param(
                    "chain", "region_field"
                ),
                "ground_truth": str(
                    Path(self.output_path) / self.i2_const.re_encoding_label_file
                ),
            }
        return task_params

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Learn model"
        return description
