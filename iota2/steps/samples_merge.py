#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the samples merge task"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import samples_merge
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class SamplesMerge(iota2_step.Step):
    """Class for launching the samples merge step"""

    resources_block_name = "samplesMerge"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        field_region = parameters.get_param("chain", "region_field")
        nb_runs = parameters.get_param("arg_train", "runs")
        sampling_validation = parameters.get_param("arg_train", "sampling_validation")
        for model_name, model_meta in self.spatial_models_distribution.items():
            for seed in range(nb_runs):
                target_model = f"model_{model_name}_seed_{seed}"
                task = self.I2Task(
                    task_name=f"merge_{target_model}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": samples_merge.samples_merge,
                        "region_tiles_seed": (model_name, model_meta["tiles"], seed),
                        "output_path": output_path,
                        "region_field": field_region,
                        "working_directory": working_directory,
                        "sampling_validation": sampling_validation,
                    },
                    task_resources=self.get_resources(),
                )

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=target_model,
                    task_dep_dico={},
                )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "merge samples by models"
        return description
