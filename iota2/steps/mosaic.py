#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""call gdal merge on tiles parts and perform color index, reencoding... """

import logging
from pathlib import Path
from typing import Literal

from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.steps import iota2_step
from iota2.typings.i2_types import MosaicInputPaths, MosaicParameters
from iota2.validation import classification_shaping as CS
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class Mosaic(iota2_step.Step):
    """Step to merge all classifications in one."""

    resources_block_name = "classifShaping"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
        mode: Literal["classif", "regression"] = "classif",
    ):
        """
        mode "regression" does not do color indexing and probability
        it does not do label reencoding
        it uses float values, not integers
        """
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        output_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path")
        self.runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")

        reference_data = rcf.ReadConfigFile(self.cfg).get_param("chain", "ground_truth")
        reference_data_field = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "data_field"
        )

        # parameters that differ between classif and regression mode
        if mode == "classif":
            data_field = self.i2_const.re_encoding_label_name
            old_label_to_new = get_re_encoding_labels_dic(
                reference_data, reference_data_field
            )
        else:  # regression
            data_field = reference_data_field
            old_label_to_new = None

        # parameters involved in boundary fusion
        enable_boundary_fusion = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_classification", "enable_boundary_fusion"
        )

        proba_map_flag = bool(
            rcf.ReadConfigFile(self.cfg).get_param(
                "arg_classification", "enable_probability_map"
            )
            and rcf.ReadConfigFile(self.cfg).get_param(
                "arg_classification", "generate_final_probability_map"
            )
        )
        available_ram = get_ram(self.get_resources().ram)

        # Get all file paths used in mosaic
        self.mosaic_input_files_list: list[MosaicInputPaths] = []
        self.get_mosaic_paths(enable_boundary_fusion, output_path)
        mosaic_input_parameters = MosaicParameters(
            runs=self.runs,
            classif_mode=rcf.ReadConfigFile(self.cfg).get_param(
                "arg_classification", "classif_mode"
            ),
            data_field=data_field,
            labels_conversion=old_label_to_new,
            tiles_from_cfg=rcf.ReadConfigFile(self.cfg).get_param("chain", "list_tile"),
            mode=mode,
            flags=MosaicParameters.Flags(
                proba_map_flag=proba_map_flag,
                output_statistics=rcf.ReadConfigFile(self.cfg).get_param(
                    "chain", "output_statistics"
                ),
            ),
        )
        task = self.I2Task(
            task_name="mosaic",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": CS.provide_input_to_mosaic,
                "available_ram": available_ram,
                "mosaic_input_paths_list": self.mosaic_input_files_list,
                "mosaic_parameters": mosaic_input_parameters,
                "working_directory": working_directory,
                "spatial_resolution": rcf.ReadConfigFile(self.cfg).get_param(
                    "chain", "spatial_resolution"
                ),
            },
            task_resources=self.get_resources(),
        )

        dependencies = self.get_dependencies(enable_boundary_fusion)
        self.add_task_to_i2_processing_graph(
            task,
            task_group="mosaic",
            task_sub_group="mosaic",
            task_dep_dico=dependencies,
        )

    def get_mosaic_paths(
        self,
        enable_boundary_fusion: bool,
        output_path: str,
    ) -> None:
        """
        Create the list of MosaicInputPaths containing the required paths

        Parameters
        ----------
        enable_boundary_fusion: bool
            Flag to enable the boundary fusion
        output_path: str
            Iota2 output directory

        Returns
        -------
        None
        """
        comparison_mode = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_classification", "boundary_comparison_mode"
        )

        if comparison_mode:
            classif_path_list = [
                str(Path(output_path) / "classif"),
                str(Path(output_path) / "boundary"),
            ]
            final_folder_list = [
                str(Path(output_path) / "final" / "standard"),
                str(Path(output_path) / "final" / "boundary"),
            ]
            final_tmp_folder_list = [
                str(Path(output_path) / "final" / "standard" / "TMP"),
                str(Path(output_path) / "final" / "boundary" / "TMP"),
            ]
        elif enable_boundary_fusion:
            classif_path_list = [str(Path(output_path) / "boundary")]
            final_folder_list = [str(Path(output_path) / "final")]
            final_tmp_folder_list = [str(Path(output_path) / "final" / "TMP")]
        else:
            classif_path_list = [str(Path(output_path) / "classif")]
            final_folder_list = [str(Path(output_path) / "final")]
            final_tmp_folder_list = [str(Path(output_path) / "final" / "TMP")]

        region_shape = rcf.ReadConfigFile(self.cfg).get_param("chain", "region_path")
        color_table = rcf.ReadConfigFile(self.cfg).get_param("chain", "color_table")
        self.mosaic_input_files_list = [
            MosaicInputPaths(
                path_classif=classif_path,
                validation_input_folder=str(Path(output_path) / "dataAppVal"),
                final_folder=final_folder,
                nb_view_folder=str(Path(output_path) / "features"),
                final_tmp_folder=final_tmp_folder,
                region_shape=region_shape,
                color_path=color_table,
            )
            for classif_path, final_folder, final_tmp_folder in zip(
                classif_path_list, final_folder_list, final_tmp_folder_list
            )
        ]

    def get_dependencies(self, enable_boundary_fusion: bool) -> dict[str, list[str]]:
        """
        Create and fill the step's dependencies dictionary

        Parameters
        ----------
        enable_boundary_fusion: bool
            Flag to enable boundary fusion (which adds more dependencies)

        Returns
        -------
        dependencies: dict[str, list[str]]
            Dictionary of the step's dependencies
        """
        dependencies: dict[str, list[str]] = {}
        for (
            model_name,
            model_meta,
        ) in self.spatial_models_distribution_no_sub_splits_classify.items():
            for seed in range(self.runs):
                model_subdivisions = model_meta["nb_sub_model"]
                dep_group = "tile_tasks_model"
                if model_subdivisions == 1:
                    dep_group = "tile_tasks_model_mode"
                if dep_group not in dependencies:
                    dependencies[dep_group] = []
                for tile in model_meta["tiles"]:
                    dependencies[dep_group].append(f"{tile}_{model_name}_{seed}")

        if enable_boundary_fusion:
            dep = []
            for seed in range(self.runs):
                for tile in self.tiles:
                    dep.append(f"{tile}_{seed}")
            dependencies = {"tile_tasks_model": dep}
        return dependencies

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "Mosaic"
        return description
