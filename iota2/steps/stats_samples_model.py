#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step for generating samples statistics by models"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import samples_stat
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class StatsSamplesModel(iota2_step.Step):
    """Class for launching samples stats by models step"""

    resources_block_name = "samplesStatistics"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
        data_field: str | None = None,
    ):
        """
        data_field
            the samples are split according to this column
        """
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        data_field = data_field or self.i2_const.re_encoding_label_name
        nb_runs = parameters.get_param("arg_train", "runs")
        sampling_validation = parameters.get_param("arg_train", "sampling_validation")
        for model_name, model_meta in self.spatial_models_distribution.items():
            for seed in range(nb_runs):
                for tile in model_meta["tiles"]:
                    target_model = f"{model_name}_S_{seed}_T_{tile}"
                    task = self.I2Task(
                        task_name=f"stats_{target_model}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f": samples_stat.samples_stats,
                            "region_seed_tile": (model_name, str(seed), tile),
                            "iota2_directory": output_path,
                            "data_field": data_field,
                            "working_directory": working_directory,
                            "sampling_validation": sampling_validation,
                        },
                        task_resources=self.get_resources(),
                    )
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="tile_tasks_model",
                        task_sub_group=f"{tile}_{model_name}_{seed}",
                        task_dep_dico={
                            "region_tasks": [f"model_{model_name}_seed_{seed}"]
                        },
                    )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Generate samples statistics by models"
        return description
