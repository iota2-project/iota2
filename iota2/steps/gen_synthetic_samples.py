# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the synthetic samples generation task"""
import logging
from pathlib import Path

from iota2.common import i2_constants
from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import data_augmentation
from iota2.steps import iota2_step
from iota2.typings.i2_types import DBInfo, LabelsConversionDict
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_constants.Iota2Constants()


class GenSyntheticSamples(iota2_step.Step):
    """Class for launching the synthetic samples generation step"""

    resources_block_name = "samplesAugmentation"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        transfert_samples: bool,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.parameters = rcf.ReadConfigFile(self.cfg)
        output_path = self.parameters.get_param("chain", "output_path")
        ground_truth = str(Path(output_path) / self.i2_const.re_encoding_label_file)
        ext = self.parameters.get_param("arg_train", "learning_samples_extension")
        sample_augmentation = dict(
            self.parameters.get_param("arg_train", "sample_augmentation")
        )

        sample_augmentation["target_models"] = list(
            sample_augmentation["target_models"]
        )

        nb_runs = self.parameters.get_param("arg_train", "runs")
        random_seed = self.parameters.get_param("arg_train", "random_seed")

        neural_network = self.parameters.get_param(
            "arg_train", "deep_learning_parameters"
        )
        self.user_labels_to_i2_labels: LabelsConversionDict | None = None

        mode = self.get_labels_conversion()

        for model_name, _ in self.spatial_models_distribution.items():
            for seed in range(nb_runs):
                samples_file = str(
                    Path(output_path)
                    / "learningSamples"
                    / f"Samples_region_{model_name}_seed{seed}_learn.{ext}",
                )
                target_model = f"model_{model_name}_seed_{seed}"
                task = self.I2Task(
                    task_name=f"data_augmentation_{target_model}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": (
                            data_augmentation.data_augmentation_synthetic_dl
                            if "dl_name" in neural_network
                            else data_augmentation.data_augmentation_synthetic
                        ),
                        "database": DBInfo(
                            self.parameters.get_param("chain", "data_field").lower(),
                            samples_file,
                            self.user_labels_to_i2_labels,
                        ),
                        "ground_truth": ground_truth,
                        "strategies": {
                            **sample_augmentation,
                            **{
                                "class_field": (
                                    I2_CONST.re_encoding_label_name
                                    if mode == "classif"
                                    else I2_CONST.fake_class
                                ),
                                "random_seed": random_seed,
                            },
                        },
                        "working_directory": working_directory,
                    },
                    task_resources=self.get_resources(),
                )
                dep_values: list[int] | list[str]
                if transfert_samples:
                    dep_key = "seed_tasks"
                    dep_values = [seed]
                else:
                    dep_key = "region_tasks"
                    dep_values = [target_model]

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=f"{target_model}",
                    task_dep_dico={dep_key: dep_values},
                )

    def get_labels_conversion(self) -> str:
        """
        Retrieve the labels converting dictionary if needed and return mode ("classif" or
        "regression").
        """
        user_ground_truth = self.parameters.get_param("chain", "ground_truth")
        user_data_field = self.parameters.get_param("chain", "data_field")
        if "I2Classification" in self.parameters.get_param(
            "builders", "builders_class_name"
        ):
            self.user_labels_to_i2_labels = get_re_encoding_labels_dic(
                user_ground_truth, user_data_field
            )
            mode = "classif"
        else:
            mode = "regression"
        return mode

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Generate synthetic samples"
        return description
