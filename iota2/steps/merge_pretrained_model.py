#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step class for merging chunk by region and tile."""
import logging
from pathlib import Path

from iota2.classification import external_classification as extcl
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class MergePreTrainedModelByTiles(iota2_step.Step):
    """Merge the chunk to reconstruct a tile according a region."""

    resources_block_name = "mergeMapsByTiles"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        """Launch the merge of subregion to reconstruct the tile."""
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = working_directory
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        # to allow writing maps using bandmath
        runs = parameters.get_param("arg_train", "runs")
        number_of_chunks = parameters.get_param(
            "python_data_managing", "number_of_chunks"
        )
        mode = parameters.get_param("pretrained_model", "mode")
        for region, model_meta in self.spatial_models_distribution_classify.items():
            for tile in model_meta["tiles"]:
                for seed in range(runs):
                    task = self.I2Task(
                        task_name=f"merge_maps_by_tiles_{tile}_{region}_{seed}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f": extcl.merge_external_classification_by_tiles,
                            "path_to_classif": str(Path(output_path) / "classif"),
                            "output_path": str(
                                Path(output_path) / "classif" / "full_pred"
                            ),
                            "tile": tile,
                            "region": region,
                            "seed": seed,
                            "res": parameters.get_param("chain", "spatial_resolution"),
                            "mode": mode,
                        },
                        task_resources=self.get_resources(),
                    )
                    dep = []
                    for cpt in range(number_of_chunks):
                        dep.append(f"ext_classif_{tile}_{region}_{seed}" f"_{cpt}")

                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="merge_maps_by_tiles_and_regions",
                        task_sub_group=f"merge_maps_by_tiles {tile}"
                        f" {region} {seed}",
                        task_dep_dico={"tile_tasks_model_mode": dep},
                    )

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "Merge external classification per tile"
        return description
