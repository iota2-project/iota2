#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Produce the boundary fusion."""
import logging
from pathlib import Path

from iota2.classification.image_classifier import get_class_by_models_from_i2_learn
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.typings.i2_types import BoundariesProbaFusionParameters
from iota2.validation.boundary_tile_fusion import proba_fusion_at_boundaries

LOGGER = logging.getLogger("distributed.worker")


class ProbBoundaryFusion(iota2_step.Step):
    """Step to compute the fusion at boundary using probabilities."""

    resources_block_name = "compute_boundary_fusion"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
    ):
        """Initialize the step."""
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # parameters
        parameters = rcf.ReadConfigFile(self.cfg)
        runs = parameters.get_param("arg_train", "runs")
        output_path = parameters.get_param("chain", "output_path")
        number_of_chunks = parameters.get_param(
            "python_data_managing", "number_of_chunks"
        )
        region_vec = parameters.get_param("chain", "region_path")
        region_field = (parameters.get_param("chain", "region_field")).lower()
        data_field = self.i2_const.re_encoding_label_name
        class_by_models = get_class_by_models_from_i2_learn(
            str(Path(output_path) / "dataAppVal"), data_field, region_field
        )
        all_seeds_class = []
        for _, region_class in class_by_models.items():
            all_seeds_class += region_class
        all_seeds_class = sorted(list(set(all_seeds_class)))
        dict_tile_model = self.map_tile_to_model()
        for seed in range(runs):
            for tile in self.tiles:

                for targeted_chunk in range(number_of_chunks):
                    proba_fusion_parameters = BoundariesProbaFusionParameters(
                        tile=tile,
                        seed=seed,
                        target_chunk=targeted_chunk,
                        number_of_chunks=number_of_chunks,
                        region_field=region_field,
                        nb_class=len(all_seeds_class),
                        generate_final_probability_map=parameters.get_param(
                            "arg_classification", "generate_final_probability_map"
                        ),
                    )
                    task = self.I2Task(
                        task_name=f"fuse_proba_{tile}_{targeted_chunk}_{seed}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f": proba_fusion_at_boundaries,
                            "distance_map_folder": str(
                                Path(output_path) / "features" / tile / "tmp"
                            ),
                            "probabilities_map_folder": str(
                                Path(output_path) / "classif"
                            ),
                            "boundary_folder_out": str(Path(output_path) / "boundary"),
                            "original_region_file": region_vec,
                            "proba_fusion_params": proba_fusion_parameters,
                        },
                        task_resources=self.get_resources(),
                    )

                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="tile_tasks_model",
                        task_sub_group=f"{tile}_{seed}_{targeted_chunk}",
                        task_dep_dico={
                            "tile_tasks_model_mode": [
                                f"{tile}_{model_name}_{seed}"
                                for model_name in dict_tile_model[tile]
                            ]
                        },
                    )

    def map_tile_to_model(self) -> dict[str, list[str]]:
        """
        Create a dictionary mapping tile names to models

        Returns
        -------
        The dictionary built
        """
        dict_tile_model: dict[str, list[str]] = {}
        for (
            model_name,
            model_meta,
        ) in self.spatial_models_distribution_no_sub_splits_classify.items():
            for tile in model_meta["tiles"]:
                if tile in dict_tile_model:
                    dict_tile_model[tile].append(model_name)
                else:
                    dict_tile_model[tile] = [model_name]

        return dict_tile_model

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "Fusion into boundary area"
        return description
