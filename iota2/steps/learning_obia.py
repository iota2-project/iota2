#!/usr/bin/python

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the OBIA learning task"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.segmentation import prepare_segmentation_obia as pso
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class ObiaLearning(iota2_step.Step):
    """Class for launching the OBIA learning step"""

    resources_block_name = "obiaLearning"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        nb_runs = parameters.get_param("arg_train", "runs")
        options = parameters.get_param("arg_train", "otb_classifier_options")
        classifier = parameters.get_param("arg_train", "classifier")
        for model_name, model_meta in self.spatial_models_distribution.items():
            for seed in range(nb_runs):

                target_model = f"model_{model_name}_seed_{seed}"

                task = self.I2Task(
                    task_name=f"{target_model}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": pso.learning_models_by_region,
                        "iota2_directory": output_path,
                        "region": model_name,
                        "seed": seed,
                        "data_field": self.i2_const.re_encoding_label_name,
                        "classifier_name": classifier,
                        "classifier_options": options,
                        "enable_stats": False,
                    },
                    task_resources=self.get_resources(),
                )
                dep = []
                for tile in model_meta["tiles"]:
                    dep.append(f"zonal_stats_learn_{tile}_seed_{seed}")
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=target_model,
                    task_dep_dico={"region_tasks": dep},
                )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "learn model for obia"
        return description
