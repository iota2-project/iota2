#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Merge final classifications step"""
import logging
from pathlib import Path

from iota2.classification import merge_final_classifications as mergeCl
from iota2.classification.merge_final_classifications import compute_fusion_options
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.typings.i2_types import LabelsConversionDict
from iota2.vector_tools.vector_functions import get_reverse_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class MergeSeedClassifications(iota2_step.Step):
    """Class for merging final classifications"""

    resources_block_name = "merge_final_classifications"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.parameters = rcf.ReadConfigFile(self.cfg)
        output_path = self.parameters.get_param("chain", "output_path")
        data_field = self.parameters.get_param("chain", "data_field")
        nomenclature = self.parameters.get_param("chain", "nomenclature_path")
        color_table = self.parameters.get_param("chain", "color_table")
        indecided_label = self.parameters.get_param(
            "arg_classification", "merge_final_classifications_indecidedlabel"
        )
        dempstershafer_mob = self.parameters.get_param(
            "arg_classification", "dempstershafer_mob"
        )
        keep_runs_results = self.parameters.get_param(
            "arg_classification", "keep_runs_results"
        )
        ground_truth = self.parameters.get_param("chain", "ground_truth")

        merge_final_classifications_method = rcf.ReadConfigFile(cfg).get_param(
            "arg_classification", "merge_final_classifications_method"
        )
        i2_labels_to_user_labels: LabelsConversionDict | None = (
            get_reverse_encoding_labels_dic(ground_truth, data_field)
        )
        assert i2_labels_to_user_labels
        if all(
            isinstance(user_label, int)
            for user_label in i2_labels_to_user_labels.values()
        ):
            i2_labels_to_user_labels = None

        runs = self.parameters.get_param("arg_train", "runs")
        final_i2_path = str(Path(output_path) / "final")

        final_classifications = [
            str(Path(output_path) / "final" / f"Classif_Seed_{run}.tif")
            for run in range(runs)
        ]

        # fusion parameters
        otb_fusion_parameters = compute_fusion_options(
            final_classifications=final_classifications,
            method=merge_final_classifications_method,
            indecidedlabel=indecided_label,
            dempstershafer_mob=dempstershafer_mob,
        )

        # confusion options

        otb_confusion_options = {
            "out": str(
                Path(final_i2_path)
                / "merge_final_classifications"
                / "confusion_mat_maj_vote.csv"
            ),
            "ref": "vector",
            "ref.vector.nodata": "0",
            "ref.vector.field": data_field.lower(),
            "nodatalabel": "0",
            "ram": "5000",
        }

        if i2_labels_to_user_labels:
            i2_labels_to_user_labels[indecided_label] = indecided_label
        task = self.I2Task(
            task_name="final_report_merge",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": mergeCl.merge_final_classifications,
                "otb_fusion_options": otb_fusion_parameters,
                "otb_confusion_options": otb_confusion_options,
                "iota2_dir": output_path,
                "nom_path": nomenclature,
                "color_file": color_table,
                "keep_runs_results": keep_runs_results,
                "validation_shape": self.get_validation_shape(ground_truth),
                "labels_raster_table": i2_labels_to_user_labels,
                "working_directory": working_directory,
            },
            task_resources=self.get_resources(),
        )
        self.add_task_to_i2_processing_graph(
            task,
            task_group="merge_final_classifications",
            task_sub_group="merge_final_classifications",
            task_dep_dico={"final_report": ["final_report"]},
        )

    def get_validation_shape(self, ground_truth: str) -> str | None:
        """
        Get the right validation shape

        Parameters
        ----------
        ground_truth:
            Path to ground truth file

        Returns
        -------
        None
        """
        fusion_cla_all_samples_val = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_classification", "fusionofclassification_all_samples_validation"
        )
        validation_shape = None
        if fusion_cla_all_samples_val is True:
            validation_shape = ground_truth
        return validation_shape

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Merge final classifications"
        return description
