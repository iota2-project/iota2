#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the samples extraction task"""
import logging
from pathlib import Path
from typing import Literal

from iota2.common.custom_numpy_features import (
    ChunkParameters,
    ComputeCustomFeaturesArgs,
    CustomFeaturesConfiguration,
    CustomNumpyFeatures,
    exogenous_data_tile,
)
from iota2.common.raster_utils import ChunkConfig, get_raster_chunks_boundaries
from iota2.common.utils import TaskConfig
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.sampling import vector_sampler as vs
from iota2.sampling.vector_sampler import SampleFlagsParameters
from iota2.steps import iota2_step
from iota2.typings.i2_types import DBInfo
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class SamplesExtraction(iota2_step.Step):
    """Class for launching the samples extraction step"""

    resources_block_name = "vectorSampler"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
        data_field: str | None = None,
        mode: Literal["classif", "regression"] = "classif",
    ):
        """
        data_field
            the samples are extracted according to this column
        mode
            "classif" or "regression"
            in regression mode, there is no label conversion
        """
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # set default value if no data_field is provided
        data_field = data_field or self.i2_const.re_encoding_label_name

        # step variables
        self.parameters = rcf.ReadConfigFile(self.cfg)
        self.output_path = self.parameters.get_param("chain", "output_path")
        self.ram_extraction = int(1024 * get_ram(self.resources.ram))
        self.learning_samples_extension = self.parameters.get_param(
            "arg_train", "learning_samples_extension"
        )
        self.custom_features = self.parameters.get_param(
            "external_features", "external_features_flag"
        )
        if self.custom_features:
            chunk_size_mode = self.parameters.get_param(
                "python_data_managing", "chunk_size_mode"
            )
            self.chunk_config = ChunkConfig(
                chunk_size_mode=chunk_size_mode,
                number_of_chunks=self.parameters.get_param(
                    "python_data_managing", "number_of_chunks"
                ),
                chunk_size_x=self.parameters.get_param(
                    "python_data_managing", "chunk_size_x"
                ),
                chunk_size_y=self.parameters.get_param(
                    "python_data_managing", "chunk_size_y"
                ),
                padding_size_x=self.parameters.get_param(
                    "python_data_managing", "padding_size_x"
                ),
                padding_size_y=self.parameters.get_param(
                    "python_data_managing", "padding_size_y"
                ),
            )
            number_of_chunks = self.chunk_config.number_of_chunks

        # implement tests for check if custom features are well provided
        # so the chain failed during step init

        user_labels_to_i2_labels = self.set_labels_conversion(mode)

        enable_gap, enable_raw = self.parameters.get_param(
            "python_data_managing", "data_mode_access"
        )

        self.sensors_dates = rcf.Iota2Parameters(
            self.parameters
        ).get_available_sensors_dates()
        if "Sentinel1" in self.sensors_dates:
            s1_dates = rcf.Iota2Parameters(self.parameters).get_sentinel1_input_dates()
            self.sensors_dates = {**self.sensors_dates, **s1_dates}
            self.sensors_dates.pop("Sentinel1", None)

        for tile in self.tiles:
            if self.custom_features and chunk_size_mode == "user_fixed":
                rois, _ = get_raster_chunks_boundaries(
                    Path(self.output_path)
                    / "features"
                    / tile
                    / "tmp"
                    / "MaskCommunSL.tif",
                    self.chunk_config,
                )
                number_of_chunks = len(rois)
            task_name = f"extraction_{tile}"

            parameters_dic = {
                "f": vs.generate_samples,
                "database": DBInfo(
                    data_field,
                    str(Path(self.output_path) / "formattingVectors" / f"{tile}.shp"),
                    region_field=self.parameters.get_param("chain", "region_field"),
                    mapping_table=user_labels_to_i2_labels,
                ),
                "learning_samples_extension": self.learning_samples_extension,
                "path_wd": working_directory,
                "output_path": self.parameters.get_param("chain", "output_path"),
                "runs": self.parameters.get_param("arg_train", "runs"),
                "sensors_parameters": rcf.Iota2Parameters(
                    self.parameters
                ).get_sensors_parameters(tile),
                "sample_flag": SampleFlagsParameters(
                    sampling_validation=self.parameters.get_param(
                        "arg_train", "sampling_validation"
                    ),
                    features_from_raw_dates=self.parameters.get_param(
                        "arg_train", "features_from_raw_dates"
                    ),
                    force_standard_labels=self.parameters.get_param(
                        "arg_train", "force_standard_labels"
                    ),
                ),
                "task_cfg": TaskConfig(ram=self.ram_extraction),
            }
            if self.custom_features:
                assert number_of_chunks is not None
                for target_chunk in range(number_of_chunks):

                    exogenous_data = exogenous_data_tile(
                        self.parameters.get_param(
                            "external_features", "exogeneous_data"
                        ),
                        tile,
                    )
                    parameters_dic["custom_features"] = ComputeCustomFeaturesArgs(
                        functions_builder=CustomNumpyFeatures(
                            sensors_params=rcf.Iota2Parameters(
                                self.parameters
                            ).get_sensors_parameters(tile),
                            module_name=self.parameters.get_param(
                                "external_features", "module"
                            ),
                            list_functions=self.parameters.get_param(
                                "external_features", "functions"
                            ),
                            configuration=CustomFeaturesConfiguration(
                                concat_mode=self.parameters.get_param(
                                    "external_features", "concat_mode"
                                ),
                                fill_missing_dates=self.parameters.get_param(
                                    "python_data_managing", "fill_missing_dates"
                                ),
                                enabled_raw=enable_raw,
                                enabled_gap=enable_gap,
                            ),
                            all_dates_dict=self.sensors_dates,
                            exogeneous_data_file=exogenous_data,
                        ),
                        chunk_params=ChunkParameters(self.chunk_config, target_chunk),
                        exogeneous_data=exogenous_data,
                        concatenate_features=self.parameters.get_param(
                            "external_features", "concat_mode"
                        ),
                    )
                    task_name_chunk = f"{task_name}_chunk_{target_chunk}"
                    task = self.I2Task(
                        task_name=task_name_chunk,
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters=parameters_dic,
                        task_resources=self.get_resources(),
                    )
                    if self.chunks_intersections[tile][target_chunk]:
                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="tile_tasks",
                            task_sub_group=f"{tile}_chunk_{target_chunk}",
                            task_dep_dico={"tile_tasks": [tile]},
                        )
            else:
                task = self.I2Task(
                    task_name=task_name,
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters=parameters_dic,
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks",
                    task_sub_group=tile,
                    task_dep_dico={"tile_tasks": [tile]},
                )

    def set_labels_conversion(
        self, mode: Literal["classif", "regression"]
    ) -> dict[str | int, int] | None:
        """
        Retrieve the labels converting dictionary if needed (in classif mode)
        """
        if mode == "classif":
            user_ground_truth = self.parameters.get_param("chain", "ground_truth")
            user_data_field = self.parameters.get_param("chain", "data_field")
            user_labels_to_i2_labels = get_re_encoding_labels_dic(
                user_ground_truth, user_data_field
            )
        # No conversion in regression mode
        else:
            user_labels_to_i2_labels = None
        # false positive "no-any-return" from mypy
        return user_labels_to_i2_labels  # type: ignore

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Extract pixels values by tiles"
        return description
