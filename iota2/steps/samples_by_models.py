#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step for merging samples dedicated to the same model """
import logging
from pathlib import Path

import iota2.common.raster_utils as ru
from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import vector_samples_merge as VSM
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class SamplesByModels(iota2_step.Step):
    """Class for launching the samples merge by model step"""

    resources_block_name = "mergeSample"

    def __init__(self, cfg: str, cfg_resources_file: Path | None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        self.output_path = parameters.get_param("chain", "output_path")
        data_field = parameters.get_param("chain", "data_field")
        self.nb_runs = parameters.get_param("arg_train", "runs")

        self.custom_features = parameters.get_param(
            "external_features", "external_features_flag"
        )
        if self.custom_features:
            self.chunk_size_mode = parameters.get_param(
                "python_data_managing", "chunk_size_mode"
            )
            self.number_of_chunks = parameters.get_param(
                "python_data_managing", "number_of_chunks"
            )
            self.chunk_config = ru.ChunkConfig(
                chunk_size_mode=self.chunk_size_mode,
                number_of_chunks=self.number_of_chunks,
                chunk_size_x=parameters.get_param(
                    "python_data_managing", "chunk_size_x"
                ),
                chunk_size_y=parameters.get_param(
                    "python_data_managing", "chunk_size_y"
                ),
                padding_size_x=parameters.get_param(
                    "python_data_managing", "padding_size_x"
                ),
                padding_size_y=parameters.get_param(
                    "python_data_managing", "padding_size_y"
                ),
            )

        neural_network = parameters.get_param("arg_train", "deep_learning_parameters")
        ext = parameters.get_param("arg_train", "learning_samples_extension")
        dl_flag = "dl_name" in neural_network
        data_aug_flag = parameters.get_param("arg_train", "sample_augmentation")[
            "activate"
        ]
        if "I2Classification" in parameters.get_param(
            "builders", "builders_class_name"
        ):
            mode = "classif"
        else:
            mode = "regression"
        dico_model_samples_files = self.expected_files_to_merge(ext)
        for model_name, model_meta in self.spatial_models_distribution.items():
            for seed in range(self.nb_runs):
                target_model = f"model_{model_name}_seed_{seed}"
                task = self.I2Task(
                    task_name=f"merge_{target_model}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": VSM.vector_samples_merge,
                        "vector_list": dico_model_samples_files[target_model],
                        "output_path": self.output_path,
                        "user_label_field": data_field.lower(),
                        "deep_learning_db": dl_flag,
                        "data_aug": data_aug_flag,
                        "mode": mode,
                    },
                    task_resources=self.get_resources(),
                )
                dependencies = list(model_meta["tiles"])
                if self.custom_features:
                    dependencies = self.generate_chunks_dep(model_meta)
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=f"{target_model}",
                    task_dep_dico={"tile_tasks": dependencies},
                )

    def generate_chunks_dep(self, model_meta: dict) -> list[str]:
        """get dependencies if data is chunked."""
        dependencies = []
        for tile in model_meta["tiles"]:
            if self.chunk_size_mode == "user_fixed":
                rois, _ = ru.get_raster_chunks_boundaries(
                    Path(self.output_path)
                    / "features"
                    / tile
                    / "tmp"
                    / "MaskCommunSL.tif",
                    self.chunk_config,
                )
                self.number_of_chunks = len(rois)
            for chunk_num in range(self.number_of_chunks):
                if self.chunks_intersections[tile][chunk_num]:
                    dependencies.append(f"{tile}_chunk_{chunk_num}")
        return dependencies

    def expected_files_to_merge(self, ext: str) -> dict[str, list[str]]:
        """Return a list of all expected paths to files to merge"""
        files_to_merge = {}
        for seed in range(self.nb_runs):
            for model_name, model_meta in self.spatial_models_distribution.items():
                file_list = []
                for tile in model_meta["tiles"]:
                    file_name = (
                        f"{tile}_region_{model_name}_seed{seed}_Samples_learn.{ext}"
                    )
                    if self.custom_features:
                        if self.chunk_size_mode == "user_fixed":
                            rois, _ = ru.get_raster_chunks_boundaries(
                                Path(self.output_path)
                                / "features"
                                / tile
                                / "tmp"
                                / "MaskCommunSL.tif",
                                self.chunk_config,
                            )
                            self.number_of_chunks = len(rois)
                        for chunk in range(self.number_of_chunks):
                            file_name = (
                                f"{tile}_region_{model_name}_seed"
                                f"{seed}_{chunk}_Samples_learn.{ext}"
                            )
                            file_list.append(
                                str(
                                    Path(self.output_path)
                                    / "learningSamples"
                                    / file_name
                                )
                            )
                    else:
                        file_list.append(
                            str(Path(self.output_path) / "learningSamples" / file_name)
                        )
                files_to_merge[f"model_{model_name}_seed_{seed}"] = file_list
        return files_to_merge

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Merge samples dedicated to the same model"
        return description
