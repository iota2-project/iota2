#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step to write features by chunk"""
import logging
from pathlib import Path

import iota2.common.raster_utils as ru
import iota2.common.write_features_map as wfm
from iota2.common.custom_numpy_features import exogenous_data_tile
from iota2.common.file_utils import file_search_and
from iota2.common.generate_features import FeaturesMapParameters
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class WriteFeaturesMap(iota2_step.Step):
    """Class for launching write features map step"""

    resources_block_name = "writeMaps"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        # read config file to init custom features and check validity
        sensors_dates = rcf.Iota2Parameters(parameters).get_available_sensors_dates()
        if "Sentinel1" in sensors_dates:
            s1_dates = rcf.Iota2Parameters(parameters).get_sentinel1_input_dates()
            sensors_dates = {**sensors_dates, **s1_dates}
            sensors_dates.pop("Sentinel1", None)

        chunk_size_mode = parameters.get_param(
            "python_data_managing", "chunk_size_mode"
        )
        number_of_chunks = parameters.get_param(
            "python_data_managing", "number_of_chunks"
        )
        chunk_config = ru.ChunkConfig(
            chunk_size_mode=chunk_size_mode,
            number_of_chunks=number_of_chunks,
            chunk_size_x=parameters.get_param("python_data_managing", "chunk_size_x"),
            chunk_size_y=parameters.get_param("python_data_managing", "chunk_size_y"),
            padding_size_x=parameters.get_param(
                "python_data_managing", "padding_size_x"
            ),
            padding_size_y=parameters.get_param(
                "python_data_managing", "padding_size_y"
            ),
        )
        enabled_gap, enabled_raw = parameters.get_param(
            "python_data_managing", "data_mode_access"
        )
        for tile in self.tiles:
            sensors_params = rcf.Iota2Parameters(parameters).get_sensors_parameters(
                tile
            )
            if chunk_size_mode == "user_fixed":
                rois, _ = ru.get_raster_chunks_boundaries(
                    file_search_and(
                        Path(output_path) / "features" / tile / "tmp",
                        True,
                        "reference.tif",
                    )[0],
                    chunk_config,
                )

                number_of_chunks = len(rois)
            for n_chunk in range(number_of_chunks):
                task = self.I2Task(
                    task_name=f"write_features_{tile}_chunk_{n_chunk}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": wfm.write_features_by_chunk,
                        "features_map_parameters": FeaturesMapParameters(
                            working_directory=working_directory,
                            output_path=parameters.get_param("chain", "output_path"),
                            force_standard_labels=parameters.get_param(
                                "arg_train", "force_standard_labels"
                            ),
                            tile=tile,
                            sensors_parameters=sensors_params,
                        ),
                        "module_path": parameters.get_param(
                            "external_features", "module"
                        ),
                        "list_functions": parameters.get_param(
                            "external_features", "functions"
                        ),
                        "chunk_config": chunk_config,
                        "concat_mode": parameters.get_param(
                            "external_features", "concat_mode"
                        ),
                        "targeted_chunk": n_chunk,
                        "multi_param_cust": {
                            "enable_raw": enabled_raw,
                            "enable_gap": enabled_gap,
                            "fill_missing_dates": parameters.get_param(
                                "python_data_managing", "fill_missing_dates"
                            ),
                            "all_dates_dict": sensors_dates,
                            "mask_valid_data": None,  # if region use it
                            "mask_value": 0,
                            "exogeneous_data": exogenous_data_tile(
                                parameters.get_param(
                                    "external_features", "exogeneous_data"
                                ),
                                tile,
                            ),
                        },
                    },
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="writing maps",
                    task_sub_group=f"writing maps {tile} {n_chunk}",
                    task_dep_dico={"tile_tasks": [tile]},
                )
        # implement tests for check if custom features are well provided
        # so the chain failed during step init

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Write features by chunk"
        return description
