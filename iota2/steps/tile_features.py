#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step to generate features by tile"""
import glob
import logging
import pickle
import re
from dataclasses import dataclass, field
from pathlib import Path
from typing import Any, Literal

import geopandas as gpd
import numpy as np
import rasterio as rio
from osgeo import gdal, gdalconst
from rasterio.coords import BoundingBox
from rasterio.crs import CRS  # pylint: disable=no-name-in-module
from rasterio.warp import calculate_default_transform, reproject
from sensorsio import sentinel1 as sio_s1
from sensorsio import srtm, worldclim
from sensorsio.sentinel2 import Sentinel2 as sio_s2
from shapely import wkt
from shapely.geometry.base import BaseGeometry

from iota2.common.file_utils import (
    delete_all_files,
    file_search_and,
    get_raster_extent,
    get_raster_projection_epsg,
    get_raster_resolution,
)
from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.common.raster_utils import reproj_bbox, str_to_gdal_pix_type
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.sensors.sentinel1 import (
    OrbitPassVal,
    Sentinel1,
    group_by_date,
    sort_s1_orbit_pass,
)
from iota2.sensors.sentinel2 import Sentinel2
from iota2.steps import iota2_step
from iota2.typings.i2_types import ImageCoordinates, PathLike, WKTGeometry
from iota2.vector_tools.vector_functions import get_layer_name

LOGGER = logging.getLogger("distributed.worker")


@dataclass
class S2AnglesFiles:
    """
    Dataclass used to store sentinel 2 angles files
    """

    out_zenith_files: list[Path] = field(default_factory=list)
    out_azimuth_files: list[Path] = field(default_factory=list)
    even_zenith_angles_files: list[Path] = field(default_factory=list)
    odd_zenith_angles_files: list[Path] = field(default_factory=list)
    even_azimuth_angles_files: list[Path] = field(default_factory=list)
    odd_azimuth_angles_files: list[Path] = field(default_factory=list)


@dataclass
class S2AnglesData:
    """
    Dataclass used to store sentinel 2 angles data as numpy arrays
    """

    zenith: np.ndarray | None = None
    azimuth: np.ndarray | None = None
    even_zenith_angles: np.ndarray | None = None
    odd_zenith_angles: np.ndarray | None = None
    even_azimuth_angles: np.ndarray | None = None
    odd_azimuth_angles: np.ndarray | None = None


@dataclass
class S1AnglesGeometricParameters:
    """
    Dataclass containing geometric parameters

    Attributes
    ----------
    tile_crs: int
        CRS of the tile
    tile_geom_wkt: BaseGeometry
        Tile geometry in WKT
    target_epsg: int
        EPSG code for the output rasters
    target_res: float
        Target resolution for the output rasters
    """

    tile_crs: int
    tile_geom_wkt: BaseGeometry
    target_epsg: int
    target_res: float


def write_s1_angles(
    s1_eia: np.ndarray,
    mask_polygon: WKTGeometry,
    out_crs: int,
    out_transform: rio.Affine,
    output_file: Path,
    data_coeff: int,
) -> None:
    """Write sentinel-1 EIA (ellipsoid incidence angle) to a file"""
    with rio.open(
        output_file,
        "w",
        driver="GTiff",
        height=s1_eia.shape[0],
        width=s1_eia.shape[1],
        count=1,
        crs=out_crs,
        transform=out_transform,
        dtype="float32",
    ) as dest:
        dest.write(s1_eia, indexes=1)
    with rio.open(output_file, "r") as file_eia:
        out_image, out_transform = rio.mask.mask(
            file_eia, [mask_polygon], invert=False, all_touched=True
        )
    with rio.open(
        output_file,
        "w",
        height=out_image.shape[1],
        width=out_image.shape[2],
        count=1,
        dtype="uint16",
        crs=out_crs,
        driver="GTiff",
        transform=out_transform,
    ) as file_eia:
        file_eia.write(out_image * data_coeff)


def extract_s1_angles(
    s1_products: list[Path],
    tile_geom: WKTGeometry,
    tile_crs: int,
    rois: list[WKTGeometry],
    out_stack_elips_incidence: Path,
    out_stack_azimuth: Path,
    target_epsg: int,
    target_res: float,
    data_coeff: int = 10,
    ram_limit: int = 128,
) -> None:
    """
    Extract s1 incidence/azimuth angles

    Notes
    -----
    tile_geom and rois must be in targeted projection.
    """
    tile_geom_wkt = wkt.loads(tile_geom)

    product_pass = sort_s1_orbit_pass(s1_products)
    for orbit_pass, product_list in product_pass.items():
        product_pass[orbit_pass] = group_by_date(product_list)

    for mode_index, (orbit_pass, orbit_pass_products) in enumerate(
        product_pass.items()
    ):
        print(
            f"computing orbit pass '{orbit_pass}' "
            f"{mode_index + 1}/{len(product_pass)}"
        )
        incidence_files = get_incidence_files(
            orbit_pass_products,
            out_stack_elips_incidence,
            orbit_pass,
            dict(zip(s1_products, rois)),
            S1AnglesGeometricParameters(
                tile_crs=tile_crs,
                tile_geom_wkt=tile_geom_wkt,
                target_epsg=target_epsg,
                target_res=target_res,
            ),
            data_coeff,
        )

        azimuth_files = get_azimuth_files(
            orbit_pass_products,
            out_stack_azimuth,
            orbit_pass,
            dict(zip(s1_products, rois)),
            S1AnglesGeometricParameters(
                tile_crs=tile_crs,
                tile_geom_wkt=tile_geom_wkt,
                target_epsg=target_epsg,
                target_res=target_res,
            ),
            data_coeff,
        )

        concatenate_app, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {
                "il": incidence_files,
                "out": str(out_stack_elips_incidence).replace(
                    ".tif", f"_{orbit_pass.lower()}.tif"
                ),
                "ram": str(ram_limit),
                "pixType": "uint16",
            },
        )
        concatenate_app.ExecuteAndWriteOutput()
        concatenate_app, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {
                "il": azimuth_files,
                "out": str(out_stack_azimuth).replace(
                    ".tif", f"_{orbit_pass.lower()}.tif"
                ),
                "ram": str(ram_limit),
                "pixType": "uint16",
            },
        )
        concatenate_app.ExecuteAndWriteOutput()
        for file_to_rm in incidence_files + azimuth_files:
            Path.unlink(Path(file_to_rm))


def get_incidence_files(
    orbit_pass_products: list[Path],
    out_stack_elips_incidence: Path,
    orbit_pass: OrbitPassVal,
    products_data_roi: dict,
    geometric_params: S1AnglesGeometricParameters,
    data_coeff: int,
) -> list[str]:
    """
    Compute the S1 incidence angle files for a given orbit pass.

    Parameters
    ----------
    orbit_pass_products: list[Path]
        List of S1 products for the current orbit pass
    out_stack_elips_incidence: Path
        Output path of the stack.
    orbit_pass: OrbitPassVal
        Name of the orbit pass ('ASCENDING' or 'DESCENDING').
    products_data_roi: dict
        Dictionary mapping each Sentinel-1 product to its roi geometry.
    geometric_params: S1AnglesGeometricParameters
        Target and tile geometric parameters.
    data_coeff: int
        Scaling coefficient

    Returns
    -------
    incidence_files : list
        List of file paths to the computed incidence angle rasters.
    """
    coords, dst_transform = get_tile_coords(
        geometric_params.target_res, geometric_params.tile_geom_wkt
    )
    incidence_files = []
    for cpt, orbit_s1_products in enumerate(orbit_pass_products):
        eia_dates_files = []
        s1_eia_date_file = str(out_stack_elips_incidence).replace(
            ".tif", f"_{orbit_pass.lower()}_{cpt}.tif"
        )
        # TODO: test this code: if orbit_s1_products is a Path as describer from type hints, it
        #       should not be possible to `enumerate` over it
        for date_num, s1_product in enumerate(orbit_s1_products):  # type: ignore
            gcp_list, _, _, _, _ = sio_s1.get_geoloc_incidence_grid(
                Path(glob.glob(f"{str(s1_product)}/annotation/*vv*xml")[0])
            )

            s1_eia, _, _ = sio_s1.get_elipsoid_incidence_angle(
                bbox=geometric_params.tile_geom_wkt,
                bbox_crs=geometric_params.tile_crs,
                target_crs=geometric_params.target_epsg,
                out_res=geometric_params.target_res,
                gcps=gcp_list,
                gcp_crs=4326,
                meshgrid=coords,
            )

            s1_eia_file = str(out_stack_elips_incidence).replace(
                ".tif", f"_{orbit_pass.lower()}_{cpt}_{date_num}.tif"
            )
            eia_dates_files.append(s1_eia_file)
            write_s1_angles(
                s1_eia,
                wkt.loads(products_data_roi[s1_product]),
                geometric_params.target_epsg,
                dst_transform,
                Path(s1_eia_file),
                data_coeff,
            )
        if len(eia_dates_files) > 1:
            rio.merge.merge(eia_dates_files, dst_path=s1_eia_date_file)
            delete_all_files(eia_dates_files)
        else:
            s1_eia_date_file = s1_eia_file
        incidence_files.append(s1_eia_date_file)
    return incidence_files


def get_azimuth_files(
    orbit_pass_products: list[Path],
    out_stack_azimuth: Path,
    orbit_pass: OrbitPassVal,
    products_data_roi: dict,
    geometric_params: S1AnglesGeometricParameters,
    data_coeff: int,
) -> list[str]:
    """
    Compute the S1 azimuth angle files for a given orbit pass.

    Parameters
    ----------
    orbit_pass_products: list[Path]
        List of S1 products for the current orbit pass
    out_stack_azimuth: Path
        Output path of the stack.
    orbit_pass: OrbitPassVal
        Name of the orbit pass ('ASCENDING' or 'DESCENDING').
    products_data_roi: dict
        Dictionary mapping each Sentinel-1 product to its roi geometry.
    geometric_params: S1AnglesGeometricParameters
        Target and tile geometric parameters.
    data_coeff: int
        Scaling coefficient

    Returns
    -------
    azimuth_files : list
        List of file paths to the computed azimuth angle rasters.
    """
    _, dst_transform = get_tile_coords(
        geometric_params.target_res, geometric_params.tile_geom_wkt
    )
    azimuth_files = []
    for cpt, orbit_s1_products in enumerate(orbit_pass_products):
        azimuth_dates_files = []
        s1_azimuth_date_file = str(out_stack_azimuth).replace(
            ".tif", f"_{orbit_pass.lower()}_{cpt}.tif"
        )
        # TODO: test this code: if orbit_s1_products is a Path as describer from type hints, it
        #       should not be possible to `enumerate` over it
        for date_num, s1_product in enumerate(orbit_s1_products):  # type: ignore
            mask_polygon = wkt.loads(products_data_roi[s1_product])
            _, _, azimuth, _, _ = sio_s1.get_geoloc_incidence_grid(
                Path(glob.glob(f"{str(s1_product)}/annotation/*vv*xml")[0])
            )

            # azimuth angles
            s1_azimuth, _ = sio_s1.get_azimuth_angle(
                bbox=geometric_params.tile_geom_wkt,
                bbox_crs=geometric_params.tile_crs,
                target_crs=geometric_params.target_epsg,
                out_res=geometric_params.target_res,
                azimuth_value=azimuth,
            )
            s1_azimuth_file = str(out_stack_azimuth).replace(
                ".tif", f"_{orbit_pass.lower()}_{cpt}_{date_num}.tif"
            )
            azimuth_dates_files.append(s1_azimuth_file)
            write_s1_angles(
                s1_azimuth,
                mask_polygon,
                geometric_params.target_epsg,
                dst_transform,
                Path(s1_azimuth_file),
                data_coeff,
            )

        if len(azimuth_dates_files) > 1:
            rio.merge.merge(azimuth_dates_files, dst_path=s1_azimuth_date_file)
            delete_all_files(azimuth_dates_files)
        else:
            s1_azimuth_date_file = s1_azimuth_file
        azimuth_files.append(s1_azimuth_date_file)
    return azimuth_files


def get_tile_coords(
    target_res: float, tile_geom_wkt: BaseGeometry
) -> tuple[np.ndarray, rio.Affine]:
    """
    Get the coordinates of the tile

    Parameters
    ----------
    target_res: float
        Target resolution
    tile_geom_wkt
        Geometry of the tile in WKT format (which gets the tile's boundaries).

    Returns
    -------
    tuple[np.ndarray, rio.Affine]
        A tuple containing:
        - coords: np.ndarray
            Grid of coordinates
        - dst_transform: rio.Affine
            Affine transformation matrix for the grid
    """
    x_tile, y_tile = zip(
        *list(tile_geom_wkt.exterior.coords)  # pylint: disable=no-member
    )
    min_x = min(x_tile)
    max_x = max(x_tile)
    min_y = min(y_tile)
    max_y = max(y_tile)
    size_y = int(int(max_y - min_y) / target_res)
    size_x = int(int(max_x - min_x) / target_res)
    x_coords = np.linspace(min_x, max_x, size_x)
    y_coords = np.linspace(min_y, max_y, size_y)
    x, y = np.meshgrid(x_coords, y_coords, indexing="ij")
    coords = np.array((x.ravel(), y.ravel())).T
    dst_transform = rio.Affine(
        target_res, 0.0, x_coords[0], 0.0, -target_res, y_coords[-1]
    )
    return coords, dst_transform


def write_angle_file(
    data: np.ndarray,
    file_path: Path,
    crs: Any,
    transform: rio.Affine,
    sizex: int,
    sizey: int,
) -> None:
    """Write angle data to a GeoTIFF file."""
    with rio.open(
        file_path,
        "w",
        driver="GTiff",
        height=sizey,
        width=sizex,
        count=1,
        crs=crs,
        transform=transform,
        dtype="float32",
    ) as dest:
        dest.write(data, indexes=1)


def extract_s2_angles(
    product_list: list[Path],
    output_dir: Path,
    target_epsg: int,
    target_res: float,
    ref_raster: Path | None = None,
    nb_threads: int = 1,
    ram_limit: int = 1024,
) -> None:
    """Extract solar and incidence angles by product.

    Note
    ----
    A stack will be generated by angles :
    zenith, azimuth, even zenith, odd zenith, even azimuth, odd azimut

    The stack is sorted regarding input product list.
    """
    angles_files = S2AnglesFiles()
    angles_data = S2AnglesData()
    for product in product_list:
        print(f"compute angles of {str(product)}")
        new_prod = sio_s2(str(product))
        in_crs = new_prod.crs
        crs = in_crs
        in_transform = new_prod.transform

        angles_data.zenith, angles_data.azimuth = new_prod.read_solar_angles_as_numpy()
        assert isinstance(angles_data.zenith, np.ndarray)
        sizex, sizey = angles_data.zenith.shape
        (
            angles_data.even_zenith_angles,
            angles_data.odd_zenith_angles,
            angles_data.even_azimuth_angles,
            angles_data.odd_azimuth_angles,
        ) = new_prod.read_incidence_angles_as_numpy()

        if str(in_crs).lower() != f"epsg:{target_epsg}" or float(target_res) != float(
            in_transform[0]
        ):
            target_crs = f"EPSG:{target_epsg}"
            if ref_raster is None:
                target_transform, sizex, sizey = calculate_default_transform(
                    in_crs,
                    target_crs,
                    sizex,
                    sizey,
                    *new_prod.bounds,
                    resolution=target_res,
                )
            else:
                with rio.open(ref_raster, "r") as ref_raster_rio:
                    target_transform = ref_raster_rio.transform
                    sizex = ref_raster_rio.width
                    sizey = ref_raster_rio.height
            angles_data = warp_angles_data(
                angles_data,
                in_transform,
                target_transform,
                in_crs,
                target_crs,
                sizex,
                sizey,
                ram_limit,
                nb_threads,
            )
            in_transform = target_transform
            crs = target_epsg

        angles_files.out_zenith_files.append(output_dir / f"{product.stem}_ZENITH.tif")
        assert isinstance(angles_data.zenith, np.ndarray)
        write_angle_file(
            angles_data.zenith,
            output_dir / f"{product.stem}_ZENITH.tif",
            crs,
            in_transform,
            sizex,
            sizey,
        )

        angles_files.out_azimuth_files.append(
            output_dir / f"{product.stem}_AZIMUTH.tif"
        )
        assert isinstance(angles_data.azimuth, np.ndarray)
        sizex, sizey = angles_data.azimuth.shape
        write_angle_file(
            angles_data.azimuth,
            output_dir / f"{product.stem}_AZIMUTH.tif",
            crs,
            in_transform,
            sizex,
            sizey,
        )
        angles_files.even_zenith_angles_files.append(
            output_dir / f"{product.stem}_EVEN_ZENITH.tif"
        )
        assert isinstance(angles_data.even_zenith_angles, np.ndarray)
        sizex, sizey = angles_data.even_zenith_angles.shape
        write_angle_file(
            angles_data.even_zenith_angles,
            output_dir / f"{product.stem}_EVEN_ZENITH.tif",
            crs,
            in_transform,
            sizex,
            sizey,
        )
        angles_files.odd_zenith_angles_files.append(
            output_dir / f"{product.stem}_ODD_ZENITH.tif"
        )
        assert isinstance(angles_data.odd_zenith_angles, np.ndarray)
        sizex, sizey = angles_data.odd_zenith_angles.shape
        write_angle_file(
            angles_data.odd_zenith_angles,
            output_dir / f"{product.stem}_ODD_ZENITH.tif",
            crs,
            in_transform,
            sizex,
            sizey,
        )
        angles_files.even_azimuth_angles_files.append(
            output_dir / f"{product.stem}_EVEN_AZIMUTH.tif"
        )
        assert isinstance(angles_data.even_azimuth_angles, np.ndarray)
        sizex, sizey = angles_data.even_azimuth_angles.shape
        write_angle_file(
            angles_data.even_azimuth_angles,
            output_dir / f"{product.stem}_EVEN_AZIMUTH.tif",
            crs,
            in_transform,
            sizex,
            sizey,
        )
        angles_files.odd_azimuth_angles_files.append(
            output_dir / f"{product.stem}_ODD_AZIMUTH.tif"
        )
        assert isinstance(angles_data.odd_azimuth_angles, np.ndarray)
        sizex, sizey = angles_data.odd_azimuth_angles.shape
        write_angle_file(
            angles_data.odd_azimuth_angles,
            output_dir / f"{product.stem}_ODD_AZIMUTH.tif",
            crs,
            in_transform,
            sizex,
            sizey,
        )

    concatenate_angles_stacks(angles_files, output_dir, ram_limit)

    remove_files(angles_files)


def remove_files(angles_files: S2AnglesFiles) -> None:
    """
    Remove all single angles files (not the stack)

    Parameters
    ----------
    angles_files: S2AnglesFiles
        Object containing all signe angles file

    Returns
    -------
    None
    """
    files_to_rm = (
        angles_files.out_zenith_files
        + angles_files.out_azimuth_files
        + angles_files.even_zenith_angles_files
        + angles_files.odd_zenith_angles_files
        + angles_files.even_azimuth_angles_files
        + angles_files.odd_azimuth_angles_files
    )
    for file_to_rm in files_to_rm:
        Path(file_to_rm).unlink()


def warp_angles_data(
    angles_data: S2AnglesData,
    in_transform: rio.Affine,
    target_transform: rio.Affine,
    in_crs: CRS | str,
    target_crs: CRS | str,
    size_x: int,
    size_y: int,
    ram_limit: int,
    nb_threads: int,
) -> S2AnglesData:
    """
    Reproject angles data to the target reference system. Return the reprojected data.

    Parameters
    ----------
    angles_data: S2AnglesData
        Object containing the angles data.
    in_transform: Affine
        Transformation of the source data.
    target_transform: Affine
        Transformation of the output data.
    in_crs: CRS
        Source CRS.
    target_crs: CRS
        Output CRS.
    size_x: int
        Width of the output raster.
    size_y: int
        Height of the output raster.
    ram_limit: int
        RAM limit.
    nb_threads: int
        Number of threads to use.

    Returns
    -------
    S2AnglesData:
        Object containing the reprojected angles data
    """
    print("reproj zenith")
    zenith_warp = np.zeros((size_x, size_y))
    zenith_warp, target_transform = reproject(
        source=angles_data.zenith,
        destination=zenith_warp,
        src_transform=in_transform,
        src_crs=in_crs,
        dst_transform=target_transform,
        dst_crs=target_crs,
        resampling=rio.enums.Resampling.cubic,
        num_threads=nb_threads,
        warp_mem_limit=ram_limit,
    )
    print("reproj azimuth")
    azimuth_warp = np.zeros((size_x, size_y))
    azimuth_warp, target_transform = reproject(
        source=angles_data.azimuth,
        destination=azimuth_warp,
        src_transform=in_transform,
        src_crs=in_crs,
        dst_transform=target_transform,
        dst_crs=target_crs,
        resampling=rio.enums.Resampling.cubic,
        num_threads=nb_threads,
        warp_mem_limit=ram_limit,
    )
    print("reproj even zenith")
    even_zenith_warp = np.zeros((size_x, size_y))
    even_zenith_warp, target_transform = reproject(
        source=angles_data.even_zenith_angles,
        destination=even_zenith_warp,
        src_transform=in_transform,
        src_crs=in_crs,
        dst_transform=target_transform,
        dst_crs=target_crs,
        resampling=rio.enums.Resampling.cubic,
        num_threads=nb_threads,
        warp_mem_limit=ram_limit,
    )
    print("reproj odd zenith")
    odd_zenith_warp = np.zeros((size_x, size_y))
    odd_zenith_warp, target_transform = reproject(
        source=angles_data.odd_zenith_angles,
        destination=odd_zenith_warp,
        src_transform=in_transform,
        src_crs=in_crs,
        dst_transform=target_transform,
        dst_crs=target_crs,
        resampling=rio.enums.Resampling.cubic,
        num_threads=nb_threads,
        warp_mem_limit=ram_limit,
    )
    print("reproj even azimuth")
    even_azimuth_warp = np.zeros((size_y, size_x))
    even_azimuth_warp, target_transform = reproject(
        source=angles_data.even_azimuth_angles,
        destination=even_azimuth_warp,
        src_transform=in_transform,
        src_crs=in_crs,
        dst_transform=target_transform,
        dst_crs=target_crs,
        resampling=rio.enums.Resampling.cubic,
        num_threads=nb_threads,
        warp_mem_limit=ram_limit,
    )
    print("reproj odd azimuth")
    odd_azimuth_warp = np.zeros((size_x, size_y))
    odd_azimuth_warp, target_transform = reproject(
        source=angles_data.odd_azimuth_angles,
        destination=odd_azimuth_warp,
        src_transform=in_transform,
        src_crs=in_crs,
        dst_transform=target_transform,
        dst_crs=target_crs,
        resampling=rio.enums.Resampling.cubic,
        num_threads=nb_threads,
        warp_mem_limit=ram_limit,
    )
    return S2AnglesData(
        zenith=zenith_warp,
        azimuth=azimuth_warp,
        even_azimuth_angles=even_azimuth_warp,
        even_zenith_angles=even_zenith_warp,
        odd_azimuth_angles=odd_azimuth_warp,
        odd_zenith_angles=odd_zenith_warp,
    )


def concatenate_angles_stacks(
    angles_files: S2AnglesFiles, output_dir: Path, ram_limit: int
) -> None:
    """
    Concatenate all files for each angle type

    Parameters
    ----------
    angles_files: S2AnglesFiles
        Object containing lists of files to concatenate
    output_dir: Path
        Path to output directory
    ram_limit: int
        Ram limit

    Returns
    -------
    None
    """
    zenith_stack_file = output_dir / "ZENITH.tif"
    concatenate_app, _ = create_application(
        AvailableOTBApp.CONCATENATE_IMAGES,
        {
            "il": [str(zen_file) for zen_file in angles_files.out_zenith_files],
            "out": str(zenith_stack_file),
            "ram": str(ram_limit),
        },
    )
    concatenate_app.ExecuteAndWriteOutput()
    azimuth_stack_file = output_dir / "AZIMUTH.tif"
    concatenate_app, _ = create_application(
        AvailableOTBApp.CONCATENATE_IMAGES,
        {
            "il": [str(az_file) for az_file in angles_files.out_azimuth_files],
            "out": str(azimuth_stack_file),
            "ram": str(ram_limit),
        },
    )
    concatenate_app.ExecuteAndWriteOutput()
    even_zenith_angles_stack_file = output_dir / "EVEN_ZENITH.tif"
    concatenate_app, _ = create_application(
        AvailableOTBApp.CONCATENATE_IMAGES,
        {
            "il": [str(cfile) for cfile in angles_files.even_zenith_angles_files],
            "out": str(even_zenith_angles_stack_file),
            "ram": str(ram_limit),
        },
    )
    concatenate_app.ExecuteAndWriteOutput()
    odd_zenith_angles_stack_file = output_dir / "ODD_ZENITH.tif"
    concatenate_app, _ = create_application(
        AvailableOTBApp.CONCATENATE_IMAGES,
        {
            "il": [str(cfile) for cfile in angles_files.odd_zenith_angles_files],
            "out": str(odd_zenith_angles_stack_file),
            "ram": str(ram_limit),
        },
    )
    concatenate_app.ExecuteAndWriteOutput()
    even_azimuth_angles_stack_file = output_dir / "EVEN_AZIMUTH.tif"
    concatenate_app, _ = create_application(
        AvailableOTBApp.CONCATENATE_IMAGES,
        {
            "il": [str(cfile) for cfile in angles_files.even_azimuth_angles_files],
            "out": str(even_azimuth_angles_stack_file),
            "ram": str(ram_limit),
        },
    )
    concatenate_app.ExecuteAndWriteOutput()
    odd_azimuth_angles_stack_file = output_dir / "ODD_AZIMUTH.tif"
    concatenate_app, _ = create_application(
        AvailableOTBApp.CONCATENATE_IMAGES,
        {
            "il": [str(cfile) for cfile in angles_files.odd_azimuth_angles_files],
            "out": str(odd_azimuth_angles_stack_file),
            "ram": str(ram_limit),
        },
    )
    concatenate_app.ExecuteAndWriteOutput()


def extract_worldclim(
    bbox: list[float],
    worldclim_dir: Path,
    output_wc: Path,
    target_epsg: int,
    target_res: float | None = None,
    dtype: np.dtype = np.dtype("float32"),
    rescale_range: Literal["uint8", "uint16"] | None = None,
) -> None:
    """gdal.Warp vs rasterio.warp ?"""
    if rescale_range:
        dtype = np.dtype(rescale_range)

    print("extracting worldclim in ", bbox)
    wc_files = list(
        filter(
            lambda x: x.endswith("tif"),
            file_search_and(str(worldclim_dir), True, ".tif"),
        )
    )
    res_xy = get_raster_resolution(wc_files[0])
    if res_xy is None:
        raise FileExistsError(f"file {wc_files[0]} can't be read")
    res_x = res_xy[0]
    if target_res is None:
        target_res = res_x
    wclim = worldclim.WorldClimData(worldclim_dir)
    req_bbox = BoundingBox(*bbox)
    wc_arr, _, _, _, transform, scalers = wclim.read_as_numpy(
        crs=target_epsg,
        resolution=target_res,
        bounds=req_bbox,
        dtype=dtype,
        rescale_range=rescale_range,
    )
    nb_bands, size_y, size_x = wc_arr.shape
    with rio.open(
        output_wc,
        "w",
        driver="GTiff",
        height=size_y,
        width=size_x,
        count=nb_bands,
        crs=f"EPSG:{target_epsg}",
        transform=transform,
        dtype=dtype,
    ) as dest:
        dest.write(wc_arr)
    if scalers:
        with open(str(output_wc).replace(".tif", "_scalers.pkl"), "wb") as scaler_file:
            pickle.dump(scalers, scaler_file, protocol=pickle.HIGHEST_PROTOCOL)


def extract_srtm(
    bbox: list[float],
    output_slope: Path,
    output_aspect: Path,
    output_elevation: Path,
    srtm_dir: Path,
    target_epsg: int,
    target_res: float,
) -> None:
    """
    Write slope, aspect and elevation to a file from SRTM file

    Parameters
    ----------
    bbox:
        Bounding box of extraction
    output_slope:
        Path of the output slope file
    output_aspect:
        Path of the output aspect file
    output_elevation:
        Path of the output elevation file
    srtm_dir:
        Directory of the SRTM file
    target_epsg:
        Output projection
    target_res:
        Output resolution
    ram_limit:
        Maximum available RAM
    nb_threads:
        Maximum threads
    """
    print("extracting srtm in : ", bbox)
    dem_handler = srtm.SRTM(srtm_dir)
    req_bbox = BoundingBox(*bbox)
    (
        elevation_arr,
        slope_arr,
        aspect_arr,
        _,
        _,
        _,
        transform,
    ) = dem_handler.read_as_numpy(
        target_epsg,
        target_res,
        req_bbox,
        algorithm=rio.enums.Resampling.cubic,
    )
    size_y, size_x = elevation_arr.shape
    with rio.open(
        output_slope,
        "w",
        driver="GTiff",
        height=size_y,
        width=size_x,
        count=1,
        crs=f"EPSG:{target_epsg}",
        transform=transform,
        dtype="float32",
    ) as dest:
        dest.write(slope_arr, indexes=1)
    with rio.open(
        output_aspect,
        "w",
        driver="GTiff",
        height=size_y,
        width=size_x,
        count=1,
        crs=f"EPSG:{target_epsg}",
        transform=transform,
        dtype="float32",
    ) as dest:
        dest.write(aspect_arr, indexes=1)
    with rio.open(
        output_elevation,
        "w",
        driver="GTiff",
        height=size_y,
        width=size_x,
        count=1,
        crs=f"EPSG:{target_epsg}",
        transform=transform,
        dtype="float32",
    ) as dest:
        dest.write(elevation_arr, indexes=1)


def do_superimpose(otb_parameters: dict) -> None:
    """Execute superimpose app with the given parameters"""
    app, _ = create_application(AvailableOTBApp.SUPERIMPOSE, otb_parameters)
    app.ExecuteAndWriteOutput()


def tilename_to_refraster(tile_list: list[str], rasters: list[Path]) -> dict[str, Path]:
    """
    Return a dictionary with tile names as keys and raster path as corresponding values.
    For example:
    {"T31TCJ": "path/to/T31TCJ.tif,
    "T31TDJ": "path/to/T31TDJ.tif,
    ...}

    Parameters
    ----------
    tile_list:
        List of tile names
    rasters:
        List of rasters paths
    """
    out_dic = {}
    for tilename in tile_list:
        for raster in rasters:
            if tilename in str(raster):
                out_dic[tilename] = raster
    return out_dic


class FeaturesTiler(iota2_step.Step):
    """Class for launching tile features step"""

    resources_block_name = "features_tiler"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        intersections: list[tuple[str, str]],
        s1_intersections_list: list[tuple[str, str]] | None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        self.output_path = Path(
            rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path")
        )
        self.worldclim_path = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "worldclim_path"
        )
        srtm_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "srtm_path")
        s2_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "s2_path")
        self.ram_per_task = 1024 * get_ram(self.resources.ram)
        self.nb_threads = self.resources.nb_cpu

        self.target_epsg = int(
            rcf.ReadConfigFile(self.cfg).get_param("chain", "proj").split(":")[-1]
        )
        self.spatial_resolution = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "spatial_resolution"
        )
        # chain.spatial_resolution is mandatory with this builder
        self.target_res = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "spatial_resolution"
        )[0]
        vector_db = rcf.ReadConfigFile(self.cfg).get_param("chain", "grid")
        tile_column = rcf.ReadConfigFile(self.cfg).get_param("chain", "tile_field")
        raster_grid_path = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "rasters_grid_path"
        )
        from_rasters = bool(raster_grid_path)

        # tasks def
        self.generate_all_tasks(intersections, tile_column, vector_db)

        # add tasks if srtm or world clim
        rasters_ref = None
        gdf = None
        if bool(vector_db):
            gdf = gpd.read_file(vector_db)

        tiles = rcf.ReadConfigFile(self.cfg).get_param("chain", "list_tile").split(" ")
        if from_rasters:
            rasters_grid = [
                Path(r_file)
                for r_file in filter(
                    lambda x: x.endswith(".tif")
                    and re.findall(r"(?=(" + "|".join(tiles) + r"))", str(x)),
                    file_search_and(raster_grid_path, True, ".tif"),
                )
            ]
            rasters_ref = tilename_to_refraster(tiles, rasters_grid)
        for tile in tiles:
            tile_name = Path(tile).stem
            ref_coords = self.get_image_coordinates(
                from_rasters,
                raster_grid_path,
                rasters_ref,
                tile,
                gdf,
                tile_column,
                tile_name,
            )
            if self.worldclim_path:
                task_name = f"worldclim_{tile_name}"
                task = self.get_worldclim_task(
                    task_name,
                    ref_coords.x_min,
                    ref_coords.y_min,
                    ref_coords.x_max,
                    ref_coords.y_max,
                    tile_name,
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tiles",
                    task_sub_group=task_name,
                    task_dep_dico={},
                )
            if srtm_path:
                task_name = f"srtm_{tile_name}"
                task = self.get_srtm_task(
                    task_name,
                    ref_coords.x_min,
                    ref_coords.y_min,
                    ref_coords.x_max,
                    ref_coords.y_max,
                    tile_name,
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tiles",
                    task_sub_group=task_name,
                    task_dep_dico={},
                )
            if s2_path:
                self.add_s2_angles_task(
                    s2_path, tile, tile_name, raster_grid_path, from_rasters
                )
        if s1_intersections_list:
            self.add_s1_angles_task(s1_intersections_list, vector_db, from_rasters)

    def add_s2_angles_task(
        self,
        s2_path: str,
        tile: str,
        tile_name: str,
        raster_grid_path: str,
        from_rasters: bool,
    ) -> None:
        """
        Add a task to extract s2 angles

        Parameters
        ----------
        s2_path:
            Path to s2 data directory
        tile:
            Path to the tile file
        tile_name:
            Name of the tile
        raster_grid_path:
            Path to the raster grid
        from_rasters:
            Flag to check if mode is raster

        Returns
        -------
        None
        """
        tile_dir = Path(s2_path) / tile
        s2_products = list(filter(Sentinel2.is_s2_theia_product, tile_dir.iterdir()))
        s2_products = Sentinel2.sort_products(s2_products)
        ref_raster = None
        if from_rasters:
            print(
                "s2 angles projections/resolution will "
                f"be fitted to data in {raster_grid_path}"
            )
            ref_raster = Path(raster_grid_path) / f"{tile}.tif"
            if not ref_raster.exists():
                raise FileNotFoundError(f"{str(ref_raster)} does not exists.")
        task_name = f"s2_angles_{tile_name}"
        task = self.I2Task(
            task_name=task_name,
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": extract_s2_angles,
                "product_list": s2_products,
                "output_dir": Path(self.output_path) / tile,
                "target_epsg": self.target_epsg,
                "target_res": self.target_res,
                "nb_threads": self.nb_threads,
                "ref_raster": ref_raster,
                "ram_limit": self.ram_per_task,
            },
            task_resources=self.get_resources(),
        )
        self.add_task_to_i2_processing_graph(
            task,
            task_group="tiles",
            task_sub_group=task_name,
            task_dep_dico={},
        )

    def add_s1_angles_task(
        self,
        s1_intersections_list: list[tuple[str, str]],
        vector_db: str,
        from_rasters: bool,
    ) -> None:
        """
        Add a task to extract s1 angles

        Parameters
        ----------
        s1_intersections_list:
            List of intersected pairs
        vector_db:
            Database used if mode is vector
        from_rasters:
            Flag to check if mode is raster

        Returns
        -------
        None
        """
        for tile_name, products in s1_intersections_list:
            products_sorted = sorted(
                products,
                key=lambda x: int(
                    Path(x[0]).name.split("_")[Sentinel1.date_position].replace("T", "")
                ),
            )
            indicidence_stack_file = (
                Path(self.output_path) / tile_name / "s1_incidence.tif"
            )
            zenith_stack_file = Path(self.output_path) / tile_name / "s1_azimuth.tif"
            products_dir, tiles_geom, rois_geom = zip(*products_sorted)
            if tiles_geom.count(tiles_geom[0]) != len(tiles_geom):
                raise ValueError(f"multiple geoms for tile '{tile_name}'")
            task_name = f"s1_angles_{tile_name}"
            if from_rasters:
                tile_crs = self.target_epsg
            else:
                tile_crs = gpd.read_file(vector_db).crs
            task = self.I2Task(
                task_name=task_name,
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": extract_s1_angles,
                    "s1_products": products_dir,
                    "tile_geom": tiles_geom[0],
                    "tile_crs": tile_crs,
                    "rois": rois_geom,
                    "target_epsg": self.target_epsg,
                    "target_res": self.target_res,
                    "ram_limit": self.ram_per_task,
                    "out_stack_elips_incidence": indicidence_stack_file,
                    "out_stack_azimuth": zenith_stack_file,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tiles",
                task_sub_group=task_name,
                task_dep_dico={},
            )

    def generate_all_tasks(
        self, intersections: list[tuple[str, str]], tile_column: str, vector_db: str
    ) -> None:
        """
        Generate tasks for all tiles

        Parameters
        ----------
        intersections:
            List of intersected pairs where the first elem of the pair is a path of 'rasters_grid'
        tile_column:
            Name of the column containing the tiles
        vector_db
            Database used

        Returns
        -------
        None
        """
        tile_list = set()
        tile_bounds = []
        for tile, features_file in intersections:
            tile_name = Path(tile).stem
            if tile_name not in tile_list:
                tile_bounds.append(tile)
            tile_list.add(tile_name)
            feature_name = Path(features_file).stem
            task_name = f"tile_{tile_name}_{feature_name}"

            output_file = self.output_path / tile_name / f"{feature_name}.tif"

            task = self.generate_task(
                features_file,
                output_file,
                vector_db,
                task_name,
                tile,
                tile_column,
                tile_name,
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tiles",
                task_sub_group=task_name,
                task_dep_dico={},
            )

    def get_image_coordinates(
        self,
        from_rasters: bool,
        raster_grid_path: PathLike,
        rasters_ref: dict[str, Path] | None,
        tile: str,
        gdf: gpd.GeoDataFrame | None,
        tile_column: str,
        tile_name: str,
    ) -> ImageCoordinates:
        """
        Return the coordinates of the selected object (from a raster or a database)

        Parameters
        ----------
        from_rasters: bool
            Flag indicating if the process uses raster or vector files as reference
        raster_grid_path: PathLike
            Path to the raster grid file
        rasters_ref: dict[str, Path],
            dictionary with tile names as keys and corresponding raster path as values.
        tile: PathLike
            Path to the tile file
        gdf: GeoDataFrame
            Geodataframe containing the vector data to use
        tile_column: str
            Name of the column containing tile names
        tile_name: str
            Current tile name

        Returns
        -------
        The coordinates of the corners of the reference object (shape or raster)
        """
        if from_rasters:
            assert rasters_ref is not None
            if tile not in rasters_ref:
                raise ValueError(
                    f"the tile '{tile}' was not found in {raster_grid_path}"
                )
            in_crs = get_raster_projection_epsg(str(rasters_ref[tile]))
            in_crs = f"epsg:{in_crs}"
            min_x, max_x, min_y, max_y = get_raster_extent(str(rasters_ref[tile]))

        else:
            assert gdf is not None
            geom = gdf.loc[gdf[tile_column] == tile_name]["geometry"]
            bounds = geom.bounds
            min_x, min_y, max_x, max_y = bounds.values[0]
            in_crs = geom.crs
        min_x, min_y, max_x, max_y = reproj_bbox(
            BoundingBox(min_x, min_y, max_x, max_y), in_crs, self.target_epsg
        )
        return ImageCoordinates(x_max=max_x, y_max=max_y, x_min=min_x, y_min=min_y)

    def generate_task(
        self,
        features_file: str,
        output_file: Path,
        vector_db: str,
        task_name: str,
        tile: str,
        tile_column: str,
        tile_name: str,
    ) -> iota2_step.Step.I2Task:
        """
        Generate the right I2Task depending on the type of input (shapefile or raster)

        Parameters
        ----------
        features_file: str
            The feature to process
        output_file: PathLike
            Path to the output file
        vector_db: str
            Path to the vector database containing the grid to use (if in vector mode)
        task_name: str
            Name of the task
        tile: str
            Path to the tile file
        tile_column: str
            Database field name containing the tiles
        tile_name: str
            Tile name

        Returns
        -------
        I2Task:
            The right I2Task created
        """
        if bool(vector_db):

            output_gdaltype = rcf.ReadConfigFile(self.cfg).get_param(
                "chain", "output_features_pix_type"
            )
            output_gdaltype = str_to_gdal_pix_type(output_gdaltype)
            x_res, y_res = self.spatial_resolution

            resampling_alg = rcf.ReadConfigFile(self.cfg).get_param(
                "chain", "from_vectordb_resampling_method"
            )
            source_epsg = int(get_raster_projection_epsg(features_file))
            cutline = (
                f"SELECT * from {get_layer_name(vector_db)} "
                f"where {tile_column}='{tile_name}'"
            )

            task = self.gen_tile_from_vector(
                task_name,
                cutline,
                features_file,
                output_file,
                vector_db,
                output_gdaltype,
                (x_res, y_res),
                (source_epsg, self.target_epsg),
                resampling_alg,
            )
        else:
            output_otbtype = rcf.ReadConfigFile(self.cfg).get_param(
                "chain", "output_features_pix_type"
            )
            resampling_alg = rcf.ReadConfigFile(self.cfg).get_param(
                "chain", "from_rasterdb_resampling_method"
            )
            bco_rad = rcf.ReadConfigFile(self.cfg).get_param(
                "chain", "resampling_bco_radius"
            )
            task = self.gen_tile_from_raster(
                task_name,
                tile,
                features_file,
                output_file,
                self.ram_per_task,
                resampling_alg,
                output_otbtype,
                bco_rad,
            )
        return task

    def get_worldclim_task(
        self,
        task_name: str,
        min_x: float,
        min_y: float,
        max_x: float,
        max_y: float,
        tile_name: str,
    ) -> iota2_step.Step.I2Task:
        """Return an 'extract_wordclim' task"""
        task = self.I2Task(
            task_name=task_name,
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": extract_worldclim,
                "bbox": [min_x, min_y, max_x, max_y],
                "worldclim_dir": self.worldclim_path,
                "target_epsg": self.target_epsg,
                "target_res": self.target_res,
                "output_wc": self.output_path / tile_name / "worldclim.tif",
                "dtype": rcf.ReadConfigFile(self.cfg).get_param(
                    "chain", "out_worldclim_dtype"
                ),
                "rescale_range": rcf.ReadConfigFile(self.cfg).get_param(
                    "chain", "out_worldclim_rescale_range"
                ),
            },
            task_resources=self.get_resources(),
        )
        return task

    def get_srtm_task(
        self,
        task_name: str,
        min_x: float,
        min_y: float,
        max_x: float,
        max_y: float,
        tile_name: str,
    ) -> iota2_step.Step.I2Task:
        """Return extract_srtm task"""
        task = self.I2Task(
            task_name=task_name,
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": extract_srtm,
                "bbox": [min_x, min_y, max_x, max_y],
                "output_slope": self.output_path / tile_name / "slope.tif",
                "output_aspect": self.output_path / tile_name / "aspect.tif",
                "output_elevation": self.output_path / tile_name / "elevation.tif",
                "srtm_dir": rcf.ReadConfigFile(self.cfg).get_param(
                    "chain", "srtm_path"
                ),
                "target_epsg": self.target_epsg,
                "target_res": self.target_res,
            },
            task_resources=self.get_resources(),
        )
        return task

    def gen_tile_from_raster(
        self,
        task_name: str,
        tile: str,
        features_file: str,
        output_file: Path,
        ram_per_task: int,
        resampling_alg: str,
        output_otbtype: str,
        bco_rad: int,
    ) -> iota2_step.Step.I2Task:
        """
        Build iota2 step (containing an otb superimpose)
        """
        otb_parameters = {
            "inr": str(tile),
            "inm": str(features_file),
            "out": str(output_file),
            "interpolator": resampling_alg,
            "ram": str(ram_per_task),
            "pixType": output_otbtype,
        }
        if resampling_alg == "bco":
            otb_parameters["interpolator.bco.radius"] = str(bco_rad)
        task = self.I2Task(
            task_name=task_name,
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": do_superimpose,
                "otb_parameters": otb_parameters,
            },
            task_resources=self.get_resources(),
        )
        return task

    def gen_tile_from_vector(
        self,
        task_name: str,
        cutline_sql: str,
        features_file: str,
        output_file: Path,
        vector_db: str,
        output_type: gdalconst,
        xy_res: tuple[int, int],
        epsg_source_target: tuple[int, int],
        resampling_alg: str = "near",
    ) -> iota2_step.Step.I2Task:
        """Build iota2 task."""
        task = self.I2Task(
            task_name=task_name,
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": gdal.Warp,
                "destNameOrDestDS": str(output_file),
                "srcDSOrSrcDSTab": str(features_file),
                "cropToCutline": True,
                "xRes": xy_res[0],
                "yRes": xy_res[1],
                "multithread": True,
                "resampleAlg": resampling_alg,
                "warpMemoryLimit": self.ram_per_task,
                "outputType": output_type,
                "targetAlignedPixels": True,
                "srcSRS": f"EPSG:{epsg_source_target[0]}",
                "dstSRS": f"EPSG:{epsg_source_target[1]}",
                "cutlineDSName": vector_db,
                "cutlineSQL": cutline_sql,
                "format": "GTiff",
                "creationOptions": [f"NUM_THREADS={self.nb_threads}"],
            },
            task_resources=self.get_resources(),
        )
        return task

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Generate features per tiles"
        return description
