#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch validation at boundary tasks."""
import logging
from pathlib import Path

import iota2.validation.boundary_validation_metrics as bvm
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.typings.i2_types import MergeMetricMatricesParameters
from iota2.vector_tools.vector_functions import get_reverse_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class ComputeAgreementMatrix(iota2_step.Step):
    """Launch task for validation at boundary scale."""

    resources_block_name = "extract_boundary_points"

    def __init__(self, cfg: str, cfg_resources_file: Path | None):
        """Initialize the step."""
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        parameters = rcf.ReadConfigFile(self.cfg)
        runs = parameters.get_param("arg_train", "runs")
        output_path = parameters.get_param("chain", "output_path")
        buffer_sizes_list = parameters.get_param("pretrained_model", "boundary_buffer")
        data_field = self.i2_const.re_encoding_label_name
        region_path = parameters.get_param("chain", "region_path")
        region_field = parameters.get_param("chain", "region_field")
        reference_data = parameters.get_param("chain", "ground_truth")
        reference_data_field = parameters.get_param("chain", "data_field")
        new_label_to_old = get_reverse_encoding_labels_dic(
            reference_data, reference_data_field
        )
        nomenclature = parameters.get_param("chain", "nomenclature_path")
        merge_parameters_list = [
            MergeMetricMatricesParameters(
                data_field=data_field,
                runs=runs,
                buffer_size=buffer_size,
                label_conv_dict=new_label_to_old,
            )
            for buffer_size in buffer_sizes_list
        ]
        task = self.I2Task(
            task_name="merge_boundary",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": bvm.find_entry_merge_metrics_matrices,
                "iota2_directory": output_path,
                "eco_region_file": region_path,
                "merge_metrics_parameters_list": merge_parameters_list,
                "region_field": region_field,
                "nomenclature_path": nomenclature,
            },
            task_resources=self.get_resources(),
        )

        dep = []
        for tile in self.tiles:
            for seed in range(runs):
                dep += [f"{tile}_{seed}"]
        self.add_task_to_i2_processing_graph(
            task,
            task_group="merge_all",
            task_sub_group="merge_matrix",
            task_dep_dico={"tile_tasks_seed": dep},
        )

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "Compute agreement matrix"
        return description
