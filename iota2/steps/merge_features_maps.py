#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Merge features map step"""
import logging
from pathlib import Path

from iota2.common import write_features_map as wfm
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class MergeFeaturesMaps(iota2_step.Step):
    """Class to merge features map"""

    resources_block_name = "mergeMaps"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")

        task = self.I2Task(
            task_name="merge_feat_maps",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": wfm.merge_features_maps,
                "iota2_directory": output_path,
                "output_name": parameters.get_param("external_features", "output_name"),
                "res": parameters.get_param("chain", "spatial_resolution"),
                "no_data_value": parameters.get_param(
                    "external_features", "no_data_value"
                ),
            },
            task_resources=self.get_resources(),
        )
        dep = []
        for tile in self.tiles:

            dep.append(f"merge_maps_by_tiles {tile}")
        self.add_task_to_i2_processing_graph(
            task, task_group="merge_maps", task_dep_dico={"merge_maps_by_tiles": dep}
        )
        # implement tests for check if custom features are well provided
        # so the chain failed during step init

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Merge features tiles into a mosaic"
        return description
