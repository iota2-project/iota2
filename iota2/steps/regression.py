# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Regression Steps

- using pytorch models
- using scikit learn random forest
"""

import logging
import shutil
from itertools import product
from pathlib import Path
from typing import Any

from iota2.classification.py_classifiers import PredictionParameters
from iota2.classification.pytorch_prediction import (
    PytorchPredictionParameters,
    TorchPredictionPaths,
    pytorch_predict,
)
from iota2.classification.scikit_prediction import scikit_learn_predict
from iota2.common import i2_constants
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.learning.launch_learning import (
    learn_scikitlearn_model,
    learn_torch_model_regression,
)
from iota2.learning.pytorch.train_pytorch_model import (
    DataLoaderParameters,
    ModelBuilder,
)
from iota2.learning.train_sklearn import CrossValidationParameters, SciKitModelParams
from iota2.steps.iota2_step import Step
from iota2.typings.i2_types import (
    CustomFeaturesParameters,
    DBInfo,
    ModelHyperParameters,
    PredictionOutput,
    RegressionMetricsParameters,
)
from iota2.validation.regression_metrics import (
    generate_metrics,
    generate_multi_tile_metrics,
    merge_metrics,
)

I2_CONST = i2_constants.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")
FAKE_CLASS = "split"  # fake class used for sample selection


class TrainRegressionPytorch(Step):
    """regression training step using pytorch
    the model to be used is given in the dl_name field of the config file
    """

    resources_block_name = "TrainRegressionPytorch"

    @classmethod
    def step_description(cls) -> str:
        return "Train pytorch regression model"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        self.working_directory = working_directory
        self.output_path = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "output_path"
        )
        self.data_field = rcf.ReadConfigFile(self.cfg).get_param("chain", "data_field")
        pytorch_params = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "deep_learning_parameters"
        )
        runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")
        hyperparameters_couples = list(
            product(
                pytorch_params["hyperparameters_solver"]["batch_size"],
                pytorch_params["hyperparameters_solver"]["learning_rate"],
            )
        )

        # spatial_models_distribution was initialized by method
        # stolen from classification builder
        for model_name, _ in self.spatial_models_distribution.items():
            for seed in range(runs):
                vector_file = str(
                    Path(self.output_path)
                    / "learningSamples"
                    / f"Samples_region_{model_name}_seed{seed}_learn.{I2_CONST.i2_database_ext}"
                )
                output_model = str(
                    Path(self.output_path)
                    / "model"
                    / f"model_{model_name}_seed_{seed}.txt"
                )
                task_name = f"learning_model_{model_name}_seed_{seed}"
                target_model = f"model_{model_name}_seed_{seed}"

                for hyper_num, (batch_size, learning_rate) in enumerate(
                    hyperparameters_couples
                ):
                    task_name_hyperparam = f"{task_name}_hyp_{hyper_num}"
                    output_model_hyper = str(output_model).replace(
                        ".txt", f"_hyp_{hyper_num}.txt"
                    )

                    # define task parameters
                    task_params = self.get_learning_pytorch_parameters(
                        str(vector_file),
                        output_model_hyper,
                        learning_rate,
                        batch_size,
                        pytorch_params,
                    )
                    # define task
                    task = self.I2Task(
                        task_name=task_name_hyperparam,
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters=task_params,
                        task_resources=self.get_resources(),
                    )

                    # add task to processing graph
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="region_tasks",
                        task_sub_group=f"{target_model}_hyp_{hyper_num}",
                        task_dep_dico={"region_tasks": [target_model]},
                    )

    def get_learning_pytorch_parameters(
        self,
        vector_file: str,
        output_model: str,
        learning_rate: float,
        batch_size: int,
        pytorch_params: dict[str, Any],
    ) -> dict:
        """Return task parameters to learn pytorch model"""
        database = DBInfo(self.data_field, vector_file)
        model_builder = ModelBuilder(
            dl_name=pytorch_params["dl_name"],
            model_path=output_model,
            model_parameters_file=output_model.replace(".txt", "_parameters.pickle"),
            encoder_convert_file=output_model.replace(".txt", "_encoder.pickle"),
            dl_module=pytorch_params["dl_module"],
        )
        model_hyperparameters = ModelHyperParameters(
            dl_parameters=pytorch_params["dl_parameters"],
            restart_from_checkpoint=pytorch_params["restart_from_checkpoint"],
            weighted_labels=pytorch_params["weighted_labels"],
            learning_rate=learning_rate,
            epochs=pytorch_params["epochs"],
            batch_size=batch_size,
            model_optimization_criterion=pytorch_params["model_optimization_criterion"],
            additional_statistics_percentage=pytorch_params[
                "additional_statistics_percentage"
            ],
            enable_early_stop=pytorch_params["enable_early_stop"],
            epoch_to_trigger=pytorch_params["epoch_to_trigger"],
            early_stop_patience=pytorch_params["early_stop_patience"],
            early_stop_metric=pytorch_params["early_stop_metric"],
            early_stop_tol=pytorch_params["early_stop_tol"],
            adaptive_lr=pytorch_params["adaptive_lr"],
        )
        dataloader_params = DataLoaderParameters(
            pytorch_params["num_workers"], pytorch_params["dataloader_mode"]
        )
        task_params = {
            "f": learn_torch_model_regression,
            "database": database,
            "model_builder": model_builder,
            "model_hyperparameters": model_hyperparameters,
            "dataloader_params": dataloader_params,
            "working_dir": self.working_directory,
        }
        return task_params


class PredictRegressionPytorch(Step):
    """regression training step using pytorch"""

    resources_block_name = "PredictRegressionPytorch"

    @classmethod
    def step_description(cls) -> str:
        return "Predict with pytorch regression model"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        output_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path")
        number_of_chunks = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "number_of_chunks"
        )
        pytorch_params = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "deep_learning_parameters"
        )
        runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")
        # custom features
        enabled_gap, enabled_raw = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "data_mode_access"
        )

        for model, model_meta in self.spatial_models_distribution_classify.items():
            for seed in range(runs):
                for tile in model_meta["tiles"]:
                    for chunk in range(number_of_chunks):

                        region_mask_name = model.split("f")[0]
                        model_file = str(
                            Path(output_path)
                            / "model"
                            / f"model_{model}_seed_{seed}.txt"
                        )

                        # is confidence used in regression ?
                        # confidence_file = config.chain.output_path / "classif" /
                        # f"{tile}_model_{model_name}_confidence_seed_{seed}.tif"

                        # is proba used in regression ?
                        # out_proba_file = config.chain.output_path / "classif" /
                        # f"PROBAMAP_{tile}_model_{model_name}_seed_{seed}.tif"

                        target_model = f"model_{model}_seed_{seed}"
                        task_params = {
                            "f": pytorch_predict,
                            "torch_parameters": PytorchPredictionParameters(
                                nn_name=pytorch_params["dl_name"],
                                model_file=str(model_file),
                                model_parameters_file=str(model_file).replace(
                                    ".txt", "_parameters.pickle"
                                ),
                                external_nn_module=pytorch_params["dl_module"],
                                max_inference_size=rcf.ReadConfigFile(
                                    self.cfg
                                ).get_param(
                                    "python_data_managing", "max_nn_inference_size"
                                ),
                            ),
                            "torch_pred_paths": TorchPredictionPaths(
                                mask=str(
                                    Path(output_path)
                                    / "classif"
                                    / "MASK"
                                    / f"MASK_region_{region_mask_name}_{tile}.tif"
                                ),
                                encoder_convert_file=str(model_file).replace(
                                    ".txt", "_encoder.pickle"
                                ),
                                working_dir=working_directory,
                                hyperparam_file=str(model_file).replace(
                                    ".txt", "_hyper.pickle"
                                ),
                            ),
                            "pred_parameters": PredictionParameters(
                                tile_name=tile,
                                enabled_gap=enabled_gap,
                                enabled_raw=enabled_raw,
                                sensors_parameters=rcf.Iota2Parameters(
                                    rcf.ReadConfigFile(self.cfg)
                                ).get_sensors_parameters(tile),
                                sensors_dates=rcf.Iota2Parameters(
                                    rcf.ReadConfigFile(self.cfg)
                                ).get_available_sensors_dates(model_meta["tiles"]),
                                exogeneous_data=rcf.ReadConfigFile(self.cfg).get_param(
                                    "external_features", "exogeneous_data"
                                ),
                                features_from_raw_dates=rcf.ReadConfigFile(
                                    self.cfg
                                ).get_param("arg_train", "features_from_raw_dates"),
                            ),
                            "pred_output": PredictionOutput(
                                classif=str(
                                    Path(output_path)
                                    / "classif"
                                    / f"Regression_{tile}_model_{model}_seed_{seed}.tif"
                                ),
                                confidence="XXX",
                                proba="",
                                output_path=output_path,
                            ),
                            "number_of_chunks": number_of_chunks,
                            "targeted_chunk": chunk,
                            "all_class_labels": None,
                            "custom_features_parameters": CustomFeaturesParameters(
                                enable=rcf.ReadConfigFile(self.cfg).get_param(
                                    "external_features", "external_features_flag"
                                ),
                                module=rcf.ReadConfigFile(self.cfg).get_param(
                                    "external_features", "module"
                                ),
                                functions=rcf.ReadConfigFile(self.cfg).get_param(
                                    "external_features", "functions"
                                ),
                                concat_mode=rcf.ReadConfigFile(self.cfg).get_param(
                                    "external_features", "concat_mode"
                                ),
                            ),
                            "mode": "regression",
                        }

                        task = self.I2Task(
                            task_name=f"regression_{tile}_model_{model}_seed_{seed}_{chunk}",
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters=task_params,
                            task_resources=self.get_resources(),
                        )

                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="tile_tasks_model_mode",
                            task_sub_group=f"{tile}_{model}_{seed}_{chunk}",
                            task_dep_dico={"region_tasks": [target_model]},
                        )


class TrainRegressionScikit(Step):
    """regression prediction step using scikit learn
    only random forest for now
    """

    resources_block_name = "TrainRegressionScikit"

    @classmethod
    def step_description(cls) -> str:
        return "Train scikit random forest"

    def __init__(self, cfg: str, cfg_resources_file: Path | None):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        db_ext = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "learning_samples_extension"
        )
        available_ram = 1024.0 * get_ram(self.resources.ram)
        sk_kwargs = rcf.ReadConfigFile(self.cfg).get_param(
            "scikit_models_parameters", "keyword_arguments"
        )  # alias
        runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")
        output_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path")

        # spatial_models_distribution was initialized by method
        # stolen from classification builder
        for model_name, _ in self.spatial_models_distribution.items():
            for seed in range(runs):
                vector_file = str(
                    Path(output_path)
                    / "learningSamples"
                    / f"Samples_region_{model_name}_seed{seed}_learn.{db_ext}"
                )
                output_model = str(
                    Path(output_path) / "model" / f"model_{model_name}_seed_{seed}.txt"
                )

                task_name = f"learning_model_{model_name}_seed_{seed}"
                target_model = f"model_{model_name}_seed_{seed}"

                scikit_parameters = SciKitModelParams(
                    DBInfo(
                        rcf.ReadConfigFile(self.cfg).get_param("chain", "data_field"),
                        str(vector_file),
                    ),
                    rcf.ReadConfigFile(self.cfg).get_param(
                        "scikit_models_parameters", "model_type"
                    ),
                    str(output_model),
                    sk_kwargs,
                )
                cross_validation = CrossValidationParameters(
                    rcf.ReadConfigFile(self.cfg).get_param(
                        "scikit_models_parameters", "cross_validation_grouped"
                    ),
                    rcf.ReadConfigFile(self.cfg).get_param(
                        "scikit_models_parameters", "cross_validation_folds"
                    ),
                    rcf.ReadConfigFile(self.cfg).get_param(
                        "scikit_models_parameters", "cross_validation_parameters"
                    ),
                )
                task_params = {
                    "f": learn_scikitlearn_model,
                    "model_parameters": scikit_parameters,
                    "cross_validation_parameters": cross_validation,
                    "apply_standardization": rcf.ReadConfigFile(self.cfg).get_param(
                        "scikit_models_parameters", "standardization"
                    ),
                    "available_ram": available_ram,
                }

                task = self.I2Task(
                    task_name=task_name,
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters=task_params,
                    task_resources=self.get_resources(),
                )

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=target_model,
                    task_dep_dico={"region_tasks": [target_model]},
                )


class PredictRegressionScikit(Step):
    """regression prediction step using scikit learn"""

    resources_block_name = "PredictRegressionScikit"

    @classmethod
    def step_description(cls) -> str:
        return "Predict with scikit random forest"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        number_of_chunks = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "number_of_chunks"
        )
        output_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path")
        runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")

        # custom features
        enabled_gap, enabled_raw = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "data_mode_access"
        )

        # main loop
        for model_name, model_meta in self.spatial_models_distribution_classify.items():
            for seed in range(runs):
                for tile in model_meta["tiles"]:
                    for chunk in range(number_of_chunks):
                        task_name = (
                            f"regression_{tile}_model_{model_name}_seed_{seed}_{chunk}"
                        )
                        target_model = f"model_{model_name}_seed_{seed}"

                        # set model file for scikit regression
                        external_model = rcf.ReadConfigFile(self.cfg).get_param(
                            "pretrained_model", "model"
                        )
                        model_file = str(
                            Path(output_path) / "model" / f"{target_model}.txt"
                        )
                        if external_model:
                            shutil.copy(external_model, model_file)

                        task_params = {
                            "f": scikit_learn_predict,
                            "pred_parameters": PredictionParameters(
                                tile_name=tile,
                                sensors_parameters=rcf.Iota2Parameters(
                                    rcf.ReadConfigFile(self.cfg)
                                ).get_sensors_parameters(tile),
                                enabled_gap=enabled_gap,
                                enabled_raw=enabled_raw,
                                sensors_dates=rcf.Iota2Parameters(
                                    rcf.ReadConfigFile(self.cfg)
                                ).get_available_sensors_dates(model_meta["tiles"]),
                                exogeneous_data=rcf.ReadConfigFile(self.cfg).get_param(
                                    "external_features", "exogeneous_data"
                                ),
                            ),
                            "mask": str(
                                Path(output_path)
                                / "classif"
                                / "MASK"
                                / f"MASK_region_{ model_name.split('f')[0]}_{tile}.tif"
                            ),
                            "model": str(model_file),
                            "pred_output": PredictionOutput(
                                classif=str(
                                    Path(output_path)
                                    / "classif"
                                    / f"Regression_{tile}_model_{model_name}_seed_{seed}.tif"
                                ),
                                confidence=str(  # not used for regression
                                    Path(output_path)
                                    / "classif"
                                    / f"{tile}_model_{model_name}_confidence_seed_{seed}.tif"
                                ),
                                proba=None,
                                output_path=output_path,
                            ),
                            "working_dir": working_directory,
                            "number_of_chunks": number_of_chunks,
                            "targeted_chunk": chunk,
                            "mode": "regression",
                            "custom_features_parameters": CustomFeaturesParameters(
                                enable=rcf.ReadConfigFile(self.cfg).get_param(
                                    "external_features", "external_features_flag"
                                ),
                                module=rcf.ReadConfigFile(self.cfg).get_param(
                                    "external_features", "module"
                                ),
                                functions=rcf.ReadConfigFile(self.cfg).get_param(
                                    "external_features", "functions"
                                ),
                                concat_mode=rcf.ReadConfigFile(self.cfg).get_param(
                                    "external_features", "concat_mode"
                                ),
                            ),
                        }

                        task = self.I2Task(
                            task_name=task_name,
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters=task_params,
                            task_resources=self.get_resources(),
                        )

                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="tile_tasks_model_mode",
                            task_sub_group=f"{tile}_{model_name}_{seed}_{chunk}",
                            task_dep_dico={"region_tasks": [target_model]},
                        )


class GenerateRegressionMetrics(Step):
    """regression metrics"""

    resources_block_name = "GenerateRegressionMetrics"

    @classmethod
    def step_description(cls) -> str:
        return "Generate regression metrics"

    def __init__(self, cfg: str, cfg_resources_file: Path | None):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        data_field = rcf.ReadConfigFile(self.cfg).get_param("chain", "data_field")
        output_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path")
        runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")
        tiles = rcf.ReadConfigFile(self.cfg).get_param("chain", "list_tile").split(" ")
        merge_run = rcf.ReadConfigFile(self.cfg).get_param(
            "multi_run_fusion", "merge_run"
        )
        for tile in tiles:
            for seed in range(runs):
                task_name = f"regression_metrics_{tile}_seed_{seed}"
                validation_data = str(
                    Path(output_path) / "dataAppVal" / f"{tile}_seed_{seed}_val.sqlite"
                )
                final_image = str(
                    Path(output_path) / "final" / "TMP" / f"{tile}_seed_{seed}.tif"
                )
                metrics_file = str(
                    Path(output_path) / "final" / f"{tile}_seed{seed}_metrics.csv"
                )

                # TODO if config.arg_train.sampling_validation == True
                # do not extract values from final images
                # instead give the path of the prediction applied on
                # validation data
                metrics_parameters = RegressionMetricsParameters(
                    data_field=data_field,
                )
                task_params = {
                    "f": generate_metrics,
                    "final_image": final_image,
                    "validation_data": validation_data,
                    "metrics_parameters": metrics_parameters,
                    "split_field": FAKE_CLASS,
                    "output_file": metrics_file,
                }
                task = self.I2Task(
                    task_name=task_name,
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters=task_params,
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="final_report",
                    task_sub_group=tile + "_" + str(seed),
                    task_dep_dico=(
                        {"mosaic": ["merge_run_mosaic", "merge_run_mosaic_confidence"]}
                        if (merge_run and (runs > 1))
                        else {"mosaic": ["mosaic"]}
                    ),
                )


class GenerateRegressionMetricsSummary(Step):
    """regression metrics"""

    resources_block_name = "GenerateRegressionMetricsSummary"

    @classmethod
    def step_description(cls) -> str:
        return "Generate regression metrics for multi tile and multi run configurations"

    def __init__(self, cfg: str, cfg_resources_file: Path | None):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        data_field = rcf.ReadConfigFile(self.cfg).get_param("chain", "data_field")
        output_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path")
        runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")
        tiles = rcf.ReadConfigFile(self.cfg).get_param("chain", "list_tile").split(" ")

        if runs > 1:
            for tile in tiles:
                dep_values = [tile + "_" + str(seed) for seed in range(runs)]
                # no need to merge metrics if single seed

                task_params = {
                    "f": merge_metrics,
                    "output_path": output_path,
                    "seeds": runs,
                    "tile": tile,
                }

                task = self.I2Task(
                    task_name="merge_metrics" + tile,
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters=task_params,
                    task_resources=self.get_resources(),
                )

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="final_report",
                    task_sub_group="merge_metrics_" + tile,
                    task_dep_dico={"final_report": dep_values},
                )

        if len(tiles) > 1:
            for seed in range(runs):
                task_name = f"regression_metrics_mosaic_seed_{seed}"
                validation_data = [
                    str(
                        Path(output_path)
                        / "dataAppVal"
                        / f"{tile}_seed_{seed}_val.sqlite"
                    )
                    for tile in tiles
                ]
                final_image = str(
                    Path(output_path) / "final" / f"Regression_Seed_{seed}.tif"
                )
                metrics_file = str(
                    Path(output_path) / "final" / f"mosaic_seed{seed}_metrics.csv"
                )

                # TODO if config.arg_train.sampling_validation == True
                # do not extract values from final images
                # instead give the path of the prediction applied on
                # validation data
                metrics_parameters = RegressionMetricsParameters(
                    data_field=data_field, mode="w", column=True
                )
                task_params = {
                    "f": generate_multi_tile_metrics,
                    "final_image": final_image,
                    "validation_data": validation_data,
                    "split_field": FAKE_CLASS,
                    "output_file": metrics_file,
                    "metrics_parameters": metrics_parameters,
                }

                task = self.I2Task(
                    task_name=task_name,
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters=task_params,
                    task_resources=self.get_resources(),
                )

                if runs > 1:
                    dep = ["merge_metrics_" + tile for tile in tiles]
                else:
                    dep = []
                    for tile in tiles:
                        for seed in range(runs):
                            dep.append(tile + "_" + str(seed))

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="final_report",
                    task_sub_group="regression_metrics_mosaic_seed_" + str(seed),
                    task_dep_dico={"final_report": dep},
                )

            if runs > 1:
                dep_values = [
                    "regression_metrics_mosaic_seed_" + str(seed)
                    for seed in range(runs)
                ]
                # no need to merge metrics if single seed

                task_params = {
                    "f": merge_metrics,
                    "output_path": output_path,
                    "seeds": runs,
                    "tile": "mosaic",
                }

                task = self.I2Task(
                    task_name="merge_metrics_mosaic",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters=task_params,
                    task_resources=self.get_resources(),
                )

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="final_report",
                    task_sub_group="merge_metrics_mosaic",
                    task_dep_dico={"final_report": dep_values},
                )
