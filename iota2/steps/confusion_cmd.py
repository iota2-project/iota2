#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Produce confusion matrix"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.steps import iota2_step
from iota2.typings.i2_types import ConfusionMatrixAppParameters
from iota2.validation import gen_confusion_matrix as GCM
from iota2.vector_tools.vector_functions import get_reverse_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class ConfusionCmd(iota2_step.Step):
    """Step for computing the confusion matrix"""

    resources_block_name = "gen_confusion_matrix"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
    ):
        """Initialize the step."""
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        self.output_path = parameters.get_param("chain", "output_path")
        runs = parameters.get_param("arg_train", "runs")
        # if the nomenclature is not int compatible i2label
        # is used in the function
        data_field = parameters.get_param("chain", "data_field")
        ground_truth = parameters.get_param("chain", "ground_truth")
        user_data_field = parameters.get_param("chain", "data_field")
        i2_labels_to_user_labels = get_reverse_encoding_labels_dic(
            ground_truth, user_data_field
        )
        self.comparison_mode = parameters.get_param(
            "arg_classification", "boundary_comparison_mode"
        )

        for seed in range(runs):
            for tile in self.tiles:
                classif_list, out_csv_list = self.entry_list(tile, seed)
                ref_vector = str(
                    Path(self.output_path)
                    / "dataAppVal"
                    / f"{tile}_seed_{seed}_val.sqlite"
                )
                ram = 1024 * get_ram(self.resources.ram)
                params_list = [
                    ConfusionMatrixAppParameters(
                        in_classif=in_classif,
                        out_csv=out_csv,
                        data_field=data_field,
                        ref_vector=ref_vector,
                        ram=ram,
                    )
                    for in_classif, out_csv in zip(classif_list, out_csv_list)
                ]

                task = self.I2Task(
                    task_name=f"confusion_{tile}_seed_{seed}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": GCM.run_gen_conf_matrix,
                        "conf_matrix_params_list": params_list,
                        "labels_table": i2_labels_to_user_labels,
                    },
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks_seed",
                    task_sub_group=f"{tile}_{seed}",
                    task_dep_dico={"mosaic": ["mosaic"]},
                )

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "generate confusion matrix by tiles"
        return description

    def entry_list(self, tile: str, seed: int) -> tuple[list[str], list[str]]:
        """Provide list of file according to step parameters."""
        if self.comparison_mode:
            classif_list = [
                str(
                    Path(self.output_path)
                    / "final"
                    / "standard"
                    / f"Classif_Seed_{seed}.tif"
                ),
                str(
                    Path(self.output_path)
                    / "final"
                    / "boundary"
                    / f"Classif_Seed_{seed}.tif"
                ),
            ]
            out_csv_list = [
                str(
                    Path(self.output_path)
                    / "final"
                    / "standard"
                    / "TMP"
                    / f"Classif_{tile}_seed_{seed}.csv"
                ),
                str(
                    Path(self.output_path)
                    / "final"
                    / "boundary"
                    / "TMP"
                    / f"Classif_{tile}_seed_{seed}.csv"
                ),
            ]
        else:
            classif_list = [
                str(Path(self.output_path) / "final" / f"Classif_Seed_{seed}.tif")
            ]
            out_csv_list = [
                str(
                    Path(self.output_path)
                    / "final"
                    / "TMP"
                    / f"Classif_{tile}_seed_{seed}.csv"
                )
            ]
        return classif_list, out_csv_list
