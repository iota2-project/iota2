#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the user inference task."""
import logging
from pathlib import Path

from iota2.classification.external_classification import apply_mask_to_map
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class ApplyMaskToMap(iota2_step.Step):
    """Class for apply a mask to a map."""

    resources_block_name = "classifications"

    def __init__(self, cfg: str, cfg_resources_file: Path | None):
        """Initialize the class.

        Parameters
        ----------
        cfg:
            configuration file
        cfg_resources_file:
            configuration resource file
        """
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        runs = parameters.get_param("arg_train", "runs")
        for (
            region_name,
            model_meta,
        ) in self.spatial_models_distribution_classify.items():
            for seed in range(runs):
                for tile in model_meta["tiles"]:
                    task_name = (
                        f"apply_mask_classification_{tile}_" f"model_{region_name}"
                    )

                    params = {
                        "f": apply_mask_to_map,
                        "tile": tile,
                        "region": region_name,
                        "path_to_classif": str(
                            Path(output_path) / "classif" / "full_pred"
                        ),
                        "output_folder": str(Path(output_path) / "classif"),
                        "mask": str(
                            Path(output_path)
                            / "classif"
                            / "MASK"
                            / f"MASK_region_{region_name}_{tile}.tif"
                        ),
                        "runs": runs,
                    }
                    task = self.I2Task(
                        task_name=task_name,
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters=params,
                        task_resources=self.get_resources(),
                    )
                    # vient apres lae retuillage de la donnée
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="tile_tasks_model_mode",
                        task_sub_group=f"{tile}_{region_name}_{seed}",
                        task_dep_dico={
                            "merge_maps_by_tiles_and_regions": [
                                "merge_maps_by_tiles " f"{tile} {region_name} {seed}"
                                for seed in range(runs)
                            ]
                        },
                    )

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "Generate user classifications"
        return description
