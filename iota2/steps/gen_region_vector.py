#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the region shape generation task"""

import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import tile_area as area
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class GenRegionVector(iota2_step.Step):
    """Class for launching the region shape generation step"""

    resources_block_name = "regionShape"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        task = self.I2Task(
            task_name="region_generation",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": area.generate_region_shape,
                "envelope_directory": str(Path(output_path) / "envelope"),
                "output_region_file": parameters.get_param("chain", "region_path"),
                "out_field_name": parameters.get_param("chain", "region_field"),
                "i2_output_path": output_path,
                "working_directory": working_directory,
            },
            task_resources=self.get_resources(),
        )
        self.add_task_to_i2_processing_graph(
            task,
            task_group="vector",
            task_sub_group="vector",
            task_dep_dico={"vector": ["vector"]},
        )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Generate a region vector"
        return description
