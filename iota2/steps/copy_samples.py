#!/usr/bin/env python3


# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step to copy samples"""

import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import data_augmentation
from iota2.steps import iota2_step
from iota2.typings.i2_types import DBInfo
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class CopySamples(iota2_step.Step):
    """Class for copying samples"""

    resources_block_name = "samplesManagement"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        data_field = self.i2_const.re_encoding_label_name
        sample_management = parameters.get_param("arg_train", "sample_management")
        nb_runs = parameters.get_param("arg_train", "runs")

        ground_truth = parameters.get_param("chain", "ground_truth")
        user_data_field = parameters.get_param("chain", "data_field")
        user_labels_to_i2_labels = get_re_encoding_labels_dic(
            ground_truth, user_data_field
        )
        # inputs
        models_by_seeds: dict[int, dict[str, list[str]]] = {}
        for seed in range(nb_runs):
            models_by_seeds[seed] = {"dep": [], "files": []}
            for model_name, _ in self.spatial_models_distribution.items():
                models_by_seeds[seed]["dep"].append(f"model_{model_name}_seed_{seed}")
                models_by_seeds[seed]["files"].append(
                    str(
                        Path(output_path)
                        / "learningSamples"
                        / f"Samples_region_{model_name}_seed{seed}_learn.sqlite",
                    )
                )
        for seed in range(nb_runs):
            task = self.I2Task(
                task_name=f"transfert_samples_seed_{seed}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": data_augmentation.data_augmentation_by_copy,
                    "database": DBInfo(
                        data_field.lower(),
                        models_by_seeds[seed]["files"],
                        user_labels_to_i2_labels,
                    ),
                    "csv_path": sample_management,
                    "working_directory": working_directory,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="seed_tasks",
                task_sub_group=seed,
                task_dep_dico={"region_tasks": models_by_seeds[seed]["dep"]},
            )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Copy samples between models according to user request"
        return description
