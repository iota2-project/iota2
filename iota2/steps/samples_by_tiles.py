#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step for sampling."""
import logging
from pathlib import Path

from iota2.common.file_utils import sort_by_first_elem
from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import samples_selection
from iota2.steps import iota2_step
from iota2.vector_tools.vector_functions import get_geometry

LOGGER = logging.getLogger("distributed.worker")


class SamplesByTiles(iota2_step.Step):
    """Provide the number of samples by tiles."""

    resources_block_name = "samplesSelection_tiles"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        """Initialize the step.

        Parameters
        ----------
        cfg:
            configuration file path
        cfg_resources_file:
            resources configuration file path
        working_directory:
            temporary folder to store results
        """
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        nb_runs = parameters.get_param("arg_train", "runs")

        tiles_model_distribution_tmp = []
        for model_name, model_meta in self.spatial_models_distribution.items():
            for tile in model_meta["tiles"]:
                tiles_model_distribution_tmp.append((tile, model_name))
        tiles_model_distribution_tmp = sort_by_first_elem(tiles_model_distribution_tmp)

        tiles_model_distribution = {}
        for tile_name, model_list in tiles_model_distribution_tmp:
            tiles_model_distribution[tile_name] = [
                f"{tile_name}_{model_name}_{seed}"
                for model_name in model_list
                for seed in range(nb_runs)
            ]
        model_distribution = {}
        for tile_name, model_list in tiles_model_distribution_tmp:
            model_distribution[tile_name] = [
                f"model_{model_name}_seed_{seed}"
                for model_name in model_list
                for seed in range(nb_runs)
            ]
        for tile in self.tiles:
            dep: dict[str, list[str]] | None
            if get_geometry(parameters.get_param("chain", "ground_truth")) == "POINT":
                dep = {}
                sample_sel_directory = str(Path(output_path) / "formattingVectors")
            else:
                sample_sel_directory = str(Path(output_path) / "samplesSelection")
                dep_key = "region_tasks"
                dep_values = model_distribution[tile]
                dep = {dep_key: dep_values}
            task = self.I2Task(
                task_name=f"merge_samples_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": samples_selection.prepare_selection,
                    "sample_sel_directory": sample_sel_directory,
                    "output_sample_sel_directory": str(
                        Path(output_path) / "samplesSelection"
                    ),
                    "tile_name": tile,
                    "working_directory": working_directory,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task, task_group="tile_tasks", task_sub_group=tile, task_dep_dico=dep
            )

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "Split pixels selected to learn models by tiles"
        return description
