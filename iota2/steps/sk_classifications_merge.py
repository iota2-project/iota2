#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch scikit classifications merge task"""
import logging
from pathlib import Path
from typing import Any, Literal

import iota2.common.raster_utils as ru
from iota2.classification.scikit_prediction import merge_sk_classifications
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")

# each can have either 2 or 3 elements, whether the 'probamap' file is used or not
RastersList = tuple[list[str], list[str]] | tuple[list[str], list[str], list[str]]
MosaicFilesList = tuple[str, str] | tuple[str, str, str]


class ScikitClassificationsMerge(iota2_step.Step):
    """Class for launching scikit classifications merge step"""

    resources_block_name = "mergeClassifications"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        mode: Literal["classif", "regression"] = "classif",
    ):
        """
        mode
            "classif" or "regression"
            in regression mode, float are used instead of int
        """
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.output_path = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "output_path"
        )
        runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")

        self.enable_proba_map = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_classification", "enable_probability_map"
        )

        self.chunk_size_mode = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "chunk_size_mode"
        )
        self.number_of_chunks = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "number_of_chunks"
        )
        self.chunk_config = ru.ChunkConfig(
            chunk_size_mode=self.chunk_size_mode,
            number_of_chunks=self.number_of_chunks,
            chunk_size_x=rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "chunk_size_x"
            ),
            chunk_size_y=rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "chunk_size_y"
            ),
            padding_size_x=rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "padding_size_x"
            ),
            padding_size_y=rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "padding_size_y"
            ),
        )

        for (
            model_name,
            model_meta,
        ) in self.spatial_models_distribution_classify.items():
            for seed in range(runs):
                for tile in model_meta["tiles"]:
                    task_parameters = self.get_param(model_name, seed, tile, mode)
                    task = self.I2Task(
                        task_name=(
                            f"classif_{tile}_model_{model_name}_" f"seed_{seed}_mosaic"
                        ),
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters=task_parameters,
                        task_resources=self.get_resources(),
                    )

                    if self.chunk_size_mode == "user_fixed":
                        rois, _ = ru.get_raster_chunks_boundaries(
                            Path(self.output_path)
                            / "features"
                            / tile
                            / "tmp"
                            / "MaskCommunSL.tif",
                            self.chunk_config,
                        )
                        self.number_of_chunks = len(rois)
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="tile_tasks_model_mode",
                        task_sub_group=f"{tile}_{model_name}_{seed}",
                        task_dep_dico={
                            "tile_tasks_model_mode": [
                                f"{tile}_{model_name}_{seed}_{chunk}"
                                for chunk in range(self.number_of_chunks)
                            ]
                        },
                    )

    def get_param(
        self,
        model_name: str,
        seed: int,
        tile_name: str,
        mode: Literal["classif", "regression"] = "classif",
    ) -> dict[str, Any]:
        """
        mode
            "classif" or "regression"
        """
        param = None
        classification_files = []
        confidence_files = []
        proba_map_files = []
        if mode == "classif":
            prefix = "Classif"
        else:  # regression
            prefix = "Regression"
        if self.chunk_size_mode == "user_fixed":
            rois, _ = ru.get_raster_chunks_boundaries(
                Path(self.output_path)
                / "features"
                / tile_name
                / "tmp"
                / "MaskCommunSL.tif",
                self.chunk_config,
            )
            self.number_of_chunks = len(rois)
        for chunk in range(self.number_of_chunks):
            classification_files.append(
                str(
                    Path(self.output_path)
                    / "classif"
                    / f"{prefix}_{tile_name}_model_{model_name}"
                    f"_seed_{seed}_SUBREGION_{chunk}.tif"
                )
            )
            confidence_files.append(
                str(
                    Path(self.output_path)
                    / "classif"
                    / f"{tile_name}_model_{model_name}"
                    f"_confidence_seed_{seed}_SUBREGION_{chunk}.tif"
                )
            )
            if self.enable_proba_map:
                proba_map_files.append(
                    str(
                        Path(self.output_path)
                        / "classif"
                        / f"PROBAMAP_{tile_name}_model_{model_name}"
                        f"_seed_{seed}_SUBREGION_{chunk}.tif"
                    )
                )

        classif_mosaic = str(
            Path(self.output_path)
            / "classif"
            / f"{prefix}_{tile_name}_model_{model_name}_seed_{seed}.tif"
        )
        confidence_mosaic = str(
            Path(self.output_path)
            / "classif"
            / f"{tile_name}_model_{model_name}_confidence_seed_{seed}.tif"
        )

        rasters_to_merge: RastersList
        mosaic_file: MosaicFilesList

        if self.enable_proba_map:
            proba_map_mosaic = str(
                Path(self.output_path)
                / "classif"
                / f"PROBAMAP_{tile_name}_model_{model_name}_seed_{seed}.tif"
            )

            rasters_to_merge = (classification_files, confidence_files, proba_map_files)
            mosaic_file = (classif_mosaic, confidence_mosaic, proba_map_mosaic)
        else:
            rasters_to_merge = (classification_files, confidence_files)
            mosaic_file = (classif_mosaic, confidence_mosaic)

        param = {
            "f": merge_sk_classifications,
            "rasters_to_merge": rasters_to_merge,
            "mosaic_file": mosaic_file,
            "mode": mode,
        }
        return param

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Merge tile's classification's part"
        return description
