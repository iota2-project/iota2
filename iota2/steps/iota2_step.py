#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""module containing base class for iota2's steps"""
# pylint: disable=too-few-public-methods

import logging
import os
import pickle
import random
import threading
import time
import traceback
import uuid
import warnings
from collections.abc import Callable, Iterator, Mapping
from functools import partial, wraps
from pathlib import Path
from subprocess import PIPE, STDOUT, Popen
from typing import Any, TypeVar
from weakref import WeakValueDictionary

import dask
import dill
from dask.delayed import Delayed
from dask.distributed import Client, LocalCluster
from dask_jobqueue import PBSCluster
from distributed.scheduler import KilledWorker, Scheduler

import iota2.common.i2_constants as i2_const
from iota2.configuration_files import read_config_file as rcf
from iota2.steps.i2_job_helpers import (
    FunctionDescriptor,
    JobFileDescriptor,
    JobHelper,
    TaskResources,
)
from iota2.typings.i2_types import AvailSchedulers

warnings.simplefilter(action="ignore", category=FutureWarning)

lock = threading.Lock()
dask.config.set({"distributed.comm.timeouts.connect": "300000s"})
dask.config.set({"distributed.scheduler.allowed-failures": 0})
dask.config.set({"worker-memory-pause": False})
dask.config.set({"distributed.worker.memory.pause": False})

LOGGER = logging.getLogger("distributed.worker")

I2_CONST = i2_const.Iota2Constants()

S = TypeVar("S", bound="Step")


def run_as_process(
    cmd: str, stdout: str, stderr: str, env: Mapping[str, str] | None = None
) -> int:
    """
    Launch a system command and raise an exception if fail

    Parameters
    ----------
    cmd:
        the system command to be launched
    stdout:
        file to log stdout
    stderr
        file to log stderr
    env:
        the environ variable (use os.environ if not set)
    """
    if env is None:
        env = os.environ
    start = time.time()
    with open(stdout, "w", encoding="utf-8") as out, open(
        stderr, "w", encoding="utf-8"
    ) as err:
        with Popen(cmd, env=env, shell=True, stdout=out, stderr=err) as proc:
            # Get output as strings
            out_msg, err_msg = proc.communicate()

            # Get return code
            rtc = proc.returncode
    end = time.time()
    with open(stdout, "a", encoding="UTF-8") as out:
        out.write("\n------- TASK SUMMARY -------\n")
        out.write(f"EXECUTION TIME : {end - start} seconds\n")
        out.write("----------------------------\n")
    # Log error code
    if rtc != 0:
        exception_msg = (
            f"Launch command fail : {cmd} \n {out_msg.decode()} \n {err_msg.decode()}"
        )
        raise Exception(exception_msg)
    return rtc


def parse_ram(ram_str: str) -> tuple[str, str, str]:
    """
    Parse RAM string
    """
    ram = ram_str.lower().replace(" ", "")
    if "gb" in ram:
        spliter = "gb"
    elif "mb" in ram:
        spliter = "mb"
    else:
        raise ValueError("can't parse ram parameter")
    ram_float, ram_unit = ram.split(spliter)

    return ram_float, ram_unit, spliter


def ram_str_to_gb_float(ram_str: str) -> int:
    """convert ram str to a float representation in gb

    ram_str_to_gb_float('10gb')
    > 10
    """
    ram_value, _, spliter = parse_ram(ram_str)

    if spliter == "mb":
        ram_float = int(ram_value) * 1024
    elif spliter == "gb":
        ram_float = int(ram_value)
    else:
        raise RuntimeError("RAM resources must have mb or gb units.")
    return ram_float


def increase_resources(cpu: int, ram: str, walltime: str) -> tuple[int, str, int, str]:
    """definition of the strategy for increasing resources in case
       of killed task due to resources consumption. The strategy is
       : double resources cpu/ram and raise by 25% the walltime

    Parameters
    ----------
    cpu :
        initial cpu
    ram :
        initial ram expected format : '10gb' or '10mb'
    walltime :
        initial walltime, expected format : 'HH:MM:SS'

    Notes
    -----
    the returned tuple is composed as the following
    (new_cpu, new_ram, new_walltime)

    """
    # cpu
    new_cpu = cpu * 2

    # ram
    (ram_value, _, spliter) = parse_ram(ram)
    new_ram = f"{int(ram_value) * 2}{spliter}"
    if spliter == "mb":
        ram_float = int(ram_value) * 2 * 1024
    elif spliter == "gb":
        ram_float = int(ram_value) * 2
    else:
        raise RuntimeError("RAM resources must have mb or gb units.")

    # walltime
    hours, minutes, seconds = walltime.split(":")
    walltime_sec = 3600 * float(hours) + 60 * float(minutes) + float(seconds)
    increase_walltime = 1.25 * walltime_sec
    new_walltime = time.strftime("%H:%M:%S", time.gmtime(increase_walltime))

    return new_cpu, new_ram, ram_float, new_walltime


def log_traceback(client: Client, logger_name: str, task_tb: str) -> None:
    """send traceback to workers

    Parameters
    ----------
    client : dask.client
        scheduler client
    logger_name
        logger to log on
    task_tb : traceback
        traceback
    """
    logger = logging.getLogger(logger_name)
    client.run(logger.log, logging.ERROR, task_tb)


def set_working_dir_parameter(t_kwargs: dict, worker_working_dir: str) -> dict:
    """set working directory in tasks arguments"""
    new_t_kwargs = t_kwargs.copy()
    working_dir_names = [
        "working_directory",
        "pathWd",
        "working_directory",
        "working_dir",
        "path_wd",
        "WORKING_DIR",
    ]
    for working_dir_name in working_dir_names:
        if working_dir_name in new_t_kwargs:
            new_t_kwargs[working_dir_name] = worker_working_dir
    return new_t_kwargs


# the step class requires more than 7 instance attributes
class Step:  # pylint: disable=too-many-instance-attributes
    """
    iota2 step base class
    """

    class I2Task:
        """Class to modelling a iota2 task"""

        i2_tasks_container: dict[str, list[str]] = {}

        def __init__(
            self,
            task_name: str,
            log_dir: str,
            execution_mode: AvailSchedulers,
            task_parameters: dict,
            task_resources: TaskResources | None = None,
        ):
            if task_resources is None:
                task_resources = TaskResources()

            self.task_name = task_name
            if not isinstance(task_name, str):
                raise ValueError("parameter 'task_name' must be a string")
            if " " in self.task_name:
                self.task_name = self.task_name.replace(" ", "")
                print(
                    f"WARNING: task_name = {task_name} contains whitespaces, "
                    f"casted as {self.task_name}"
                )
            if "-" in self.task_name:
                self.task_name = self.task_name.replace("-", "")
                print(
                    "WARNING: task_name contains '-' character, "
                    "automatically removed"
                )
            self.log_err = str(Path(log_dir) / f"{task_name}.err")
            self.log_out = str(Path(log_dir) / f"{task_name}.out")
            self.execution_mode = execution_mode
            self.parameters = task_parameters
            self.resources = task_resources

        def is_addable_to_graph(self, graph_name: str) -> bool:
            """
            check if the task is not already present in the list of tasks.
            Indeed, a task must be unique in all graphs
            """
            if graph_name not in self.i2_tasks_container:
                self.i2_tasks_container[graph_name] = [self.task_name]
                is_addable = True
            else:
                buff = []
                for (
                    containers_name,
                    container_tasks,
                ) in self.i2_tasks_container.items():
                    container_name_no_uuid = "_".join(containers_name.split("_")[0:-1])
                    graph_name_no_uuid = "_".join(graph_name.split("_")[0:-1])
                    # allow the use of iota2.run(), if not we check
                    # containers of an other run.
                    if container_name_no_uuid == graph_name_no_uuid:
                        continue
                    buff.append(self.task_name not in container_tasks)
                is_addable = all(buff)
                if is_addable:
                    self.i2_tasks_container[graph_name].append(self.task_name)
            return is_addable

    tiles: list[str] = []
    chunks_intersections: dict = {}
    spatial_models_distribution: dict[str, dict[str, list[str]]] = {}
    spatial_models_distribution_no_sub_splits: dict[str, dict[str, int]] = {}

    spatial_models_distribution_classify: dict[str, dict[str, Any]] = {}
    spatial_models_distribution_no_sub_splits_classify: dict[str, dict[str, Any]] = {}

    # status file
    tasks_status_file = None

    # hardware computing limits (ram in gb)
    ram_hardware_limits: int = 0
    cpu_hardware_limits: int = 0

    # number of allowed task failure
    allowed_task_failure: int = 0

    # other run state
    older_i2_state: dict[str, str] = {}

    execution_mode: AvailSchedulers = "localCluster"

    tasks_graph: dict[str, Delayed | dict[str, Delayed]] = {}
    tasks_graph_figure: dict[str, Delayed | dict[str, Delayed]] = {}

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        resources_block_name: str | None = None,
    ):

        # attributes
        self.cfg = cfg
        self.step_name = self.build_step_name()
        self.step_group = ""
        slurm_static_params = rcf.ReadConfigFile(self.cfg).get_section_as_dataclass(
            "slurm"
        )
        self.job_helper = JobHelper(
            cfg_resources_file, resources_block_name, slurm_static_params
        )
        self.container_name: str
        self.log_err: str
        self.log_out: str

        # get resources needed
        self.resources = self.job_helper.resources

        # define log path
        output_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path")
        self.logger_lvl = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "logger_level"
        )

        log_dir = str(Path(output_path) / "logs")
        self.log_step_dir = str(Path(log_dir) / f"{self.step_name}")

        # step_tasks is a list containing
        # delayed task_launcher of this step's dependencies
        self.step_tasks: list[Delayed] = []
        self.step_tasks_figure: list[Delayed] = []
        self.i2_const = i2_const.Iota2Constants()
        step_container.append(self)

    def get_resources(self) -> TaskResources:
        """get resources for a tasks (parsed from a resources
        configuration file)"""
        return self.resources

    def set_container_name(self, name: str) -> None:
        """define the name of the container, useful to populate the
        right task graph"""
        self.container_name = name

    @classmethod
    def set_hardware_limits(cls, cpu: int, ram: int) -> None:
        """set maximum cpu and ram (Gb) allowed for a task"""
        cls.ram_hardware_limits = ram
        cls.cpu_hardware_limits = cpu

    @classmethod
    def set_allowed_task_failure(cls, allowed_task_failure: int) -> None:
        """Set if task can be retried"""
        cls.allowed_task_failure = allowed_task_failure

    @classmethod
    def restart_from_i2_state(cls, older_i2_state_file: str | None = None) -> None:
        """relaunch failed or not launched tasks"""
        if older_i2_state_file is None:
            raise ValueError(f"{older_i2_state_file} can't be None")
        if Path(older_i2_state_file).exists():
            with open(older_i2_state_file, "rb") as tasks_status_file:
                try:
                    cls.older_i2_state = pickle.load(tasks_status_file)
                except EOFError as exc:
                    raise EOFError(f"{older_i2_state_file} is empty") from exc

    @classmethod
    def set_tasks_status_directory(cls, directory_path: str) -> None:
        """set directory which will contains the iota2 state status"""
        cls.tasks_status_file = str(
            Path(directory_path) / I2_CONST.i2_tasks_status_filename
        )

    @classmethod
    def set_chunks_intersections(cls, chunks_intersections: dict) -> None:
        """Inform which chunks do not intersect learning polygons."""
        cls.chunks_intersections = chunks_intersections

    @classmethod
    def set_models_spatial_information(
        cls, tiles: list[str], spatial_models_distribution: dict
    ) -> None:
        """define models distribution across tiles

        TODO : move to I2Classification builder.
        """
        cls.tiles = tiles
        cls.spatial_models_distribution = spatial_models_distribution.copy()
        cls.spatial_models_distribution_no_sub_splits = {}
        for model_name, model_meta in spatial_models_distribution.items():
            model = model_name
            if "f" in model_name:
                model = model_name.split("f")[0]

            if model not in cls.spatial_models_distribution_no_sub_splits:
                cls.spatial_models_distribution_no_sub_splits[model] = model_meta
                cls.spatial_models_distribution_no_sub_splits[model]["nb_sub_model"] = 0
            cls.spatial_models_distribution_no_sub_splits[model]["nb_sub_model"] += 1

    @classmethod
    def get_exec_graph(cls) -> Delayed:
        """Return the execution graph of the last step append."""

        def ending(dependencies: list) -> str:  # pylint: disable=unused-argument
            """Inconsequential function.

            Used to add "ending" node to graph
            it takes the list of dependencies in input
            and returns a string that is ignored.
            """
            return "ending"

        return dask.delayed(ending)(step_container[-1].step_tasks)

    @classmethod
    def get_figure_graph(cls) -> Delayed:
        """get the dask graph where each node are labelized by task's name"""
        return dask.delayed(ChangeName("ending")(cls.task_launcher))(
            step_container[-1].step_tasks_figure,
            task_name="",
            log_err=None,
            log_out=None,
        )

    def init_log(self) -> None:
        """function use to init logging in every workers"""
        log_formatter = logging.Formatter(
            "%(asctime)s [%(name)s] [%(levelname)s] - %(message)s"
        )
        logging.basicConfig(format="%(asctime)s %(message)s")
        root_logger = logging.getLogger("distributed.worker")

        root_logger.setLevel(self.logger_lvl)
        for handler in root_logger.handlers:
            handler.setFormatter(log_formatter)

    def init_log_local(self) -> None:
        """function use to init logging in every workers"""
        log_formatter_local = logging.Formatter(
            "%(asctime)s [%(name)s] [%(levelname)s] - %(message)s"
        )
        root_logger = logging.getLogger("distributed.worker")
        root_logger.setLevel(self.logger_lvl)

        if Path(self.log_out).exists():
            Path(self.log_out).unlink()
        file_handler = logging.FileHandler(self.log_out)
        file_handler.setFormatter(log_formatter_local)
        file_handler.setLevel(self.logger_lvl)
        root_logger.addHandler(file_handler)

        if Path(self.log_err).exists():
            Path(self.log_err).unlink()
        file_handler = logging.FileHandler(self.log_err)
        file_handler.setFormatter(log_formatter_local)
        file_handler.setLevel(self.logger_lvl)
        root_logger.addHandler(file_handler)

    def save_task_status(self, task_name: str, task_status: str) -> None:
        """save task state in a shared dictionary"""
        if not self.tasks_status_file:
            raise RuntimeError(f"No tasks_status_file for task {task_name}")

        success = False
        max_nb_fail = 10
        for nb_try in range(max_nb_fail):
            try:
                time.sleep(random.randint(1, 10))
                with lock:
                    if not Path(self.tasks_status_file).exists():
                        with open(self.tasks_status_file, "wb") as tasks_status_file:
                            pickle.dump({task_name: task_status}, tasks_status_file)
                    else:
                        with open(self.tasks_status_file, "rb") as tasks_status_file:
                            task_completed_dic = pickle.load(tasks_status_file)

                        task_completed_dic[task_name] = task_status

                        with open(self.tasks_status_file, "wb") as tasks_status_file:
                            pickle.dump(task_completed_dic, tasks_status_file)
                    success = True
                    break
            except EOFError:
                print(
                    f"EOFError DETECTECTED, task name {task_name}, "
                    f"nb try : {nb_try}"
                )
                time.sleep(random.randint(1, 10))
        if not success:
            raise EOFError(f"EOFError DETECTECT, task name {task_name}")

    def send_task(
        self,
        scheduler_type: AvailSchedulers,
        task_kwarguments: dict,
        log_err: str,
        log_out: str,
        pbs_worker_name: str,
        resources: TaskResources,
    ) -> tuple[str, bool]:
        """
        Send task to the appropriate dask scheduler

        Parameters
        ----------
        scheduler_type:
            define which kind of scheduling will be used
            choices are 'PBS', 'Slurm', 'cluster', 'localCluster' and 'debug'
        task_kwarguments:
            Dictionary containing the task function and its arguments
        log_err:
            file to store error flux
        log_out:
            file to store std flux
        pbs_worker_name:
            task name
        resources:
            task resources

        Notes
        -----
        the returned type is formated as
        (task_status, killed_worker)

        * task_status : indicate one of available task's status, 'done',
                        'failed' or 'unlaunchable'

        * killed_worker : inform if the task has been killed by the scheduler
                          due to resources consumption
        """
        task_kwargs = task_kwarguments.copy()

        logging.captureWarnings(True)
        log_dir = Path(log_err).parent
        if not log_dir.exists():
            log_dir.mkdir(parents=True, exist_ok=True)

        func = task_kwargs["f"]
        task_kwargs.pop("f", None)
        f_kwargs = task_kwargs

        # here we launch the fonction with all the arguments of kwargs
        killed_worker = False
        task_status = None
        if scheduler_type in ["PBS", "Slurm"]:
            self.init_log()
            job_file = log_err.replace(
                ".err",
                f".{self.job_helper.inventory_jobfile_helper[scheduler_type.lower()].extension}",
            )
            self.job_helper.write(
                scheduler_type.lower(),
                JobFileDescriptor(Path(job_file), pbs_worker_name, log_err, log_out),
                FunctionDescriptor(func, f_kwargs, self.logger_lvl),
            )
            cmd = (
                f"{self.job_helper.inventory_jobfile_helper[scheduler_type.lower()].submit_cmd}"
                f" {job_file}"
            ).split(" ")
            cmd = [val for val in cmd if val != ""]
            with Popen(cmd, shell=False, stdout=PIPE, stderr=STDOUT) as process:
                process.wait()
                stdout, _ = process.communicate()
            task_status, killed_worker = self.job_helper.get_task_status(
                scheduler_type.lower(), log_err, log_out, str(stdout)
            )
        if scheduler_type == "cluster":
            killed_worker, task_status = self._launch_function_in_cluster_mode(
                f_kwargs,
                func,
                killed_worker,
                log_err,
                log_out,
                pbs_worker_name,
                resources,
            )
        if scheduler_type == "localCluster":
            killed_worker, task_status = self._launch_function_in_local_cluster_mode(
                f_kwargs,
                func,
                killed_worker,
                log_err,
                log_out,
                pbs_worker_name,
                resources,
            )
        if scheduler_type == "debug":
            self.log_err = log_err
            self.log_out = log_out
            self.init_log_local()
            try:
                task_status = "done"
                func(**f_kwargs)
            # catch any other type of exception
            except Exception:  # pylint: disable=broad-except
                traceback.print_exc()
                task_status = "failed"
        assert task_status is not None
        return task_status, killed_worker

    def _launch_function_in_local_cluster_mode(
        self,
        f_kwargs: dict,
        func: Callable,
        killed_worker: bool,
        log_err: str,
        log_out: str,
        pbs_worker_name: str,
        resources: TaskResources,
    ) -> tuple[bool, str]:
        """
        Launch the selected function using the cluster scheduler_type

        Parameters
        ----------
        f_kwargs: dict
            Dictionary containing the task function and it's arguments
        func: Callable
            Function to be launched
        killed_worker: bool
            Flag indicating if the worker was killed. False by defaults, sets to True if the worker
            is killed during the process.
        log_err: str
            File to store error flux
        log_out: str
            File to store std flux
        pbs_worker_name: str
            Task name
        resources: TaskResources
            Task resources

        Returns
        -------
        tuple[bool, str]:
            Tuple containing:
                - The state of the worker as a bool (killed or not)
                - The status of the task as a string
        """
        cluster = LocalCluster(
            n_workers=1,
            threads_per_worker=resources.nb_cpu,
            memory_limit=resources.ram,
            processes=True,
            silence_logs="ERROR",
        )
        client = Client(cluster)
        client.wait_for_workers(1)
        serialized_file = log_err.replace(".err", ".dill")
        with open(serialized_file, "wb") as file_to_dump:
            dill.dump((func, f_kwargs, self.logger_lvl), file_to_dump)
        cmd_dask: str = (
            f"ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS={resources.nb_cpu} "
            f"task_launcher.py -dill_file {serialized_file}"
        )
        sub_results = client.submit(
            run_as_process, cmd_dask, stdout=log_out, stderr=log_err
        )
        try:
            sub_results.result()
            task_status = "done"
        except KilledWorker:
            task_status = "failed"
            killed_worker = True

        # catch any other type of exception
        except Exception:  # pylint: disable=broad-except
            task_status = "failed"
            LOGGER.error(
                f"the task '{pbs_worker_name}' has failed. "
                f"Please check logs '{log_err}' and '{log_out}' "
                "for more details"
            )
        finally:
            client.close()
            client.shutdown()
        return killed_worker, task_status

    def _launch_function_in_cluster_mode(
        self,
        f_kwargs: dict,
        func: Callable,
        killed_worker: bool,
        log_err: str,
        log_out: str,
        pbs_worker_name: str,
        resources: TaskResources,
    ) -> tuple[bool, str]:
        """
        Launch the selected function using the cluster scheduler_type

        Parameters
        ----------
        f_kwargs: dict
            Dictionary containing the task function and it's arguments
        func: Callable
            Function to be launched
        killed_worker: bool
            Flag indicating if the worker was killed. False by defaults, sets to True if the worker
            is killed during the process.
        log_err: str
            File to store error flux
        log_out: str
            File to store std flux
        pbs_worker_name: str
            Task name
        resources: TaskResources
            Task resources

        Returns
        -------
        tuple[bool, str]:
            Tuple containing:
                - The state of the worker as a bool (killed or not)
                - The status of the task as a string
        """
        env_vars = [
            f"export PYTHONPATH={os.environ.get('PYTHONPATH')}",
            f"export PATH={os.environ.get('PATH')}",
            f"export LD_LIBRARY_PATH={os.environ.get('LD_LIBRARY_PATH')}",
            f"export OTB_APPLICATION_PATH={os.environ.get('OTB_APPLICATION_PATH')}",
            f"export GDAL_DATA={os.environ.get('GDAL_DATA')}",
            f"export GEOTIFF_CSV={os.environ.get('GEOTIFF_CSV')}",
            f"export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS={resources.nb_cpu}",
        ]
        extras = [f"-N {pbs_worker_name[0:30]}"]
        if log_err is not None:
            extras.append(f"-e {log_err}")
        if log_out is not None:
            extras.append(f"-o {log_out}")
        if resources.queue is not None:
            extras.append(f"-q {resources.queue}")
        if resources.nb_gpu is not None:
            extras.append(f"-l select=1:ngpus={resources.nb_gpu}")
        cluster = PBSCluster(
            n_workers=1,
            cores=resources.nb_cpu,
            memory=resources.ram,
            walltime=resources.walltime,
            env_extra=env_vars,
            interface="ib0",
            silence_logs="error",
            processes=1,
            nanny=False,
            job_extra=extras,
            local_directory="$TMPDIR",
        )
        client = Client(cluster)
        client.wait_for_workers(1)
        client.register_worker_callbacks(self.init_log)
        for _, worker_meta in client.scheduler_info()["workers"].items():
            working_dir = str(Path(worker_meta["local_directory"]).parent)
        f_kwargs = set_working_dir_parameter(f_kwargs, working_dir)

        def set_allowed_failures(dask_scheduler: Scheduler) -> None:
            dask_scheduler.allowed_failures = 0

        client.run_on_scheduler(set_allowed_failures)
        sub_results = client.submit(func, **f_kwargs)
        try:
            sub_results.result()
            task_status = "done"
        except KilledWorker:
            task_status = "failed"
            killed_worker = True

        # catch any other type of exception
        except Exception as task_exception:  # pylint: disable=broad-except
            task_status = "failed"
            message = f"IOTA2 found python exception :{type(task_exception).__name__} "
            log_traceback(client, pbs_worker_name, message)
        finally:
            time.sleep(20)
            client.close()
            client.shutdown()
        return killed_worker, task_status

    def task_launcher(
        self,
        *dependencies: list,
        task_name: str,
        log_err: str,
        log_out: str,
        scheduler_type: AvailSchedulers = "debug",
        pbs_worker_name: str = "i2-worker",
        resources: TaskResources = TaskResources(),
        **kwargs: dict,
    ) -> str:
        """this function encapsulate tasks to be delayed

        *dependencies: dask.delayed dependencies
           captures all arguments (after self) into a list of tasks dependencies
           each dependency is a delayed task_launcher or its string output value

        i2_log_dir :
            path to a directory which will contains tasks files
        scheduler_type :
            define which kind of scheduling will be operate
            choices are 'PBS', 'cluster', 'localCluster' and 'debug'
        resources :
            task resources
        Notes
        -----
        the return value represent the task's status and is in
          ('done', 'failed', 'unlaunchable')
        """
        if task_name and self.older_i2_state:
            if (
                task_name in self.older_i2_state
                and self.older_i2_state[task_name] == "done"
            ):
                return "done"
        if (
            task_name
            and dependencies
            and not all(dep == "done" for dep in dependencies)
        ):
            task_status = "unlaunchable"
            self.save_task_status(task_name, task_status)
            return task_status

        kwargs = kwargs.copy()
        task_kwargs = kwargs.get("task_kwargs", None)
        if task_kwargs:
            max_retry = self.allowed_task_failure
            cpu = resources.nb_cpu
            ram = resources.ram

            task_status = ""
            nb_try = 0
            ram_gb = ram_str_to_gb_float(ram)

            if max_retry == 0:
                nb_try = -1
            not_consistent_msg = ""
            resources_consistancy = True
            if cpu > self.cpu_hardware_limits:
                not_consistent_msg = (
                    f"task : {task_name}, allowed cpu = {cpu} "
                    f"is not consistent with cpu hardware limit : {self.cpu_hardware_limits} "
                )

            if ram_gb > self.ram_hardware_limits:
                not_consistent_msg += (
                    f"task : {task_name}, allowed ram = {ram_gb} "
                    f"is not consistent with ram hardware limit : {self.ram_hardware_limits}"
                )
            if not_consistent_msg:
                not_consistent_msg = (
                    f"{not_consistent_msg} please refer to "
                    "configuration resources file and "
                    "'task_retry_limits' configuration file parameters"
                )
                LOGGER.error(not_consistent_msg)
                task_status = "failed"
                resources_consistancy = False
            while (
                resources_consistancy
                and int(nb_try) < int(max_retry)
                and float(ram_gb) <= float(self.ram_hardware_limits)
                and int(cpu) <= int(self.cpu_hardware_limits)
            ):
                task_status, killed_worker = self.send_task(
                    scheduler_type,
                    task_kwargs,
                    log_err,
                    log_out,
                    pbs_worker_name,
                    resources,
                )
                if task_status == "done":
                    break
                if killed_worker:
                    cpu, ram, ram_gb, walltime = increase_resources(
                        resources.nb_cpu, resources.ram, resources.walltime
                    )
                    resources.nb_cpu = cpu
                    resources.ram = ram
                    resources.walltime = walltime

                nb_try += 1
        if task_kwargs and task_name:
            self.save_task_status(task_name, task_status)
            return task_status

        return "failed"

    def trace_graph(self, *args: list, **kwargs: dict) -> None:
        """purposely empty"""

    def add_task_to_i2_processing_graph(
        self,
        task: I2Task,
        task_group: str,
        task_sub_group: str | int | None = None,
        task_dep_dico: Mapping[str, list[str] | list[int]] | None = None,
    ) -> None:
        """function used to add a step's task to a processing graph

        task :
            task to launch
        task_group:
            group of tasks the task belong to
        task_sub_group:
            subgroup of tasks the task belong to
        task_dep_dico:
            task's dependencies
        """
        if task_dep_dico and not isinstance(task_dep_dico, dict):
            raise ValueError("task_dep_dico parameter must be a dictionary")
        is_addable = task.is_addable_to_graph(self.container_name)
        if not is_addable:
            raise ValueError(
                f"task called : {task.task_name} can't be add to the execution graph"
                ", already in execution graph"
            )
        new_task = None
        if self.container_name not in self.tasks_graph:
            self.tasks_graph[self.container_name] = {}
            self.tasks_graph_figure[self.container_name] = {}

        if task_group not in self.tasks_graph[self.container_name]:
            self.tasks_graph[self.container_name][task_group] = {}
            self.tasks_graph_figure[self.container_name][task_group] = {}

        if task_group == "first_task":
            if self.tasks_graph[self.container_name]["first_task"]:
                raise ValueError("first task already exists")
            new_task = dask.delayed(self.task_launcher)(
                task_name=task.task_name,
                log_err=task.log_err,
                log_out=task.log_out,
                scheduler_type=task.execution_mode,
                pbs_worker_name=task.task_name,
                resources=task.resources,
                task_kwargs=task.parameters,
            )
            new_task_figure = dask.delayed(
                ChangeName(task.task_name)(self.trace_graph)
            )()
            self.tasks_graph[self.container_name]["first_task"] = new_task
            self.tasks_graph_figure[self.container_name]["first_task"] = new_task_figure
        elif task_group != "first_task" and isinstance(task_dep_dico, dict):
            # second step case, then the dependency is the "first_task"
            dep_granularity_names = list(task_dep_dico.keys())
            if (
                len(dep_granularity_names) == 1
                and task_dep_dico[dep_granularity_names[0]] == []
            ):
                if dep_granularity_names[0] in self.tasks_graph[self.container_name]:
                    new_task = dask.delayed(self.task_launcher)(
                        self.tasks_graph[self.container_name][dep_granularity_names[0]],
                        task_name=task.task_name,
                        log_err=task.log_err,
                        log_out=task.log_out,
                        scheduler_type=task.execution_mode,
                        pbs_worker_name=task.task_name,
                        resources=task.resources,
                        task_kwargs=task.parameters,
                    )
                    new_task_figure = dask.delayed(
                        ChangeName(task.task_name)(self.trace_graph)
                    )(
                        self.tasks_graph_figure[self.container_name][
                            dep_granularity_names[0]
                        ]
                    )
                else:
                    new_task = dask.delayed(self.task_launcher)(
                        task_name=task.task_name,
                        log_err=task.log_err,
                        log_out=task.log_out,
                        scheduler_type=task.execution_mode,
                        pbs_worker_name=task.task_name,
                        resources=task.resources,
                        task_kwargs=task.parameters,
                    )
                    new_task_figure = dask.delayed(
                        ChangeName(task.task_name)(self.trace_graph)
                    )()

            elif len(step_container) != 1:
                dep_list, dep_list_figure = self.get_dependency_list(
                    task, task_dep_dico
                )
                new_task = dask.delayed(self.task_launcher)(
                    *dep_list,
                    task_name=task.task_name,
                    log_err=task.log_err,
                    log_out=task.log_out,
                    scheduler_type=task.execution_mode,
                    pbs_worker_name=task.task_name,
                    resources=task.resources,
                    task_kwargs=task.parameters,
                )
                new_task_figure = dask.delayed(
                    ChangeName(task.task_name)(self.trace_graph)
                )(*dep_list_figure)
            else:
                new_task = dask.delayed(self.task_launcher)(
                    task_name=task.task_name,
                    log_err=task.log_err,
                    log_out=task.log_out,
                    scheduler_type=task.execution_mode,
                    pbs_worker_name=task.task_name,
                    resources=task.resources,
                    task_kwargs=task.parameters,
                )
                new_task_figure = dask.delayed(
                    ChangeName(task.task_name)(self.trace_graph)
                )()
            self.tasks_graph[self.container_name][task_group][task_sub_group] = new_task
            self.tasks_graph_figure[self.container_name][task_group][
                task_sub_group
            ] = new_task_figure

        self.step_tasks.append(new_task)
        self.step_tasks_figure.append(new_task_figure)

    def get_dependency_list(
        self, task: I2Task, task_dep_dico: Mapping[str, list[str] | list[int]]
    ) -> tuple[
        list[dict[str, Delayed | dict[str, Delayed]]],
        list[dict[str, Delayed | dict[str, Delayed]]],
    ]:
        """
        Return the list of dependencies graphs and the list of corresponding figures
        for a given task.

        Parameters
        ----------
        task:
            The task for which the dependency graphs and figures are retrieved
        task_dep_dico:
            Dependencies dictionary of the task

        Returns
        -------
        Tuple:
            Tuple with the list of dep graphs and the list of corresponding figures
        """
        dep_list = []
        dep_list_figure = []
        for dep_granularity_name, dep_tasks_names in task_dep_dico.items():
            for dep_task in dep_tasks_names:
                if (
                    dep_task
                    not in self.tasks_graph[self.container_name][dep_granularity_name]
                ):
                    raise ValueError(
                        f"[{task.task_name}] dependency {dep_task} "
                        f"does not exists in dependency graph"
                    )
                dep_list.append(
                    self.tasks_graph[self.container_name][dep_granularity_name][
                        dep_task
                    ]
                )
                dep_list_figure.append(
                    self.tasks_graph_figure[self.container_name][dep_granularity_name][
                        dep_task
                    ]
                )
        return dep_list, dep_list_figure

    def build_step_name(self) -> str:
        """
        strategy to build step name
        the name define logging output files and resources access
        """
        return self.__class__.__name__

    def __str__(self) -> str:
        return f"{self.step_name}"

    @classmethod
    def step_description(cls) -> str:
        """short step description, will be printed at launch"""
        return "short step description"


class Singleton(type):
    """metaclass dedicated to build a class as a singleton

    from https://stackoverflow.com/questions/43619748/destroying-a-singleton-object-in-python
    """

    _instances: WeakValueDictionary = WeakValueDictionary()

    def __call__(cls, *args: list, **kwargs: dict) -> Any:
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class SingletonList(list, metaclass=Singleton):  # type: ignore # conflict isn't an issue here
    """singleton list"""


# step_container is a list where **Step instances**
# are to be appended after instanciation
# it is only used twice:
# 1. in `get_exec_graph`
#   when building execution graph to get the dependencies of last step
# 2. in `add_task_to_i2_processing_graph`
#   to check is the step is the first one or not
step_container = SingletonList()


class StepContainer:
    """
    this class is dedicated to contains Step
    """

    def __init__(self, name: str = "iota2_tasks"):
        # container attribute is a list where the builder will append
        # partials of Step constructors
        # i.e. `container.append(partial(Step.__init__))`
        self.container: list[Step | partial[Step]] = []
        self.name = f"{name}_{str(uuid.uuid1())}"
        self.prelaunch_functions: list[Callable[[], Any]] = []

    def append(self, step: S | partial[S], step_group: str = "") -> None:  # type: ignore
        """
        Append step to step container
        """
        if step not in self.container:
            self.container.append(step)
            # step can be a partial[Step] but mypy doesn't recognize it as a Step and struggles to
            # recognize its attributes and methods
            step.step_group = step_group  # type: ignore
            # Actually 'func' is a partial of Step
            step.func.set_container_name(step.func, self.name)  # type: ignore

        else:
            raise Exception(
                f"step '{step.step_name}' " f"already present in container"
            )  # type: ignore

    def __contains__(self, step_ask: Step) -> bool:
        """
        The __contains__ method is based on step's name
        """
        return any(step.step_name == step_ask.step_name for step in self.container)  # type: ignore

    def __setitem__(self, index: int, val: Step) -> None:
        self.container[index] = val

    # type: ignore
    def __getitem__(
        self, index: int | slice
    ) -> Step | partial[Step] | list[Step] | list[partial[Step]]:
        return self.container[index]  # type: ignore

    def __iter__(self) -> Iterator[Step | partial[Step]]:
        return iter(self.container)

    def __str__(self) -> str:
        return f"[{', '.join(step.step_name for step in self.container)}]"  # type: ignore

    def __len__(self) -> int:
        return len(self.container)


class ChangeName:
    """
    decorator to temporary change a function's name
    useful to plot dask graph
    """

    def __init__(self, new_name: str):
        self.new_name = new_name

    def __call__(self, func: Delayed) -> Delayed:
        if "__name__" in dir(func):
            func.__name__ = self.new_name
        else:
            func.__func__.__name__ = self.new_name

        @wraps(func)
        def wrapped_f(*args: list, **kwargs: dict) -> Delayed:
            return func(*args, **kwargs)

        return wrapped_f


if __name__ == "__main__":
    pass
