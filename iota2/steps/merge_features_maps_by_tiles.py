#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Merge features map by tile step"""
import logging
from pathlib import Path

from iota2.common import write_features_map as wfm
from iota2.common.file_utils import file_search_and
from iota2.common.raster_utils import ChunkConfig, get_raster_chunks_boundaries
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class MergeFeaturesMapsByTiles(iota2_step.Step):
    """Class for merging features map by tile"""

    resources_block_name = "mergeMapsByTiles"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        self.output_path = parameters.get_param("chain", "output_path")
        # read config file to init custom features and check validity
        self.external_features_flag = parameters.get_param(
            "external_features", "external_features_flag"
        )
        chunk_size_mode = parameters.get_param(
            "python_data_managing", "chunk_size_mode"
        )
        chunk_config = ChunkConfig(
            chunk_size_mode=chunk_size_mode,
            number_of_chunks=parameters.get_param(
                "python_data_managing", "number_of_chunks"
            ),
            chunk_size_x=parameters.get_param("python_data_managing", "chunk_size_x"),
            chunk_size_y=parameters.get_param("python_data_managing", "chunk_size_y"),
            padding_size_x=parameters.get_param(
                "python_data_managing", "padding_size_x"
            ),
            padding_size_y=parameters.get_param(
                "python_data_managing", "padding_size_y"
            ),
        )
        # to allow writing maps using bandmath
        number_of_chunks = 0
        if self.external_features_flag:
            number_of_chunks = parameters.get_param(
                "python_data_managing", "number_of_chunks"
            )
        for tile in self.tiles:
            if chunk_size_mode == "user_fixed":
                rois, _ = get_raster_chunks_boundaries(
                    file_search_and(
                        Path(self.output_path) / "features" / tile / "tmp",
                        True,
                        "reference.tif",
                    )[0],
                    chunk_config,
                )
                number_of_chunks = len(rois)

            task = self.I2Task(
                task_name=f"merge_feat_maps_by_tiles_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": wfm.merge_features_by_tiles,
                    "iota2_directory": self.output_path,
                    "tile": tile,
                    "res": parameters.get_param("chain", "spatial_resolution"),
                },
                task_resources=self.get_resources(),
            )
            dep = []
            for n_chunk in range(number_of_chunks):
                dep.append(f"writing maps {tile} {n_chunk}")
            self.add_task_to_i2_processing_graph(
                task,
                task_group="merge_maps_by_tiles",
                task_sub_group=f"merge_maps_by_tiles {tile}",
                task_dep_dico={"writing maps": dep},
            )
        # implement tests for check if custom features are well provided
        # so the chain failed during step init

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Merge features chunks per tile"
        return description
