#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch validation at boundary tasks."""
import logging
from pathlib import Path

import iota2.validation.boundary_validation_metrics as bvm
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class ExtractBoundariesPoints(iota2_step.Step):
    """Launch task for validation at boundary scale."""

    resources_block_name = "extract_boundary_points"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        """Initialize the step."""
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        parameters = rcf.ReadConfigFile(self.cfg)
        runs = parameters.get_param("arg_train", "runs")
        output_path = parameters.get_param("chain", "output_path")

        buffer_size = parameters.get_param("pretrained_model", "boundary_buffer")
        dict_tile = {}
        for model_name, model_meta in self.spatial_models_distribution_classify.items():
            for tile in model_meta["tiles"]:
                if tile not in dict_tile:
                    dict_tile[tile] = [model_name]
                else:
                    dict_tile[tile].append(model_name)
        # for buf_size in buffer_size:
        for seed in range(runs):
            for tile in self.tiles:
                task = self.I2Task(
                    task_name=f"extract_point_{tile}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": bvm.prepare_entry_for_boundary_validation,
                        "iota2_directory": output_path,
                        "tile": tile,
                        "data_field": self.i2_const.re_encoding_label_name,
                        "seed": seed,
                        "working_directory": working_directory,
                    },
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks_seed",
                    task_sub_group=f"{tile}_{seed}",
                    task_dep_dico={
                        "boundary": [f"boundary_{buf_size}" for buf_size in buffer_size]
                    },
                )

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "generate boundary samples"
        return description
