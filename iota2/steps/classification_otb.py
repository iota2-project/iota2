#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the OTB classification task"""
import logging
from pathlib import Path

import iota2.common.raster_utils as ru
from iota2.classification import image_classifier as imageClassifier
from iota2.classification.image_classifier import (
    ClassificationOptions,
    ClassificationPaths,
    get_class_by_models_from_i2_learn,
)
from iota2.common.custom_numpy_features import exogenous_data_tile
from iota2.common.utils import TaskConfig
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.steps import iota2_step
from iota2.typings.i2_types import CustomFeaturesParameters, I2ParameterT

LOGGER = logging.getLogger("distributed.worker")


class ClassificationUsingOTB(iota2_step.Step):
    """Class for launching the OTB classification step"""

    resources_block_name = "classifications"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        """Initialize the classification class

        Parameters
        ----------
        cfg:
            configuration file
        cfg_resources_file:
            configuration resource file
        working_directory:
            folder for temporary data
        """
        # heritage init

        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.external_features_flag = rcf.ReadConfigFile(self.cfg).get_param(
            "external_features", "external_features_flag"
        )

        self.chunk_config = ru.ChunkConfig(
            chunk_size_mode=rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "chunk_size_mode"
            ),
            number_of_chunks=rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "number_of_chunks"
            ),
            chunk_size_x=rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "chunk_size_x"
            ),
            chunk_size_y=rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "chunk_size_y"
            ),
            padding_size_x=rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "padding_size_x"
            ),
            padding_size_y=rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "padding_size_y"
            ),
        )

        self.working_directory = working_directory
        self.output_path = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "output_path"
        )
        self.data_field = self.i2_const.re_encoding_label_name
        runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")
        self.enable_proba_map = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_classification", "enable_probability_map"
        )

        number_of_chunks = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "number_of_chunks"
        )
        chunk_size_mode = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "chunk_size_mode"
        )
        region_field = (
            rcf.ReadConfigFile(self.cfg).get_param("chain", "region_field")
        ).lower()
        class_by_models = get_class_by_models_from_i2_learn(
            str(Path(self.output_path) / "dataAppVal"), self.data_field, region_field
        )
        self.all_seeds_class = []
        for _, region_class in class_by_models.items():
            self.all_seeds_class += region_class
        self.all_seeds_class = sorted(list(set(self.all_seeds_class)))
        if self.enable_proba_map and not class_by_models:
            raise ValueError(
                "Iota2 cannot generate probability map,"
                " database is missing in 'dataAppVal' directory"
            )

        for (
            model_name,
            model_meta,
        ) in self.spatial_models_distribution_classify.items():
            sensors_dates = rcf.Iota2Parameters(
                rcf.ReadConfigFile(self.cfg)
            ).get_available_sensors_dates(model_meta["tiles"])
            if "Sentinel1" in sensors_dates:
                s1_dates = rcf.Iota2Parameters(
                    rcf.ReadConfigFile(self.cfg)
                ).get_sentinel1_input_dates(model_meta["tiles"])
                sensors_dates = {**sensors_dates, **s1_dates}
                sensors_dates.pop("Sentinel1", None)
            for seed in range(runs):
                for tile in model_meta["tiles"]:
                    task_name = f"classification_{tile}_model_{model_name}_seed_{seed}"
                    if self.external_features_flag:
                        if chunk_size_mode == "user_fixed":
                            rois, _ = ru.get_raster_chunks_boundaries(
                                Path(self.output_path)
                                / "features"
                                / tile
                                / "tmp"
                                / "MaskCommunSL.tif",
                                self.chunk_config,
                            )
                            number_of_chunks = len(rois)
                        for chunk in range(number_of_chunks):
                            task = self.I2Task(
                                task_name=f"{task_name}_{chunk}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters=self.get_classification_params(
                                    model_name, tile, seed, sensors_dates, chunk
                                ),
                                task_resources=self.get_resources(),
                            )
                            self.add_task_to_i2_processing_graph(
                                task,
                                task_group="tile_tasks_model_mode",
                                task_sub_group=f"{tile}_{model_name}_{seed}_{chunk}",
                                task_dep_dico={
                                    "region_tasks": [f"model_{model_name}_seed_{seed}"]
                                },
                            )
                    else:
                        task = self.I2Task(
                            task_name=task_name,
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters=self.get_classification_params(
                                model_name, tile, seed, sensors_dates
                            ),
                            task_resources=self.get_resources(),
                        )
                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="tile_tasks_model_mode",
                            task_sub_group=f"{tile}_{model_name}_{seed}",
                            task_dep_dico={
                                "region_tasks": [f"model_{model_name}_seed_{seed}"]
                            },
                        )

    def get_classification_params(
        self,
        region_name: str,
        tile_name: str,
        seed: int,
        sensors_dates: dict[str, list[str]],
        target_chunk: int | None = None,
    ) -> I2ParameterT:
        """Generate the classification parameters

        Parameters
        ----------
        region_name:
            the region to be classified
        tile_name:
            the tile to be classified
        seed:
            the run targeted
        sensors_dates:
            Dictionary mapping sensors to dates, for example:
            dict([('Sentinel2', ['20200101', '20200112', '20200127'])])
        target_chunk:
            the chunk targeted
        """
        region_mask_name = region_name.split("f")[0]
        target_model_name = f"model_{region_name}_seed_{seed}.txt"
        classifier = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "classifier")
        classif_mask_file = str(
            Path(self.output_path)
            / "classif"
            / "MASK"
            / f"MASK_region_{region_mask_name}_{tile_name}.tif"
        )
        model_file = str(Path(self.output_path) / "model" / target_model_name)
        stats_file = None
        enable_gap, enable_raw = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "data_mode_access"
        )
        if classifier and "svm" in classifier:
            stats_file = str(
                Path(self.output_path)
                / "stats"
                / f"Model_{region_name}_seed_{seed}.xml"
            )
        boundary_fusion_enabled = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_classification", "enable_boundary_fusion"
        )
        boundary_mask = None
        if boundary_fusion_enabled:
            boundary_mask = str(
                Path(self.output_path)
                / "classif"
                / "MASK"
                / f"Boundary_MASK_region_{region_mask_name}_{tile_name}.tif"
            )

        custom_features_parameters = CustomFeaturesParameters(
            enable=self.external_features_flag,
            module=rcf.ReadConfigFile(self.cfg).get_param(
                "external_features", "module"
            ),
            functions=rcf.ReadConfigFile(self.cfg).get_param(
                "external_features", "functions"
            ),
            concat_mode=rcf.ReadConfigFile(self.cfg).get_param(
                "external_features", "concat_mode"
            ),
        )

        param = {
            "f": imageClassifier.launch_classification,
            "classif_paths": ClassificationPaths(
                classif_mask=classif_mask_file,
                classif_dir=f"{self.output_path}/classif",
                model=model_file,
                stats=stats_file,
                output_path=f"{self.output_path}",
                boundary_mask=boundary_mask,
            ),
            "path_wd": self.working_directory,
            "classif_options": ClassificationOptions(
                classifier_type=classifier,
                tile=tile_name,
                proba_map_expected=self.enable_proba_map,
                write_features=rcf.ReadConfigFile(self.cfg).get_param(
                    "sensors_data_interpolation", "write_outputs"
                ),
                all_class=self.all_seeds_class,
                pix_type="uint8",
            ),
            "data_field": self.data_field,
            "sensors_parameters": rcf.Iota2Parameters(
                rcf.ReadConfigFile(self.cfg)
            ).get_sensors_parameters(tile_name),
            "multi_param_cust": {
                "enable_raw": enable_raw,
                "enable_gap": enable_gap,
                "fill_missing_dates": rcf.ReadConfigFile(self.cfg).get_param(
                    "python_data_managing", "fill_missing_dates"
                ),
                "all_dates_dict": sensors_dates,
                "exogeneous_data": exogenous_data_tile(
                    rcf.ReadConfigFile(self.cfg).get_param(
                        "external_features", "exogeneous_data"
                    ),
                    tile_name,
                ),
            },
            "task_config": TaskConfig(ram=1024 * get_ram(self.resources.ram)),
            "custom_features_parameters": custom_features_parameters,
        }
        if self.external_features_flag:
            param["chunk_config"] = self.chunk_config
            param["targeted_chunk"] = target_chunk
            # mypy can't infer the right type
            param["classif_options"].force_standard_labels = rcf.ReadConfigFile(  # type: ignore
                self.cfg
            ).get_param(
                "arg_train", "force_standard_labels"
            )

        return param

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Generate classifications"
        return description
