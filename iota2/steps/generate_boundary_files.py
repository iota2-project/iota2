#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provide the step to compute boundary files."""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.validation import boundary_validation_metrics as bvm

LOGGER = logging.getLogger("distributed.worker")


class GenerateBoundaryRegionFiles(iota2_step.Step):
    """Launch the creation of boundary files."""

    resources_block_name = "boundary_buffer"

    def __init__(self, cfg: str, cfg_resources_file: Path | None):
        """Initialize the step."""
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")

        region_shape = parameters.get_param("chain", "region_path")
        region_field = parameters.get_param("chain", "region_field")
        buffer_size = parameters.get_param("pretrained_model", "boundary_buffer")

        runs = parameters.get_param("arg_train", "runs")
        dep = []
        for tile in self.tiles:
            for seed in range(runs):
                dep.append(f"{tile}_{seed}")
        for buf_size in buffer_size:
            task = self.I2Task(
                task_name=f"generate_boundary_files_{buf_size}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": bvm.compute_all_boundaries,
                    "eco_region_file": region_shape,
                    "region_field": region_field,
                    "buffer_size": buf_size,
                    "output_path": str(
                        Path(output_path)
                        / "boundary_vectors"
                        / "boundary_files"
                        / f"{buf_size}"
                    ),
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="boundary",
                task_sub_group=f"boundary_{buf_size}",
                task_dep_dico={"final_report": ["final_report"]},
            )

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "generate boundary files"
        return description
