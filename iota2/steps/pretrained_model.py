#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the user inference task."""
import logging
from pathlib import Path

from iota2.classification.external_classification import (
    ExternalInferenceParameters,
    do_external_prediction,
)
from iota2.classification.image_classifier import get_class_by_models_from_i2_learn
from iota2.classification.py_classifiers import PredictionParameters
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.typings.i2_types import CustomFeaturesParameters, PredictionOutput

LOGGER = logging.getLogger("distributed.worker")


class PreTrainedModel(iota2_step.Step):
    """Class for launching user inference."""

    resources_block_name = "classifications"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        """Initialize the class.

        Parameters
        ----------
        cfg:
            configuration file
        cfg_resources_file:
            configuration resource file
        working_directory:
            folder for temporary data
        """
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        parameters = rcf.ReadConfigFile(self.cfg)
        self.output_path = parameters.get_param("chain", "output_path")
        external_features_flag = parameters.get_param(
            "external_features", "external_features_flag"
        )

        if external_features_flag:
            concat_mode = parameters.get_param("external_features", "concat_mode")
        else:
            # ensure that is false if feature_from_raw_dates
            concat_mode = False

        self.class_by_models = get_class_by_models_from_i2_learn(
            str(Path(self.output_path) / "dataAppVal"),
            parameters.get_param("chain", "data_field"),
            parameters.get_param("chain", "region_field").lower(),
        )
        all_seeds_class = []
        for _, region_class in self.class_by_models.items():
            all_seeds_class += region_class
        all_seeds_class = sorted(list(set(all_seeds_class)))
        runs = parameters.get_param("arg_train", "runs")
        model = parameters.get_param("pretrained_model", "model")
        self.number_of_chunks = parameters.get_param(
            "python_data_managing", "number_of_chunks"
        )
        self.enable_gap, self.enable_raw = parameters.get_param(
            "python_data_managing", "data_mode_access"
        )
        mode = parameters.get_param("pretrained_model", "mode")
        if mode == "classif":
            self.prefix = "Classif"
        elif mode == "regression":
            self.prefix = "Regression"

        for (
            region_name,
            model_meta,
        ) in self.spatial_models_distribution_classify.items():
            self.sensors_dates = rcf.Iota2Parameters(
                parameters
            ).get_available_sensors_dates(model_meta["tiles"])
            if "Sentinel1" in self.sensors_dates:
                s1_dates = rcf.Iota2Parameters(parameters).get_sentinel1_input_dates(
                    model_meta["tiles"]
                )
                self.sensors_dates = {**self.sensors_dates, **s1_dates}
                self.sensors_dates.pop("Sentinel1", None)
            for seed in range(runs):
                for tile in model_meta["tiles"]:
                    for target_chunk in range(self.number_of_chunks):

                        params = {
                            "f": do_external_prediction,
                            "pred_parameters": PredictionParameters(
                                tile_name=tile,
                                enabled_gap=self.enable_gap,
                                enabled_raw=self.enable_raw,
                                sensors_parameters=rcf.Iota2Parameters(
                                    parameters
                                ).get_sensors_parameters(tile),
                                sensors_dates=self.sensors_dates,
                                exogeneous_data=parameters.get_param(
                                    "external_features", "exogeneous_data"
                                ),
                                features_from_raw_dates=parameters.get_param(
                                    "arg_train", "features_from_raw_dates"
                                ),
                            ),
                            "pred_output": PredictionOutput(
                                classif=str(
                                    Path(self.output_path)
                                    / "classif"
                                    / "full_pred"
                                    / f"{self.prefix}_{tile}_model_"
                                    f"{region_name}_seed_{seed}_"
                                    f"SUBREGION_{target_chunk}.tif",
                                ),
                                confidence=str(
                                    Path(self.output_path)
                                    / "classif"
                                    / "full_pred"
                                    / f"{tile}_model_{region_name}"
                                    "_confidence_seed_{seed}_"
                                    f"SUBREGION_{target_chunk}.tif",
                                ),
                                proba=str(
                                    Path(self.output_path)
                                    / "classif"
                                    / "full_pred"
                                    / f"PROBAMAP_{tile}_model"
                                    f"_{region_name}_seed_{seed}_"
                                    f"SUBREGION_{target_chunk}.tif"
                                ),
                                output_path=self.output_path,
                            ),
                            "custom_features_parameters": CustomFeaturesParameters(
                                enable=parameters.get_param(
                                    "external_features", "external_features_flag"
                                ),
                                module=parameters.get_param(
                                    "external_features", "module"
                                ),
                                functions=parameters.get_param(
                                    "external_features", "functions"
                                ),
                                concat_mode=concat_mode,
                            ),
                            "inference_params": ExternalInferenceParameters(
                                function_infer=parameters.get_param(
                                    "pretrained_model", "function"
                                ),
                                inf_module_path=parameters.get_param(
                                    "pretrained_model", "module"
                                ),
                                model=model.replace("$TILE", tile)
                                .replace("$REGION", region_name)
                                .replace("$SEED", f"{seed}"),
                            ),
                            "all_class_labels": all_seeds_class,
                            "number_of_chunks": parameters.get_param(
                                "python_data_managing", "number_of_chunks"
                            ),
                            "targeted_chunk": target_chunk,
                            "working_dir": working_directory,
                            "inference_mode": mode,
                        }
                        task = self.I2Task(
                            task_name=(
                                f"ext_classification_{tile}_model_{region_name}"
                                f"_seed_{seed}_{target_chunk}"
                            ),
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters=params,
                            task_resources=self.get_resources(),
                        )
                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="tile_tasks_model_mode",
                            task_sub_group=(
                                f"ext_classif_{tile}_{region_name}_{seed}"
                                f"_{target_chunk}"
                            ),
                            task_dep_dico={"first_task": []},
                        )

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "Generate user classifications"
        return description
