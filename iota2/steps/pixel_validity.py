# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Pixel validity step"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.sensors import process_launcher
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class PixelValidity(iota2_step.Step):
    """Class to compute the pixel validity"""

    resources_block_name = "get_pixValidity"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.ram = 1024.0 * get_ram(self.resources.ram)
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        cloud_threshold = parameters.get_param("chain", "cloud_threshold")

        for tile in self.tiles:
            task = self.I2Task(
                task_name=f"validity_raster_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": process_launcher.validity,
                    "tile_name": tile,
                    "config_path": self.cfg,
                    "output_path": output_path,
                    "maskout_name": f"CloudThreshold_{cloud_threshold}.shp",
                    "view_threshold": cloud_threshold,
                    "working_directory": working_directory,
                    "ram": self.ram,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_tasks",
                task_sub_group=tile,
                task_dep_dico={"tile_tasks": [tile]},
            )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Compute validity raster by tile"
        return description
