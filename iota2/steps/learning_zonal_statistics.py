#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the zonal statistics task"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.segmentation import prepare_segmentation_obia as pso
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class LearningZonalStatistics(iota2_step.Step):
    """Class for launching the zonal statistics step"""

    resources_block_name = "learning_zonal_statistics"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        nb_runs = parameters.get_param("arg_train", "runs")

        stats_used = parameters.get_param("obia", "stats_used")
        buffer_size = parameters.get_param("obia", "buffer_size")
        region_field = parameters.get_param("chain", "region_field")
        spatial_res = parameters.get_param("chain", "spatial_resolution")[0]
        for seed in range(nb_runs):
            for tile in self.tiles:
                target_model = f"zonal_stats_learn_{tile}_seed_{seed}"
                task = self.I2Task(
                    task_name=f"{target_model}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": pso.compute_zonal_stats_by_tiles,
                        "tile": tile,
                        "working_dir": working_directory,
                        "iota2_directory": output_path,
                        "seed": seed,
                        "sensors_parameters": rcf.Iota2Parameters(
                            parameters
                        ).get_sensors_parameters(tile),
                        "spatial_res": spatial_res,
                        "region_field": region_field,
                        "stat_used": stats_used,
                        "buffer_size": buffer_size,
                        "seg_field": self.i2_const.i2_segmentation_field_name,
                    },
                    task_resources=self.get_resources(),
                )

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=target_model,
                    task_dep_dico={"tile_tasks_model": [f"{tile}_{seed}"]},
                )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Compute zonal statistics for learning polygons"
        return description
