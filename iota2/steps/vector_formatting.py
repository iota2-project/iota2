#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the vector formatting task"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import vector_formatting as VF
from iota2.sampling.vector_formatting import (
    DistanceMapParameters,
    PolygonsSelection,
    VectorFormattingFlags,
    VectorFormattingPaths,
)
from iota2.steps import iota2_step
from iota2.typings.i2_types import DBInfo

LOGGER = logging.getLogger("distributed.worker")


class VectorFormatting(iota2_step.Step):
    """Class for launching the vector formatting step"""

    resources_block_name = "samplesFormatting"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
        data_field: str | None = None,
        original_label_column: str | None = None,
    ):
        """
        data_field
            the samples are split according to this column
            in classif mode, this is the classes column
            in regression mode, this is an fake column
        original_label_column
            this column is kept in the output shapefile
            used in regression mode to keep the target column
        """
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = working_directory

        # parameters
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        ground_truth_vec = str(Path(output_path) / self.i2_const.re_encoding_label_file)

        # in classif mode, the label used for splitting is "i2label"
        data_field = data_field or self.i2_const.re_encoding_label_name

        classif_mode = parameters.get_param("arg_classification", "classif_mode")

        enable_split_ground_truth = parameters.get_param(
            "arg_train", "split_ground_truth"
        )
        fusion_merge_all_validation = parameters.get_param(
            "arg_classification", "fusionofclassification_all_samples_validation"
        )
        epsg = int((parameters.get_param("chain", "proj")).split(":")[-1])
        region_vec = parameters.get_param("chain", "region_path")
        region_field = (parameters.get_param("chain", "region_field")).lower()
        if not region_vec:
            region_vec = str(
                Path(parameters.get_param("chain", "output_path")) / "MyRegion.shp"
            )
        original_label_column = (
            original_label_column
            or (parameters.get_param("chain", "data_field")).lower()
        )
        merge_final_classifications = parameters.get_param(
            "arg_classification", "merge_final_classifications"
        ) | (
            (
                parameters.get_param("builders", "builders_class_name")
                == ["I2Regression"]
            )
            and parameters.get_param("multi_run_fusion", "merge_run")
        )

        distance_map_params = DistanceMapParameters(
            exterior_buffer_size=parameters.get_param(
                "arg_classification", "boundary_exterior_buffer_size"
            ),
            interior_buffer_size=parameters.get_param(
                "arg_classification", "boundary_interior_buffer_size"
            ),
            epsilon=parameters.get_param(
                "arg_classification", "boundary_fusion_epsilon"
            ),
            spatial_res=parameters.get_param("chain", "spatial_resolution"),
        )

        polygons_selection = PolygonsSelection(
            cloud_threshold=parameters.get_param("chain", "cloud_threshold"),
            ratio=parameters.get_param("arg_train", "ratio"),
            random_seed=parameters.get_param("arg_train", "random_seed"),
            runs=parameters.get_param("arg_train", "runs"),
            merge_final_classifications_ratio=parameters.get_param(
                "arg_classification", "merge_final_classifications_ratio"
            ),
        )

        self.boundary_fusion_enabled = parameters.get_param(
            "arg_classification", "enable_boundary_fusion"
        )

        for tile in self.tiles:
            task = self.I2Task(
                task_name=f"vector_form_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": VF.vector_formatting,
                    "paths": VectorFormattingPaths(output_path, tile, region_vec),
                    "database": DBInfo(
                        data_field, ground_truth_vec, region_field=region_field
                    ),
                    "polygons_selection": polygons_selection,
                    "distance_map_params": distance_map_params,
                    "flags": VectorFormattingFlags(
                        enable_split_ground_truth,
                        fusion_merge_all_validation,
                        self.boundary_fusion_enabled,
                        merge_final_classifications,
                    ),
                    "epsg": epsg,
                    "classif_mode": classif_mode,
                    "original_label_column": original_label_column,
                    "working_directory": self.working_directory,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_tasks",
                task_sub_group=tile,
                task_dep_dico={"vector": ["vector"]},
            )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Prepare samples"
        return description
