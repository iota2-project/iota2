#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the check inputs task"""
import logging
from pathlib import Path
from typing import Literal

from iota2.common.verify_inputs import check_iota2_inputs
from iota2.configuration_files import read_config_file as rcf
from iota2.sensors.sensorscontainer import SensorsContainer
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class CheckInputsClassifWorkflow(iota2_step.Step):
    """Class for launching the check inputs step"""

    resources_block_name = "check_inputs"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        mode: Literal["classif", "regression"],
    ):
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        output_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path")
        reference_db = str(Path(output_path) / self.i2_const.re_encoding_label_file)
        region_shape = rcf.ReadConfigFile(self.cfg).get_param("chain", "region_path")
        region_field = rcf.ReadConfigFile(self.cfg).get_param("chain", "region_field")
        data_field = rcf.ReadConfigFile(self.cfg).get_param("chain", "data_field")
        tiles = rcf.ReadConfigFile(self.cfg).get_param("chain", "list_tile").split(" ")
        epsg = int(
            rcf.ReadConfigFile(self.cfg).get_param("chain", "proj").split(":")[-1]
        )
        running_parameters = rcf.Iota2Parameters(rcf.ReadConfigFile(self.cfg))
        sensors_parameters = running_parameters.get_sensors_parameters(tiles[0])
        sensor_tile_container = SensorsContainer(
            tiles[0], None, output_path, **sensors_parameters
        )
        sensor_path = sensor_tile_container.get_enabled_sensors_path()[0]
        check_inputs_task = self.I2Task(
            task_name="check_inputs",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": check_iota2_inputs,
                "i2_output_path": output_path,
                "ground_truth": reference_db,
                "region_shape": region_shape,
                "data_field": (
                    data_field
                    if mode == "regression"
                    else self.i2_const.re_encoding_label_name
                ),
                "region_field": region_field,
                "epsg": epsg,
                "sensor_path": sensor_path,
                "tiles": tiles,
            },
            task_resources=self.get_resources(),
        )

        self.add_task_to_i2_processing_graph(check_inputs_task, "first_task")

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "check inputs"
        return description
