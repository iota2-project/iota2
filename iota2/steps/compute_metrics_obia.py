#!/usr/bin/python

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the OBIA metrics computation task"""
import logging
from pathlib import Path

import iota2.segmentation.obia_metrics
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class ComputeMetricsObia(iota2_step.Step):
    """Class for launching the OBIA metrics computation step"""

    resources_block_name = "compute_metrics_obia"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        data_field = parameters.get_param("chain", "data_field")
        runs = parameters.get_param("arg_train", "runs")
        reference_data = parameters.get_param("chain", "ground_truth")
        reference_data_field = parameters.get_param("chain", "data_field")
        old_label_to_new = get_re_encoding_labels_dic(
            reference_data, reference_data_field
        )
        for seed in range(runs):
            for tile in self.tiles:
                task = self.I2Task(
                    task_name=f"compute_metrics_{tile}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": iota2.segmentation.obia_metrics.compute_metrics_obia,
                        "iota2_directory": output_path,
                        "tile": tile,
                        "seed": seed,
                        "labels_vector_table": old_label_to_new,
                        "data_field": data_field,
                        "ref_label_name": self.i2_const.re_encoding_label_name,
                        "seg_field": self.i2_const.i2_segmentation_field_name,
                    },
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks",
                    task_sub_group=f"{tile}_seed_{seed}",
                    task_dep_dico={"tile_tasks": [f"{tile}_seed_{seed}"]},
                )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Reassemble tiles and compute confusion matrix"
        return description
