#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step for merging all confusion matrices found in a folder."""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.validation import confusion_fusion
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class ConfusionsMerge(iota2_step.Step):
    """Merge confusion matrix of each tile to one."""

    resources_block_name = "confusionMatrixFusion"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
    ):
        """Initialize the step."""
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        data_field = self.i2_const.re_encoding_label_name
        runs = parameters.get_param("arg_train", "runs")

        ground_truth = parameters.get_param("chain", "ground_truth")
        user_data_field = parameters.get_param("chain", "data_field")
        user_labels_to_i2_labels = get_re_encoding_labels_dic(
            ground_truth, user_data_field
        )

        all_castable = []
        assert user_labels_to_i2_labels
        for user_label, _ in user_labels_to_i2_labels.items():
            try:
                _ = int(user_label)
                all_castable.append(True)
            except ValueError:
                all_castable.append(False)
        re_encode_labels = all(all_castable)
        if re_encode_labels:
            ground_truth = parameters.get_param("chain", "ground_truth")
            data_field = parameters.get_param("chain", "data_field")
        comparison_mode = parameters.get_param(
            "arg_classification", "boundary_comparison_mode"
        )
        if comparison_mode:
            path_in_out = [
                str(Path(output_path) / "final" / "standard" / "TMP"),
                str(Path(output_path) / "final" / "boundary" / "TMP"),
            ]
        else:
            path_in_out = [str(Path(output_path) / "final" / "TMP")]

        task = self.I2Task(
            task_name="merge_confusions",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": confusion_fusion.confusion_fusion_folder,
                "input_vector": ground_truth,
                "data_field": data_field,
                "out_path_list": path_in_out,
                "runs": runs,
            },
            task_resources=self.get_resources(),
        )
        self.add_task_to_i2_processing_graph(
            task,
            task_group="confusion_merge",
            task_sub_group="confusion_merge",
            task_dep_dico={
                "tile_tasks_seed": [
                    f"{tile}_{seed}" for seed in range(runs) for tile in self.tiles
                ]
            },
        )

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "Merge all confusions"
        return description
