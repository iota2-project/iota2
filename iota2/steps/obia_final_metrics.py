#!/usr/bin/python

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Merge all metrics for OBIA"""
import logging
from pathlib import Path

import iota2.segmentation.obia_metrics
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class MergeFinalMetrics(iota2_step.Step):
    """Class to merge all metrics for OBIA"""

    resources_block_name = "merge_final_metrics"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        runs = parameters.get_param("arg_train", "runs")

        reference_data = parameters.get_param("chain", "ground_truth")
        reference_data_field = parameters.get_param("chain", "data_field")
        old_label_to_new = get_re_encoding_labels_dic(
            reference_data, reference_data_field
        )
        for seed in range(runs):

            task = self.I2Task(
                task_name=f"merge_final_metrics{seed}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": iota2.segmentation.obia_metrics.cumulate_confusion_matrix,
                    "out_directory": str(Path(output_path) / "final"),
                    "seed": seed,
                    "nomenclature_file": parameters.get_param(
                        "chain", "nomenclature_path"
                    ),
                    "data_field": self.i2_const.re_encoding_label_name,
                    "label_vector_table": old_label_to_new,
                    "tmp_dir": str(Path(output_path) / "final" / "TMP"),
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="seed_tasks",
                task_sub_group=f"seed_{seed}",
                task_dep_dico={"seed_tasks": [f"seed_{seed}"]},
            )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Merge all metrics files"
        return description
