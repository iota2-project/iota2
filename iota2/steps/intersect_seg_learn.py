#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch intersection between learning samples and segmentation task"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.segmentation.vector_operations import segmentation_and_samples_intersection
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class IntersectSegLearn(iota2_step.Step):
    """Class for launching learning samples and segmentation intersection step"""

    resources_block_name = "intersect_seg_learn"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        nb_runs = parameters.get_param("arg_train", "runs")
        spatial_resolution = parameters.get_param("chain", "spatial_resolution")
        region_field = parameters.get_param("chain", "region_field")
        full_segment = parameters.get_param("obia", "full_learn_segment")
        for seed in range(nb_runs):
            dep: dict[str, list[str]] = {}
            for tile in self.tiles:
                dep[tile] = []
            for model_name, model_meta in self.spatial_models_distribution.items():
                for tile in model_meta["tiles"]:
                    dep[tile].append(f"model_{model_name}_seed_{seed}")
            for tile in self.tiles:

                task = self.I2Task(
                    task_name=f"intersect_{tile}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": segmentation_and_samples_intersection,
                        "tile": tile,
                        "iota2_directory": output_path,
                        "seed": seed,
                        "working_directory": working_directory,
                        "spatial_resolution": spatial_resolution,
                        "region_field": region_field,
                        "seg_field": self.i2_const.i2_segmentation_field_name,
                        "full_segment": full_segment,
                    },
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks_model",
                    task_sub_group=f"{tile}_{seed}",
                    # create a region dep. Once one region was processed launch training
                    task_dep_dico={"region_tasks": dep[tile]},
                )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Intersect samples and segmentation"
        return description
