#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" The OBIA segmentation preparation step"""
import logging
from pathlib import Path

import iota2.segmentation.vector_operations
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class ComputeIntersectionSegRegions(iota2_step.Step):
    """Class to launch the step for OBIA segmentation preparation"""

    resources_block_name = "compute_inter_seg_reg"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        parameters = rcf.ReadConfigFile(self.cfg)

        # step variables
        output_path = parameters.get_param("chain", "output_path")
        region_field = parameters.get_param("chain", "region_field")
        region_priority = parameters.get_param("obia", "region_priority")
        for tile in self.tiles:

            task_parameters = {
                "f": iota2.segmentation.vector_operations.segmentation_regions_intersection,
                "tile": tile,
                "iota2_directory": output_path,
                "seg_field": self.i2_const.i2_segmentation_field_name,
                "region_priority": region_priority,
            }
            if region_field is not None:
                task_parameters["region_field"] = region_field

            task = self.I2Task(
                task_name=f"compute_inter_sef_regs_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters=task_parameters,
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_tasks",
                task_sub_group=tile,
                task_dep_dico={"tile_tasks": [tile]},
            )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Compute intersection between segment and region"
        return description
