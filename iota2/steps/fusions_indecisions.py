#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the indecision fusion task"""
import logging
from pathlib import Path

from iota2.classification import indecision_management as UM
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.typings.i2_types import DBInfo

LOGGER = logging.getLogger("distributed.worker")


class FusionsIndecisions(iota2_step.Step):
    """Class for launching the indecision fusion task"""

    resources_block_name = "noData"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init

        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        output_path = rcf.ReadConfigFile(self.cfg).get_param("chain", "output_path")
        field_region = rcf.ReadConfigFile(self.cfg).get_param("chain", "region_field")
        runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")
        no_label = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_classification", "no_label_management"
        )
        features = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "features")
        region_vec = rcf.ReadConfigFile(self.cfg).get_param("chain", "region_path")
        patterns = rcf.ReadConfigFile(self.cfg).get_param("userFeat", "patterns")
        path_to_img = str(Path(output_path) / "features")

        for (
            model_name,
            model_meta,
        ) in self.spatial_models_distribution_no_sub_splits_classify.items():
            for seed in range(runs):
                for tile in model_meta["tiles"]:
                    fusion_img = str(
                        Path(output_path)
                        / "classif"
                        / f"{tile}_FUSION_model_{model_name}_seed_{seed}.tif",
                    )
                    task = self.I2Task(
                        task_name=f"manage_indecision_{tile}_model_{model_name}_seed_{seed}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f": UM.indecision_management,
                            "path_test": output_path,
                            "path_fusion": fusion_img,
                            "region_db": DBInfo(
                                "", region_vec, region_field=field_region
                            ),
                            "path_to_img": path_to_img,
                            "no_label_management": no_label,
                            "path_wd": working_directory,
                            "list_indices": list(features),
                            "pix_type": "uint8",
                            "user_feat_pattern": patterns,
                        },
                        task_resources=self.get_resources(),
                    )
                    if model_meta["nb_sub_model"] > 1:
                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="tile_tasks_model",
                            task_sub_group=f"{tile}_{model_name}_{seed}",
                            task_dep_dico={
                                "tile_tasks_model": [f"{tile}_{model_name}_{seed}"]
                            },
                        )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Manage indecisions in classification's fusion"
        return description
