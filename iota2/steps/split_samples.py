#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Split samples step"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import split_samples as splitS
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class SplitSamples(iota2_step.Step):
    """Class for samples split step"""

    resources_block_name = "split_samples"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        task = self.I2Task(
            task_name="models_subdivision",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": splitS.split_samples,
                "output_path": rcf.ReadConfigFile(self.cfg).get_param(
                    "chain", "output_path"
                ),
                "data_field": self.i2_const.re_encoding_label_name,
                "region_threshold": rcf.ReadConfigFile(self.cfg).get_param(
                    "arg_train", "mode_outside_regionsplit"
                ),
                "region_field": rcf.ReadConfigFile(self.cfg).get_param(
                    "chain", "region_field"
                ),
                "ratio": rcf.ReadConfigFile(self.cfg).get_param("arg_train", "ratio"),
                "random_seed": rcf.ReadConfigFile(self.cfg).get_param(
                    "arg_train", "random_seed"
                ),
                "runs": rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs"),
                "epsg": rcf.ReadConfigFile(self.cfg).get_param("chain", "proj"),
                "working_directory": working_directory,
            },
            task_resources=self.get_resources(),
        )
        self.add_task_to_i2_processing_graph(
            task,
            task_group="vector",
            task_sub_group="vector",
            task_dep_dico={"tile_tasks": self.tiles},
        )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = (
            "split learning polygons and Validation polygons in sub-sample if necessary"
        )
        return description
