#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the sample selection task"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import samples_selection
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class SamplingLearningPolygons(iota2_step.Step):
    """Class for launching the sample selection step"""

    resources_block_name = "samplesSelection"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
        data_field: str | None = None,
    ):
        """
        data_field
            samples are split according to the data_field
        """
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        nb_runs = parameters.get_param("arg_train", "runs")
        otb_parameters = dict(parameters.get_param("arg_train", "sample_selection"))
        data_field = data_field or self.i2_const.re_encoding_label_name
        if random_seed := parameters.get_param("arg_train", "random_seed"):
            otb_parameters["rand"] = random_seed
        sampling_validation = parameters.get_param("arg_train", "sampling_validation")
        parameters_validation = dict(
            parameters.get_param("arg_train", "sample_validation")
        )
        for model_name, model_meta in self.spatial_models_distribution.items():
            for seed in range(nb_runs):
                target_model = f"model_{model_name}_seed_{seed}"
                task = self.I2Task(
                    task_name=f"s_sel_{target_model}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": samples_selection.samples_selection,
                        "model": str(
                            Path(output_path)
                            / "samplesSelection"
                            / f"samples_region_{model_name}_seed_{seed}.shp"
                        ),
                        "working_directory": working_directory,
                        "output_path": output_path,
                        "runs": nb_runs,
                        "masks_name": "MaskCommunSL.tif",
                        "otb_parameters": otb_parameters,
                        "data_field": data_field,
                        "sampling_validation": sampling_validation,
                        "parameters_validation": parameters_validation,
                    },
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=target_model,
                    task_dep_dico={
                        "tile_tasks_model": [
                            f"{tile}_{model_name}_{seed}"
                            for tile in model_meta["tiles"]
                        ]
                    },
                )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Select pixels in learning polygons by models"
        return description
