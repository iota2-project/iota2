#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Merge boundary classifications."""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.validation.boundary_tile_fusion import merge_map_and_boundary

LOGGER = logging.getLogger("distributed.worker")


class MergeClassificationBoundary(iota2_step.Step):
    """Step to compute the fusion at boundary using probabilities."""

    resources_block_name = "compute_boundary_fusion"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        """Initialize the step."""
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # parameters
        parameters = rcf.ReadConfigFile(self.cfg)
        runs = parameters.get_param("arg_train", "runs")
        output_path = parameters.get_param("chain", "output_path")
        number_of_chunks = parameters.get_param(
            "python_data_managing", "number_of_chunks"
        )

        for seed in range(runs):
            for tile in self.tiles:
                task = self.I2Task(
                    task_name=f"merge_proba_{tile}_{seed}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": merge_map_and_boundary,
                        "tile": tile,
                        "path_to_chunks": str(Path(output_path) / "boundary"),
                        "path_to_env": str(Path(output_path) / "envelope"),
                        "seed": seed,
                        "working_directory": working_directory,
                    },
                    task_resources=self.get_resources(),
                )

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks_model",
                    task_sub_group=f"{tile}_{seed}",
                    task_dep_dico={
                        "tile_tasks_model": [
                            f"{tile}_{seed}_{targeted_chunk}"
                            for targeted_chunk in range(number_of_chunks)
                        ]
                    },
                )

    @classmethod
    def step_description(cls) -> str:
        """Print a short description of the step's purpose."""
        description = "Merge all classification with boundary area"
        return description
