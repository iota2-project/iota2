#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step for SLIC segmentation"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.segmentation import segmentation
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class SlicSegmentation(iota2_step.Step):
    """Class for SLIC segmentation step"""

    resources_block_name = "slic_segmentation"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        ram = 1024.0 * get_ram(self.resources.ram)

        running_parameters = rcf.Iota2Parameters(rcf.ReadConfigFile(self.cfg))

        for tile in self.tiles:
            task = self.I2Task(
                task_name=f"slic_segmentation_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": segmentation.slic_segmentation,
                    "tile_name": tile,
                    "output_path": rcf.ReadConfigFile(self.cfg).get_param(
                        "chain", "output_path"
                    ),
                    "sensors_parameters": running_parameters.get_sensors_parameters(
                        tile
                    ),
                    "ram": ram,
                    "working_dir": working_directory,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_tasks",
                task_sub_group=tile,
                task_dep_dico={"tile_tasks": [tile]},
            )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Compute SLIC segmentation by tile"
        # ~ About SLIC segmentation implementation :
        # ~     https://ieeexplore.ieee.org/document/8606448
        return description
