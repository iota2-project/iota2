#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Pytorch best model choice step"""
import logging
from itertools import product
from pathlib import Path
from typing import Literal

from iota2.classification.py_classifiers import choose_best_model
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class ModelChoice(iota2_step.Step):
    """Class for choosing the best model - only available with a pytorch workflow"""

    resources_block_name = "model_choice"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        mode: Literal["classif", "regression"] = "classif",
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        runs = parameters.get_param("arg_train", "runs")

        neural_network = parameters.get_param("arg_train", "deep_learning_parameters")

        hyperparameters_couples = list(
            product(
                neural_network["hyperparameters_solver"]["batch_size"],
                neural_network["hyperparameters_solver"]["learning_rate"],
            )
        )
        selection_criterion = neural_network["model_selection_criterion"]

        # dep
        for model_name, _ in self.spatial_models_distribution.items():
            for seed in range(runs):
                target_model = f"model_{model_name}_seed_{seed}"
                output_model = str(
                    Path(output_path) / "model" / f"model_{model_name}_seed_{seed}.txt"
                )
                dep_tasks = []
                hyperparameters_models = []
                for hyper_num in range(len(hyperparameters_couples)):
                    dep_tasks.append(f"{target_model}_hyp_{hyper_num}")
                    hyperparameters_models.append(
                        f"{output_model.replace('.txt','')}_hyp_{hyper_num}.txt"
                    )
                task = self.I2Task(
                    task_name=f"choose_{target_model}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": choose_best_model,
                        "input_pytorch_models": hyperparameters_models,
                        "selection_criterion": selection_criterion,
                        "output_model_file": output_model,
                        "model_parameters_file": output_model.replace(
                            ".txt", "_parameters.pickle"
                        ),
                        "encoder_convert_file": output_model.replace(
                            ".txt", "_encoder.pickle"
                        ),
                        "hyperparameters_file": output_model.replace(
                            ".txt", "_hyper.pickle"
                        ),
                        "mode": mode,
                    },
                    task_resources=self.get_resources(),
                )
                dep_key = "region_tasks"
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=f"{target_model}",
                    task_dep_dico={dep_key: dep_tasks},
                )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = (
            "from pytorch models generated for each"
            " hyperparameters couples, choose the best one"
        )
        return description
