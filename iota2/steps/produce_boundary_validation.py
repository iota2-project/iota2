#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the step to produce files for boundary validation"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.validation import boundary_tile_fusion as btf

LOGGER = logging.getLogger("distributed.worker")


class ProduceBoundaryValidation(iota2_step.Step):
    """
    Step to produce files for boundary validation
    """

    resources_block_name = "boundary_validation"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = working_directory
        parameters = rcf.ReadConfigFile(self.cfg)
        self.output_path = parameters.get_param("chain", "output_path")
        self.user_data_field = parameters.get_param("chain", "data_field")
        self.runs = parameters.get_param("arg_train", "runs")

        for seed in range(self.runs):
            for tile in self.tiles:
                task = self.I2Task(
                    task_name=f"boundary_validation_{seed}_{tile}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": btf.prepare_boundary_validation_dataset,
                        "iota2_directory": self.output_path,
                        "tile": tile,
                        "data_field": self.user_data_field,
                        "seed": seed,
                        "working_directory": self.working_directory,
                        "classifications": (
                            str(
                                Path(self.output_path)
                                / "final"
                                / "standard"
                                / f"Classif_Seed_{seed}.tif"
                            ),
                            str(
                                Path(self.output_path)
                                / "final"
                                / "boundary"
                                / f"Classif_Seed_{seed}.tif"
                            ),
                        ),
                    },
                    task_resources=self.get_resources(),
                )
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="final_report",
                    task_sub_group=f"final_report_{tile}_{seed}",
                    task_dep_dico={"final_report": ["final_report"]},
                )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Compute boundary validation files"
        return description
