#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the classification task with pytorch"""
import logging
from pathlib import Path

import iota2.classification.pytorch_prediction
import iota2.common.raster_utils as ru
from iota2.classification.image_classifier import get_class_by_models_from_i2_learn
from iota2.classification.py_classifiers import PredictionParameters
from iota2.classification.pytorch_prediction import (
    PytorchPredictionParameters,
    TorchPredictionPaths,
)
from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.typings.i2_types import (
    CustomFeaturesParameters,
    I2ParameterT,
    PredictionOutput,
)

LOGGER = logging.getLogger("distributed.worker")


class ClassificationUsingPytorch(iota2_step.Step):
    """Class for launching the pytorch classification step"""

    resources_block_name = "classifications"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        """Initialize the classification class

        Parameters
        ----------
        cfg:
            configuration file
        cfg_resources_file:
            configuration resource file
        working_directory:
            folder for temporary data
        """
        # heritage init

        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.external_features_flag = rcf.ReadConfigFile(self.cfg).get_param(
            "external_features", "external_features_flag"
        )

        self.number_of_chunks = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "number_of_chunks"
        )

        chunk_size_mode = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "chunk_size_mode"
        )
        self.output_path = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "output_path"
        )
        data_field = self.i2_const.re_encoding_label_name

        runs = rcf.ReadConfigFile(self.cfg).get_param("arg_train", "runs")
        self.classifier = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "classifier"
        )
        self.enable_proba_map = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_classification", "enable_probability_map"
        )

        # about custom features
        if self.external_features_flag:
            self.number_of_chunks = rcf.ReadConfigFile(self.cfg).get_param(
                "python_data_managing", "number_of_chunks"
            )

        region_field = (
            rcf.ReadConfigFile(self.cfg).get_param("chain", "region_field")
        ).lower()
        self.class_by_models = get_class_by_models_from_i2_learn(
            str(Path(self.output_path) / "dataAppVal"), data_field, region_field
        )
        if self.enable_proba_map and not self.class_by_models:
            raise ValueError(
                "Iota2 cannot generate probability map,"
                " database is missing in 'dataAppVal' directory"
            )

        for (
            model_name,
            model_meta,
        ) in self.spatial_models_distribution_classify.items():
            self.sensors_dates = rcf.Iota2Parameters(
                rcf.ReadConfigFile(self.cfg)
            ).get_available_sensors_dates(model_meta["tiles"])
            if "Sentinel1" in self.sensors_dates:
                s1_dates = rcf.Iota2Parameters(
                    rcf.ReadConfigFile(self.cfg)
                ).get_sentinel1_input_dates(model_meta["tiles"])
                self.sensors_dates = {**self.sensors_dates, **s1_dates}
                self.sensors_dates.pop("Sentinel1", None)
            for seed in range(runs):
                for tile in model_meta["tiles"]:
                    task_name = f"classification_{tile}_model_{model_name}_seed_{seed}"
                    target_model = f"model_{model_name}_seed_{seed}"

                    number_of_chunks = self.number_of_chunks
                    if chunk_size_mode == "user_fixed":
                        rois, _ = ru.get_raster_chunks_boundaries(
                            Path(self.output_path)
                            / "features"
                            / tile
                            / "tmp"
                            / "MaskCommunSL.tif",
                            chunk_config=ru.ChunkConfig(
                                chunk_size_mode=rcf.ReadConfigFile(self.cfg).get_param(
                                    "python_data_managing", "chunk_size_mode"
                                ),
                                number_of_chunks=self.number_of_chunks,
                                chunk_size_x=rcf.ReadConfigFile(self.cfg).get_param(
                                    "python_data_managing", "chunk_size_x"
                                ),
                                chunk_size_y=rcf.ReadConfigFile(self.cfg).get_param(
                                    "python_data_managing", "chunk_size_y"
                                ),
                                padding_size_x=rcf.ReadConfigFile(self.cfg).get_param(
                                    "python_data_managing", "padding_size_x"
                                ),
                                padding_size_y=rcf.ReadConfigFile(self.cfg).get_param(
                                    "python_data_managing", "padding_size_y"
                                ),
                            ),
                        )
                        number_of_chunks = len(rois)
                    for chunk in range(number_of_chunks):
                        task_params = self.get_classification_params(
                            model_name, tile, seed, working_directory, chunk
                        )
                        task = self.I2Task(
                            task_name=f"{task_name}_{chunk}",
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters=task_params,
                            task_resources=self.get_resources(),
                        )
                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="tile_tasks_model_mode",
                            task_sub_group=f"{tile}_{model_name}_{seed}_{chunk}",
                            task_dep_dico={"region_tasks": [target_model]},
                        )

    def get_classification_params(
        self,
        region_name: str,
        tile_name: str,
        seed: int,
        working_dir: str | None,
        target_chunk: int | None = None,
    ) -> I2ParameterT:
        """Generate the classification parameters

        Parameters
        ----------
        region_name:
            the region to be classified
        tile_name:
            the tile to be classified
        seed:
            the run targeted
        working_dir:
            Working directory
        target_chunk:
            the chunk targeted
        """
        target_model_name = f"model_{region_name}_seed_{seed}.txt"
        classif_file = str(
            Path(self.output_path)
            / "classif"
            / f"Classif_{tile_name}_model_{region_name}_seed_{seed}.tif"
        )
        confidence_file = str(
            Path(self.output_path)
            / "classif"
            / f"{tile_name}_model_{region_name}_confidence_seed_{seed}.tif"
        )
        classif_mask_file = str(
            Path(self.output_path)
            / "classif"
            / "MASK"
            / f"MASK_region_{region_name.split('f')[0]}_{tile_name}.tif"
        )
        model_file = str(Path(self.output_path) / "model" / target_model_name)

        out_proba_file = None
        all_seeds_class = None
        if self.enable_proba_map:
            all_seeds_class = []
            for _, region_class in self.class_by_models.items():
                all_seeds_class += region_class
            all_seeds_class = sorted(list(set(all_seeds_class)))

            out_proba_file = (
                f"PROBAMAP_{tile_name}_model" f"_{region_name}_seed_{seed}.tif"
            )
            out_proba_file = str(Path(self.output_path) / "classif" / out_proba_file)

        enable_gap, enable_raw = rcf.ReadConfigFile(self.cfg).get_param(
            "python_data_managing", "data_mode_access"
        )
        features_from_raw_dates = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "features_from_raw_dates"
        )
        pred_parameters = PredictionParameters(
            tile_name=tile_name,
            sensors_parameters=rcf.Iota2Parameters(
                rcf.ReadConfigFile(self.cfg)
            ).get_sensors_parameters(tile_name),
            enabled_gap=enable_gap,
            enabled_raw=enable_raw,
            sensors_dates=self.sensors_dates,
            features_from_raw_dates=features_from_raw_dates,
        )
        if features_from_raw_dates:
            pred_parameters.enabled_raw = True

        pytorch_params = rcf.ReadConfigFile(self.cfg).get_param(
            "arg_train", "deep_learning_parameters"
        )

        if self.external_features_flag:
            pred_parameters.exogeneous_data = rcf.ReadConfigFile(self.cfg).get_param(
                "external_features", "exogeneous_data"
            )

        param = {
            "f": iota2.classification.pytorch_prediction.pytorch_predict,
            "pred_output": PredictionOutput(
                classif=classif_file,
                confidence=confidence_file,
                proba=out_proba_file,
                output_path=rcf.ReadConfigFile(self.cfg).get_param(
                    "chain", "output_path"
                ),
            ),
            "number_of_chunks": self.number_of_chunks,
            "targeted_chunk": target_chunk,
            "custom_features_parameters": CustomFeaturesParameters(
                enable=self.external_features_flag,
                module=rcf.ReadConfigFile(self.cfg).get_param(
                    "external_features", "module"
                ),
                functions=rcf.ReadConfigFile(self.cfg).get_param(
                    "external_features", "functions"
                ),
                concat_mode=rcf.ReadConfigFile(self.cfg).get_param(
                    "external_features", "concat_mode"
                ),
            ),
            "torch_pred_paths": TorchPredictionPaths(
                mask=str(classif_mask_file),
                encoder_convert_file=model_file.replace(".txt", "_encoder.pickle"),
                working_dir=working_dir,
                hyperparam_file=model_file.replace(".txt", "_hyper.pickle"),
            ),
            "all_class_labels": all_seeds_class,
            "torch_parameters": PytorchPredictionParameters(
                nn_name=pytorch_params["dl_name"],
                model_file=model_file,
                model_parameters_file=model_file.replace(".txt", "_parameters.pickle"),
                external_nn_module=pytorch_params.get("dl_module", None),
                max_inference_size=rcf.ReadConfigFile(self.cfg).get_param(
                    "python_data_managing", "max_nn_inference_size"
                ),
            ),
            "pred_parameters": pred_parameters,
        }
        return param

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Generate classifications"
        return description
