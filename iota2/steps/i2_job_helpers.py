# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Definition of classes to help job scheduling on different platforms"""
import logging
import os
import re
import time
from abc import ABC, abstractmethod
from collections.abc import Callable
from dataclasses import dataclass, fields, is_dataclass
from pathlib import Path
from subprocess import PIPE, STDOUT, Popen
from typing import Any

import dill
import pandas as pd

from config import Config
from iota2.common.service_error import ConfigFileError
from iota2.configuration_files.sections.cfg_utils import Iota2ParamSection

LOGGER = logging.getLogger("distributed.worker")


@dataclass
class TaskResources:  # pylint: disable=too-many-instance-attributes
    """
    Represents the resources required for a task.

    Attributes
    ----------
    nb_cpu : int, default=1
        Number of CPUs required.
    ram : str, default="5gb"
        Amount of RAM required.
    walltime : str, default="01:00:00"
        Maximum wall time for the task.
    nb_gpu : int | None
        Number of GPUs required (default is None).
    qos : str | None
        Quality of Service (QoS) for the job (default is None).
    queue : str | None
        Queue for the job (default is None).
    partition : str | None
        Partition for the job (default is None).
    resource_block_name : str | None
        Name of the resource block (default is None).
    resource_block_found : bool, default=False
        Indicates if the resource block was found.
    """

    nb_cpu: int = 1
    ram: str = "5gb"
    walltime: str = "01:00:00"
    nb_gpu: int | None = None
    qos: str | None = None
    queue: str | None = None
    partition: str | None = None
    resource_block_name: str | None = None
    resource_block_found: bool = False


@dataclass
class JobFileDescriptor:
    """
    Descriptor for job files and related settings.

    Attributes
    ----------
    job_file : Path
        Path to the job file.
    job_name : str
        Name of the job.
    resources : TaskResources
        Resources required for the job.
    err_file : str
        Path to the error log file.
    std_file : str
        Path to the standard output log file.
    """

    job_file: Path
    job_name: str
    err_file: str
    std_file: str
    resources: TaskResources | None = None

    def __post_init__(self) -> None:
        if not self.resources:
            self.resources = TaskResources()


@dataclass
class FunctionDescriptor:
    """
    Descriptor for a function to be executed.

    Attributes
    ----------
    func : Callable | None
        Function to be executed (default is None).
    func_kwargs : dict[str, Any] | None
        Keyword arguments for the function (default is None).
    logger_level : str | None
        Logger level for the function (default is None).
    """

    func: Callable | None = None
    func_kwargs: dict[str, Any] | None = None
    logger_level: str | None = None


class JobFileHelper(ABC):
    """Abstract class to use different scheduling soft."""

    @property
    @abstractmethod
    def submit_cmd(self) -> str:
        """Define how to submit jobs."""

    @property
    @abstractmethod
    def extension(self) -> str:
        """Define jobfile extension."""

    def task_env(self, cpu: int) -> str:
        """Set environment for tasks."""
        return (
            f"\nmodule purge\n"
            f"export PYTHONPATH={os.environ.get('PYTHONPATH')}\n"
            f"export PATH={os.environ.get('PATH')}\n"
            f"export LD_LIBRARY_PATH={os.environ.get('LD_LIBRARY_PATH')}\n"
            f"export OTB_APPLICATION_PATH={os.environ.get('OTB_APPLICATION_PATH')}\n"
            f"export GDAL_DATA={os.environ.get('GDAL_DATA')}\n"
            f"export GEOTIFF_CSV={os.environ.get('GEOTIFF_CSV')}\n"
            "export GDAL_CACHEMAX=128\n"
            f"export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS={cpu}\n\n"
        )

    @staticmethod
    @abstractmethod
    def get_task_status(
        log_err: str, log_out: str, cmd_stdout: str
    ) -> tuple[str, bool]:
        """Guess tasks status by checking log files or thanks to the job id."""

    @abstractmethod
    def write(
        self,
        job_descriptor: JobFileDescriptor,
        function_descriptor: FunctionDescriptor,
        force_executable: str | None = None,
    ) -> None:
        """Write job file on disk."""
        raise NotImplementedError

    @staticmethod
    def get_executable_task(
        log_out: str,
        func: Callable,
        func_kwargs: dict,
        logger_lvl: str,
    ) -> str:
        """Build executable cmd and serialise object."""
        serialized_file = log_out.replace(".out", ".dill")
        with open(serialized_file, "wb") as file_to_dump:
            dill.dump((func, func_kwargs, logger_lvl), file_to_dump)
        cmd = f"task_launcher.py -dill_file {serialized_file}"
        return cmd


class PBSJobHelper(JobFileHelper):
    """Class defining some helper functions with PBS scheduler."""

    submit_cmd = "qsub -W block=true "
    extension = "pbs"

    @staticmethod
    def get_task_status(
        log_err: str, log_out: str, cmd_stdout: str
    ) -> tuple[str, bool]:
        """
        Guess tasks status by checking log files or thanks to the job id.

        Parameters
        ----------
        log_err
            error log file.
        log_out
            standard log file.
        cmd_stdout
            not used in this context.

        Notes
        -----
        the returned tuple is formated as the following
        (task_status, killed_worker)
        where task_status is in ("done", "failed", "unlaunchable")
        and killed_worker inform if the PBS scheduler kill the task due
        to resources consumption.
        """
        killed_worker = False
        task_status = "failed"

        done_pattern = {"JOB EXIT CODE": 0}
        walltime_killed_pattern = "PBS: job killed: walltime"
        time.sleep(10)

        # it has been observed that is some conditions, the log_err file is not
        # generated by PBS (nothing to put in ?)
        if Path(log_out).exists():
            with open(log_out, encoding="utf-8") as log_file:
                for line in log_file:
                    if list(done_pattern.keys())[0] in line:
                        code = list(map(int, re.findall(r"\b\d+\b", line)))[0]
                        break
            if code == done_pattern[list(done_pattern.keys())[0]]:
                task_status = "done"
                killed_worker = False
            if Path(log_err).exists():
                with open(log_err, encoding="utf-8") as log_file:
                    for line in log_file:
                        if walltime_killed_pattern in line:
                            killed_worker = True
                            break
            if code in [137, 271]:
                killed_worker = True
            elif code == 1:
                task_status = "failed"
        else:
            print(f"logs not found : {log_out}")
            task_status = "failed"
            killed_worker = True
        return task_status, killed_worker

    def write(
        self,
        job_descriptor: JobFileDescriptor,
        function_descriptor: FunctionDescriptor,
        force_executable: str | None = None,
    ) -> None:
        """
        Write a job script for execution on a PBS job scheduler.

        Parameters
        ----------
        job_descriptor : JobFileDescriptor
            Descriptor containing job file, job name, and resource requirements
            (CPU, RAM, GPU, walltime).
        function_descriptor : FunctionDescriptor
            Descriptor containing the function to be executed, its arguments, and the logger level.
        force_executable : str | None
            If provided, this executable will be used instead of the function in the
            function descriptor (default is None).
        """
        assert job_descriptor.resources
        execution: str | None
        nb_gpu = (
            f":ngpus={job_descriptor.resources.nb_gpu}"
            if job_descriptor.resources.nb_gpu
            else ""
        )
        header = (
            "#!/bin/bash\n"
            f"#PBS -N {job_descriptor.job_name}\n"
            f"#PBS -l select=1:ncpus={job_descriptor.resources.nb_cpu}"
            f":mem={job_descriptor.resources.ram}{nb_gpu}\n"
            f"#PBS -l walltime={job_descriptor.resources.walltime}\n"
            f"#PBS -e {job_descriptor.err_file}\n"
            f"#PBS -o {job_descriptor.std_file}\n"
        )
        if job_descriptor.resources.queue:
            header = f"{header}#PBS -q {job_descriptor.resources.queue}"

        if (
            force_executable is None
            and function_descriptor.func
            and function_descriptor.func_kwargs
            and function_descriptor.logger_level
        ):
            serialized_file = job_descriptor.std_file.replace(".out", ".dill")
            with open(serialized_file, "wb") as file_to_dump:
                dill.dump(
                    (
                        function_descriptor.func,
                        function_descriptor.func_kwargs,
                        function_descriptor.logger_level,
                    ),
                    file_to_dump,
                )
            execution = self.get_executable_task(
                job_descriptor.std_file,
                function_descriptor.func,
                function_descriptor.func_kwargs,
                function_descriptor.logger_level,
            )
        else:
            execution = force_executable
        assert execution
        pbs_content = (
            header + self.task_env(job_descriptor.resources.nb_cpu) + execution
        )

        with open(job_descriptor.job_file, "w", encoding="utf-8") as pbs_file:
            pbs_file.write(pbs_content)


class SlurmJobHelper(JobFileHelper):
    """Class defining some helper functions with Slurm scheduler."""

    submit_cmd = "sbatch -W "
    extension = "slurm"

    def __init__(self, static_parameters: Iota2ParamSection | None = None):
        self.static_parameters = static_parameters

    @staticmethod
    def get_task_status(
        log_err: str, log_out: str, cmd_stdout: str
    ) -> tuple[str, bool]:
        """Guess tasks status by checking log files or thanks to the job id.

        Parameters
        ----------
        log_err
            error log file.
        log_out
            standard log file.
        cmd_stdout
            string containing the job id.

        Notes
        -----
        the returned tuple is formated as the following
        (task_status, killed_worker)
        where task_status is in ("done", "failed", "unlaunchable")
        and killed_worker inform if the Slurm scheduler kill the task.
        """
        killed_worker = False
        task_status = "done"
        job_id = list(map(int, re.findall(r"\b\d+\b", cmd_stdout)))[0]
        job_summary_cmd = f"sacct -j {job_id} --format=All -p --delimiter=|".split(" ")
        with Popen(job_summary_cmd, shell=False, stdout=PIPE, stderr=STDOUT) as process:
            process.wait()
            stdout, _ = process.communicate()

        lines = stdout.decode("utf-8").split("\n")
        columns = lines[0].split("|")

        job_summary_df = pd.DataFrame(
            [row.split("|") for row in lines[1:-1]], columns=columns
        )
        job_status = job_summary_df[["JobName", "State"]]
        for val in job_status["State"].values:
            if val != "COMPLETED":
                task_status, killed_worker = ("failed", True)
                break
        return task_status, killed_worker

    def write(
        self,
        job_descriptor: JobFileDescriptor,
        function_descriptor: FunctionDescriptor,
        force_executable: str | None = None,
    ) -> None:
        """
        Write a job script for execution on a SLURM job scheduler.

        Parameters
        ----------
        job_descriptor : JobFileDescriptor
            Descriptor containing job file, job name, and resource requirements
            (CPU, RAM, GPU, walltime, partition, QoS).
        function_descriptor : FunctionDescriptor
            Descriptor containing the function to be executed, its arguments, and the logger level.
        force_executable : str | None
            If provided, this executable will be used instead of the function in the function
            descriptor (default is None).
        """
        gpu_req = ""
        assert job_descriptor.resources
        execution: str | None
        if job_descriptor.resources.nb_gpu:
            gpu_req = f"#SBATCH --gres=gpu:{job_descriptor.resources.nb_gpu}\n"
        header = (
            "#!/bin/bash\n"
            f"#SBATCH --job-name {job_descriptor.job_name}\n"
            "#SBATCH -N 1\n"
            "#SBATCH --ntasks=1\n"
            f"#SBATCH --cpus-per-task={job_descriptor.resources.nb_cpu}\n"
            f"#SBATCH --mem={job_descriptor.resources.ram}\n"
            f"{gpu_req}"
            f"#SBATCH --time={job_descriptor.resources.walltime}\n"
            f"#SBATCH --error={job_descriptor.err_file}\n"
            f"#SBATCH --output={job_descriptor.std_file}\n"
        )
        if job_descriptor.resources.partition:
            header = (
                f"{header}#SBATCH --partition={job_descriptor.resources.partition}\n"
            )
        if job_descriptor.resources.qos:
            header = f"{header}#SBATCH --qos={job_descriptor.resources.qos}\n"
        if self.static_parameters and self.static_parameters.account:
            header = f"{header}#SBATCH --account={self.static_parameters.account}\n"
        if (
            force_executable is None
            and function_descriptor.func
            and function_descriptor.func_kwargs
            and function_descriptor.logger_level
        ):
            serialized_file = job_descriptor.std_file.replace(".out", ".dill")
            with open(serialized_file, "wb") as serialized:
                dill.dump(
                    (
                        function_descriptor.func,
                        function_descriptor.func_kwargs,
                        function_descriptor.logger_level,
                    ),
                    serialized,
                )
            execution = self.get_executable_task(
                job_descriptor.std_file,
                function_descriptor.func,
                function_descriptor.func_kwargs,
                function_descriptor.logger_level,
            )
        else:
            execution = force_executable
        assert execution
        slurm_content = (
            header + self.task_env(job_descriptor.resources.nb_cpu) + execution
        )

        with open(job_descriptor.job_file, "w", encoding="utf-8") as slurm_file:
            slurm_file.write(slurm_content)


class JobHelper:
    """Writer class dedicated to write job file regarding scheduler tech."""

    def __init__(
        self,
        cfg_resources_file: Path | None = None,
        cfg_resources_name: str | None = None,
        slurm_static_params: Iota2ParamSection | None = None,
    ):
        """
        Parameters
        ----------
        cfg_resources_file
            file containing resources
        cfg_resources_name
            resource block name in resources file
        """
        self.cfg_resources_file = cfg_resources_file
        self.cfg_resources_name = cfg_resources_name
        self.inventory_jobfile_helper: dict[str, JobFileHelper] = {
            "pbs": PBSJobHelper(),
            "slurm": SlurmJobHelper(slurm_static_params),
        }
        self.avail_schedulers = list(self.inventory_jobfile_helper.keys())
        if self.cfg_resources_file and isinstance(self.cfg_resources_file, str):
            self.cfg_resources_file = Path(cfg_resources_file)
        if self.cfg_resources_file:
            if not Path.exists(self.cfg_resources_file):
                raise FileNotFoundError(
                    f"file {cfg_resources_file}" " does not exists."
                )
            assert cfg_resources_name, (
                "'cfg_resources_file' then " "'cfg_resources_name' is mandatory"
            )
        self.resources = self.parse_resource_file(
            cfg_resources_name, self.cfg_resources_file
        )

    def force_resource(self, resource_name: str, value: str | int) -> None:
        """Force resource."""
        is_invalid = bool(
            self.get_invalid_dataclass_attribute(TaskResources, [resource_name])
        )
        if is_invalid:
            raise ValueError(
                f"{resource_name} not in available resources :"
                f" '{', '.join([field.name for field in list(fields(self.resources))])}'"
            )
        setattr(self.resources, resource_name, value)

    def check_scheduler(self, scheduler_name: str) -> None:
        """Check if the scheduler is available."""
        if scheduler_name not in self.avail_schedulers:
            raise ValueError(
                f"scheduler {scheduler_name} not in : "
                f"{(',').join(self.avail_schedulers)}"
            )

    def write(
        self,
        scheduler: str,
        job_descriptor: JobFileDescriptor,
        func_descriptor: FunctionDescriptor = FunctionDescriptor(),
        force_executable: str | None = None,
    ) -> None:
        """
        Write a job file using the specified scheduler.

        This function checks the scheduler and writes the job file
        using the appropriate scheduler helper. It optionally forces
        the use of a specified executable.

        Parameters
        ----------
        scheduler : str
            The scheduler to use (e.g., 'pbs', 'slurm').
        job_descriptor : JobFileDescriptor
            Descriptor for the job file and related settings.
        func_descriptor : FunctionDescriptor, optional
            Descriptor for the function to be executed (default is an empty FunctionDescriptor).
        force_executable : str | None
            Path to a specific executable to force the use of (default is None).
        """
        self.check_scheduler(scheduler)
        current_scheduler = self.inventory_jobfile_helper[scheduler]
        current_scheduler.write(
            JobFileDescriptor(
                job_descriptor.job_file,
                job_descriptor.job_name,
                job_descriptor.err_file,
                job_descriptor.std_file,
                self.resources,
            ),
            func_descriptor,
            force_executable,
        )

    def get_task_status(
        self,
        scheduler_name: str,
        log_err: str,
        log_out: str,
        job_id: str,
    ) -> tuple[str, bool]:
        """Guess tasks status by checking log files or thanks to the job id.

        Parameters
        ----------
        scheduler_name
            scheduler name (ie: 'pbs' or 'slurm').
        log_err
            error log file.
        log_out
            standard log file.
        job_id
            string containing the job id.

        Notes
        -----
        the returned tuple is formated as the following
        (task_status, killed_worker)
        where task_status is in ("done", "failed", "unlaunchable")
        and killed_worker inform if the PBS scheduler kill the task due
        to resources consumption.
        """
        self.check_scheduler(scheduler_name)
        scheduler = self.inventory_jobfile_helper[scheduler_name]
        return scheduler.get_task_status(log_err, log_out, job_id)

    @staticmethod
    def get_invalid_dataclass_attribute(
        dataclass_type: type, attributes: list[str]
    ) -> list[str]:
        """Check if a list of attributes exists in dataclass."""
        invalid_attribute = []
        if not is_dataclass(dataclass_type):
            raise TypeError(f"{dataclass_type} is not a dataclass")
        valid_attributes = [str(field.name) for field in list(fields(dataclass_type))]
        for attribute in attributes:
            if attribute not in valid_attributes:
                invalid_attribute.append(attribute)
        return invalid_attribute

    @classmethod
    def parse_resource_file(
        cls, step_name: str | None = None, cfg_resources_file: Path | None = None
    ) -> TaskResources:
        """Parse a configuration file dedicated to reserve resources on HPC.
        Notes
        -----
        return a TaskResources dataclass
        """
        cfg_resources = {}
        if cfg_resources_file and cfg_resources_file.exists():
            cfg_resources = Config(str(cfg_resources_file)).as_dict()

        resource_raw = {}
        if step_name and step_name in cfg_resources:
            resource_raw = cfg_resources[step_name]
            invalid_attributes = cls.get_invalid_dataclass_attribute(
                TaskResources, list(resource_raw.keys())
            )
            if invalid_attributes:
                raise ConfigFileError(
                    f"Invalid keys '{', '.join(invalid_attributes)}' "
                    f"found in file {cfg_resources_file} in bloc {step_name}."
                )
        resource = TaskResources(**resource_raw)
        resource.resource_block_name = step_name
        if cfg_resources:
            resource.resource_block_found = step_name in cfg_resources
        return resource
