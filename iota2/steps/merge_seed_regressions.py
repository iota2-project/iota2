#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Merge final regressions step"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.regression import merge_final_regressions as mergeRg
from iota2.steps import iota2_step
from iota2.validation import classification_shaping as CS

LOGGER = logging.getLogger("distributed.worker")


class MergeSeedRegressions(iota2_step.Step):
    """Class for merging final regressions"""

    resources_block_name = "merge_final_regressions"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        runs = parameters.get_param("arg_train", "runs")

        merge_run_method = rcf.ReadConfigFile(cfg).get_param(
            "multi_run_fusion", "merge_run_method"
        )

        tiles = parameters.get_param("chain", "list_tile").split(" ")
        for tile in tiles:
            task = self.I2Task(
                task_name=f"merge_run_{tile}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": mergeRg.merge_final_regressions,
                    "iota2_dir": output_path,
                    "runs": runs,
                    "method": merge_run_method,
                    "working_directory": working_directory,
                    "tile": tile,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="mosaic",
                task_sub_group="merge_run_" + tile,
                task_dep_dico={"mosaic": ["mosaic"]},
            )

        # use a classification as ref image
        reference = output_path + "/final/Regression_Seed_0.tif"

        task = self.I2Task(
            task_name="merge_run_mosaic",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": CS.mosaic_regression,
                "tiles": tiles,
                "file_name": "fusion",
                "output_path": output_path + "/final/Regressions_fusion.tif",
                "path_classif": output_path + "/final/TMP",
                "reference_image": reference,
            },
            task_resources=self.get_resources(),
        )

        dep_values = ["merge_run_" + tile for tile in tiles]

        self.add_task_to_i2_processing_graph(
            task,
            task_group="mosaic",
            task_sub_group="merge_run_mosaic",
            task_dep_dico={"mosaic": dep_values},
        )

        task = self.I2Task(
            task_name="merge_run_mosaic_confidence",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": CS.mosaic_regression,
                "tiles": self.tiles,
                "file_name": "confidence",
                "output_path": output_path + "/final/Confidence.tif",
                "path_classif": output_path + "/final/TMP",
                "reference_image": reference,
            },
            task_resources=self.get_resources(),
        )

        dep_values = ["merge_run_" + tile for tile in tiles]

        self.add_task_to_i2_processing_graph(
            task,
            task_group="mosaic",
            task_sub_group="merge_run_mosaic_confidence",
            task_dep_dico={"mosaic": dep_values},
        )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Merge final regressions"
        return description


class MergeSeedValidation(iota2_step.Step):
    """Class for merging regressions validation"""

    resources_block_name = "merge_seed_validation"

    def __init__(self, cfg: str, cfg_resources_file: Path | None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        data_field = parameters.get_param("chain", "data_field")
        runs = parameters.get_param("arg_train", "runs")
        keep_runs_results = parameters.get_param(
            "multi_run_fusion", "keep_runs_results"
        )
        task = self.I2Task(
            task_name="merge_validation",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": mergeRg.merge_final_validation,
                "iota2_dir": output_path,
                "data_field": data_field,
                "runs": runs,
                "keep_runs_results": keep_runs_results,
            },
            task_resources=self.get_resources(),
        )
        if len(self.tiles) > 1:
            dep_values = ["merge_metrics_mosaic" + str(seed) for seed in range(runs)]
            if runs > 1:
                dep_values = ["merge_metrics_mosaic"]

        elif runs > 1:
            dep_values = ["merge_metrics_" + tile for tile in self.tiles]

        else:
            dep_values = [
                tile + "_" + str(seed) for tile, seed in zip(self.tiles, range(runs))
            ]

        self.add_task_to_i2_processing_graph(
            task,
            task_group="merge_final_validation",
            task_sub_group="merge_final_validation",
            task_dep_dico={"final_report": dep_values},
        )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Merge final validation"
        return description
