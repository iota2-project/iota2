#!/usr/bin/python

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the OBIA classification task"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.segmentation import prepare_segmentation_obia as pso
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class ObiaClassification(iota2_step.Step):
    """Class for launching the OBIA classification step"""

    resources_block_name = "obia_classification"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")

        runs = parameters.get_param("arg_train", "runs")
        field_region = parameters.get_param("chain", "region_field")
        if field_region is None:
            field_region = "region"
        res = parameters.get_param("chain", "spatial_resolution")[0]
        stats = parameters.get_param("obia", "stats_used")
        buffer_size = parameters.get_param("obia", "buffer_size")
        dep_dict = {}
        for model_name, model_meta in self.spatial_models_distribution.items():
            for tile in model_meta["tiles"]:
                if tile not in dep_dict:
                    dep_dict[tile] = [model_name]
                elif model_name not in dep_dict[tile]:
                    dep_dict[tile].append(model_name)
        for seed in range(runs):
            for tile in self.tiles:
                task = self.I2Task(
                    task_name=f"classif_tile_{tile}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": pso.classify_zonal_stats,
                        "iota2_directory": output_path,
                        "tile": tile,
                        "sensors_parameters": rcf.Iota2Parameters(
                            rcf.ReadConfigFile(self.cfg)
                        ).get_sensors_parameters(tile),
                        "seed": seed,
                        "spatial_res": res,
                        "stats": stats,
                        "seg_field": self.i2_const.i2_segmentation_field_name,
                        "region_field": field_region,
                        "buffer_size": buffer_size,
                        "working_dir": working_directory,
                    },
                    task_resources=self.get_resources(),
                )
                dep = [f"model_{reg}_seed_{seed}" for reg in dep_dict[tile]]
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks",
                    task_sub_group=f"{tile}_seed_{seed}",
                    task_dep_dico={"region_tasks": dep},
                )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Classify tiles using zonal stats"
        return description
