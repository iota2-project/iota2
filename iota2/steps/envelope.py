#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Launch the enveloppe step"""
import logging
from pathlib import Path

from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import tile_envelope as env
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class Envelope(iota2_step.Step):
    """Class for launching the envelope step"""

    resources_block_name = "envelope"

    def __init__(
        self,
        cfg: str,
        cfg_resources_file: Path | None,
        working_directory: str | None = None,
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        parameters = rcf.ReadConfigFile(self.cfg)
        output_path = parameters.get_param("chain", "output_path")
        proj = int(parameters.get_param("chain", "proj").split(":")[-1])
        padding_size = 0
        custom_features = parameters.get_param(
            "external_features", "external_features_flag"
        )
        if custom_features:
            padding_size_x = parameters.get_param(
                "python_data_managing", "padding_size_x"
            )
            padding_size_y = parameters.get_param(
                "python_data_managing", "padding_size_y"
            )
            padding_size = max(padding_size_x, padding_size_y)

        task = self.I2Task(
            task_name="tiles_envelopes",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": env.generate_shape_tile,
                "tiles": self.tiles,
                "path_wd": working_directory,
                "output_path": output_path,
                "proj": proj,
                "padding_size": padding_size,
            },
            task_resources=self.get_resources(),
        )
        self.add_task_to_i2_processing_graph(
            task,
            task_group="vector",
            task_sub_group="vector",
            task_dep_dico={"tile_tasks": self.tiles},
        )

    @classmethod
    def step_description(cls) -> str:
        """
        function use to print a short description of the step's purpose
        """
        description = "Generate tile's envelope"
        return description
