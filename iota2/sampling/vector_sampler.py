#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
The Vector Sampler Module
"""
import logging
import multiprocessing as mp
import os
import shutil
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Literal

import iota2.common.utils as ut
from iota2.common import file_utils as fu
from iota2.common import generate_features as genFeatures
from iota2.common import i2_constants, service_error
from iota2.common.compression_options import delete_compression_suffix
from iota2.common.custom_numpy_features import (
    ChunkParameters,
    ComputeCustomFeaturesArgs,
    MaskingRaster,
    compute_custom_features,
)
from iota2.common.generate_features import FeaturesMapParameters
from iota2.common.otb_app_bank import AvailableOTBApp, create_application, execute_app
from iota2.common.raster_utils import turn_off_otb_pipelines
from iota2.sampling.samples_selection import prepare_selection
from iota2.sampling.vector_formatting import split_vector_by_region
from iota2.typings.i2_types import DBInfo, OtbApp, OtbAppWithDep
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.vector_functions import get_vector_proj

LOGGER = logging.getLogger("distributed.worker")
# in order to avoid issue 'No handlers could be found for logger...'
LOGGER.addHandler(logging.NullHandler())
SensorsParams = dict[str, str | list[str] | int]

FunctionNameWithParams = tuple[str, dict[str, Any]]
I2_CONST = i2_constants.Iota2Constants()


def get_features_application(
    database: DBInfo,
    samples: str,
    sensors_parameters: SensorsParams,
    ram: int = 128,
    force_standard_labels: bool = False,
    features_from_raw_dates: bool = False,
    custom_features: ComputeCustomFeaturesArgs | None = None,
    logger: logging.Logger = LOGGER,
) -> OtbAppWithDep:
    """
    Generates and configures an OTB (Orfeo ToolBox) application for feature extraction.

    Parameters:
    -----------
    database : DBInfo
        Database information containing the path to the training shapefile.
    samples : str
        Path where the extracted samples will be saved.
    sensors_parameters : SensorsParams
        Parameters related to the sensors used for data acquisition.
    ram : int, optional
        Amount of RAM (in MB) allocated for the OTB application. Default is 128 MB.
    force_standard_labels : bool, optional
        If True, forces the use of standard labels. Default is False.
    features_from_raw_dates : bool, optional
        If True, generates features from raw dates instead of interpolated data. Default is False.
    custom_features : ComputeCustomFeaturesArgs | None
        Custom feature computation arguments, if any. Default is None.
    logger : logging.Logger, optional
        Logger for logging information and debug messages. Default is LOGGER.

    Returns:
    --------
    OtbAppWithDep
        A tuple containing the configured OTB SampleExtraction application and a list
        of dependencies.
    """
    train_shape = database.db_file
    assert isinstance(train_shape, str)
    tile = train_shape.split("/")[-1].split(".")[0].split("_")[0]
    features_map_parameters = FeaturesMapParameters(
        working_directory="",
        tile=tile,
        output_path="",
        sensors_parameters=sensors_parameters,
        force_standard_labels=force_standard_labels,
    )
    (otb_pipelines, labs_dict) = genFeatures.generate_features(
        features_map_parameters,
        logger=logger,
    )

    all_features = otb_pipelines.interpolated_pipeline
    feat_labels = labs_dict.interp

    if features_from_raw_dates:
        assert otb_pipelines.raw_pipeline is not None
        assert otb_pipelines.binary_mask_pipeline is not None
        otb_pipelines.raw_pipeline.Execute()
        otb_pipelines.binary_mask_pipeline.Execute()
        all_features, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {"il": [otb_pipelines.raw_pipeline, otb_pipelines.binary_mask_pipeline]},
        )
        feat_labels = labs_dict.raw + labs_dict.masks

    assert all_features is not None
    all_features.Execute()

    sample_extr, _ = create_application(
        AvailableOTBApp.SAMPLE_EXTRACTION,
        {"vec": train_shape, "out": samples, "outfield": "list", "ram": str(0.7 * ram)},
    )

    if not custom_features:
        sample_extraction_in = all_features.GetParameterOutputImage("out")
        sample_extr.SetParameterInputImage("in", sample_extraction_in)
    else:
        assert custom_features.functions_builder is not None
        otb_pipelines = turn_off_otb_pipelines(
            otb_pipelines,
            turn_off_raw=custom_features.functions_builder.configuration.enabled_raw
            is False,
            turn_off_interpolated=custom_features.functions_builder.configuration.enabled_gap
            is False,
        )
        assert custom_features.chunk_params is not None
        assert custom_features.concatenate_features is not None
        data, _ = compute_custom_features(
            functions_builder=custom_features.functions_builder,
            otb_pipelines=otb_pipelines,
            feat_labels=feat_labels,
            chunk_params=ChunkParameters(
                custom_features.chunk_params.chunk_config,
                custom_features.chunk_params.targeted_chunk,
            ),
            exogeneous_data=custom_features.functions_builder.exogeneous_data_file,
            masking_raster=MaskingRaster(
                custom_features.masking_raster.mask_valid_data,
                custom_features.masking_raster.mask_value,
            ),
            concatenate_features=custom_features.concatenate_features,
            logger=logger,
        )
        sample_extraction_in = data.otb_img
        feat_labels = data.labels
        sample_extr.ImportVectorImage("in", sample_extraction_in)
    if len(feat_labels) >= I2_CONST.sqlite_max_column:
        os.environ["OGR_CSV_MAX_FIELD_COUNT"] = str(len(feat_labels) + 10)
        if Path(samples).suffix == ".sqlite":
            raise service_error.TooManyColumnsError()

    sample_extr.SetParameterStringList(
        "outfield.list.names", [str(feat) for feat in feat_labels]
    )
    sample_extr.UpdateParameters()
    sample_extr.SetParameterStringList("field", [database.data_field.lower()])

    assert otb_pipelines.pipeline_dependencies is not None
    all_dep = [all_features] + otb_pipelines.pipeline_dependencies
    return sample_extr, all_dep


@dataclass
class SampleFlagsParameters:
    """
    A data class for holding sample flags parameters.

    Attributes
    ----------
    force_standard_labels : bool, optional
        Indicates whether to force the use of standard labels. Default is False.
    features_from_raw_dates : bool, optional
        Indicates whether to generate features from raw dates. Default is False.
    sampling_validation : bool, optional
        Indicates whether to perform sampling validation. Default is False.
    """

    force_standard_labels: bool = False
    features_from_raw_dates: bool = False
    sampling_validation: bool = False


def generate_samples_simple(
    database: DBInfo,
    samples: str,
    path_wd: str,
    runs: int,
    sensors_parameters: SensorsParams,
    flags_parameters: SampleFlagsParameters,
    ram: int = 128,
    custom_features: ComputeCustomFeaturesArgs | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Generates samples and features from a database and sample selections.

    Parameters
    ----------
    database : DBInfo
        Information about the database.
    samples : str
        Path to the samples file.
    path_wd : str
        Working directory path.
    runs : int
        Number of runs for sample generation.
    sensors_parameters : SensorsParams
        Parameters for sensors used in sample generation.
    flags_parameters : SampleFlagsParameters
        Flags for sample generation.
    ram : int, optional
        Amount of RAM to use in MB, by default 128.
    custom_features : ComputeCustomFeaturesArgs, optional
        Custom features to compute, by default None.
    logger : logging.Logger, optional
        Logger instance, by default LOGGER.
    """
    tile = Path(samples).stem.split("_")[0]
    output_path = Path(samples).parent.parent

    sample_sel_directory = str(output_path / "samplesSelection")

    if path_wd:
        samples = str(Path(path_wd) / Path(samples).name)

    if database.db_file:
        sample_selection = database.db_file
    else:
        sample_selection = prepare_selection(
            sample_sel_directory, tile, working_directory=None
        )
    sample_extr: OtbApp
    sample_extr, _ = get_features_application(
        DBInfo(database.data_field, sample_selection),
        samples,
        sensors_parameters,
        ram,
        force_standard_labels=flags_parameters.force_standard_labels,
        features_from_raw_dates=flags_parameters.features_from_raw_dates,
        custom_features=custom_features,
        logger=logger,
    )
    sample_extraction_output = str(
        output_path
        / "learningSamples"
        / Path(delete_compression_suffix(sample_extr.GetParameterValue("out"))).name
    )
    if not Path(sample_extraction_output).exists():
        logger.info("--------> Start Sample Extraction <--------")
        logger.info(
            f"RAM before features extraction :" f" {fu.memory_usage_psutil()} MB"
        )
        print("RAM before features extraction : " f"{fu.memory_usage_psutil()} MB")

        # sample_extr.ExecuteAndWriteOutput()
        multi_proc = mp.Process(target=execute_app, args=[sample_extr])
        multi_proc.start()
        multi_proc.join()

        print("RAM after features extraction :" f" {fu.memory_usage_psutil()} MB")
        logger.info("RAM after features extraction :" f" {fu.memory_usage_psutil()} MB")
        logger.info("--------> END Sample Extraction <--------")

        split_and_copy_vectors(
            custom_features,
            database,
            flags_parameters,
            logger,
            output_path,
            path_wd,
            runs,
            sample_extr,
            sample_sel_directory,
            samples,
        )


def split_and_copy_vectors(
    custom_features: ComputeCustomFeaturesArgs | None,
    database: DBInfo,
    flags_parameters: SampleFlagsParameters,
    logger: logging.Logger,
    output_path: Path,
    path_wd: str | None,
    runs: int,
    sample_extr: OtbApp,
    sample_sel_directory: str,
    samples: str,
) -> None:
    """
    Splits vector data by region and copies the resulting files to a specified directory.

    This function performs a split of vector data based on regions, and then copies the
    resulting split vectors to a target directory. It also handles projection information and
    manages custom features if provided.

    Parameters
    ----------
    custom_features : ComputeCustomFeaturesArgs | None
        Custom features to be computed for the dataset. If not provided, the default split is used.
    database : DBInfo
        Database information including data fields and region fields.
    flags_parameters : SampleFlagsParameters
        Parameters defining the flags for sample generation, such as whether validation sampling
        is enabled.
    logger : logging.Logger
        Logger instance to record messages and warnings during execution.
    output_path : Path
        Path to the directory where the output files will be stored.
    path_wd : str | None
        Path to the working directory. If not provided, defaults to the output directory.
    runs : int
        Number of runs to perform for the sample splitting process.
    sample_extr : OtbApp
        OTB (Orfeo Toolbox) application object used to extract features.
    sample_sel_directory : str
        Directory path where sample selections are stored.
    samples : str
        Path to the input sample file.

    Raises
    ------
    OSError
        If there is an issue creating the output directories.
    """
    ext = Path(samples).suffix
    tile = Path(samples).stem.split("_")[0]
    folder_features = str(output_path / "features")
    folder_sample = str(output_path / "learningSamples")
    if ext == ".csv":
        proj = None
    else:
        proj = f"EPSG:{get_vector_proj(samples, vf.get_driver(samples))}"
    targeted_chunk = None
    if custom_features is not None:
        assert custom_features.chunk_params
        targeted_chunk = custom_features.chunk_params.targeted_chunk
    split_vectors = split_vector_by_region(
        DBInfo(
            "",
            delete_compression_suffix(sample_extr.GetParameterValue("out")),
            region_field=database.region_field,
        ),
        output_dir=(
            str(Path(output_path) / "learningSamples") if not path_wd else path_wd
        ),
        runs=int(runs),
        proj_out=proj,
        targeted_chunk=targeted_chunk,
        sampling_validation=flags_parameters.sampling_validation,
        sample_selection_dir=sample_sel_directory,
        logger=logger,
    )
    Path(delete_compression_suffix(sample_extr.GetParameterValue("out"))).unlink()
    if path_wd:
        for sample in split_vectors:
            shutil.copy(sample, folder_sample)
    working_directory_features = str(Path(folder_features) / tile / "tmp")
    try:
        Path(working_directory_features).mkdir(parents=True, exist_ok=True)
    except OSError as e:
        logger.warning(f"Unable to create directory {working_directory_features}: {e}")


def generate_samples(
    database: DBInfo,
    output_path: str,
    path_wd: str,
    runs: int,
    sensors_parameters: SensorsParams,
    sample_flag: SampleFlagsParameters,
    task_cfg: ut.TaskConfig,
    sample_selection: str | None = None,
    custom_features: ComputeCustomFeaturesArgs | None = None,
    learning_samples_extension: Literal["csv", "sqlite"] = "sqlite",
) -> str | None:
    """
    Generates samples based on the provided configuration and parameters.

    Parameters
    ----------
    database : DBInfo
        The database information including file paths and field names.
    output_path : str
        The path where the output files will be saved.
    path_wd : str
        The working directory path.
    runs : int
        The number of runs to perform.
    sensors_parameters : SensorsParams
        Parameters for the sensors used in the sampling.
    sample_flag : SampleFlagsParameters
        Flags to control the sampling process.
    task_cfg : ut.TaskConfig
        Configuration for the task including logging and RAM settings.
    sample_selection : str | None
        Specific sample selection criteria, by default None.
    custom_features : ComputeCustomFeaturesArgs | None
        Arguments for computing custom features, by default None.
    learning_samples_extension : Literal["csv", "sqlite"], optional
        The file extension for the learning samples, by default "sqlite".

    Returns
    -------
    str | None
        The path to the learning samples file (or None)
    """
    train_shape = database.db_file
    data_field = database.data_field
    logger = task_cfg.logger
    assert isinstance(train_shape, (str, Path))
    if not Path(str(train_shape)).exists():
        return None
    folder_sample = str(Path(output_path) / "learningSamples")
    if not Path(folder_sample).exists():
        try:
            Path(folder_sample).mkdir()
        except OSError:
            logger.warning(f"{folder_sample} already exists")
    samples = build_output_file(
        custom_features,
        learning_samples_extension,
        str(train_shape),
        folder_sample,
    )
    generate_samples_simple(
        DBInfo(data_field, sample_selection, region_field=database.region_field),
        samples,
        path_wd,
        runs,
        sensors_parameters,
        sample_flag,
        task_cfg.ram,
        custom_features,
        logger,
    )

    return samples


def build_output_file(
    custom_features: ComputeCustomFeaturesArgs | None,
    learning_samples_extension: Literal["csv", "sqlite"],
    train_shape: str,
    working_directory: str,
) -> str:
    """
    Constructs the path for the output file based on input parameters.

    Parameters
    ----------
    custom_features : ComputeCustomFeaturesArgs
        Arguments related to custom feature computation.
    learning_samples_extension : Literal["csv", "sqlite"]
        The file extension for the learning samples.
    train_shape : str
        The file path of the training shape file.
    working_directory : str
        The working directory where the output file will be stored.

    Returns
    -------
    str
        The constructed output file path.
    """
    if custom_features:
        assert custom_features.chunk_params is not None
        samples = str(
            Path(working_directory)
            / train_shape.split("/")[-1].replace(
                ".shp",
                "_Samples_"
                f"{custom_features.chunk_params.targeted_chunk}.{learning_samples_extension}",
            ),
        )

    else:
        samples = str(
            Path(working_directory)
            / train_shape.split("/")[-1].replace(
                ".shp", "_Samples." + learning_samples_extension
            ),
        )
    return samples
