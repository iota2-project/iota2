"""Data augmentation iota2 module."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import csv
import logging
import shutil
import sqlite3
from collections import Counter
from collections.abc import Iterable
from dataclasses import dataclass
from pathlib import Path
from typing import Literal

import pandas as pd
from osgeo import ogr

from iota2.common import file_utils as fut
from iota2.common import i2_constants
from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.sampling.vector_samples_merge import build_raw_db
from iota2.typings.i2_types import (
    AugmentationParams,
    DBInfo,
    ListPathLike,
    PathLike,
    TransfertQuantity,
)
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def get_user_samples_management(csv_path: PathLike) -> list[TransfertQuantity]:
    """Parse CSV file and return rules to fill samples set.

    Parameters
    ----------
    csv_path :
        path to a csv file

    Example
    -------
    >>> cat /MyFile.csv
            1,2,4,2

    Mean:

    +--------+-------------+------------+----------+
    | source | destination | class name | quantity |
    +========+=============+============+==========+
    |   1    |      2      |      4     |     2    |
    +--------+-------------+------------+----------+

    2 samples of class 4 will be added from model 1 to 2
    """
    delimiter = fut.detect_csv_delimiter(csv_path)
    with open(csv_path, encoding="utf-8") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=delimiter)
        extraction_rules = [TransfertQuantity(*line) for line in csv_reader]
    return extraction_rules


def get_samples_from_model_name(
    model_name: str, samples_set: list[str], logger: logging.Logger = LOGGER
) -> str | None:
    """Use to get the sample file by the model name.

    Parameters
    ----------
    model_name : string
        the model's name
    samples_set: list
        paths to samples
    logger : logging object
        root logger

    Return
    ------
    string
        path to the targeted sample-set.
    """
    model_pos = 2
    sample = [
        path
        for path in samples_set
        if Path(path).name.split("_")[model_pos] == model_name
    ]

    if len(sample) > 1:
        logger.error(
            f"Too many files detected in {Path(samples_set[0]).parent} "
            f"for the model {model_name}"
        )
        raise Exception(
            "ERROR in managementSamples.py, too many sample "
            "files detected for a given model name"
        )
    if not sample:
        logger.warning(f"Model {model_name} not found")
        out_samples = None
    else:
        out_samples = sample[0]

    return out_samples


def count_class(
    source_samples: str,
    data_field: str,
    class_name: str,
    table_name: str = "output",
    logger: logging.Logger = LOGGER,
) -> int:
    """Count in CSV or SQLite file, occurency of a value.

    Parameters
    ----------
    source_samples : string
        Path to a SQLite file
    data_field : string
        Field's name
    class_name : string
        Class name
    table_name : string
        Table's name (in case of sqlite file)
    logger : logging object
        root logger

    Return
    ------
    int
        occurrence of the class into the input file
    """
    ext = Path(source_samples).suffix
    if ext == ".sqlite":
        return count_class_in_sqlite(
            source_samples, data_field, class_name, table_name, logger
        )
    return count_class_in_csv(source_samples, data_field.lower(), class_name, logger)


def count_class_in_sqlite(
    source_samples: PathLike,
    data_field: str,
    class_name: str,
    table_name: str = "output",
    logger: logging.Logger = LOGGER,
) -> int:
    """Count in SQLite file, occurency of a value.

    Parameters
    ----------
    source_samples : string
        Path to a SQLite file
    data_field : string
        Field's name
    class_name : string
        Class name
    table_name : string
        Table's name
    logger : logging object
        root logger

    Return
    ------
    int
        occurrence of the class into the SQLite file
    """
    conn = sqlite3.connect(source_samples)
    cursor = conn.cursor()
    sql = f"select * from {table_name} where {data_field}={class_name}"
    cursor.execute(sql)
    features = cursor.fetchall()
    features_number = len(features)
    if not features_number:
        logger.warning(
            f"There is no class with the label {class_name} in {source_samples}"
        )
        features_number = 0
    return features_number


def count_class_in_csv(
    source_samples: str,
    data_field: str,
    class_name: str,
    logger: logging.Logger = LOGGER,
) -> int:
    """Count in CSV file, occurency of a value.

    Parameters
    ----------
    source_samples : string
        Path to a CSV file
    data_field : string
        Field's name
    class_name : string
        Class name
    logger : logging object
        root logger

    Return
    ------
    int
        occurrence of the class into the CSV file
    """
    df_samples = pd.read_csv(source_samples)
    features_number = df_samples[data_field].value_counts()[int(class_name)].item()
    if not features_number:
        logger.warning(
            f"There is no class with the label {class_name} in {source_samples}"
        )
        features_number = 0
    return features_number


def get_projection(vector_file: str, driver_name: str = "SQLite") -> str:
    """Return the projection of a vector file.

    Parameters
    ----------
    vector_file : string
        Path to a vector file
    driver_name : string
        Ogr driver's name

    Return
    ------
    int
        EPSG's code
    """
    driver = ogr.GetDriverByName(driver_name)
    vector = driver.Open(vector_file)
    layer = vector.GetLayer()
    spatial_ref = layer.GetSpatialRef()
    projection_code = spatial_ref.GetAttrValue("AUTHORITY", 1)
    return str(projection_code)


def get_region_from_sample_name(samples: PathLike) -> str:
    """Get the model's name from samples's path file."""
    region_pos = 2
    return Path(samples).name.split("_")[region_pos]


def samples_augmentation_counter(
    class_count: dict[int, int],
    mode: Literal["minNumber", "balance", "byClass"],
    min_number: int | None = None,
    by_class: str | None = None,
    labels_table: dict | None = None,
) -> dict[int, int]:
    """Compute how many samples must be add into the sample-set according to user's request.

    Parameters
    ----------
    class_count : dict
        count by class
    mode : string
        minNumber/balance/byClass
    min_number : int
        vector should have at least class samples
    by_class : string
        csv path

    Example
    -------
    >>> class_count = {51: 147, 11: 76, 12: 37, 42: 19}
    >>>
    >>> print samples_augmentation_counter(class_count, mode="minNumber", min_number=120)
    >>> {42: 101, 11: 44, 12: 83}
    >>>
    >>> print samples_augmentation_counter(class_count, mode="balance")
    >>> {42: 128, 11: 71, 12: 110}
    >>>
    >>> print samples_augmentation_counter(class_count, mode="byClass",
    >>>                                  min_number=None, by_class="/pathTo/MyFile.csv")
    >>> {42: 11, 51: 33, 12: 1}

    If

    >>> cat /pathTo/MyFile.csv
        51,180
        11,70
        12,38
        42,30

    Return
    ------
    dict
        by class, the number of samples to add in the samples set.
    """
    augmented_class = {}
    if min_number and mode == "minNumber":
        for class_name, count in list(class_count.items()):
            if count < min_number:
                augmented_class[class_name] = min_number - count

    elif mode == "balance":
        max_class_count = class_count[
            max(class_count, key=lambda key: class_count[key])
        ]
        for class_name, count in list(class_count.items()):
            if count < max_class_count:
                augmented_class[class_name] = max_class_count - count

    elif by_class and mode == "byClass":

        delimiter = fut.detect_csv_delimiter(by_class)

        with open(by_class, encoding="utf-8") as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=delimiter)
            for c_name, c_samples in csv_reader:
                class_name = int(c_name)
                class_samples = int(c_samples)
                if labels_table:
                    if isinstance(list(labels_table.keys())[0], int):
                        class_name = labels_table[class_name]
                    else:
                        class_name = labels_table[str(class_name)]
                if class_samples > class_count[class_name]:
                    augmented_class[class_name] = (
                        class_samples - class_count[class_name]
                    )
    return augmented_class


def do_augmentation(
    samples: PathLike,
    class_augmentation: dict[int, int],
    sample_augmentation_params: AugmentationParams,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Perform data augmentation on the input samples.

    Data augmentation is performed using OTB SampleAugmentation application

    Parameters
    ----------
    samples : PathLike
        Path to the input samples.
    class_augmentation : dict[int, int]
        Dictionary specifying the number of samples to generate for each class
        through data augmentation.
    sample_augmentation_params : AugmentationParams
        Parameters for data augmentation.
    working_directory : str | None
        Path to the working directory where augmented samples will be stored.
        If None, augmentation is performed in place, by default None
    logger : logging.Logger, optional
        Logger object for logging messages, by default LOGGER
    """
    ext = Path(samples).suffix

    samples_dir_o = Path(samples).parent
    samples_name = Path(samples).name
    samples_dir = str(samples_dir_o)
    if working_directory:
        samples_dir = working_directory
        shutil.copy(samples, samples_dir)
    samples = str(Path(samples_dir) / samples_name)

    augmented_files = generate_augmented_samples(
        class_augmentation,
        sample_augmentation_params,
        samples,
        samples_dir,
        logger,
    )

    samples_name_path = Path(samples_name)
    output_vector = str(Path(samples_dir) / f"{samples_name_path.stem}_augmented{ext}")

    vf.merge_db(
        "_".join([Path(samples_name).stem, "augmented"]),
        str(samples_dir),
        [samples] + augmented_files,
    )

    logger.info(f"Every data augmentation done in {samples}")
    shutil.move(output_vector, Path(samples_dir_o) / samples_name)

    # clean-up
    for augmented_file in augmented_files:
        Path(augmented_file).unlink()


def generate_augmented_samples(
    class_augmentation: dict[int, int],
    sample_augmentation_params: AugmentationParams,
    samples: str,
    samples_dir: str,
    logger: logging.Logger = LOGGER,
) -> list[str]:
    """
    Generate augmented samples based on the input samples and augmentation parameters.

    Parameters
    ----------
    class_augmentation : dict[int, int]
        Dictionary specifying the number of samples to generate for each class
        through data augmentation.
    sample_augmentation_params : AugmentationParams
        Parameters for data augmentation.
    samples : str
        Path to the input samples.
    samples_dir : str
        Path to the directory where augmented samples will be stored.
    logger : logging.Logger, optional
        Logger object for logging messages, by default LOGGER

    Returns
    -------
    list[str]
        List of paths to the generated augmented samples.
    """
    ext = Path(samples).suffix
    samples_name = Path(samples).name
    excluded_fields = sample_augmentation_params.excluded_fields
    if excluded_fields is None:
        excluded_fields = []
    augmented_files = []
    for class_name, class_samples_augmentation in list(class_augmentation.items()):
        logger.info(
            f"{class_samples_augmentation} samples of class {class_name} will "
            f"be generated by data augmentation ({sample_augmentation_params.strategy} "
            f"method) in {samples}"
        )
        sample_name_augmented = "_".join(
            [
                Path(samples_name).stem,
                f"aug_class_{class_name}{ext}",
            ]
        )
        output_sample_augmented = str(Path(samples_dir) / sample_name_augmented)
        parameters = {
            "in": samples,
            "field": sample_augmentation_params.field,
            "out": output_sample_augmented,
            "label": class_name,
            "strategy": sample_augmentation_params.strategy,
            "samples": class_samples_augmentation,
        }
        if sample_augmentation_params.random_seed:
            parameters["seed"] = int(sample_augmentation_params.random_seed)
        if excluded_fields:
            parameters["exclude"] = excluded_fields
        if sample_augmentation_params.strategy.lower() == "jitter":
            parameters[
                "strategy.jitter.stdfactor"
            ] = sample_augmentation_params.jitter_std_factor
        elif sample_augmentation_params.strategy.lower() == "smote":
            parameters[
                "strategy.smote.neighbors"
            ] = sample_augmentation_params.smote_neighbors

        augmentation_application, _ = create_application(
            AvailableOTBApp.SAMPLE_AUGMENTATION, parameters
        )
        augmentation_application.ExecuteAndWriteOutput()
        logger.debug(
            f"{class_samples_augmentation} samples of class "
            f"{class_name} were added in {samples}"
        )
        augmented_files.append(output_sample_augmented)
    return augmented_files


def get_fields_type(vector_file: PathLike) -> dict[str, str]:
    """Get field's type.

    Parameters
    ----------
    vector_file : string
        path to a shape file
    Return
    ------
    dict
        dictionary with field's name as key and type as value

    Example
    -------
    >>> print get_fields_type("/path/to/MyVector.shp")
        {'CODE': 'integer64', 'id': 'integer64', 'Area': 'integer64'}
    """
    data_source = ogr.Open(vector_file)
    da_layer = data_source.GetLayer(0)
    layer_definition = da_layer.GetLayerDefn()
    field_dict = {}
    for n_field in range(layer_definition.GetFieldCount()):
        field_name = layer_definition.GetFieldDefn(n_field).GetName()
        field_type_code = layer_definition.GetFieldDefn(n_field).GetType()
        field_type = layer_definition.GetFieldDefn(n_field).GetFieldTypeName(
            field_type_code
        )
        field_dict[field_name] = field_type.lower()
    return field_dict


I2_CONST = i2_constants.Iota2Constants()


def data_augmentation_synthetic_dl(
    database: DBInfo,
    ground_truth: PathLike,
    strategies: dict,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Compute how many samples should be added in the sample set and launch data augmentation method.

    This function computes how many samples should be added to the sample set based on
    the provided strategies for data augmentation. It then launches the data augmentation
    method using the specified strategies.

    Parameters
    ----------
    database : DBInfo
        Information about the database.
    ground_truth : PathLike
        Path to the original ground truth vector file.
        Usefull to know origin field type and ignore them as
        they are no features.
    strategies:
        Dictionary containing various strategies for data augmentation.
    working_directory : str | None
        Path to a working directory, by default None.
    logger : logging.Logger, optional
        Logger object for logging messages, by default LOGGER.
    """
    samples = str(database.db_file)
    data_field = database.data_field
    class_field = strategies["class_field"]
    data_augmentation_synthetic(
        database, ground_truth, strategies, working_directory, logger
    )
    build_raw_db(
        str(Path(samples).with_suffix(".nc")),
        [samples],
        [class_field, data_field, "originfid"],
        field_filter=None,
    )
    Path(samples).unlink()


def data_augmentation_synthetic(
    database: DBInfo,
    ground_truth: PathLike,
    strategies: dict,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Compute how many samples should be added in the sample set and launch data augmentation method.

    This function computes how many samples should be added to the sample set based on
    the provided strategies for data augmentation. It then launches the data augmentation
    method using the specified strategies.

    Parameters
    ----------
    database : DBInfo
        Information about the database.
    ground_truth : PathLike
        Path to the original ground truth vector file.
        Useful to know origin field type and ignore them as
        they are no features.
    strategies:
        Dictionary containing various strategies for data augmentation.
    working_directory : str | None
        Path to a working directory, by default None.
    logger : logging.Logger, optional
        Logger object for logging messages, by default LOGGER.
    """
    random_seed = strategies.pop("random_seed", None)
    samples = database.db_file
    assert isinstance(samples, str)
    class_field = strategies.pop("class_field")
    driver = vf.get_driver(samples)
    if (
        get_region_from_sample_name(samples) in strategies["target_models"]
        or "all" in strategies["target_models"]
    ):
        class_count = Counter(
            vf.get_field_element(samples, elem_type=int, field=class_field, mode="all")
        )
        class_augmentation = samples_augmentation_counter(
            class_count,
            mode=strategies["samples.strategy"],
            min_number=strategies.get("samples.strategy.minNumber", None),
            by_class=strategies.get("samples.strategy.byClass", None),
            labels_table=database.mapping_table,
        )

        fields_types = get_fields_type(ground_truth)

        excluded_fields_origin = [
            field_name.lower()
            for field_name, field_type in list(fields_types.items())
            if "int" in field_type or "float" in field_type
        ]
        samples_fields = vf.get_all_fields_in_shape(samples, driver_name=driver)
        excluded_fields = list(set(excluded_fields_origin).intersection(samples_fields))
        excluded_fields.append("originfid")

        do_augmentation(
            samples,
            class_augmentation,
            sample_augmentation_params=AugmentationParams(
                strategies["strategy"],
                class_field,
                excluded_fields,
                strategies.get("strategy.jitter.stdfactor", None),
                strategies.get("strategy.smote.neighbors", None),
                random_seed,
            ),
            working_directory=working_directory,
            logger=logger,
        )


@dataclass
class ExtractionRule:
    """
    Represents how much samples must be extracted.

    Attributes
    ----------
    data_field:
        The name of the field of interest into the database.
    class_name:
        value of interest.
    quantity:
        quantity to extract.
    """

    db_field: str
    class_name: str | int
    quantity: int


def do_copy(
    source_samples: PathLike,
    destination_samples: PathLike,
    extraction_rule: ExtractionRule,
    prim_key: str = "ogc_fid",
    source_samples_table_name: str = "output",
) -> None:
    """
    Copy samples from one subset to another.

    Parameters
    ----------
    source_samples : PathLike
        Path to the sample-set to extract features.
    destination_samples : PathLike
        Path to the sample-set to fill-up features.
    extraction_rule : ExtractionRule
        Object specifying the extraction rule for copying samples.
    prim_key : str, optional
        OGR primary key, by default "ogc_fid"
    source_samples_table_name : str, optional
        Input vector file table's name (in case of SQLite file), by default "output"
    """
    ext = Path(source_samples).suffix
    if ext == ".sqlite":
        do_copy_sqlite(
            source_samples,
            destination_samples,
            extraction_rule,
            prim_key,
            source_samples_table_name,
        )
    else:
        do_copy_csv(
            source_samples,
            destination_samples,
            extraction_rule,
            prim_key,
        )


def do_copy_sqlite(
    source_samples: PathLike,
    destination_samples: PathLike,
    extraction_rule: ExtractionRule,
    prim_key: str = "ogc_fid",
    source_samples_table_name: str = "output",
) -> None:
    """
    Copy samples from one subset to another using a SQLite query.

    Parameters
    ----------
    source_samples : PathLike
        Path to the sample-set to extract features.
    destination_samples : PathLike
        Path to the sample-set to fill-up features.
    extraction_rule : ExtractionRule
        Object specifying the extraction rule for copying samples.
    prim_key : str, optional
        OGR primary key, by default "ogc_fid"
    source_samples_table_name : str, optional
        Input vector file table's name, by default "output"
    """
    class_name = extraction_rule.class_name
    data_field = extraction_rule.db_field
    extract_quantity = extraction_rule.quantity
    # open destination sqlite and put it in dataframe
    conn = sqlite3.connect(destination_samples)
    df_destination = pd.read_sql_query(
        f"SELECT * FROM {source_samples_table_name}", conn
    )
    # Same for source
    conx = sqlite3.connect(source_samples)
    df_source = pd.read_sql_query(
        (
            f"SELECT * FROM {source_samples_table_name} WHERE "
            f"{data_field.lower()}={class_name} ORDER BY RANDOM() LIMIT {extract_quantity}"
        ),
        conx,
    )

    df_final = pd.concat([df_destination, df_source], axis="index", ignore_index=True)
    # repack
    if prim_key in df_final:
        df_final[prim_key] = list(range(1, len(df_final) + 1))
    # Replace and update the destination sqlite file
    df_final.to_sql(source_samples_table_name, conn, if_exists="replace", index=False)


def do_copy_csv(
    source_samples: PathLike,
    destination_samples: PathLike,
    extraction_rule: ExtractionRule,
    prim_key: str = "ogc_fid",
) -> None:
    """
    Copy a specific quantity of samples from one subset to another using pandas.

    Parameters
    ----------
    source_samples : PathLike
        Path to the sample-set to extract features.
    destination_samples : PathLike
        Path to the sample-set to fill-up features.
    extraction_rule : ExtractionRule
        Object specifying the extraction rule for copying samples.
    prim_key : str, optional
        OGR primary key, by default "ogc_fid"
    """
    class_name = extraction_rule.class_name
    data_field = extraction_rule.db_field
    extract_quantity = extraction_rule.quantity

    # open destination csv and put it in dataframe
    df_destination = pd.read_csv(destination_samples)
    # Same for source
    df_source = pd.read_csv(source_samples)
    df_source = df_source[df_source[data_field.lower()] == int(class_name)].sample(
        extract_quantity
    )
    df_final = pd.concat([df_destination, df_source], axis="index", ignore_index=True)
    # repack
    if prim_key in df_final:
        df_final[prim_key] = list(range(1, len(df_final) + 1))
    # Replace and update the destination csv file
    df_final.to_csv(destination_samples, index=False)


def copy_samples(source_samples: ListPathLike, destination_dir: Path) -> list[Path]:
    """
    Copy samples to a destination directory.

    Parameters
    ----------
    source_samples:
        List of paths to the source samples.
    destination_dir:
        Path to the destination directory.

    Returns
    -------
    list[Path]
        list of paths to the copied samples in the destination directory.
    """
    copied_samples = []
    for source_sample in source_samples:
        copied_sample = destination_dir / Path(source_sample).name
        shutil.copy(source_sample, destination_dir)
        copied_samples.append(copied_sample)
    return copied_samples


def update_extraction_rules(
    extraction_rules: list[TransfertQuantity], labels_table: dict
) -> list[TransfertQuantity]:
    """
    Update extraction rules with label information.

    Parameters
    ----------
    extraction_rules:
        List of extraction rules.
    labels_table
        Mapping between user label (in csv) and labels in database.

    Returns
    -------
    list[TransfertQuantity]
        Updated extraction rules.
    """
    updated_rules = []
    for rule in extraction_rules:
        updated_rules.append(
            TransfertQuantity(
                rule.src_model,
                rule.dst_model,
                labels_table[int(rule.class_name)],
                str(rule.quantity),
            )
        )
    return updated_rules


def process_extraction_rule(
    transfert_quantity: TransfertQuantity,
    samples_set: list[str],
    data_field: str,
    logger: logging.Logger,
) -> None:
    """
    Process an extraction rule and perform copying operations.

    This function processes an extraction rule, extracts the required number of samples
    from the source model, and copies them to the destination model. It also handles cases
    where the specified quantity is -1 (indicating all available samples of the specified class),
    and performs the copying operation using the do_copy function.

    Parameters
    ----------
    transfert_quantity:
        Dataclass containing information about the transfer quantity:
            - src_model : str
                Name of the source model.
            - dst_model : str
                Name of the destination model.
            - class_name : str
                Name of the class to transfer.
            - quantity : str
                Quantity of samples to transfer.
    samples_set:
        List of paths to the samples in the database.
    data_field:
        Name of the data field in vectors.
    logger:
        Logger object for logging messages.
    """
    prim_key = "ogc_fid"
    source_samples_table_name = "output"
    source_samples = get_samples_from_model_name(
        transfert_quantity.src_model, samples_set, logger=logger
    )
    extract_quantity = int(transfert_quantity.quantity)
    dst_samples = get_samples_from_model_name(
        transfert_quantity.dst_model, samples_set, logger=logger
    )
    if not source_samples or not dst_samples:
        return
    if source_samples == dst_samples:
        return
    if int(extract_quantity) == -1:
        extract_quantity = count_class(
            source_samples, data_field, transfert_quantity.class_name, logger=logger
        )
    if extract_quantity == 0:
        return
    extraction_rule = ExtractionRule(
        data_field, transfert_quantity.class_name, extract_quantity
    )
    do_copy(
        source_samples,
        dst_samples,
        extraction_rule,
        prim_key,
        source_samples_table_name,
    )


def data_augmentation_by_copy(
    database: DBInfo,
    csv_path: PathLike,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Use to copy samples between models.

    This function copies samples between models based on the information provided
    in a CSV file. It extracts the necessary information from the database and
    performs the required copying operations.

    Parameters
    ----------
    database : DBInfo
        Information about the database.
    csv_path : PathLike
        Absolute path to a csv file which describe samples movements.
    working_directory : str | None
        Path to a working directory, by default None.
    logger : logging.Logger, optional
        Root logger, by default LOGGER.
    """
    data_field = database.data_field
    labels_table = database.mapping_table
    samples_set = database.db_file
    assert isinstance(samples_set, Iterable)
    if working_directory:
        assert isinstance(samples_set, list)
        samples_set = copy_samples(samples_set, Path(working_directory))
        origin_dir = [Path(sample).parent for sample in samples_set]

    extraction_rules = get_user_samples_management(csv_path)
    if labels_table:
        extraction_rules = update_extraction_rules(extraction_rules, labels_table)

    for extraction_rule in extraction_rules:
        process_extraction_rule(
            extraction_rule, [str(cfile) for cfile in samples_set], data_field, logger
        )

    if working_directory:
        for o_dir, sample_aug in zip(origin_dir, samples_set):
            sample_aug_filename = Path(str(sample_aug)).name
            Path(o_dir / sample_aug_filename).unlink()
            shutil.copy(str(sample_aug), o_dir)
