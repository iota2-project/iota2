#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""The vector Samples Merge module"""
import csv
import logging
import os
import re
import shutil
import sqlite3 as lite
from logging import Logger
from pathlib import Path
from typing import Literal

import pandas as pd
from osgeo import ogr

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fu
from iota2.learning.utils import I2Label, I2TemporalLabel, i2_label_factory
from iota2.typings.i2_types import ListPathLike, MergeDBParameters, PathLike
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.merge_files import merge_db_to_netcdf

I2_CONST = i2_const.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")


def split_vectors_by_regions(path_list: list[str]) -> list[str]:
    """
    split vectors by regions
    IN:
        path_list [list(str)]: list of path
    OUT:
        list[str] : the list of vector by region
    """
    regions_position = 2
    seed_position = 3

    output = []
    seed_vector_ = fu.sort_by_first_elem(
        [(Path(vec).name.split("_")[seed_position], vec) for vec in path_list]
    )
    seed_vector = [seed_vector for seed, seed_vector in seed_vector_]

    for current_seed in seed_vector:
        region_vector = [
            (Path(vec).name.split("_")[regions_position], vec) for vec in current_seed
        ]
        region_vector_sorted_ = fu.sort_by_first_elem(region_vector)
        region_vector_sorted = [
            r_vectors for region, r_vectors in region_vector_sorted_
        ]
        for seed_vect_region in region_vector_sorted:
            output.append(seed_vect_region)
    return output


def tile_vectors_to_models(iota2_learning_samples_dir: str) -> list[str]:
    """
    use to feed vector_samples_merge function

    Parameters
    ----------
    iota2_learning_samples_dir : string
        path to "learningSamples" iota² directory
    Return
    ------
    list
        list of list of vectors to be merged to form a vector by model
    """
    vectors = fu.file_search_and(
        iota2_learning_samples_dir, True, "Samples_learn.sqlite"
    )
    vectors_sar = fu.file_search_and(
        iota2_learning_samples_dir, True, "Samples_SAR_learn.sqlite"
    )

    vect_to_model = split_vectors_by_regions(vectors) + split_vectors_by_regions(
        vectors_sar
    )
    return vect_to_model


def check_duplicates(path_file: str, ext: str, logger: Logger = LOGGER) -> None:
    """
    check_duplicates
    Parameters
    ----------
    path_file : string
        the input database file
    Return
    ------
    None
    """
    file = path_file + ext
    if ext == ".sqlite":
        conn = lite.connect(file)
        cursor = conn.cursor()
        sql_clause = (
            "select * from output where ogc_fid in (select min(ogc_fid)"
            " from output group by GEOMETRY having count(*) >= 2);"
        )
        cursor.execute(sql_clause)

        if results := cursor.fetchall():
            sql_clause = (
                "delete from output where ogc_fid in (select min(ogc_fid)"
                " from output group by GEOMETRY having count(*) >= 2);"
            )
            cursor.execute(sql_clause)
            conn.commit()
            logger.warning(f"{len(results)} were removed in {file}")

    else:
        df_samples = pd.read_csv(file)
        cols = list(df_samples.columns)
        cols.remove("originfid")
        df_samples = df_samples.loc[df_samples[cols].drop_duplicates().index]
        df_samples.to_csv(file, index=False)


def clean_repo(output_path: str, logger: Logger = LOGGER) -> None:
    """
    remove from the directory learningSamples all unnecessary files
    """
    learning_content = [str(p) for p in Path(output_path, "learningSamples").iterdir()]
    for c_content in learning_content:
        c_path = output_path + "/learningSamples/" + c_content
        if Path(c_path).is_dir():
            try:
                shutil.rmtree(c_path)
            except OSError:
                logger.debug(f"{c_path} does not exists")


def is_sar(path: str, sar_pos: int = 5) -> bool:
    """
    Check if the input image is a sar product
    Parameters
    ----------
    path: string
        the input image
    Return
    bool
    ------
    """
    return Path(path).name.split("_")[sar_pos] == "sar"


def sort_s1_fields(s1_fields: list[I2TemporalLabel]) -> list[I2TemporalLabel]:
    """
    Sort Sentinel-1 fields

    Parameters
    ----------
    s1_fields:
        List of fields to sort

    Notes
    -----
    The function returns a list containing:
        - Sorted descending VV fields
        - Sorted descending VH fields
        - Sorted ascending VV fields
        - Sorted ascending VH fields
    """
    s1_sorted_fields = []
    # looking for DES vv fields
    des_vv = [field for field in s1_fields if "desvv" in field.feat_name]
    s1_sorted_fields += sorted(des_vv, key=lambda x: int(x.date))
    # looking for DES vh fields
    des_vh = [field for field in s1_fields if "desvh" in field.feat_name]
    s1_sorted_fields += sorted(des_vh, key=lambda x: int(x.date))
    # looking for ASC vv fields
    asc_vv = [field for field in s1_fields if "ascvv" in field.feat_name]
    s1_sorted_fields += sorted(asc_vv, key=lambda x: int(x.date))
    # looking for ASC vh fields
    asc_vh = [field for field in s1_fields if "ascvh" in field.feat_name]
    s1_sorted_fields += sorted(asc_vh, key=lambda x: int(x.date))
    return s1_sorted_fields


def get_tile_vector_list(
    vector_list: ListPathLike, logger: logging.Logger = LOGGER
) -> list[str]:
    """
    Keep from an extraction files list only one vector by tile

    Parameters
    ----------
    vector_list:
        List of databases
    """
    tiles_list = []
    tile_vector_list: list[str] = []
    for vector in vector_list:
        tile = Path(vector).name.split("_")[0]
        tile_regex = re.compile("T[0-9][0-9][A-Z][A-Z][A-Z]")
        if not re.fullmatch(tile_regex, tile):
            logger.warning(
                f"fields from file {vector} ignored because "
                f"file name does not conform to expected format:"
                f" TILE_region_[1:nregion]_seed[0:nseed-1]_[0:nchunk]_Samples_learn.csv"
            )
        else:
            if tile not in tiles_list:
                tiles_list.append(tile)
                tile_vector_list.append(str(vector))
    return tile_vector_list


def setup_vector_list(
    vector_list: ListPathLike,
    field_filter: str | None,
    logger: logging.Logger,
) -> ListPathLike:
    """
    Prepare and return the list of vectors to process based on the specified field filter.

    Parameters
    ----------
    vector_list : list[str]|list[Path]
        A list of file paths to the vectors to be processed.
    field_filter : str
        The granularity filter applied (e.g., 'tile'). Determines how the vectors are selected.
    logger : logging.Logger
        Logger instance for logging process information.

    Returns
    -------
    list[str] | list[Path]
        A list of vector file paths to be processed.
    """
    if Path(vector_list[0]).suffix == ".csv":
        handle_large_csv(vector_list[0], logger)

    if field_filter == "tile":
        return get_tile_vector_list(vector_list, logger)
    return vector_list


def handle_large_csv(vector_path: PathLike, logger: logging.Logger) -> None:
    """
    Handle CSV files with a large number of fields by adjusting environment variables.

    Parameters
    ----------
    vector_path : PathLike
        The path to the CSV file that needs processing.
    logger : logging.Logger
        Logger instance for logging process information.
    """
    with open(vector_path, encoding="utf-8") as vector_list_file:
        data = list(csv.reader(vector_list_file))

    if len(data[0]) > I2_CONST.sqlite_max_column:
        col_max = len(data[0]) + 10
        logger.info(
            f"number of features exceeds {I2_CONST.sqlite_max_column}, setting "
            f"'OGR_CSV_MAX_FIELD_COUNT' to {col_max}"
        )
        os.environ["OGR_CSV_MAX_FIELD_COUNT"] = str(col_max)


def build_all_fields_list(
    features_fields: dict[str, list[I2Label | I2TemporalLabel]],
    masks: dict[str, list[I2Label | I2TemporalLabel]],
    no_features_fields: list,
) -> list[str]:
    """
    Build the complete list of fields to be included in the final output database.

    Parameters
    ----------
    features_fields : dict[str, list[I2Label | I2TemporalLabel]]
        Dictionary where keys are sensor names and values are lists of feature labels (fields).
    masks : dict[str, list[I2Label | I2TemporalLabel]]
        Dictionary where keys are sensor names and values are lists of mask labels (fields).
    no_features_fields : list[str]
        A list of field names that are not related to features.

    Returns
    -------
    list[str]
        A sorted list of all fields to be included in the final database, converted to strings.
    """
    all_fields = no_features_fields

    for sensor_name, fields in features_fields.items():
        if sensor_name == "sentinel1":
            all_fields += sort_s1_fields(fields)
        else:
            all_fields += (
                sorted(fields, key=lambda x: int(x.date))
                if isinstance(fields[0], I2TemporalLabel)
                else fields
            )

    for sensor_name, fields in masks.items():
        all_fields += sorted(fields, key=lambda x: int(x.date))

    return [str(field) for field in all_fields]


def process_vector(
    vector: str,
    features_fields: dict[str, list[I2Label | I2TemporalLabel]],
    masks: dict[str, list[I2Label | I2TemporalLabel]],
    no_features_fields: list,
    expected_no_features_cols: list[str],
) -> None:
    """
    Process a single vector, extracting fields and classifying them as features, masks,
    or non-features.

    Parameters
    ----------
    vector : str
        The path to the vector file to process.
    features_fields : dict[str, list[I2Label | I2TemporalLabel]]
        Dictionary for storing feature fields for each sensor.
    masks : dict[str, list[I2Label | I2TemporalLabel]]
        Dictionary for storing mask fields for each sensor.
    no_features_fields : list[str]
        List for storing non-feature fields.
    expected_no_features_cols : list[str]
        List of field names that are expected to not be features.
    """
    driver = vf.get_driver(vector)
    ds = ogr.GetDriverByName(driver).Open(vector, 0)
    lyr = ds.GetLayer()
    lyr_dfn = lyr.GetLayerDefn()

    for field_num in range(lyr_dfn.GetFieldCount()):
        field_name = lyr_dfn.GetFieldDefn(field_num).GetName()
        process_field(
            field_name,
            features_fields,
            masks,
            no_features_fields,
            expected_no_features_cols,
        )


def process_field(
    field_name: str,
    features_fields: dict[str, list[I2Label | I2TemporalLabel]],
    masks: dict[str, list[I2Label | I2TemporalLabel]],
    no_features_fields: list,
    expected_no_features_cols: list[str],
) -> None:
    """
    Process a field from the vector, classifying it as a feature, mask, or non-feature field.

    Parameters
    ----------
    field_name : str
        The name of the field being processed.
    features_fields : dict[str, list[I2Label | I2TemporalLabel]]
        Dictionary for storing feature fields for each sensor.
    masks : dict[str, list[I2Label | I2TemporalLabel]]
        Dictionary for storing mask fields for each sensor.
    no_features_fields : list[str]
        List for storing non-feature fields.
    expected_no_features_cols : list[str]
        List of field names expected to not be features.
    """
    try:
        i2_label = i2_label_factory(field_name)
    except ValueError:
        if (
            field_name not in no_features_fields
            and field_name in expected_no_features_cols
        ):
            no_features_fields.append(field_name)
    else:
        add_field_to_features_or_masks(i2_label, features_fields, masks)


def add_field_to_features_or_masks(
    i2_label: I2Label | I2TemporalLabel,
    features_fields: dict[str, list[I2Label | I2TemporalLabel]],
    masks: dict[str, list[I2Label | I2TemporalLabel]],
) -> None:
    """
    Add a field to the appropriate feature or mask dictionary based on its label.

    Parameters
    ----------
    i2_label : I2Label | I2TemporalLabel
        The label object representing the field being processed.
    features_fields : dict[str, list[I2Label | I2TemporalLabel]]
        Dictionary for storing feature fields for each sensor.
    masks : dict[str, list[I2Label | I2TemporalLabel]]
        Dictionary for storing mask fields for each sensor.
    """
    sensor_name = i2_label.sensor_name
    feat_name = i2_label.feat_name

    if sensor_name not in features_fields:
        features_fields[sensor_name] = []
        masks[sensor_name] = []

    if "mask" in feat_name:
        if i2_label not in masks[sensor_name]:
            masks[sensor_name].append(i2_label)
    else:
        if i2_label not in features_fields[sensor_name]:
            features_fields[sensor_name].append(i2_label)


def build_raw_db(
    output_dummy: str,
    vector_list: ListPathLike,
    expected_no_features_cols: list[str],
    field_filter: str | None = "tile",
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Create a raw database using a list of smaller databases

    Parameters
    ----------
    output_dummy:
        Output hdf database
    vector_list:
        List of databases to merge
    expected_no_features_cols:
        List of non-feature column names
    field_filter:
        Granularity applied to get all the fields
    logger:
        Logger
    """
    no_features_fields: list[I2Label | I2TemporalLabel] = []
    features_fields: dict[str, list[I2Label | I2TemporalLabel]] = {}
    masks: dict[str, list[I2Label | I2TemporalLabel]] = {}

    vector_to_parse_list = setup_vector_list(vector_list, field_filter, logger)

    for vector in vector_to_parse_list:
        process_vector(
            str(vector),
            features_fields,
            masks,
            no_features_fields,
            expected_no_features_cols,
        )

    all_fields_list = build_all_fields_list(features_fields, masks, no_features_fields)

    merge_db_to_netcdf(
        [str(vector_file) for vector_file in vector_list],
        output_dummy,
        MergeDBParameters(
            fid_col="fid",
            cols_order=all_fields_list,
            nans_rules={"(mask)": I2_CONST.i2_missing_dates_no_data_mask},
        ),
        logger=logger,
    )


def vector_samples_merge(
    vector_list: list[str],
    output_path: str,
    user_label_field: str,
    deep_learning_db: bool = False,
    data_aug: bool = False,
    mode: Literal["classif", "regression"] = "classif",
    logger: Logger = LOGGER,
) -> None:
    """

    Parameters
    ----------
    vector_list : List[string]
    output_path : string
    Return
    ------

    """
    regions_position = 2
    seed_position = 3

    clean_repo(output_path, logger=logger)

    current_model = Path(vector_list[0]).name.split("_")[regions_position]
    seed = Path(vector_list[0]).name.split("_")[seed_position].replace("seed", "")
    ext = Path(vector_list[0]).suffix

    shape_out_name = f"Samples_region_{current_model}_seed{seed}_learn"

    if is_sar(vector_list[0]):
        shape_out_name = shape_out_name + "_SAR"

    logger.info(f"Vectors to merge in {shape_out_name}")
    logger.info("\n".join(vector_list))

    vector_list = [str(path) for path in map(Path, vector_list) if path.exists()]

    if (not deep_learning_db) or data_aug:
        vf.merge_db(
            shape_out_name, str(Path(output_path) / "learningSamples"), vector_list
        )

        check_duplicates(
            str(Path(output_path, "learningSamples", shape_out_name)),
            ext,
            logger=logger,
        )

    if deep_learning_db and (not data_aug):

        output_hdf_dir = str(Path(output_path) / "learningSamples")
        output_hdf = str(
            Path(output_hdf_dir) / f"{shape_out_name}.{I2_CONST.i2_database_ext}"
        )
        if mode == "classif":
            class_label = I2_CONST.re_encoding_label_name
        else:
            class_label = I2_CONST.fake_class
        build_raw_db(
            output_hdf,
            vector_list,
            [class_label, user_label_field, "originfid"],
        )
