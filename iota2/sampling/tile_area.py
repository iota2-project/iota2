# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to generate region shape"""
import logging
import shutil
from pathlib import Path

from osgeo import ogr

from iota2.common import file_utils as fu
from iota2.common.utils import run
from iota2.typings.i2_types import PathLike
from iota2.vector_tools import merge_files as mf
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def add_field_model(
    shp_in: PathLike, mod_num: int, field_out: str, logger: logging.Logger = LOGGER
) -> None:
    """
    add a field to a shapeFile and for every feature, add an ID
    IN :
        - shp_in : a shapeFile
        - mod_num : the number to associate to features
        - field_out : the new field name
    OUT :
        - an update of the shape file in
    """
    source = ogr.Open(shp_in, 1)
    layer = source.GetLayer()
    new_field = ogr.FieldDefn(field_out, ogr.OFTInteger)
    layer.CreateField(new_field)

    for feat in layer:
        if feat.GetGeometryRef():
            layer.SetFeature(feat)
            feat.SetField(field_out, mod_num)
            layer.SetFeature(feat)
        else:
            logger.debug(f"feature {feat.GetFID()} has not geometry")


def create_model_shape_from_tiles(
    tiles_model: list[str],
    path_tiles: PathLike,
    output_path: PathLike,
    out_shp_name: str,
    field_out: str,
    working_directory: str,
    logger: logging.Logger = LOGGER,
) -> None:
    r"""
    create one shapeFile where all features belong to a model number according to the model
    description

    IN :
        - tiles_model : a list of list which describe which tile belong to which model
            ex : for 3 models
                tile model 1 : D0H0,D0H1
                tile model 2 : D0H2,D0H3
                tile model 3 : D0H4,D0H5,D0H6

                tiles_model = [["D0H0","D0H1"],["D0H2","D0H3"],["D0H4","D0H5","D0H6"]]
        - path_tiles : path to the tile's envelope with priority consideration
            ex : /xx/x/xxx/x
            /!\ the folder which contain the envelopes must contain only the envelopes
        - output_path : path to store the resulting shapeFile
            ex : x/x/x/xxx
        - out_shp_name : the name of the resulting shapeFile
            ex : "model"
        - field_out : the name of the field which will contain the model number
            ex : "Mod"
        - working_directory : path to working directory (not mandatory, due to cluster's
        architecture default = None)

    OUT :
        a shapeFile which contains for all feature the model number which it belongs to
    """
    if working_directory is None:
        path_to_tmp = output_path + "/AllTMP"
    else:
        # HPC case
        path_to_tmp = working_directory
    if not Path(path_to_tmp).exists():
        run("mkdir " + path_to_tmp)

    to_remove = []
    for n_model in tiles_model:
        for n_tile in n_model:
            to_remove.append(
                vf.rename_shapefile(path_tiles, n_tile, "", "", path_to_tmp)
            )

    all_tile_path: list[str] = []
    all_tile_path_er = []

    for n_model in tiles_model:
        for n_tile in n_model:
            try:
                _ = all_tile_path.index(str(path_tiles) + "/" + n_tile + ".shp")
            except ValueError:
                all_tile_path.append(path_to_tmp + "/" + n_tile + ".shp")
                all_tile_path_er.append(path_to_tmp + "/" + n_tile + "_ERODE.shp")

    for model_num, model in enumerate(tiles_model):
        for n_tile in model:
            current_tile = path_to_tmp + "/" + n_tile + ".shp"
            add_field_model(current_tile, model_num + 1, field_out, logger)

    for path in all_tile_path:
        vf.erode_shapefile(path, path.replace(".shp", "_ERODE.shp"), 0.1)

    mf.merge_vectors(out_shp_name, str(output_path), all_tile_path_er)
    remove_temporary_files(path_to_tmp, to_remove, working_directory)


def remove_temporary_files(
    path_to_tmp: str, to_remove: list[Path], working_directory: str
) -> None:
    """remove temporary files"""
    if not working_directory:
        shutil.rmtree(path_to_tmp, ignore_errors=True)
    else:
        for rm_shp in to_remove:
            vf.remove_shape(str(rm_shp))


def generate_region_shape(
    envelope_directory: str,
    output_region_file: str,
    out_field_name: str,
    i2_output_path: str,
    working_directory: str,
    logger: logging.Logger = LOGGER,
) -> None:
    """Generate regions shape.

    Parameters
    ----------
    envelope_directory: str
        directory containing all iota2 tile's envelope
    output_region_file: str
        output file
    out_field_name: str
        output field containing region
    i2_output_path: str
        iota2 output path
    working_directory: str
        path to a working directory
    """
    region = []
    all_tiles = fu.file_search_and(envelope_directory, False, ".shp")
    region.append(all_tiles)

    if not output_region_file:
        output_region_file = str(Path(i2_output_path) / "MyRegion.shp")

    p_f = output_region_file.replace(" ", "").split(
        "/"
    )  # ["ver", "long", "path", "to", "file.shp"]
    out_name = p_f[-1].split(".")[0]

    path_mod = ""
    for dir_depth in range(1, len(p_f) - 1):
        path_mod = path_mod + "/" + p_f[dir_depth]

    create_model_shape_from_tiles(
        region,
        envelope_directory,
        path_mod,
        out_name,
        out_field_name,
        working_directory,
        logger,
    )
