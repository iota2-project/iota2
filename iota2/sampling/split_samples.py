#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to split samples into smaller subsets"""
import logging
import math
import operator
import shutil
import sqlite3 as db
from logging import Logger
from pathlib import Path

import geopandas as gpd
from osgeo import ogr

from iota2.common import file_utils as fut
from iota2.common.file_utils import file_search_and
from iota2.common.utils import run
from iota2.sampling import split_in_subsets as subset
from iota2.sampling.vector_formatting import split_by_sets
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")
LOGGER.addHandler(logging.NullHandler())


def get_ordered_learning_samples(learning_directory: str) -> list[str]:
    """
    scan learning directory and return a list of files ordered considering
    model and seed

    Parameters
    ----------
    learning_directory : str
        path to the learning directory

    Return
    ------
    list
        list of path
    """
    tile_position = 0
    model_position = 2
    seed_position = 3

    learning_files = file_search_and(learning_directory, False, "_Samples_learn.sqlite")
    files_indexed = [
        (
            c_file.split("_")[model_position],
            c_file.split("_")[tile_position],
            int(c_file.split("_")[seed_position].replace("seed", "")),
            str(Path(learning_directory) / f"{c_file}.sqlite"),
        )
        for c_file in learning_files
    ]
    files_indexed_sorted = sorted(files_indexed, key=operator.itemgetter(0, 1, 2))
    return [learning_file for _, _, _, learning_file in files_indexed_sorted]


def split_superpixels_and_reference(
    vector_file: str,
    superpix_column: str = "superpix",
    driver_in: str = "SQLite",
    working_dir: str | None = None,
    logger: logging.Logger = LOGGER,
) -> tuple[str, str]:
    """
    reference feature contains the value 0 in column 'superpix'
    Parameters
    ----------
    vector_file : string
                the input vector file
    superpix_column: string
                the column name for superpixels in vector_file
    driver : string
            the vector_file format
    Return
    ------
    None
    """
    driver = ogr.GetDriverByName(driver_in)
    data_source = driver.Open(vector_file, 0)
    layer = data_source.GetLayer()
    table_name = layer.GetName()
    feat = layer.GetNextFeature()
    geom = feat.GetGeometryRef()
    spatial_ref = geom.GetSpatialReference()
    epsg_code = int(spatial_ref.GetAttrValue("AUTHORITY", 1))
    vectors_dir = Path(vector_file).parent
    vector_name = Path(vector_file).name

    tmp_dir = str(vectors_dir)
    if working_dir:
        tmp_dir = working_dir

    superpix_db = str(Path(tmp_dir) / vector_name.replace("learn.sqlite", "SP.sqlite"))
    ref_db = str(Path(tmp_dir) / vector_name.replace("learn.sqlite", "REF.sqlite"))

    logger.info(
        f"Extract superpixel samples from file {superpix_db} and save it "
        f"in {superpix_db}"
    )
    sql = f"select * from {table_name} where {superpix_column}!={0}"
    cmd = (
        f"ogr2ogr -t_srs EPSG:{epsg_code} -s_srs EPSG:{epsg_code} -nln"
        f' {table_name} -f "{driver_in}" -sql "{sql}" {superpix_db} {vector_file}'
    )
    run(cmd)

    logger.info(
        f"Extract reference samples from file {vector_file} and save it"
        f" in {vector_file}"
    )
    sql = f"select * from {table_name} where {superpix_column}={0}"
    cmd = (
        f"ogr2ogr -t_srs EPSG:{epsg_code} -s_srs EPSG:{epsg_code} "
        f'-nln {table_name} -f "{driver_in}" -sql "{sql}" '
        f"{ref_db} {vector_file}"
    )
    run(cmd)
    shutil.move(ref_db, vector_file)
    if working_dir:
        shutil.copy(superpix_db, vectors_dir)
        Path(superpix_db).unlink()

    return vector_file, str(Path(vectors_dir / Path(superpix_db).name))


# #############################################################################
# This function was replaced by the one below which use geopandas
# This is the solution to remove call from mod_spatialite which raise
# exception with libiconv. More recent versions should solve this issue
# When possible, an eficiency analysis should be done to compare sqlite and
# geopandas approaches.
# def get_regions_area(vectors: List[str], regions: List[str],
#                      formatting_vectors_dir: str, working_directory: [str],
#                      region_field: [str]) -> Dict[str, List[str]]:
#     """
#     usage : get all models polygons area
#     IN
#     vectors [list of strings] : path to vector file
#     regions [list of string] : all possible regions
#     formatting_vectors_dir [string] : path to /iota2/formattingVectors
#     working_directory [string]
#     region_field [string]

#     OUT
#     dico_region_area [dict] : dictionnary containing area by region's key
#     """
#     from iota2.common.utils import run

#     import sqlite3 as db
#     tmp_data = []
#     # init dict
#     dico_region_area = {}
#     dico_region_tile = {}
#     for reg in regions:
#         dico_region_area[reg] = 0.0
#         dico_region_tile[reg] = []

#     for vector in vectors:
#         # move vectors to sqlite (faster format)
#         transform_dir = formatting_vectors_dir
#         if working_directory:
#             transform_dir = working_directory
#         transform_vector_name = os.path.split(vector)[-1].replace(
#             ".shp", ".sqlite")
#         sqlite_vector = os.path.join(transform_dir, transform_vector_name)
#         cmd = "ogr2ogr -f 'SQLite' {} {}".format(sqlite_vector, vector)
#         run(cmd)
#         tmp_data.append(sqlite_vector)
#         region_vector = vf.get_field_element(sqlite_vector,
#                                            driver_name="SQLite",
#                                            field=region_field,
#                                            mode="unique",
#                                            elem_type="str")
#         conn = db.connect(sqlite_vector)
#         conn.enable_load_extension(True)
#         conn.load_extension("mod_spatialite")
#         cursor = conn.cursor()
#         table_name = (transform_vector_name.replace(".sqlite", "")).lower()
#         for current_region in region_vector:
#             sql_clause = (
#                 f"SELECT AREA(GEOMFROMWKB(GEOMETRY)) FROM "
#                 f"{table_name} WHERE {region_field}={current_region}")
#             cursor.execute(sql_clause)
#             res = cursor.fetchall()

#             dico_region_area[current_region] += sum([area[0] for area in res])

#             if vector not in dico_region_tile[current_region]:
#                 dico_region_tile[current_region].append(sqlite_vector)

#         conn = cursor = None

#     return dico_region_area, dico_region_tile, tmp_data


def get_regions_area(
    vectors: list[str],
    regions: list[str],
    formatting_vectors_dir: str,
    region_field: str,
    working_directory: str | None = None,
) -> tuple[dict[str, float], dict[str, list[str]], list[str]]:
    """
    usage : get all models polygons area
    IN
    vectors [list of strings] : path to vector file
    regions [list of string] : all possible regions
    formatting_vectors_dir [string] : path to /iota2/formattingVectors
    working_directory [string]
    region_field [string]

    OUT
    dico_region_area [dict] : dictionnary containing area by region's key
    """
    tmp_data = []
    # init dict
    dico_region_area = {}
    dico_region_tile: dict[str, list[str]] = {}

    for reg in regions:
        dico_region_area[reg] = 0.0
        dico_region_tile[reg] = []

    for vector in vectors:
        # move vectors to sqlite (faster format)
        transform_dir = formatting_vectors_dir
        if working_directory:
            transform_dir = working_directory
        transform_vector_name = Path(vector).name.replace(".shp", ".sqlite")
        sqlite_vector = str(Path(transform_dir) / transform_vector_name)
        cmd = f"ogr2ogr -f 'SQLite' {sqlite_vector} {vector}"
        run(cmd)
        tmp_data.append(sqlite_vector)
        gdf = gpd.GeoDataFrame.from_file(vector)
        region_vector = gdf[region_field].unique()

        for current_region in region_vector:
            df_t = gdf[gdf[region_field] == current_region]
            # NOTE: area is sensitive to projection
            # here there is no interset to know the exact area
            area = sum(df_t["geometry"].area)

            dico_region_area[f"{current_region}"] += area
            if vector not in dico_region_tile[f"{current_region}"]:
                dico_region_tile[f"{current_region}"].append(sqlite_vector)

    return dico_region_area, dico_region_tile, tmp_data


def get_splits_regions(
    areas: dict[str, float], region_threshold: float
) -> dict[str, int]:
    """
    return regions which must be split
    """
    dic = {}  # {'region':Nsplits,..}
    for region, area in list(areas.items()):
        fold = int(math.ceil(area / (region_threshold * 1e6)))
        if fold > 1:
            dic[region] = fold
    return dic


def get_fid_values(
    vector_path: str, data_field: str, region_field: str, region: str, value: int | str
) -> list[str]:
    """
    Parameters
    ----------
    vector_path: string
    data_field: string
    region_field: string
    region: string
    value: int
    Return
    ------
    list[string]
    """
    with db.connect(vector_path) as conn:

        cursor = conn.cursor()
        table_name = Path(vector_path).stem.lower()

        sql_clause = (
            f"SELECT ogc_fid FROM {table_name} WHERE {data_field}={value}"
            f" AND {region_field}='{region}'"
        )
        cursor.execute(sql_clause)
        fids = cursor.fetchall()
    return [fid[0] for fid in fids]


def update_vector(
    vector_path: str,
    region_field: str,
    new_regions_dict: dict[str, list],
    logger: Logger = LOGGER,
) -> None:
    """
    Parameters
    ----------
    vector_path: string
    region_field: string
    new_regions_dict: dict[str,str]
    Return
    ------
    None
    """
    # const
    sqlite3_query_limit = 1000.0
    with db.connect(vector_path) as conn:

        cursor = conn.cursor()
        table_name = Path(vector_path).stem.lower()

        for new_region_name, fids in list(new_regions_dict.items()):
            nb_sub_split_sqlite = int(math.ceil(len(fids) / sqlite3_query_limit))
            sub_fid_sqlite = fut.split_list(fids, nb_sub_split_sqlite)

            sub_fid_clause = []
            for sub_fid in sub_fid_sqlite:
                sub_fid_clause.append(f"(ogc_fid in ({', '.join(map(str, sub_fid))}))")
            fid_clause = " OR ".join(sub_fid_clause)
            sql_clause = (
                f"UPDATE {table_name} SET {region_field}="
                f"'{new_region_name}' WHERE {fid_clause}"
            )
            logger.debug("update fields")
            logger.debug(sql_clause)

            cursor.execute(sql_clause)
            conn.commit()


def split(
    regions_split: dict[str, int],
    regions_tiles: dict[str, list[str]],
    data_field: str,
    region_field: str,
    logger: logging.Logger = LOGGER,
) -> list[str]:
    """
    function dedicated to split to huge regions in sub-regions
    Parameters
    ----------
    regions_split: dict[string, int]
    regions_tiles: dict[str, List[str]]
    Return
    ------
    list(str)
    """
    updated_vectors = []

    for region, fold in list(regions_split.items()):
        vector_paths = regions_tiles[region]
        for vec in vector_paths:
            # init dict new regions
            new_regions_dict: dict[str, list] = {}
            for sub_fold in range(fold):
                # new region's name are define here
                new_regions_dict[f"{region}f{sub_fold + 1}"] = []

            # get possible class
            class_vector = vf.get_field_element(
                vec, elem_type=str, field=data_field, mode="unique"
            )
            dic_class = {}
            # get FID values for all class of current region into
            # the current tile
            assert class_vector
            for c_class in class_vector:
                dic_class[c_class] = get_fid_values(
                    vec, data_field, region_field, region, c_class
                )

            nb_feat = 0
            for _, fid_cl in list(dic_class.items()):
                if fid_cl:
                    fid_folds = fut.split_list(fid_cl, fold)
                    # fill new_regions_dict
                    for n_fold, fid_fold in enumerate(fid_folds):
                        new_regions_dict[f"{region}f{n_fold+1}"] += fid_fold
                nb_feat += len(fid_cl)
            update_vector(vec, region_field, new_regions_dict, logger=logger)
            if vec not in updated_vectors:
                updated_vectors.append(vec)

    return updated_vectors


def transform_to_shape(
    sqlite_vectors: list[str],
    formatting_vectors_dir: str,
    logger: logging.Logger = LOGGER,
) -> list[str]:
    """
    Parameters
    ----------
    sqlite_vectors: list(str)
    formatting_vectors_dir: str
    Return
    ------
    list(str)
    """
    out = []
    for sqlite_vector in sqlite_vectors:
        out_name = Path(sqlite_vector).stem
        out_path = str(Path(formatting_vectors_dir) / f"{out_name}.shp")
        if Path(out_path).exists():
            vf.remove_shape(out_path)
        cmd = f"ogr2ogr -f 'ESRI Shapefile' {out_path} {sqlite_vector}"
        run(cmd, logger=logger)
        out.append(out_path)
    return out


# def update_learningValination_sets(new_regions_shapes, dataAppVal_dir,
def update_learning_validation_sets(
    new_regions_shapes: list[str],
    data_app_val_dir: str,
    data_field: str,
    region_field: str,
    ratio: float,
    seeds: int,
    epsg: str | int,
    random_seed: int,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Parameters
    ----------
    new_regions_shapes: list(string)
    data_app_val_dir: string
    data_field: string
    region_field: string
    ratio: float
    seeds: intersect
    epsg: string
    random_seed: int
    """

    for new_region_shape in new_regions_shapes:
        tile_name = Path(new_region_shape).stem
        vectors_to_rm = fut.file_search_and(data_app_val_dir, True, tile_name)
        for vect in vectors_to_rm:
            Path(vect).unlink()
        # remove seeds fields
        subset.split_in_subsets(
            new_region_shape,
            data_field,
            region_field,
            ratio,
            seeds,
            "ESRI Shapefile",
            random_seed=random_seed,
        )

        split_by_sets(
            new_region_shape,
            seeds,
            data_app_val_dir,
            int(epsg),
            int(epsg),
            tile_name,
            logger=logger,
        )


def split_samples(
    output_path: str,
    data_field: str,
    region_threshold: str | float,
    region_field: str,
    ratio: float,
    random_seed: int,
    runs: int,
    epsg: str | int,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Split samples into subsets, depending on threshold
    """
    # const
    if isinstance(epsg, str):
        epsg = int(epsg.split(":")[-1])
    if isinstance(region_threshold, str):
        region_threshold = float(region_threshold)

    # get All possible regions by parsing shapeFile's name
    regions = get_all_regions(output_path)

    # compute region's area
    areas, regions_tiles, data_to_rm = compute_regions_area(
        output_path, regions, region_field, working_directory
    )

    # get how many sub-regions must be created by too huge regions.
    regions_split = get_splits_regions(areas, region_threshold)

    for region_name, area in list(areas.items()):
        logger.info(f"region : {region_name} , area : {area}")

    updated_vectors = split(
        regions_split, regions_tiles, data_field, region_field, logger=logger
    )

    # transform sqlites to shape file, according to input data format
    new_regions_shapes = convert_to_shape(updated_vectors, output_path, logger)
    for data in data_to_rm:
        Path(data).unlink()

    update_learning_validation_sets(
        new_regions_shapes,
        str(Path(output_path) / "dataAppVal"),
        data_field,
        region_field,
        ratio,
        runs,
        epsg,
        random_seed,
        logger=logger,
    )


def get_all_regions(output_path: str) -> list[str]:
    """Get all possible regions from shapefiles."""
    shapes_region = fut.file_search_and(
        str(Path(output_path) / "shapeRegion"), True, ".shp"
    )
    regions = list({Path(shape).name.split("_")[-2] for shape in shapes_region})
    return regions


def compute_regions_area(
    output_path: str,
    regions: list[str],
    region_field: str,
    working_directory: str | None,
) -> tuple[dict, dict, list[str]]:
    """Compute areas for all regions."""
    formatting_vectors_dir = str(Path(output_path) / "formattingVectors")
    return get_regions_area(
        fut.file_search_and(formatting_vectors_dir, True, ".shp"),
        regions,
        formatting_vectors_dir,
        region_field,
        working_directory,
    )


def convert_to_shape(
    updated_vectors: list, output_path: str, logger: logging.Logger
) -> list:
    """Convert the updated vectors to shapefiles."""
    formatting_vectors_dir = str(Path(output_path) / "formattingVectors")
    return transform_to_shape(updated_vectors, formatting_vectors_dir, logger=logger)
