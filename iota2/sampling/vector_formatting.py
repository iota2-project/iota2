#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""The vector formatting module."""
# import argparse
import logging
import math
import shutil
from collections.abc import Iterable
from dataclasses import dataclass, field
from pathlib import Path
from typing import Literal

import dask.dataframe as dd
import numpy as np
import xmltodict
from osgeo import ogr

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fut
from iota2.common import otb_app_bank as otb
from iota2.common.file_utils import get_raster_resolution
from iota2.common.otb_app_bank import AvailableOTBApp
from iota2.common.utils import run
from iota2.sampling import split_in_subsets as subset
from iota2.typings.i2_types import (
    DBInfo,
    DistanceMapParameters,
    IntersectionDatabaseParameters,
)
from iota2.vector_tools import spatial_operations as intersect
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.add_field import add_field
from iota2.vector_tools.compute_boundary_regions import compute_distance_map
from iota2.vector_tools.vector_functions import (
    get_driver,
    get_geom_column_name,
    get_vector_proj,
    is_readable,
)

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()


def check_validation_db_integrity(
    databases: list[str], logger: logging.Logger = LOGGER
) -> None:
    """Ensure databases integrity."""
    valid_db = [elem for elem in databases if "val.sqlite" in elem]
    driver = ogr.GetDriverByName("SQLite")
    for database in valid_db:
        db_ds = driver.Open(database, 0)
        layer = db_ds.GetLayer()
        if layer is None or layer.GetFeatureCount() == 0:
            # validation on learning polygons
            logger.warning(
                f"{database} is equivalent to "
                f"{database.replace('val.sqlite', 'learn.sqlite')}"
            )
            shutil.copy(database.replace("val.sqlite", "learn.sqlite"), database)


def get_total_number_of_samples(stats_file: str) -> int:
    """Return the total number of samples for xml file.

    Parse statistics file from OTB and return the total number of samples
    to avoid create empty validation files

    Parameters
    ----------
    stats_file:
        the file to be parsed
    """
    with open(stats_file, "rb") as read_file:
        dico = xmltodict.parse(read_file)
        samples_per_class_list = dico["GeneralStatistics"]["Statistic"][0][
            "StatisticMap"
        ]
        if isinstance(samples_per_class_list, list):
            # if there are multiple StatisticMap, this is a list
            values = [int(d["@value"]) for d in samples_per_class_list]
            return np.sum(values)
        if isinstance(samples_per_class_list, dict):
            # in case of regression, it is possible to have only one
            # StatisticMap, in that case, it is a dict
            return int(samples_per_class_list["@value"])
        raise ValueError(f"file {stats_file} content is unexpected")


def split_vector_by_region(
    database: DBInfo,
    output_dir: str,
    sampling_validation: bool,
    runs: int = 1,
    proj_out: str | None = "EPSG:2154",
    sample_selection_dir: str | None = None,
    targeted_chunk: int | None = None,
    table_name: str = "output",
    logger: logging.Logger = LOGGER,
) -> list[str]:
    """Create new files by regions in input vector.

    Return the list of produced vector files.

    Parameters
    ----------
    database:
        input database
    output_dir:
        path to output directory
    sample_selection_dir:
        folder where the sample selection files are stored
    sampling_validation:
        enable the validation sampling
    runs:
        the number of seeds
    proj_out:
        output projection (None in the case of a csv)
    targeted_chunk:
        indicate if the chunk mode is on and then keep the chunk information
    table_name:
        database tablename
    logger:
        logging object
    """
    output_paths = []
    assert isinstance(database.db_file, (str, Path))
    tile = Path(database.db_file).name.split("_")[0]
    ext = Path(Path(database.db_file).name).suffix
    assert database.region_field
    regions = vf.get_field_element(
        str(database.db_file),
        elem_type=str,
        field=database.region_field,
        mode="unique",
        logger=logger,
    )
    if not regions:
        return []
    # split vector
    assert isinstance(database.db_file, str)
    for seed in range(runs):
        fields_to_keep = [
            elem
            for elem in vf.get_all_fields_in_shape(
                database.db_file, get_driver(database.db_file)
            )
            if "seed_" not in elem
        ]
        for region in regions:
            out_vec_name_learn, out_vec_name_val = get_vect_output_name(
                tile,
                region,
                seed,
                "" if targeted_chunk is None else f"{targeted_chunk}_",
            )
            output_vec = str(Path(output_dir) / out_vec_name_learn) + ext
            output_vec_out = output_vec.replace("_tmp", "")
            # split vectors by runs and learning sets
            if get_driver(database.db_file) != "CSV":
                assert proj_out is not None
                split_vector_by_region_sqlite_shp(
                    fields_to_keep,
                    seed,
                    database,
                    "learn",
                    region,
                    output_vec_out,
                    output_vec,
                    table_name,
                    proj_out,
                    logger,
                )
            else:
                split_vector_by_region_csv(
                    fields_to_keep,
                    seed,
                    str(database.db_file),
                    "learn",
                    database.region_field,
                    region,
                    output_vec_out,
                )
            if is_readable(output_vec_out):
                output_paths.append(output_vec_out)
                if get_driver(database.db_file) != "CSV":
                    Path(output_vec).unlink()
            else:
                Path(output_vec).unlink()
                Path(output_vec_out).unlink()
            # split vector by runs and validation sets
            logger.info(f"Sampling Validation: {sampling_validation}")
            if sampling_validation:
                assert sample_selection_dir
                if (
                    get_total_number_of_samples(
                        str(
                            Path(sample_selection_dir)
                            / f"{tile}_region_{region}_seed_{seed}_stats.xml",
                        )
                    )
                    > 0
                ):
                    # ensure that stats.xml is not empty
                    output_vec = str(Path(output_dir) / out_vec_name_val) + ext
                    output_vec_out = output_vec.replace("_tmp", "")
                    if get_driver(database.db_file) != "CSV":
                        assert proj_out is not None
                        split_vector_by_region_sqlite_shp(
                            fields_to_keep,
                            seed,
                            database,
                            "validation",
                            region,
                            output_vec_out,
                            output_vec,
                            table_name,
                            proj_out,
                            logger,
                        )
                        Path(output_vec).unlink()
                    else:
                        split_vector_by_region_csv(
                            fields_to_keep,
                            seed,
                            str(database.db_file),
                            "validation",
                            database.region_field,
                            region,
                            output_vec_out,
                        )
                    output_paths.append(output_vec_out)
    return output_paths


def get_vect_output_name(
    tile: str,
    region: str,
    seed: int,
    targeted_chunk: str | None,
) -> tuple[str, str]:
    """Generates path for output vectors of samples at seed, tile, regional and chunk scale.
    (useful) for function above.

    Return tuple of path (str)

    Parameters
    ----------
    tile:
        targeted tile
    region:
        targeted region
    seed:
        targeted seed
    targeted_chunk:
        indicate if the chunk mode is on and then keep the chunk information
    mode:
        define if we split sar sensor to the other
    """
    out_vec_name_learn = (
        f"{tile}_region_{region}_seed{seed}_" f"{targeted_chunk}Samples_learn_tmp"
    )
    out_vec_name_val = (
        f"{tile}_region_{region}_seed{seed}_" f"{targeted_chunk}Samples_val_tmp"
    )
    return out_vec_name_learn, out_vec_name_val


def split_vector_by_region_csv(
    fields_to_keep: list[str],
    seed: int,
    in_vect: str,
    flag: str,
    region_field: str,
    region: str,
    output_vec_out: str,
) -> None:
    """Create a new file containing the samples of a unique region from a input CSV file.

    Parameters
    ----------
    fields_to_keep:
        fields to keep in the output file
    seed:
        targeted seed
    in_vect:
        input vector path to CSV file
    flag:
        define if it's validation or learning samples
    region_field:
        field in in_vect describing regions
    region:
        targeted region
    output_vec_out:
        output vector path
    """
    fields_to_keep.append(f"seed_{seed}")
    df = dd.read_csv(in_vect, usecols=fields_to_keep)
    df = df[(df[f"seed_{seed}"] == f"{flag}") & (df[f"{region_field}"] == int(region))]
    df = df.drop(columns=f"seed_{seed}")
    df.to_csv(output_vec_out, index=False, single_file=True)


def split_vector_by_region_sqlite_shp(
    fields_to_keep: list[str],
    seed: int,
    database: DBInfo,
    flag: str,
    region: str,
    output_vec_out: str,
    output_vec: str,
    table_name: str,
    proj_out: str,
    logger: logging.Logger,
) -> None:
    """Create a new file containing the samples of a unique region from a input sqlite file.

    Parameters
    ----------
    fields_to_keep:
        fields to keep in the output file
    seed:
        targeted seed
    database:
        input database
    flag:
        define if it's validation or learning samples
    region:
        targeted region
    output_vec_out:
        output vector path
    output_vec:
        tmp output vector path
    proj_out:
        output projection
    table_name:
        database tablename
    """
    assert isinstance(database.db_file, (str, Path))
    driver = get_driver(str(database.db_file))
    proj_in = f"EPSG:{get_vector_proj(str(database.db_file), driver)}"
    vec_name = Path(database.db_file).name
    table = vec_name.split(".")[0]
    if driver != "ESRI shapefile":
        table = "output"

    fields_to_keep_str = ",".join(fields_to_keep)
    seed_clause_learn = f"seed_{seed}='{flag}'"
    region_clause = f"{database.region_field}='{region}'"
    sql_cmd_learn = (
        f"select * FROM {table} WHERE {seed_clause_learn}" f" AND {region_clause}"
    )
    run(
        (
            f"ogr2ogr -t_srs {proj_out} -s_srs {proj_in} -nln {table}"
            f' -f "{driver}" -sql "{sql_cmd_learn}" {output_vec} '
            f"{database.db_file}"
        ),
        logger=logger,
    )

    # drop useless column
    sql_clause = f"select GEOMETRY,{fields_to_keep_str} from {table_name}"
    run(
        (
            f"ogr2ogr -s_srs {proj_in} -t_srs {proj_out} -dialect "
            f"'SQLite' -f 'SQLite' -nln {table_name} -sql '{sql_clause}' "
            f"{output_vec_out} {output_vec}"
        ),
        logger=logger,
    )


def extract_regions(tile_region: str, region_field: str) -> list[str]:
    """Extract and transform regions from the tile_region file."""
    all_regions_tmp = vf.get_field_element(
        tile_region, elem_type=str, field=region_field.lower(), mode="unique"
    )
    assert all_regions_tmp
    return [region.split("f")[0] for region in all_regions_tmp]


def rasterize_tile_boundaries(
    tile_boundaries_file: str, img_ref: str, logger: logging.Logger
) -> str:
    """Rasterize the tile boundaries into a raster file."""
    if x_res_y_res := get_raster_resolution(img_ref):
        x_res, y_res = x_res_y_res
        tile_boundaries_file_raster = tile_boundaries_file.replace(".shp", ".tif")
        run(
            f"gdal_rasterize -ot Byte -tr {x_res} {abs(y_res)} -burn 1 -at"
            f" {tile_boundaries_file} {tile_boundaries_file_raster}",
            logger=logger,
        )
        return tile_boundaries_file_raster
    raise FileNotFoundError(f"{img_ref} can't be read")


def superimpose_raster(img_ref: str, tile_boundaries_file_raster: str) -> None:
    """Superimpose the raster with the reference image."""
    super_imp, _ = otb.create_application(
        AvailableOTBApp.SUPERIMPOSE,
        {
            "inr": img_ref,
            "inm": tile_boundaries_file_raster,
            "out": tile_boundaries_file_raster,
            "interpolator": "nn",
        },
    )
    super_imp.ExecuteAndWriteOutput()


def create_tile_region_masks(
    database: DBInfo,
    tile_name: str,
    output_directory: str,
    origin_name: str,
    img_ref: str,
    i2_running_dir: str,
    classification_mode: Literal["separate", "fusion"],
    tile_boundaries_file: str,
    boundary_fusion_enabled: bool = False,
    logger: logging.Logger = LOGGER,
) -> None:
    """Produce mask of regions for classification purpose.

    Parameters
    ----------
    database:
        contain the path to a SQLite file containing polygons. Each feature is a region
    tile_name:
        current tile name
    output_directory:
        directory to save masks
    origin_name:
        region's field vector file name
    img_ref:
        path to a tile reference image
    i2_running_dir:
        iota2 output directory
    classification_mode:
        "separate" or "fusion"
    tile_boundaries_file:
         folder to envelope path
    boundary_fusion_enabled:
         enable the boundary fusion mode
    """
    assert database.region_field
    all_regions = extract_regions(str(database.db_file), database.region_field)
    tile_boundaries_file_raster = rasterize_tile_boundaries(
        tile_boundaries_file, img_ref, logger
    )
    # in order to be aligned with the ref image
    superimpose_raster(img_ref, tile_boundaries_file_raster)
    if res := get_raster_resolution(img_ref):
        x_res = res[0]
    else:
        raise FileNotFoundError(f"{img_ref} can't be read")
    driver = ogr.GetDriverByName("ESRI Shapefile")
    for region in all_regions:
        output_path, output_path_buff = create_region_shapefile(
            database, logger, origin_name, output_directory, region, tile_name, x_res
        )
        tile_region_raster = rasterize_region(img_ref, output_path_buff)

        clip_tile_region(tile_boundaries_file_raster, tile_region_raster)

        # generate classification mask
        generate_classification_mask(
            classification_mode,
            database,
            i2_running_dir,
            img_ref,
            output_path_buff,
            region,
            tile_boundaries_file_raster,
            tile_name,
            tile_region_raster,
        )
        vectorize_tile_region(
            tile_region_raster,
            database,
            region,
            output_path,
            output_path_buff,
            driver,
            logger,
        )
    if not boundary_fusion_enabled:
        # if boundary fusion enabled this file is used as mask
        # at the end of the workflow
        Path(tile_boundaries_file_raster).unlink()


def vectorize_tile_region(
    tile_region_raster: str,
    database: DBInfo,
    region: str,
    output_path: str,
    output_path_buff: str,
    driver: ogr.Driver,
    logger: logging.Logger,
) -> None:
    """
    Convert raster to shapefile and add a region field

    Parameters
    ----------
    tile_region_raster: str
        Path to the raster file.
    database: DBInfo
        Database object with region field information.
    region: str
        Region to add to the shapefile.
    output_path: str
        Path to the output file to be deleted.
    output_path_buff: str
        Path to the buffered output file to be deleted.
    driver: org.Driver
        OGR driver used to delete data sources.
    logger: Logger
        Logger for logging the command execution.

    Returns
    -------
    None
    """
    tile_region_db = tile_region_raster.replace(".tif", ".shp")
    tile_region_db_name = Path(tile_region_db).name.split(".")[0]
    run(
        f"gdal_polygonize.py {tile_region_raster}"
        f" {tile_region_db} {tile_region_db_name}",
        logger=logger,
    )
    region_field = database.region_field
    assert region_field
    add_field(tile_region_db, region_field, region, str)
    driver.DeleteDataSource(output_path)
    driver.DeleteDataSource(output_path_buff)


def generate_classification_mask(
    classification_mode: Literal["separate", "fusion"],
    database: DBInfo,
    i2_running_dir: str,
    img_ref: str,
    output_path_buff: str,
    region: str,
    tile_boundaries_file_raster: str,
    tile_name: str,
    tile_region_raster: str,
) -> None:
    """
    Generate the classification mask for a specific region within a tile.

    Parameters
    ----------
    classification_mode : Literal["separate", "fusion"]
        Defines how the classification mask will be created: "separate" or "fusion".
    database : DBInfo
        Contains path to the database and region field information.
    i2_running_dir : str
        Directory where the classification masks will be saved.
    img_ref : str
        Path to the reference image.
    output_path_buff : str
        Path to the buffered shapefile of the region.
    region : str
        The name of the region being processed.
    tile_boundaries_file_raster : str
        Path to the rasterized tile boundaries.
    tile_name : str
        The name of the current tile.
    tile_region_raster : str
        Path to the rasterized region file.
    """
    classification_mask_dir = str(Path(i2_running_dir) / "classif" / "MASK")
    classification_mask_name = f"MASK_region_{region}_{tile_name}.tif"
    classification_mask_file = str(
        Path(classification_mask_dir) / classification_mask_name
    )
    fut.Path(classification_mask_dir).mkdir(parents=True, exist_ok=True)
    if classification_mode == "separate":
        classif_mask_app, _ = otb.create_application(
            AvailableOTBApp.RASTERIZATION,
            {
                "in": output_path_buff,
                "out": classification_mask_file,
                "im": img_ref,
                "mode": "attribute",
                "mode.attribute.field": database.region_field,
            },
        )
        classif_mask_app.ExecuteAndWriteOutput()
        clip, _ = otb.create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": [classification_mask_file, tile_boundaries_file_raster],
                "exp": "im1b1*im2b1",
                "out": classification_mask_file,
            },
        )
        clip.ExecuteAndWriteOutput()
    else:
        shutil.copy(tile_region_raster, classification_mask_file)


def clip_tile_region(tile_boundaries_file_raster: str, tile_region_raster: str) -> None:
    """
    Clip the rasterized region using the boundaries of the tile.

    Parameters
    ----------
    tile_boundaries_file_raster : str
        Path to the rasterized boundaries of the tile.
    tile_region_raster : str
        Path to the rasterized region file.
    """
    clip, _ = otb.create_application(
        AvailableOTBApp.BAND_MATH,
        {
            "il": [tile_region_raster, tile_boundaries_file_raster],
            "exp": "im1b1*im2b1",
            "out": tile_region_raster,
            "pixType": "uint8",
        },
    )
    clip.ExecuteAndWriteOutput()


def rasterize_region(img_ref: str, output_path_buff: str) -> str:
    """
    Rasterize a buffered shapefile for a given region.

    Parameters
    ----------
    img_ref : str
        Path to the reference image.
    output_path_buff : str
        Path to the buffered shapefile of the region.

    Returns
    -------
    tile_region_raster : str
        Path to the rasterized region file.
    """
    tile_region_raster = output_path_buff.replace("_buff.shp", ".tif")
    tile_region_app, _ = otb.create_application(
        AvailableOTBApp.RASTERIZATION,
        {
            "in": output_path_buff,
            "out": tile_region_raster,
            "im": img_ref,
            "mode": "binary",
            "pixType": "uint8",
            "background": "0",
            "mode.binary.foreground": "1",
        },
    )
    tile_region_app.ExecuteAndWriteOutput()
    return tile_region_raster


def create_region_shapefile(
    database: DBInfo,
    logger: logging.Logger,
    origin_name: str,
    output_directory: str,
    region: str,
    tile_name: str,
    x_res: float,
) -> tuple[str, str]:
    """
    Create a shapefile for a region by querying the database, and apply buffering.

    Parameters
    ----------
    database : DBInfo
        Contains path to the database and region field information.
    logger : logging.Logger
        Logger for output messages.
    origin_name : str
        Name of the original region.
    output_directory : str
        Directory to save the shapefile.
    region : str
        The name of the region being processed.
    tile_name : str
        The name of the current tile.
    x_res : float
        Resolution of the raster, used for buffering.

    Returns
    -------
    output_path : str
        Path to the generated shapefile.
    output_path_buff : str
        Path to the buffered shapefile.
    """
    output_name = f"{origin_name}_region_{region}_{tile_name}_tmp.shp"
    output_path = str(Path(output_directory) / output_name)
    assert isinstance(database.db_file, (str, Path))
    db_name = (Path(database.db_file).stem).lower()
    cmd = (
        f"ogr2ogr -f 'ESRI Shapefile' -sql \"SELECT * FROM {db_name}"
        f" WHERE {database.region_field}='{region}'\" {output_path} {database.db_file}"
    )
    run(cmd, logger=logger)
    output_path_buff = output_path.replace("_tmp.shp", "_buff.shp")
    vf.erode_or_dilate_shapefile(
        infile=output_path, outfile=output_path_buff, buffdist=2 * x_res
    )
    return output_path, output_path_buff


def keep_fields(
    vec_in: str,
    vec_out: str,
    fields: list[str] | None = None,
    proj_in: int = 2154,
    proj_out: int = 2154,
    logger: logging.Logger = LOGGER,
) -> None:
    """Extract fields of an input SQLite file.

    Parameters
    ----------
    vec_in:
        input SQLite vector File
    vec_out:
        output SQLite vector File
    fields:
        list of fields to keep
    proj_in:
        input projection
    proj_out:
        output projection
    """
    fields = fields or []  # disallow None, replace by empty list

    table_in = Path(vec_in).stem.lower()
    table_out = Path(vec_out).stem.lower()

    ext = Path(vec_in).suffix
    driver_vec_in = "ESRI Shapefile"
    if "sqlite" in ext:
        driver_vec_in = "SQLite"
    geom_column_name = get_geom_column_name(vec_in, driver_name=driver_vec_in)
    sql_clause = f"select {geom_column_name}," f"{','.join(fields)} from {table_in}"
    cmd = (
        f"ogr2ogr -s_srs EPSG:{proj_in} -t_srs EPSG:{proj_out} -dialect "
        f"'SQLite' -f 'SQLite' -nln {table_out} -sql '{sql_clause}' "
        f"{vec_out} {vec_in}"
    )
    run(cmd, logger=logger)


def split_by_sets(
    vector: str,
    seeds: int,
    split_directory: str,
    proj_in: int,
    proj_out: int,
    tile_name: str,
    split_ground_truth: bool = True,
    logger: logging.Logger = LOGGER,
) -> list[str]:
    """Create new vector file by learning / validation sets.

    Parameters
    ----------
    vector:
        path to a shape file containg ground truth
    seeds:
        number of run
    split_directory:
        output directory
    proj_in:
        input projection
    proj_out:
        output projection
    tile_name:
        tile's name
    split_ground_truth:
        flat to split ground truth
    """
    out_vectors = []
    fields = fields_to_retain(seeds, vector)

    # start split
    for seed in range(seeds):
        valid_clause = f"seed_{seed}='validation'"
        learn_clause = f"seed_{seed}='learn'"
        output_vec_valid = process_set(
            vector,
            valid_clause,
            split_directory,
            tile_name,
            seed,
            "val",
            (proj_in, proj_out),
            fields,
            split_ground_truth,
            logger,
        )
        output_vec_learn = process_set(
            vector,
            learn_clause,
            split_directory,
            tile_name,
            seed,
            "learn",
            (proj_in, proj_out),
            fields,
            True,
            logger,
        )

        if split_ground_truth is False:
            shutil.copy(output_vec_learn, output_vec_valid)
        if split_ground_truth:
            out_vectors.append(output_vec_valid)
        out_vectors.append(output_vec_learn)
    return out_vectors


def process_set(
    vector: str,
    clause: str,
    split_directory: str,
    tile_name: str,
    seed: int,
    set_type: str,
    proj_in_out: tuple[int, int],
    fields: list[str],
    process: bool,
    logger: logging.Logger,
) -> str:
    """
    Process a specific set (learning or validation) for a given seed by executing SQL queries
    and preserving relevant fields.

    Parameters
    ----------
    vector : str
        Path to the vector shapefile containing ground truth data.
    clause : str
        SQL clause to filter the data (e.g., 'seed_X=validation' or 'seed_X=learn').
    split_directory : str
        Directory where the resulting split vectors will be saved.
    tile_name : str
        Name of the current tile being processed.
    seed : int
        The seed number, used to split data into different sets (e.g., learning or validation).
    set_type : str
        Type of set to process ('learn' or 'validation').
    proj_in_out : tuple[int, int]
        Tuple containing input projection (EPSG) and output projection (EPSG) as integers.
    fields : list[str]
        List of fields to retain in the resulting vector file after processing.
    logger : logging.Logger
        Logger instance for logging the process steps.

    Returns
    -------
    str
        Path to the processed vector file.
    """
    vector_layer_name = Path(vector).stem.lower()
    sql_cmd = f"SELECT * FROM {vector_layer_name} WHERE {clause}"
    output_vec_name = "_".join([tile_name, f"seed_{seed}", set_type])
    output_vec_name_tmp = "_".join([tile_name, f"seed_{seed}", set_type, "tmp"])

    output_vec_tmp = str(Path(split_directory) / output_vec_name_tmp) + ".sqlite"
    output_vec = str(Path(split_directory) / output_vec_name) + ".sqlite"

    cmd = (
        f"ogr2ogr -t_srs EPSG:{proj_in_out[1]} -s_srs EPSG:{proj_in_out[0]} "
        f'-nln {output_vec_name_tmp} -f "SQLite" -sql "{sql_cmd}" {output_vec_tmp} {vector}'
    )
    if process:
        run(cmd, logger=logger)
        keep_fields(
            output_vec_tmp, output_vec, fields, proj_in_out[0], proj_in_out[1], logger
        )
        Path(output_vec_tmp).unlink()

    return output_vec


def fields_to_retain(seeds: int, vector: str) -> list[str]:
    """
    Determine the fields to retain from a vector by removing fields related to the seeds
    and tile origin.

    Parameters
    ----------
    seeds : int
        Number of seeds used in the process, each seed corresponds to a field like 'seed_X'.
    vector : str
        Path to the shapefile from which fields are to be extracted.

    Returns
    -------
    list[str]
        List of field names to retain after filtering out seed-related and tile origin fields.
    """
    fields_to_rm = ["seed_" + str(seed) for seed in range(seeds)]
    fields_to_rm.append(I2_CONST.tile_origin_field)
    all_fields = vf.get_all_fields_in_shape(vector)
    fields = [field_name for field_name in all_fields if field_name not in fields_to_rm]
    return fields


def built_where_sql_exp(sample_id_to_extract: Iterable[int], clause: str) -> str:
    """Construct the where clause for sql request.

    Parameters
    ----------
    sample_id_to_extract:
        list of ID to extract
    clause:
        "in" or "not in" clause values
    """
    if clause not in ["in", "not in"]:
        raise Exception("clause must be 'in' or 'not in'")
    sql_limit = 1000.0
    sample_id_to_extract_as_str = [str(elem) for elem in sample_id_to_extract]
    sample_id_to_extract_split = fut.split_list(
        sample_id_to_extract_as_str,
        nb_split=int(math.ceil(float(len(sample_id_to_extract_as_str)) / sql_limit)),
    )
    list_fid = [
        f"fid {clause} ({','.join(chunk)})" for chunk in sample_id_to_extract_split
    ]
    sql_exp = " AND ".join(list_fid) if "not" in clause else " OR ".join(list_fid)
    return sql_exp


def extract_maj_vote_samples(
    vec_in: str,
    vec_out: str,
    ratio_to_keep: float,
    data_field: str,
    region_field: str,
    random_seed: int | None = None,
    driver_name: str = "ESRI Shapefile",
    logger: logging.Logger = LOGGER,
) -> None:
    """Extract samples according a ratio.

    dedicated to extract samples by class according to a ratio.
    Samples are remove from vec_in and place in vec_out

    Parameters
    ----------
    vec_in : string
        path to a shapeFile (.shp)
    vec_out : string
        path to a sqlite (.sqlite)
    ratio_to_keep [float]
        percentage of samples to extract ratio_to_keep = 0.1
        mean extract 10% of each class in each regions
    data_field:
        field containing class labels
    region_field:
        field containing regions labels
    random_seed : int
        random seed
    driver_name : string
        OGR driver
    """
    class_avail = vf.get_field_element(
        vec_in, elem_type=int, field=data_field, mode="unique"
    )
    region_avail = vf.get_field_element(
        vec_in, elem_type=str, field=region_field, mode="unique"
    )

    driver = ogr.GetDriverByName(driver_name)
    source = driver.Open(vec_in, 1)
    layer = source.GetLayer(0)
    assert region_avail and class_avail
    sample_id_to_extract, _ = subset.get_random_poly(
        layer,
        data_field,
        class_avail,
        ratio_to_keep,
        region_field,
        region_avail,
        random_seed=random_seed,
    )

    # Create new file with targeted FID
    fid_samples_in = built_where_sql_exp(sample_id_to_extract, clause="in")
    cmd = f"ogr2ogr -where '{fid_samples_in}' -f 'SQLite' {vec_out} {vec_in}"
    run(cmd, logger=logger)

    # remove in vec_in targeted FID
    vec_in_rm = vec_in.replace(".shp", "_tmp.shp")
    fid_samples_not_in = built_where_sql_exp(sample_id_to_extract, clause="not in")
    cmd = f"ogr2ogr -where '{fid_samples_not_in}' {vec_in_rm} {vec_in}"
    run(cmd, logger=logger)

    vf.remove_shape(vec_in)

    cmd = f"ogr2ogr {vec_in} {vec_in_rm}"
    run(cmd, logger=logger)

    vf.remove_shape(vec_in_rm)


@dataclass
class VectorFormattingPaths:
    """
    Paths related to vector formatting.

    Attributes
    ----------
    output_path : str
        The output directory path.
    tile_name : str
        The name of the tile.
    region_vec : str | None
        The path to the region vector file, by default None.
    """

    output_path: str
    tile_name: str
    region_vec: str | None = None


@dataclass
class VectorFormattingFlags:
    """
    Flags for vector formatting options.

    Attributes
    ----------
    enable_split_ground_truth : bool
        Flag to enable splitting of the ground truth data.
    fusion_merge_all_validation : bool
        Flag to enable fusion and merging of all validation data.
    boundary_fusion_enabled : bool
        Flag to enable boundary fusion.
    merge_final_classifications : bool, optional
        Flag to enable merging of final classifications, by default False.
    """

    enable_split_ground_truth: bool
    fusion_merge_all_validation: bool
    boundary_fusion_enabled: bool
    merge_final_classifications: bool = False


@dataclass
class PolygonsSelection:
    """
    Parameters for polygon sample selection.

    Attributes
    ----------
    cloud_threshold : float
        Threshold value for cloud filtering.
    ratio : float
        Ratio for sample selection.
    random_seed : int
        Seed for random number generator.
    runs : int
        Number of runs for sampling.
    merge_final_classifications_ratio : float | None
        Ratio for merging final classifications, by default None.
    """

    cloud_threshold: float
    ratio: float
    random_seed: int
    runs: int
    merge_final_classifications_ratio: float | None = None


def create_directories(
    working_directory: str | None, paths: VectorFormattingPaths
) -> str:
    """
    Create the required directories for processing and return the work directory path.
    """
    work_dir = str(Path(paths.output_path) / "formattingVectors")
    if working_directory:
        work_dir = working_directory

    work_dir = str(Path(work_dir) / paths.tile_name)
    try:
        Path(work_dir).mkdir(parents=True, exist_ok=True)
    except OSError:
        LOGGER.warning(f"{work_dir} already exists")
    return work_dir


@dataclass
class InternalVectorPaths:
    """
    Class gathering paths during vector formating process.
    """

    paths: VectorFormattingPaths
    polygons_selection: PolygonsSelection
    working_directory: str | None = None
    output: str = field(init=False)
    cloud_vec: str = field(init=False)
    tile_env_vec: str = field(init=False)
    region_vec: str = field(init=False)
    final_directory: str = field(init=False)
    split_directory: str = field(init=False)
    features_directory: str = field(init=False)
    img_ref: str = field(init=False)

    def __post_init__(self) -> None:
        """Set up the paths after initialization."""
        output_name = self.paths.tile_name + ".shp"
        self.formatting_directory = str(
            Path(self.paths.output_path) / "formattingVectors"
        )
        if self.working_directory:
            self.formatting_directory = self.working_directory
        self.features_directory = str(Path(self.paths.output_path) / "features")

        # Initialize paths
        self.output = str(Path(self.formatting_directory) / output_name)
        self.cloud_vec = str(
            Path(self.features_directory)
            / self.paths.tile_name
            / f"CloudThreshold_{self.polygons_selection.cloud_threshold}.shp"
        )
        self.tile_env_vec = str(
            Path(self.paths.output_path) / "envelope" / f"{self.paths.tile_name}.shp"
        )
        self.region_vec = self.paths.region_vec or str(
            Path(self.paths.output_path) / "MyRegion.shp"
        )
        self.final_directory = str(Path(self.paths.output_path) / "final")
        self.split_directory = str(Path(self.paths.output_path) / "dataAppVal")
        self.img_ref = fut.file_search_and(
            str(Path(self.features_directory) / self.paths.tile_name / "tmp"),
            True,
            ".tif",
        )[0]


def vector_formatting(
    paths: VectorFormattingPaths,
    database: DBInfo,
    polygons_selection: PolygonsSelection,
    distance_map_params: DistanceMapParameters,
    flags: VectorFormattingFlags,
    epsg: int,
    classif_mode: Literal["separate", "fusion"],
    original_label_column: str,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Formats vector data for a given tile.

    Parameters
    ----------
    paths : VectorFormattingPaths
        Paths for tile name, output path, and optional region vector.
    database : DBInfo
        Database information including the ground truth vector, data field, and region field.
    polygons_selection : PolygonsSelection
        Parameters for sample selection including cloud threshold, ratio, random seed, and runs.
    distance_map_params : DistanceMapParameters
        Parameters for distance map calculations including buffer sizes, epsilon, and spatial
        resolution.
    flags : VectorFormattingFlags
        Boolean flags for various processing options.
    epsg : int
        EPSG code for spatial reference.
    classif_mode : Literal["separate", "fusion"]
        Classification mode.
    original_label_column : str
        Name of the original label column in the data.
    working_directory : str | None
        Directory for intermediate working files, by default None.
    logger : logging.Logger, optional
        Logger for the process, by default uses module logger.
    """
    work_dir = create_directories(working_directory, paths)
    internal_paths = InternalVectorPaths(paths, polygons_selection, working_directory)
    wd_maj_vote = None

    if flags.merge_final_classifications:
        wd_maj_vote = str(
            Path(internal_paths.final_directory) / "merge_final_classifications"
        )
        if working_directory:
            wd_maj_vote = working_directory

    output_driver = "SQLite"
    if Path(internal_paths.output).suffix == ".shp":
        output_driver = "ESRI Shapefile"

    # log
    logger.info(f"formatting vector for tile : {paths.tile_name}")
    logger.info("launch intersection between tile's envelope and regions")
    tile_region = str(Path(work_dir) / "tileRegion_") + paths.tile_name + ".sqlite"
    assert database.region_field
    region_tile_intersection = intersect.intersection_sqlite(
        IntersectionDatabaseParameters(
            data_base1=internal_paths.tile_env_vec,
            data_base2=internal_paths.region_vec,
            fields_to_keep=[database.region_field.lower()],
        ),
        output=tile_region,
        epsg=epsg,
    )
    if not region_tile_intersection:
        raise Exception(
            f"there is no intersections between the tile '{paths.tile_name}' "
            f"and the region shape '{internal_paths.region_vec}'"
        )

    # Create standard mask for sampling and classification masks
    create_tile_region_masks(
        DBInfo("", tile_region, region_field=database.region_field.lower()),
        paths.tile_name,
        str(Path(paths.output_path) / "shapeRegion"),
        Path(internal_paths.region_vec).stem,
        internal_paths.img_ref,
        i2_running_dir=paths.output_path,
        classification_mode=classif_mode,
        tile_boundaries_file=internal_paths.tile_env_vec,
        boundary_fusion_enabled=flags.boundary_fusion_enabled,
        logger=logger,
    )

    # Then compute classification masks for boundary
    if flags.boundary_fusion_enabled:
        distance_map_params = DistanceMapParameters(
            exterior_buffer_size=distance_map_params.exterior_buffer_size,
            interior_buffer_size=distance_map_params.interior_buffer_size,
            epsilon=distance_map_params.epsilon,
            spatial_res=distance_map_params.spatial_res,
            tile=paths.tile_name,
            region_field=database.region_field.lower(),
            paths=DistanceMapParameters.Paths(
                region_file=internal_paths.region_vec,
                common_mask=fut.file_search_and(
                    str(
                        Path(internal_paths.features_directory)
                        / paths.tile_name
                        / "tmp"
                    ),
                    True,
                    "Mask",
                    ".tif",
                )[0],
                output_path=str(
                    Path(paths.output_path) / "features" / paths.tile_name / "tmp"
                ),
                masks_region_path=str(Path(paths.output_path) / "classif" / "MASK"),
            ),
        )
        compute_distance_map(
            working_directory=working_directory,
            distance_map_params=distance_map_params,
        )

    logger.info("launch intersection between tile's envelopeRegion and groundTruth")
    tile_region_ground_truth = (
        str(Path(work_dir) / "tileRegionGroundTruth_") + paths.tile_name + ".sqlite"
    )

    fields_to_keep = [
        database.data_field,
        database.region_field.lower(),
        original_label_column,
    ]
    assert isinstance(database.db_file, (str, Path))
    if (
        intersect.intersection_sqlite(
            IntersectionDatabaseParameters(
                data_base1=tile_region,
                data_base2=database.db_file,
                fields_to_keep=fields_to_keep,
                db_2_fid="originfid",
            ),
            output=tile_region_ground_truth,
            epsg=epsg,
        )
        is False
    ):
        logger.warning(
            f"there si no intersections between the tile "
            f"'{paths.tile_name}' and the ground truth '{database.db_file}'"
        )
        return None
    logger.info("remove un-usable samples")
    intersect.intersection_sqlite(
        IntersectionDatabaseParameters(
            data_base1=tile_region_ground_truth,
            data_base2=internal_paths.cloud_vec,
            fields_to_keep=fields_to_keep + ["originfid"],
        ),
        internal_paths.output,
        epsg,
    )

    Path(tile_region).unlink()
    Path(tile_region_ground_truth).unlink()
    maj_vote_sample_tile = ""
    if (
        polygons_selection.merge_final_classifications_ratio
        and flags.merge_final_classifications
        and flags.fusion_merge_all_validation is False
    ):
        assert wd_maj_vote is not None
        maj_vote_sample_tile = str(
            Path(wd_maj_vote) / f"{paths.tile_name}_majvote.sqlite"
        )
        extract_maj_vote_samples(
            internal_paths.output,
            maj_vote_sample_tile,
            polygons_selection.merge_final_classifications_ratio,
            database.data_field.lower(),
            database.region_field.lower(),
            random_seed=polygons_selection.random_seed,
            driver_name="ESRI Shapefile",
            logger=logger,
        )

    logger.info(
        f"split {internal_paths.output} in {polygons_selection.runs} subsets "
        f"with the ratio {polygons_selection.ratio}"
    )
    subset.split_in_subsets(
        internal_paths.output,
        database.data_field.lower(),
        database.region_field.lower(),
        polygons_selection.ratio,
        polygons_selection.runs,
        output_driver,
        split_groundtruth=flags.enable_split_ground_truth,
        random_seed=polygons_selection.random_seed,
    )
    add_field(
        internal_paths.output,
        I2_CONST.tile_origin_field,
        paths.tile_name,
        value_type=str,
    )

    # splits by learning and validation sets (use in validations steps)
    output_splits = split_by_sets(
        internal_paths.output,
        polygons_selection.runs,
        internal_paths.split_directory,
        epsg,
        epsg,
        paths.tile_name,
        split_ground_truth=flags.enable_split_ground_truth,
        logger=logger,
    )

    check_validation_db_integrity(output_splits, logger=logger)

    if working_directory:
        finalize_outputs(
            flags,
            internal_paths,
            maj_vote_sample_tile,
            paths,
        )
    return None


def finalize_outputs(
    flags: VectorFormattingFlags,
    internal_paths: InternalVectorPaths,
    maj_vote_sample_tile: str,
    paths: VectorFormattingPaths,
) -> None:
    """
    Finalizes and saves the output files based on the specified output driver.
    The function manages the copying of vector formatting outputs and the removal of
    intermediate files.

    Parameters
    ----------
    flags : VectorFormattingFlags
        Flags controlling whether to merge final classifications and validation options.
    internal_paths : InternalVectorPaths
        Paths object containing internal paths used during processing (e.g., output path...).
    maj_vote_sample_tile : str
        Path to the major vote sample tile file.
    paths : VectorFormattingPaths
        Paths object containing various directories and tile-specific path information.
    """
    output_name = paths.tile_name + ".shp"
    output_driver = "SQLite"
    if Path(internal_paths.output).suffix == ".shp":
        output_driver = "ESRI Shapefile"

    if output_driver == "SQLite":
        shutil.copy(
            internal_paths.output,
            str(Path(paths.output_path) / "formattingVectors"),
        )
        Path(internal_paths.output).unlink()

    elif output_driver == "ESRI Shapefile":
        vf.copy_shapefile(
            internal_paths.output,
            str(Path(paths.output_path) / "formattingVectors" / output_name),
        )
        vf.remove_shape(internal_paths.output)

    if flags.merge_final_classifications and flags.fusion_merge_all_validation is False:
        shutil.copy(
            maj_vote_sample_tile,
            str(Path(internal_paths.final_directory) / "merge_final_classifications"),
        )
