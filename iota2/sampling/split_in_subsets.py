# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to split a shape into subsets of training and validation"""
import logging
import random

from osgeo import ogr

from iota2.vector_tools import vector_functions as vf

logger = logging.getLogger("distributed.worker")


def get_random_poly(
    layer: ogr.Layer,
    field: str,
    classes: list[int],
    ratio: float,
    region_field: str,
    regions: list[str],
    random_seed: int | None = None,
) -> tuple[set[int], set[int]]:
    """Use to randomly split samples in learning and validation considering classes in regions.

    Parameters
    ----------
    layer :
    field : string
        data field
    classes : list
    ratio : float
        ratio number of validation polygons / number of training polygons
    region_field : string
        region field
    regions : list
        list of regions to consider
    random_seed : int
        random seed (default = None)
    Returns
    -------
    tuple(sample_id_learn, sample_id_valid)
        where sample_id_learn is a set of polygon's ID dedicated to learn models
        where sample_id_valid is a set of polygon's ID dedicated to validate models
    """
    sample_id_learn = []
    sample_id_valid = []
    layer.ResetReading()
    for region in regions:
        for ref_class in classes:
            listid = []
            layer.SetAttributeFilter(None)
            attrib_filter = f"{field}={ref_class} AND {region_field}='{region}'"
            layer.SetAttributeFilter(attrib_filter)
            feature_count = float(layer.GetFeatureCount())
            if feature_count == 1:
                for feat in layer:
                    _id = feat.GetFID()
                    sample_id_learn.append(_id)
                    feature_clone = feat.Clone()
                    layer.CreateFeature(feature_clone)
                    sample_id_valid.append(feature_clone.GetFID())
                    break

            elif feature_count > 1:
                polbysel = round(feature_count * float(ratio))
                polbysel = max(polbysel, 1)
                for feat in layer:
                    _id = feat.GetFID()
                    listid.append(_id)
                    listid.sort()

                random.seed(random_seed)
                random_id_learn = random.sample(listid, int(polbysel))
                sample_id_learn += list(random_id_learn)
                sample_id_valid += [
                    currentFid
                    for currentFid in listid
                    if currentFid not in sample_id_learn
                ]

    sample_id_learn.sort()
    sample_id_valid.sort()

    layer.SetAttributeFilter(None)
    return set(sample_id_learn), set(sample_id_valid)


def split_in_subsets(
    vecto_file: str,
    data_field: str,
    region_field: str,
    ratio: float = 0.5,
    seeds: int = 1,
    driver_name: str = "SQLite",
    split_groundtruth: bool = True,
    random_seed: int | None = None,
) -> None:
    """
    This function is dedicated to split a shape into N subsets
    of training and validations samples by adding a new field
    by subsets (seed_X) containing 'learn', 'validation'

    Parameters
    ----------

    vecto_file : string
        input vector file
    data_field : string
        field which discriminate class
    region_field : string
        field which discriminate region
    ratio : int
        ratio between learn and validation features
    seeds : int
        number of random splits
    driver_name : string
        OGR layer name
    split_groundtruth
        enable the ground truth split
    random_seed : int
        random seed
    """
    driver = ogr.GetDriverByName(driver_name)
    source = driver.Open(vecto_file, 1)
    layer = source.GetLayer(0)
    list_fid = [f.GetFID() for f in layer]

    for seed in range(seeds):
        source = driver.Open(vecto_file, 1)
        layer = source.GetLayer(0)

        seed_field_name = "seed_" + str(seed)

        if seed_field_name not in vf.get_all_fields_in_shape(
            vecto_file, driver_name=driver_name
        ):
            layer.CreateField(ogr.FieldDefn(seed_field_name, ogr.OFTString))

        random_seed_number = None
        if random_seed is not None:
            random_seed_number = random_seed + seed
        class_list = vf.get_field_element(
            vecto_file, elem_type=int, field=data_field, mode="unique"
        )
        regions_list = vf.get_field_element(
            vecto_file, elem_type=str, field=region_field, mode="unique"
        )
        assert class_list and regions_list
        id_learn, id_val = get_random_poly(
            layer,
            data_field,
            class_list,
            ratio,
            region_field,
            regions_list,
            random_seed_number,
        )

        if split_groundtruth is False:
            id_learn = id_learn.union(id_val)
        set_feature(
            id_learn,
            id_val,
            layer,
            "learn",
            list_fid,
            seed_field_name,
            "validation",
        )
        layer = None


def set_feature(
    id_learn: set[int],
    id_val: set[int],
    layer: ogr.Layer,
    learning_flag: str,
    list_fid: list[int],
    seed_field_name: str,
    validation_flag: str,
) -> None:
    """
    Assigns flags ('learn' or 'validation') to features in a layer based on their FID.

    This function updates each feature's attribute in the layer based on whether its FID
    is present in the `id_learn` or `id_val` sets. The assigned flag is written into
    the field specified by `seed_field_name`.

    Parameters
    ----------
    id_learn : set[int]
        A set of feature IDs that belong to the learning set.
    id_val : set[int]
        A set of feature IDs that belong to the validation set.
    layer : ogr.Layer
        The OGR layer where features are located and to be modified.
    learning_flag : str
        The string flag to assign for features in the learning set.
    list_fid : list[int]
        A list of all feature IDs (FIDs) in the layer.
    seed_field_name : str
        The name of the field where the flags ('learn' or 'validation') will be set.
    validation_flag : str
        The string flag to assign for features in the validation set.
    """
    for fid in list_fid:
        flag = None
        if fid in id_learn:
            flag = learning_flag
        elif fid in id_val:
            flag = validation_flag

        feat = layer.GetFeature(fid)
        feat.SetField(seed_field_name, flag)
        layer.SetFeature(feat)
