# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Generate statistics by tiles, useful to select samples"""
import logging
import shutil
from pathlib import Path

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fut
from iota2.common import otb_app_bank as otb

I2_CONST = i2_const.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")


def samples_stats(
    region_seed_tile: tuple[str, str, str],
    iota2_directory: str,
    data_field: str,
    sampling_validation: bool,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> str:
    """generate samples statistics by tiles

    Parameters
    ----------
    region_seed_tile: tuple
    iota2_directory: str
        iota2 output directory
    data_field: str
        data field in region database
        the samples are split according to this column
    sampling_validation:bool
        perform statistics also on validation sample selection
    working_directory: str
        path to a working directory
    logger: logging
        root logger

    Return
    ------
    str
        file containing statistics from otbcli_PolygonClassStatistics
    """
    region, seed, tile = region_seed_tile
    samples_selection_dir = str(Path(iota2_directory) / "samplesSelection")

    w_dir = samples_selection_dir
    if working_directory:
        w_dir = working_directory

    raster_mask = fut.file_search_and(
        str(Path(iota2_directory) / "shapeRegion"),
        True,
        "region_" + region.split("f")[0] + "_",
        ".tif",
        tile,
    )[0]
    region_vec = fut.file_search_and(
        samples_selection_dir, True, f"_region_{region}_seed_{seed}.shp"
    )[0]

    logger.info(f"Launch statistics on tile {tile} in region {region} run {seed}")
    region_tile_stats_name = f"{tile}_region_{region}_seed_{seed}_stats.xml"
    region_tile_stats = str(Path(w_dir) / region_tile_stats_name)
    class_stats_param = {
        "in": raster_mask,
        "mask": raster_mask,
        "vec": region_vec,
        "field": data_field.lower(),
        "out": region_tile_stats,
    }
    logger.debug(f"PolygonClassStatisticsApplication parameters : {class_stats_param}")
    polygon_stats_app, _ = otb.create_application(
        otb.AvailableOTBApp.POLYGON_CLASS_STATISTICS, class_stats_param
    )
    polygon_stats_app.ExecuteAndWriteOutput()
    if working_directory:
        shutil.copy(region_tile_stats, samples_selection_dir)
    if sampling_validation:
        region_val = fut.file_search_and(
            samples_selection_dir, True, "_region_" + region, f"val_seed_{seed}.shp"
        )[0]
        logger.info(
            f"Launch statistics on tile {tile} in "
            f"region {region} run {seed} for validation"
        )
        region_tile_stats_val = str(
            Path(w_dir) / f"{tile}_region_{region}_" f"seed_{seed}_stats_val.xml"
        )
        class_stats_param_val = {
            "in": raster_mask,
            "mask": raster_mask,
            "vec": region_val,
            "field": data_field.lower(),
            "out": region_tile_stats_val,
        }
        logger.debug(
            f"PolygonClassStatisticsApplication parameters "
            f"validation : {class_stats_param}"
        )
        polygon_stats_app, _ = otb.create_application(
            otb.AvailableOTBApp.POLYGON_CLASS_STATISTICS, class_stats_param_val
        )
        polygon_stats_app.ExecuteAndWriteOutput()
        if working_directory:
            shutil.copy(region_tile_stats_val, samples_selection_dir)
    return region_tile_stats
