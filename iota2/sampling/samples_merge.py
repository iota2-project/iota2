#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
The samples merge module
"""
import logging
from pathlib import Path

from iota2.common import file_utils as fut
from iota2.common.utils import run
from iota2.vector_tools import merge_files as mf
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def get_models(
    formatting_vector_directory: str, region_field: str, runs: int
) -> list[tuple[str, list[str], int]]:
    """
    usage :
    describe samples spatial repartition
    function use to determine with shapeFile as to be merged in order to
    compute statistics thanks to otb_SampleSelection

    OUT:
    regions_tiles_seed [list] :
    example
    regions_tiles_seed = [('1', ['T1', 'T2'], 0), ('1', ['T1', T2], 1),
                          ('2', ['T2', 'T3], 0), ('2', ['T2', 'T3], 1)]
    mean the region '1' is present in tiles 'T1' and 'T2' in run 0 and 1
    and region '2' in 'T2', 'T3' in runs 0 and 1
    """
    # the way of getting region could be improve ?
    tiles = fut.file_search_and(formatting_vector_directory, True, ".shp")
    region_tile = []
    all_regions_in_run = []
    for tile in tiles:
        all_regions = []
        tile_name = Path(tile).stem
        r_tmp = vf.get_field_element(
            tile, elem_type=str, field=region_field, mode="unique"
        )
        assert r_tmp
        for r_tile in r_tmp:
            if r_tile not in all_regions:
                all_regions.append(r_tile)

        for region in all_regions:
            if region not in all_regions_in_run:
                all_regions_in_run.append(region)
            region_tile.append((region, tile_name))

    region_tile_tmp = dict(fut.sort_by_first_elem(region_tile))
    region_tile_dic = {}
    for region, region_tiles in list(region_tile_tmp.items()):
        region_tile_dic[region] = list(set(region_tiles))

    all_regions_in_run = sorted(all_regions_in_run)
    regions_tiles_seed = [
        (region, region_tile_dic[region], run)
        for run in range(runs)
        for region in all_regions_in_run
    ]
    return regions_tiles_seed


def extract_poi(
    tile_vector: str,
    region: str,
    seed: int,
    region_field: str,
    poi: str,
    poi_val: str | None = None,
    force_seed_field: str | None = None,
) -> None:
    """
    Extract Polygon Of Interest
    Parameters
    ----------
    tile_vector: string
    region: string
    seed: int
    region_field: str
    poi: str
    poi_val: str
    force_seed_field: bool
    Return
    ------
    None
    """
    learn_flag = "learn"
    validation_flag = "validation"
    seed_field = f"seed_{seed}"
    cmd = (
        f"ogr2ogr -where \"{region_field}='{region}' AND {seed_field}"
        f"='{learn_flag}'\" {poi} {tile_vector}"
    )
    run(cmd)
    if poi_val:
        if force_seed_field:
            seed_field = force_seed_field
        cmd = (
            f"ogr2ogr -where \"{region_field}='{region}' AND {seed_field}="
            f"'{validation_flag}'\" {poi_val} {tile_vector}"
        )
        run(cmd)


def samples_merge(
    region_tiles_seed: tuple[str, list[str], int],
    output_path: str,
    region_field: str,
    working_directory: str,
    sampling_validation: bool,
) -> None:
    """
    to a given region and seed, extract features through tiles
    then merge features to a new file
    """

    region, tiles, seed = region_tiles_seed
    by_models_val = str(Path(str(Path(output_path) / "dataAppVal")) / "bymodels")

    if not Path(by_models_val).exists():
        try:
            Path(by_models_val).mkdir()
        except OSError:
            pass
    wd_val = by_models_val
    work_dir = str(Path(output_path) / "samplesSelection")

    if working_directory:
        work_dir = working_directory
        wd_val = working_directory

    cross_validation_field = None

    vector_region, vector_region_val = process_tiles(
        cross_validation_field,
        output_path,
        region,
        region_field,
        sampling_validation,
        seed,
        tiles,
        wd_val,
        work_dir,
    )

    merged_poi_name = f"samples_region_{region}_seed_{seed}"
    merged_poi_name_val = f"samples_region_{region}_val_seed_{seed}"
    merged_poi = mf.merge_vectors(merged_poi_name, work_dir, vector_region)
    merged_poi_val = mf.merge_vectors(merged_poi_name_val, work_dir, vector_region_val)

    for vector_r in vector_region:
        vf.remove_shape(vector_r)

    if working_directory:
        assert merged_poi
        vf.copy_shapefile(merged_poi, str(Path(output_path) / "samplesSelection"))
        if sampling_validation:
            assert merged_poi_val
            vf.copy_shapefile(
                merged_poi_val, str(Path(output_path) / "samplesSelection")
            )


def process_tiles(
    cross_validation_field: str | None,
    output_path: str,
    region: str,
    region_field: str,
    sampling_validation: bool,
    seed: int,
    tiles: list[str],
    wd_val: str,
    work_dir: str,
) -> tuple[list[str], list[str]]:
    """
    Process tiles, extract points of interest (POI), and return lists of POI files for the
    region and validation.

    This function processes each tile in the given list of `tiles`, searches for corresponding
    shapefiles, extracts points of interest based on the provided parameters, and prepares a list
    of POI files for learning and, optionally, validation.

    Parameters
    ----------
    cross_validation_field : str | None
        Field name used for cross-validation. If not needed, set to None.
    output_path : str
        The base directory where the formatted vector files and other outputs are located.
    region : str
        The name of the region being processed.
    region_field : str
        The field in the shapefile used to discriminate regions.
    sampling_validation : bool
        If True, also extracts validation samples from the shapefiles.
    seed : int
        Seed used for random operations to ensure reproducibility.
    tiles : list[str]
        List of tile names to process.
    wd_val : str
        Working directory for validation outputs.
    work_dir : str
        Working directory for sample selection outputs.

    Returns
    -------
    tuple[list[str], list[str]]
        A tuple containing two lists:
        - vector_region: list of shapefile paths for the learning points of interest.
        - vector_region_val: list of shapefile paths for the validation points of interest
          (if `sampling_validation` is True).
    """
    vector_region = []
    vector_region_val = []
    for tile in tiles:
        vector_tiles = fut.file_search_and(
            str(Path(output_path) / "formattingVectors"), True, tile, ".shp"
        )
        if vector_tiles:
            vector_tile = vector_tiles[0]
        else:
            continue

        poi_name = f"{tile}_region_{region}_seed_{seed}_samples.shp"
        poi_learn = str(Path(work_dir) / poi_name)
        poi_val = None
        # if sar and Optical post-classification fusion extract validation
        # samples
        if sampling_validation:
            poi_val_name = f"{tile}_region_{region}_seed_{seed}_samples_val.shp"
            poi_val = str(Path(wd_val) / poi_val_name)
            vector_region_val.append(poi_val)
        extract_poi(
            vector_tile,
            region,
            seed,
            region_field,
            poi_learn,
            poi_val,
            cross_validation_field,
        )
        vector_region.append(poi_learn)
    return vector_region, vector_region_val
