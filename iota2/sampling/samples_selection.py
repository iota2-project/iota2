#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provide the sample selection functions."""
import logging
import shutil
import sqlite3 as lite
from pathlib import Path
from typing import Any
from xml.dom import minidom
from xml.etree import ElementTree as ET

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fut
from iota2.common import otb_app_bank
from iota2.common.utils import run
from iota2.typings.i2_types import DBInfo
from iota2.vector_tools import merge_files as mf
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.vector_functions import get_driver, get_vector_proj

I2_CONST = i2_const.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")


def prepare_selection(
    sample_sel_directory: str,
    tile_name: str,
    output_sample_sel_directory: str | None = None,
    working_directory: str | None = None,
) -> str:
    """Prepare the sample selection.

    This function is dedicated to merge selection comming from different
    models by tiles. It is necessary in order to prepare sampleExtraction
    in the step call 'generate samples'

    Parameters
    ----------
    sample_sel_directory :
        path to the iota2 directory containing selections by models
    output_sample_sel_directory:
        path to the iota2 directory to save the merge selection files
    tile_name :
        tile's name
    working_directory :
        path to a working directory
    """
    if not output_sample_sel_directory:
        output_sample_sel_directory = sample_sel_directory

        vectors = fut.file_search_and(
            sample_sel_directory, True, tile_name, "selection.sqlite"
        )

    else:
        vectors = fut.file_search_and(sample_sel_directory, True, tile_name, ".shp")

    work_dir = output_sample_sel_directory
    if working_directory:
        work_dir = working_directory

    merge_selection_name = f"{tile_name}_selection_merge"
    output_selection_merge = str(Path(work_dir) / merge_selection_name) + ".sqlite"

    mf.merge_vectors(
        merge_selection_name, work_dir, vectors, ext="sqlite", out_tbl_name="output"
    )

    if working_directory:
        if Path(output_selection_merge).exists():
            shutil.copy(
                output_selection_merge,
                str(Path(output_sample_sel_directory) / merge_selection_name)
                + ".sqlite",
            )

    return str(Path(output_sample_sel_directory) / merge_selection_name) + ".sqlite"


def write_xml(
    samples_per_class: dict,
    samples_per_vector: dict,
    output_merged_stats: str,
) -> None:
    """Write a xml file according to otb's xml file pattern.

    Parameters
    ----------
    samples_per_class :
        by class (as key), the pixel count
    samples_per_vector :
        by vector (as key), the pixel count
    output_merged_stats :
        output path

    Note
    ----

    output xml format as `PolygonClassStatistics
    <http://www.orfeo-toolbox.org/Applications/PolygonClassStatistics.html>`_'s
    output
    """

    def prettify(elem: ET.Element) -> Any:
        rough_string = ET.tostring(elem, "utf-8")
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="  ")

    top = ET.Element("GeneralStatistics")
    parent_a = ET.SubElement(top, "Statistic", name="samplesPerClass")
    parent_b = ET.SubElement(top, "Statistic", name="samplesPerVector")

    samples_per_class_xml = "".join(
        [
            f'<StatisticMap value="{count}" />'
            for _, count in list(samples_per_class.items())
        ]
    )
    samples_per_class_part = ET.XML(f"""<root>{samples_per_class_xml}</root>""")

    samples_per_vector_xml = "".join(
        [
            f'<StatisticMap value="{count}" />'
            for _, count in list(samples_per_vector.items())
        ]
    )
    samples_per_vector_part = ET.XML(f"""<root>{samples_per_vector_xml}</root>""")

    for index, c_statistic_map in enumerate(samples_per_class_part):
        c_statistic_map.set("key", list(samples_per_class.keys())[index])

    for index, c_statistic_map in enumerate(samples_per_vector_part):
        c_statistic_map.set("key", list(samples_per_vector.keys())[index])

    # Add to first parent
    parent_a.extend(samples_per_class_part)

    # Copy nodes to second parent
    parent_b.extend(samples_per_vector_part)

    with open(output_merged_stats, "w", encoding="UTF-8") as xml_f:
        xml_f.write(prettify(top))


def merge_write_stats(stats: list[str], merged_stats: str) -> None:
    """Use to merge sample's statistics.

    Parameters
    ----------
    stats :
        list of xml files to be merged
    merged_stats :
        output xml file
    """
    samples_per_class = []
    samples_per_vector = []

    for stat in stats:
        tree = ET.parse(stat)
        root_class = tree.getroot()[0]
        root_vector = tree.getroot()[1]

        for val_class in root_class.iter("StatisticMap"):
            samples_per_class.append(
                (val_class.attrib["key"], int(val_class.attrib["value"]))
            )
        for val_vector in root_vector.iter("StatisticMap"):
            samples_per_vector.append(
                (val_vector.attrib["key"], int(val_vector.attrib["value"]))
            )

    samples_per_class_dict = dict(fut.sort_by_first_elem(samples_per_class))
    samples_per_vector_dict = dict(fut.sort_by_first_elem(samples_per_vector))

    samples_per_class_sum = {}
    for class_name, count_list in list(samples_per_class_dict.items()):
        samples_per_class_sum[class_name] = sum(count_list)

    samples_per_vector_sum = {}
    for poly_fid, count_list in list(samples_per_vector_dict.items()):
        samples_per_vector_sum[poly_fid] = sum(count_list)

    # write stats
    if Path(merged_stats).exists():
        Path(merged_stats).unlink()

    write_xml(samples_per_class_sum, samples_per_vector_sum, merged_stats)


def gen_raster_ref(
    vec: str, output_path: str, masks_name: str, working_directory: str
) -> tuple[str, list[str]]:
    """Generate the reference image needed to sample_selection application.

    Parameters
    ----------
    vec :
        path to the shapeFile containing all polygons dedicated to learn
        a model.
    output_path:
        output path
    masks_name:
        path to mask
    working_directory :
        Path to a working directory
    """
    tile_field_name = I2_CONST.tile_origin_field
    features_directory = str(Path(output_path) / "features")
    tiles = vf.get_field_element(
        vec, elem_type=str, field=tile_field_name, mode="unique"
    )
    assert tiles
    # masks_name = fut.getCommonMaskName(cfg) + ".tif"
    rasters_tiles = [
        fut.file_search_and(
            str(Path(features_directory) / tile_name), True, masks_name
        )[0]
        for tile_name in tiles
    ]
    vec_name = Path(vec).stem
    raster_ref_name = f"ref_raster_{vec_name}.tif"
    raster_ref = str(Path(working_directory) / raster_ref_name)
    raster_ref_cmd = (
        "gdal_merge.py -ot Byte -n 0 -createonly -o"
        f" {raster_ref} {' '.join(rasters_tiles)}"
    )
    run(raster_ref_cmd)
    return raster_ref, tiles


def get_sample_selection_param(
    model_name: str,
    stats: str,
    vec: str,
    working_directory: str,
    parameters: dict[str, str | int | list],
    data_field: str,
    masks_name: str,
    output_path: str,
) -> tuple[dict[str, str | int | list[Any]], list[str], str]:
    """Use to determine SampleSelection otb's parameters.

    Parameters
    ----------
    model_name :
        path to model
    stats :
        path to a xml file containing polygons statistics
    vec :
        shapeFile to sample
    working_directory :
        path to a working directory
    parameters:
        :class:`iota2.configuration_files.read_config_file.iota2_parameters`
    data_field:
        class column name
        the samples are split according to this column
    masks_name:
        path to common mask
    output_path:
        output path
    Notes
    -----
    SampleSelection's parameters are define
    `here <http://www.orfeo-toolbox.org/Applications/SampleSelection.html>`_
    """
    per_model = None
    if "per_model" in parameters:
        per_model = parameters["per_model"]
        parameters.pop("per_model", None)

    if isinstance(per_model, list):
        for strat in per_model:
            if str(model_name.split("f")[0]) == str(strat["target_model"]):
                parameters = dict(strat)
                parameters.pop("target_model", None)

    parameters["vec"] = vec
    parameters["instats"] = stats
    parameters["field"] = data_field.lower()
    raster_ref, tiles_model = gen_raster_ref(
        vec, output_path, masks_name, working_directory
    )

    parameters["in"] = raster_ref

    basename = Path(vec).stem
    sample_sel_name = f"{basename}_selection.sqlite"
    sample_sel = str(Path(working_directory) / sample_sel_name)
    outrates_name = f"{basename}_outrates.csv"
    outrates = str(Path(working_directory) / outrates_name)
    parameters["out"] = sample_sel
    parameters["outrates"] = outrates

    return parameters, tiles_model, outrates


def split_sel(
    model_selection: str, tiles: list[str], working_directory: str, epsg: str
) -> list[str]:
    """Split a SQLite file containing points by tiles.

    Parameters
    ----------
    model_selection :
        path to samplesSelection's output to a given model
    tiles :
        list of tiles intersected by the model
    epsg :
        epsg's projection. ie : epsg="EPSG:2154"
    """
    tile_field_name = I2_CONST.tile_origin_field

    out_tiles = []
    for tile in tiles:
        mod_sel_name = Path(model_selection).stem
        tile_mod_sel_name_tmp = f"{tile}_{mod_sel_name}_tmp"
        tile_mod_sel_tmp = (
            str(Path(working_directory) / tile_mod_sel_name_tmp) + ".sqlite"
        )
        if Path(tile_mod_sel_tmp).exists():
            Path(tile_mod_sel_tmp).unlink()

        with lite.connect(tile_mod_sel_tmp) as conn:
            cursor = conn.cursor()
            cursor.execute(f"ATTACH '{model_selection}' AS db")
            cursor.execute(
                f"CREATE TABLE {tile_mod_sel_name_tmp.lower()} as "
                f"SELECT * FROM db.output WHERE {tile_field_name}='{tile}'"
            )
            conn.commit()

            tile_mod_sel_name = f"{tile}_{mod_sel_name}"
            tile_mod_sel = str(Path(working_directory) / tile_mod_sel_name) + ".sqlite"
            clause = f"SELECT * FROM {tile_mod_sel_name_tmp}"
            cmd = (
                f'ogr2ogr -sql "{clause}" -dialect "SQLite" -f "SQLite" -s_srs '
                f"{epsg} -t_srs {epsg} -nln {tile_mod_sel_name.lower()}"
                f" {tile_mod_sel} {tile_mod_sel_tmp}"
            )
            run(cmd)

            Path(tile_mod_sel_tmp).unlink()
            out_tiles.append(tile_mod_sel)
    return out_tiles


def print_dict(dico: dict) -> str:
    """Use to print some dictionnary."""
    sep = "\n" + "\t".join(["" for _ in range(22)])
    return sep + sep.join([f"{key} : {val}" for key, val in list(dico.items())])


def do_sample_selection(
    model_name: str,
    merged_stats: str,
    database: DBInfo,
    working_directory: str,
    parameters: dict[str, str | int | list],
    masks_name: str,
    output_path: str,
    runs: int,
    samples_sel_dir: str,
    logger: logging.Logger = LOGGER,
) -> None:
    """Execute the sample selection.

    Parameters
    ----------
    model_name:
        identify the model by the region name
    merged_stats:
        file containing the merged xml stats of each tile
    database:
        database containing polygons to sample
    working_directory:
        Path to a working directory
    parameters:
        OTB understandable dictionary of parameters for sampleSelection app
    masks_name:
        Name of the raster mask
    output_path:
        Path to store final files
    runs:
        The number of runs
    samples_sel_dir:
        Path to store final products
    logger :
        root logger
    """
    epsg = f"EPSG:{get_vector_proj(str(database.db_file), get_driver(str(database.db_file)))}"
    sel_parameters, tiles_model, outrates = get_sample_selection_param(
        model_name,
        merged_stats,
        str(database.db_file),
        working_directory,
        parameters,
        database.data_field,
        masks_name,
        output_path,
    )

    logger.info(f"SampleSelection parameters : {print_dict(sel_parameters)}")
    sample_sel_app, _ = otb_app_bank.create_application(
        otb_app_bank.AvailableOTBApp.SAMPLE_SELECTION, sel_parameters
    )
    sample_sel_app.ExecuteAndWriteOutput()
    logger.info("sample selection terminated")
    sel_out = sel_parameters["out"]
    assert isinstance(sel_out, str)
    update_flags(sel_out, runs)
    # split by tiles
    sel_tiles = split_sel(sel_out, tiles_model, working_directory, epsg)
    if not working_directory == samples_sel_dir:
        shutil.copy(outrates, samples_sel_dir)
        for sel_tile in sel_tiles:
            shutil.copy(sel_tile, samples_sel_dir)
    # remove tmp data
    assert isinstance(sel_parameters["in"], (Path, str))
    Path(sel_parameters["in"]).unlink()


def update_flags(
    vec_in: str, runs: int, flag_val: str = "XXXX", table_name: str = "output"
) -> None:
    """Set the special value to the seeds different from the current one.

    Parameters
    ----------
    vec_in :
        path to a sqlite file containing "seed_*" field(s)
    runs :
        number of random samples
    flag_val :
        features's value for seeds different from the current one
    table_name:
        the layer table name
    """
    current_seed = int(Path(vec_in).stem.split("_")[-2])

    if runs > 1:
        update_seed = ",".join(
            [f"seed_{run} = '{flag_val}'" for run in range(runs) if run != current_seed]
        )
        conn = lite.connect(vec_in)
        cursor = conn.cursor()
        sql_clause = f"UPDATE {table_name} SET {update_seed}"
        cursor.execute(sql_clause)
        conn.commit()


def samples_selection(
    model: str,
    working_directory: str,
    output_path: str,
    runs: int,
    masks_name: str,
    otb_parameters: dict[str, str | int | list],
    data_field: str,
    sampling_validation: bool,
    parameters_validation: dict[str, str | int | list],
    logger: logging.Logger = LOGGER,
) -> None:
    """Compute sample selection.

    Parameters
    ----------
    model:
        path to a shapeFile containing all polygons to build a model
    working_directory:
        Path to a working directory
    output_path:
        Path to store final files
    runs:
        The number of runs
    masks_name:
        Name of the raster mask
    otb_parameters:
        OTB understandable dictionary of parameters for sampleSelection app
    data_field:
        The column name containing labels
        the samples are split according to this column
    sampling_validation:
        Enable the validation sampling strategy
    parameters_validation:
        OTB parameters in a dictionary for sampling the validation set
    logger :
        root logger
    """
    samples_sel_dir = str(Path(output_path) / "samplesSelection")

    merged_stats = model.replace(".shp", ".xml")
    if Path(merged_stats).exists():
        Path(merged_stats).unlink()

    wdir = samples_sel_dir
    if working_directory:
        wdir = working_directory

    model_name = Path(model).stem.split("_")[2]
    seed = Path(model).stem.split("_")[4]

    logger.info(f"Launch sample selection for the model {model_name} run {seed}")

    # merge stats
    stats = fut.file_search_and(
        samples_sel_dir, True, "region_" + str(model_name), f"seed_{seed}_stats.xml"
    )

    merge_write_stats(stats, merged_stats)
    do_sample_selection(
        model_name,
        merged_stats,
        DBInfo(data_field, model),
        wdir,
        otb_parameters,
        masks_name,
        output_path,
        runs,
        samples_sel_dir,
    )

    # Prepare for validation
    if sampling_validation:
        model_val = model.replace("_seed", "_val_seed")
        merged_stats_val = model.replace(".shp", "_val.xml")
        stats_val = fut.file_search_and(
            samples_sel_dir,
            True,
            "region_" + str(model_name),
            f"seed_{seed}_stats_val.xml",
        )
        merge_write_stats(stats_val, merged_stats_val)
        if otb_parameters["rand"]:
            parameters_validation["rand"] = otb_parameters["rand"]
        do_sample_selection(
            model_name,
            merged_stats_val,
            DBInfo(data_field, model_val),
            wdir,
            parameters_validation,
            masks_name,
            output_path,
            runs,
            samples_sel_dir,
        )
