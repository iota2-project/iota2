# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to handle tile's priority."""

import logging
import shutil
from pathlib import Path

from osgeo import gdal, ogr, osr

from iota2.common import file_utils as fu
from iota2.common.file_utils import get_raster_resolution
from iota2.typings.i2_types import Envelope
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


class Tile:
    """Represent a tile."""

    def __init__(self, path: str, name: str, test_mode: bool = False):
        gtif = gdal.Open(path)
        self.path = path
        if not test_mode:
            self.x = float(gtif.GetGeoTransform()[0])
        else:
            self.x = 0.0
        if not test_mode:
            self.y = float(gtif.GetGeoTransform()[3])
        else:
            self.y = 0.0
        self.name = name
        self.envelope = "None"
        self.priority_env = "None"

    def get_x(self) -> float:
        """Get tile's x coordinate"""
        return self.x

    def get_y(self) -> float:
        """Get tile's y coordinate"""
        return self.y

    def get_path(self) -> str:
        """Get tile's path"""
        return self.path

    def get_name(self) -> str:
        """Get tile's name"""
        return self.name

    def set_envelope(self, env: str) -> None:
        """Set tile's envelope"""
        self.envelope = env

    def get_envelope(self) -> str:
        """Get tile's envelope"""
        return self.envelope

    def get_origin(self) -> tuple[float, float]:
        """Get tile's origin"""
        return self.x, self.y

    def get_priority_env(self) -> str:
        """Get the priority enveloppe"""
        return self.priority_env

    def set_priority_env(self, priority: str) -> None:
        """Set the priority enveloppe"""
        self.priority_env = priority

    def set_x(self, x_val: float) -> None:
        """Set tile's x coordinate"""
        self.x = x_val

    def set_y(self, y_val: float) -> None:
        """Set tile's y coordinate"""
        self.y = y_val


def create_shape(
    min_x: float,
    min_y: float,
    max_x: float,
    max_y: float,
    out: str,
    name: str,
    proj: int = 2154,
) -> None:
    """Create a shape with only one geometry (a rectangle)."""
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(min_x, min_y)
    ring.AddPoint(max_x, min_y)
    ring.AddPoint(max_x, max_y)
    ring.AddPoint(min_x, max_y)
    ring.AddPoint(min_x, min_y)

    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)

    driver = ogr.GetDriverByName("ESRI Shapefile")
    try:
        output = driver.CreateDataSource(out)
    except ValueError as exc:
        raise Exception("Could not create output datasource " + out) from exc

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(proj)
    new_layer = output.CreateLayer(name, geom_type=ogr.wkbPolygon, srs=srs)
    if new_layer is None:
        raise Exception("Could not create output layer")

    new_layer.CreateField(ogr.FieldDefn("FID", ogr.OFTInteger))
    new_layer_def = new_layer.GetLayerDefn()
    feature = ogr.Feature(new_layer_def)
    feature.SetGeometry(poly)
    ring.Destroy()
    poly.Destroy()
    new_layer.CreateFeature(feature)

    output.Destroy()


def substract_shape(
    shape1: str, shape2: str, shapeout: str, name_shp: str, proj: int
) -> None:
    """
    shape 1 - shape 2 in shapeout/nameshp.shp
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    driver_1 = driver.Open(shape1, 0)
    driver_2 = driver.Open(shape2, 0)

    for feature1 in driver_1.GetLayer():
        geom_1 = feature1.GetGeometryRef()

    for feature2 in driver_2.GetLayer():
        geom_2 = feature2.GetGeometryRef()

    newgeom = geom_1.Difference(geom_2)

    # -----------------
    # -- Create output file
    try:
        output = driver.CreateDataSource(shapeout)
    except ValueError as exc:
        raise Exception("Could not create output datasource " + str(shapeout)) from exc

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(proj)

    new_layer = output.CreateLayer(name_shp, geom_type=ogr.wkbPolygon, srs=srs)
    if new_layer is None:
        raise Exception("Could not create output layer")

    new_layer.CreateField(ogr.FieldDefn("FID", ogr.OFTInteger))
    new_layer_def = new_layer.GetLayerDefn()
    feature = ogr.Feature(new_layer_def)
    feature.SetGeometry(newgeom)
    newgeom.Destroy()
    new_layer.CreateFeature(feature)

    output.Destroy()


def create_raster_footprint(
    tile_path: str, common_vec_mask: str, padding_size: int = 0, erode_unit: int = 4
) -> str:
    """Return a shapefile containing an eroded raster's footprint"""
    common_vec_mask_tmp = common_vec_mask.replace(".shp", "_tmp.shp")
    vf.keep_biggest_area(tile_path.replace(".tif", ".shp"), common_vec_mask_tmp)

    res = get_raster_resolution(tile_path)
    if res is None:
        raise ValueError(f"Can't read {tile_path}")
    x_res = res[0]
    vf.erode_or_dilate_shapefile(
        infile=common_vec_mask_tmp,
        outfile=common_vec_mask,
        buffdist=-(erode_unit + padding_size) * x_res,
    )
    driver = ogr.GetDriverByName("ESRI Shapefile")
    driver.DeleteDataSource(common_vec_mask_tmp)
    return common_vec_mask


def is_intersect(shp1: str, shp2: str) -> bool:
    """
    IN :
        shp1,shp2 : 2 tile's envelope (only one polygon by shapes)
    OUT :
        intersect : true or false
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    data_source_1 = driver.Open(shp1, 0)
    data_source_2 = driver.Open(shp2, 0)

    layer1 = data_source_1.GetLayer()
    for feature1 in layer1:
        geom1 = feature1.GetGeometryRef()
    layer2 = data_source_2.GetLayer()
    for feature2 in layer2:
        geom2 = feature2.GetGeometryRef()

    intersection = geom1.Intersection(geom2)
    if intersection.GetArea() != 0:
        return True
    return False


def get_shape_extent(shape: str) -> Envelope:
    """Get vector file extent and return the envelope geometry object
    Parameters
    ----------
    shape:
        the input shapefile
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    data_source = driver.Open(shape, 0)
    layer = data_source.GetLayer()
    for feature in layer:
        geom = feature.GetGeometryRef()
    out_env: tuple[float, float, float, float] = geom.GetEnvelope()
    return out_env  # [min_x, max_x, min_y, max_y]


def erode_inter(
    current_tile: Tile,
    next_tile: Tile,
    intersection: str,
    buff: float | int,
    proj: int,
) -> bool:
    """Erode the given intersection between the two given tiles"""
    x_origin, y_origin = current_tile.get_origin()
    x_next_origin, y_next_origin = next_tile.get_origin()
    extent = get_shape_extent(intersection)

    if y_origin == y_next_origin and x_origin != x_next_origin:  # left priority
        min_x = extent[0]
        max_x = extent[1] - buff
        min_y = extent[2]
        max_y = extent[3]

    elif y_origin != y_next_origin and x_origin == x_next_origin:  # upper priority
        min_x = extent[0]
        max_x = extent[1]
        min_y = extent[2] + buff
        max_y = extent[3]

    else:
        return False

    vf.remove_shape(intersection)
    path_folder = "/".join(
        intersection.split("/")[0 : len(intersection.split("/")) - 1]
    )
    create_shape(
        min_x,
        min_y,
        max_x,
        max_y,
        path_folder,
        intersection.split("/")[-1].replace(".shp", ""),
        proj,
    )
    return True


def diag(current_tile: Tile, next_tile: Tile) -> bool:
    """Return True if the tiles are diagonal to each other"""
    x_origin, y_origin = current_tile.get_origin()
    x_next_origin, y_next_origin = next_tile.get_origin()
    if not (y_origin == y_next_origin and x_origin != x_next_origin) and not (
        y_origin != y_next_origin and x_origin == x_next_origin
    ):
        return True
    return False


def priority_key(item: Tile) -> tuple[float, float]:
    """
    IN :
        item [list of Tile object]
    OUT :
        return tile origin (upper left corner) in order to manage tile's priority
    """
    return (-item.get_y(), item.get_x())  # upper left priority


def erode_diag(
    current_tile: Tile,
    next_tile: Tile,
    intersection: str,
    buff: float | int,
    tmp: str,
    proj: int,
) -> None:
    """Erode intersection in case of diagonal tiles"""
    x_origin, y_origin = current_tile.get_origin()  # tuile la plus prio
    x_next_origin, y_next_origin = next_tile.get_origin()
    extent = get_shape_extent(intersection)  # [min_x, max_x, min_y, max_y]

    if y_origin > y_next_origin and x_origin > x_next_origin:
        min_x = extent[1] - buff
        max_x = extent[1]
        min_y = extent[2]
        max_y = extent[3]

        vf.remove_shape(intersection)
        path_folder = "/".join(
            intersection.split("/")[0 : len(intersection.split("/")) - 1]
        )
        # erode intersection
        create_shape(
            min_x,
            min_y,
            max_x,
            max_y,
            path_folder,
            intersection.split("/")[-1].replace(".shp", ""),
            proj,
        )

        tmp_name = next_tile.get_name() + "_TMP"
        substract_shape(next_tile.get_priority_env(), intersection, tmp, tmp_name, proj)

        vf.remove_shape(next_tile.get_priority_env())
        vf.copy_shapefile(
            str(Path(tmp, tmp_name).with_suffix(".shp")), next_tile.get_priority_env()
        )
        vf.remove_shape(tmp + "/" + tmp_name + ".shp")

        tmp_name = current_tile.get_name() + "_TMP"
        substract_shape(
            current_tile.get_priority_env(),
            next_tile.get_priority_env(),
            tmp,
            tmp_name,
            proj,
        )

        vf.remove_shape(current_tile.get_priority_env())
        vf.copy_shapefile(
            tmp + "/" + tmp_name + ".shp", current_tile.get_priority_env()
        )
        vf.remove_shape(tmp + "/" + tmp_name + ".shp")

    if y_origin > y_next_origin and x_origin < x_next_origin:

        tmp_name = next_tile.get_name() + "_TMP"
        substract_shape(
            next_tile.get_priority_env(),
            current_tile.get_priority_env(),
            tmp,
            tmp_name,
            proj,
        )

        vf.remove_shape(next_tile.get_priority_env())
        vf.copy_shapefile(
            str(Path(tmp, tmp_name).with_suffix(".shp")), next_tile.get_priority_env()
        )
        vf.remove_shape(tmp + "/" + tmp_name + ".shp")


def gen_tile_env_prio(
    obj_list_tile: list[Tile], tmp_file: str, proj: int, padding_size: int
) -> None:
    """
    Generate the envelope with top priority
    Parameters
    ----------
    obj_list_tile
        List of tiles
    tmp_file
        Temporary files directory
    proj
        Projection
    padding_size
        Padding size (in pixels)

    """
    buff = 600  # offset in order to manage nodata in image's border

    obj_list_tile.reverse()
    list_shp = [
        create_raster_footprint(
            c_obj_list_tile.get_path(),
            tmp_file + "/" + c_obj_list_tile.get_name() + ".shp",
            padding_size,
        )
        for c_obj_list_tile in obj_list_tile
    ]

    for env, current_tile in zip(list_shp, obj_list_tile):
        env_prio = env.replace(".shp", "_PRIO.shp")
        current_tile.set_envelope(env)
        current_tile.set_priority_env(env_prio)
        vf.copy_shapefile(env, env_prio)

    for n_current_tile, c_tile in enumerate(obj_list_tile):
        current_tile_env = c_tile.get_envelope()
        for n_next_tile in range(1 + n_current_tile, len(obj_list_tile)):
            next_tile_env = obj_list_tile[n_next_tile].get_envelope()
            if is_intersect(current_tile_env, next_tile_env):

                inter_name = (
                    c_tile.get_name()
                    + "_inter_"
                    + obj_list_tile[n_next_tile].get_name()
                )
                intersection = vf.clip_vector_data(
                    c_tile.get_envelope(),
                    obj_list_tile[n_next_tile].get_envelope(),
                    tmp_file,
                    inter_name,
                )
                not_diag = erode_inter(
                    c_tile,
                    obj_list_tile[n_next_tile],
                    intersection,
                    buff,
                    int(proj),
                )
                if not_diag:
                    tmp_name = c_tile.get_name() + "_TMP"
                    substract_shape(
                        c_tile.get_priority_env(),
                        intersection,
                        tmp_file,
                        tmp_name,
                        int(proj),
                    )

                    vf.remove_shape(c_tile.get_priority_env())
                    vf.copy_shapefile(
                        str(Path(tmp_file, tmp_name).with_suffix(".shp")),
                        c_tile.get_priority_env(),
                    )
                    vf.remove_shape(tmp_file + "/" + tmp_name + ".shp")

    obj_list_tile.reverse()
    for n_current_tile, c_tile in enumerate(obj_list_tile):
        current_tile_env = c_tile.get_envelope()
        for n_next_tile in range(1 + n_current_tile, len(obj_list_tile)):
            next_tile_env = obj_list_tile[n_next_tile].get_envelope()
            if is_intersect(current_tile_env, next_tile_env):
                if diag(c_tile, obj_list_tile[n_next_tile]):
                    inter_name = (
                        c_tile.get_name()
                        + "_inter_"
                        + obj_list_tile[n_next_tile].get_name()
                    )
                    intersection = vf.clip_vector_data(
                        c_tile.get_envelope(),
                        obj_list_tile[n_next_tile].get_envelope(),
                        tmp_file,
                        inter_name,
                    )
                    erode_diag(
                        c_tile,
                        obj_list_tile[n_next_tile],
                        intersection,
                        buff,
                        tmp_file,
                        int(proj),
                    )
                else:
                    tmp_name = c_tile.get_name() + "_TMP"
                    substract_shape(
                        c_tile.get_priority_env(),
                        obj_list_tile[n_next_tile].get_priority_env(),
                        tmp_file,
                        tmp_name,
                        int(proj),
                    )

                    vf.remove_shape(c_tile.get_priority_env())
                    vf.copy_shapefile(
                        str(Path(tmp_file, tmp_name).with_suffix(".shp")),
                        c_tile.get_priority_env(),
                    )
                    vf.remove_shape(tmp_file + "/" + tmp_name + ".shp")


def generate_shape_tile(
    tiles: list[str], path_wd: str, output_path: str, proj: int, padding_size: int
) -> None:
    """generate tile's envelope with priority management

    Parameters
    ----------
    tiles : list
        list of tiles envelopes to generate
    path_wd : str
        working directory
    output_path : str
        iota2 output directory
    proj : int
        epsg code of target projection
    """
    path_out = str(Path(output_path) / "envelope")
    if not Path(path_out).exists():
        Path(path_out).mkdir()
    features_path = str(Path(output_path) / "features")

    cmask_name = "MaskCommunSL"
    for tile in tiles:

        if not (Path(features_path) / tile).exists():
            Path(features_path, tile).mkdir()
            Path(features_path, tile, "tmp").mkdir()
    common_directory = str(Path(path_out, "commonMasks"))
    if not Path(common_directory).exists():
        Path(common_directory).mkdir()

    common = [
        str(Path(features_path, Ctile, "tmp", cmask_name + ".tif")) for Ctile in tiles
    ]

    obj_list_tile = [
        Tile(current_tile, name) for current_tile, name in zip(common, tiles)
    ]
    obj_list_tile_sort = sorted(obj_list_tile, key=priority_key)

    tmp_file = str(Path(path_out) / "TMP")

    if path_wd:
        tmp_file = str(Path(path_wd) / "TMP")
    if not Path(tmp_file).exists():
        Path(tmp_file).mkdir()
    gen_tile_env_prio(obj_list_tile_sort, tmp_file, int(proj), padding_size)
    all_prio = fu.file_search_and(tmp_file, True, "_PRIO.shp")
    for prio_tile in all_prio:
        tile_name = prio_tile.split("/")[-1].split("_")[0] + ".shp"
        vf.copy_shapefile(prio_tile, str(Path(path_out, tile_name)))

    shutil.rmtree(tmp_file)
    shutil.rmtree(common_directory)
