#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
ConfigFile dataclass allows to manage the config file in a simpler way
should be refactored when pydantic is merged

Parameters dataclass contains parameters derived from config file and
iota2 constants

ModelLib enum is used to match between sklearn and pytorch workflows
"""

from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from typing import Self

from iota2.common import i2_constants
from iota2.configuration_files import read_config_file as rcf

# ==============================================================================
# model lib
# ==============================================================================


class ModelLib(Enum):
    "available libraries for statistics model"
    SKLEARN = 0
    PYTORCH = 1


@dataclass
class ConfigChain:  # pylint: disable=C0115
    output_path: Path
    ground_truth: Path
    list_tile: list[str]
    data_field: str
    remove_output_path: bool


@dataclass
class ConfigHyperparametersSolver:  # pylint: disable=C0115
    batch_size: list[int]
    learning_rate: list[float]


@dataclass
class ConfigDeepLearningParameters:  # pylint: disable=C0115, too-many-instance-attributes
    dl_name: str
    dl_parameters: dict
    model_optimization_criterion: str
    model_selection_criterion: str
    epochs: int
    weighted_labels: bool
    num_workers: int
    hyperparameters_solver: ConfigHyperparametersSolver
    dl_module: str | None  # representing a path
    restart_from_checkpoint: bool
    dataloader_mode: str
    enable_early_stop: bool
    epoch_to_trigger: int
    early_stop_patience: int
    early_stop_tol: int
    early_stop_metric: str
    additional_statistics_percentage: float
    adaptive_lr: dict

    @classmethod
    def from_dict(cls, dlp: dict) -> Self | None:
        """alternative constructor"""
        # construct only when dl_name is defined
        if "dl_name" not in dlp.keys():
            return None
        dlp["hyperparameters_solver"] = ConfigHyperparametersSolver(
            **dlp["hyperparameters_solver"]
        )
        return cls(**dlp)


@dataclass
class ConfigArgTrain:  # pylint: disable=C0115
    runs: int
    deep_learning_parameters: ConfigDeepLearningParameters | None
    sampling_validation: bool
    force_standard_labels: bool
    features_from_raw_dates: bool


@dataclass
class ConfigMultiRunFusion:  # pylint: disable=C0115
    merge_run: bool
    merge_run_method: str
    keep_runs_results: bool


@dataclass
class ConfigScikitModelsParameters:  # pylint: disable=C0115
    model_type: str
    standardization: bool
    cross_validation_parameters: dict
    cross_validation_grouped: bool
    cross_validation_folds: int
    keyword_arguments: dict

    @classmethod
    def from_dict(cls, skp: dict) -> Self | None:
        """alternative constructor"""
        # construct only when model_type is defined
        if skp["model_type"] is None:
            return None
        return cls(**skp)


@dataclass
class ConfigPythonDataManaging:  # pylint: disable=C0115, too-many-instance-attributes
    number_of_chunks: int
    max_nn_inference_size: int | None
    chunk_size_mode: str
    data_mode_access: tuple[bool, bool]
    fill_missing_dates: bool
    chunk_size_x: int
    chunk_size_y: int
    padding_size_x: int
    padding_size_y: int


@dataclass
class ConfigExternalFeatures:  # pylint: disable=C0115
    concat_mode: bool
    exogeneous_data: str | None  # which represents a path
    external_features_flag: bool
    functions: list
    module: str  # which represents a Path
    output_name: str
    no_data_value: int


@dataclass
class ConfigFile:
    """
    dataclass used to store and pass config file
    partial implementation with parameters needed for my step
    """

    chain: ConfigChain
    arg_train: ConfigArgTrain
    multi_run_fusion: ConfigMultiRunFusion
    scikit_models_parameters: ConfigScikitModelsParameters | None
    python_data_managing: ConfigPythonDataManaging
    external_features: ConfigExternalFeatures

    @classmethod
    def from_config_path(cls, path: str) -> Self:
        """shortcut to read from path directly"""
        cfg = rcf.ReadConfigFile(path)
        return cls.from_rcf(cfg)

    @classmethod
    def from_rcf(cls, cfg: rcf.ReadConfigFile) -> Self:
        """alternative constructor using iota2 rcf and getParam"""
        # perform some conversion on parameters
        chain = ConfigChain(
            output_path=Path(cfg.get_param("chain", "output_path")),
            ground_truth=Path(cfg.get_param("chain", "ground_truth")),
            list_tile=cfg.get_param("chain", "list_tile").split(" "),
            data_field=cfg.get_param("chain", "data_field"),
            remove_output_path=cfg.get_param("chain", "remove_output_path"),
        )
        deep_learning_parameters = ConfigDeepLearningParameters.from_dict(
            cfg.get_param("arg_train", "deep_learning_parameters")
        )
        arg_train = ConfigArgTrain(
            runs=cfg.get_param("arg_train", "runs"),
            deep_learning_parameters=deep_learning_parameters,
            sampling_validation=cfg.get_param("arg_train", "sampling_validation"),
            force_standard_labels=cfg.get_param("arg_train", "force_standard_labels"),
            features_from_raw_dates=cfg.get_param(
                "arg_train", "features_from_raw_dates"
            ),
        )
        multi_run_fusion = ConfigMultiRunFusion(
            merge_run=cfg.get_param("multi_run_fusion", "merge_run"),
            merge_run_method=cfg.get_param("multi_run_fusion", "merge_run_method"),
            keep_runs_results=cfg.get_param("multi_run_fusion", "keep_runs_results"),
        )
        scikit_models_parameters = ConfigScikitModelsParameters.from_dict(
            cfg.get_section("scikit_models_parameters")
        )
        python_data_managing = ConfigPythonDataManaging(
            **cfg.get_section("python_data_managing")
        )
        # specific to data_mode_access
        python_data_managing.data_mode_access = cfg.get_param(
            "python_data_managing", "data_mode_access"
        )
        external_features = ConfigExternalFeatures(
            **cfg.get_section("external_features")
        )
        # specific to function list
        external_features.functions = cfg.get_param("external_features", "functions")
        # return ConfigFile instance
        return cls(
            chain=chain,
            arg_train=arg_train,
            multi_run_fusion=multi_run_fusion,
            scikit_models_parameters=scikit_models_parameters,
            python_data_managing=python_data_managing,
            external_features=external_features,
        )


# ==============================================================================
# parameters
# ==============================================================================

I2_CONST = i2_constants.Iota2Constants()  # instantiate constant data class


def get_model_lib(config: ConfigFile) -> ModelLib:
    """get kind of model expressed by user in config

    pytorch and scikit workflows are mutually exclusive
    """
    use_scikit = (
        config.scikit_models_parameters is not None
        and config.scikit_models_parameters.model_type is not None
    )
    use_pytorch = config.arg_train.deep_learning_parameters is not None

    if use_scikit and use_pytorch:
        raise ValueError("can not use both scikit and pytorch")
    if not use_scikit and not use_pytorch:
        raise ValueError("must use at least scikit or pytorch")

    if use_scikit:
        modellib = ModelLib.SKLEARN
    else:
        modellib = ModelLib.PYTORCH

    return modellib


@dataclass
class Parameters:
    """parameters derived from config file and iota2 constants"""

    reference_data: Path  # path of copied and modified reference data
    reencoded_field: str  # field containing reencoded label
    model_lib: ModelLib  # scikit or pytorch

    @classmethod
    def from_config(cls, config: ConfigFile) -> Self:
        """alternative constructor using config file"""
        reference_data = config.chain.output_path / "reference_data.shp"
        return cls(
            reference_data=reference_data,
            reencoded_field=I2_CONST.re_encoding_label_name,
            model_lib=get_model_lib(config),
        )
