#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
This module contains all functions for testing known configuration parameters

These test must concern only typing

They can be overloaded by providing custom check.
"""
import logging
import string
from collections.abc import Callable
from inspect import signature
from pathlib import Path
from typing import Any

import iota2.common.file_utils as fut
from iota2.common import service_error as sErr
from iota2.typings.i2_types import FunctionNameWithParams, FunctionsMapping, PathLike

LOGGER = logging.getLogger("distributed.worker")


def get_param(  # type: ignore
    cfg,  # circular import: iota2.configuration_files.config_file.ConfigFile
    section: str,
    variable: str,
) -> Any:
    """
    Return the value of variable in the section from config
    file define in the init phase of the class.
    :param section: string name of the section
    :param variable: string name of the variable
    :return: the value of variable
    """

    if not hasattr(cfg, section):
        # not an osoError class because it should NEVER happened
        raise Exception("Section is not in the configuration file: " + str(section))

    obj_section = getattr(cfg, section)
    if not hasattr(obj_section, variable):
        # not an osoError class because it should NEVER happened
        raise Exception("Variable is not in the configuration file: " + str(variable))

    tmp_var = getattr(obj_section, variable)

    return tmp_var


def test_ifexists(path: PathLike) -> None:
    """
    Check if file exists, raise an exception otherwise

    Parameters
    ----------
    path:
        Path to file
    """
    if not Path(path).exists():
        raise sErr.DirError(path)


def test_shape_name(input_vector: PathLike) -> None:
    """
    Check if shapefile name is valid, raise an exception otherwise

    Parameters
    ----------
    input_vector:
        Name of shapefile (with extension)
    """
    input_vector = str(input_vector)
    avail_characters = string.ascii_letters
    if (first_character := Path(input_vector).name[0]) not in avail_characters:
        raise sErr.ConfigError(
            f"the file '{input_vector}' is containing a non-ascii letter at first "
            f"position in it's name : {first_character}"
        )

    ext = input_vector.rsplit(".", maxsplit=1)[-1]
    # If required test if gdal can open this file
    if ext not in (allowed_format := ["shp", "sqlite"]):
        raise sErr.ConfigError(
            f"{input_vector} is not in right format."
            f"\nAllowed format are {allowed_format}"
        )


def verify_code_path(code_path: PathLike) -> bool:
    """checks the code path
    returns False if code path refers to default external_code
    raises an error if code path refers to non-existing custom code file
    returns True if the code path refers to an existing custom code file"""
    code_path = str(code_path)
    if code_path is None:
        return False
    if code_path.lower() == "none":
        return False
    if len(code_path) < 1:
        return False
    if not Path(code_path).is_file():
        raise ValueError(f"Error: {code_path} is not a correct path")

    return True


def get_external_features_functions(
    module_path: str,
    function_names: list[str],
) -> tuple[FunctionsMapping, list[str]]:
    """search for functions given by name
    returns found_functions and not_found_functions
    module_path: path of custom external features
    function_names: names of the searched functions"""

    found_functions = {}  # dict mapping name -> function
    not_found_functions = []
    # tests validity of module path and if it contains external functions
    if verify_code_path(module_path):
        module = fut.verify_import(module_path)
        for function in function_names:
            try:
                func_name = getattr(module, function)
                found_functions[function] = func_name
            except AttributeError:
                not_found_functions.append(function)
    else:
        # all function names are not found
        not_found_functions = function_names
    return found_functions, not_found_functions


def search_external_features_function(
    user_defined_module_path: str,
    list_functions: list[FunctionNameWithParams],
) -> FunctionsMapping:
    """searches function first in user defined module
    and then fallbacks to iota2 module path"""

    # location of iota2 external_code
    in_source_module = str(
        Path(fut.get_iota2_project_dir()) / "iota2" / "common" / "external_code.py"
    )

    # extracts only function names as string
    function_names = [x[0] for x in list_functions]

    # looks for function in user defined module path
    found_functions, not_found_functions = get_external_features_functions(
        user_defined_module_path, function_names
    )

    # if some functions has not been found, search functions in source
    if not_found_functions:
        in_source_functions, not_in_source_functions = get_external_features_functions(
            in_source_module, not_found_functions
        )

        # all remaining function must be found in source
        if not_in_source_functions:
            raise AttributeError(
                f"functions {not_in_source_functions} not"
                "found in external code source\n"
                f"user_code_source {user_defined_module_path}\n"
                f"iota2 code source {in_source_module}\n"
            )

        # add in_source_functions to found_functions
        found_functions.update(in_source_functions)
    return found_functions


def type_check_functions_kwargs(
    functions: FunctionsMapping, flist: list[FunctionNameWithParams]
) -> None:
    """checks keword arguments typing
    functions: a dictionary mapping function name to associated callable
    flist: a list of tuples with function names / arguments"""
    for fname, fkwargs in flist:
        type_check_callable_kwargs(functions[fname], fkwargs)


def type_check_callable_kwargs(function: Callable, kwargs: dict[str, Any]) -> None:
    """checks if kwargs type match callable
    callable: the callable to check against
    kwargs: keyword arguments to check against callable"""
    sig = signature(function)
    for kwarg, value in kwargs.items():
        param = sig.parameters.get(kwarg)
        if not param:
            raise KeyError(f"function with signature {sig} has no {kwarg} argument")
        if param.annotation == param.empty:
            pass  # no indication given, could log a warning?
        elif param.annotation != type(value):
            raise (
                TypeError(
                    f"argument '{kwarg}' has type {type(value)} but {param.annotation} expected"
                )
            )
