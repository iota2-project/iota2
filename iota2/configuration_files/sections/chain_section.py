# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Definitions of the parameters corresponding to the chain section of the configuration file"""
import warnings
from collections.abc import Generator
from typing import ClassVar, Literal

from pydantic import Field, root_validator
from pydantic.fields import ModelField

from iota2.configuration_files.sections.cfg_utils import (
    ConfigError,
    ConfigParamWarning,
    FileParameter,
    Iota2ParamSection,
    PathParameter,
)


class Iota2ProjParam(str):
    """Class dedicated to the 'proj' parameter."""

    @classmethod
    def __get_validators__(cls) -> Generator:
        yield cls.validate

    @classmethod
    def validate(cls, value: str, field: ModelField) -> str:
        """Validate projection field."""
        if not value.startswith("EPSG:"):
            raise ConfigError(f"Parameter chain.{field.name} must starts with 'EPSG:'")
        try:
            _ = int(value.split(":")[-1])
        except ValueError as exc:
            raise ConfigError(
                f"EPSG code '{value.split(':')[-1]}' can not be casted to an integer"
            ) from exc
        return value


class ListTileParameter(str):
    """
    Class dedicated to the 'list_tile' parameter
    """

    @classmethod
    def __get_validators__(cls) -> Generator:
        """
        Pydantic validator generator
        """
        yield cls.validate

    @classmethod
    def validate(cls, value: str, field: ModelField) -> str:
        """
        Check if tiles are strings separated with spaces, raise an error otherwise
        """
        delimiter = " "
        forbiden_chars = [",", "|", ":", ";"]

        if not isinstance(value, str):
            raise ConfigError(
                f"The parameter '{field.name}' must be a string,"
                f" type detected : {type(value)}"
            )
        value = delimiter.join(value.split())
        for forbiden_char in forbiden_chars:
            if forbiden_char in value:
                raise ConfigError(
                    "'list_tile' configuration file parameter"
                    f" delimiter must be a whitespace. "
                    f"forbiden characters :'{forbiden_chars}'"
                )
        return value


class SpatialResolution(list):
    """https://field-idempotency--pydantic-docs.netlify.app/usage/types/#custom-data-types"""

    @classmethod
    def __get_validators__(cls) -> Generator:
        """
        Pydantic validator generator
        """
        yield cls.validate

    @classmethod
    def validate(
        cls, value: float | int | list | tuple, field: ModelField
    ) -> list[float]:
        """
        Check if resolution is correctly given, raise an error otherwise
        """
        if isinstance(value, str):
            raise ConfigError(
                f"{field.name} must be build as [spx, spy] or spx (square pixel)"
            )
        init_spatiale_res = []
        if isinstance(value, (float, int)):
            init_spatiale_res = [float(value), float(value)]
        elif isinstance(value, (list, tuple)):
            init_spatiale_res = [float(val) for val in value]
        if init_spatiale_res and len(init_spatiale_res) > 2:
            raise ConfigError(
                f"{field.name} must contains at most " "2 values : [spx, spy]"
            )
        return init_spatiale_res

    @classmethod
    def __modify_schema__(cls, field_schema: dict, field: ModelField | None) -> None:
        if field:
            field_schema["available_on_builders"] = [
                "I2Classification",
                "I2Obia",
                "I2FeaturesMap",
                "I2Regression",
                "I2FeaturesToGrid",
            ]
            field_schema["mandatory_on_builders"] = [
                "I2FeaturesToGrid",
            ]
            field_schema["doc_type"] = "list or scalar"
            field_schema["short_desc"] = "Output spatial resolution"
            field_schema["long_desc"] = (
                "The spatial resolution expected."
                "It can be provided as integer or float,"
                "or as a list containing two values"
                " for non squared resolution"
            )


class ChainSection(Iota2ParamSection):
    """Definition of the parameters that belong to the 'chain' section."""

    section_name: ClassVar[str] = "chain"

    output_statistics: bool = Field(
        True,
        short_desc="output_statistics",
        doc_type="bool",
        available_on_builders=["I2Classification"],
    )

    # sensors paths
    l5_path_old: PathParameter = Field(
        None,
        doc_type="str",
        short_desc="Absolute path to Landsat-5 images coming"
        " from old THEIA format (D*H*)",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    l8_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc="Absolute path to Landsat-8 images coming"
        "from new tiled THEIA data",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    l8_usgs_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`"
        ),
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    l8_usgs_optical_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`"
        ),
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    l8_usgs_thermal_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`"
        ),
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    l8_usgs_infrared_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to :doc:`Landsat-8 images coming from USGS data <landsat_8_usgs>`"
        ),
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    l8_path_old: PathParameter = Field(
        None,
        doc_type="str",
        short_desc="Absolute path to Landsat-8 images coming"
        " from old THEIA format (D*H*)",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    s2_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to Sentinel-2" " images (THEIA format)"),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
            "I2FeaturesToGrid",
        ],
    )
    s2_output_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to store preprocessed data " "in a dedicated directory"
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    s2_s2c_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to Sentinel-2" " images (Sen2Cor format)"),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    s2_s2c_output_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to store preprocessed data " "in a dedicated directory"
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    s2_l3a_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to Sentinel-2" " L3A images (THEIA format)"),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    s2_l3a_output_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to store preprocessed data " "in a dedicated directory"
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    s1_path: FileParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to Sentinel-1 configuration file"),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    user_feat_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to the user's features path"),
        long_desc=(
            "Absolute path to the user's features path. " "They must be stored by tiles"
        ),
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    # other params
    minimum_required_dates: int = Field(
        2,
        doc_type="int",
        short_desc=("required minimum number of available dates for each sensor"),
        available_on_builders=[
            "I2Classification",
            "I2Regression",
            "I2Obia",
            "I2FeaturesMap",
        ],
    )
    cloud_threshold: int = Field(
        0,
        doc_type="int",
        short_desc="Threshold to consider that a pixel is valid",
        long_desc=(
            "Indicates the threshold for a polygon to"
            " be used for learning. It use the validity count,"
            " which is incremented if a cloud, "
            "a cloud shadow or a saturated pixel is detected"
        ),
        available_on_builders=["I2Classification", "I2Regression", "I2Obia"],
    )
    first_step: str = Field(
        doc_type="str",
        short_desc="The step group name indicating where the chain start",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    last_step: str = Field(
        doc_type="str",
        short_desc="The step group name indicating where the chain ends",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    logger_level: Literal[
        "NOTSET", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"
    ] = Field(
        "INFO",
        doc_type="str",
        short_desc=(
            "Set the logger level: NOTSET, DEBUG," " INFO, WARNING, ERROR, CRITICAL"
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    region_path: FileParameter = Field(
        None,
        doc_type="str",
        short_desc=("Absolute path to a " "region vector file"),
        available_on_builders=["I2Classification", "I2Obia", "I2Regression"],
    )
    region_field: str = Field(
        "region",
        doc_type="str",
        short_desc=("The column name for region indicator in" "`region_path` file"),
        long_desc=(
            "This column in the database must contains string which"
            " can be converted into integers. For instance '1_2' "
            "does not match this condition.\n"
            "It is mandatory that the region identifiers are > 0."
        ),
        available_on_builders=["I2Classification", "I2Obia", "I2Regression"],
    )
    check_inputs: bool = Field(
        True,
        doc_type="bool",
        short_desc="Enable the inputs verification",
        long_desc=(
            "Enable the inputs verification."
            " It can take a lot of time for large dataset. "
            "Check if region intersect reference data for instance"
        ),
        available_on_builders=["I2Classification", "I2Regression", "I2Obia"],
    )

    compression_algorithm: Literal["LZW", "NONE", "ZSTD"] = Field(
        "ZSTD",
        doc_type="str",
        short_desc=(
            "Set the gdal compression algorithm to use: NONE, LZW, ZSTD (default)."
            "All rasters write with OTB will be compress with the chosen algorithm."
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
        ],
    )

    compression_predictor: Literal[1, 2] = Field(
        2,
        doc_type="int",
        short_desc=(
            "Set the predictor for LZW and ZSTD compression: 1 "
            "(no predictor), 2 (horizontal differencing, default)"
        ),
        long_desc=(
            "It has been noted that in some cases, once the features are "
            "written to disk, the raster file may be empty. If this is the"
            " case, please change the predictor to 1 or 3."
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
        ],
    )

    spatial_resolution: SpatialResolution = SpatialResolution()
    remove_output_path: bool = Field(
        True,
        doc_type="bool",
        short_desc=(
            "Before the launch of iota2, " "remove the content of `output_path`"
        ),
        long_desc=(
            "Before the launch of iota2, remove the content of `output_path`."
            " Only if the `first_step` is `init` and the folder name is valid "
        ),
        available_on_builders=[
            "I2Classification",
            "I2Regression",
            "I2Obia",
            "I2FeaturesMap",
        ],
    )
    output_path: str = Field(
        None,
        doc_type="str",
        short_desc="Absolute path to the output directory",
        long_desc=(
            "Absolute path to the output directory."
            "It is recommended to have one directory"
            " per run of the chain"
        ),
        mandatory_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
            "I2FeaturesToGrid",
        ],
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
            "I2FeaturesToGrid",
        ],
    )
    nomenclature_path: FileParameter = Field(
        doc_type="str",
        short_desc="Absolute path to the nomenclature description file",
        mandatory_on_builders=["I2Classification", "I2Obia"],
        available_on_builders=["I2Classification", "I2Obia"],
    )
    color_table: FileParameter = Field(
        doc_type="str",
        short_desc=(
            "Absolute path to the file that links " "the classes and their colours"
        ),
        mandatory_on_builders=["I2Classification", "I2Obia"],
        available_on_builders=["I2Classification", "I2Obia"],
    )
    list_tile: ListTileParameter = Field(
        doc_type="str",
        short_desc="List of tile to process, separated by space",
        mandatory_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
            "I2FeaturesToGrid",
        ],
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
            "I2FeaturesToGrid",
        ],
    )
    ground_truth: FileParameter = Field(
        doc_type="str",
        short_desc="Absolute path to reference data",
        mandatory_on_builders=["I2Classification", "I2Obia", "I2Regression"],
        available_on_builders=["I2Classification", "I2Obia", "I2Regression"],
    )
    data_field: str = Field(
        doc_type="str",
        short_desc=("Field name indicating classes " "labels in `ground_thruth`"),
        mandatory_on_builders=["I2Classification", "I2Obia", "I2Regression"],
        available_on_builders=["I2Classification", "I2Obia", "I2Regression"],
        long_desc=(
            "All the labels values must be different to 0.\n"
            "It is recommended to use a continuous range of values but it is not mandatory.\n"
            "Keep in mind that the final product type is detected according to "
            "the maximum label value.\n"
            "Try to keep values between 1 and 255 to avoid heavy products."
        ),
    )
    proj: Iota2ProjParam = Field(
        doc_type="str",
        short_desc="The projection wanted. Format EPSG:XXXX is mandatory",
        mandatory_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    grid: FileParameter = Field(
        None,
        doc_type="str",
        short_desc=("input grid to fit"),
        available_on_builders=["I2FeaturesToGrid"],
    )
    tile_field: str = Field(
        None,
        doc_type="str",
        short_desc=("column name in 'grid' containing tile's name."),
        available_on_builders=["I2FeaturesToGrid"],
    )
    rasters_grid_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("input grid to fit"),
        long_desc=(
            "all rasters must be in the same directory and must be named by tile ie : 'T31TCJ.tif'"
        ),
        available_on_builders=["I2FeaturesToGrid"],
    )
    features_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("input directory containing features as rasters"),
        available_on_builders=["I2FeaturesToGrid"],
    )

    output_features_pix_type: Literal[
        "uint8", "uint16", "int16", "uint32", "int32", "float", "double"
    ] = Field(
        "float",
        doc_type="str",
        short_desc=(
            "output features type choice among "
            "uint8/uint16/int16/uint32/int32/float/double."
        ),
        available_on_builders=["I2FeaturesToGrid"],
    )

    srtm_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Path to a directory containing srtm data"),
        available_on_builders=["I2FeaturesToGrid"],
    )
    worldclim_path: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Path to a directory containing world clim data"),
        available_on_builders=["I2FeaturesToGrid"],
    )
    from_vectordb_resampling_method: Literal[
        "near",
        "bilinear",
        "cubic",
        "cubicspline",
        "lanczos",
        "average",
        "rms",
        "mode",
        "max",
        "min",
        "med",
        "q1",
        "q3",
        "sum",
    ] = Field(
        "near",
        doc_type="str",
        short_desc=(
            "output features type choice among `gdalwarp.html#cmdoption-gdalwarp-r "
            "<https://gdal.org/programs/gdalwarp.html#cmdoption-gdalwarp-r>`_. "
            "Enabled if chain.grid is set"
        ),
        available_on_builders=["I2FeaturesToGrid"],
    )

    from_rasterdb_resampling_method: Literal["bco", "nn", "linear"] = Field(
        "nn",
        doc_type="str",
        short_desc=(
            "output features type choice among `gdalwarp.html#cmdoption-gdalwarp-r "
            "<https://www.orfeo-toolbox.org/CookBook/Applications/app_Superimpose.html>`_. "
            "Enabled if chain.rasters_grid_path is set"
        ),
        available_on_builders=["I2FeaturesToGrid"],
    )
    resampling_bco_radius: int = Field(
        2,
        doc_type="int",
        short_desc=("otb radius for bicubic interpolation."),
        long_desc=(
            "otb radius for bicubic interpolation. "
            "Enabled if 'nn' was set to bco in 'from_rasterdb_resampling_method' parameter"
        ),
        available_on_builders=["I2FeaturesToGrid"],
    )
    s1_dir: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=("Sentinel1 data directory"),
        long_desc=("The directory must contains all *.SAFE Sentinel1 data."),
        available_on_builders=["I2FeaturesToGrid"],
    )
    out_worldclim_dtype: str = Field(
        "float32",
        doc_type="np.dtype",
        short_desc=("output worlclim data type, ie : 'uint16', 'float32'."),
        available_on_builders=["I2FeaturesToGrid"],
    )
    out_worldclim_rescale_range: Literal["uint8", "uint16"] = Field(
        None,
        doc_type="np.dtype",
        short_desc=(
            "rescale worldclim data between 0 and max(np.dtype) at run time for RAM usage purpose."
        ),
        available_on_builders=["I2FeaturesToGrid"],
    )

    @root_validator(skip_on_failure=True)
    @classmethod
    def chain_section_root(cls, values: dict) -> dict:
        """Validate chain section."""
        if "I2FeaturesToGrid" in values["builders"]:
            if values["out_worldclim_rescale_range"] is not None:
                if (
                    values["out_worldclim_dtype"]
                    != values["out_worldclim_rescale_range"]
                ):
                    warnings.warn(
                        (
                            "parameter 'out_worldclim_dtype'' "
                            "different from 'out_worldclim_rescale_range'. "
                            "'out_worldclim_dtype' automatically set to "
                            f"{values['out_worldclim_rescale_range']}"
                        ),
                        ConfigParamWarning,
                    )
                    values["out_worldclim_dtype"] = values[
                        "out_worldclim_rescale_range"
                    ]

            from_rasters = bool(values["rasters_grid_path"])
            from_vector = bool(values["grid"])
            if not values["spatial_resolution"]:
                raise ConfigError("parameter 'spatial_resolution' is mandatory")
            if not from_rasters and not from_vector:
                raise ConfigError(
                    "parameters 'rasters_grid_path' and 'grid' are both empty. "
                    "Iota2 needs one of them to fit a grid."
                )
            if from_rasters and from_vector:
                raise ConfigError(
                    "parameters 'rasters_grid_path' and 'grid' are both set. "
                    "Only one of them must be set."
                )
            target_res = bool(values["spatial_resolution"])
            if from_vector:
                if values["tile_field"] is None:
                    raise ConfigError(
                        "'tile_field' parameter is mandatory with the grid shapefile usage."
                    )
                if not target_res:
                    raise ConfigError(
                        "if chain.grid is set, then chain.spatial_resolution is mandatory"
                    )
        return values
