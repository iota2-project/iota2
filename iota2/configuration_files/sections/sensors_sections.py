# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Definitions of the parameters corresponding to all the sensors sections of the configuration file
"""
from typing import ClassVar

from pydantic import Field, validator

from iota2.configuration_files.sections.cfg_utils import ConfigError, Iota2ParamSection

LONG_DESC_DATE = (
    "\n\nWARNING\n"
    "-------\n\n"
    "\n\nFor this parameter to be taken into account,"
    "the auto_date variable in the sensors_data_interpolation"
    "section must also be set to `False`:\n\n"
    "    .. code-block:: python\n\n"
    "        sensors_data_interpolation : \n"
    "        {\n"
    "          'auto_date':False,\n"
    "        }\n"
)

LONG_DESC_KEEP_BAND = (
    "\n\nWARNING\n"
    "-------\n\n"
    "\n\nFor this parameter to be taken into account,"
    "the extract_bands variable in the iota2_feature_extraction "
    "section must also be set to `True`:\n\n"
    "    .. code-block:: python\n\n"
    "        iota2_feature_extraction : \n"
    "        {\n"
    "          'extract_bands':True,\n"
    "        }\n"
)


class Landsat8Section(Iota2ParamSection):
    """Parameters of the section 'Landsat8'."""

    section_name: ClassVar[str] = "Landsat8"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    temporal_resolution: int = Field(
        16,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc="The first date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc="The last date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    keep_bands: list[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        long_desc=LONG_DESC_KEEP_BAND,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )


class Landsat8USGSOpticalSection(Iota2ParamSection):
    """Parameters of the section 'Landsat8_usgs_optical'."""

    section_name: ClassVar[str] = "Landsat8_usgs_optical"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    enable_sensor_gapfilling: bool = Field(
        True,
        doc_type="bool",
        short_desc="Enable or disable gapfilling for landsat 8 and 9 optical data",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    temporal_resolution: int = Field(
        16,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc="The first date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc="The last date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    keep_bands: list[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        long_desc=LONG_DESC_KEEP_BAND,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )


class Landsat8USGSThermalSection(Iota2ParamSection):
    """Parameters of the section 'Landsat8_usgs_thermal'."""

    section_name: ClassVar[str] = "Landsat8_usgs_thermal"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    enable_sensor_gapfilling: bool = Field(
        False,
        doc_type="bool",
        short_desc="Enable or disable gapfilling for landsat 8 and 9 thermal data"
        "(temperature and emissivity)",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    temporal_resolution: int = Field(
        16,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc="The first date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc="The last date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    keep_bands: list[str] = Field(
        ["B10", "EMIS"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        long_desc=LONG_DESC_KEEP_BAND,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )


class Landsat8USGSInfraredSection(Iota2ParamSection):
    """Parameters of the section 'Landsat8_usgs_infrared'."""

    section_name: ClassVar[str] = "Landsat8_usgs_infrared"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    enable_sensor_gapfilling: bool = Field(
        False,
        doc_type="bool",
        short_desc="Enable or disable gapfilling for landsat 8 and 9 IR data",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    temporal_resolution: int = Field(
        16,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc="The first date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc="The last date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    keep_bands: list[str] = Field(
        ["B10", "B11"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        long_desc=LONG_DESC_KEEP_BAND,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )


class Landsat8USGSSection(Iota2ParamSection):
    """Parameters of the section 'Landsat8_usgs'."""

    section_name: ClassVar[str] = "Landsat8_usgs"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    temporal_resolution: int = Field(
        16,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc="The first date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc="The last date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    keep_bands: list[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        long_desc=LONG_DESC_KEEP_BAND,
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )


class Sentinel2TheiaSection(Iota2ParamSection):
    """Parameters of the section 'Sentinel_2'."""

    section_name: ClassVar[str] = "Sentinel_2"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    temporal_resolution: int = Field(
        10,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc="The first date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc="The last date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    keep_bands: list[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        long_desc=LONG_DESC_KEEP_BAND,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )


class Sentinel2S2CSection(Iota2ParamSection):
    """Parameters of the section 'Sentinel_2_S2C'."""

    section_name: ClassVar[str] = "Sentinel_2_S2C"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    temporal_resolution: int = Field(
        10,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc="The first date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc="The last date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    keep_bands: list[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        long_desc=LONG_DESC_KEEP_BAND,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )


class Sentinel2L3ASection(Iota2ParamSection):
    """Parameters of the section 'Sentinel_2_L3A'."""

    section_name: ClassVar[str] = "Sentinel_2_L3A"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    temporal_resolution: int = Field(
        10,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc="The first date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc="The last date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    keep_bands: list[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        long_desc=LONG_DESC_KEEP_BAND,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )


class Landsat5OldSection(Iota2ParamSection):
    """Parameters of the section 'Landsat5_old'."""

    section_name: ClassVar[str] = "Landsat5_old"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    temporal_resolution: int = Field(
        10,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc="The first date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc="The last date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    keep_bands: list[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        long_desc=LONG_DESC_KEEP_BAND,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )


class Landsat8OldSection(Iota2ParamSection):
    """Parameters of the section 'Landsat8_old'."""

    section_name: ClassVar[str] = "Landsat8_old"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    temporal_resolution: int = Field(
        10,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc="The first date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc="The last date of interpolated image time series : YYYYMMDD format",
        long_desc=LONG_DESC_DATE,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    keep_bands: list[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        long_desc=LONG_DESC_KEEP_BAND,
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )


class UserFeatSection(Iota2ParamSection):
    """Parameters of the section 'userFeat'."""

    section_name: ClassVar[str] = "userFeat"
    arbo: str = Field(
        "/*",
        doc_type="str",
        short_desc="input folder hierarchy",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )
    patterns: str = Field(
        "ALT,ASP,SLP",
        doc_type="str",
        short_desc="key name for detect the input images",
        available_on_builders=["I2Classification", "I2Obia", "I2FeaturesMap"],
    )

    @validator("patterns")
    @classmethod
    def adaptive_lr_forbidden_values(cls, value: str) -> str:
        """Check forbidden char."""
        if " " in value:
            raise ConfigError(f"userFeat {value} must not contains whitespaces")
        return value
