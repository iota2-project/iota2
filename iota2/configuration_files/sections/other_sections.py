# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Definitions of the parameters corresponding to the all the other sections of the configuration file
"""
import importlib
from collections import Counter
from importlib import util  # noqa: F401, pylint: disable=unused-import
from importlib.abc import Loader
from pathlib import Path
from types import ModuleType
from typing import Any, ClassVar, Literal

from pydantic import BaseModel, Field, root_validator, validator

import iota2.common.i2_constants as i2_const
from iota2.common.file_utils import get_iota2_project_dir
from iota2.configuration_files.check_config_parameters import (
    search_external_features_function,
    type_check_functions_kwargs,
)
from iota2.configuration_files.sections.cfg_utils import (
    ConfigError,
    FileParameter,
    Iota2ParamSection,
)

I2_CONST = i2_const.Iota2Constants()


def check_module_path(code_path: str) -> bool:
    """Ensure the path is correct."""
    if code_path is None:
        return False
    if code_path.lower() == "none":
        return False
    if len(code_path) < 1:
        return False
    if not Path(code_path).is_file():
        raise ValueError(f"Error: {code_path} is not a correct path")

    return True


def check_import(module_path: str) -> ModuleType:
    """Ensure the module can be imported."""
    spec = importlib.util.spec_from_file_location(Path(module_path).stem, module_path)
    assert spec
    assert isinstance(spec.loader, Loader)
    module = importlib.util.module_from_spec(spec)
    assert module
    spec.loader.exec_module(module)
    return module


class BuilderSection(BaseModel):
    """Parameters of the section 'builders'."""

    section_name: ClassVar[str] = I2_CONST.i2_builders_section
    avail_builders: ClassVar[tuple[str, ...]] = (
        "I2Classification",
        "I2FeaturesMap",
        "I2FeaturesToGrid",
        "I2Obia",
        "I2Regression",
    )
    builders_paths: list[str] = Field(
        None,
        doc_type="str",
        short_desc="The path to user builders",
        long_desc=(
            "If not indicated, the iota2 source directory"
            " is used: */iota2/sequence_builders/"
        ),
        available_on_builders=(
            "I2Classification",
            "I2FeaturesMap",
            "I2FeaturesToGrid",
            "I2Obia",
            "I2Regression",
        ),
    )

    builders_class_name: list[str] = Field(
        ["I2Classification"],
        doc_type="list",
        short_desc="The name of the class defining the builder",
        long_desc=(
            "Available builders are : 'I2Classification', "
            "'I2FeaturesMap' and 'I2Obia'"
        ),
        available_on_builders=(
            "I2Classification",
            "I2FeaturesMap",
            "I2FeaturesToGrid",
            "I2Obia",
            "I2Regression",
        ),
    )

    @validator("builders_class_name")
    @classmethod
    def builders_compatibility(cls, builders_list: list[str]) -> list[str]:
        """Check builders compatibilty."""
        # common rules
        # A builder can't be instanciate twice
        for counter_module, occurs in Counter(builders_list).items():
            if occurs != 1:
                raise ValueError(
                    f"module '{counter_module}' cannot be used twice or more."
                    " Please check builders in iota2 configuration file"
                )
        # about feature's map
        if "I2FeaturesMap" in builders_list and "I2Classification" in builders_list:
            raise ValueError(
                "I2FeaturesMap and I2Classification are incompatible "
                "builders. Please check builders in iota2 configuration file"
            )
        return builders_list


class TaskRetry(Iota2ParamSection):
    """Parameters of the section 'task_retry_limits'."""

    section_name: ClassVar[str] = "task_retry_limits"

    allowed_retry: int = Field(
        0,
        doc_type="int",
        short_desc="allow dask to retry a failed job N times",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    maximum_ram: float = Field(
        16.0,
        doc_type="float",
        short_desc="the maximum amount of RAM available. (gB)",
        long_desc=(
            "the amount of RAM will be doubled if the task"
            " is killed due to ram overconsumption "
            "until maximum_ram or allowed_retry are reach"
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    maximum_cpu: int = Field(
        4,
        doc_type="int",
        short_desc="the maximum number of CPU available",
        long_desc=(
            "the amount of cpu will be doubled if the task "
            "is killed due to ram overconsumption "
            "until maximum_cpu or allowed_retry are reach"
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )


class MultiRunFusionSection(Iota2ParamSection):
    """Definition of the parameters that belong to the 'multi_run_fusion' section."""

    section_name: ClassVar[str] = "multi_run_fusion"

    merge_run: bool = Field(
        False,
        doc_type="bool",
        short_desc=(
            "Enable the fusion of "
            "regression mode, merging all "
            "run in a unique result."
        ),
        available_on_builders=["I2Regression"],
    )

    merge_run_method: Literal["mean", "median"] = Field(
        "mean",
        doc_type="str",
        short_desc=("Indicate the fusion of regression method: 'mean' or 'median'"),
        long_desc=(
            "In addition to the regression fusion map, a confidence map is also produced."
            " If the merging method is the mean then the method used to calculate "
            "the confidence map will be the standard deviation,"
            "if the median is chosen to merge the maps from the different runs then "
            "the method used to calculate the confidence map will be th median absolute deviation."
        ),
        available_on_builders=["I2Regression"],
    )

    keep_runs_results: bool = Field(
        True,
        doc_type="bool",
        short_desc="",
        long_desc=(" "),
        available_on_builders=["I2Regression"],
    )

    fusionof_all_samples_validation: bool = Field(
        False,
        doc_type="bool",
        short_desc=(
            "Enable the use of all reference data " "to evaluate the fusion raster"
        ),
        long_desc=(
            "If the fusion mode is enabled, "
            "enable the use of all reference data samples"
            " for validation"
        ),
        available_on_builders=["I2Regression"],
    )

    merge_run_ratio: float = Field(
        0.1,
        doc_type="float",
        short_desc=(
            "Percentage of samples to use " "in order to evaluate the fusion raster"
        ),
        available_on_builders=["I2Regression"],
    )


class SensorsDataInterpSection(Iota2ParamSection):
    """Parameters of the section 'sensors_data_interpolation'."""

    section_name: ClassVar[str] = "sensors_data_interpolation"
    auto_date: bool = Field(
        True,
        doc_type="bool",
        short_desc=("Enable the use of " "`start_date` and `end_date`"),
        long_desc=(
            "If True, iota2 will automatically guess the first"
            " and the last interpolation date. Else, `start_date` "
            "and `end_date` of each sensors will be used"
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    write_outputs: bool = Field(
        False,
        doc_type="bool",
        short_desc="write temporary files",
        long_desc=(
            "Write the time series before and after gapfilling,"
            " the mask time series, and also the feature"
            " time series. This option required a large amount of"
            " free disk space."
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )
    use_additional_features: bool = Field(
        False,
        doc_type="bool",
        short_desc="enable the use of additional features",
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
        ],
    )
    use_gapfilling: bool = Field(
        True,
        doc_type="bool",
        short_desc=("enable the use of gapfilling " "(clouds/temporal interpolation)"),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
            "I2Regression",
        ],
    )


class I2FeatureExtractionSection(Iota2ParamSection):
    """Parameters of the section 'iota2_feature_extraction'."""

    section_name: ClassVar[str] = "iota2_feature_extraction"
    copy_input: bool = Field(
        True,
        doc_type="bool",
        short_desc="use spectral bands as features",
        available_on_builders=["I2Classification", "I2Regression"],
    )
    rel_refl: bool = Field(
        False,
        doc_type="bool",
        short_desc="compute relative reflectances by the red band",
        available_on_builders=["I2Classification", "I2Regression"],
    )
    keep_duplicates: bool = Field(
        True,
        doc_type="bool",
        short_desc=(
            "use 'rel_refl' can generate duplicated feature "
            "(ie: NDVI), set to False remove these duplicated features"
        ),
        available_on_builders=["I2Classification", "I2Regression"],
    )
    extract_bands: bool = Field(
        False,
        doc_type="bool",
        short_desc="",
        available_on_builders=["I2Classification", "I2Regression"],
    )
    acor_feat: bool = Field(
        False,
        doc_type="bool",
        short_desc="Apply atmospherically corrected features",
        long_desc=(
            "Apply atmospherically corrected features"
            "as explained at : "
            "http://www.cesbio.ups-tlse.fr/multitemp/?p=12746"
        ),
        available_on_builders=["I2Classification", "I2Regression"],
    )


class ExternalFeaturesSection(Iota2ParamSection):
    """Parameters of the section 'external_features'."""

    section_name: ClassVar[str] = "external_features"
    external_features_flag: bool = Field(
        False,
        doc_type="bool",
        short_desc="enable the external features mode",
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    module: FileParameter = Field(
        str(Path(get_iota2_project_dir()) / "iota2" / "common" / "external_code.py"),
        doc_type="str",
        short_desc="absolute path for user source code",
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )

    functions: list | str = Field(
        None,
        doc_type="str/list",
        short_desc="function list to be used to compute features",
        long_desc=(
            "Can be a string of space-separated function names"
            "Can be a list of either strings of function name"
            "or lists of one function name and one argument mapping"
        ),
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    concat_mode: bool = Field(
        True,
        doc_type="bool",
        short_desc="enable the use of all features",
        long_desc=(
            "if disabled, only external features are" " used in the whole processing"
        ),
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    output_name: str = Field(
        None,
        doc_type="str",
        short_desc="temporary chunks are written using this name as prefix",
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    no_data_value: int = Field(
        -10000,
        doc_type="int",
        short_desc=(
            "value considered as no_data in "
            "features map mosaic ('I2FeaturesMap' builder name)"
        ),
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    exogeneous_data: str = Field(
        None,
        doc_type="str",
        short_desc=(
            "Path to a Geotiff file containing additional "
            "data to be used in external features"
        ),
        long_desc=(
            "If the =exogeneous_data= contains '$TILE', "
            "it will be replaced by the tile name being processed."
            "If you want to reproject your data on given tiles, "
            "you can use the =split_raster_into_tiles.py= command line tool.\n\n"
            "Usage: =split_raster_into_tiles.py --help=."
        ),
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )

    @validator("module", always=True, pre=True)
    @classmethod
    def replace_none(cls, param: None | str) -> str:
        """
        Replace None with external code file path
        """
        if param is None:
            param = str(
                Path(get_iota2_project_dir()) / "iota2" / "common" / "external_code.py"
            )
        return param

    @validator("functions", always=True)
    @classmethod
    def convert_functions(
        cls, param: None | str | list
    ) -> None | list[tuple[str, dict]]:
        """
        Convert the 'functions' parameter to the right format.

        Take a config external_features "functions" parameter and converts it
        to unified representation. raises a ValueError if type does not fit
        param: config param for external_features functions parameter.
        """

        def process_str_param(str_param: str) -> list[tuple[str, dict]]:
            """Processes the case where param is a string of space-separated function names."""
            return [(function_name, {}) for function_name in str_param.split(" ")]

        def process_list_param(list_parm: list) -> list[tuple[str, dict]]:
            """Processes the case where param is a list of functions with optional parameters."""
            list_out_param: list[tuple[str, dict]] = []
            for elt in list_parm:
                if isinstance(elt, str):  # only function name (no parameters)
                    list_out_param.append((elt, {}))
                elif isinstance(elt, list):  # function name plus parameters
                    fn_name = ""
                    kwargs = {}
                    if len(elt) == 0:
                        raise ValueError("please provide at least function name")
                    if len(elt) >= 1:
                        fn_name = elt[0]
                    if len(elt) == 2:
                        kwargs = elt[1]
                    if len(elt) > 2:
                        raise ValueError(
                            "only function name and parameters" f"allowed, found {elt}"
                        )
                    if not isinstance(fn_name, str):
                        raise ValueError(
                            "expected function name to be a string" f"not {fn_name}"
                        )
                    if not isinstance(kwargs, dict):
                        raise ValueError("expected kwargs to be a dict" f"not {kwargs}")
                    list_out_param.append((fn_name, dict(kwargs)))
                else:
                    raise ValueError(
                        f"invalid element {elt}," "expecting function name and kwargs"
                    )
            return list_out_param

        if not param:  # empty string or list
            return None

        if isinstance(param, str):
            # param is a string with space-separated function names
            out_param = process_str_param(param)
        elif isinstance(param, list):  # functions with parameters
            out_param = process_list_param(param)
        else:
            raise ValueError(f"invalid parameter functions {param}")
        return out_param

    @root_validator(skip_on_failure=True)
    @classmethod
    def enable_external_features(cls, values: dict) -> dict:
        """Automatically enable external features on conditions."""

        def check_code_path(code_path: str) -> bool:
            if code_path is None:
                return False
            if code_path.lower() == "none":
                return False
            if len(code_path) < 1:
                return False
            if not Path(code_path).is_file():
                raise ValueError(f"Error: {code_path} is not a correct path")

            return True

        if values["functions"] is None:
            return values
        module_path = values["module"]
        in_source_module = str(
            Path(get_iota2_project_dir()) / "iota2" / "common" / "external_code.py"
        )
        list_functions = [f_name for f_name, _ in values["functions"]]
        not_found_functions = []
        flag = False

        if list_functions is None:
            flag = False

        module_path_valid = check_code_path(module_path)
        if module_path_valid:
            module = check_import(module_path)
            for function in list_functions:
                try:
                    getattr(module, function)
                except AttributeError:
                    not_found_functions.append(function)

        else:
            not_found_functions = list_functions
        not_in_source_functions = []
        if not_found_functions or not module_path_valid:
            source_module = check_import(in_source_module)
            for function in not_found_functions:
                try:
                    getattr(source_module, function)
                except AttributeError:
                    not_in_source_functions.append(function)

        if len(not_in_source_functions) > 0:
            raise AttributeError(
                f"functions {' '.join(not_in_source_functions)} not"
                "found in external code source\n"
                f"user_code_source {module_path}\n"
                f"iota2 code source {in_source_module}"
            )
        flag = True

        found_functions = search_external_features_function(
            module_path, values["functions"]
        )
        type_check_functions_kwargs(found_functions, values["functions"])
        values["external_features_flag"] = flag

        return values


class PythonDataManagingSection(Iota2ParamSection):
    """Parameters of the section 'python_data_managing'."""

    section_name: ClassVar[str] = "python_data_managing"
    data_mode_access: Literal["gapfilled", "raw", "both"] = Field(
        "gapfilled",
        doc_type="str",
        short_desc="choose which data can be accessed in custom features",
        long_desc=(
            "Three values are allowed:\n\n"
            "- gapfilled: give access only the gapfilled data\n\n"
            "- raw: gives access only the original raw data\n\n"
            "- both: provides access to both data\n\n"
            "..Notes:: Data are spatialy resampled, these parameters"
            " concern only temporal interpolation"
        ),
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    fill_missing_dates: bool = Field(
        False,
        doc_type="bool",
        short_desc="fill raw data with no data if dates are missing",
        long_desc=(
            "If raw data access is enabled, this option "
            "considers all unique dates for all tiles and identify"
            " which dates are missing for each tile."
            " A missing date is filled using a no data constant value."
            "Cloud or saturation are not corrected, but masks are provided"
            " Masks contain three value: 0 for valid data, 1 for cloudy "
            "or saturated pixels, 2 for a missing date"
        ),
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    number_of_chunks: int = Field(
        50,
        doc_type="int",
        short_desc="the expected number of chunks",
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    max_nn_inference_size: int = Field(
        None,
        doc_type="int",
        short_desc="maximum batch inference size",
        long_desc=(
            "Involved if a neural network inference is performed. "
            "If not set (None), the inference size will be the same"
            " as the one used during the learning stage"
        ),
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    chunk_size_x: int = Field(
        50,
        doc_type="int",
        short_desc="number of cols for one chunk",
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    chunk_size_y: int = Field(
        50,
        doc_type="int",
        short_desc="number of rows for one chunk",
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    chunk_size_mode: str = Field(
        "split_number",
        doc_type="str",
        short_desc=("The chunk split mode, currently the choice is 'split_number'"),
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    padding_size_x: int = Field(
        0,
        doc_type="int",
        short_desc="The padding for chunk",
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )
    padding_size_y: int = Field(
        0,
        doc_type="int",
        short_desc="The padding for chunk",
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )

    @validator("chunk_size_mode")
    @classmethod
    def check_forbidden_values(cls, param: tuple) -> tuple:
        """Check the parameter 'chunk_size_mode'."""
        if param not in ("split_number", "user_fixed"):
            raise ConfigError(
                "parameter 'chunk_size_mode' must be equal to 'split_number' or 'user_fixed'"
            )
        return param

    @validator("data_mode_access", always=True)
    @classmethod
    def convert_mode_access(cls, param: str) -> tuple[bool, bool]:
        """Convert the parameter 'data_mode_access' to tuple of booleans."""
        if param == "raw":
            out_param = (False, True)
        elif param == "gapfilled":
            out_param = (True, False)
        elif param == "both":
            out_param = (True, True)
        else:
            raise ValueError(
                f"{param} is not a correct value for " "parameter data_mode_access"
            )
        return out_param


class SciKitSection(Iota2ParamSection):
    """Parameters of the section 'external_features'."""

    section_name: ClassVar[str] = "scikit_models_parameters"
    model_type: str = Field(
        None,
        doc_type="str",
        short_desc="machine learning algorthm’s name",
        long_desc=(
            "Models comming from scikit-learn are use "
            "if scikit_models_parameters.model_type is "
            "different from None. More "
            "informations about how to use "
            "scikit-learn is available at iota2 "
            "and scikit-learn machine learning algorithms."
        ),
        available_on_builders=["I2Classification", "I2Regression"],
    )
    cross_validation_folds: int = Field(
        5,
        doc_type="int",
        short_desc="the number of k-folds",
        available_on_builders=["I2Classification", "I2Regression"],
    )
    cross_validation_grouped: bool = Field(
        False,
        doc_type="bool",
        short_desc="",
        available_on_builders=["I2Classification", "I2Regression"],
    )
    standardization: bool = Field(
        True,
        doc_type="bool",
        short_desc="",
        available_on_builders=["I2Classification", "I2Regression"],
    )
    cross_validation_parameters: dict = Field(
        {}, doc_type="dict", short_desc="", available_on_builders=["I2Classification"]
    )
    keyword_arguments: dict = Field(
        default={},
        doc_type="dict",
        short_desc="keyword arguments to be passed to model",
        long_desc="keyword arguments to be passed to model",
        available_on_builders=["I2Classification", "I2Regression"],
    )


class OBIASection(Iota2ParamSection):
    """Parameters of the section 'obia'."""

    section_name: ClassVar[str] = "obia"
    obia_segmentation_path: FileParameter = Field(
        None,
        doc_type="str",
        short_desc="filename for input segmentation",
        long_desc=(
            "If parameter is None then a segmentation for "
            "each tile is processed using SLIC algorithm"
        ),
        available_on_builders=["I2Obia"],
    )
    buffer_size: int = Field(
        None,
        doc_type="int",
        short_desc="define the working size batch in number of pixels",
        long_desc=(
            "this parameter is used to avoid memory issue."
            "In case of a large temporal series,i.e one year of "
            "Sentinel2 images a recommended size is 2000."
            "For lower number of date, the buffer size can be increased."
            "If buffer_size is larger than the image size, the whole "
            "image will be processed in one time."
        ),
        available_on_builders=["I2Obia"],
    )
    region_priority: list = Field(
        None,
        doc_type="list",
        short_desc="define an order for region intersection",
        long_desc=(
            "if a list is provided, the list order is used instead"
            " of the numeric order."
            "This option can be used in case of very unbalanced region size."
        ),
        available_on_builders=["I2Obia"],
    )
    full_learn_segment: bool = Field(
        False,
        doc_type="bool",
        short_desc="enable the use of entire segment for learning",
        long_desc=(
            "if True: keep each segment which intersect the learning"
            " samples. If False, the segments are clipped with learning"
            " polygon shape"
        ),
        available_on_builders=["I2Obia"],
    )
    stats_used: list = Field(
        ["mean"],
        doc_type="list",
        short_desc="list of stats used for train and classification",
        long_desc=(
            "this list accepts only five values:"
            " mean, count, min, max, std\n"
            "The choice of statistics used should be considered in "
            "relation to the number of dates used."
            "Because of the constraints on vector formats, one must think"
            " about the number of features this creates:"
            " nb_stats_choosen * nb_bands * nb_dates. "
            "Too many spectral bands can cause an"
            " error in the execution of the string."
        ),
        available_on_builders=["I2Obia"],
    )


class SlurmSection(Iota2ParamSection):
    """Parameters of the section 'slurm'."""

    section_name: ClassVar[str] = "slurm"
    account: str = Field(
        None,
        doc_type="str",
        short_desc="Feed the sbatch parameter 'account'",
        long_desc=(
            "The section 'slurm' is only available once the "
            "`Slurm <https://slurm.schedmd.com/documentation.html>`_"
            " orchestrator is involved in jobs submission."
        ),
        available_on_builders=[
            "I2Classification",
            "I2Obia",
            "I2FeaturesMap",
        ],
    )


class PreTrainedModel(Iota2ParamSection):
    """Parameters for the 'pretrained_model' section."""

    section_name: ClassVar[str] = "pretrained_model"
    function: str = Field(
        None,
        doc_type="str",
        short_desc="Predict function name",
        long_desc="This function must have the imposed signature."
        " It not accept any others parameters. "
        "All model dedicated parameters must be stored alongside the model.",
    )
    module: str = Field(
        None,
        doc_type="str",
        short_desc="Absolute path to the python module",
        long_desc=(
            "The python module must contains the predict function"
            " It must handle all the potential dependencies and import "
            "related to the correct model instanciation"
        ),
    )
    model: str = Field(
        None,
        doc_type="str",
        short_desc="Serialized object containing the model",
        long_desc=(
            "In the configuration file, the mandatory "
            "keys $REGION and $SEED must be present"
            " as they are replaced by iota2."
            " In case of only one region, the region value is set to 1."
            " Look at the documentation about the model constraint."
        ),
    )
    boundary_buffer: list = Field(
        None, doc_type="list", short_desc="List of boundary buffer size"
    )
    mode: str = Field(
        None,
        doc_type="str",
        short_desc="Algorythm nature (classification or regression)",
        long_desc=(
            "The python module must contains the predict function"
            " It must handle all the potential dependencies and import "
            "related to the correct model instanciation"
        ),
    )

    @root_validator(skip_on_failure=True)
    @classmethod
    def ensure_predict_function(cls, values: dict[str, Any]) -> dict[str, Any] | None:
        """Check if the predict function exists."""
        if values["function"] is None:
            return values
        module = values["module"]
        is_valid = check_module_path(module)
        if is_valid:
            in_module = check_import(module)
            try:
                getattr(in_module, values["function"])
                values["function"] = [(values["function"], {})]
            except AttributeError as err:
                raise AttributeError(
                    f"{values['function']} not found in {module}"
                ) from err
        else:
            raise ConfigError(f"{module} is not a valid external module")
        return values
