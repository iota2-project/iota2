# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module that groups all the definitions of the simplification parameters"""
from typing import ClassVar

from pydantic import Field

from iota2.configuration_files.sections.cfg_utils import (
    FileParameter,
    Iota2ParamSection,
)


class SimplificationSection(Iota2ParamSection):
    """Parameters of the section 'simplification'."""

    section_name: ClassVar[str] = "simplification"

    umc1: int = Field(
        None,
        doc_type="int",
        short_desc="MMU for the first regularization",
        long_desc=(
            "It is an interface of parameter '-st' of gdal_sieve.py function."
            " If None, classification is not regularized"
        ),
        available_on_builders=["I2Obia"],
    )
    umc2: int = Field(
        None,
        doc_type="int",
        short_desc="MMU for the second regularization",
        long_desc=(
            "OSO-like vectorization process requires 2 successive "
            "regularization, if you need a single regularization, "
            "let this parameter to None"
        ),
        available_on_builders=["I2Obia"],
    )
    inland: FileParameter = Field(
        None,
        doc_type="str",
        short_desc="inland water limit shapefile",
        long_desc=(
            "to vectorize only inland waters, and not unnecessary sea water areas"
        ),
        available_on_builders=["I2Obia"],
    )
    rssize: int = Field(
        20,
        doc_type="int",
        short_desc=("Resampling size of input classification raster (projection unit)"),
        long_desc=(
            "OSO-like vectorization requires a resampling step in order "
            "to regularize and decrease raster polygons number, "
            "If None, classification is not resampled"
        ),
        available_on_builders=["I2Obia"],
    )
    gridsize: int = Field(
        None,
        doc_type="int",
        short_desc="number of lines and columns of the serialization process",
        long_desc=(
            "This parameter is useful only for large areas for which "
            "vectorization process can not be executed (memory limitation). "
            "By 'serialization', we mean parallel vectorization processes. "
            "If not None, regularized classification raster is splitted "
            "in gridsize x gridsize rasters"
        ),
        available_on_builders=["I2Obia"],
    )
    nomenclature: FileParameter = Field(
        None,
        doc_type="configuration file which describe nomenclature",
        short_desc="configuration file which describe nomenclature",
        long_desc=(
            "This configuration file includes code, color, description "
            "and vector field alias of each class\n\n"
            ".. code-block:: bash\n\n"
            "    Classes:\n"
            "    {\n"
            "            Level1:\n"
            "            {\n"
            '                    "Urbain":\n'
            "                    {\n"
            "                    code:100\n"
            '                    alias:"Urbain"\n'
            '                    color:"#b106b1"\n'
            "                    }\n"
            "                    ...\n"
            "            }\n"
            "            Level2:\n"
            "            {\n"
            '                   "Urbain dense":\n'
            "                   {\n"
            "                   code:1\n"
            '                   alias:"UrbainDens"\n'
            '                   color:"#ff00ff"\n'
            "                   parent:100\n"
            "                   }\n"
            "                   ...\n"
            "            }\n"
            "    }\n"
        ),
        available_on_builders=["I2Obia"],
    )
