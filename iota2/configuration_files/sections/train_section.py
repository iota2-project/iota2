# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Definitions of the parameters corresponding to the train section of the configuration file"""
import warnings
from collections import namedtuple
from collections.abc import Generator
from dataclasses import dataclass
from pathlib import Path
from typing import ClassVar, Literal

from pydantic import BaseModel, Field, root_validator, validator
from pydantic.fields import ModelField

from iota2.common.file_utils import verify_import
from iota2.configuration_files.check_config_parameters import verify_code_path
from iota2.configuration_files.sections.cfg_utils import (
    ConfigError,
    ConfigNotRecognisedParamWarning,
    FileParameter,
    Iota2ParamSection,
)
from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetwork


class HyperparametersSolver(BaseModel):
    """Parameter class to feed DeepLearningParameters."""

    batch_size: list[int] = [1000]
    learning_rate: list[float] = [1e-05]


class DeepLearningParameters(BaseModel):
    """Deep learning parameters definition."""

    dl_name: str | None = None
    dl_parameters: dict = {}
    model_optimization_criterion: Literal[
        "cross_entropy",
        "MSE",
        "MAE",
        "HuberLoss",
    ] = "cross_entropy"
    model_selection_criterion: Literal[
        "loss", "fscore", "oa", "kappa", "MAE", "MSE", "HuberLoss"
    ] = "loss"
    epochs: int = 100
    weighted_labels: bool = False
    num_workers: int = 1
    hyperparameters_solver: HyperparametersSolver = HyperparametersSolver()
    dl_module: FileParameter | None = None
    restart_from_checkpoint: bool = True
    dataloader_mode: Literal["stream", "full"] = "stream"
    enable_early_stop: bool = False
    epoch_to_trigger: int = 5
    early_stop_patience: int = 10
    early_stop_tol: float = 0.01
    early_stop_metric: Literal[
        "train_loss", "val_loss", "fscore", "oa", "kappa"
    ] = "val_loss"
    additional_statistics_percentage: float | None = None
    adaptive_lr: dict | None = None

    @validator("adaptive_lr")
    @classmethod
    def adaptive_lr_forbidden_values(cls, value: dict) -> dict:
        """Validate adaptive_lr parameter."""
        forbidden_values = ["optimizer", "mode"]
        error_mess = []
        for forbidden_value in forbidden_values:
            if forbidden_value in value:
                error_mess.append(
                    f"parameter '{forbidden_value}' " "is forbidden in adaptive_lr"
                )
        if error_mess:
            raise ConfigError(" and ".join(error_mess))
        return value

    @validator("additional_statistics_percentage")
    @classmethod
    def additional_statistics_percentage_range(cls, value: float) -> float:
        """Check if the range is in ]0;1]."""
        if value > 1.0 or value < 0:
            raise ConfigError(
                "Parameter 'additional_statistics_percentage' must be in : ]0;1]"
            )
        return value

    @root_validator(skip_on_failure=True)
    @classmethod
    def nn_root_val(cls, values: dict) -> dict:
        """Validate dl parameters."""
        nn_name, nn_module = values["dl_name"], values["dl_module"]
        if nn_module and nn_name is None:
            raise ConfigError(
                "External neural network module provided "
                "but 'dl_name' not set. Please set 'dl_name'"
                " parameter in iota2 configuration file"
            )
        if nn_module:
            verify_code_path(nn_module)
            try:
                external_nn_module = verify_import(nn_module)
            except TypeError as exception:
                raise ValueError from exception
            user_nn_class = getattr(external_nn_module, nn_name)
            if not issubclass(user_nn_class, Iota2NeuralNetwork):
                raise ConfigError(
                    f"the class {user_nn_class} must be "
                    "a subclass of 'Iota2NeuralNetwork'"
                )
        return values


class OtbClassifierParam(dict):
    """Parameters to the otb app TrainVectorClassifier."""

    @classmethod
    def __get_validators__(cls) -> Generator:
        yield cls.validate

    @classmethod
    def validate(cls, value: dict, field: ModelField) -> dict:
        """Check classifier options."""
        avail_keys = dict(
            [
                ("classifier.libsvm.k", str),
                ("classifier.libsvm.m", str),
                ("classifier.libsvm.c", float),
                ("lassifier.libsvm.gamma", float),
                ("classifier.libsvm.coef0", float),
                ("classifier.libsvm.degree", int),
                ("classifier.libsvm.nu", float),
                ("classifier.libsvm.opt", bool),
                ("classifier.libsvm.prob", bool),
                ("classifier.boost.t", str),
                ("classifier.boost.w", int),
                ("classifier.boost.r", float),
                ("classifier.boost.m", int),
                ("classifier.dt.max", int),
                ("classifier.dt.min", int),
                ("classifier.dt.ra", float),
                ("classifier.dt.cat", int),
                ("classifier.dt.r", bool),
                ("classifier.dt.t", bool),
                ("classifier.ann.t", str),
                ("classifier.ann.sizes", list),
                ("classifier.ann.f", str),
                ("classifier.ann.a", float),
                ("classifier.ann.b", float),
                ("classifier.ann.bpdw", float),
                ("classifier.ann.bpms", float),
                ("classifier.ann.rdw", float),
                ("classifier.ann.rdwm", float),
                ("classifier.ann.term", str),
                ("classifier.ann.eps", float),
                ("classifier.ann.iter", int),
                ("classifier.rf.max", int),
                ("classifier.rf.min", int),
                ("classifier.rf.ra", float),
                ("classifier.rf.cat", int),
                ("classifier.rf.var", int),
                ("classifier.rf.nbtrees", int),
                ("classifier.rf.acc", float),
                ("classifier.knn.k", int),
                ("classifier.sharkrf.nbtrees", int),
                ("classifier.sharkrf.nodesize", int),
                ("classifier.sharkrf.mtry", int),
                ("classifier.sharkrf.oobr", float),
                ("classifier.sharkkm.maxiter", int),
                ("classifier.sharkkm.k", int),
                ("classifier.sharkkm.incentroids", str),
                ("classifier.sharkkm.cstats", str),
                ("classifier.sharkkm.outcentroids", str),
            ]
        )
        error_message = []
        for key in value.keys():
            if key in avail_keys:
                if not isinstance(value[key], avail_keys[key]):
                    error_message.append(
                        f"parameter '{key}' must be of "
                        f"type {avail_keys[key].__name__}. "
                        f"Value detected : {type(value[key]).__name__}"
                        f" in {field.name}"
                    )
            else:
                error_message.append(
                    f"parameter '{key}' not "
                    f"recognised. Please remove it from {field.name}"
                )
        if error_message:
            raise ConfigError("\n".join(error_message))
        return value


class SampleAugmentation(dict):
    """Sample augmentation parameter definition."""

    @dataclass(frozen=True)
    class AugmentationFieldsRules:  # pylint: disable=too-many-instance-attributes
        """Define fields rules for dataAugmentation."""

        Rules = namedtuple(
            "Rules",
            ["expected_type", "validation_function", "validation_function_err_msg"],
        )

        strategy: Rules = Rules(
            str,
            lambda x: x in ["replicate", "jitter", "smote"],
            "value must be one of 'replicate', 'jitter', 'smote'",
        )
        strategy_jitter_stdfactor: Rules = Rules(int, None, None)
        strategy_smote_neighbors: Rules = Rules(int, None, None)
        samples_strategy: Rules = Rules(
            str,
            lambda x: x in ["minNumber", "balance", "byClass"],
            "value must be one of 'minNumber', 'balance', byClass",
        )
        samples_strategy_minnumber: Rules = Rules(int, None, None)
        samples_strategy_byclass: Rules = Rules(
            str,
            lambda x: Path(x).exists(),
            ("path in samples.strategy.byclass does not exists"),
        )
        activate: Rules = Rules(bool, None, None)
        target_models: Rules = Rules(
            list, lambda x: isinstance(x[0], str), "list elements must be strings"
        )
        bins: Rules = Rules(list, lambda x: x == sorted(x), "bins must be sorted")

        def __getattribute__(self, name: str) -> Rules:
            attribute_name = name.replace(".", "_").lower()
            rule = super().__getattribute__(attribute_name)
            return rule  # type: ignore  # mypy can't infer type from getattr

    @classmethod
    def __get_validators__(cls) -> Generator:
        yield cls.validate

    @classmethod
    def validate(cls, value: dict, field: ModelField) -> dict:
        """Validate data augmentation parameters."""
        forbidden_keys = [
            "in",
            "out",
            "field",
            "layer",
            "label",
            "seed",
            "inxml",
            "progress",
            "help",
        ]
        avail_keys = [
            "target_models",
            "strategy",
            "strategy.jitter.stdfactor",
            "strategy.smote.neighbors",
            "seed",
            "samples.strategy",
            "samples.strategy.minNumber",
            "samples.strategy.byClass",
            "activate",
            "bins",
        ]

        forbidden_params = []
        for key in value.keys():
            if key not in avail_keys:
                warnings.warn(key, ConfigNotRecognisedParamWarning)
            if key in forbidden_keys:
                forbidden_params.append(key)
        err_message = []
        if forbidden_params:
            err_message.append(
                "sample_augmentation section, forbidden "
                f"parameters detected : '{', '.join(forbidden_params)}'."
                f" Please remove them from {field.name}"
            )
        rules = cls.AugmentationFieldsRules()
        for key, val in value.items():
            rule = getattr(rules, key, None)
            if rule and not isinstance(val, rule.expected_type):
                raise ConfigError(
                    f"sample_augmentation.{key} : wrong type. "
                    f"Need {rule.expected_type.__name__}, detected {type(val).__name__}"
                )
            if rule and rule.validation_function is not None:
                if not rule.validation_function(val):
                    raise ConfigError(rule.validation_function_err_msg)
        return value

    @classmethod
    def __modify_schema__(cls, field_schema: dict, field: ModelField | None) -> None:
        if field:
            field_schema["doc_type"] = "dict"
            field_schema["available_on_builders"] = [
                "I2Classification",
                "I2Regression",
            ]
            field_schema["short_desc"] = "OTB parameters for sample augmentation"
            field_schema["long_desc"] = (
                "In supervised classification the balance between "
                "class samples is important. There are"
                " any ways to manage class balancing in iota2, using "
                ":ref:`refSampleSelection` or "
                "the classifier's options to limit the number of "
                "samples by class."
                "\nAn other approch is to generate synthetic "
                "samples. It is the purpose of this "
                "functionality, which is called "
                "'sample augmentation'.\n\n"
                "    .. code-block:: python\n\n"
                "        {'activate':False}\n"
                "\nExample\n"
                "^^^^^^^\n\n"
                "    .. code-block:: python\n\n"
                "        sample_augmentation : {'target_models':['1', '2'],\n"
                "                              'strategy' : 'jitter',\n"
                "                              'strategy.jitter.stdfactor' : 10,\n"
                "                              'strategy.smote.neighbors'  : 5,\n"
                "                              'samples.strategy' : 'balance',\n"
                "                              'activate' : True\n"
                "                              }\n"
                "\n\niota2 implements an interface to the OTB `SampleAugmentation"
                " <https://www.orfeo-toolbox.org/CookBook/Applications/"
                "app_SampleSelection.html>`_ application.\n"
                "There are three methods to generate samples : replicate, jitter"
                " and smote."
                "The documentation :doc:`here <sampleAugmentation_explain>`"
                " explains the difference"
                " between these approaches.\n\n"
                " ``samples.strategy`` specifies how many samples must be created."
                "There are 3 different strategies:"
                "    - minNumber\n"
                "        To set the minimum number of samples by class required\n"
                "    - balance\n"
                "        balance all classes with the same number of "
                "samples as the majority one\n"
                "    - byClass\n"
                "        augment only some of the classes"
                "\nParameters related to ``minNumber`` and ``byClass`` strategies"
                " are\n"
                "    - samples.strategy.minNumber\n"
                "        minimum number of samples\n"
                "    - samples.strategy.byClass\n"
                "        path to a CSV file containing in first column the class's"
                " label and \n"
                "        in the second column the minimum number of samples"
                " required.\n"
                "In the above example, classes of models '1' and '2' will be"
                " augmented to "
                "the most represented class in the corresponding model using the"
                " jitter method.\n\n"
                "To perform sample augmentation for regression problems, "
                "we return to a configuration similar to classification.\n"
                "For that purpose we create fake classes using the ``bins`` parameter. "
                "The ``bins`` parameter can be an integer in which case the interval of values "
                "of the target output variable is divided into ``bins`` sub-intervals of equal"
                " width, and each sample gets a fake a class corresponding to the number "
                "of the interval in which its label fell.\n"
                "``bins`` can also be a list of ascending values used as interval boundaries "
                "for assign the classes."
            )


class SampleSelection(dict):
    """https://field-idempotency--pydantic-docs.netlify.app/usage/types/#custom-data-types."""

    @dataclass(frozen=True)
    class SampleSelectionFieldsRules:  # pylint: disable=too-many-instance-attributes
        """Define fields rules for SampleSeleciton App."""

        Rules = namedtuple(
            "Rules",
            ["expected_type", "validation_function", "validation_function_err_msg"],
        )
        strategy: Rules = Rules(
            str,
            lambda x: x
            in [
                "byclass",
                "constant",
                "percent",
                "total",
                "smallest",
                "all",
            ],
            (
                "strategy value must be one of 'byclass', 'constant', 'percent', "
                "'total', 'smallest', 'all'"
            ),
        )
        strategy_percent_p: Rules = Rules(float, None, None)
        strategy_byclass_in: Rules = Rules(
            str, lambda x: Path(x).exists, "path in strategy.byclass.in does not exists"
        )
        sampler: Rules = Rules(
            str,
            lambda x: x in ["periodic", "random"],
            "sampler must be one of 'periodic', 'random'",
        )
        per_models: Rules = Rules(list, None, None)
        sampler_periodic_jitter: Rules = Rules(int, None, None)
        strategy_constant_nb: Rules = Rules(int, None, None)
        strategy_total_v: Rules = Rules(int, None, None)
        elev_dem: Rules = Rules(
            str, lambda x: Path(x).exists, "path in elev.dem does not exists"
        )
        elev_geoid: Rules = Rules(
            str, lambda x: Path(x).exists, "path in elev.geoid does not exists"
        )
        elev_default: Rules = Rules(
            str, lambda x: Path(x).exists, "path in elev.default does not exists"
        )
        ram: Rules = Rules(int, None, None)
        target_model: Rules = Rules(int, None, None)

        def __getattribute__(self, name: str) -> Rules:
            attribute_name = name.replace(".", "_").lower()
            # mypy can't infer type from getattr
            return super().__getattribute__(attribute_name)  # type: ignore

    @classmethod
    def __get_validators__(cls) -> Generator:
        yield cls.validate

    @classmethod
    def validate(cls, value: dict, field: ModelField) -> dict:  # pylint: disable=W0613
        """Validate sample selection parameters"""
        rules = cls.SampleSelectionFieldsRules()

        def check_sample_selection_param(params: dict) -> None:
            """
            Check samples selection parameters for a single model

            Parameters
            ----------
            params:
                Parameters (as a dictionary)
            """
            avail_keys = [
                "sampler",
                "sampler.periodic.jitter",
                "strategy",
                "strategy.byclass.in",
                "strategy.constant.nb",
                "strategy.percent.p",
                "ram",
                "per_models",
                "target_model",
            ]
            for key in params.keys():
                if key not in avail_keys:
                    warnings.warn(key, ConfigNotRecognisedParamWarning)
            forbidden_keys = [
                "outrates",
                "in",
                "mask",
                "vec",
                "out",
                "instats",
                "field",
                "layer",
                "rand",
                "inxml",
            ]
            for forbidden_key in forbidden_keys:
                if forbidden_key in params:
                    raise ConfigError(
                        f"'{forbidden_key}' parameter must "
                        "not be set in arg_train.sample_selection"
                    )
            for param_name, param_val in params.items():
                rule = getattr(rules, param_name, None)
                if rule is None:
                    raise ConfigError(
                        f"parameter {param_name} does not exists, please remove it."
                    )
                if not isinstance(param_val, rule.expected_type):
                    raise ConfigError(
                        f"sample_selection.{param_name} wrong type. "
                        f"Need {rule.expected_type.__name__}, "
                        f"detected {type(param_val).__name__}"
                    )
                if rule.validation_function is not None:
                    if not rule.validation_function(param_val):
                        raise ConfigError(rule.validation_function_err_msg)

        check_sample_selection_param(value)
        if "per_models" in value:
            for model in value["per_models"]:
                check_sample_selection_param(model)

        return value

    @classmethod
    def __modify_schema__(cls, field_schema: dict, field: ModelField | None) -> None:
        if field:
            field_schema["available_on_builders"] = [
                "I2Classification",
                "I2Obia",
                "I2Regression",
            ]
            field_schema["doc_type"] = "dict"
            field_schema[
                "short_desc"
            ] = "OTB parameters for sampling the validation set"
            field_schema["long_desc"] = (
                "This field parameters the strategy of polygon "
                "sampling. It directly refers to options of "
                "OTB’s SampleSelection application."
                "\n\nExample\n"
                "-------\n\n"
                "    .. code-block:: python\n\n"
                "        sample_selection : {'sampler':'random',\n"
                "                           'strategy':'percent',\n"
                "                           'strategy.percent.p':0.2,\n"
                "                           'per_models':[{'target_model':'4',\n"
                "                                          'sampler':'periodic'}]\n"
                "                           }"
                "\n\nIn the example above, all polygons will be "
                "sampled with the 20% ratio. But "
                "the polygons which belong to the model 4 will "
                "be periodically sampled,"
                " instead of the ransom sampling used for other "
                "polygons."
                "\nNotice than ``per_models`` key contains a list of"
                " strategies. Then we can imagine the following :\n\n"
                "    .. code-block:: python\n\n"
                "        sample_selection : {'sampler':'random',\n"
                "                           'strategy':'percent',\n"
                "                           'strategy.percent.p':0.2,\n"
                "                           'per_models':[{'target_model':'4',\n"
                "                                          'sampler':'periodic'},\n"
                "                                         {'target_model':'1',\n"
                "                                          'sampler':'random',\n"
                "                                          'strategy', 'byclass',\n"
                "                                          'strategy.byclass.in',"
                " '/path/to/myCSV.csv'\n"
                "                                         }]\n"
                "                           }\n"
                " where the first column of /path/to/myCSV.csv is"
                " class label (integer), second one is the required"
                " samples number (integer)."
            )


class TrainSection(Iota2ParamSection):
    """Definition of the parameters that belong to the 'arg_train' section."""

    section_name: ClassVar[str] = "arg_train"
    mode_outside_regionsplit: float = Field(
        0.1,
        doc_type="float",
        short_desc="Set the threshold for split huge model",
        long_desc=(
            "This parameter is available if "
            "regionPath is used and arg_train.classif_mode"
            " is set to fusion. "
            "It represents the maximum size covered by a region."
            " If the regions are larger than this threshold, "
            "then N models are built by randomly"
            " selecting features inside the region."
        ),
        available_on_builders=["I2Classification"],
    )
    runs: int = Field(
        1,
        doc_type="int",
        short_desc="Number of independent runs processed",
        long_desc=(
            "Number of independent runs processed."
            " Each run has his own learning samples."
            " Must be an integer greater than 0"
        ),
        available_on_builders=["I2Classification", "I2Regression", "I2Obia"],
    )
    split_ground_truth: bool = Field(
        True,
        doc_type="bool",
        short_desc="Enable the split of reference data",
        long_desc=(
            "If set to False, the chain"
            " use all polygons for both "
            "training and validation"
        ),
        available_on_builders=["I2Classification", "I2Obia"],
    )
    ratio: float = Field(
        0.5,
        doc_type="float",
        short_desc=(
            "Should be between 0.0 and 1.0 and represent "
            "the proportion of the dataset to include in the train split."
        ),
        available_on_builders=["I2Classification", "I2Regression", "I2Obia"],
    )
    random_seed: int = Field(
        None,
        doc_type="int",
        short_desc="Fix the random seed for random split of reference data",
        long_desc=(
            "Fix the random seed used for random split "
            "of reference data"
            " If set, the results must be the "
            "same for a given classifier"
        ),
        available_on_builders=["I2Classification", "I2Regression", "I2Obia"],
    )
    deep_learning_parameters: DeepLearningParameters = Field(
        {},
        doc_type="dict",
        short_desc=(
            "deep learning parameter description is available "
            ":doc:`here <deep_learning>`"
        ),
        available_on_builders=[
            "I2Classification",
            "I2Regression",
        ],
    )
    force_standard_labels: bool = Field(
        False,
        doc_type="bool",
        short_desc="Standardize labels for feature extraction",
        long_desc=(
            "The chain label each features by the sensors name,"
            " the spectral band or indice and the date."
            " If activated this parameter use the OTB default value"
            " (`value_X`)"
        ),
        available_on_builders=["I2Classification"],
    )

    learning_samples_extension: Literal["sqlite", "csv"] = Field(
        "sqlite",
        doc_type="str",
        short_desc=(
            "learning samples file extension, possible values are 'sqlite' and 'csv'"
        ),
        long_desc=(
            "Default value is 'sqlite' (faster), in case of the number of "
            "features is superior to 2000, as sqlite file doesn't accept more "
            "than 2000 columns, it should be turn to 'csv'."
        ),
        available_on_builders=["I2Classification", "I2Regression"],
    )

    sample_selection: SampleSelection = SampleSelection(
        {"sampler": "random", "strategy": "all"}
    )
    sampling_validation: bool = Field(
        False,
        doc_type="bool",
        short_desc="Enable sampling validation",
        available_on_builders=["I2Classification", "I2Regression"],
    )
    sample_augmentation: SampleAugmentation = SampleAugmentation(
        {"activate": False, "bins": 10}
    )
    sample_management: FileParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to a CSV file " "containing samples transfert" " strategies"
        ),
        long_desc=(
            "The CSV must contain a row per transfert\n"
            "    .. code-block:: python\n\n"
            "        >>> cat /absolute/path/myRules.csv\n"
            "            1,2,4,2\n"
            "Meaning :\n"
            "        +--------+-------------+------------+----------+\n"
            "        | source | destination | class name | quantity |\n"
            "        +========+=============+============+==========+\n"
            "        |   1    |      2      |      4     |     2    |\n"
            "        +--------+-------------+------------+----------+\n\n"
            "Currently, setting the 'random_seed' parameter has no effect on this workflow."
        ),
        available_on_builders=["I2Classification", "I2Regression"],
    )

    features_from_raw_dates: bool = Field(
        False,
        doc_type="bool",
        short_desc="learn model from raw sensor's date (no interpolations)",
        long_desc=(
            "If True, during the learning and classification step, each pixel"
            " will receive a vector of values of the size of the number of"
            " all dates detected. As the pixels were not all acquired on "
            "the same dates, the vector will contains NaNs on the unacquired "
            "dates."
        ),
        available_on_builders=["I2Classification"],
    )
    classifier: str | None = Field(
        doc_type="str",
        short_desc="otb classification algorithm",
        builders={"I2Classification": True},
        available_on_builders=["I2Classification", "I2Obia"],
    )
    otb_classifier_options: OtbClassifierParam = Field(
        None,
        doc_type="dict",
        short_desc=(
            "OTB option for classifier." "If None, the OTB default values are used"
        ),
        long_desc=(
            "This parameter is a dictionnary"
            " which accepts all OTB application parameters."
            " To know the exhaustive parameter list "
            " use `otbcli_TrainVectorClassifier` in a terminal or"
            " look at the OTB documentation"
        ),
        available_on_builders=["I2Classification", "I2Obia"],
    )
    validity_threshold: int = Field(
        1,
        doc_type="int",
        short_desc="threshold above which a " "training pixel is considered valid",
        available_on_builders=["I2Classification", "I2Obia"],
    )
    features: list[str] = Field(
        ["NDVI", "NDWI", "Brightness"],
        doc_type="list",
        short_desc="List of additional features computed",
        long_desc=(
            "This parameter enable the computation of the "
            "three indices if available for the sensor used."
            "There is no choice for using only one of them"
        ),
        available_on_builders=["I2Classification", "I2FeaturesMap", "I2Regression"],
    )

    sample_validation: SampleSelection = SampleSelection(
        {"sampler": "random", "strategy": "all"}
    )
