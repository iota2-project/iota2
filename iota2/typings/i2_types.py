"""
Module defining custom types for iota2
"""

from collections.abc import Callable
from dataclasses import dataclass
from pathlib import Path
from typing import IO, Annotated, Any, Literal, NewType, TypedDict, TypeVar

import numpy as np
import osgeo.gdal
import otbApplication as otb
from affine import Affine
from osgeo import osr

from iota2.common.service_error import I2error
from iota2.learning.utils import I2Label, I2LabelAlias, I2TemporalLabel

# related to OTB
OtbApp = type[otb.Application]
OTBInputImage = str | list[str] | list[OtbApp] | list[tuple]
OtbDep = list[OtbApp]
OtbAppWithDep = tuple[OtbApp, OtbDep]
OtbSARAppWithDep = tuple[dict[str, OtbApp], OtbDep]
OtbPixType = Literal[
    "complexDouble",
    "complexFloat",
    "double",
    "float",
    "int16",
    "int32",
    "uint16",
    "uint32",
    "uint8",
]
OTBType = int
StartX = int
StartY = int
SizeX = int
SizeY = int
EndX = int
EndY = int
OtbLikeBoundingBox = tuple[StartX, SizeX, StartY, SizeY]


@dataclass
class AugmentationParams:
    """
    Data augmentation parameters

    Attributes
    ----------

    strategy : string
        which method to use in order to perform data augmentation (replicate/jitter/smote)
    field : string
        data's field
    excluded_fields : list
        do not consider these fields to perform data augmentation
    jitter_std_factor : float
        Factor for dividing the standard deviation of each feature
    smote_neighbors : int
        Number of nearest neighbors (smote's method)
    random_seed : int
        Set the random seed
    """

    strategy: str
    field: str
    excluded_fields: list[str]
    jitter_std_factor: int
    smote_neighbors: int
    random_seed: int


# Parameters are coming from otb.
# pylint: disable=C0103, R0902
@dataclass
class OTBParamsTypes:
    """Dataclass containing every otb parameter types."""

    ParameterType_Int: int
    ParameterType_Float: int
    ParameterType_Double: int
    ParameterType_String: int
    ParameterType_StringList: int
    ParameterType_InputFilename: int
    ParameterType_InputFilenameList: int
    ParameterType_OutputFilename: int
    ParameterType_Directory: int
    ParameterType_Choice: int
    ParameterType_InputImage: int
    ParameterType_InputImageList: int
    ParameterType_InputVectorData: int
    ParameterType_InputVectorDataList: int
    ParameterType_OutputImage: int
    ParameterType_OutputVectorData: int
    ParameterType_Radius: int
    ParameterType_Group: int
    ParameterType_ListView: int
    ParameterType_RAM: int
    ParameterType_Bool: int
    ParameterType_Field: int
    ParameterType_Band: int


class OtbImage(TypedDict):
    """output type of otbApplication.Application.ExportImage

    Parameters
    ----------
    array: np.ndarray
    origin: otb.itkPoint
    spacing: otb.itkVector
    size: otb.itkSize
    region: otb.itkRegion
    metadata: otb.ImageMetadata
    """

    array: np.ndarray
    origin: otb.itkPoint
    spacing: otb.itkVector
    size: otb.itkSize
    region: otb.itkRegion
    metadata: otb.ImageMetadata


# Related to database
WKTGeometry = NewType("WKTGeometry", str)
OgrType = int

# Related to rasters
InputRaster = str | osgeo.gdal.Dataset
GdalType = int
UpLeftX = float
ResX = float
RotationCoeff = float
UpLeftY = float
ResY = float
GdalGeoTranform = tuple[UpLeftX, ResX, RotationCoeff, UpLeftY, RotationCoeff, ResY]
PaddingBox = tuple[StartX, EndX, StartY, EndY]
AvailCompressions = Literal[
    "JPEG",
    "LZW",
    "PACKBITS",
    "DEFLATE",
    "CCITTRLE",
    "CCITTFAX3",
    "CCITTFAX4",
    "LZMA",
    "ZSTD",
    "LERC",
    "LERC_DEFLATE",
    "LERC_ZSTD",
    "WEBP",
    "JXL",
    "NONE",
]

# Related to configuration file
FunctionNameWithParams = tuple[str, dict[str, Any]]
I2ParameterT = dict[str, Any]
Param = dict[str, str | list[str] | int]
ClassifierOptions = None | dict[str, str | int]

# Related to pipeline
I2FeaturesPipeline = tuple[OtbAppWithDep, list[I2LabelAlias]]
I2SARFeaturesPipeline = tuple[OtbSARAppWithDep, dict[str, list[I2TemporalLabel]]]


@dataclass
class PipelinesLabels:
    """
    dataclass representing labels associated with different pipelines.

    Attributes
    ----------
    interp:
        Labels associated with interpolated pipelines.
        It can be either a list of `I2LabelT` or a list of strings.
    raw:
        Labels associated with raw pipelines.
        It is a list of `I2LabelT`.
    masks:
        Labels associated with masks.
        It is a list of `I2LabelT`.
    """

    interp: list[I2LabelAlias]
    raw: list[I2LabelAlias]
    masks: list[I2LabelAlias]


# Sensors
FeatureToDatesType = dict[str, list[str]]
SensorToFeaturesType = dict[str, FeatureToDatesType]
SensorsParameters = dict[str, str | list[str] | int]
SensorName = str
YYYYMMDD = str
SensorsDates = dict[SensorName, list[YYYYMMDD]]

SARDatesDict = dict[str, dict[str, dict[str, OtbApp | list[str]]]]

# Miscellaneous
AvailSchedulers = Literal["PBS", "Slurm", "cluster", "localCluster", "debug"]
FunctionsMapping = dict[str, Callable]
PathLike = Path | str
ListPathLike = list[str] | list[Path]
LabelsCoords = list[tuple[int, tuple[int, float]]]
SortedLabelsCoords = list[tuple[int, list[tuple[int, float]]]]
BroadType = TypeVar("BroadType", int, float, str)
ConfusionMatrixT = dict[int, dict[int, Any]]
CoordXY = tuple[int, int] | tuple[float, float]
CoordXYList = list[CoordXY]
SpatialResolution = tuple[float | int, float | int]
MetaDataType = list[str]
EmptyList = list[()]
ErrorsList = list[I2error]
FileObject = IO
LabelsConversionDict = dict[str | int, str | int]
Percent = Annotated[float, lambda x: 0 <= x <= 100]
Envelope = tuple[float, float, float, float]
DepTasksNamesList = list[str] | list[int]


@dataclass
class ClassifScores:
    """
    Dataclass used to represent all scores of a classification
    """

    kappa: float
    oacc: float
    precision: dict[int, float]
    recall: dict[int, float]
    fscore: dict[int, float]


@dataclass
class MultipleClassifScores:
    """
    Dataclass used to represent multiple iterations scores of multiple classifications
    """

    all_kappa: list[float]
    all_oacc: list[float]
    all_precision: list[dict[int, float]]
    all_recall: list[dict[int, float]]
    all_fscore: list[dict[int, float]]
    precision_mean: dict
    recall_mean: dict
    fscore_mean: dict


@dataclass
class ConfMatrixImageParameters:
    """
    Dataclass containing parameters for plotting a confusion matrix

    Attributes
    ----------
    dpi : int
        dpi
    write_conf_score : bool
        allow the display of confusion score
    grid_conf : bool
        display confusion matrix grid
    conf_score : string
        'count' / 'percentage'
    point_of_view : string
        'ref' / 'prod' define how to normalize the confusion matrix
    threshold : float
        display confusion > threshold (in percentage of confusion)
    """

    dpi: int = 900
    write_conf_score: bool = True
    grid_conf: bool = False
    conf_score: Literal["count", "count_sci"] = "count"
    point_of_view: str = "ref"
    threshold: float | int = 0


@dataclass
class ConfMatrixParameters:
    """
    Dataclass containing parameters to configure the confusion matrix

    Attributes
    ----------
    indecidedlabel : int
        indecided label
    class_order : list
        list of integers. Define labels order
    threshold : float
        display confusion > threshold (in percentage of confusion)
    labels_table :
        Labels conversion table
    merge_class : list
        list of dictionary.
        example :
            merge_class = [{"src_label": [1,2,3],
                            "dst_label": 50,
                            "dst_name" : "URBAN"},
                           {...}]
    """

    indecidedlabel: int | None = None
    class_order: list[int] | None = None
    threshold: int | float = 0
    labels_table: dict | None = None
    merge_class: list[dict] | None = None


@dataclass
class ConfusionMatrixAppParameters:
    """
    Dataclass containing parameters for the confusion matrix OTB app

    Attributes
    ----------
    in_classif : string
        input classification file
    out_csv : string
        output csv file
    data_field : string
        data field in the ground truth database
    ref_vector : string
        reference vector file (containing polygons)
    ram : float
        available ram in Mo
    """

    in_classif: PathLike
    out_csv: PathLike
    data_field: str
    ref_vector: PathLike
    ram: int


@dataclass
class MosaicInputPaths:
    """
    Dataclass containing all requires paths for mosaic

    Attributes
    ----------
    path_classif:
        path to classification
    validation_input_folder:
        Path to DataAppVal folder
    final_folder:
        Path to final folder
    final_tmp_folder:
        Path to final/TMP folder
    nb_view_folder:
        Path to features folder
    region_shape:
        the region shapefile
    color_path:
        the text file containing colors
    """

    path_classif: str
    validation_input_folder: str
    final_folder: str
    final_tmp_folder: str
    nb_view_folder: str
    region_shape: str
    color_path: str


@dataclass
class MosaicParameters:
    """
    Dataclass containing parameters for the mosaic step

    Attributes
    ----------
    runs:
        the number of random seeds
    classif_mode:
        the classification mode 'separate' or 'fusion'
    data_field:
        the class label column name
    labels_conversion:
        dictionary to decode internal labels to original labels
    tiles_from_cfg:
        the full list of tiles provided by users
    mode:
        "classif" or "regression"
        controls where to look for data, if color indexing and probability
        map should be merged, and if re-encoding should be done
        also control pixel type (integer vs float)
    """

    @dataclass
    class Flags:
        """
        Attributes
        ----------
        proba_map_flag:
            enable the generation of probabilities maps
        output_statistics:
            compute additional statistics on cloud coverage
        """

        proba_map_flag: bool
        output_statistics: bool

    runs: int
    classif_mode: Literal["separate", "fusion"]
    data_field: str
    labels_conversion: LabelsConversionDict | None
    tiles_from_cfg: list[str]
    flags: Flags
    mode: Literal["classif", "regression"] = "classif"


@dataclass
class MergeMetricMatricesParameters:
    """
    Parameters for merging the metrics matrices

    Attributes
    ----------
    data_field:
        The data field in the validation data set.
    runs:
        Number of seeds.
    buffer_size:
        Buffer size.
    label_conv_dict:
        Dictionary for label conversion.
    """

    data_field: str
    runs: int
    buffer_size: int
    label_conv_dict: LabelsConversionDict


@dataclass
class BoundaryValidationParameters:
    """
    Dataclass containing parameters for boundary validation operations

    Attributes
    ----------
    tile:
        Name of the tile.
    iota2_directory:
        Path to iota2 root output path.
    data_field:
        The data field in the validation data set.
    seed:
        Seed value.
    """

    tile: str
    iota2_directory: PathLike
    data_field: str
    seed: int


@dataclass
class BoundariesProbaFusionParameters:
    """
    Dataclass containing parameters for the proba-map fusion at boundaries

    Attributes
    ----------

    tile:
        the processed tile name
    seed:
        the seed targeted
    region_field:
        the name of region column in vector file
    number_of_chunks:
        the number of chunks used to split a tile to fit RAM
    nb_class:
        the number of classes in the nomenclature
    target_chunk:
        process a particular chunk
    generate_final_probability_map:
        flag to generate final proba map
    """

    tile: str
    seed: int
    region_field: str
    number_of_chunks: int
    nb_class: int
    target_chunk: int = 1
    generate_final_probability_map: bool = False


@dataclass
class RegressionMetricsParameters:
    """
    Dataclass containing parameters for computing regression metrics

    Attributes
    ----------
    data_field:
        Field of shapefile containing data
    mode:
        Mode in which you want to open the results file
    column:
        Write out the column names at the top of the results file (false if mode = "a")
    """

    data_field: str
    mode: str = "w"
    column: bool = True


@dataclass
class SampleSelectionConcatParams:
    """
    Dataclass containing parameters for `concatenate_sample_selection`

    Attributes
    ----------
    seed:
        number of the seed
    regions:
        set of region labels
    labels_conversion:
        maps i2-encoded label to original label
    """

    seed: int
    regions: set[str]
    labels_conversion: LabelsConversionDict


@dataclass
class IntersectionDatabaseParameters:
    """
    Class containing parameters concerning database intersection computation

    Attributes
    ----------

    fields_to_keep:
        list of fields to keep
    db_1_fid:
        add FID column to first input database
    db_2_fid
        add FID column to second input database
    """

    data_base1: PathLike
    data_base2: PathLike
    fields_to_keep: list[str]
    db_1_fid: str | None = None
    db_2_fid: str | None = None


@dataclass
class MergeDBParameters:
    """

    Attributes
    ----------
    cols_order
        Specify output columns and their order. It must be build considering every input columns.
        The first element is the name of a column containing a unique integer for each row ("fid"
        by de fault if the list is not set).
    fid_col:
        output column name containing a unique integer
    nan_values:
        replace nan values in dataframe (into columns not in specific_nans_rules)
    nans_rules
        {regular expression pattern:nan_value}
        ie : merge_db_to_netcdf(..., nan_values=-1, specific_nans_rules={('mask'): 2})
        then every column containing 'mask' in their name, nans
        will be replaced by 2. Every other nan values will be replaced by -1
    chunk_size
        read input sqlite max size
    """

    cols_order: list[str] | None = None
    fid_col: str = "fid"
    nan_values: float | None = None
    nans_rules: dict[str, float] | None = None
    chunk_size: int = 100000


@dataclass
class ModelHyperParameters:
    """
    A data class that holds hyperparameters for configuring the model.

    Attributes
    ----------
    dl_parameters:
        Dictionary of parameters (kwargs) to instantiate the deep learning model.
    checkpoints_interval:
        Interval between saving model checkpoints, by default 1.
    restart_from_checkpoint:
        Whether to restart training from a saved checkpoint, by default True.
    weighted_labels:
        Whether to use weighted labels during training, by default False.
    learning_rate:
        Learning rate for training the model, by default 0.00001.
    epochs:
        Number of epochs for training, by default 100.
    batch_size:
        Batch size for training, by default 1000.
    model_optimization_criterion:
        Optimization criterion for the model, by default "cross_entropy".
    additional_statistics_percentage:
        Percentage of additional statistics, by default None.
    enable_early_stop:
        Whether to enable early stopping during training, by default False.
    epoch_to_trigger:
        Epoch to trigger early stopping, by default 5.
    early_stop_patience:
        Patience for early stopping, by default 10.
    early_stop_metric:
        Metric to use for early stopping, by default "val_loss".
    early_stop_tol:
        Tolerance for early stopping, by default 0.01.
    adaptive_lr:
        Dictionary of parameters for adaptive learning rate, by default None.
    """

    dl_parameters: dict
    checkpoints_interval: int = 1
    restart_from_checkpoint: bool = True
    weighted_labels: bool = False
    learning_rate: float = 0.00001
    epochs: int = 100
    batch_size: int = 1000
    model_optimization_criterion: str = "cross_entropy"
    additional_statistics_percentage: float | None = None

    enable_early_stop: bool = False
    epoch_to_trigger: int = 5
    early_stop_patience: int = 10
    early_stop_metric: str = "val_loss"
    early_stop_tol: float = 0.01
    adaptive_lr: dict | None = None


@dataclass
class DistanceMapParameters:
    """

    Attributes
    ----------
    exterior_buffer_size:
        the size in meters of the exterior buffer (outside the region)
    epsilon:
        the lower limit (0 can create holes in map)
        to be stored it must be valid once multiplied by 1000
    interior_buffer_size:
        the size in meters for the interior buffer (inside the region)
    spatial_res:
        the spatial resolution
    tile:
        the tile name
    region_field:
        the region column name in vector file
    """

    @dataclass
    class Paths:
        """
        Sub-dataclass containing all paths for computing the distance map

        Attributes
        ----------
        region_file:
            region vector file
        common_mask:
            common mask of the tile
        output_path:
            output folder to store results
        masks_region_path:
            path where classification masks are stored
        """

        region_file: str
        common_mask: str
        output_path: str
        masks_region_path: str
        working_directory: str | None = None

    exterior_buffer_size: int
    interior_buffer_size: int
    epsilon: float
    spatial_res: SpatialResolution
    tile: str | None = None
    region_field: str | None = None
    paths: Paths | None = None


@dataclass
class CustomFeaturesParameters:
    """
    Dataclass containing the parameters of custom features:

    Attributes
    ----------
    enable: bool
        Flag to enable custom features
    module: str
        Path to the module containing the function(s) to use in custom features
    functions: list[FunctionNameWithParams] | None
        List of functions to use in custom features
    concat_mode: bool
        activate the concatenation mode
    """

    concat_mode: bool
    enable: bool = False
    module: str = ""
    functions: list[FunctionNameWithParams] | None = None


@dataclass
class PredictionOutput:
    """
    Dataclass containing paths of all outputs of predictions: classif, confidence and proba maps
    + iota2 output path

    Attributes
    ----------
    classif: str
        output classification raster path
    confidence: str
        output confidence raster path
    proba: str
        output confidence raster path
    output_path: str
        iota2 output path
    """

    classif: str
    confidence: str
    proba: str | None
    output_path: str


@dataclass
class PredictionResultMaps:
    """
    Dataclass containing a prediction's output maps

    Attributes
    ----------
    confidence_map: np.ndarray
        Numpy array containing the confidence map
    labels_map: np.ndarray
        Numpy array containing the labels map
    proba_map: np.ndarray | None
        Numpy array containing the proba map
    """

    confidence_map: np.ndarray
    labels_map: np.ndarray
    proba_map: np.ndarray | None


@dataclass
class RasterioSpatialMetadata:
    """
    Dataclass containing transform and epsg code
    """

    epsg: int
    transform: Affine


@dataclass
class TransfertQuantity:
    """
    Represents sample transfert between models.

    Attributes
    ----------
    src_model:
        source model
    dst_model:
        destination model
    class_name:
        class value
    quantity:
        quantity to extract
    """

    src_model: str
    dst_model: str
    class_name: str
    quantity: str


@dataclass
class SpatialMetadata:
    """dataclass to gather spatial metadata."""

    projection: osr.SpatialReference
    gdal_geo_transform: GdalGeoTranform


@dataclass
class PredictionPipelineData:
    """
    Store the prediction pipeline data

    Attributes
    ----------
    stack:
        Features stack
    mask:
        Mask stack
    feat_labels:
        List of features labels
    mask_labels:
        List of mask labels
    spatial_mask:
        every output pixels are element-wise multiply by mask_spatial,
        output = output * mask_spatial
    dependencies:
        List of otb dependencies
    mask_value:
        Masking value
    """

    stack: np.ndarray | None
    mask: np.ndarray | None
    feat_labels: list[I2Label]
    mask_labels: list[I2Label | str] | None
    spatial_mask: np.ndarray | None = None
    dependencies: list[OtbDep] | None = None
    mask_value: int | None = 0


@dataclass
class DBInfo:
    """
    Represents information about a database.

    Attributes
    ----------
    data_field:
        The name of the field of interest into the database.
    db_file:
        The path or list of paths to the database file(s).
    mapping_table:
        mapping between the content of the database to another representation
    features_fields:
        list of fields representing features
    mask_fields:
        list of fields representing masks
    fids_field:
        The name of the field representing fid into the database.
    region_field:
        The name of the field representing regions into the database.
    """

    data_field: str
    db_file: PathLike | ListPathLike | None
    mapping_table: dict | None = None
    features_fields: list[str] | None = None
    mask_fields: list[str] | None = None
    fids_field: str | None = None
    region_field: str | None = None


@dataclass
class ImageCoordinates:
    """
    Dataclass containing coordinates of each corner of a raster
    """

    x_min: float
    x_max: float
    y_min: float
    y_max: float
