#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"train scikit learn model"

import logging
import math
import pickle
import sqlite3
from dataclasses import dataclass
from pathlib import Path
from typing import Any

import numpy as np
import pandas as pd
import sklearn.ensemble
from sklearn.ensemble import (
    AdaBoostClassifier,
    BaggingClassifier,
    ExtraTreesClassifier,
    GradientBoostingClassifier,
    RandomForestClassifier,
    RandomForestRegressor,
    VotingClassifier,
)
from sklearn.linear_model import HuberRegressor, LassoCV, RidgeCV
from sklearn.model_selection import GridSearchCV, GroupKFold, KFold
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

from config import Mapping
from iota2.common.file_utils import memory_usage_psutil
from iota2.typings.i2_types import DBInfo
from iota2.vector_tools.vector_functions import get_layer_name

LOGGER = logging.getLogger("distributed.worker")

# define list of available models
SK_MODEL_LIST = [
    SVC,
    RandomForestClassifier,
    ExtraTreesClassifier,
    AdaBoostClassifier,
    BaggingClassifier,
    GradientBoostingClassifier,
    VotingClassifier,
    RandomForestRegressor,
    RidgeCV,
    LassoCV,
    HuberRegressor,
]

# define the associated type
# we would like to do
# AVAIL_SKL_CLF = Union[*SK_MODEL_LIST]
# however, its not possible
# so I'll get rid of type alias and set
AvailSKLClf = Any

# creates a dictionnary mapping the name to the model
AVAIL_SK_MODELS = {model.__name__: model for model in SK_MODEL_LIST}
# add an alias for SupportVectorClassification
AVAIL_SK_MODELS["SupportVectorClassification"] = SVC


def model_name_to_function(model_name: str) -> AvailSKLClf:
    """cast the model'name from string to sklearn object

    This function must be fill-in with new scikit-learn classifier we want to
    manage in iota2

    Parameters
    ----------
    model_name : str
        scikit-learn model's name (ex: "RandomForestClassifier")

    Return
    ------
    a scikit-learn classifier object
    """
    if model_name not in AVAIL_SK_MODELS:
        raise ValueError(
            f"{model_name} not suported "
            f"in iota2 sklearn models : {', '.join(AVAIL_SK_MODELS.keys())}"
        )
    return AVAIL_SK_MODELS[model_name]


def can_perform_cv(cv_paramerters: dict, clf: sklearn.ensemble.BaseEnsemble) -> bool:
    """check if the cross validation can be done

    Parameters
    ----------
    cv_paramerters : dict
        model parameters to estimate
    clf : ABCMeta
        scikit learn estimator class

    Return
    ------
    bool
        True if all user cross validation parameters can be estimate, else False
    """
    # get_params is comming from scikit-learn BaseEstimator class -> Base class for all estimators
    user_cv_parameters = list(cv_paramerters.keys())

    # there is no other way to get this information
    avail_cv_parameters = list(
        clf._get_param_names()  # pylint: disable=protected-access
    )
    check = [
        user_cv_parameter in avail_cv_parameters
        for user_cv_parameter in user_cv_parameters
    ]

    return all(check)


def cast_config_cv_parameters(config_cv_parameters: Mapping) -> dict[str, list[int]]:
    """cast cross validation parameters coming from config to a compatible sklearn type

    Parameters
    ----------
    config_cv_parameters : config.Mapping
        cross validation parameters from iota2's configuration file

    Return
    ------
    dict
        cross validation parameters as a dictionary containing lists
    """
    sklearn_cv_parameters = dict(config_cv_parameters)
    for key, val in sklearn_cv_parameters.items():
        sklearn_cv_parameters[key] = list(val)
    return sklearn_cv_parameters


def save_cross_val_best_param(output_path: str, clf: AvailSKLClf) -> None:
    """save cross validation parameters in a text file

    Parameters
    ----------
    output_path : str
        output path
    clf : SVC / RandomForestClassifier / ExtraTreesClassifier
        classifier comming from scikit-learn
    """
    with open(output_path, "w", encoding="UTF-8") as cv_results:
        cv_results.write(f"Best Score: {clf.best_score_}\n")
        cv_results.write("Best Parameters:\n")
        cv_results.write(f"{clf.best_params_}\n\n")

        means = clf.cv_results_["mean_test_score"]
        stds = clf.cv_results_["std_test_score"]
        for mean, std, params in zip(means, stds, clf.cv_results_["params"]):
            cv_results.write(f"{mean} (+/- {2 * std}) for {params}\n")


def force_proba(sk_classifier: AvailSKLClf) -> None:
    """force the classifier model to be able of generate proabilities

    change the classifier parameter in place
    Parameters
    ----------
    sk_classifier : SVC / RandomForestClassifier / ExtraTreesClassifier
        classifier comming from scikit-learn
    """
    sk_classifier_parameters = sk_classifier.get_params()
    if isinstance(sk_classifier, SVC):
        sk_classifier_parameters["probability"] = True
    sk_classifier.set_params(**sk_classifier_parameters)


def get_df_features(
    dataset_path: str, features_labels: list[str], data_field: str, cv_groups: bool
) -> tuple[np.ndarray, np.ndarray, np.ndarray | None]:
    """
    Extracts features, labels, and groups from a dataset.

    Parameters
    ----------
    dataset_path : str
        Path to the dataset file.
    features_labels : list of str
        List of feature labels to extract.
    data_field : str
        The field name for the labels.
    cv_groups : bool
        Whether to extract group information for cross-validation.

    Returns
    -------
    tuple of (numpy.ndarray, numpy.ndarray, numpy.ndarray | None)
        - features_values : numpy.ndarray
            Array of feature values.
        - labels_values : numpy.ndarray
            Array of label values.
        - groups : numpy.ndarray | None
            Array of group values if `cv_groups` is True, otherwise None.
    """
    if "sqlite" in Path(dataset_path).suffix:
        layer_name = get_layer_name(dataset_path, "SQLite")
        conn = sqlite3.connect(dataset_path)
        df_features = pd.read_sql_query(
            f"select {','.join(features_labels)} from {layer_name}", conn
        )
        # collect labels from data_field
        # if mode == "classif", this corresponds to reencoded labels
        # if mode == "regression", this is target values to be standardized
        df_labels = pd.read_sql_query(f"select {data_field} from {layer_name}", conn)
        df_groups = pd.read_sql_query(f"select originfid from {layer_name}", conn)
    else:
        df_features = pd.read_csv(dataset_path, usecols=features_labels)
        df_labels = pd.read_csv(dataset_path, usecols=[data_field])
        df_groups = pd.read_csv(dataset_path, usecols=["originfid"])
    # collect features
    groups = np.ravel(df_groups.to_numpy()) if cv_groups else None
    features_values = df_features.to_numpy()
    labels_values = np.ravel(df_labels.to_numpy())
    return features_values, labels_values, groups


@dataclass
class SciKitModelParams:
    """
    Parameters for configuring a scikit-learn model.

    Attributes
    ----------
    in_database : DBInfo
        The database information containing features and data fields.
    model_name : str
        The name of the scikit-learn model to be used.
    output_model : str
        The path where the trained model will be saved.
    model_kwargs : dict[str, Any] | None
        Additional keyword arguments to instantiate the model, by default None.
    """

    in_database: DBInfo
    model_name: str
    output_model: str
    model_kwargs: dict[str, Any] | None = None


@dataclass
class CrossValidationParameters:
    """
    Parameters for configuring cross-validation.

    Attributes
    ----------
    grouped : bool, optional
        Whether to use grouped cross-validation, by default False.
    folds : int, optional
        The number of folds for cross-validation, by default 5.
    parameters : dict[str, Any] | None
        The parameters to use for cross-validation, by default None.
    """

    grouped: bool = False
    folds: int = 5
    parameters: dict[str, Any] | None = None


def save_model(
    clf: AvailSKLClf,
    cross_validation_params: CrossValidationParameters,
    output_model: str,
    scaler_features: StandardScaler | None = None,
    scaler_target: StandardScaler | None = None,
) -> None:
    """
    Save the trained model, along with any scalers used, to the specified output path.

    If cross-validation parameters are provided, the best estimator from the cross-validation
    will be saved, and the cross-validation parameters will also be saved to a separate file.

    Parameters
    ----------
    clf : AVAIL_SKL_CLF
        The trained scikit-learn model or cross-validation object.
    cross_validation_params : CrossValidationParameters
        Parameters related to cross-validation.
    output_model : str
        The file path where the model will be saved.
    scaler_features : StandardScaler | None
        The scaler used for standardizing features, if any.
    scaler_target : StandardScaler | None
        The scaler used for standardizing target values, if any.
    """
    sk_model = clf
    if cross_validation_params.parameters:
        file_ext = Path(output_model).suffix
        cross_val_path = output_model.replace(file_ext, "_cross_val_param.cv")
        save_cross_val_best_param(cross_val_path, clf)
        sk_model = clf.best_estimator_
    with open(output_model, "wb") as model_file:
        pickle.dump((sk_model, scaler_features, scaler_target), model_file)


def fit_model(
    clf: AvailSKLClf,
    features_values: np.ndarray,
    labels_values: np.ndarray,
    logger: logging.Logger,
    available_ram: int | None = None,
) -> None:
    """
    Fit a scikit-learn model using the provided feature and label values,
    adjusting the number of jobs based on available RAM.

    Parameters
    ----------
    clf : AVAIL_SKL_CLF
        The scikit-learn classifier to be trained.
    features_values : np.ndarray
        The feature values to be used for training the model.
    labels_values : np.ndarray
        The label values to be used for training the model.
    logger : logging.Logger
        Logger for logging information during model fitting.
    available_ram : int | None
        The amount of available RAM in bytes.
    """
    jobs = 1
    if available_ram:
        jobs = int(math.floor(available_ram / memory_usage_psutil()))
        jobs = jobs if jobs >= 1 else 1
        if hasattr(clf, "n_jobs"):
            clf.n_jobs = jobs
        if hasattr(clf, "pre_dispatch"):
            clf.pre_dispatch = jobs
    logger.info(f"Fit model using {jobs} jobs")
    clf.fit(features_values, labels_values)


def sk_learn_regression(
    model_parameters: SciKitModelParams,
    apply_standardization: bool = False,
    cross_validation_params: CrossValidationParameters = CrossValidationParameters(),
    available_ram: int | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Train a scikit-learn regression model with optional standardization and cross-validation.

    This function sets up the model parameters, applies standardization if specified,
    and trains the model using scikit-learn. It also supports cross-validation and
    saving the trained model to disk.

    Parameters
    ----------
    model_parameters : SciKitModelParams
        The parameters for the model, including database info, model name, output path,
        and any additional arguments.
    apply_standardization : bool, optional
        Flag indicating whether to apply standardization to the data. Defaults to False.
    cross_validation_params : CrossValidationParameters, optional
        The parameters for cross-validation.
        Defaults to an empty CrossValidationParameters instance.
    available_ram : int | None
        Amount of available RAM in MB to be used during the training process.
        Defaults to None.
    logger : logging.Logger, optional
        Logger for logging messages and progress. Defaults to a predefined global logger `LOGGER`.
    """
    logger.info(
        f"Features use to build model : {model_parameters.in_database.features_fields}"
    )
    assert model_parameters.in_database.features_fields
    features_values, labels_values, groups = get_df_features(
        str(model_parameters.in_database.db_file),
        model_parameters.in_database.features_fields,
        model_parameters.in_database.data_field,
        cross_validation_params.grouped,
    )
    scaler_features = None
    scaler_target = None
    if apply_standardization:
        logger.info("Applying standardization on features")
        scaler_features = StandardScaler()
        scaler_features.fit(features_values)
        if 0.0 in scaler_features.var_:
            logger.warning(
                "features std with 0 values was found, "
                "automatically replaced by 1 by scikit-learn"
            )
        features_values = scaler_features.transform(features_values)
        logger.info("Applying standardization on target")
        scaler_target = StandardScaler()
        labels_values = scaler_target.fit_transform(labels_values)

    # get scikit class corresponding to model name
    clf = model_name_to_function(model_parameters.model_name)
    if cross_validation_params.parameters:
        if not can_perform_cv(cross_validation_params.parameters, clf):
            fail_msg = (
                f"ERROR : impossible to cross validate the model `{model_parameters.model_name}` "
                f"with the parameters {list(cross_validation_params.parameters.keys())}"
            )
            logger.error(fail_msg)
            raise ValueError(fail_msg)

    if model_parameters.model_kwargs:
        clf = clf(**model_parameters.model_kwargs)
    else:
        clf = clf()

    if cross_validation_params.parameters:
        logger.info("Cross validation in progress")
        if cross_validation_params.grouped:
            splitter = list(
                GroupKFold(n_splits=cross_validation_params.folds).split(
                    features_values, labels_values, groups
                )
            )
        else:
            splitter = KFold(n_splits=cross_validation_params.folds)
        clf = GridSearchCV(
            clf,
            cross_validation_params.parameters,
            cv=splitter,
            return_train_score=True,
        )
    fit_model(clf, features_values, labels_values, logger, available_ram)
    save_model(
        clf,
        cross_validation_params,
        model_parameters.output_model,
        scaler_features,
        scaler_target,
    )


def sk_learn(
    model_parameters: SciKitModelParams,
    apply_standardization: bool = False,
    cross_validation_params: CrossValidationParameters = CrossValidationParameters(),
    available_ram: int | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Train a scikit-learn model with optional standardization and cross-validation.

    This function sets up the model parameters, applies standardization if specified,
    and trains the model using scikit-learn. It also supports cross-validation and
    saving the trained model to disk.

    Parameters
    ----------
    model_parameters : SciKitModelParams
        The parameters for the model, including database info, model name, output path,
        and any additional arguments.
    apply_standardization : bool, optional
        Flag indicating whether to apply standardization to the data. Defaults to False.
    cross_validation_params : CrossValidationParameters, optional
        The parameters for cross-validation.
        Defaults to an empty CrossValidationParameters instance.
    available_ram : int | None
        Amount of available RAM in MB to be used during the training process.
        Defaults to None.
    logger : logging.Logger, optional
        Logger for logging messages and progress. Defaults to a predefined global logger `LOGGER`.
    """
    logger.info(
        f"Features use to build model : {model_parameters.in_database.features_fields}"
    )
    assert model_parameters.in_database.features_fields
    features_values, labels_values, groups = get_df_features(
        str(model_parameters.in_database.db_file),
        model_parameters.in_database.features_fields,
        model_parameters.in_database.data_field,
        cross_validation_params.grouped,
    )
    scaler_features = None
    scaler_target = None
    if apply_standardization:
        logger.info("Applying standardization on features")
        scaler_features = StandardScaler()
        scaler_features.fit(features_values)
        if 0.0 in scaler_features.var_:
            logger.warning(
                "features std with 0 values was found, "
                "automatically replaced by 1 by scikit-learn"
            )
        features_values = scaler_features.transform(features_values)
    clf = model_name_to_function(model_parameters.model_name)
    if cross_validation_params.parameters:
        if not can_perform_cv(cross_validation_params.parameters, clf):
            fail_msg = (
                f"ERROR : impossible to cross validate the model `{model_parameters.model_name}` "
                f"with the parameters {list(cross_validation_params.parameters.keys())}"
            )
            logger.error(fail_msg)
            raise ValueError(fail_msg)
    if model_parameters.model_kwargs:
        clf = clf(**model_parameters.model_kwargs)
    else:
        clf = clf()

    force_proba(clf)

    if cross_validation_params.parameters:
        logger.info("Cross validation in progress")
        if cross_validation_params.grouped:
            splitter = list(
                GroupKFold(n_splits=cross_validation_params.folds).split(
                    features_values, labels_values, groups
                )
            )
        else:
            splitter = KFold(n_splits=cross_validation_params.folds)
        clf = GridSearchCV(
            clf,
            cross_validation_params.parameters,
            cv=splitter,
            return_train_score=True,
        )
    fit_model(clf, features_values, labels_values, logger, available_ram)
    save_model(
        clf,
        cross_validation_params,
        model_parameters.output_model,
        scaler_features,
        scaler_target,
    )
