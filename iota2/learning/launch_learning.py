#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module to start learning processes"""
import logging
import shutil
from pathlib import Path
from typing import Any

import numpy as np
from osgeo import ogr

import iota2.common.i2_constants as i2_const
from iota2.common import otb_app_bank as otb
from iota2.learning import train_sklearn
from iota2.learning.pytorch import train_pytorch_model
from iota2.learning.pytorch.train_pytorch_model import (
    DataLoaderParameters,
    ModelBuilder,
)
from iota2.learning.train_sklearn import CrossValidationParameters, SciKitModelParams
from iota2.typings.i2_types import DBInfo, ModelHyperParameters
from iota2.vector_tools import vector_functions as vf

I2_CONST = i2_const.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")


def get_features_labels(
    learning_vector: str,
    nb_no_features: int = 5,
) -> list[str]:
    """
    get target values in learning database

    learning_vector:
        inputdatabase (ncdf or sqlite)
    nb_no_features:
        number of feature to exclude
        if file extension is ".nc", force this parameter to 3
        except if force_nb_no_feature is True
    force_nb_no_features:
        prevent this function from overwriting the value of nb_no_features
        (added as a quick fix, this function should be refactored)
    """
    ext = Path(learning_vector).suffix

    if I2_CONST.i2_database_ext in ext:
        nb_no_features = 3
        fields = vf.get_all_fields_in_netcdf_df(learning_vector)
        if "ogc_fid" in fields:
            fields.remove("ogc_fid")
        if "geometry" in fields:
            fields.remove("geometry")
    else:
        driver = vf.get_driver(learning_vector)
        fields = vf.get_all_fields_in_shape(learning_vector, driver_name=driver)
    output_fields: list[str] = fields[nb_no_features::]
    return output_fields


def learn_scikitlearn_model(
    model_parameters: SciKitModelParams,
    cross_validation_parameters: CrossValidationParameters,
    apply_standardization: bool,
    available_ram: int,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Learn a model using scikit-learn.

    This function initializes model parameters from the provided database information,
    configures the model with the specified settings, and trains it using scikit-learn.
    Optionally, it can apply standardization and use cross-validation.

    Parameters
    ----------
    model_parameters : SciKitModelParams
        The parameters for the model, including database info, model name, output path,
        and any additional arguments.
    cross_validation_parameters : CrossValidationParameters
        The parameters for cross-validation.
    apply_standardization : bool
        Flag indicating whether to apply standardization to the data.
    available_ram : int
        Amount of available RAM in MB to be used during the training process.
    logger : logging.Logger, optional
        Logger for logging messages and progress. Defaults to a predefined global logger `LOGGER`.
    """
    assert isinstance(model_parameters.in_database.db_file, (str, Path))
    scikit_parameters = SciKitModelParams(
        DBInfo(
            model_parameters.in_database.data_field,
            model_parameters.in_database.db_file,
            features_fields=get_features_labels(
                str(model_parameters.in_database.db_file)
            ),
        ),
        model_parameters.model_name,
        model_parameters.output_model,
        model_parameters.model_kwargs,
    )
    train_sklearn.sk_learn(
        scikit_parameters,
        apply_standardization=apply_standardization,
        cross_validation_params=cross_validation_parameters,
        available_ram=available_ram,
        logger=logger,
    )


def learn_torch_model_regression(
    database: DBInfo,
    model_builder: ModelBuilder,
    model_hyperparameters: ModelHyperParameters,
    dataloader_params: DataLoaderParameters,
    working_dir: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Train a PyTorch regression model with the specified parameters.

    This function processes the database information, filters features and masks,
    copies the database to the working directory if specified, and trains the model
    using the provided PyTorch model builder and hyperparameters.

    Parameters
    ----------
    database : DBInfo
        The database information, including data field, database file, feature fields,
        and mask fields.
    model_builder : ModelBuilder
        The builder for creating the PyTorch model.
    model_hyperparameters : ModelHyperParameters
        The hyperparameters for the model training process.
    dataloader_params : DataLoaderParameters
        The parameters for the data loader.
    working_dir : str | None
        The working directory where the database file will be copied. Defaults to None.
    logger : logging.Logger, optional
        Logger for logging messages and progress. Defaults to a predefined global logger `LOGGER`.
    """
    assert isinstance(database.db_file, (Path, str))
    feat_labels = get_features_labels(str(database.db_file))
    masks_cols: list[str] | None
    masks_cols = []
    feat_cols = []
    for feat_label in feat_labels:
        if "mask" in feat_label.split("_")[1]:
            masks_cols.append(feat_label)
        else:
            feat_cols.append(feat_label)
    masks_cols = masks_cols or None
    samples_file = database.db_file
    if working_dir:
        dataset_name = Path(database.db_file).name
        shutil.copy(database.db_file, working_dir)
        samples_file = str(Path(working_dir) / dataset_name)
    database_updated = DBInfo(
        database.data_field,
        samples_file,
        features_fields=feat_cols,
        mask_fields=masks_cols,
        fids_field="originfid",
    )
    train_pytorch_model.torch_learn_regression(
        database_updated,
        model_builder,
        model_hyperparameters,
        dataloader_params,
        logger=logger,
    )


def learn_torch_model(
    database: DBInfo,
    model_builder: ModelBuilder,
    model_hyperparameters: ModelHyperParameters,
    dataloader_params: DataLoaderParameters,
    working_dir: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Train a PyTorch model with the specified parameters.

    This function processes the database information, filters features and masks,
    copies the database to the working directory if specified, and trains the model
    using the provided PyTorch model builder and hyperparameters.

    Parameters
    ----------
    database : DBInfo
        The database information, including data field, database file, feature fields,
        and mask fields.
    model_builder : ModelBuilder
        The builder for creating the PyTorch model.
    model_hyperparameters : ModelHyperParameters
        The hyperparameters for the model training process.
    dataloader_params : DataLoaderParameters
        The parameters for the data loader.
    working_dir : str | None
        The working directory where the database file will be copied. Defaults to None.
    logger : logging.Logger, optional
        Logger for logging messages and progress. Defaults to a predefined global logger `LOGGER`.
    """
    assert isinstance(database.db_file, (str, Path))
    feat_labels = get_features_labels(str(database.db_file))
    masks_cols: list[str] | None
    masks_cols = []
    feat_cols = []
    for feat_label in feat_labels:
        if "mask" in feat_label.split("_")[1]:
            masks_cols.append(feat_label)
        else:
            feat_cols.append(feat_label)
    masks_cols = masks_cols or None
    samples_file = database.db_file
    if working_dir:
        dataset_name = Path(samples_file).name
        shutil.copy(samples_file, working_dir)
        samples_file = str(Path(working_dir) / dataset_name)
    database_updated = DBInfo(
        database.data_field,
        samples_file,
        features_fields=feat_cols,
        mask_fields=masks_cols,
        fids_field="originfid",
    )
    train_pytorch_model.torch_learn(
        database_updated,
        model_builder,
        model_hyperparameters,
        dataloader_params,
        logger=logger,
    )


def learn_otb_model(
    train_params: dict[str, Any],
    classifier_options: dict[str, str | int | bool],
    region_field: str,
    ground_truth: str,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Trains an OTB model using the specified training parameters.

    This function updates the training parameters with feature labels and classifier options,
    computes statistics for SVM classifiers if necessary, and executes the training application.

    Parameters
    ----------
    train_params : dict[str, Any]
        A dictionary containing the training parameters.
    classifier_options : dict[str, str | int | bool]
        A dictionary containing specific options for the classifier.
    region_field : str
        The name of the field in the ground truth data that defines the regions for training.
    ground_truth : str
        The path to the ground truth vector data.
    logger : logging.Logger, optional
        A logger instance for logging information. Defaults to LOGGER.

    Raises
    ------
    AssertionError
        If 'svm' is in the classifier name and 'io.stats' is not provided in the
        training parameters.

    """
    updated_training_params = dict(train_params)
    features = get_features_labels(updated_training_params["io.vd"][0])
    updated_training_params["feat"] = features

    if classifier_options:
        updated_training_params = {**updated_training_params, **classifier_options}

    if "svm" in train_params["classifier"].lower():
        assert updated_training_params["io.stats"]
        if Path(updated_training_params["io.stats"]).exists():
            Path(updated_training_params["io.stats"]).unlink()
        write_stats_from_sample(
            train_params["io.vd"][0],
            updated_training_params["io.stats"],
            ground_truth,
            region_field,
        )
    logger.info(f"OTB train parameters : {updated_training_params}")
    app_train, _ = otb.create_application(
        otb.AvailableOTBApp.TRAIN_VECTOR_CLASSIFIER, updated_training_params
    )
    app_train.ExecuteAndWriteOutput()


def write_stats_from_sample(
    in_samples: str, out_stats: str, ground_truth: str, region_field: str
) -> None:
    """write statistics by reading samples database"""
    all_mean, all_std_dev = get_stats_from_samples(
        in_samples, ground_truth, region_field
    )

    with open(out_stats, "w", encoding="UTF-8") as stats_file:
        stats_file.write(
            '<?xml version="1.0" ?>\n\
            <FeatureStatistics>\n\
            <Statistic name="mean">\n'
        )
        for current_mean in all_mean:
            stats_file.write(
                '        <StatisticVector value="' + str(current_mean) + '" />\n'
            )
        stats_file.write(
            '    </Statistic>\n\
                            <Statistic name="stddev">\n'
        )
        for current_std in all_std_dev:
            stats_file.write(
                '        <StatisticVector value="' + str(current_std) + '" />\n'
            )
        stats_file.write(
            "    </Statistic>\n\
                            </FeatureStatistics>"
        )


def get_stats_from_samples(
    in_samples: str, ground_truth: str, region_field: str
) -> tuple[list[float], list[float]]:
    """get statistics (mean + std) from a database file

    Parameters
    ----------
    in_samples : str
        path to a database file (SQLite format) generated by iota2
    ground_truth : str
        path to the user database
    region_field : str
        region field

    """
    driver = ogr.GetDriverByName("SQLite")
    if driver.Open(in_samples, 0):
        data_source = driver.Open(in_samples, 0)
    else:
        raise Exception("Can not open : " + in_samples)

    layer = data_source.GetLayer()
    features_fields = vf.get_vector_features(ground_truth, region_field, in_samples)

    all_stat = []
    for current_band in features_fields:
        band_values = []
        for feature in layer:
            val = feature.GetField(current_band)
            if isinstance(val, (float, int)):
                band_values.append(val)
        band_values = np.asarray(band_values)
        all_stat.append((np.mean(band_values), np.std(band_values)))
    all_mean = [float(mean) for mean, stddev in all_stat]
    all_std_dev = [float(stddev) for mean, stddev in all_stat]
    return all_mean, all_std_dev
