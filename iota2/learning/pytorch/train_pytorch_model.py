#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""train pytorch models"""

import logging
import math
import pickle
from collections.abc import Callable
from dataclasses import asdict, dataclass, fields
from datetime import datetime
from pathlib import Path
from typing import Any, Literal, TypeVar

import numpy as np
import torch
import torch.nn.functional as F
from sklearn.metrics import accuracy_score, cohen_kappa_score, f1_score
from tabulate import tabulate
from torch.optim.lr_scheduler import ReduceLROnPlateau

from iota2.learning.pytorch.dataloaders import (
    database_to_dataloader,
    database_to_dataloader_batch_stream,
    database_to_dataloader_batch_stream_regression,
    database_to_dataloader_regression,
)
from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetworkFactory
from iota2.learning.pytorch.torch_utils import (
    EarlyStop,
    EpochsScores,
    VectorDataIndexPosition,
    plot_loss,
    plot_metrics,
    sensors_information,
    tensors_to_kwargs,
    update_confusion_matrix,
)
from iota2.typings.i2_types import DBInfo, ModelHyperParameters
from iota2.vector_tools.vector_functions import get_netcdf_df_column_values

LOGGER = logging.getLogger("distributed.worker")
MAX_SCORE = 100000  # high value to initialize metrics to minimize
# maps name to criterion
CRIT_NAME_DICT = {
    "loss": F.cross_entropy,
    "cross_entropy": F.cross_entropy,
    "MSE": F.mse_loss,
    "MAE": F.l1_loss,
    "HuberLoss": F.huber_loss,
}


@dataclass
class ValidationLosses:
    """stores regression validation metrics"""

    mse: float  # mean square error
    mae: float  # mean absolute error
    huberloss: float  # huber loss

    @classmethod
    def from_targ_pred(
        cls, target: torch.Tensor, predicted: torch.Tensor
    ) -> "ValidationLosses":
        """compute metrics from target values and predicted values"""
        # target and predicted are torch tensors
        # .numpy() for sklearn metrics
        return cls(
            mse=float(F.mse_loss(predicted, target)),
            mae=float(F.l1_loss(predicted, target)),
            huberloss=float(F.huber_loss(predicted, target)),
        )

    @classmethod
    def to_dict_of_lists(cls, list_of_self: list) -> dict:
        """takes a list of ValidationLosses and returns a dictionary mapping
        attribute name -> list of attribute values"""
        dol: dict = {key: [] for key in [field.name for field in fields(cls)]}
        for elt in list_of_self:
            delt = asdict(elt)
            for field in fields(elt):
                dol[field.name].append(delt[field.name])
        return dol


@dataclass
class Hyperparams:
    """stores hyperparameters"""

    learning_rate: float
    batch_size: int


@dataclass
class StatedictCheckpoint:
    """stores state_dict and checkpoint information"""

    score: float
    state_dict: dict | None = None
    epoch: int | None = None
    parameter_file: str | None = None
    enc: str | None = None
    hyperparameters: Hyperparams | None = None


BestCoefDict = dict[str, dict[str, int | None] | StatedictCheckpoint | None]


def criterion_name_to_class(name: str) -> Any:
    """takes a criterion name and returns the corresponding class"""
    if name not in CRIT_NAME_DICT:
        raise ValueError(
            f"optimization criterion {name} "
            f"not in available list: {CRIT_NAME_DICT.keys()}"
        )
    return CRIT_NAME_DICT[name]


ValidationLossesT = TypeVar("ValidationLossesT", bound="ValidationLosses")


def update_state_dict(
    o_acc: float,
    kappa: float,
    weighted_f1_score: float,
    valid_loss_mean: float,
    state_dict_best_coeff: dict[str, dict],
    model: torch.nn,
    current_epoch: int,
    model_parameters_file: str,
    encoder_convert_file: str,
    hyperparameters: ModelHyperParameters,
) -> dict[str, dict]:
    """update best state dict regarding input coeff

    Parameters
    ----------
    o_acc
        overall accuracy
    kappa
        kappa
    weighted_f1_score
        f-score
    valid_loss_mean
        loss mean across batches
    state_dict_best_coeff
        dictionary containing model.state_dict for every coeff
    model
        neural network
    current_epoch
        current epoch
    model_parameters_file
        path to a file containing a serialized dict to instantiate the model
    encoder_convert_file
        path to a file containing a serialized dict to decode labels
    hyperparameters
        hyperparameters

    Returns
    -------
    dict[str, dict]
    return an update of the input best step_dictionary
    """
    new_state_dict_best_coeff = state_dict_best_coeff.copy()
    if o_acc > new_state_dict_best_coeff["oa"]["score"]:
        new_state_dict_best_coeff["oa"]["score"] = o_acc
        new_state_dict_best_coeff["oa"]["state_dict"] = model.state_dict()
        new_state_dict_best_coeff["oa"]["epoch"] = current_epoch + 1
        new_state_dict_best_coeff["oa"]["parameter_file"] = model_parameters_file
        new_state_dict_best_coeff["oa"]["enc"] = encoder_convert_file
        new_state_dict_best_coeff["oa"]["hyperparameters"] = {
            "learning_rate": hyperparameters.learning_rate,
            "batch_size": hyperparameters.batch_size,
        }
    if kappa > new_state_dict_best_coeff["kappa"]["score"]:
        new_state_dict_best_coeff["kappa"]["score"] = kappa
        new_state_dict_best_coeff["kappa"]["state_dict"] = model.state_dict()
        new_state_dict_best_coeff["kappa"]["epoch"] = current_epoch + 1
        new_state_dict_best_coeff["kappa"]["parameter_file"] = model_parameters_file
        new_state_dict_best_coeff["kappa"]["enc"] = encoder_convert_file
        new_state_dict_best_coeff["kappa"]["hyperparameters"] = {
            "learning_rate": hyperparameters.learning_rate,
            "batch_size": hyperparameters.batch_size,
        }
    if weighted_f1_score > new_state_dict_best_coeff["fscore"]["score"]:
        new_state_dict_best_coeff["fscore"]["score"] = weighted_f1_score
        new_state_dict_best_coeff["fscore"]["state_dict"] = model.state_dict()
        new_state_dict_best_coeff["fscore"]["epoch"] = current_epoch + 1
        new_state_dict_best_coeff["fscore"]["parameter_file"] = model_parameters_file
        new_state_dict_best_coeff["fscore"]["enc"] = encoder_convert_file
        new_state_dict_best_coeff["fscore"]["hyperparameters"] = {
            "learning_rate": hyperparameters.learning_rate,
            "batch_size": hyperparameters.batch_size,
        }
    if valid_loss_mean < new_state_dict_best_coeff["loss"]["score"]:
        new_state_dict_best_coeff["loss"]["score"] = valid_loss_mean
        new_state_dict_best_coeff["loss"]["state_dict"] = model.state_dict()
        new_state_dict_best_coeff["loss"]["epoch"] = current_epoch + 1
        new_state_dict_best_coeff["loss"]["parameter_file"] = model_parameters_file
        new_state_dict_best_coeff["loss"]["enc"] = encoder_convert_file
        new_state_dict_best_coeff["loss"]["hyperparameters"] = {
            "learning_rate": hyperparameters.learning_rate,
            "batch_size": hyperparameters.batch_size,
        }
    return new_state_dict_best_coeff


@dataclass
class ModelBuilder:
    """
    A data class that encapsulates the parameters needed to build and save
    a machine learning model.

    Attributes
    ----------
    dl_name:
        The name of the deep learning model.
    model_path:
        The file path where the model will be saved.
    model_parameters_file:
        The file path where the model's parameters will be saved.
    encoder_convert_file:
        The file path for the encoder conversion file.
    dl_module:
        The module containing the neural network definition, by default None.
    """

    dl_name: str
    model_path: str
    model_parameters_file: str
    encoder_convert_file: str
    dl_module: str | None = None


@dataclass
class DataLoaderParameters:
    """
    A data class that holds parameters for configuring data loaders.

    Attributes
    ----------
    num_workers:
        The number of worker threads to use for data loading, by default 1.
    dataloader_mode:
        The mode for data loading, either 'full' to load the entire dataset into memory
        or 'stream' to load data in batches from disk, by default 'stream'.
    """

    num_workers: int = 1
    dataloader_mode: Literal["full", "stream"] = "stream"


def learning_state_initialization(
    database: DBInfo,
    model_builder: ModelBuilder,
    model_hyperparameters: ModelHyperParameters,
    dataloader_params: DataLoaderParameters,
    nb_class: int,
    mode: Literal["classif", "regression"] = "classif",
) -> tuple:
    """
    Initialize the learning state for training the model.

    Parameters
    ----------
    database:
        Information about the database.
    model_builder:
        Information about the model builder.
    model_hyperparameters:
        Hyperparameters for the model.
    dataloader_params:
        Parameters for configuring data loaders.
    nb_class:
        Number of classes.
    mode:
        Mode of operation, either 'classif' for classification or 'regression' for regression,
        by default 'classif'.

    Returns
    -------
    tuple
        A tuple containing the following elements:
        - model: The initialized model.
        - stats: Statistics related to the model.
        - first_epoch: The starting epoch.
        - early_stop_obj: Object for early stopping.
        - scheduler: Scheduler for learning rate adjustments.
        - train_loader: DataLoader for training data.
        - valid_loader: DataLoader for validation data.
        - label_convert_dic: Dictionary for label conversion.
        - dataset_sensor_pos: Position information of dataset sensors.
        - optimizer: Optimizer for the model.
        - scores: Dataclass containing scores (see the TrainingScores class definition)
        - state_dict_best_coeff: Dictionary to store the best coefficients.
        - batch_prov_train: Provider for training batches.
        - batch_prov_valid: Provider for validation batches.
        - checkpoint_model_file: File path for saving checkpoints.
    """
    stats = None
    train_loader = None
    valid_loader = None
    label_convert_dic = None
    dataset_sensor_pos = None

    checkpoint_model_file = model_builder.model_path.replace(".txt", "_checkpoint.txt")
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    assert database.features_fields is not None

    model = Iota2NeuralNetworkFactory().get_nn(
        model_builder.dl_name,
        model_hyperparameters.dl_parameters,
        sensors_information(database.features_fields),
        nb_class,
        model_builder.model_parameters_file,
        model_builder.dl_module,
    )

    optimizer = torch.optim.Adam(
        model.parameters(), lr=model_hyperparameters.learning_rate
    )
    scores = EpochsScores()
    state_dict_best_coeff: BestCoefDict = {}
    if mode == "classif":
        state_dict_best_coeff = {
            "kappa": {
                "score": -1,
                "state_dict": None,
                "epoch": None,
                "hyperparameters": None,
            },
            "loss": {
                "score": 10000,
                "state_dict": None,
                "epoch": None,
                "hyperparameters": None,
            },
            "fscore": {
                "score": -1,
                "state_dict": None,
                "epoch": None,
                "hyperparameters": None,
            },
            "oa": {
                "score": -1,
                "state_dict": None,
                "epoch": None,
                "hyperparameters": None,
            },
        }

    # in regression mode, the valid_kappa/fscore/oa are not used
    elif mode == "regression":
        init_hyp = Hyperparams(0.0, 0)
        state_dict_best_coeff = {
            "mse": StatedictCheckpoint(MAX_SCORE, None, None, None, None, init_hyp),
            "mae": StatedictCheckpoint(MAX_SCORE, None, None, None, None, init_hyp),
            "huberloss": StatedictCheckpoint(
                MAX_SCORE, None, None, None, None, init_hyp
            ),
        }

    first_epoch = 0
    batch_prov_train = batch_prov_valid = None
    early_stop_obj: EarlyStop | None = None
    scheduler: ReduceLROnPlateau | None = None
    if model_hyperparameters.enable_early_stop:
        early_stop_obj = EarlyStop(
            model_hyperparameters.epoch_to_trigger,
            model_hyperparameters.early_stop_patience,
            model_hyperparameters.early_stop_tol,
            model_hyperparameters.early_stop_metric,
        )
    if model_hyperparameters.adaptive_lr:
        model_hyperparameters.adaptive_lr["optimizer"] = optimizer
        scheduler = ReduceLROnPlateau(**model_hyperparameters.adaptive_lr)
    compute_stats = True
    checkpoint_stats = None
    if (
        Path(checkpoint_model_file).is_file()
        and model_hyperparameters.restart_from_checkpoint
    ):
        compute_stats = False
        (
            model,
            optimizer,
            checkpoint_epoch,
            state_dict_best_coeff,
            scores,
            batch_prov_train,
            batch_prov_valid,
            early_stop_obj,
            scheduler,
            checkpoint_stats,
        ) = load_checkpoint(checkpoint_model_file, model, optimizer, mode=mode)
        first_epoch = checkpoint_epoch + 1
        # according to:
        # https://discuss.pytorch.org/t/loading-a-saved-model-for-continue-training/17244/3
        for state in optimizer.state.values():
            for key, val in state.items():
                if isinstance(val, torch.Tensor):
                    state[key] = val.to(device)

    # dataloader instance must be created
    # after the variables coming from the checkpoint
    if dataloader_params.dataloader_mode == "stream":
        dataloader_func = database_to_dataloader_batch_stream
        if mode == "regression":
            dataloader_func = database_to_dataloader_batch_stream_regression
        (
            train_loader,
            valid_loader,
            label_convert_dic,
            dataset_sensor_pos,
            stats,
        ) = dataloader_func(
            database,
            model_hyperparameters,
            dataloader_params.num_workers,
            (batch_prov_train, batch_prov_valid),
            compute_stats,
        )
        batch_prov_train = train_loader.dataset.fids_provider
        batch_prov_valid = valid_loader.dataset.fids_provider

    elif dataloader_params.dataloader_mode == "full":
        batch_prov_train = batch_prov_valid = None
        data_loader_func = database_to_dataloader
        if mode == "regression":
            data_loader_func = database_to_dataloader_regression
        (
            train_loader,
            valid_loader,
            label_convert_dic,
            dataset_sensor_pos,
            stats,
        ) = data_loader_func(
            database,
            model_hyperparameters.batch_size,
            dataloader_params.num_workers,
            compute_stats,
            model_hyperparameters.additional_statistics_percentage,
        )
        stats = checkpoint_stats
    model.set_stats(stats)
    return (
        model,
        stats,
        first_epoch,
        early_stop_obj,
        scheduler,
        train_loader,
        valid_loader,
        label_convert_dic,
        dataset_sensor_pos,
        optimizer,
        scores,
        state_dict_best_coeff,
        batch_prov_train,
        batch_prov_valid,
        checkpoint_model_file,
    )


def update_state_dict_regression(
    state_dict_best_coeff: dict[str, StatedictCheckpoint],
    valid_loss: ValidationLosses,
    model: torch.nn,
    current_epoch: int,
    model_builder: ModelBuilder,
    model_hyperparameters: ModelHyperParameters,
) -> None:
    """
    Update the state dictionary for the best regression metrics.

    This function updates the state dictionary with the model's state and hyperparameters if the
    current validation loss is better (i.e., lower) than the best recorded value for each metric.

    It assumes that all regression metrics need to be minimized (e.g., MSE, MAE, Huber loss).
    If a metric's current validation loss is lower than the best recorded loss,
    the state dictionary is updated with the current model's state, epoch number,
    and hyperparameters.

    Parameters
    ----------
    state_dict_best_coeff : dict[str, StatedictCheckpoint]
        A dictionary where keys are metric names and values are `StatedictCheckpoint` objects
        storing the best model state for each metric.
    valid_loss : ValidationLosses
        An object containing the current validation losses for the relevant metrics.
    model : torch.nn
        The neural network model being evaluated.
    current_epoch : int
        The current epoch number in the training process.
    model_builder : ModelBuilder
        An object containing the model's file paths and encoder information.
    model_hyperparameters : ModelHyperParameters
        An object containing the hyperparameters of the model including learning rate and
        batch size.
    """
    vld = asdict(valid_loss)

    for metric in vld.keys():
        metric_value = vld[metric]
        state_dict: StatedictCheckpoint = state_dict_best_coeff[metric]
        if metric_value < state_dict.score and state_dict.hyperparameters:
            state_dict.score = metric_value
            state_dict.state_dict = model.state_dict()
            state_dict.epoch = current_epoch + 1
            state_dict.parameter_file = model_builder.model_parameters_file
            state_dict.enc = model_builder.encoder_convert_file
            state_dict.hyperparameters.learning_rate = (
                model_hyperparameters.learning_rate
            )
            state_dict.hyperparameters.batch_size = model_hyperparameters.batch_size


def doy_by_sensors(
    features_labels: list[str], logger: logging.Logger = LOGGER
) -> dict[str, dict[str, list[int] | int]]:
    """for each sensor's features return the number of days from the first feature

    Example
    -------
    features_labels = ["sentinel2_b1_20150101", "sentinel2_b1_20150110"]
    doy = doy_by_sensors(features_labels)
    >>> doy["doy"] = [0, 9]
    >>> doy["features_per_date"] = 1
    """
    sensors_dates: dict[str, dict[str, list]] = {}
    sensors_doy: dict[str, dict[str, list[int] | int]] = {}
    for feature_labels in features_labels:
        try:
            sensor_name, feat, date_time_str = feature_labels.split("_")
            if sensor_name == "sentinel1":
                sensor_name = f"{sensor_name}_{feat}"
            if sensor_name not in sensors_dates:
                sensors_dates[sensor_name] = {"dates": [], "features_per_date": []}
            date_time_obj = datetime.strptime(date_time_str, "%Y%m%d")
            if date_time_obj not in sensors_dates[sensor_name]["dates"]:
                sensors_dates[sensor_name]["dates"].append(date_time_obj)
            nb_component = len(
                [
                    elem
                    for elem in features_labels
                    if sensor_name in elem and date_time_str in elem
                ]
            )
            sensors_dates[sensor_name]["features_per_date"].append(nb_component)
        except ValueError:
            logger.warning(f"{feature_labels} cannot be converted to DOY")
    sensors_dates_sorted: dict[str, dict] = {}
    for sensor_name, sensor_date_dict in sensors_dates.items():
        sensors_dates_sorted[sensor_name] = {}
        sensors_dates_sorted[sensor_name]["dates"] = sorted(sensor_date_dict["dates"])
        nb_components = sensor_date_dict["features_per_date"]
        if nb_components.count(nb_components[0]) != len(nb_components):
            raise ValueError(
                "There is not the same number of features per date "
                f"in the sensor {sensor_name}: "
                f"sensors_dates = {sensors_dates} "
                f"features_labels = {features_labels}"
            )
        sensors_dates_sorted[sensor_name]["features_per_date"] = sensors_dates[
            sensor_name
        ]["features_per_date"][0]

    for sensor_name, dic in sensors_dates_sorted.items():
        first_date = dic["dates"][0]
        delta_days = [(date - first_date).days for date in dic["dates"]]
        sensors_doy[sensor_name] = {
            "doy": delta_days,
            "features_per_date": int(
                sensors_dates_sorted[sensor_name]["features_per_date"]
            ),
        }

    return sensors_doy


def load_checkpoint(
    checkpoint_file: str,
    model: torch.nn.Module,
    optimizer: torch.optim.Optimizer,
    mode: Literal["classif", "regression"] = "classif",
) -> tuple:
    """
    mode:
        "classif" or "regression"
        regression use different metrics (not kappa, fscore, oa)
    """
    if not Path(checkpoint_file).exists():
        raise ValueError(f"checkpoint file : '{checkpoint_file} does not exists'")
    scores = EpochsScores()
    with open(checkpoint_file, "rb") as checkpoint:
        checkpoint_dic = pickle.load(checkpoint)
        batch_prov_train, batch_prov_valid = checkpoint_dic["batch_provider"]
        ckp_ep = checkpoint_dic["epoch_checkpoint"]
        ckp_model = checkpoint_dic["current_model_state"]
        ckp_optim = checkpoint_dic["current_optimizer_state"]
        state_dict_best_coeff = checkpoint_dic["best_coeff"]
        if mode == "classif":
            scores.kappa = checkpoint_dic["coeff_history"]["kappa"]
            scores.fscore = checkpoint_dic["coeff_history"]["fscore"]
            scores.o_acc = checkpoint_dic["coeff_history"]["oa"]
        elif mode == "regression":
            # WARN abuse kappa field to store a list of ValidationLosses
            scores.kappa = checkpoint_dic["coeff_history"]["kappa"]
            scores.fscore = []
            scores.o_acc = []
        scores.valid_loss = checkpoint_dic["coeff_history"]["loss_val"]
        scores.train_loss = checkpoint_dic["coeff_history"]["loss_train"]
        ckp_early_stop = checkpoint_dic["early_stop"]
        ckp_scheduler = checkpoint_dic["scheduler"]
        ckp_stats = checkpoint_dic["stats"]
    optimizer.load_state_dict(ckp_optim)
    model.load_state_dict(ckp_model)
    return (
        model,
        optimizer,
        ckp_ep,
        state_dict_best_coeff,
        scores,
        batch_prov_train,
        batch_prov_valid,
        ckp_early_stop,
        ckp_scheduler,
        ckp_stats,
    )


def torch_learn_regression(
    database: DBInfo,
    model_builder: ModelBuilder,
    model_hyperparameters: ModelHyperParameters,
    dataloader_params: DataLoaderParameters,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Train a deep learning regression model using PyTorch with the specified parameters.

    This function handles the training process, including data loading, model initialization,
    training loop, validation, early stopping, learning rate scheduling, and checkpointing.

    The function performs the following steps:
    1. Unpacks and verifies parameters.
    2. Initializes the model, criterion, optimizer, DataLoader, and other necessary components.
    3. Executes the training loop, including training and validation at each epoch.
    4. Applies early stopping and learning rate scheduling if specified.
    5. Saves checkpoints at regular intervals.
    6. Logs and prints training and validation metrics.
    7. Saves the final model and training figures (loss and confusion metrics).

    Parameters
    ----------
    database : DBInfo
        Information about the database, including the file path, data field, and feature fields.
    model_builder : ModelBuilder
        Configuration for the model, including paths for saving the model and encoder files.
    model_hyperparameters : ModelHyperParameters
        Hyperparameters for the model, such as learning rate, batch size, number of epochs,
        optimization criterion, and early stopping parameters.
    dataloader_params : DataLoaderParameters
        Parameters for the DataLoader, including the mode (e.g., "stream") and other relevant
        settings.
    logger : logging.Logger, optional
        Logger for logging information during the training process. Defaults to LOGGER.

    Raises
    ------
    ValueError
        If the model path does not end with '.txt'.
    """
    if model_hyperparameters.adaptive_lr is None:
        model_hyperparameters.adaptive_lr = {}
    if ".txt" not in model_builder.model_path:
        raise ValueError("models has to be save under '.txt'")

    nb_class = 1
    assert database.features_fields is not None
    logger.info(f"There are {len(database.features_fields)} input features.")

    model_hyperparameters.dl_parameters["doy_sensors_dic"] = doy_by_sensors(
        database.features_fields, logger=logger
    )
    criterion = criterion_name_to_class(
        model_hyperparameters.model_optimization_criterion
    )
    (
        model,
        stats,
        first_epoch,
        early_stop_obj,
        scheduler,
        train_loader,
        valid_loader,
        label_convert_dic,
        dataset_sensor_pos,
        optimizer,
        scores,
        state_dict_best_coeff,
        batch_prov_train,
        batch_prov_valid,
        checkpoint_model_file,
    ) = learning_state_initialization(
        database,
        model_builder,
        model_hyperparameters,
        dataloader_params,
        1,
        mode="regression",
    )
    print(f"START EPOCH : {database.db_file}")
    learning_state_dict = {"stats": stats}
    checkpoints_interval_cpt = 0
    for epoch_num in np.arange(first_epoch, model_hyperparameters.epochs):
        checkpoints_interval_cpt += 1
        if dataloader_params.dataloader_mode == "stream":
            train_loader.dataset.load_batch_series(epoch_num)
            valid_loader.dataset.load_batch_series(epoch_num)
        train_loss_mean = train_model(
            criterion,
            dataloader_params.dataloader_mode,
            dataset_sensor_pos,
            model,
            optimizer,
            train_loader,
            model_hyperparameters.weighted_labels,
            mode="regression",
        )
        scores.train_loss.append(train_loss_mean)
        logger.info(f"Epoch {epoch_num} train loss mean : {train_loss_mean}")
        with torch.no_grad():
            all_preds, all_targets, _, valid_loss_mean = validate_model(
                criterion,
                dataloader_params.dataloader_mode,
                dataset_sensor_pos,
                model,
                nb_class,
                valid_loader,
                model_hyperparameters.weighted_labels,
                mode="regression",
            )
            scores.valid_loss.append(valid_loss_mean)
            logger.info(f"Epoch {epoch_num} valid loss mean : {valid_loss_mean}")
            # classification metrics
            regr_valid_loss: ValidationLosses = ValidationLosses.from_targ_pred(
                all_targets, all_preds
            )
            # WARN abusing valid_kappa to store list[ValidationLosses]
            scores.kappa.append(regr_valid_loss)
            update_state_dict_regression(
                state_dict_best_coeff,
                regr_valid_loss,
                model,
                epoch_num,
                model_builder,
                model_hyperparameters,
            )

        if model_hyperparameters.adaptive_lr:
            scheduler.step(valid_loss_mean)
        if checkpoints_interval_cpt - model_hyperparameters.checkpoints_interval == 0:
            checkpoints_interval_cpt = 0
            create_checkpoint(
                checkpoint_model_file,
                learning_state_dict,
                state_dict={
                    "scheduler": scheduler,
                    "early_stop": early_stop_obj,
                    "batch_provider": (batch_prov_train, batch_prov_valid),
                    "epoch_checkpoint": epoch_num,
                    "current_model_state": model.state_dict(),
                    "current_optimizer_state": optimizer.state_dict(),
                    "best_coeff": state_dict_best_coeff,
                    "coeff_history": {
                        "kappa": scores.kappa,
                        "loss_val": scores.valid_loss,
                        "loss_train": scores.train_loss,
                    },
                },
            )

    display_best_coeff(
        model_hyperparameters.batch_size,
        model_hyperparameters.learning_rate,
        logger,
        state_dict_best_coeff,
        mode="regression",
    )
    model.save(
        model_builder.model_path,
        state_dict_best_coeff,
        stats,
        database.features_fields,
        device="cpu",
    )
    with open(model_builder.encoder_convert_file, "wb") as enc_file:
        pickle.dump(label_convert_dic, enc_file, protocol=pickle.HIGHEST_PROTOCOL)

    # save figures
    output_loss_figure = model_builder.model_path.replace(".txt", "_loss.png")
    output_conf_metrics_figure = model_builder.model_path.replace(
        ".txt", "_confusion_metrics.png"
    )
    scores = EpochsScores(
        ValidationLosses.to_dict_of_lists(scores.kappa)["mse"],
        ValidationLosses.to_dict_of_lists(scores.kappa)["mae"],
        ValidationLosses.to_dict_of_lists(scores.kappa)["huberloss"],
        scores.train_loss,
        scores.valid_loss,
    )
    plot_metrics(
        scores,
        output_conf_metrics_figure,
        model_hyperparameters.learning_rate,
        model_hyperparameters.batch_size,
        labels=["mse", "mae", "huberloss"],
    )
    plot_loss(
        scores,
        output_loss_figure,
        model_hyperparameters.learning_rate,
        model_hyperparameters.batch_size,
        logscale=False,
    )


def torch_learn(
    database: DBInfo,
    model_builder: ModelBuilder,
    model_hyperparameters: ModelHyperParameters,
    dataloader_params: DataLoaderParameters,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Train a deep learning model using PyTorch with the specified parameters.

    This function handles the training process, including data loading, model initialization,
    training loop, validation, early stopping, learning rate scheduling, and checkpointing.

    The function performs the following steps:
    1. Unpacks and verifies parameters.
    2. Initializes the model, criterion, optimizer, DataLoader, and other necessary components.
    3. Executes the training loop, including training and validation at each epoch.
    4. Applies early stopping and learning rate scheduling if specified.
    5. Saves checkpoints at regular intervals.
    6. Logs and prints training and validation metrics.
    7. Saves the final model and training figures (loss and confusion metrics).

    Parameters
    ----------
    database : DBInfo
        Information about the database, including the file path, data field, and feature fields.
    model_builder : ModelBuilder
        Configuration for the model, including paths for saving the model and encoder files.
    model_hyperparameters : ModelHyperParameters
        Hyperparameters for the model, such as learning rate, batch size, number of epochs,
        optimization criterion, and early stopping parameters.
    dataloader_params : DataLoaderParameters
        Parameters for the DataLoader, including the mode (e.g., "stream") and other relevant
        settings.
    logger : logging.Logger, optional
        Logger for logging information during the training process. Defaults to LOGGER.

    Raises
    ------
    ValueError
        If the model path does not end with '.txt'.
    """
    if model_hyperparameters.adaptive_lr is None:
        model_hyperparameters.adaptive_lr = {}
    if ".txt" not in model_builder.model_path:
        raise ValueError("models has to be save under '.txt'")

    assert isinstance(database.db_file, (str, Path))
    nb_class = len(
        set(get_netcdf_df_column_values(str(database.db_file), database.data_field))
    )
    assert database.features_fields is not None
    logger.info(
        f"Found {nb_class} classes in the dataset "
        f"There are {len(database.features_fields)} input features."
    )

    model_hyperparameters.dl_parameters["doy_sensors_dic"] = doy_by_sensors(
        database.features_fields, logger=logger
    )
    # define criterion based on the given name
    criterion = criterion_name_to_class(
        model_hyperparameters.model_optimization_criterion
    )
    (
        model,
        stats,
        first_epoch,
        early_stop_obj,
        scheduler,
        train_loader,
        valid_loader,
        label_convert_dic,
        dataset_sensor_pos,
        optimizer,
        scores,
        state_dict_best_coeff,
        batch_prov_train,
        batch_prov_valid,
        checkpoint_model_file,
    ) = learning_state_initialization(
        database,
        model_builder,
        model_hyperparameters,
        dataloader_params,
        nb_class,
        mode="classif",
    )
    print(f"START EPOCH : {database.db_file}")
    learning_state_dict = {"stats": stats}
    checkpoints_interval_cpt = 0
    for epoch_num in np.arange(first_epoch, model_hyperparameters.epochs):
        checkpoints_interval_cpt += 1
        if dataloader_params.dataloader_mode == "stream":
            train_loader.dataset.load_batch_series(epoch_num)
            valid_loader.dataset.load_batch_series(epoch_num)
        train_loss_mean = train_model(
            criterion,
            dataloader_params.dataloader_mode,
            dataset_sensor_pos,
            model,
            optimizer,
            train_loader,
            model_hyperparameters.weighted_labels,
        )
        scores.train_loss.append(train_loss_mean)
        logger.info(
            f"Epoch {epoch_num} train loss mean : {math.log(train_loss_mean)} dB"
        )
        with torch.no_grad():
            all_preds, all_targets, conf_matrix, valid_loss_mean = validate_model(
                criterion,
                dataloader_params.dataloader_mode,
                dataset_sensor_pos,
                model,
                nb_class,
                valid_loader,
                model_hyperparameters.weighted_labels,
                "classif",
            )
            scores.valid_loss.append(valid_loss_mean)
            logger.info(
                f"Epoch {epoch_num} valid loss mean : {math.log(valid_loss_mean)} dB"
            )
            # classification metrics
            assert isinstance(conf_matrix, torch.Tensor)
            logger.info(f"\n{tabulate(conf_matrix.numpy())}\n")
            kappa, o_acc, weighted_f1_score = get_scores(all_preds, all_targets)
            print(
                f"ep {epoch_num} F1-score {weighted_f1_score} - OA {o_acc} - Kappa {kappa}"
            )
            scores.kappa.append(kappa)
            scores.o_acc.append(o_acc)
            scores.fscore.append(weighted_f1_score)
            state_dict_best_coeff = update_state_dict(
                o_acc,
                kappa,
                weighted_f1_score,
                valid_loss_mean,
                state_dict_best_coeff,
                model,
                epoch_num,
                model_builder.model_parameters_file,
                model_builder.encoder_convert_file,
                model_hyperparameters,
            )
        if model_hyperparameters.enable_early_stop:
            case = {
                "oa": o_acc,
                "kappa": kappa,
                "fscore": weighted_f1_score,
                "train_loss": math.log(train_loss_mean),
                "val_loss": math.log(valid_loss_mean),
            }
            if early_stop_obj.step(score=case[model_hyperparameters.early_stop_metric]):
                break
        if model_hyperparameters.adaptive_lr:
            scheduler.step(valid_loss_mean)
        if checkpoints_interval_cpt - model_hyperparameters.checkpoints_interval == 0:
            checkpoints_interval_cpt = 0
            create_checkpoint(
                checkpoint_model_file,
                learning_state_dict,
                state_dict={
                    "scheduler": scheduler,
                    "early_stop": early_stop_obj,
                    "batch_provider": (batch_prov_train, batch_prov_valid),
                    "epoch_checkpoint": epoch_num,
                    "current_model_state": model.state_dict(),
                    "current_optimizer_state": optimizer.state_dict(),
                    "best_coeff": state_dict_best_coeff,
                    "coeff_history": {
                        "kappa": scores.kappa,
                        "fscore": scores.fscore,
                        "oa": scores.o_acc,
                        "loss_val": scores.valid_loss,
                        "loss_train": scores.train_loss,
                    },
                },
            )

    display_best_coeff(
        model_hyperparameters.batch_size,
        model_hyperparameters.learning_rate,
        logger,
        state_dict_best_coeff,
    )
    model.save(
        model_builder.model_path,
        state_dict_best_coeff,
        stats,
        database.features_fields,
        device="cpu",
    )
    with open(model_builder.encoder_convert_file, "wb") as enc_file:
        pickle.dump(label_convert_dic, enc_file, protocol=pickle.HIGHEST_PROTOCOL)

    # save figures
    output_loss_figure = model_builder.model_path.replace(".txt", "_loss.png")
    output_conf_metrics_figure = model_builder.model_path.replace(
        ".txt", "_confusion_metrics.png"
    )
    plot_metrics(
        scores,
        output_conf_metrics_figure,
        model_hyperparameters.learning_rate,
        model_hyperparameters.batch_size,
        labels=["kappa", "fscore", "overall accuracy"],
    )
    plot_loss(
        scores,
        output_loss_figure,
        model_hyperparameters.learning_rate,
        model_hyperparameters.batch_size,
        logscale=True,
    )


def get_scores(
    all_preds: torch.Tensor, all_targets: torch.Tensor
) -> tuple[float, float, float]:
    """
    Compute classification metrics based on predictions and targets.

    This function calculates the weighted F1 score, overall accuracy (OA),
    and Cohen's kappa score from the given predictions and targets.

    The metrics are computed as follows:
    - Weighted F1 score: Evaluates the F1 score for each class and computes the average,
      weighted by the number of true instances for each class.
    - Overall accuracy (OA): The ratio of correctly predicted instances to the total instances.
    - Cohen's kappa score: A statistic that measures inter-rater agreement for categorical items,
      taking into account the possibility of agreement occurring by chance.

    Parameters
    ----------
    all_preds : torch.Tensor
        The predicted labels as a tensor.
    all_targets : torch.Tensor
        The true labels as a tensor.

    Returns
    -------
    tuple[float, float, float]
        A tuple containing the Cohen's kappa score, overall accuracy (OA),
        and weighted F1 score, in that order.
    """
    weighted_f1_score = f1_score(
        all_targets.numpy(), all_preds.numpy(), average="macro"
    )
    o_acc = accuracy_score(all_targets.numpy(), all_preds.numpy())
    kappa = cohen_kappa_score(all_targets.numpy(), all_preds.numpy())
    return kappa, o_acc, weighted_f1_score


def display_best_coeff(
    batch_size: int,
    learning_rate: float,
    logger: logging.Logger,
    state_dict_best_coeff: dict,
    mode: Literal["classif", "regression"] = "classif",
) -> None:
    """
    Display and log the best coefficients from the training process.

    This function iterates over the state dictionary of the best coefficients,
    printing and logging the best value, the epoch at which it was achieved,
    and the hyperparameters used during training.

    The function logs and prints messages in the following format:
    "Coeff : {coeff_name}, best_value : {best_val} at epoch : {epoch}, with hyperparameters :
    batch_size = {batch_size}, learning_rate = {learning_rate}"
    Parameters
    ----------
    batch_size:
        The size of the batch used during training.
    learning_rate:
        The learning rate used during training.
    logger:
        The logger instance for logging the messages.
    state_dict_best_coeff:
        A dictionary containing the best coefficients, their scores, and the epochs at
        which they were achieved.
    mode:
        The mode of the model, either "classif" for classification or "regression" for regression,
        by default "classif".
    """
    for coeff_name, coeff_dict in state_dict_best_coeff.items():
        best_val = coeff_dict["score"] if mode == "classif" else coeff_dict.score
        epoch = coeff_dict["epoch"] if mode == "classif" else coeff_dict.epoch
        msg = (
            f"Coeff : {coeff_name}, best_value : {best_val} "
            f"at epoch : {epoch}, with hyperparameters : "
            f"batch_size = {batch_size}, learning_rate = {learning_rate}\n"
        )
        print(msg)
        logger.info(msg)


def create_checkpoint(
    checkpoint_model_file: str, learning_state_dict: dict, state_dict: dict
) -> None:
    """
    Create and save a checkpoint of the model's learning state.

    This function saves the current state of the model, including various parameters
    and statistics, to a file using the pickle module. This allows for the resumption
    of training or analysis from a specific point.

    Parameters
    ----------
    checkpoint_model_file:
        The file path where the checkpoint will be saved.
    learning_state_dict:
        A dictionary containing the current learning state of the model.
    state_dict:
        A dictionary containing additional state information to be added to the learning state.
    """
    with open(checkpoint_model_file, "wb") as model_to_save:
        learning_state_dict.update(state_dict)
        pickle.dump(
            learning_state_dict,
            model_to_save,
            protocol=pickle.HIGHEST_PROTOCOL,
        )


def validate_model(
    criterion: Callable,
    dataloader_mode: Literal["stream", "full"],
    dataset_sensor_pos: VectorDataIndexPosition,
    model: torch.nn.Module,
    nb_class: int,
    valid_loader: torch.utils.data.DataLoader,
    weighted_labels: bool,
    mode: Literal["classif", "regression"] = "classif",
) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor | None, float]:
    """
    Validate the model on the validation dataset.

    This function performs validation of the model using the provided validation data loader.
    It computes the validation loss, updates the confusion matrix (for classification),
    and collects predictions and targets for metric calculation.

    Parameters
    ----------
    criterion:
        The loss function used to compute the loss between the predictions and targets.
    dataloader_mode:
        Mode of the data loader, either 'stream' or other modes indicating batch processing.
    dataset_sensor_pos:
        A dictionary indicating the positions of various data tensors within the dataset.
    model:
        The neural network model to be validated.
    nb_class:
        The number of classes in the dataset (used for classification).
    valid_loader:
        DataLoader providing the validation data.
    weighted_labels:
        Whether to apply weights to the labels in the loss computation.
    mode:
        The mode of validation, either 'classif' for classification or 'regression'.

    Returns
    -------
    tuple
        A tuple containing the following elements:
        - all_preds (torch.Tensor): All predictions made by the model.
        - all_targets (torch.Tensor): All target values from the validation data.
        - conf_matrix (torch.Tensor or None): The confusion matrix for classification tasks.
        - valid_loss_mean (float): The mean validation loss for the epoch.
    """
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model.eval()
    valid_loss_loader = []
    conf_matrix = None
    if mode == "classif":
        conf_matrix = torch.zeros(nb_class, nb_class, dtype=torch.int32)
    all_preds = torch.zeros(0)
    all_targets = torch.zeros(0)
    weights_val = 0
    for _, valid_data_batch in enumerate(valid_loader):
        if dataloader_mode == "stream":
            targets_val = np.squeeze(
                valid_data_batch[dataset_sensor_pos.target], axis=0
            ).to(device, non_blocking=False)
            if mode == "classif":
                weights_val = np.squeeze(
                    valid_data_batch[dataset_sensor_pos.weight], axis=0
                ).to(device, non_blocking=False)
        else:
            targets_val = valid_data_batch[dataset_sensor_pos.target].to(
                device, non_blocking=False
            )
            if mode == "classif":
                weights_val = valid_data_batch[dataset_sensor_pos.weight].to(
                    device, non_blocking=False
                )
        valid_model_tensors_kwargs = tensors_to_kwargs(
            valid_data_batch, dataset_sensor_pos, dataloader_mode == "stream"
        )
        pred = model(**valid_model_tensors_kwargs)
        if mode == "classif":
            loss_pred = criterion(
                pred,
                targets_val,
                reduce=False,
            )
            loss_pred = (
                torch.mean(loss_pred * weights_val)
                if weighted_labels
                else torch.mean(loss_pred)
            )
            conf_matrix, all_preds, all_targets = update_confusion_matrix(
                pred.cpu(),
                targets_val.cpu(),
                conf_matrix,
                all_preds,
                all_targets,
            )
        elif mode == "regression":
            # in regression mode, this is not cross_entropy,
            # it corresponds to the selected optimization loss
            loss_pred = criterion(pred, targets_val)
            # no confusion matrix in regression mode
            # neither argmax decoding
            all_preds = torch.cat((all_preds.float(), pred.cpu().float()))
            all_targets = torch.cat((all_targets.float(), targets_val.cpu()))
        valid_loss_loader.append(loss_pred.item())
    valid_loss_mean = sum(valid_loss_loader) / len(valid_loss_loader)
    model.to("cpu")
    return all_preds, all_targets, conf_matrix, valid_loss_mean


def train_model(
    criterion: Callable,
    dataloader_mode: Literal["stream", "full"],
    dataset_sensor_pos: VectorDataIndexPosition,
    model: torch.nn.Module,
    optimizer: torch.optim.Adam,
    train_loader: torch.utils.data.DataLoader,
    weighted_labels: bool,
    mode: Literal["classif", "regression"] = "classif",
) -> float:
    """
    Train the model for one epoch.

    This function performs a single epoch of training on the provided model using the
    specified training data loader. It computes the loss, performs backpropagation, and
    updates the model weights.

    Parameters
    ----------
    criterion:
        The loss function used to compute the loss between the predictions and targets.
        Currently on of F.cross_entropy, F.mse_loss, F.l1_loss or F.huber_loss.
    dataloader_mode:
        Mode of the data loader, either 'stream' or 'full' modes indicating batch processing.
    dataset_sensor_pos:
        A dictionary indicating the positions of various data sensors tensors within the dataset.
    model:
        The neural network model to be trained.
    optimizer:
        The optimizer used to update the model's weights.
    train_loader:
        DataLoader providing the training data.
    weighted_labels:
        Whether to apply weights to the labels in the loss computation.
    mode:
        The mode of training, either 'classif' for classification or 'regression'.

    Returns
    -------
    float
        The mean training loss for the epoch.
    """
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model.to(device)
    model.train()
    train_loss_loader = []
    for _, train_data_batch in enumerate(train_loader):
        train_model_tensors_kwargs = tensors_to_kwargs(
            train_data_batch, dataset_sensor_pos, dataloader_mode == "stream"
        )
        weights = 0
        if dataloader_mode == "stream":
            targets = np.squeeze(
                train_data_batch[dataset_sensor_pos.target], axis=0
            ).to(device, non_blocking=False)
            if mode == "classif":
                weights = np.squeeze(
                    train_data_batch[dataset_sensor_pos.weight], axis=0
                ).to(device, non_blocking=False)
        else:
            targets = train_data_batch[dataset_sensor_pos.target].to(
                device, non_blocking=False
            )
            if mode == "classif":
                weights = train_data_batch[dataset_sensor_pos.weight].to(
                    device, non_blocking=False
                )
        y_hat = model(**train_model_tensors_kwargs)

        if mode == "classif" and weighted_labels:
            loss = criterion(y_hat, targets, reduce=False)
            loss = torch.mean(loss * weights)
        else:
            loss = criterion(y_hat, targets)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        train_loss_loader.append(loss.item())
    train_loss_mean: float = sum(train_loss_loader) / len(train_loss_loader)
    return train_loss_mean
