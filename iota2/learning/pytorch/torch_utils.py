#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Utility functions for working with pytorch models"""
import functools
import math
import random
import warnings
from collections.abc import Generator
from dataclasses import dataclass, field, fields
from typing import Any, Literal

import numpy as np
import torch
from sklearn.preprocessing import StandardScaler

from iota2.common.file_utils import sort_by_first_elem
from iota2.learning.utils import I2Label, I2TemporalLabel, i2_label_factory

warnings.filterwarnings("ignore")


@dataclass
class SensorStats:
    """
    A class to represent statistical data for a sensor.

    Attributes
    ----------
    mean : torch.Tensor
        The mean of the values recorded by the sensor.
    var : torch.Tensor
        The variance of the values recorded by the sensor.
    max : torch.Tensor | None
        The maximum value recorded by the sensor (default is None).
    min : torch.Tensor | None
        The minimum value recorded by the sensor (default is None).
    user_stats : dict[str, torch.Tensor] | None
        A dictionary of additional user-defined statistics (default is None).
    """

    mean: torch.Tensor
    var: torch.Tensor
    max: torch.Tensor | None = None
    min: torch.Tensor | None = None
    user_stats: dict[str, torch.Tensor] | None = None


@dataclass
class SensorIndexPosition:
    """
    A data class that holds the positions of sensor data and mask tensors.

    Attributes
    ----------
    data : int | None
        The position index of the data tensor for the sensor.
    mask : int | None
        The position index of the mask tensor for the sensor, if applicable.
        Default is None.
    """

    data: int | None = None
    mask: int | None = None


@dataclass
class VectorDataIndexPosition:
    """
    A data class that holds the positions of target, weight, and sensor tensors.

    Attributes
    ----------
    target : int
        The position index of the target tensor.
    weight : int
        The position index of the weight tensor.
    sensors : dict[str, SensorIndexPosition]
        A dictionary where each key is a sensor name and each value is a
        `SensorIndexPosition` indicating the positions of data and mask
        tensors for that sensor.
    """

    target: int
    sensors: dict[str, SensorIndexPosition]
    weight: int | None = None


def sensors_mask_from_labels_to_index(
    masks_labels: list[I2TemporalLabel],
) -> dict:
    """guess sensors masks labels from the list of features mask"""
    sensors_list = []
    sensors_index = {}
    for feat_label in list(masks_labels):
        sensor_name, feat_name = feat_label.sensor_name, feat_label.feat_name
        if sensor_name.lower() == "sentinel1":
            sensor_name = f"{sensor_name}_{feat_name[0:3]}".lower()
        if sensor_name not in sensors_list:
            sensors_list.append(sensor_name)
    for sensor in sensors_list:
        indexes = [
            ind
            for ind, label in enumerate(masks_labels)
            if sensor.lower() in str(label).lower()
        ]
        bands_per_dates = sort_by_first_elem(
            [(masks_labels[ind].date, masks_labels[ind]) for ind in indexes]
        )
        # check if every date contain the same number of bands
        bands_count = [len(date_bands) for _, date_bands in bands_per_dates]
        if bands_count.count(1) != len(bands_count):
            raise ValueError(f"duplicated mask found for the sensor '{sensor}'")
        sensors_index[sensor] = {
            "index": indexes,
        }
    return sensors_index


def sensors_from_labels_to_index(
    features_labels: list[str], masks_labels: list[I2Label | str] | None = None
) -> dict:
    """guess sensors positions thanks to features order

    Parameters
    ----------
    features_labels
        feature's names. Must not contain mask's name
    masks_labels
        mask's name
    """

    sensors_list = []
    sensors_index = {}

    # get all sensors from labels and create a list
    for feat_label in list(features_labels):
        i2_feat_label = i2_label_factory(feat_label)
        sensor_name, feat_name = i2_feat_label.sensor_name, i2_feat_label.feat_name
        if sensor_name.lower() == "sentinel1":
            sensor_name = f"{sensor_name}_{feat_name}".lower()
        if sensor_name not in sensors_list:
            sensors_list.append(sensor_name)

    # get index of interest and nb component for each sensor
    for sensor in sensors_list:
        indexes = [
            ind
            for ind, label in enumerate(features_labels)
            if sensor.lower() in label.lower()
        ]

        bands_per_dates = sort_by_first_elem(
            [
                (features_labels[ind].split("_")[-1], features_labels[ind])
                for ind in indexes
            ]
        )
        # check if every date contain the same number of bands
        bands_count = [len(date_bands) for _, date_bands in bands_per_dates]
        if bands_count.count(bands_count[0]) != len(bands_count):
            raise ValueError("nb dates component does not match")

        if isinstance(i2_label_factory(features_labels[indexes[0]]), I2TemporalLabel):
            sensors_index[sensor] = {"index": indexes, "nb_date_comp": bands_count[0]}
        else:
            sensors_index[sensor] = {"index": indexes, "nb_date_comp": None}
    masks_dic = {}
    if masks_labels:
        masks_i2_labels = [
            i2_label_factory(label) if isinstance(label, str) else label
            for label in masks_labels
        ]

        masks_dic = sensors_mask_from_labels_to_index(masks_i2_labels)
        for sensor_name, sensor_dic in sensors_index.items():
            if "sentinel1_des" in sensor_name:
                sensor_dic["mask_index"] = masks_dic["sentinel1_des"]["index"]
            elif "sentinel1_asc" in sensor_name:
                sensor_dic["mask_index"] = masks_dic["sentinel1_asc"]["index"]
            else:
                sensor_dic["mask_index"] = masks_dic[sensor_name]["index"]
    return sensors_index


def sensors_information(features_labels: list[str]) -> dict:
    """Get sensors information : sensor name, nb dates, nb bands.

    it's helpful for setting deep models layer parameters.
    """
    features_labels = list(filter(lambda x: "mask" not in x, features_labels))
    sensors_index = sensors_from_labels_to_index(features_labels)
    out_sensors_information = {}
    for sensors_name, meta in sensors_index.items():
        if meta["nb_date_comp"]:
            out_sensors_information[sensors_name] = {
                "nb_dates": int(len(meta["index"]) / meta["nb_date_comp"]),
                "nb_components": meta["nb_date_comp"],
            }
        else:
            out_sensors_information[sensors_name] = {
                "nb_dates": None,
                "nb_components": len(meta["index"]),
            }
    return out_sensors_information


class StatsCalculator:
    """compute stats by batch or considering sample of the dataset"""

    def __init__(
        self,
        dataloader: torch.utils.data.DataLoader,
        sensors_ind: VectorDataIndexPosition,
        squeeze_first: bool,
        subsample: float | None = None,
        min_stat: bool = False,
        max_stat: bool = False,
        var_stat: bool = False,
        expected_quantiles: list[float] | None = None,
        custom_stats_on_subset: dict | None = None,
    ):
        """
        Parameters
        ----------
        dataloader
            pytorch dataloader
        sensors_ind
            dictionary as :
            {'sentinel2': {'data': 0, 'mask': 1}, 'target': 2, 'weight': 3}
            which describe sensors position in data coming from dataloader
        subsample
            percentage [0;1] of the full dataset to compute stats on it
        expected_quantiles
            list of quantiles to compute (thanks to numpy.quantile).
            Quantiles will be computed using the subsample dataset.
        custom_stats_on_subset
            Dictionnary of partial function to compute stats on
            the subsample dataset. Key as the statistics label

            >>> custom_stats_on_subset["quantile"] = functools.partial(
                get_quantiles, **{"quantiles": [0.1, 0.5, 0.95]})

            if the subsample dataset contains 3 samples and 2 Sentinel-2
            dates (10 bandes each), then the function will receive
            a tensor of data with the shape (3, 20)
        squeeze_first:
            useful to squeeze first dimension of tensors
        Warning
        -------
        data provided by the dataloder must be in
        (batch, date, component) shape
        """
        if custom_stats_on_subset is None:
            custom_stats_on_subset = {}
        if expected_quantiles is None:
            expected_quantiles = [0.1, 0.5, 0.95]
        self.dataloader = dataloader
        self.sensors_ind = sensors_ind
        self.subsample = subsample
        self.min_stat = min_stat
        self.max_stat = max_stat
        self.var_stat = var_stat
        self.mean_stat = True
        self.squeeze_first = squeeze_first
        self.device = (
            torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        )

        self.custom_stats_on_subset = custom_stats_on_subset
        if f"quantile_{expected_quantiles[0]}" not in custom_stats_on_subset:
            for quantile in expected_quantiles:
                self.custom_stats_on_subset[f"quantile_{quantile}"] = functools.partial(
                    self.get_quantiles, **{"quantile": quantile}  # type: ignore
                )

    def get_quantiles(self, sensor_data: np.ndarray, quantile: Any) -> torch.Tensor:
        """compute nanquantiles (ignore nans)"""
        return torch.tensor(np.nanquantile(sensor_data, quantile, axis=0)).to(
            self.device
        )

    def get_max(self, sensor_data: np.ndarray) -> torch.tensor:
        """np.nanmax() -> ignore nan in array, torch 1.4 does not"""
        return torch.tensor(np.nanmax(sensor_data, axis=0)).to(self.device)

    def get_min(self, sensor_data: np.ndarray) -> torch.tensor:
        """np.nanmin() -> ignore nan in array, torch 1.4 does not"""
        return torch.tensor(np.nanmin(sensor_data, axis=0)).to(self.device)

    def check_full_date(
        self,
        input_shape: tuple,
        indices: list[int],
    ) -> bool:
        """according to a input shape formatted as (batch_size, dates, bands)
        check if all indexes belong to the same dates -> return True if
        indices belong to a a list of full dates.
        """
        try:
            _, dates, bands = input_shape
        except ValueError:
            _, bands = input_shape
            dates = 1
        hist, _ = np.histogram(indices, bins=range(0, (dates * bands) + bands, bands))
        return all(val in (0, bands) for val in hist)

    @classmethod
    def stats_to_device(
        cls, stats: dict[str, SensorStats], device: str
    ) -> dict[str, SensorStats]:
        """place stats tensor to the expected device"""
        tmp_stats: dict[str, SensorStats] = {}
        for sensor_name, sensor_stats in stats.items():
            values = {}
            for sensor_field in fields(sensor_stats):
                if sensor_field.name != "user_stats":
                    val = getattr(sensor_stats, sensor_field.name).to(device)
                else:
                    val = {}
                    user_features_stats = getattr(sensor_stats, sensor_field.name)
                    if user_features_stats:
                        for (
                            user_feat_name,
                            user_feat_val,
                        ) in user_features_stats.items():
                            val[user_feat_name] = user_feat_val.to(device)
                values[sensor_field.name] = val
            tmp_stats[sensor_name] = SensorStats(**values)
        return tmp_stats

    def compute_sub_stats(
        self, stats: dict, subset_data: dict[str, np.ndarray]
    ) -> dict[str, SensorStats]:
        """Compute stats on data set aside."""
        updated_stats = stats.copy()
        output_stats: dict[str, SensorStats] = {}
        assert subset_data
        for sensor_name, sensor_data in subset_data.items():
            del updated_stats[sensor_name]["scaler"]
            means = updated_stats[sensor_name]["mean"]
            is_nan = torch.isnan(means)
            indices = is_nan.nonzero()
            excluded_indices_stats = [int(elem[0]) for elem in indices]
            if not self.check_full_date(
                updated_stats[sensor_name]["shape"], excluded_indices_stats
            ):
                raise ValueError(
                    "iota2 detected a date containing a band full of NaNs"
                    ", something go wrong during the deepL stats"
                    " computation"
                )
            del updated_stats[sensor_name]["shape"]

            if self.subsample:
                updated_stats[sensor_name]["user_stats"] = {}
                for stat_name, func in self.custom_stats_on_subset.items():
                    stat_vals = func(sensor_data)
                    updated_stats[sensor_name]["user_stats"][stat_name] = stat_vals

            output_stats[sensor_name] = SensorStats(**updated_stats[sensor_name])

        return output_stats

    def compute(self) -> dict[str, SensorStats]:
        """
        Compute statistics for each sensor in the dataset.

        This function iterates over batches of data from the dataloader, processes the
        sensor data, and computes statistics such as mean, variance, maximum, and minimum
        values for each sensor. It uses a `StandardScaler` to fit the data and optionally
        subsamples the data for additional statistical computations.

        Returns
        -------
        dict[str, SensorStats]
            A dictionary where each key is the sensor name and the value is a `SensorStats`
            dataclass containing computed statistics for that sensor.

        Example
        -------
        >>> stats = self.compute()
        >>> print(stats['sentinel2'].mean)
        """
        stats: dict = {}
        subset_data: dict = {}
        for batch_num, batch in enumerate(self.dataloader):
            values = tensors_to_kwargs(
                batch, self.sensors_ind, self.squeeze_first, as_tuple=True
            )
            if batch_num == 0:
                self.initialize_stats(stats, subset_data, values)
            self.process_batch(stats, subset_data, values)
        output_stats = self.compute_sub_stats(stats, subset_data)
        # attempt to free memory
        self.subsample = None
        return output_stats

    def process_batch(self, stats: dict, subset_data: dict, values: dict) -> None:
        """
        Process a single batch of data and update statistics for each sensor.

        This function iterates over the sensor data in the batch, reshapes the data,
        applies masks, handles subsampling if required, and updates the statistics
        (mean, variance, etc.) for each sensor using a `StandardScaler`.

        Parameters
        ----------
        stats : dict
            A dictionary where keys are sensor names and values are dictionaries containing
            statistics scalers and other related information.
        subset_data : dict
            A dictionary to hold subsets of the data for additional statistical computations
            when subsampling is enabled.
        values : dict
            A dictionary where keys are sensor names and values are tuples containing
            the sensor data tensor and its corresponding mask tensor.
        """
        for sensor_name, (batch_data, batch_mask) in values.items():
            if batch_data is None:
                continue
            (
                batch_data,
                batch_mask,
                batch_size,
                nb_comp,
                nb_dates,
            ) = self.reshape_batch_data(batch_data, batch_mask)
            batch_data[batch_mask != 0] = np.nan
            samples_ind = None
            if self.subsample:
                samples_ind = random.sample(
                    range(batch_size), int(self.subsample * batch_size)
                )
            batch_reshape = batch_data.reshape((batch_size, nb_dates * nb_comp))
            stats[sensor_name]["scaler"].partial_fit(batch_reshape)
            self.update_stats(
                batch_reshape, sensor_name, stats, subset_data, samples_ind
            )

    def update_stats(
        self,
        batch_reshape: np.ndarray,
        sensor_name: str,
        stats: dict,
        subset_data: dict,
        samples_ind: list[int] | None = None,
    ) -> None:
        """
        Update statistical measures for a given sensor based on the reshaped batch data.

        This function updates various statistics such as mean, variance, max, and min
        for the given sensor. It also handles subsampling if enabled and stores the
        subsampled data for further analysis.

        Parameters
        ----------
        batch_reshape : np.ndarray
            The reshaped batch data as a NumPy array.
        samples_ind : list[int] or None
            List of indices for subsampling the batch data. If subsampling is not
            enabled, this is None.
        sensor_name : str
            The name of the sensor whose statistics are being updated.
        stats : dict
            A dictionary where keys are sensor names and values are dictionaries
            containing statistics scalers and other related information.
        subset_data : dict
            A dictionary to hold subsets of the data for additional statistical
            computations when subsampling is enabled.
        """
        if self.mean_stat:
            stats[sensor_name]["mean"] = torch.tensor(
                stats[sensor_name]["scaler"].mean_
            ).to(self.device)
        if self.var_stat:
            stats[sensor_name]["var"] = torch.tensor(
                stats[sensor_name]["scaler"].var_
            ).to(self.device)
        if self.subsample and samples_ind:
            if len(subset_data[sensor_name]):
                subset_data[sensor_name] = np.concatenate(
                    (subset_data[sensor_name], batch_reshape[samples_ind])
                )
            else:
                subset_data[sensor_name] = batch_reshape[samples_ind]
        if self.max_stat:
            batch_max = self.get_max(batch_reshape)
            if "max" not in stats[sensor_name]:
                stats[sensor_name]["max"] = batch_max
            else:
                stats[sensor_name]["max"] = self.get_max(
                    torch.stack([stats[sensor_name]["max"].cpu(), batch_max.cpu()])
                )
        if self.min_stat:
            batch_min = self.get_min(batch_reshape)
            if "min" not in stats[sensor_name]:
                stats[sensor_name]["min"] = batch_min
            else:
                stats[sensor_name]["min"] = self.get_min(
                    torch.stack([stats[sensor_name]["min"].cpu(), batch_min.cpu()])
                )

    @staticmethod
    def reshape_batch_data(
        batch_data: torch.Tensor, batch_mask: torch.Tensor | None = None
    ) -> tuple[torch.Tensor, torch.Tensor, int, int, int]:
        """
        Reshape and prepare batch data and batch mask for further processing.

        This function handles the reshaping of batch data and the corresponding mask
        depending on whether the data is 2D or 3D. It ensures that the data and mask
        are correctly formatted for subsequent statistical computations.

        Parameters
        ----------
        batch_data : torch.Tensor
            The batch data tensor, which can be either 2D or 3D.
        batch_mask : torch.Tensor or None
            The batch mask tensor, which can be either 2D or 3D. If None, a mask of
            zeros is created.

        Returns
        -------
        batch_data : torch.Tensor
            The reshaped batch data as a torch.Tensor.
        batch_mask : torch.Tensor
            The reshaped batch mask as a torch.Tensor.
        batch_size : int
            The size of the batch.
        nb_comp : int
            The number of components (features) in the batch data.
        nb_dates : int
            The number of dates (time steps) in the batch data, set to 1 for 2D data.
        """
        if len(batch_data.shape) == 3:
            batch_size, nb_dates, nb_comp = batch_data.shape
            if batch_mask is None:
                batch_mask = torch.ones(batch_size, nb_dates, 1) * 0.0
            batch_mask = batch_mask.cpu()
            batch_mask = np.dstack([batch_mask] * nb_comp)
        elif len(batch_data.shape) == 2:
            batch_size, nb_comp = batch_data.shape
            batch_mask = torch.ones(batch_size, nb_comp) * 0.0
            batch_mask = batch_mask.cpu()
            nb_dates = 1
        else:
            raise ValueError("data must me 2D or 3D")
        batch_data = np.array(batch_data.cpu())
        return batch_data, batch_mask, batch_size, nb_comp, nb_dates

    def initialize_stats(self, stats: dict, subset_data: dict, values: dict) -> None:
        """
        Initialize the statistics and subset data for each sensor based on the first batch of data.

        This function sets up the initial structures for collecting and computing
        statistics for each sensor. It creates a `StandardScaler` for each sensor and
        stores the shape of the batch data. It also initializes an empty tensor for
        storing a subset of data for each sensor.

        Parameters
        ----------
        stats : dict
            A dictionary to store the statistics for each sensor. Each entry is
            initialized with a `StandardScaler` and the shape of the batch data.
        subset_data : dict
            A dictionary to store a subset of data for each sensor. Each entry is
            initialized as an empty tensor.
        values : dict
            A dictionary containing the batch data and masks for each sensor. The keys
            are sensor names and the values are tuples of (batch_data, batch_mask).
        """
        for sensor_name, (batch_data, _) in values.items():
            if "mask" in sensor_name or batch_data is None:
                continue
            stats[sensor_name] = {
                "scaler": StandardScaler(),
                "shape": batch_data.shape,
            }
            subset_data[sensor_name] = torch.tensor([])


def tensors_to_kwargs(
    batch_tensor: torch.Tensor,
    sensors_ind: VectorDataIndexPosition,
    squeeze_first_dim: bool,
    as_tuple: bool = False,
) -> dict:
    """translate data coming from dataloader to fed the nn
    forward method
    """
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    out_tensors = {}
    for sensors_name, sensor_ind_position in sensors_ind.sensors.items():
        data = batch_tensor[sensor_ind_position.data]
        if squeeze_first_dim:
            out_tensors[sensors_name] = np.squeeze(data, axis=0).to(
                device, non_blocking=False
            )
        else:
            out_tensors[sensors_name] = data.to(device, non_blocking=False)
        sensor_mask = None
        if sensor_ind_position.mask:
            sensor_mask = batch_tensor[sensor_ind_position.mask].to(
                device, non_blocking=False
            )
            if squeeze_first_dim:
                sensor_mask = np.squeeze(
                    batch_tensor[sensor_ind_position.mask], axis=0
                ).to(device, non_blocking=False)
        if as_tuple:
            out_tensors[f"{sensors_name}"] = (
                out_tensors[f"{sensors_name}"],
                sensor_mask,
            )
        else:
            if sensor_ind_position.mask:
                out_tensors[f"{sensors_name}_masks"] = sensor_mask
    return out_tensors


class EarlyStop:
    """stop the learning stage on conditions"""

    def __init__(
        self,
        epoch_to_trigger: int,
        patience: int,
        tol: float,
        metric: Literal["train_loss", "val_loss", "fscore", "oa", "kappa"] = "val_loss",
    ):
        """

        Parameters
        ----------
        epoch_to_trigger:
            epoch to start monitoring score
        patience:
            number of checks with no improvement after which
            the training will be stopped
        tol:
            minimum change in the monitored quantity to qualify
            as an improvement.
            if metric is 'train_loss' or 'valid_loss' then tol must be in dB.
        metric:
            metric to be monitored ['train_loss', 'val_loss',
                                    'fscore', 'oa', 'kappa']
            default is val_loss
        """
        self.epoch_to_trigger = epoch_to_trigger
        self.patience = patience
        self.tol = tol
        self.metric = metric
        available_metrics = ["train_loss", "val_loss", "fscore", "oa", "kappa"]
        if self.metric not in available_metrics:
            raise ValueError(f"metric parameter must be in {available_metrics}")

        self.current_epoch = 0
        self.patience_counter = 0
        self.last_score: float

    def step(self, score: float) -> bool:
        """
        return True if an early stop is required
        """
        self.current_epoch += 1
        flag = False
        if self.current_epoch >= self.epoch_to_trigger:
            if self.current_epoch != 1:
                diff = abs(score - self.last_score)
                worst = (
                    (score - self.last_score) > 0
                    if "loss" in self.metric
                    else (score - self.last_score) < 0
                )
                if worst or (diff <= self.tol and not worst):
                    self.patience_counter += 1
                else:
                    self.patience_counter = 0
                self.last_score = score
                flag = self.patience_counter >= self.patience
        else:
            self.last_score = score
        return flag


class BatchProvider:
    """Randomly distribute fids accros batches. This random distribution
    is done as many times as there is epochs.
    """

    def __init__(self, possible_fids_list: list[int], batch_size: int, nb_epoch: int):
        """
        Parameters
        ----------

        possible_fids_lits
            list of possible fids (index in the database)
        batch_size
            size of the batch
        nb_epoch
            number of epochs

        Examples
        --------
        >>> fids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        >>> batch_size = 5
        >>> nb_epoch = 2
        >>> batch_p = BatchProvider(fids, batch_size, nb_epoch)
        >>> batch_p.get_epoch(0)
        >>> batch_p.get_epoch(1)
        [[7, 9, 10, 8], [6, 4, 1, 5], [2, 3]]
        [[6, 2, 10, 1], [4, 3, 7, 5], [9, 8]]
        """
        self.possible_fids_list = possible_fids_list
        self.batch_size = batch_size

        self.batch_ep = []
        for _ in range(nb_epoch):
            random.shuffle(self.possible_fids_list)
            batch = []
            for elem in self.batch_generator(self.possible_fids_list, batch_size):
                batch.append(elem)
            self.batch_ep.append(batch)
        self.nb_batch = len(self.batch_ep[0])

    def get_epoch(self, ep_num: int) -> list[list[int]]:
        """
        Get epoch using given number

        Parameters
        ----------
        ep_num:
            Epoch number
        """
        return self.batch_ep[ep_num]

    def __len__(self) -> int:
        return self.nb_batch

    def batch_generator(self, my_list: list[int], size: int) -> Generator:
        """generate batch of data

        Parameters
        ----------
        my_list
            list of data (usally fids)
        size
            batch's size
        """
        ind = 0
        while ind < len(my_list):
            yield my_list[ind : ind + size]
            ind += size


def update_confusion_matrix(
    pred: torch.Tensor,
    targets: torch.Tensor,
    conf_matrix: torch.Tensor,
    all_preds: torch.Tensor,
    all_targets: torch.Tensor,
) -> tuple[torch.Tensor, ...]:
    """
    Update confusion matrix

    Parameters
    ----------
    pred:
        Model's predictions
    targets: torch.Tensor
        Target labels
    conf_matrix: torch.Tensor
        Current confusion matrix
    all_preds: torch.Tensor
        Previous model predictions
    all_targets: torch.Tensor
        Previous target labels.

    Notes
    -----
        Returns the updated confusion matrix and the corresponding predictions and labels
    """
    local_cm = torch.zeros_like(conf_matrix)
    decoded_predictions = torch.argmax(pred, 1)
    decoded_targets = targets

    for target, prediction in zip(decoded_targets, decoded_predictions):
        local_cm[target, prediction] += 1
    return (
        conf_matrix + local_cm,
        torch.cat((all_preds.float(), decoded_predictions.float())),
        torch.cat((all_targets.float(), decoded_targets.float())),
    )


@dataclass
class EpochsScores:
    """
    A data class for storing various performance metrics over multiple epochs.

    Attributes
    ----------
    kappa : list[float] | None, default=None
        List of Cohen's kappa scores for each epoch.
    fscore : list[float] | None, default=None
        List of F1 scores for each epoch.
    o_acc : list[float] | None, default=None
        List of overall accuracy scores for each epoch.
    train_loss : list[float] | None, default=None
        List of training losses for each epoch.
    valid_loss : list[float] | None, default=None
        List of validation losses for each epoch.
    """

    kappa: list[float] | None = field(default_factory=list)
    fscore: list[float] | None = field(default_factory=list)
    o_acc: list[float] | None = field(default_factory=list)
    train_loss: list[float] | None = field(default_factory=list)
    valid_loss: list[float] | None = field(default_factory=list)


def plot_metrics(
    scores: EpochsScores,
    output_path: str,
    learning_rate: float,
    batch_size: int,
    labels: list[str],
) -> None:
    """
    Plot various metrics over epochs and save the plot to a file.

    This function plots Cohen's kappa, F1 score, and overall accuracy over epochs.
    It also includes the batch size and learning rate as annotations in the legend.

    Parameters
    ----------
    scores : EpochsScore
        An instance of EpochsScore containing metrics for each epoch.
    output_path : str
        The file path where the plot will be saved.
    learning_rate : float
        The learning rate used during training.
    batch_size : int
        The batch size used during training.
    labels : list[str]
        Labels for the metrics to be plotted. Should contain three elements,
        corresponding to the labels for kappa, fscore, and o_acc.
    """
    # imported inside function on purpose.
    # If it's not, an unexpected segfault appears
    # pylint: disable=import-outside-toplevel
    import matplotlib
    import matplotlib.pyplot as plt

    matplotlib.get_backend()
    matplotlib.use("Agg")

    kappa = scores.kappa
    assert kappa
    fscore = scores.fscore
    o_acc = scores.o_acc
    epochs = np.arange(len(kappa))

    xsize = max(int(0.2 * len(kappa)), 10)
    ysize = 10
    _, axe = plt.subplots(figsize=(xsize, ysize))

    plt.plot(epochs, kappa, label=labels[0])
    plt.plot(epochs, fscore, label=labels[1])
    plt.plot(epochs, o_acc, label=labels[2])
    axe.set_ylabel("values")
    axe.set_title("metrics for each epochs for valid dataset")
    axe.set_xticks(epochs)
    axe.set_xticklabels([ep + 1 for ep in epochs], rotation=90)

    # plot hyperparameters values in legend
    plt.plot([], [], c="w", label=f"batch size {batch_size}")
    plt.plot([], [], c="w", label=f"learning rate {learning_rate}")

    axe.legend()
    plt.savefig(output_path, format="png", dpi=200)


def plot_loss(
    scores: EpochsScores,
    output_path: str,
    learning_rate: float,
    batch_size: int,
    logscale: bool = True,
) -> None:
    """
    Plot training and validation loss over epochs and save the plot to a file.

    This function plots training and validation loss over epochs.
    It can optionally plot the loss on a logarithmic scale.

    Parameters
    ----------
    scores : EpochsScore
        An instance of EpochsScore containing training and validation loss for each epoch.
    output_path : str
        The file path where the plot will be saved.
    learning_rate : float
        The learning rate used during training.
    batch_size : int
        The batch size used during training.
    logscale : bool, optional
        If True, the loss will be plotted on a logarithmic scale (default is True).
    """
    # imported inside function on purpose.
    # If it's not, an unexpected segfault appears
    # pylint: disable=import-outside-toplevel
    import matplotlib
    import matplotlib.pyplot as plt

    train_loss = scores.train_loss
    valid_loss = scores.valid_loss
    assert train_loss and valid_loss

    matplotlib.get_backend()
    matplotlib.use("Agg")

    if logscale:
        # list(map(math.log, train_loss))
        train_loss = [math.log(loss) for loss in train_loss]
        valid_loss = [math.log(loss) for loss in valid_loss]

    epochs = np.arange(len(train_loss))

    xsize = max(int(0.2 * len(train_loss)), 10)
    ysize = 10
    _, axe = plt.subplots(figsize=(xsize, ysize))

    plt.plot(epochs, train_loss, label="train loss")
    plt.plot(epochs, valid_loss, label="valid loss")
    ylabel = "log(loss)" if logscale else "loss"
    axe.set_ylabel(ylabel)
    axe.set_title(f"{ylabel} by epochs for train and valid dataset")
    axe.set_xticks(epochs)
    axe.set_xticklabels([ep + 1 for ep in epochs], rotation=90)

    plt.plot([], [], c="w", label=f"batch size {batch_size}")
    plt.plot([], [], c="w", label=f"learning rate {learning_rate}")

    axe.legend()
    plt.savefig(output_path, format="png", dpi=200)
