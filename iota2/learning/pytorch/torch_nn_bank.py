#!/usr/bin/env python3
# pylint: disable=invalid-name
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module containing all natively usable pytorch neural networks in iota2"""
import inspect
import pickle
from abc import ABC, ABCMeta, abstractmethod
from functools import reduce
from typing import Any

import torch
import torch.nn.functional as F
from torch import nn

from iota2.common.file_utils import verify_import
from iota2.common.service_error import NeuralNetworkConstructorError
from iota2.learning.pytorch.lightweight_temporal_attention_encoder import LTAE
from iota2.learning.pytorch.torch_utils import SensorStats, StatsCalculator

# global dictionary to register available models
# models must be registered using @register decorator
AVAIL_MODELS = {}


def register(model: Any) -> Any:
    """decorator used to register models in AVAIL_MODELS"""
    AVAIL_MODELS[model.__name__] = model
    return model


class NeuralNetworkMeta(ABCMeta):
    """Metaclass which check constructor definition.

    from
    https://stackoverflow.com/questions/55309793/python-enforce-specific-method-signature-for-subclasses
    """

    def __init__(
        cls, name: str, bases: list, attrs: dict
    ):  # pylint: disable=unused-argument
        for base_cls in bases:
            if base_cls.__class__ == cls.__class__:
                orig_argspec = inspect.getfullargspec(getattr(base_cls, "__init__"))
                current_argspec = inspect.getfullargspec(getattr(cls, "__init__"))
                for arg_name in orig_argspec.args:
                    if arg_name not in current_argspec.args:
                        raise NeuralNetworkConstructorError(
                            orig_argspec.args, current_argspec.args
                        )


class Iota2NeuralNetworkFactory:
    """Factory to a Iota2NeuralNetwork object."""

    def get_nn(
        self,
        nn_name: str,
        nn_parameters: dict,
        sensors_information: dict | None = None,
        nb_class: int | None = None,
        pickle_file: str | None = None,
        external_nn_module: str | None = None,
        random_seed_number: int = 42,
    ) -> nn.Module:
        """from the neural network name, instanciate the right
        pytorch model with the input parameters

        Note
        ----
        if pickle_file, then nn_parameters is save at pickle_file as
        a pickle object
        """
        torch_model = None
        nn_parameters_feat = {}
        nn_parameters_class: dict = {}
        if "sensors_information" not in nn_parameters:
            if not sensors_information:
                raise ValueError(
                    "sensors_information dictionary not found in 'dl_parameters' -> mandatory"
                )
            nn_parameters_feat = {"sensors_information": sensors_information}
        if "nb_class" not in nn_parameters:
            if not nb_class:
                raise ValueError(
                    "number of class not found in 'dl_parameters' -> mandatory"
                )
            nn_parameters_class = {"nb_class": nb_class}

        self.nn_parameters = {
            **nn_parameters_feat,
            **nn_parameters_class,
            **nn_parameters,
        }
        torch.manual_seed(random_seed_number)
        torch_model = self._get_nn(nn_name, external_nn_module)
        if pickle_file:
            with open(pickle_file, "wb") as file_instance_nn_parameters:
                pickle.dump(
                    self.nn_parameters,
                    file_instance_nn_parameters,
                    protocol=pickle.HIGHEST_PROTOCOL,
                )
        if torch_model is None:
            raise ValueError(f"neural network {nn_name} is not handle by iota2")
        return torch_model

    def _get_nn(self, nn_name: str, external_nn_module: str | None) -> nn.Module:
        """instanciate the right nn model"""
        if nn_name in AVAIL_MODELS:
            return AVAIL_MODELS[nn_name](**self.nn_parameters)

        if external_nn_module:
            external_nn_module = verify_import(external_nn_module)
            user_nn_class = getattr(external_nn_module, nn_name)
            return user_nn_class(**self.nn_parameters)

        raise ValueError(f"{nn_name} not in {list(AVAIL_MODELS.keys())}")


class Iota2NeuralNetwork(nn.Module, ABC, metaclass=NeuralNetworkMeta):
    """Every neural networks provided by iota2 must inherit from this
    one to ensure some specific signatures.

    nb_features, nb_class and doy_sensors_dic
    are mandatory because of native iota2 neural networks needs.
    """

    def __init__(
        self,
        nb_class: int,  # pylint: disable=unused-argument
        sensors_information: dict | None,  # pylint: disable=unused-argument
        doy_sensors_dic: dict | None = None,  # pylint: disable=unused-argument
        default_std: int = 1,
        default_mean: int = 0,
    ):
        super().__init__()
        self.default_std = default_std
        self.default_mean = default_mean
        self._stats: dict[str, SensorStats] = {}
        if torch.cuda.is_available():
            self.device = torch.device("cuda")
        else:
            self.device = torch.device("cpu")

    @abstractmethod
    def forward(
        self, sentinel2: torch.Tensor, sentinel2_masks: torch.Tensor | None = None
    ) -> torch.Tensor:
        """Abstractmethod used to force the forward definition"""
        raise NotImplementedError

    def set_stats(self, stats: dict) -> None:
        """Set stats"""
        self._stats = stats

    @staticmethod
    def nans_imputation(
        data: torch.tensor,
        imputation_values: torch.tensor,
        default_stats_values: float | None = None,
    ) -> torch.tensor:
        """replace input values

        data:
           input data formatted as batch_size, dates, bands = data.shape
        """
        batch_size, dates, bands = data.shape
        stats_is_nan = torch.isnan(imputation_values)
        if default_stats_values is not None:
            stats_indices = stats_is_nan.nonzero(as_tuple=True)
            imputation_values[stats_indices] = default_stats_values

        stats_stack = torch.stack(
            batch_size * [imputation_values.reshape((dates, bands))]
        )
        data_is_nan = torch.isnan(data)
        data_indices = data_is_nan.nonzero(as_tuple=True)

        data[data_indices] = stats_stack[data_indices].float().to(data.device)
        return data

    @staticmethod
    def standardize(
        features: torch.tensor,
        mean: torch.tensor,
        std: torch.tensor,
        default_mean: float = 0.0,
        default_std: float = 1.0,
    ) -> torch.tensor:
        """
        Standardize features by removing the mean and scaling to
        unit variance as (features - mean) / std

        Parameters
        ----------
        features
            tensor of features (batch_size, nb_dates, nb_bands)
        mean
            mean values of each dates (nb_dates * nb_bands)
        std
            std values of each dates (nb_dates * nb_bands)
        default_mean
            replace nan values
        default_std
            replace nan and 0 values
        """
        std[torch.isnan(std).nonzero(as_tuple=True)] = default_std
        std[std == 0] = default_std
        mean[torch.isnan(mean).nonzero(as_tuple=True)] = default_mean

        _, nb_dates, nb_bands = features.shape
        mean = mean.reshape((nb_dates, nb_bands))
        std = std.reshape((nb_dates, nb_bands))

        x = (features - mean) / std
        x = x.float()
        return x

    @classmethod
    def save(
        cls,
        output_file: str,
        state_dict: dict,
        stats: dict[str, SensorStats],
        labels: list[str],
        device: str = "cpu",
    ) -> None:
        """class method to save the model's state"""
        stats_on_device = StatsCalculator.stats_to_device(stats, device)
        with open(output_file, "wb") as model_to_save:
            pickle.dump(
                (state_dict, stats_on_device, labels),
                model_to_save,
                protocol=pickle.HIGHEST_PROTOCOL,
            )


@register
class ANN(Iota2NeuralNetwork):
    """Same as ANN but reshape its inputs"""

    def __init__(
        self,
        nb_class: int,
        sensors_information: dict,
        layer: int = 1,
        doy_sensors_dic: dict | None = None,
        default_std: int = 1,
        default_mean: int = 0,
    ):
        super().__init__(
            nb_class, sensors_information, doy_sensors_dic, default_std, default_mean
        )
        nb_features = (
            sensors_information["sentinel2"]["nb_components"]
            * sensors_information["sentinel2"]["nb_dates"]
        )
        self.nb_class = nb_class
        self.layer = layer
        self.sensors_doy = doy_sensors_dic
        self.fc1 = nn.Linear(in_features=nb_features, out_features=16)
        self.fc2 = nn.Linear(in_features=16, out_features=12)
        self.output = nn.Linear(in_features=12, out_features=nb_class)

    def forward(
        self, sentinel2: torch.Tensor, sentinel2_masks: torch.Tensor | None = None
    ) -> torch.Tensor:
        mean = self._stats["sentinel2"].mean
        x = self.nans_imputation(sentinel2, mean, self.default_mean)
        std = torch.sqrt(self._stats["sentinel2"].var)
        x = self.standardize(x, mean, std, self.default_mean, self.default_std)

        # flat x due to flat input nn first layer
        x = x.reshape(-1, x.shape[1] * x.shape[-1])
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.output(x)
        return x


@register
class MLPClassifier(Iota2NeuralNetwork):  # pylint: disable=R0902
    """Simple Multi Layer Perceptron for Satellite Image Time Series
    classification"""

    def __init__(
        self,
        nb_class: int,
        sensors_information: dict,
        doy_sensors_dic: dict | None = None,
        default_std: int = 1,
        default_mean: int = 0,
    ):
        super().__init__(
            nb_class, sensors_information, doy_sensors_dic, default_std, default_mean
        )

        feat_per_date = sensors_information["sentinel2"]["nb_components"]
        self.nb_class = nb_class
        self.nb_bands = feat_per_date
        self.nb_dates = sensors_information["sentinel2"]["nb_dates"]
        nb_features = feat_per_date * self.nb_dates
        self.internal_size = nb_class * 3
        self.embed_size = max(nb_features // 2, self.internal_size)
        self.fc1 = nn.Linear(in_features=nb_features, out_features=self.embed_size)
        self.fc2 = nn.Linear(
            in_features=self.embed_size, out_features=self.internal_size
        )
        self.fc1_mask = nn.Linear(
            in_features=self.nb_dates, out_features=self.internal_size
        )
        self.fc2_mask = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.fc3 = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.fc4 = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.output = nn.Linear(in_features=self.internal_size, out_features=nb_class)

    def _forward(self, x: torch.Tensor, m: torch.Tensor | None) -> torch.Tensor:
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))

        if m is not None:
            m = F.relu(self.fc1_mask(m))
            m = F.relu(self.fc2_mask(m))
            x = F.relu(self.fc3(x + m))

        x = F.relu(self.fc4(x))
        x = self.output(x)
        return x

    def forward(
        self,
        sentinel2: torch.Tensor | None = None,
        sentinel2_masks: torch.Tensor | None = None,
    ) -> torch.Tensor:
        mean = self._stats["sentinel2"].mean
        sentinel2 = self.nans_imputation(sentinel2, mean, self.default_mean)
        std = torch.sqrt(self._stats["sentinel2"].var)
        sentinel2 = self.standardize(
            sentinel2, mean, std, self.default_mean, self.default_std
        )

        data_batch, data_dates, data_bands = sentinel2.shape
        # reshape due to nn design
        sentinel2 = sentinel2.reshape(data_batch, data_dates * data_bands)
        # if sentinel2_masks is not None:
        #     mask_batch, mask_dates, mask_bands = sentinel2_masks.shape
        #     sentinel2_masks = sentinel2_masks.reshape(mask_batch, mask_dates * mask_bands)
        return self._forward(sentinel2, sentinel2_masks)


class SelfAttention(nn.Module):
    """Self attention layer using dot product and addition"""

    def __init__(self, input_size: int, output_size: int):
        super().__init__()
        self.fc = nn.Linear(in_features=input_size, out_features=output_size)
        self.bn = nn.BatchNorm1d(output_size)
        self.fc_att = nn.Linear(in_features=input_size, out_features=output_size)
        self.bn_att = nn.BatchNorm1d(output_size)
        self.fc_att2 = nn.Linear(in_features=output_size, out_features=output_size)
        self.bn_att2 = nn.BatchNorm1d(output_size)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Forward pass of the neural network

        Parameters
        ----------
        x:
            input tensor
        """
        x_attend = torch.softmax(
            self.bn_att2(self.fc_att2(F.relu(self.bn_att(self.fc_att(x))))), dim=1
        )
        x = F.relu(self.bn(self.fc(x)))
        x = x * x_attend + x / x_attend.mean()
        return x


class MaskedAttention(nn.Module):
    """Attention layer using validity masks as context vector"""

    def __init__(self, nb_features: int, nb_bands: int):
        """nb_features = nb_dates * nb_bands"""
        super().__init__()
        self.nb_bands = nb_bands
        self.nb_dates = nb_features // self.nb_bands
        self.fc1_att = nn.Linear(in_features=self.nb_dates, out_features=self.nb_dates)
        self.bn1 = nn.BatchNorm1d(self.nb_dates)
        self.fc1_att2 = nn.Linear(in_features=self.nb_dates, out_features=self.nb_dates)
        self.bn2 = nn.BatchNorm1d(self.nb_dates)

    def forward(self, x: torch.Tensor, m: torch.Tensor) -> torch.Tensor:
        """
        Forward pass of the neural network using validity mask

        Parameters
        ----------
        x:
            input tensor
        m:
            mask tensor
        """
        x_attend = torch.softmax(
            self.bn2(self.fc1_att2(self.bn1(F.relu(self.fc1_att(m))))), dim=1
        )
        x = x * x_attend.repeat_interleave(self.nb_bands, dim=1) + x / x_attend.max()
        return x


@register
class SimpleSelfAttentionClassifier(Iota2NeuralNetwork):  # pylint: disable=R0902
    """SITS classifier using validity masks and self attention."""

    def __init__(
        self,
        nb_class: int,
        sensors_information: dict,
        nb_bands: int = 10,
        nb_attention_heads: int = 3,
        doy_sensors_dic: dict | None = None,
        default_std: int = 1,
        default_mean: int = 0,
    ):
        super().__init__(
            nb_class, sensors_information, doy_sensors_dic, default_std, default_mean
        )
        nb_features = (
            sensors_information["sentinel2"]["nb_dates"]
            * sensors_information["sentinel2"]["nb_components"]
        )
        self.nb_class = nb_class
        self.nb_bands = nb_bands
        self.internal_size = nb_class * 3
        self.embed_size = max(nb_features // 2, self.internal_size)
        self.mask_attend_list = [
            MaskedAttention(nb_features, nb_bands).to(self.device)
            for _ in range(nb_attention_heads)
        ]
        self.fc1 = nn.Linear(in_features=nb_features, out_features=self.embed_size)
        self.bn1 = nn.BatchNorm1d(self.embed_size)
        self.self_attend = SelfAttention(self.embed_size, self.internal_size)
        self.bnat = nn.BatchNorm1d(self.internal_size)
        self.fc2 = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.bn2 = nn.BatchNorm1d(self.internal_size)
        self.fc3 = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.bn3 = nn.BatchNorm1d(self.internal_size)
        self.fc4 = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.bn4 = nn.BatchNorm1d(self.internal_size)
        self.output = nn.Linear(in_features=self.internal_size, out_features=nb_class)

    def _forward(self, x: torch.Tensor, m: torch.Tensor) -> torch.Tensor:

        if m is not None:
            x = reduce(torch.add, [sa(x, m) for sa in self.mask_attend_list])
        x = F.relu(self.bn1(self.fc1(x)))
        x = self.bnat(self.self_attend(x))
        x = F.relu(self.bn2(self.fc2(x)))
        x = F.relu(self.bn3(self.fc3(x)))
        x = F.relu(self.bn4(self.fc4(x)))
        x = self.output(x)
        return x

    def forward(
        self, sentinel2: torch.Tensor, sentinel2_masks: torch.Tensor | None = None
    ) -> torch.Tensor:

        mean = self._stats["sentinel2"].mean
        sentinel2 = self.nans_imputation(sentinel2, mean, self.default_mean)
        std = torch.sqrt(self._stats["sentinel2"].var)
        sentinel2 = self.standardize(
            sentinel2, mean, std, self.default_mean, self.default_std
        )

        # reshape due to nn design
        data_batch, data_dates, data_bands = sentinel2.shape
        sentinel2 = sentinel2.reshape(data_batch, data_dates * data_bands)
        if sentinel2_masks is not None:
            mask_batch, mask_dates, mask_bands = sentinel2_masks.shape
            sentinel2_masks = sentinel2_masks.reshape(
                mask_batch, mask_dates * mask_bands
            )
        return self._forward(sentinel2, sentinel2_masks)


@register
class LTAEClassifier(Iota2NeuralNetwork):  # pylint: disable=R0902
    """SITS classifier using a transformer encoder."""

    def __init__(  # pylint: disable=R0913 # arguments are coming from original class def
        self,
        nb_class: int,
        sensors_information: dict,
        nb_attention_heads: int = 4,
        use_masks: bool = True,
        doy_sensors_dic: dict | None = None,
        embed_size: int | None = None,
        internal_size: int | None = None,
        dk: int | None = None,
        dropout: float | None = None,
        default_std: int = 1,
        default_mean: int = 0,
    ):
        super().__init__(nb_class, sensors_information, doy_sensors_dic)
        self.default_std = default_std
        self.default_mean = default_mean
        self.nb_class = nb_class
        self.nb_bands = sensors_information["sentinel2"]["nb_components"]
        self.nb_dates = sensors_information["sentinel2"]["nb_dates"]
        self.embed_size = embed_size or min(512, self.nb_bands * self.nb_dates // 2)

        # embed_size has to be a multiple of the nb of attention heads
        self.embed_size = (self.embed_size // nb_attention_heads) * nb_attention_heads
        self.internal_size = internal_size or max(256, self.embed_size // 2)
        self.dk = dk or 8
        self.dropout_proba = dropout
        self.nb_channels = self.nb_bands
        self.use_masks = use_masks
        assert doy_sensors_dic
        self.doys = doy_sensors_dic["sentinel2"]["doy"]
        if use_masks:
            self.nb_channels += 1
        self.ltae_module = LTAE(
            self.nb_channels,
            nb_attention_heads,
            self.dk,
            [self.embed_size, self.internal_size],
            d_model=self.embed_size,
            len_max_seq=self.nb_dates,
            positions=self.doys,
        )
        self.bnhidden1 = nn.BatchNorm1d(self.internal_size)
        self.dropout = nn.Dropout(p=(self.dropout_proba or 0.0))
        self.hidden1 = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.bnhidden2 = nn.BatchNorm1d(self.internal_size)
        self.hidden2 = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.bnout = nn.BatchNorm1d(self.internal_size)
        self.output = nn.Linear(in_features=self.internal_size, out_features=nb_class)

    def _forward(self, x: torch.Tensor, m: torch.Tensor) -> torch.Tensor:

        mean = self._stats["sentinel2"].mean
        x = self.nans_imputation(x, mean, self.default_mean)
        std = torch.sqrt(self._stats["sentinel2"].var)
        x = self.standardize(x, mean, std, self.default_mean, self.default_std)

        if self.use_masks:
            x = torch.cat((x, m), 2)
        x = self.bnhidden1(self.ltae_module(x))
        if self.dropout_proba is not None and self.training:
            x = self.dropout(x)
        x = F.relu(x)
        x = F.relu(self.bnhidden2(self.hidden1(x)))
        x = F.relu(self.bnout(self.hidden2(x)))
        x = self.output(x)
        return x

    def forward(
        self, sentinel2: torch.Tensor, sentinel2_masks: torch.Tensor | None = None
    ) -> torch.Tensor:
        return self._forward(sentinel2, sentinel2_masks)


@register
class MLPRegressor(Iota2NeuralNetwork):  # pylint: disable=R0902
    """Simple Multi Layer Perceptron for Satellite Image Time Series
    regression inspired by MLPClassifier"""

    def __init__(
        self,
        nb_class: int = 1,
        sensors_information: dict | None = None,
        doy_sensors_dic: dict | None = None,
        default_std: int = 1,
        default_mean: int = 0,
    ):
        super().__init__(
            nb_class, sensors_information, doy_sensors_dic, default_std, default_mean
        )
        self.nb_class = nb_class
        assert sensors_information
        self.nb_bands = sensors_information["sentinel2"]["nb_components"]
        self.nb_dates = sensors_information["sentinel2"]["nb_dates"]
        nb_features = self.nb_dates * self.nb_bands
        self.internal_size = nb_class * 3
        self.embed_size = max(nb_features // 2, self.internal_size)
        self.fc1 = nn.Linear(in_features=nb_features, out_features=self.embed_size)
        self.fc2 = nn.Linear(
            in_features=self.embed_size, out_features=self.internal_size
        )
        self.fc1_mask = nn.Linear(
            in_features=self.nb_dates, out_features=self.internal_size
        )
        self.fc2_mask = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.fc3 = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.fc4 = nn.Linear(
            in_features=self.internal_size, out_features=self.internal_size
        )
        self.output = nn.Linear(in_features=self.internal_size, out_features=nb_class)

    def _forward(self, x: torch.Tensor, m: torch.Tensor) -> torch.Tensor:

        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))

        if m is not None:
            m = F.relu(self.fc1_mask(m))
            m = F.relu(self.fc2_mask(m))
            x = F.relu(self.fc3(x + m))

        x = F.relu(self.fc4(x))
        x = self.output(x)
        return x

    def forward(
        self, sentinel2: torch.Tensor, sentinel2_masks: torch.Tensor | None = None
    ) -> torch.Tensor:

        mean = self._stats["sentinel2"].mean
        sentinel2 = self.nans_imputation(sentinel2, mean, self.default_mean)
        std = torch.sqrt(self._stats["sentinel2"].var)
        sentinel2 = self.standardize(
            sentinel2, mean, std, self.default_mean, self.default_std
        )

        data_batch, data_dates, data_bands = sentinel2.shape
        # reshape due to nn design
        sentinel2 = sentinel2.reshape(data_batch, data_dates * data_bands)
        if sentinel2_masks is not None:
            mask_batch, mask_dates, mask_bands = sentinel2_masks.shape
            sentinel2_masks = sentinel2_masks.reshape(
                mask_batch, mask_dates * mask_bands
            )
        return self._forward(sentinel2, sentinel2_masks)
