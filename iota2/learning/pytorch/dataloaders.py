#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""iota2 module dedicated to provided DataLoader torch object
"""
import logging
import random
from collections import Counter
from dataclasses import dataclass
from pathlib import Path

import numpy as np
import pandas as pd
import torch
import xarray as xr
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, StandardScaler
from torch.utils.data import DataLoader, TensorDataset

from iota2.learning.pytorch.torch_utils import (
    BatchProvider,
    SensorIndexPosition,
    SensorStats,
    StatsCalculator,
    VectorDataIndexPosition,
    sensors_from_labels_to_index,
)
from iota2.typings.i2_types import DBInfo, ModelHyperParameters
from iota2.vector_tools.vector_functions import get_netcdf_df_column_values

LOGGER = logging.getLogger("distributed.worker")


def get_encoder_conversion_dict(categories: list[float]) -> dict[int, int]:
    """traduction from original iota2 labels to LabelEncoder

    Parameters
    ----------
    categories
        input labels encoded by the LabelEncoder
    """
    convert_dic = {}
    for cat_index, categorie in enumerate(categories):
        convert_dic[cat_index] = int(categorie)
    return convert_dic


def get_proportion(
    database: str, proportion_column: str, target_column: str, target_values: list
) -> dict:
    """get proportion, as a percentage, of every class in the the database
       considering an other column and its values

    Parameters
    ----------
    database
        hdf input database
    proportion_column
        column containing class labels
    target_column
        discrimating column name
    target_values
        discrimating values in target_column

    Notes
    -----
    return proportion [0;1] as a dictionary.
    proportion[1] = 0.4 # 1 constitute 40% of the database.
    """
    netcdf_data = xr.open_dataset(database)
    netcdf_data = netcdf_data.sel(
        {
            "bands": [proportion_column, target_column],
            "fid": list(netcdf_data["fid"].values),
        }
    )
    netcdf_data = netcdf_data.compute()
    net_array = netcdf_data.to_array()
    df_prop = net_array.to_dataframe("sub_sel")
    df_prop = df_prop.unstack()
    df_prop.columns = [str(col_name) for _, col_name in list(df_prop.columns)]

    df_prop = df_prop.loc[df_prop[target_column].isin(target_values)]

    values = list(np.ravel(df_prop[proportion_column].values))
    return {value: count / len(values) for value, count in Counter(values).items()}


def originfid_to_fid(
    database_file: str, values: list[int], origin_column: str
) -> list[int]:
    """
    target_colmun become the index
    """
    # print(target_colmun, origin_column)
    netcdf_data = xr.open_dataset(database_file)
    netcdf_data = netcdf_data.sel(
        {"bands": [origin_column], "fid": list(netcdf_data["fid"].values)}
    )
    netcdf_data = netcdf_data.compute()
    net_array = netcdf_data.to_array()
    full_df = net_array.to_dataframe("sub_sel")
    full_df = full_df.unstack()
    full_df.columns = [str(col_name) for _, col_name in list(full_df.columns)]

    # full_df = sub_sel.to_array().to_pandas().T
    full_df = full_df.loc[full_df[origin_column].isin(values)]
    fids = [val for _, val in list(full_df.index.values)]
    random.shuffle(fids)
    return fids


def split_train_valid_by_fid(
    database_file: str,
    labels_column: str,
    origin_fid_column: str,
    test_size: float = 0.2,
    seed: int = 42,
) -> tuple[list[int], list[int]]:
    """split database in train and valid subset according to a column name

    Parameters
    ----------

    database_file
        hdf database file
    labels_column
        labels column
    origin_fid_column
        column containing original fids
    test_size
        percentage of samples use to valid the model [0;1]
    seed
        random seed to split the database in train/validation sample-set

    Note
    ----

    Return a tuple of two list, the first list is dedicated to learn model
    and the second to validated it
    """
    netcdf_data = xr.open_dataset(database_file)
    netcdf_data = netcdf_data.sel(
        {
            "bands": [labels_column, origin_fid_column],
            "fid": list(netcdf_data["fid"].values),
        }
    )
    netcdf_data = netcdf_data.compute()
    net_array = netcdf_data.to_array()
    df_label_origin_fid = net_array.to_dataframe("sub_sel")
    df_label_origin_fid = df_label_origin_fid.unstack()
    # sub_sel = netcdf_data[[labels_column, origin_fid_column]]
    # df_label_origin_fid = sub_sel.to_array().to_pandas().T
    df_label_origin_fid.columns = [
        str(col_name) for _, col_name in list(df_label_origin_fid.columns)
    ]

    label_to_fids = dict(
        df_label_origin_fid.groupby(labels_column)[origin_fid_column].apply(set)
    )
    fids_train = []
    fids_valid = []
    for _, fids in label_to_fids.items():
        list_fids = list(fids)
        try:
            _, _, train_fids, valid_fids = train_test_split(
                list_fids, list_fids, test_size=test_size, random_state=seed
            )
            # in order to manage unique label case value
            if not train_fids:
                train_fids = valid_fids
        except ValueError:
            train_fids = valid_fids = fids
        fids_train += train_fids
        fids_valid += valid_fids

    return fids_train, fids_valid


class BatchStreamDataSet(torch.utils.data.Dataset):
    """Custom dataset to read the input database per batch.

    Attributes
    ----------
    batch_fids:
        List of lists containing batch file IDs. One fids list per epochs
    class_distribution : dict
        Dictionary representing the class distribution.
    """

    batch_fids: list[list[int]]
    class_distribution: dict | None

    def __init__(
        self,
        database: DBInfo,
        fids_provider: BatchProvider,
        enc: LabelEncoder | StandardScaler,
        sensor_ind: dict,
        class_distribution: dict | None,
    ):
        """
        Initialize the BatchStreamDataSet.

        Parameters
        ----------
        database : DBInfo
            Information about the database.
        fids_provider : BatchProvider
            Provider for batch file IDs.
        enc : LabelEncoder | StandardScaler
            Encoder for labels.
        sensor_ind : dict
            Dictionary mapping sensor names to their indices in the vector dataset.
        class_distribution : dict | None
            Dictionary representing the class distribution.
        """
        self.database = database
        self.enc = enc
        self.sensor_ind = sensor_ind
        self.class_distribution = class_distribution
        self.fids_provider = fids_provider
        self.batch_fids: list[list[int]]
        self.load_batch_series(0)

    def load_batch_series(self, current_epoch: int) -> list[list[int]]:
        """load a list of batch for a given epoch
        Parameters
        ----------
        current_epoch
            epoch to load
        """
        self.batch_fids: list[list[int]] = self.fids_provider.get_epoch(current_epoch)
        return self.batch_fids

    def get_sensor_tensor_position(
        self,
    ) -> VectorDataIndexPosition:
        """
        Gets the positions of sensor data tensors within the dataset.

        This method returns the positions of the tensors for each sensor in the dataset,
        as well as the positions for the target and weight tensors.

        Returns
        -------
        VectorDataIndexPosition
            An instance of `VectorDataIndexPosition` containing:
            - `target`: The position index of the target tensor.
            - `weight`: The position index of the weight tensor.
            - `sensors`: A dictionary where each key is a sensor name and each value is a
                         `SensorIndexPosition`
              indicating the positions of data and mask tensors for that sensor.
        """
        sensors_datasets: dict[str, SensorIndexPosition] = {}
        ind = 0
        for sensor_name in self.sensor_ind.keys():
            sensor_ind = SensorIndexPosition(data=ind)
            ind += 1
            if self.database.mask_fields:
                sensor_ind.mask = ind
                ind += 1
            sensors_datasets[sensor_name] = sensor_ind
        vector_data_pos = VectorDataIndexPosition(
            target=ind, weight=ind + 1, sensors=sensors_datasets
        )
        return vector_data_pos

    def __len__(self) -> int:
        """
        Get the number of batches.

        Returns
        -------
        int
            Number of batches.
        """
        return len(self.fids_provider)

    def __getitem__(self, index: int) -> list:
        """
        Get a batch of data.

        Parameters
        ----------
        index : int
            Index of the batch to retrieve.

        Returns
        -------
        list
            List of tensors representing the batch data.
        """
        netcdf_data = xr.open_dataset(self.database.db_file)
        netcdf_data = netcdf_data.isel({"fid": self.batch_fids[index]})
        netcdf_data = netcdf_data.compute()
        full_df = netcdf_data.to_array().to_dataframe("sub_sel")
        full_df = full_df.unstack()
        full_df.columns = [str(col_name) for _, col_name in list(full_df.columns)]
        df_features = full_df[self.database.features_fields].copy()
        df_labels = full_df[[self.database.data_field]].copy()

        if self.database.mask_fields:
            df_masks = full_df[self.database.mask_fields].copy()

        datasets = []
        for _, sensor_dict in self.sensor_ind.items():
            df_features_sensor = df_features.iloc[:, sensor_dict["index"]]
            sensor_nb_bands = sensor_dict["nb_date_comp"]
            if sensor_nb_bands:
                sensor_nb_dates = len(sensor_dict["index"]) / sensor_nb_bands
                if sensor_nb_dates.is_integer():
                    sensor_nb_dates = int(sensor_nb_dates)
                else:
                    raise ValueError(
                        "number of dates is inconsistent with sensor component"
                    )
                tensor_feat = torch.tensor(
                    df_features_sensor.values.astype(np.float32)
                ).reshape(-1, sensor_nb_dates, sensor_nb_bands)
            else:
                tensor_feat = torch.tensor(
                    df_features_sensor.values.astype(np.float32)
                ).reshape(-1, len(sensor_dict["index"]))
            datasets.append(tensor_feat)
            if self.database.mask_fields:
                tensor_mask = torch.tensor(
                    df_masks.iloc[:, sensor_dict["mask_index"]].values.astype(
                        np.float32
                    )
                ).reshape(-1, len(sensor_dict["mask_index"]), 1)
                datasets.append(tensor_mask)

        tensor_labels = torch.tensor(
            self.enc.transform(np.ravel(df_labels.values))
        ).type(torch.LongTensor)
        datasets.append(tensor_labels)
        assert self.class_distribution
        # append weights
        datasets.append(
            torch.tensor(
                df_labels[self.database.data_field]
                .apply(lambda x: 1.0 / self.class_distribution[x])
                .to_numpy()
                .astype(np.float32)
            ).unsqueeze_(-1)
        )
        return datasets


class BatchStreamDataSetRegression(BatchStreamDataSet):
    """Custom dataset to read the input database per batch."""

    def __getitem__(self, index: int) -> list:
        """
        Get a batch of data.

        Parameters
        ----------
        index : int
            Index of the batch to retrieve.

        Returns
        -------
        list
            List of tensors representing the batch data.
        """
        netcdf_data = xr.open_dataset(self.database.db_file)
        netcdf_data = netcdf_data.isel({"fid": self.batch_fids[index]})
        netcdf_data = netcdf_data.compute()
        full_df = netcdf_data.to_array().to_dataframe("sub_sel")
        full_df = full_df.unstack()
        full_df.columns = [str(col_name) for _, col_name in list(full_df.columns)]
        df_features = full_df[self.database.features_fields].copy()
        df_labels = full_df[[self.database.data_field]].copy()

        if self.database.mask_fields:
            df_masks = full_df[self.database.mask_fields].copy()

        datasets = []
        for _, sensor_dict in self.sensor_ind.items():
            df_features_sensor = df_features.iloc[:, sensor_dict["index"]]
            sensor_nb_bands = sensor_dict["nb_date_comp"]
            if sensor_nb_bands:
                sensor_nb_dates = len(sensor_dict["index"]) / sensor_nb_bands
                if sensor_nb_dates.is_integer():
                    sensor_nb_dates = int(sensor_nb_dates)
                else:
                    raise ValueError(
                        "number of dates is inconsistent with sensor component"
                    )
                tensor_feat = torch.tensor(
                    df_features_sensor.values.astype(np.float32)
                ).reshape(-1, sensor_nb_dates, sensor_nb_bands)
            else:
                tensor_feat = torch.tensor(
                    df_features_sensor.values.astype(np.float32)
                ).reshape(-1, len(sensor_dict["index"]))
            datasets.append(tensor_feat)
            if self.database.mask_fields:
                tensor_mask = torch.tensor(
                    df_masks.iloc[:, sensor_dict["mask_index"]].values.astype(
                        np.float32
                    )
                ).reshape(-1, len(sensor_dict["mask_index"]), 1)
                datasets.append(tensor_mask)

        # in case of regression, 'enc' is a StandardScaler
        # 'labels' are target values and are Float
        tensor_labels = torch.tensor(
            self.enc.transform(np.ravel(df_labels.values)[:, np.newaxis])
        ).type(torch.FloatTensor)
        datasets.append(tensor_labels)
        return datasets


def database_to_dataloader_batch_stream(
    database: DBInfo,
    hyperparameters: ModelHyperParameters,
    num_workers: int,
    batch_prov: tuple[BatchProvider | None, BatchProvider | None],
    compute_stats: bool = True,
) -> tuple[
    DataLoader,
    DataLoader,
    dict,
    VectorDataIndexPosition,
    dict[str, SensorStats] | None,
]:
    """
    Converts data from a database into DataLoader for batch streaming.

    Parameters
    ----------
    database : DBInfo
           Information about the database.
    hyperparameters : ModelHyperParameters
           Hyperparameters for the model.
    num_workers : int
           Number of worker to use for data loading.
    batch_prov : tuple[BatchProvider | None, BatchProvider | None]
           Tuple containing BatchProviders for training and validation batches.
    compute_stats : bool | None
           Whether to compute statistics, by default True.

    Returns
    -------
    tuple[DataLoader, DataLoader, dict, VectorDataIndexPosition, dict[str, SensorStats] | None]
        A tuple containing:
        - train_loader : DataLoader
            DataLoader for training data.
        - valid_loader : DataLoader
            DataLoader for validation data.
        - encoder_conversion_dict : dict
            Dictionary for label encoding conversion.
        - sensors_datasets_ind : VectorDataIndexPosition
            Indices of sensor data in the datasets.
        - stats : dict[str, SensorStats] | None
            Statistics computed from the data if compute_stats is True, otherwise None.
    """
    train_batch_prov, valid_batch_prov = batch_prov
    assert database.fids_field is not None
    assert isinstance(database.db_file, (str, Path))
    fids_train_valid = split_train_valid_by_fid(
        str(database.db_file), database.data_field, database.fids_field
    )
    ogc_fids_train = originfid_to_fid(
        str(database.db_file), fids_train_valid[0], database.fids_field
    )
    ogc_fids_valid = originfid_to_fid(
        str(database.db_file), fids_train_valid[1], database.fids_field
    )
    if train_batch_prov is None:
        train_batch_prov = BatchProvider(
            ogc_fids_train, hyperparameters.batch_size, hyperparameters.epochs
        )
    if valid_batch_prov is None:
        valid_batch_prov = BatchProvider(
            ogc_fids_valid, hyperparameters.batch_size, hyperparameters.epochs
        )

    (
        train_loader,
        valid_loader,
        sensors_datasets_ind,
        models_labels,
    ) = build_data_loaders(
        database,
        fids_train_valid[0] + fids_train_valid[1],
        num_workers,
        train_batch_prov,
        valid_batch_prov,
    )
    stats = None
    if compute_stats:
        stats = StatsCalculator(
            train_loader,
            sensors_datasets_ind,
            True,
            subsample=hyperparameters.additional_statistics_percentage,
            min_stat=True,
            max_stat=True,
            var_stat=True,
        ).compute()
    return (
        train_loader,
        valid_loader,
        get_encoder_conversion_dict(models_labels),
        sensors_datasets_ind,
        stats,
    )


def build_data_loaders(
    database: DBInfo,
    fids: list[int],
    num_workers: int,
    train_batch_prov: BatchProvider,
    valid_batch_prov: BatchProvider,
) -> tuple[DataLoader, DataLoader, VectorDataIndexPosition, list[float]]:
    """
    Construct data loaders for training and validation datasets.

    Parameters
    ----------
    database : DBInfo
        dataclass containing relevant data fields and features.
    fids : list
        list of target field IDs.
    num_workers : int
        number of subprocesses to use for data loading.
    train_batch_prov : BatchProvider
        batch provider for training dataset.
    valid_batch_prov : BatchProvider
        batch provider for validation dataset.

    Returns
    -------
    train_loader : torch.utils.data.DataLoader
        DataLoader for the training dataset.
    valid_loader : torch.utils.data.DataLoader
        DataLoader for the validation dataset.
    sensors_datasets_ind : dict
        Dictionary containing sensor tensor positions.
    models_labels : list
        Sorted list of unique labels in the dataset.
    """
    masks_columns = database.mask_fields
    if masks_columns is None:
        masks_columns = []
    assert isinstance(database.db_file, (str, Path))
    models_labels = sorted(
        list(
            set(get_netcdf_df_column_values(str(database.db_file), database.data_field))
        )
    )
    assert database.fids_field is not None
    labels_train_distrib = get_proportion(
        database=str(database.db_file),
        proportion_column=database.data_field,
        target_column=database.fids_field,
        target_values=fids,
    )
    enc = LabelEncoder().fit(models_labels)
    assert database.features_fields
    sensor_ind_dic = sensors_from_labels_to_index(
        database.features_fields, masks_columns
    )
    train_dataset = BatchStreamDataSet(
        DBInfo(
            database.data_field,
            database.db_file,
            None,
            database.features_fields,
            masks_columns,
        ),
        train_batch_prov,
        enc,
        sensor_ind_dic,
        labels_train_distrib,
    )
    valid_dataset = BatchStreamDataSet(
        DBInfo(
            database.data_field,
            database.db_file,
            None,
            database.features_fields,
            masks_columns,
        ),
        valid_batch_prov,
        enc,
        sensor_ind_dic,
        labels_train_distrib,
    )
    sensors_datasets_ind = train_dataset.get_sensor_tensor_position()
    train_loader = DataLoader(
        train_dataset,
        batch_size=1,  # must be 1 here
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    valid_loader = DataLoader(
        valid_dataset,
        batch_size=1,  # must be 1 here
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    return train_loader, valid_loader, sensors_datasets_ind, models_labels


def database_to_dataloader_batch_stream_regression(
    database: DBInfo,
    hyperparameters: ModelHyperParameters,
    num_workers: int,
    batch_prov: tuple[BatchProvider | None, BatchProvider | None],
    compute_stats: bool = True,
) -> tuple[
    DataLoader,
    DataLoader,
    StandardScaler,
    VectorDataIndexPosition,
    dict[str, SensorStats] | None,
]:
    """
    Convert a database to DataLoader objects for a regression task using batch streaming.

    Parameters
    ----------
    database : DBInfo
        Database object containing relevant data fields and features.
    hyperparameters : ModelHyperParameters
        Hyperparameters for the model, including batch size and epochs.
    num_workers : int
        Number of worker to use for data loading.
    batch_prov : tuple[BatchProvider | None, BatchProvider | None]
        Tuple containing batch providers for training and validation datasets.
    compute_stats : bool, optional
        Whether to compute statistics on the training dataset, by default True.

    Returns
    -------
    train_loader : torch.utils.data.DataLoader
        DataLoader for the training dataset.
    valid_loader : torch.utils.data.DataLoader
        DataLoader for the validation dataset.
    enc : StandardScaler
        StandardScaler object fitted on the labels for normalization.
    sensors_datasets_ind : VectorDataIndexPosition
        Dataclass containing sensor tensor positions.
    stats : dict[str, SensorStats] | None
        Dictionary containing computed statistics for sensors,
        or None if compute_stats is False.
    """
    train_batch_prov, valid_batch_prov = batch_prov
    assert database.fids_field is not None
    assert isinstance(database.db_file, (str, Path))
    fids_train_valid = split_train_valid_by_fid(
        str(database.db_file), database.data_field, database.fids_field
    )
    ogc_fids_train = originfid_to_fid(
        str(database.db_file), fids_train_valid[0], database.fids_field
    )
    ogc_fids_valid = originfid_to_fid(
        str(database.db_file), fids_train_valid[1], database.fids_field
    )

    if train_batch_prov is None:
        train_batch_prov = BatchProvider(
            ogc_fids_train, hyperparameters.batch_size, hyperparameters.epochs
        )
    if valid_batch_prov is None:
        valid_batch_prov = BatchProvider(
            ogc_fids_valid, hyperparameters.batch_size, hyperparameters.epochs
        )
    (
        enc,
        sensors_datasets_ind,
        train_loader,
        valid_loader,
    ) = build_data_loaders_regression(
        database, num_workers, train_batch_prov, valid_batch_prov
    )
    stats = None
    if compute_stats:
        stats = StatsCalculator(
            train_loader,
            sensors_datasets_ind,
            True,
            subsample=hyperparameters.additional_statistics_percentage,
            min_stat=True,
            max_stat=True,
            var_stat=True,
        ).compute()
    return train_loader, valid_loader, enc, sensors_datasets_ind, stats


def build_data_loaders_regression(
    database: DBInfo,
    num_workers: int,
    train_batch_prov: BatchProvider,
    valid_batch_prov: BatchProvider,
) -> tuple[StandardScaler, VectorDataIndexPosition, DataLoader, DataLoader]:
    """
    Construct data loaders for training and validation datasets for a regression task.

    Parameters
    ----------
    database : DBInfo
        Database object containing relevant data fields and features.
    num_workers : int
        Number of worker to use for data loading.
    train_batch_prov : BatchProvider
        Batch provider for training dataset.
    valid_batch_prov : BatchProvider
        Batch provider for validation dataset.

    Returns
    -------
    enc : StandardScaler
        StandardScaler object fitted on the labels for normalization.
    sensors_datasets_ind : VectorDataIndexPosition
        Dataclass containing sensor tensor positions.
    train_loader : torch.utils.data.DataLoader
        DataLoader for the training dataset.
    valid_loader : torch.utils.data.DataLoader
        DataLoader for the validation dataset.
    """

    masks_columns = database.mask_fields
    if masks_columns is None:
        masks_columns = []

    assert isinstance(database.db_file, (str, Path))
    assert database.features_fields
    models_labels = np.array(
        get_netcdf_df_column_values(str(database.db_file), database.data_field)
    )
    models_labels = models_labels[:, np.newaxis]
    enc: StandardScaler = StandardScaler().fit(models_labels)
    sensor_ind_dic = sensors_from_labels_to_index(
        database.features_fields, masks_columns
    )
    train_dataset = BatchStreamDataSetRegression(
        DBInfo(
            database.data_field,
            database.db_file,
            None,
            database.features_fields,
            masks_columns,
        ),
        train_batch_prov,
        enc,
        sensor_ind_dic,
        None,
    )
    valid_dataset = BatchStreamDataSetRegression(
        DBInfo(
            database.data_field,
            database.db_file,
            None,
            database.features_fields,
            masks_columns,
        ),
        valid_batch_prov,
        enc,
        sensor_ind_dic,
        None,
    )
    sensors_datasets_ind = train_dataset.get_sensor_tensor_position()
    train_loader = DataLoader(
        train_dataset,
        batch_size=1,  # must be 1 here
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    valid_loader = DataLoader(
        valid_dataset,
        batch_size=1,  # must be 1 here
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    return enc, sensors_datasets_ind, train_loader, valid_loader


def database_to_dataloader_regression(
    database: DBInfo,
    batch_size: int = 1000,
    num_workers: int = 1,
    compute_stats: bool = True,
    additional_statistics_percentage: float | None = None,
) -> tuple[
    DataLoader,
    DataLoader,
    StandardScaler,
    VectorDataIndexPosition,
    dict[str, SensorStats] | None,
]:
    """
    Converts database information into data loaders for regression tasks.

    This function processes the input database to generate DataLoader instances for
    training and validation, calculates the necessary weights and encodings, and
    optionally computes statistics on the training data for regression purposes only.

    Parameters
    ----------
    database : DBInfo
        An object containing information about the database, including file paths and fields.
    batch_size : int, optional
        The number of samples per batch to load (default is 1000).
    num_workers : int, optional
        How many worker to use for data loading (default is 1).
    compute_stats : bool, optional
        Whether to compute additional statistics on the training data (default is True).
    additional_statistics_percentage : float | None
        The percentage of data to use for computing additional statistics,
        if any (default is None).

    Returns
    -------
        A tuple containing:
        - DataLoader: DataLoader for training data.
        - DataLoader: DataLoader for validation data.
        - StandardScaler: A fitted StandardScaler instance for label encoding.
        - VectorDataIndexPosition: A position object for vector data indices.
        - dict[str, SensorStats] | None: A dictionary (sensor name as key) containing sensor
                                            statistics, if computed.
    """
    dataset = dataset_to_df(database)

    enc: StandardScaler = encode_labels(
        dataset.labels_train, dataset.labels_valid, StandardScaler
    )

    (train_datasets, valid_datasets, ind, sensors_datasets_ind,) = create_datasets(
        dataset.features_train,
        dataset.features_valid,
        database,
        dataset.masks_train,
        dataset.masks_valid,
    )

    train_datasets.append(
        torch.tensor(enc.transform(dataset.labels_train)).type(torch.FloatTensor)
    )
    valid_datasets.append(
        torch.tensor(enc.transform(dataset.labels_valid)).type(torch.FloatTensor)
    )

    vector_data_pos = VectorDataIndexPosition(target=ind, sensors=sensors_datasets_ind)
    ind += 1
    train_loader = DataLoader(
        TensorDataset(*train_datasets),
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
    )
    valid_loader = DataLoader(
        TensorDataset(*valid_datasets),
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
    )
    stats = None
    if compute_stats:
        stats = StatsCalculator(
            train_loader,
            vector_data_pos,
            False,
            subsample=additional_statistics_percentage,
            min_stat=True,
            max_stat=True,
            var_stat=True,
        ).compute()
    return train_loader, valid_loader, enc, vector_data_pos, stats


@dataclass
class Dataset:
    """
    A dataclass representing a processed dataset, containing training and validation
    features, labels, masks, and optional weights for machine learning tasks.

    Attributes
    ----------
    features_train : pd.DataFrame
        DataFrame containing the features for the training set.
    features_valid : pd.DataFrame
        DataFrame containing the features for the validation set.
    labels_train : pd.DataFrame
        DataFrame containing the labels for the training set.
    labels_valid : pd.DataFrame
        DataFrame containing the labels for the validation set.
    masks_train : pd.DataFrame | None
        DataFrame containing the masks for the training set.
    masks_valid : pd.DataFrame | None
        DataFrame containing the masks for the validation set.
    weights_train : pd.DataFrame | None
        DataFrame containing the weights for the training set, by default None.
    weights_valid : pd.DataFrame | None
        DataFrame containing the weights for the validation set, by default None.
    """

    features_train: pd.DataFrame
    features_valid: pd.DataFrame
    labels_train: pd.DataFrame
    labels_valid: pd.DataFrame
    masks_train: pd.DataFrame | None = None
    masks_valid: pd.DataFrame | None = None
    weights_train: pd.DataFrame | None = None
    weights_valid: pd.DataFrame | None = None


def dataset_to_df(database: DBInfo, compute_weights: bool = False) -> Dataset:
    """
    Processes a database and splits it into training and validation datasets,
    including features, labels, and masks.

    This function takes in database information, splits the data into training and
    validation sets based on file identifiers (fids), and prepares the corresponding features,
    labels, and masks for both sets.

    Parameters
    ----------
    database : DBInfo
        An object containing information about the database, including file paths,
        fields for data, and fids.
    compute_weights : bool
        boolean value to compute weights dataframes

    Returns
    -------
    tuple
        A tuple containing pandas DataFrames:

        - df_features_train : pd.DataFrame
            DataFrame containing the features for the training set.
        - df_features_valid : pd.DataFrame
            DataFrame containing the features for the validation set.
        - df_labels_train : pd.DataFrame
            DataFrame containing the labels for the training set.
        - df_labels_valid : pd.DataFrame
            DataFrame containing the labels for the validation set.
        - df_masks_train : pd.DataFrame
            DataFrame containing the masks for the training set.
        - weights_train_df : pd.DataFrame
            DataFrame containing the weights for the training set.
        - weights_valid_df : pd.DataFrame
            DataFrame containing the weights for the validation set.
    """
    assert database.fids_field is not None
    assert isinstance(database.db_file, (str, Path))
    fids_train, fids_valid = split_train_valid_by_fid(
        str(database.db_file), database.data_field, database.fids_field
    )

    (
        df_features_train,
        df_labels_train,
        df_features_valid,
        df_labels_valid,
        full_df,
    ) = split_and_prepare_data(database, fids_train, fids_valid)
    df_masks_train, df_masks_valid = prepare_masks(
        full_df, database, fids_train, fids_valid
    )
    weights_train_df = None
    weights_valid_df = None
    if compute_weights:
        weights_train_df = calculate_weights(
            df_labels_train, database, fids_train, fids_valid
        )
        weights_valid_df = calculate_weights(
            df_labels_valid, database, fids_train, fids_valid
        )
    return Dataset(
        df_features_train,
        df_features_valid,
        df_labels_train,
        df_labels_valid,
        df_masks_train,
        df_masks_valid,
        weights_train_df,
        weights_valid_df,
    )


def encode_labels(
    df_labels_train: pd.DataFrame,
    df_labels_valid: pd.DataFrame,
    encoder_class: LabelEncoder | StandardScaler,
) -> LabelEncoder | StandardScaler:
    """
    Encodes labels from training and validation DataFrames using a LabelEncoder.

    This function combines the unique labels from both the training and validation
    DataFrames, sorts them, and fits a LabelEncoder on this combined set of labels.

    Parameters
    ----------
    df_labels_train : pd.DataFrame
        DataFrame containing the training labels.
    df_labels_valid : pd.DataFrame
        DataFrame containing the validation labels.
    encoder_class : LabelEncoder | StandardScaler
        Class of the encoder to use for encoding labels.

    Returns
    -------
    LabelEncoder | StandardScaler
        A fitted encoder instance that can be used to transform labels.

    Examples
    --------
    >>> df_train = pd.DataFrame({'label': [1, 2, 2, 3]})
    >>> df_valid = pd.DataFrame({'label': [2, 3, 4]})
    >>> encoder = encode_labels(df_train, df_valid)
    >>> encoder.transform([1, 2, 3, 4])
    array([0, 1, 2, 3])

    Notes
    -----
    The function assumes that the labels in `df_labels_train` and `df_labels_valid`
    are one-dimensional and can be flattened using `np.ravel`.
    """
    labels_train = set(np.ravel(df_labels_train))
    labels_valid = set(np.ravel(df_labels_valid))
    all_labels = sorted(list(labels_train.union(labels_valid)))
    enc = encoder_class().fit(all_labels)
    return enc


def prepare_masks(
    full_df: pd.DataFrame,
    database: DBInfo,
    fids_train: list[int],
    fids_valid: list[int],
) -> tuple[pd.DataFrame | None, pd.DataFrame | None]:
    """
    Prepares masks from the full DataFrame based on the provided FIDs.

    This function extracts masks from the full DataFrame based on the provided training
    and validation FIDs. If no masks are specified in the database information, it returns
    None for both training and validation masks.

    Parameters
    ----------
    full_df : pd.DataFrame
        Full DataFrame containing all data.
    database : DBInfo
        Information about the database, including the database file and field names.
    fids_train : list[int]
        List of FIDs for training data.
    fids_valid : list[int]
        List of FIDs for validation data.

    Returns
    -------
    tuple[pd.DataFrame | None, pd.DataFrame | None]
        A tuple containing:
        - pd.DataFrame | None: DataFrame containing training masks,
                                  or None if no masks are specified.
        - pd.DataFrame | None: DataFrame containing validation masks,
                                  or None if no masks are specified.
    """
    if masks_columns := database.mask_fields:
        df_masks_train = full_df.loc[full_df[database.fids_field].isin(fids_train)][
            masks_columns
        ]
        df_masks_valid = full_df.loc[full_df[database.fids_field].isin(fids_valid)][
            masks_columns
        ]
        return df_masks_train, df_masks_valid
    return None, None


def calculate_weights(
    df_labels: pd.DataFrame,
    database: DBInfo,
    fids_train: list[int],
    fids_valid: list[int],
) -> pd.DataFrame:
    """
    Calculates weights for each label in the DataFrame based on their distribution.

    This function calculates weights for each label in the DataFrame by computing the
    inverse of their distribution within the dataset.

    Parameters
    ----------
    df_labels : pd.DataFrame
        DataFrame containing the labels.
    database : DBInfo
        Information about the database, including the database file and field names.
    fids_train : list[int]
        List of FIDs for training data.
    fids_valid : list[int]
        List of FIDs for validation data.

    Returns
    -------
    pd.DataFrame
        DataFrame containing calculated weights for each label.
    """
    assert database.fids_field is not None
    assert isinstance(database.db_file, (str, Path))
    labels_distrib = get_proportion(
        database=str(database.db_file),
        proportion_column=database.data_field,
        target_column=database.fids_field,
        target_values=fids_train + fids_valid,
    )
    weights_df = df_labels.copy()
    weights_df[database.data_field] = weights_df[database.data_field].apply(
        lambda lb: 1 / labels_distrib[lb]
    )
    return weights_df


def split_and_prepare_data(
    database: DBInfo, fids_train: list[int], fids_valid: list[int]
) -> tuple[pd.DataFrame, ...]:
    """
    Splits and prepares data from the database based on provided training and validation FIDs.

    This function reads data from the specified database file, extracts features and labels
    based on the given FIDs for training and validation, and returns them in dataframes.

    Parameters
    ----------
    database : DBInfo
        Information about the database, including the database file and field names.
    fids_train : list[int]
        List of FIDs for training data.
    fids_valid : list[int]
        List of FIDs for validation data.

    Returns
    -------
    tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]
        A tuple containing:
        - pd.DataFrame: DataFrame containing training features.
        - pd.DataFrame: DataFrame containing training labels.
        - pd.DataFrame: DataFrame containing validation features.
        - pd.DataFrame: DataFrame containing validation labels.
        - pd.DataFrame: Full DataFrame with all features and labels.
    """
    netcdf_data = xr.open_dataset(database.db_file).compute()
    net_array = netcdf_data.to_array()
    full_df = net_array.to_dataframe("sub_sel").unstack()
    full_df.columns = [str(col_name) for _, col_name in list(full_df.columns)]

    df_features_train = full_df.loc[full_df[database.fids_field].isin(fids_train)][
        database.features_fields
    ]
    df_labels_train = full_df.loc[full_df[database.fids_field].isin(fids_train)][
        [database.data_field]
    ]

    df_features_valid = full_df.loc[full_df[database.fids_field].isin(fids_valid)][
        database.features_fields
    ]
    df_labels_valid = full_df.loc[full_df[database.fids_field].isin(fids_valid)][
        [database.data_field]
    ]

    return (
        df_features_train,
        df_labels_train,
        df_features_valid,
        df_labels_valid,
        full_df,
    )


def process_sensor_data(
    df_features_train: pd.DataFrame,
    df_features_valid: pd.DataFrame,
    sensor_dict: dict,
) -> tuple[torch.Tensor, torch.Tensor]:
    """
    Processes sensor data from feature DataFrames and converts them to tensors.

    This function extracts and reshapes sensor-specific feature data from the provided
    training and validation DataFrames, converting them into tensors suitable for model input.

    Parameters
    ----------
    df_features_train : pd.DataFrame
        DataFrame containing training features.
    df_features_valid : pd.DataFrame
        DataFrame containing validation features.
    sensor_dict : dict
        Dictionary containing index positions and metadata for the sensor components.

    Returns
    -------
    tuple[torch.Tensor, torch.Tensor]
        A tuple containing:
        - torch.Tensor: Tensor of training features for the sensor.
        - torch.Tensor: Tensor of validation features for the sensor.

    Raises
    ------
    ValueError
        If the number of dates is inconsistent with the sensor component configuration.
    """
    df_features_train_sensor = df_features_train.iloc[:, sensor_dict["index"]]
    df_features_valid_sensor = df_features_valid.iloc[:, sensor_dict["index"]]
    if sensor_nb_bands := sensor_dict["nb_date_comp"]:
        sensor_nb_dates = len(sensor_dict["index"]) / sensor_nb_bands
        if sensor_nb_dates.is_integer():
            sensor_nb_dates = int(sensor_nb_dates)
        else:
            raise ValueError("number of dates is inconsistent with sensor component")
        # tensors
        tensor_feat_train = torch.tensor(
            df_features_train_sensor.values.astype(np.float32)
        ).reshape(-1, sensor_nb_dates, sensor_nb_bands)
        tensor_feat_valid = torch.tensor(
            df_features_valid_sensor.values.astype(np.float32)
        ).reshape(-1, sensor_nb_dates, sensor_nb_bands)
    else:
        tensor_feat_train = torch.tensor(
            df_features_train_sensor.values.astype(np.float32)
        ).reshape(-1, len(sensor_dict["index"]))
        tensor_feat_valid = torch.tensor(
            df_features_valid_sensor.values.astype(np.float32)
        ).reshape(-1, len(sensor_dict["index"]))
    return tensor_feat_train, tensor_feat_valid


def create_datasets(
    df_features_train: pd.DataFrame,
    df_features_valid: pd.DataFrame,
    database: DBInfo,
    df_masks_train: pd.DataFrame | None = None,
    df_masks_valid: pd.DataFrame | None = None,
) -> tuple[list[torch.Tensor], list[torch.Tensor], int, dict[str, SensorIndexPosition]]:
    """
    Creates datasets for training and validation from given feature and mask DataFrames.

    This function processes the feature and mask DataFrames for both training and
    validation sets, converts them to tensors, and organizes them by sensor.

    Parameters
    ----------
    df_features_train : pd.DataFrame
        DataFrame containing training features.
    df_features_valid : pd.DataFrame
        DataFrame containing validation features.
    database : DBInfo
        An object containing information about the database,
        including file paths and fields.
    df_masks_train : pd.DataFrame | None
        DataFrame containing training masks, if any.
    df_masks_valid : pd.DataFrame | None
        DataFrame containing validation masks, if any.

    Returns
    -------
    tuple
        A tuple containing the following:
        - list[torch.Tensor]: List of tensors for training datasets.
        - list[torch.Tensor]: List of tensors for validation datasets.
        - int: Current index position for tracking the number of datasets.
        - dict[str, SensorIndexPosition]: Dictionary mapping sensor names to their
          index positions for data and masks.
    """
    columns_names = list(df_features_train.columns)
    sensor_ind_dic = sensors_from_labels_to_index(
        columns_names, database.mask_fields or []
    )

    train_datasets = []
    valid_datasets = []
    sensors_datasets_ind = {}
    ind = 0
    for sensor_name, sensor_dict in sensor_ind_dic.items():
        sensors_datasets_ind[sensor_name] = SensorIndexPosition(data=None, mask=None)
        tensor_feat_train, tensor_feat_valid = process_sensor_data(
            df_features_train, df_features_valid, sensor_dict
        )
        train_datasets.append(tensor_feat_train)
        valid_datasets.append(tensor_feat_valid)
        sensors_datasets_ind[sensor_name].data = ind
        ind += 1
        if df_masks_train is not None and df_masks_valid is not None:
            tensor_mask_train = torch.tensor(
                df_masks_train.iloc[:, sensor_dict["mask_index"]].values.astype(
                    np.float32
                )
            ).reshape(-1, len(sensor_dict["mask_index"]), 1)
            tensor_mask_valid = torch.tensor(
                df_masks_valid.iloc[:, sensor_dict["mask_index"]].values.astype(
                    np.float32
                )
            ).reshape(-1, len(sensor_dict["mask_index"]), 1)
            train_datasets.append(tensor_mask_train)
            valid_datasets.append(tensor_mask_valid)
            sensors_datasets_ind[sensor_name].mask = ind
            ind += 1
    return (
        train_datasets,
        valid_datasets,
        ind,
        sensors_datasets_ind,
    )


def database_to_dataloader(
    database: DBInfo,
    batch_size: int = 1000,
    num_workers: int = 1,
    compute_stats: bool = True,
    additional_statistics_percentage: float | None = None,
) -> tuple[
    DataLoader,
    DataLoader,
    dict,
    VectorDataIndexPosition,
    dict[str, SensorStats] | None,
]:
    """
    Creates DataLoader instances for training and validation datasets from a database.

    This function processes the input database to generate DataLoader instances for
    training and validation, calculates the necessary weights and encodings, and
    optionally computes statistics on the training data.

    Parameters
    ----------
    database : DBInfo
        An object containing information about the database, including file paths and fields.
    batch_size : int, optional
        The number of samples per batch to load (default is 1000).
    num_workers : int, optional
        How many worker to use for data loading (default is 1).
    compute_stats : bool, optional
        Whether to compute additional statistics on the training data (default is True).
    additional_statistics_percentage : float | None
        The percentage of data to use for computing additional statistics,
        if any (default is None).

    Returns
    -------
    tuple
        A tuple containing the following:
        - DataLoader: DataLoader instance for the training dataset.
        - DataLoader: DataLoader instance for the validation dataset.
        - dict: A dictionary for label encoding conversion.
                dict[new_label] = old_label
        - VectorDataIndexPosition: A position object for vector data indices.
        - dict[str, SensorStats] | None: A dictionary (sensor name as key) containing sensor
                                            statistics, if computed.

    Examples
    --------
    >>> db_info = DBInfo(...)
    >>> train_loader, valid_loader, enc_dict, vector_pos, stats = database_to_dataloader(db_info)
    """
    dataset = dataset_to_df(database, True)

    enc = encode_labels(dataset.labels_train, dataset.labels_valid, LabelEncoder)

    (train_datasets, valid_datasets, ind, sensors_datasets_ind,) = create_datasets(
        dataset.features_train,
        dataset.features_valid,
        database,
        dataset.masks_train,
        dataset.masks_valid,
    )
    train_datasets.append(
        torch.tensor(enc.transform(dataset.labels_train)).type(torch.LongTensor)
    )
    valid_datasets.append(
        torch.tensor(enc.transform(dataset.labels_valid)).type(torch.LongTensor)
    )

    vector_data_pos = VectorDataIndexPosition(target=ind, sensors=sensors_datasets_ind)
    ind += 1
    assert isinstance(dataset.weights_train, pd.DataFrame)
    assert isinstance(dataset.weights_valid, pd.DataFrame)
    tensor_weights_train = torch.tensor(
        dataset.weights_train.to_numpy().astype(np.float32)
    ).unsqueeze_(-1)
    tensor_weights_valid = torch.tensor(
        dataset.weights_valid.to_numpy().astype(np.float32)
    ).unsqueeze_(-1)
    train_datasets.append(tensor_weights_train)
    valid_datasets.append(tensor_weights_valid)
    vector_data_pos.weight = ind

    train_loader = DataLoader(
        TensorDataset(*train_datasets),
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
    )
    valid_loader = DataLoader(
        TensorDataset(*valid_datasets),
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
    )
    stats = None
    if compute_stats:
        stats = StatsCalculator(
            train_loader,
            vector_data_pos,
            False,
            subsample=additional_statistics_percentage,
            min_stat=True,
            max_stat=True,
            var_stat=True,
        ).compute()
    return (
        train_loader,
        valid_loader,
        get_encoder_conversion_dict(enc.classes_),
        vector_data_pos,
        stats,
    )
