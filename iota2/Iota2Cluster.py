#!/usr/bin/env python3
# pylint: disable=invalid-name
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Iota2Cluster.py allow user to deploy iota2 on HPC architecture

check 'Iota2Cluster.py -h' for options
"""

import argparse
import logging
import sys
from pathlib import Path
from subprocess import PIPE, Popen

from iota2.common.file_utils import get_iota2_project_dir
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2 import Iota2JobParameters, iota2_arguments
from iota2.sequence_builders.i2_sequence_builder_merger import WorkflowMerger
from iota2.steps import i2_job_helpers
from iota2.steps.i2_job_helpers import JobFileDescriptor

LOGGER = logging.getLogger("distributed.worker")


def get_ram(ram: str) -> int:
    """
    Return ram in gb

    Parameters
    ----------
    ram : str
        RAM in Mb or Gb

    Returns
    -------
    ram_f : int
        ram in gb (as an int)
    """

    ram = ram.lower().replace(" ", "")
    if "gb" in ram:
        ram_f = float(ram.split("gb")[0])
    elif "mb" in ram:
        ram_f = float(ram.split("mb")[0]) / 1024
    else:
        raise ValueError("Cannot convert ram")
    return int(ram_f)


def submit_job_cmd(
    i2_job_parameters: Iota2JobParameters,
    nb_parallel_tasks: int,
    execution_graph_files: list[str],
    tmp_directory: str,
    scheduler_name: str,
) -> str:
    """Build command to launch iota2 on HPC."""
    cfg = rcf.ReadConfigFile(i2_job_parameters.config_path)
    slurm_section = cfg.get_section_as_dataclass("slurm")
    log_dir = str(Path(cfg.get_param("chain", "output_path")) / "logs")
    job_dir = log_dir
    Path(job_dir).mkdir(parents=True, exist_ok=True)

    job_helper = i2_job_helpers.JobHelper(
        i2_job_parameters.config_ressources, "iota2_chain", slurm_section
    )

    iota2_main = str(
        Path(job_dir)
        / f"iota2.{job_helper.inventory_jobfile_helper[scheduler_name.lower()].extension}",
    )
    log_err = str(Path(log_dir) / "iota2_err.log")
    log_out = str(Path(log_dir) / "iota2_out.log")

    Path(iota2_main).unlink(missing_ok=True)

    cpu_from_cfg = job_helper.resources.nb_cpu
    nb_cpu = nb_parallel_tasks if nb_parallel_tasks > cpu_from_cfg else cpu_from_cfg
    job_helper.force_resource("nb_cpu", nb_cpu)

    scripts = str(Path(get_iota2_project_dir()) / "iota2")
    exe = (
        f"python {scripts}/Iota2.py "
        f"-config {i2_job_parameters.config_path} "
        f"-starting_step {i2_job_parameters.starting_step} "
        f"-ending_step {i2_job_parameters.ending_step} "
        f"-nb_parallel_tasks {nb_parallel_tasks} "
        f"-scheduler_type {i2_job_parameters.scheduler_type} "
        f"-tmp_directory_env {tmp_directory} "
    )

    if i2_job_parameters.restart_from is not None:
        exe = f"{exe} -restart_from {i2_job_parameters.restart_from}"
    if i2_job_parameters.restart:
        exe = f"{exe} -restart"
    if i2_job_parameters.config_ressources:
        exe = f"{exe} -config_ressources {i2_job_parameters.config_ressources}"
    if execution_graph_files:
        exe = f"{exe} -execution_graph_files {' '.join(execution_graph_files)}"
    job_helper.write(
        scheduler_name.lower(),
        JobFileDescriptor(Path(iota2_main), "IOTA2", log_err, log_out),
        force_executable=exe,
    )
    submit_cmd = (
        f"{job_helper.inventory_jobfile_helper[scheduler_name.lower()].submit_cmd} "
        f"{iota2_main}"
    )
    return submit_cmd


def run_cluster(parsed_args: argparse.Namespace) -> int:
    """Launch iota2 on HPC."""
    if parsed_args.restart and parsed_args.restart_from:
        raise ValueError("parameters 'restart' and 'restart_from' are not compatible")
    cfg = rcf.ReadConfigFile(parsed_args.config_path)
    chains = cfg.get_builders()
    if len(chains) > 1:
        chain_to_process = WorkflowMerger(
            chains,
            parsed_args.config_path,
            parsed_args.config_ressources,
            parsed_args.scheduler_type,
            parsed_args.restart,
            parsed_args.restart_from,
        )
    else:
        chain_to_process = chains[0](
            parsed_args.config_path,
            parsed_args.config_ressources,
            parsed_args.scheduler_type,
            parsed_args.restart,
            parsed_args.restart_from,
        )
    if parsed_args.start == parsed_args.end == 0:
        all_steps = chain_to_process.get_steps_number()
        parsed_args.start = all_steps[0]
        parsed_args.end = all_steps[-1]

    first_step_index = parsed_args.start - 1
    last_step_index = parsed_args.end - 1

    chain_to_process.get_final_i2_exec_graph(
        first_step_index, last_step_index, parsed_args.graph_figures
    )

    print(
        chain_to_process.print_step_summarize(
            parsed_args.start,
            parsed_args.end,
            parsed_args.config_ressources is not None,
        )
    )
    config_ressources = parsed_args.config_ressources
    if config_ressources:
        config_ressources = Path(config_ressources)
    if not parsed_args.only_summary:
        i2_job_parameters = Iota2JobParameters(
            config_path=parsed_args.config_path,
            config_ressources=config_ressources,
            scheduler_type=parsed_args.scheduler_type,
            starting_step=parsed_args.start,
            ending_step=parsed_args.end,
            restart=parsed_args.restart,
            restart_from=parsed_args.restart_from,
        )
        submit_cmd = submit_job_cmd(
            i2_job_parameters,
            parsed_args.nb_parallel_tasks,
            parsed_args.graph_figures,
            parsed_args.tmp_directory,
            parsed_args.scheduler_type.lower(),
        )
        # pylint: disable = R1732, consider-using-with
        Popen(submit_cmd, shell=True, stdout=PIPE, stderr=PIPE)
    return 0


def main() -> int:
    """Function to create conda entry point"""
    parser = iota2_arguments()

    # if we use a cluster, we must have a working directory
    parser.set_defaults(tmp_directory="TMPDIR")

    args = parser.parse_args()
    cluster_schedulers = ("Slurm", "PBS", "cluster")
    if args.scheduler_type in cluster_schedulers:
        run_cluster(args)
    else:
        print(f"'Iota2Cluster.py -scheduler_type' must be in {cluster_schedulers} ")

    return 0


if __name__ == "__main__":
    sys.exit(main())
