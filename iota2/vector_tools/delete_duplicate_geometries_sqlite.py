# !/usr/bin/env python3


# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

"""Create a new shapefile without duplicate geometries from input SQLITE file."""
import sqlite3 as lite
from pathlib import Path

from iota2.common.utils import run
from iota2.typings.i2_types import PathLike
from iota2.vector_tools import vector_functions as vf


def delete_duplicate_geometries_sqlite(
    shapefile: str,
    outformat: str = "ESRI Shapefile",
    do_corrections: bool = True,
    output_file: str | None = None,
    quiet_mode: bool = False,
) -> tuple[str, int]:
    """Check if a features is duplicate; if so, it will not be copied in output shapeFile.

    Parameters
    ----------
    shapefile:
        input shapeFile
    outformat:
        output format
    do_corrections:
        flag to remove duplicates
    output_file:
        output shapeFile, if set to None output_shape = input_shape
    quiet_mode : bool
        flag to print information
    Return
    ------
    tuple
        (output_shape, duplicates_features_number) where duplicates_features_number
        is the number of duplicated features
    """
    outsqlite: PathLike
    if outformat != "SQLite":
        tmpnamelyr = "tmp" + Path(shapefile).stem
        tmpname = f"{tmpnamelyr}.sqlite"
        outsqlite = str(Path(shapefile).parent / tmpname)
        run(f"ogr2ogr -f SQLite {outsqlite} {shapefile} -nln tmp")
        lyrin = "tmp"
    else:
        lyrin = vf.get_layer_name(shapefile, "SQLite")
        outsqlite = shapefile

    conn: lite.Connection | None
    cursor: lite.Cursor | None

    conn = lite.connect(outsqlite)
    cursor = conn.cursor()
    cursor.execute(f"select count(*) from {lyrin}")
    nbfeat0 = cursor.fetchall()

    cursor.execute("create temporary table to_del (ogc_fid int, geom blob);")
    cursor.execute(
        f"insert into to_del(ogc_fid, geom) select min(ogc_fid), "
        f"GEOMETRY from {lyrin} group by GEOMETRY having count(*) > 1;"
    )
    cursor.execute(
        f"delete from {lyrin} where exists(select * from to_del where "
        f"to_del.geom = {lyrin}.GEOMETRY and to_del.ogc_fid <> {lyrin}.ogc_fid);"
    )
    cursor.execute(f"select count(*) from {lyrin}")
    nbfeat1 = cursor.fetchall()
    nb_dupplicates = int(nbfeat0[0][0]) - int(nbfeat1[0][0])

    if do_corrections:
        conn.commit()

        if output_file is None and outformat != "SQLite":
            run(f"rm {shapefile}")

        shapefile = output_file if output_file is not None else shapefile

        if output_file is None and outformat != "SQLite":
            run(f"ogr2ogr -f 'ESRI Shapefile' {shapefile} {outsqlite}")

        if nb_dupplicates != 0:
            if quiet_mode is False:
                print(
                    "Analyse of duplicated features done. "
                    f"{nb_dupplicates} duplicates found and deleted"
                )
        else:
            if quiet_mode is False:
                print("Analyse of duplicated features done. No duplicates found")

        cursor = conn = None

    if outformat != "SQLite":
        Path(outsqlite).unlink()
    return shapefile, nb_dupplicates
