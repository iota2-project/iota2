#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Add a field called 'Perimeter' and populate it with geometries' perimeter"""
from osgeo import ogr


def add_field_perimeter(filein: str) -> int:
    """Add a field called 'Perimeter' and populate it with geometries' perimeter"""
    source = ogr.Open(filein, 1)
    layer = source.GetLayer()
    layer_defn = layer.GetLayerDefn()
    field_names = [
        layer_defn.GetFieldDefn(i).GetName() for i in range(layer_defn.GetFieldCount())
    ]
    if "Perimeter" in field_names or "perimeter" in field_names:
        if "Perimeter" in field_names:
            field_index = field_names.index("Perimeter")
            layer.DeleteField(field_index)
        if "perimeter" in field_names:
            field_index = field_names.index("perimeter")
            layer.DeleteField(field_index)
    new_field1 = ogr.FieldDefn("Perimeter", ogr.OFTReal)
    layer.CreateField(new_field1)

    for feat in layer:
        if feat.GetGeometryRef():
            geom = feat.GetGeometryRef()
            perim = geom.Boundary().Length()
            layer.SetFeature(feat)
            feat.SetField("Perimeter", perim)
            layer.SetFeature(feat)

    return 0
