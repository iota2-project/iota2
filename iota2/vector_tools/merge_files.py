#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module used to merge different types of files"""
import logging
import re
import sqlite3
import time
from collections.abc import Iterator
from pathlib import Path
from typing import Literal

import joblib
import numpy as np
import pandas as pd
import xarray as xr
from pandas.io import sql
from pandas.io.parsers import TextFileReader

import iota2.common.i2_constants as i2_const
from iota2.common.utils import run
from iota2.typings.i2_types import MergeDBParameters, PathLike
from iota2.vector_tools.vector_functions import get_driver, get_fields, get_layer_name

I2_CONST = i2_const.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")


def get_chunk_iterator(
    file: PathLike, chunk_size: int
) -> pd.DataFrame | Iterator[pd.DataFrame] | TextFileReader:
    """
    return an iterator processing over input file chunk by chunk

    Parameters
    ----------
    file: PathLike
        file to iterate
    chunk_size: int
        chunk size

    Returns
    -------
    pd.DataFrame | Iterator[pd.DataFrame] | TextFileReader
        Iterator processing over input file chunk by chunk
    """
    if Path(file).suffix == ".sqlite":
        tbl_name = get_layer_name(file, "SQLite")
        conn = sqlite3.connect(file)

        return sql.read_sql(f"SELECT * FROM {tbl_name};", conn, chunksize=chunk_size)

    return pd.read_csv(file, chunksize=chunk_size, iterator=True, low_memory=False)


def create_chunk_netcdf(
    file: str,
    merge_parameters: MergeDBParameters,
    expected_columns: list[str],
    nans_rules_flat: dict[str, float] | None,
    logger: logging.Logger = LOGGER,
) -> str | None:
    """
    Create a netcdf file from en extraction output file

    Parameters
    ----------
    file: str
        extraction output file to treat
    merge_parameters: MergeDBParameters
        Parameters of the merging process
    expected_columns: list[str]
        columns of file to keep in the output dataaray
    nans_rules_flat: dict[str, float] | None
        Flat dict containing rules specifying what values to replace nans with for given columns
    logger: logging.Logger
        Logger

    Returns
    -------
    str
        Path to the output netcdf file or "empty" if the chunk is empty
    """
    in_df = get_chunk_iterator(file, merge_parameters.chunk_size)
    for chk_num, i2_df in enumerate(in_df):
        if len(i2_df) > 0:
            dummy_df, _ = get_chunk_dataframe(expected_columns, i2_df, nans_rules_flat)
            da_xr = xr.DataArray(
                dummy_df.values,
                dims=(merge_parameters.fid_col, "bands"),
                coords={
                    "bands": list(dummy_df.columns),
                },
                name="dataarray",
            )

            output_parquet = file.replace(Path(file).suffix, f"_chk_{chk_num}.nc")
            da_xr.to_netcdf(output_parquet, mode="w")
            return output_parquet
        logger.warning("Empty database: " + file)
        return "empty"
    return None


def merge_db_to_netcdf(
    files: list[str],
    output_hdf: str,
    merge_parameters: MergeDBParameters,
    n_jobs: int | Literal[-1] = -1,
    logger: logging.Logger = LOGGER,
) -> None:
    """merge sqlite or csv db into a hdf db.

    This function can manage
        - databases with more than 2000 columns
        - input databases with different columns
        - nans values

    The resulting hdf database will contain a new col, 'fid_col', which represents the row number

    Parameters
    ----------
    files
        list of input files to merge
    output_hdf
        output hdf database file
        n_jobs
        Maximum number of concurrently running jobs.
        If -1 is given, joblib tries to use all CPUs.
        If 1 is given, no parallel computing code is used at all,
        and the behavior amounts to a simple python for loop.
    merge_parameters:
        Parameters of the merge process
    n_jobs:
        Number of jobs
    logger:
        logger

    Returns
    -------
    None
    """
    if Path(output_hdf).exists():
        logger.warning(f"output file '{output_hdf}' already exists, overwriting")
        Path(output_hdf).unlink()
    expected_columns = get_expected_columns(files, merge_parameters.cols_order)
    nans_rules_flat = None
    if merge_parameters.nans_rules:
        nans_rules_flat = flatten_nans_rules_dict(
            expected_columns, merge_parameters.nans_rules
        )

    netcdf_chunks_files = joblib.Parallel(n_jobs=n_jobs, verbose=10)(
        joblib.delayed(create_chunk_netcdf)(
            file,
            merge_parameters,
            expected_columns,
            nans_rules_flat,
            logger,
        )
        for file in files
    )

    start = time.time()
    netcdf_chunks_files = [path for path in netcdf_chunks_files if path != "empty"]
    netcdf_data = xr.open_mfdataset(
        netcdf_chunks_files,
        combine="nested",
        concat_dim=merge_parameters.fid_col,
        engine="netcdf4",
    )
    netcdf_data = netcdf_data.get("dataarray")
    netcdf_data = netcdf_data.assign_coords(fid=np.arange(len(netcdf_data)))
    end = time.time()
    logger.info(
        f"xr.open_mfdataset nb files : {len(netcdf_chunks_files)} timing : {end - start} sec"
    )
    start = time.time()
    netcdf_data.to_netcdf(output_hdf)
    end = time.time()
    logger.info(f"to_netcdf {output_hdf} timing : {end - start} sec")

    for netcdf_chunks_file in netcdf_chunks_files:
        Path(netcdf_chunks_file).unlink(missing_ok=True)


def get_expected_columns(files: list[str], cols_order: list[str] | None) -> list[str]:
    """
    Compute the list of expected columns

    Parameters
    ----------
    files: list[str]
        List of input files
    cols_order: list[str]
        Specified columns in order

    Returns
    -------
    expected_columns: list[str]
        List of expected columns
    """
    if cols_order:
        expected_columns = cols_order
    else:
        expected_columns = []
        driver = get_driver(files[0])
        for file in files:
            expected_columns += get_fields(file, driver=driver)
        expected_columns = list(set(expected_columns))
    return expected_columns


def flatten_nans_rules_dict(
    expected_columns: list[str], nans_rules: dict[str, float]
) -> dict[str, float]:
    """
    Create a dictionary containing all column names corresponding to regex given in input
    dictionary and assign corresponding values.

    Parameters
    ----------
    expected_columns:
        List of expected column names, used in the output dictionary
    nans_rules:
        Dictionary containing regex str as keys and values to replace nans with

    Returns
    -------
    nans_rules_flat: dict[str, float]
        Dictionary containing the rules

    Example
    -------
    >>> expected_columns = ["feat1", "feat2", "feat3", "mask1", "mask2", "mask3"]
    >>> nans_rules = {"(mask)": -1}
    >>> flatten_nans_rules_dict(expected_columns, nans_rules)
    >>> {"mask1": -1, "mask2": -1, "mask3": -1}

    """
    nans_rules_flat = {"default": nans_rules.get("default", np.nan)}
    for col in expected_columns:
        for re_pattern, nan_value in nans_rules.items():
            if re.search(re_pattern, col):
                nans_rules_flat[col] = nan_value
    return nans_rules_flat


def get_chunk_dataframe(
    expected_columns: list[str],
    i2_df: pd.DataFrame,
    nans_rules_flat: dict[str, float] | None,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    """
    Compute the pandas.Dataframe for a chunk

    Parameters
    ----------
    expected_columns:
        List of column names
    i2_df:
        Dataframe obtained from reading a chunk of input file
    nans_rules_flat:
        Flat dict containing rules specifying what values to replace nans with for given columns
    """
    dummy_df = pd.DataFrame(columns=expected_columns)
    i2_df = i2_df.select_dtypes(exclude=["object"])
    # i2_df[expected_columns] allow us to exclude regions columns
    cols_of_interest = [
        f"{col_name}"
        for col_name in list(i2_df.columns)
        if col_name in expected_columns
    ]
    dummy_df = pd.concat([dummy_df, i2_df[cols_of_interest]], ignore_index=True)
    dummy_df[cols_of_interest] = dummy_df[cols_of_interest].astype(float)
    # Remove nan values
    if nans_rules_flat:
        # build a list of columns with nans
        cols_with_nan = dummy_df.columns[dummy_df.isna().any()]
        for col in cols_with_nan:
            # if a column isn't in the dict => add it and give it the default value
            if col not in nans_rules_flat:
                nans_rules_flat[col] = nans_rules_flat["default"]
        dummy_df = dummy_df.fillna(nans_rules_flat)
    return dummy_df, i2_df


def merge_vectors(
    outname: str,
    opath: str,
    files: list[str],
    ext: str = "shp",
    out_tbl_name: str | None = None,
) -> str | None:
    """Merge a list of vector files in one.

    Parameters
    ----------
    outname: str
        output filename (without extension)
    opath: str
        path to the directory which will contains the resulting file
    files:list[str]
        list of files to merge
    ext: str
        output file extension (shp or sqlite)
    out_tbl_name: str | None = None
        force output layer/table name

    Returns
    -------
    file_fusion: str
        Path to the output file
    """
    done = []

    out_type = ""
    if ext == "sqlite":
        out_type = " -f SQLite "
    if files:
        good_files = []
        for filein in files:
            if Path(filein).exists():
                good_files.append(filein)
            else:
                print(f"{filein} not exist !")

        if good_files:
            file1 = files[0]
            nb_files = len(files)
            file_fusion = f"{opath}/{outname}.{ext}"
            if not Path(file_fusion).exists():
                table_name = outname
                if out_tbl_name:
                    table_name = out_tbl_name
                fusion = (
                    "ogr2ogr "
                    + file_fusion
                    + " "
                    + file1
                    + " "
                    + out_type
                    + " -nln "
                    + table_name
                )
                run(fusion)

                done.append(file1)
                for n_file in range(1, nb_files):
                    fusion = (
                        "ogr2ogr -update -append "
                        + file_fusion
                        + " "
                        + files[n_file]
                        + " -nln "
                        + table_name
                        + " "
                        + out_type
                    )
                    run(fusion)
                    done.append(files[n_file])

        return file_fusion
    return None
