#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Perform intersection, difference or union on two vector files (.shp or .sqlite)."""
import warnings
from pathlib import Path
from typing import Any

import fiona
import rtree
from fiona import Collection
from fiona.crs import from_epsg  # pylint: disable=no-name-in-module
from shapely.geometry import mapping, shape
from shapely.geometry.base import BaseGeometry

from iota2.common.utils import run
from iota2.typings.i2_types import IntersectionDatabaseParameters
from iota2.vector_tools import vector_functions as vf


def get_output_schema(
    layer_1: fiona.collection,
    layer_2: fiona.collection,
    keep_fields: list[str],
    db1_fid_name: str | None = None,
    db2_fid_name: str | None = None,
) -> dict[str, Any]:
    """Return expected database fields schema in a fiona format."""
    new_schema: dict[str, Any] = layer_2.schema.copy()
    if keep_fields:
        properties = {}
        for field in keep_fields:
            if field in layer_1.schema["properties"]:
                properties[field] = layer_1.schema["properties"][field]
            elif field in layer_2.schema["properties"]:
                properties[field] = layer_2.schema["properties"][field]
            else:
                warnings.warn(
                    f"Can't find field '{field}' "
                    f"in input databases : {layer_1.path}"
                    f" and {layer_2.path}"
                )
            new_schema = {"properties": properties, "geometry": "Polygon"}
    else:
        for field_name, field_value in layer_1.schema["properties"].items():
            new_schema["properties"][field_name] = field_value

    if db1_fid_name:
        new_schema["properties"][db1_fid_name] = "int:9"
    if db2_fid_name:
        new_schema["properties"][db2_fid_name] = "int:9"
    if layer_1.schema["geometry"] == "Point" or layer_2.schema["geometry"] == "Point":
        new_schema["geometry"] = "Point"
    return new_schema


def get_intersection_prop(
    feat1: dict[str, Any],
    feat2: dict[str, Any],
    keepfields: list[str],
    db_1_fid: str | None = None,
    db_2_fid: str | None = None,
) -> dict[str, Any]:
    """
    Calculate the properties for the intersection feature

    Parameters
    ----------
    db_1_fid:
        Field name to store the ID from the first layer
    db_2_fid:
        Field name to store the ID from the second layer
    feat1:
        Feature from the first layer
    feat2:
        Feature from the second layer
    keepfields:
        List of fields to keep from the features' properties
    """
    prop = {}
    if keepfields:
        for field in keepfields:
            try:
                if field in feat1["properties"]:
                    prop[field] = feat1["properties"][field]
                else:
                    prop[field] = feat2["properties"][field]
            except KeyError:
                pass
    else:
        prop = {**feat1["properties"], **feat2["properties"]}
    if db_1_fid:
        prop[db_1_fid] = feat1["id"]
    if db_2_fid:
        prop[db_2_fid] = feat2["id"]
    return prop


def write_intersection_db(
    output_file: str,
    crs: dict,
    layer_1: fiona.collection,
    layer_2: fiona.collection,
    intersection_parameters: IntersectionDatabaseParameters,
) -> None:
    """Write the intersection of layers in a output database."""
    new_schema = get_output_schema(
        layer_1,
        layer_2,
        intersection_parameters.fields_to_keep,
        intersection_parameters.db_1_fid,
        intersection_parameters.db_2_fid,
    )

    with fiona.open(
        output_file, "w", crs=crs, driver="ESRI Shapefile", schema=new_schema
    ) as layer_out:
        index = rtree.index.Index()
        for feat1 in layer_1:
            fid = int(feat1["id"])
            geom1 = shape(feat1["geometry"])
            index.insert(fid, geom1.bounds)
        for feat2 in layer_2:
            geom2 = shape(feat2["geometry"])
            for fid in list(index.intersection(geom2.bounds)):
                feat1 = layer_1[fid]
                geom1 = shape(feat1["geometry"])
                geom_intersection = geom1.intersects(geom2)
                if geom_intersection:
                    prop = get_intersection_prop(
                        feat1,
                        feat2,
                        intersection_parameters.fields_to_keep,
                        intersection_parameters.db_1_fid,
                        intersection_parameters.db_2_fid,
                    )
                    intersect_geom = shape(feat1["geometry"]).intersection(
                        shape(feat2["geometry"])
                    )
                    write_intersection_to_file(intersect_geom, layer_out, prop)


def write_intersection_to_file(
    intersect_geom: BaseGeometry, layer_out: Collection, prop: dict[str, Any]
):
    """
    Write the intersection to the output layer.

    Parameters
    ----------
    intersect_geom: BaseGeometry
        The geometry representing the intersection of two features
    layer_out: Collection
        The output file opened with Fiona
    prop: dict[str, Any]
        Properties to associate with the intersection

    Returns
    -------
    None
        The function writes data to the output layer and does not return anything.
    """
    if intersect_geom.geom_type in (
        "Polygon",
        "MultiPolygon",
        "GeometryCollection",
        "Point",
    ):
        if intersect_geom.geom_type != "GeometryCollection":
            layer_out.write(
                {
                    "geometry": mapping(intersect_geom),
                    "properties": prop,
                }
            )
        else:
            for simple_geom in intersect_geom.geoms:
                if simple_geom.geom_type in ("Polygon", "MultiPolygon"):
                    layer_out.write(
                        {
                            "geometry": mapping(simple_geom),
                            "properties": prop,
                        }
                    )


def intersection_sqlite(
    intersection_parameters: IntersectionDatabaseParameters,
    output: str,
    epsg: int,
) -> bool:
    """Perform vector intersection by using fiona.

    If there is intersection return True, else False.
    Parameters
    ----------
    intersection_parameters:
        Parameters for the intersection
    output:
        output vector path
    epsg:
        epsg code

    Notes
    -----
        Intersections are done thanks to fiona which does not understand
        'SQLite' format, then every in/out vector are translated into
        ShapeFile format if necessary
    """
    output_no_ext = Path(output).with_suffix("")
    output_ext = Path(output).suffix
    if output_ext == ".sqlite":
        output = str(Path(output).with_suffix(".shp"))

    db1_ext = Path(intersection_parameters.data_base1).suffix
    if db1_ext == ".sqlite":
        db1 = str(Path(intersection_parameters.data_base1).with_suffix(".shp"))
        if Path(db1).exists():
            raise OSError(f"{db1} already exists, please remove it")
        run(f"ogr2ogr {db1} {intersection_parameters.data_base1}")
    else:
        db1 = str(intersection_parameters.data_base1)

    db2_ext = Path(intersection_parameters.data_base2).suffix
    if db2_ext == ".sqlite":
        db2 = str(Path(intersection_parameters.data_base2).with_suffix(".shp"))
        if Path(db2).exists():
            raise OSError(f"{db2} already exists, please remove it")
        run(f"ogr2ogr {db2} {intersection_parameters.data_base2}")
    else:
        db2 = str(intersection_parameters.data_base2)
    with fiona.open(db1, "r") as layer1:
        with fiona.open(db2, "r") as layer2:
            crs = from_epsg(int(epsg))
            write_intersection_db(output, crs, layer1, layer2, intersection_parameters)

    output_field = vf.get_fields(output)[0]
    features = vf.get_field_element(output, elem_type=str, field=output_field)

    if output_ext == ".sqlite":
        expected_out = f"{output_no_ext}.sqlite"
        run(f"ogr2ogr -f 'SQLite' {expected_out} {output}")
        vf.remove_shape(output)
    if db1_ext == ".sqlite":
        vf.remove_shape(db1)
    if db2_ext == ".sqlite":
        vf.remove_shape(db2)
    return bool(features)
