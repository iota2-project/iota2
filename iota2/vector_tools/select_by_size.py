#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Filter features by a given characteristic and threshold and write them in a file."""
import sys
from pathlib import Path

from osgeo import ogr

from iota2.typings.i2_types import PathLike
from iota2.vector_tools import vector_functions as vf


def select_by_size(filein: PathLike, col: str, nbpix: int, fileout: PathLike) -> int:
    """
    Filter features by a given characteristic and threshold and write them in a file.
    For example, only select features with an area greater than a given threshold
    (the column 'area' must be present in the file)
    Parameters
    ----------
    filein:
        path to the input shapefile
    col:
        Name of the column to use as comparison
    nbpix:
        Threshold
    fileout:
        Path to the output file (.shp or .sqlite)
    """
    source = ogr.Open(filein, 0)

    baseext = Path(fileout).suffix
    if baseext == ".shp":
        outformat = "ESRI Shapefile"
    elif baseext == ".sqlite":
        outformat = "SQLite"
    else:
        print("Output format not managed")
        sys.exit()

    layer = source.GetLayer()
    request = col + " >= " + str(nbpix)
    layer.SetAttributeFilter(request)
    vf.create_new_layer(layer, fileout, outformat)
    return 0
