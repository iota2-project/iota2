#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Delete a given field in a given shapefile"""

from osgeo import ogr

from iota2.typings.i2_types import PathLike


def delete_field(shapefile: PathLike, field: str) -> None:
    """Delete a given field in a given shapefile"""
    shp = ogr.Open(shapefile, 1)
    lyr = shp.GetLayer()
    lyr_defn = lyr.GetLayerDefn()
    field_names = [
        lyr_defn.GetFieldDefn(i).GetName() for i in range(lyr_defn.GetFieldCount())
    ]
    if field in field_names:
        field_index = field_names.index(field)
        lyr.DeleteField(field_index)

    # shp.ExecuteSQL("REPACK " + lyr.GetName())
    lyr = shp = None
