# !/usr/bin/env python3

# =========================================================================
#   Program:   vector tools
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module gathering most of the database operations in iota2."""
import logging
import math
import random
import shutil
import sys
import time
from collections.abc import Callable
from pathlib import Path
from typing import Any, Literal

import dask.dataframe as dd
import geopandas as gpd
import numpy as np
import numpy.typing as npt
import osgeo
import xarray as xr
from osgeo import gdal, ogr
from shapely.geometry.base import BaseGeometry
from shapely.wkt import loads

import iota2.common.i2_constants as i2_const
from iota2.common.service_error import DataBaseError
from iota2.common.utils import run
from iota2.typings.i2_types import BroadType, LabelsConversionDict, OgrType, PathLike

I2_CONST = i2_const.Iota2Constants()

LOGGER = logging.getLogger("distributed.worker")


def get_geometry(vector: str, ogr_driver: str = "ESRI Shapefile") -> str:
    """Return the geometry of a vector file.

    Parameters
    ----------
    vector :
        path to a vector file
    ogr_driver :
        ogr driver name

    """
    driver = ogr.GetDriverByName(ogr_driver)
    in_ds = driver.Open(vector, 0)
    in_lyr = in_ds.GetLayer()
    feature = in_lyr.GetNextFeature()
    geometry: str = feature.GetGeometryRef().GetGeometryName()
    return geometry


def get_netcdf_df_column_values(database_file: str, column: str) -> list[float]:
    """Get column values of a netcdf file."""
    netcdf_data = xr.open_dataset(database_file)

    netcdf_data = netcdf_data.sel(
        {"bands": [column], "fid": list(netcdf_data["fid"].values)}
    )
    netcdf_data = netcdf_data.compute()
    net_array = netcdf_data.to_array()
    df_sub_sel = net_array.to_dataframe("sub_sel")
    df_sub_sel = df_sub_sel.unstack()
    df_sub_sel.columns = [str(col_name) for _, col_name in list(df_sub_sel.columns)]

    return list(df_sub_sel[column])


def get_all_fields_in_netcdf_df(
    netcdf_df_file: str, col_dim: str = "bands"
) -> list[str]:
    """get all fields of a pandas dataframe stored in a hdf file"""
    netcdf_data = xr.open_dataset(netcdf_df_file)
    fields = list(netcdf_data[col_dim].values)
    return fields


def get_all_fields_names(layer: ogr.Layer) -> list[str]:
    """get names of all fields in given layer"""
    all_fields = []
    in_layer_defn = layer.GetLayerDefn()
    for n_field in range(in_layer_defn.GetFieldCount()):
        field = in_layer_defn.GetFieldDefn(n_field).GetName()
        all_fields.append(field)
    return all_fields


def create_field(layer: ogr.Layer, field_label: str, field_type: OgrType) -> None:
    """creates a new field in layer after check that it does not already exist

    layer:
        ogr layer to modify (opened in write mode)
    field_label:
        label of the new field to add
    field_type:
        ogr type of the field to be added

    """

    if field_label in get_all_fields_names(layer):
        raise ValueError("can't add a field already present in layer")

    field = ogr.FieldDefn(field_label, field_type)
    layer.CreateField(field)


def fill_field(
    layer: ogr.Layer, field_label: str, value: Callable[[int, ogr.Feature], OgrType]
) -> None:
    """fills the field of given layer with return value of a function

    layer:
        ogr layer to modify (opened in write mode)
    field_label:
        label of the new field to add
    value [callable]:
        function to set the field value for each feature
        takes:
            number of the feature
            feature itself
        returns:
            value to put in field

    example:
        ```
        from iota2.vector_tools.vector_functions import openToWrite, add_field
        shp = "/XXX/data_colza.shp"
        s = openToWrite(shp)
        l = s.GetLayer()
        create_field(l, "newfield", ogr.OFTString)
        fill_field(l, "newfield", lambda i, feat : feat.GetField("cult_x") + f" {i}")
        ```
    """

    for n_feat, feat in enumerate(layer):
        feat.SetField(field_label, value(n_feat, feat))
        layer.SetFeature(feat)


def fill_field_with_array(
    layer: ogr.Layer, field_label: str, array: np.ndarray
) -> None:
    """Use an array to fill a field"""
    for n_feat, feat in enumerate(layer):
        feat.SetField(field_label, int(array[n_feat]))
        layer.SetFeature(feat)


def get_re_encoding_labels_dic(
    ref_data: str, ref_data_field: str
) -> LabelsConversionDict | None:
    """
    Parameters
    ----------
    ref_data
        input reference database
    ref_data_field
        column containing labels database
    Notes
    -----
    return the labels conversion dictionary dic[old_label] = new_label
    """
    ref_field_type = get_field_type(ref_data, ref_data_field)
    label_type: type
    if int == ref_field_type:
        label_type = int
    elif str == ref_field_type:
        label_type = str
    else:
        raise ValueError(
            "Input reference database labels must be string or integer format."
        )
    all_labels = get_field_element(
        ref_data, elem_type=label_type, field=ref_data_field, mode="unique"
    )
    assert all_labels is not None
    sorted_labels = sorted(all_labels)
    lut_labels: LabelsConversionDict = {}
    for new_label, old_label in zip(range(1, len(sorted_labels) + 1), sorted_labels):
        lut_labels[old_label] = new_label
    return lut_labels


def get_reverse_encoding_labels_dic(
    ref_data: str, ref_data_field: str
) -> LabelsConversionDict:
    """gets the mapping between i2 labels and user labels
    ref_data: input reference database
    ref_data_field: column containing labels database"""
    user_labels_to_i2_labels = get_re_encoding_labels_dic(ref_data, ref_data_field)
    assert user_labels_to_i2_labels is not None
    i2_labels_to_user_labels = {v: k for k, v in user_labels_to_i2_labels.items()}

    return i2_labels_to_user_labels


def get_vector_proj(vector_file: str, driver_name: str = "ESRI Shapefile") -> Any:
    """get vector projection"""
    gdf = gpd.GeoDataFrame().from_file(vector_file, driver=driver_name)
    return gdf.crs.to_epsg()


def get_geom_column_name(vector_file: str, driver_name: str = "ESRI Shapefile") -> str:
    """
    get field's name containing features' geometries

    Parameters
    ----------
    vector_file:
        input vector file
    driver_name:
        ogr driver's name

    """
    column_name: str
    if Path(vector_file).exists():
        driver = ogr.GetDriverByName(driver_name)
        data_src = driver.Open(vector_file, 0)
        layer = data_src.GetLayer()
        column_name = layer.GetGeometryColumn()
    else:
        raise Exception(f"File not found : {vector_file}")

    if not column_name:
        column_name = "GEOMETRY"

    return column_name


def open_to_read(
    shapefile: PathLike, driver_name: str = "ESRI Shapefile"
) -> ogr.DataSource:
    """
    Opens a shapefile to read it and returns the datasource in read mode

    Parameters
    ----------
    shapefile:
        the input vector file to open
    driver_name:
        Optional driver name for vector file
    """

    driver = ogr.GetDriverByName(driver_name)
    if driver.Open(shapefile, 0):
        data_source = driver.Open(shapefile, 0)
    else:
        raise ValueError(f"Not possible to open the file {shapefile}")

    return data_source


def open_to_write(
    shapefile: PathLike, driver_name: str = "ESRI Shapefile"
) -> ogr.DataSource:
    """Open a shapefile to read it and returns the datasource in write mode."""
    driver = ogr.GetDriverByName(driver_name)
    if driver.Open(shapefile, 1):
        data_src = driver.Open(shapefile, 1)
    else:
        print(f"Not possible to open the file {shapefile}")
        sys.exit(1)

    return data_src


def get_nb_feat(shapefile: PathLike, driver: str = "ESRI Shapefile") -> int:
    """Return the number of features of a shapefile."""
    data_src = open_to_read(shapefile, driver)
    layer = data_src.GetLayer()
    feature_count = layer.GetFeatureCount()
    return int(feature_count)


def get_fid_list(vect: PathLike) -> list[int]:
    """Return the list of fids in database."""
    shape = open_to_read(vect)
    lyr = shape.GetLayer()
    fidlist = []
    for feat in lyr:
        fidlist.append(feat.GetFID())

    return list(set(fidlist))


def get_geom_type(shapefile: PathLike, driver: str = "ESRI Shapefile") -> int:
    """Return the type of geometry of the file (WKBGeometryType)."""
    data_src = open_to_read(shapefile, driver)
    layer = data_src.GetLayer()
    layer_type: int = layer.GetGeomType()
    return layer_type


def get_fid_spatial_filter(
    shapefile: PathLike,
    shapefile_to_intersect: PathLike,
    field: str | None = None,
    driver: str = "ESRI Shapefile",
) -> list[int] | list[str]:
    """
    Return the list of either feature IDs or field value of all features inside the
    intersection of shapefile and shapefile_to_intersect.

    Parameters
    ----------
    shapefile:
        Path to source shapefile
    shapefile_to_intersect:
        Path to the shapefile used for the intersection
    field:
        Name of the field to retrieve values from. If None, features IDs are returned
    driver:
        Driver name
    """
    data_src = open_to_read(shapefile, driver)
    lyr = data_src.GetLayer()
    dsinter = open_to_read(shapefile_to_intersect, driver)
    lyrinter = dsinter.GetLayer()

    lyr.SetSpatialFilterRect(
        lyrinter.GetExtent()[0],
        lyrinter.GetExtent()[2],
        lyrinter.GetExtent()[1],
        lyrinter.GetExtent()[3],
    )
    fidlist = []
    for feat in lyr:
        if field is None:
            fidlist.append(feat.GetFID())
        else:
            fidlist.append(feat.GetField(field))

    return fidlist


def nb_geom_intersections(geometry: str, database: Path) -> int:
    """From a wkt geometry, return nb intersections with database entries.

    geometry projection must match the database one.
    if the database is empty, return 0.
    """
    data_src = ogr.Open(str(database))
    driver = data_src.GetDriver()
    data_source = driver.Open(str(database), 0)
    lyr = data_source.GetLayer()
    if lyr is None:
        count = 0
    else:
        lyr.SetSpatialFilter(geometry)
        count = lyr.GetFeatureCount()
    return count


def is_readable(db_file: str) -> bool:
    """Return True if the database is readable."""
    driver_name = get_driver(db_file)
    driver = ogr.GetDriverByName(driver_name)
    db_is_readable = bool(driver.Open(db_file, 0))
    driver = None
    return db_is_readable


def get_driver(vector: str) -> str:
    """Get correct driver for the given file"""
    if not Path(vector).exists():
        raise FileNotFoundError(f"Vector file '{vector}' doesn't exist.")
    inds = ogr.Open(vector)
    driver_name: str = inds.GetDriver().GetName()
    return driver_name


def get_layer_name(
    shapefile: PathLike, driver: str = "ESRI Shapefile", layernb: int = 0
) -> str:
    """
    Return the name of the nth layer of a vector file
    """
    data_src = open_to_read(shapefile, driver)
    layer = data_src.GetLayer(layernb)
    layer_name: str = layer.GetName()
    return layer_name


def get_fields(shp: PathLike, driver: str = "ESRI Shapefile") -> list[str]:
    """
    Returns the list of fields of a vector file
    """
    if not isinstance(shp, osgeo.ogr.Layer):
        data_src = open_to_read(shp, driver)
        lyr = data_src.GetLayer()
    else:
        lyr = shp

    in_layer_defn = lyr.GetLayerDefn()
    field_name_list = []
    for n_field in range(in_layer_defn.GetFieldCount()):
        field = in_layer_defn.GetFieldDefn(n_field).GetName()
        field_name_list.append(field)
    return field_name_list


def get_field_type(shp: PathLike, field: str, driver: str = "ESRI Shapefile") -> type:
    """Return the type of data in a given field of a shapefile"""
    data_src = open_to_read(shp, driver)
    layer = data_src.GetLayer()
    layer_definition = layer.GetLayerDefn()
    dico = {"String": str, "Real": float, "Integer": int, "Integer64": int}
    field_type = ""
    for n_field in range(layer_definition.GetFieldCount()):
        if layer_definition.GetFieldDefn(n_field).GetName() == field:
            field_type_code = layer_definition.GetFieldDefn(n_field).GetType()
            field_type = layer_definition.GetFieldDefn(n_field).GetFieldTypeName(
                field_type_code
            )
    if field_type not in dico:
        raise ValueError(f"{field_type} not in possible values : {list(dico.keys())}")
    return dico[field_type]


def get_first_layer(shp: PathLike, driver: str = "ESRI Shapefile") -> str:
    """Return the name of the first layer in the shapefile"""
    data_src = open_to_read(shp, driver)
    layer = data_src.GetLayerByIndex(0)
    layer_name: str = layer.GetName()

    return layer_name


def list_value_fields(
    shp: ogr.Layer | PathLike, field: str, driver: str = "ESRI Shapefile"
) -> list:
    """
    Returns the list of fields of a shapefile
    """
    if not isinstance(shp, osgeo.ogr.Layer):
        data_src = open_to_read(shp, driver)
        lyr = data_src.GetLayer()
    else:
        lyr = shp

    values = []
    for feat in lyr:
        if not feat.GetField(field) in values:
            values.append(feat.GetField(field))

    return sorted(values)


def create_new_layer(
    layer: ogr.Layer, out_shapefile: PathLike, outformat: str = "ESRI Shapefile"
) -> PathLike:
    """
    This function creates a new shapefile with a layer as input
    Parameters
    ----------
    layer :
        the input layer
    out_shapefile :
        the name of the output shapefile

    """
    in_layer_defn = layer.GetLayerDefn()
    field_name_target = []
    for n_field in range(in_layer_defn.GetFieldCount()):
        field = in_layer_defn.GetFieldDefn(n_field).GetName()
        field_name_target.append(field)

    out_driver = ogr.GetDriverByName(outformat)
    # if file already exists, delete it
    if Path(out_shapefile).exists():
        out_driver.DeleteDataSource(out_shapefile)

    out_datasource = out_driver.CreateDataSource(out_shapefile)
    out_lyr_name = Path(out_shapefile).stem
    # Get the spatial reference of the input layer
    srs_obj = layer.GetSpatialRef()
    # Creates the spatial reference of the output layer
    out_layer = out_datasource.CreateLayer(
        out_lyr_name, srs_obj, geom_type=layer.GetGeomType()
    )
    # Add input Layer Fields to the output Layer if it is the one we want

    for n_field in range(0, in_layer_defn.GetFieldCount()):
        field_defn = in_layer_defn.GetFieldDefn(n_field)
        field_name = field_defn.GetName()
        if field_name not in field_name_target:
            continue
        out_layer.CreateField(field_defn)
    # Get the output Layer's Feature Definition
    out_layer_defn = out_layer.GetLayerDefn()

    # Add features to the output Layer
    copy_fields_and_geometry(field_name_target, layer, out_layer, out_layer_defn)
    return out_shapefile


def copy_fields_and_geometry(
    field_name_target: list[str],
    layer: ogr.Layer,
    out_layer: ogr.Layer,
    out_layer_defn: ogr.FeatureDefn,
) -> None:
    """
    Copies features from the input layer to the output layer, including only specified fields and
    geometries in `field_name_target`.

    Parameters
    ----------
    field_name_target:
        List of field names to specify what fields to copy
    layer:
        The input layer containing features to be copied.
    out_layer:
        The output layer where the copied features will be stored.
    out_layer_defn:
        The feature definition for the output layer, specifying the fields and their types.
    """
    for in_feature in layer:
        # Create output Feature
        out_feature = ogr.Feature(out_layer_defn)

        # Add field values from input Layer
        for n_field in range(0, out_layer_defn.GetFieldCount()):
            field_defn = out_layer_defn.GetFieldDefn(n_field)
            field_name = field_defn.GetName()
            if field_name not in field_name_target:
                continue

            out_feature.SetField(
                out_layer_defn.GetFieldDefn(n_field).GetNameRef(),
                in_feature.GetField(n_field),
            )
        # Set geometry as centroid
        geom = in_feature.GetGeometryRef()
        out_feature.SetGeometry(geom.Clone())
        # Add new feature to output Layer
        out_layer.CreateFeature(out_feature)


def copy_feat_in_shp2(in_feat: ogr.Layer, shp: PathLike) -> None:
    """Copy a feature into a new file verifying thad does not exist in the new file."""
    data_src = open_to_write(shp)
    layer = data_src.GetLayer()
    out_layer_defn = layer.GetLayerDefn()
    in_geom = in_feat.GetGeometryRef()
    layer.SetSpatialFilter(in_geom)
    field_name_list = get_fields(shp)
    out_feat = ogr.Feature(out_layer_defn)
    for field in field_name_list:
        in_value = in_feat.GetField(field)
        out_feat.SetField(field, in_value)
    geom = in_feat.GetGeometryRef()
    out_feat.SetGeometry(geom)
    layer.CreateFeature(out_feat)
    layer.SetFeature(out_feat)
    data_src.ExecuteSQL("REPACK " + layer.GetName())


def check_valid_geom(
    input_shapefile: str,
    outformat: str = "ESRI shapefile",
    display: bool = True,
    do_corrections: bool = True,
) -> tuple[str, int, int]:
    """Check the validity of geometries in a file. If geometry is not valid then
    apply buffer 0 to correct. Works for files with polygons

    Parameters
    ----------
    input_shapefile:
        input shapeFile
    outformat:
        output shapeFile's format
    display:
        flag to print messages
    do_corrections:
        flag to correct files

    Return
    ------
    tuple
        (output_shape, count, corr) where count is the number of invalid features
        and corr the number of invalid features corrected
    """
    if display:
        print("Verifying geometries validity")

    driver = get_driver(input_shapefile)

    data_src = open_to_write(input_shapefile, driver)
    layer = data_src.GetLayer()
    count = 0
    corr = 0
    fidl = []

    for feat in layer:
        fid = feat.GetFID()
        if feat.GetGeometryRef() is None and not do_corrections:
            if display:
                print(fid)
            count += 1
            layer.DeleteFeature(fid)
            data_src.ExecuteSQL("REPACK " + layer.GetName())
            layer.ResetReading()
        else:
            geom = feat.GetGeometryRef()
            if not geom.IsValid() and do_corrections:
                fidl.append(fid)
                buffer_test = feat.SetGeometry(geom.Buffer(0))
                layer.SetFeature(feat)
                if buffer_test == 0:
                    if display:
                        print(f"Feature {feat.GetFID()} has been corrected")
                    corr += 1
                else:
                    if display:
                        print(f"Feature {feat.GetFID()} could not be corrected")
                count += 1
    if display:
        print(f"From {count} invalid features, {corr} were corrected")

    if outformat == "ESRI shapefile":
        data_src.ExecuteSQL(f"REPACK {layer.GetName()}")
    elif outformat == "SQLite":
        data_src = layer = None

    return input_shapefile, count, corr


def check_empty_geom(
    input_shape: str,
    informat: str = "ESRI shapefile",
    do_corrections: bool = True,
    output_file: PathLike | None = None,
) -> tuple[str, int]:
    """
    Check all geometries of a layer, and only copy non-empty ones (if `do_corrections` is True)

    Parameters
    ----------
    input_shape:
        input shapeFile
    informat:
        format of the input file
    do_corrections:
        flag to remove empty geometries
    output_file:
        output shapeFile, if set to None output_shape = input_shape
    Return
    ------
    tuple
        (output_shape, invalid_geom_number) where invalid_geom_number is
        the number of empty geometries
    """

    data_src = open_to_read(input_shape, informat)
    layer = data_src.GetLayer()
    all_fid = []
    count = 0
    for feat in layer:
        fid = feat.GetFID()
        geom = feat.GetGeometryRef()
        if geom is not None:
            if geom.IsEmpty():
                count += 1
            else:
                all_fid.append(fid)
    if do_corrections:
        if count == 0:
            print("No empty geometries")
        else:
            fid_str = " ".join(" OR ".join([f"FID={fid}" for fid in all_fid]))
            layer.SetAttributeFilter(fid_str)
            out_shapefile = (
                output_file
                if output_file is not None
                else str(input_shape).split(".", maxsplit=1)[0] + "-NoEmpty.shp"
            )
            create_new_layer(layer, out_shapefile)
            print(f"{len(all_fid)} empty geometries were deleted")

    if output_file is None:
        output_file = input_shape

    return str(output_file), count


def difference(geom1: ogr.Geometry, geom2: ogr.Geometry) -> BaseGeometry:
    """Returns the difference of 2 geometries."""
    obj1 = loads(geom1.ExportToWkt())
    obj2 = loads(geom2.ExportToWkt())
    return obj1.difference(obj2)


def union(geom1: ogr.Geometry, geom2: ogr.Geometry) -> BaseGeometry:
    """Returns the difference of 2 geometries."""
    obj1 = loads(geom1.ExportToWkt())
    obj2 = loads(geom2.ExportToWkt())
    print(obj1.union(obj2))
    return obj1.union(obj2)


def merge_db(outname: str, opath: PathLike, files: list[str]) -> None:
    """Merge database files in one file"""
    extension = Path(files[0]).suffix
    filefusion = Path(opath) / f"{outname}{extension}"
    Path(filefusion).unlink(missing_ok=True)
    if len(files) > 1:
        if extension == ".sqlite":
            merge_sqlite(filefusion, files)
        else:
            merge_csv(filefusion, files)
    else:
        shutil.copy(files[0], filefusion)


def merge_sqlite(filefusion: PathLike, files: list[str]) -> None:
    """Merge SQLITE files in one file (using ogr)"""
    if len(files) > 1:
        first = files[0]
        cmd = f"ogr2ogr -f SQLite {filefusion} {first}"
        run(cmd)
        for file in files[1::]:
            fusion_cmd = f"ogr2ogr -f SQLite -update -append {filefusion} {file}"
            print(fusion_cmd)
            run(fusion_cmd)
    else:
        shutil.copy(files[0], filefusion)


def merge_csv(filefusion: PathLike, files: list[str]) -> None:
    """Merge csv files in one file (using dask dataframe)"""
    csv_df = dd.read_csv(files)
    csv_df.to_csv(filefusion, index=False, single_file=True)


def categorize_continuous_field(
    field_arr: np.ndarray, bins: int | list
) -> npt.ArrayLike:
    """
    Turns a real variable into a discrete variable by returning the index
    of the interval in which the variable falls.

    Parameters
    ----------
    field_arr:
        numpy array of continuous values
    bins:
        if integer, the labels will be categorized into "bins" classes of equal-width,
        the parameter can also be a list of ascending values used as interval boundaries

    Notes
    -----
    Return the input numpy array discretized
    """
    if isinstance(bins, int):
        bins_as_array = np.histogram_bin_edges(field_arr, bins)
        bins_as_array[-1] += 0.001
    else:
        bins_as_array = np.array(bins)
    categorized_arr = np.digitize(field_arr, bins_as_array)

    return categorized_arr


def get_fake_class_vector(
    reference_data_path: str,
    data_field: str,
    data_aug_params: dict,
) -> npt.ArrayLike:
    """
    Generate a vector of classes for data augmentation in a regression problem.

    Parameters
    ----------
    reference_data_path:
        shapefile with continuous values to be used to create classes
    data_field:
        field with values to be used to create classes
    data_aug_params:
        dictionary with data augmentation parameters

    Notes
    -----
    Return a numpy array of data_field continuous values categorized
    """

    bins = data_aug_params["bins"]

    field_arr = np.array(
        get_field_element(
            shape=reference_data_path, elem_type=float, field=data_field, mode="all"
        )
    )

    fake_class_vector = categorize_continuous_field(field_arr, bins)
    return fake_class_vector


def get_field_element(
    shape: str,
    elem_type: type[BroadType],
    field: str = "CODE",
    mode: Literal["all", "unique"] = "all",
    logger: logging.Logger = LOGGER,
) -> list[BroadType] | None:
    """
    Parameters
    ----------
    shape:
        shape to compute
    elem_type:
        type of the element (as a string)
    field:
        data's field
    mode:
        "all" or "unique"
    logger:
        logger

    Notes
    -----
    Return a list containing all/unique element in shape's field

    Example :
        get_field_element("./MyShape.sqlite", int, "SQLite","CODE",mode = "all")
        >> [1,2,2,2,2,3,4]
        get_field_element("./MyShape.sqlite", int, "SQLite","CODE",mode = "unique")
        >> [1,2,3,4]
    """
    driver_name = get_driver(shape)
    if elem_type not in [int, float, str]:
        raise TypeError("Type of element should be int, float or str")
    driver = ogr.GetDriverByName(driver_name)
    layer = None
    if driver_name.lower() == "sqlite":
        max_try = 6
        for _ in range(max_try):
            try:
                data_src = driver.Open(shape, 0)
                layer = data_src.GetLayer()
                break
            # can be multiple exceptions
            except (AttributeError, FileNotFoundError, TypeError, RuntimeError):
                print(f"failed to open database {shape}, waiting...")
                time.sleep(random.randint(1, 10))
    else:
        data_src = driver.Open(shape, 0)
        layer = data_src.GetLayer()

    if layer is None:
        logger.info(
            f"{shape} is empty. Probably no intersection between"
            " learning points and the chunk."
            " Ensure you are using external_features"
        )
        return None
    try:
        retour_mode = [elem_type(currentFeat.GetField(field)) for currentFeat in layer]
    except (ValueError, TypeError):
        logger.info(
            f"{shape} is empty. Probably no intersection between"
            " learning points and the chunk."
            " Ensure you are using external_features"
        )
        return None
    if mode == "unique":
        retour_mode = list(set(retour_mode))

    return retour_mode


def get_vector_features(
    ground_truth: str, region_field: str, input_shape: str
) -> list[str]:
    """
    PARAMETERS
    ----------
    ground_truth :
        original ground truth file
    region_field :
        the region name
    input_shape :
        path to a vector (otbcli_SampleExtraction output)

    Notes
    -----
    all_feat [list of string] : list of all feature found in InputShape.
    This vector must contain field with pattern 'value_N' N:[0,int(someInt)]
    """
    input_fields = get_all_fields_in_shape(ground_truth) + [
        region_field,
        "originfid",
        I2_CONST.tile_origin_field,
    ]

    data_src = ogr.Open(input_shape)
    da_layer = data_src.GetLayer(0)
    layer_definition = da_layer.GetLayerDefn()

    all_feat = []
    for field_id in range(layer_definition.GetFieldCount()):
        field_name = layer_definition.GetFieldDefn(field_id).GetName()
        if field_name not in input_fields and field_name not in [
            input_field.lower() for input_field in input_fields
        ]:
            all_feat.append(layer_definition.GetFieldDefn(field_id).GetName())
    return all_feat


def get_all_fields_in_shape(
    vector: str, driver_name: str = "ESRI Shapefile"
) -> list[str]:
    """
    Return all fields in vector

    Parameters
    ----------
    vector :
        path to vector file
    driver_name :
        gdal driver name


    """
    driver = ogr.GetDriverByName(driver_name)
    data_source = driver.Open(vector, 0)
    if data_source is None:
        raise Exception(f"Could not open {vector}")
    layer = data_source.GetLayer()
    layer_definition = layer.GetLayerDefn()
    return [
        layer_definition.GetFieldDefn(i).GetName()
        for i in range(layer_definition.GetFieldCount())
    ]


def create_output_layer(
    in_layer: ogr.Layer,
    shpout: PathLike,
    field_name_list: list[str],
) -> ogr.DataSource:
    """
    Create a layer based on an input layer, using a specific list of field names and return the
    datasource of the layer

    Parameters
    ----------
    in_layer:
        Input layer, used to create the output layer
    shpout:
        Path to the output shapefile
    field_name_list:
        List of field names
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    srs_obj = in_layer.GetSpatialRef()
    if Path(shpout).exists():
        driver.DeleteDataSource(shpout)
    out_ds = driver.CreateDataSource(shpout)
    out_lyr = out_ds.CreateLayer("poly", srs_obj, geom_type=ogr.wkbPolygon)
    in_layer_defn = in_layer.GetLayerDefn()
    for n_field in range(0, len(field_name_list)):
        field_defn = in_layer_defn.GetFieldDefn(n_field)
        field_name = field_defn.GetName()
        if field_name in field_name_list:
            out_lyr.CreateField(field_defn)
    return out_ds


def keep_biggest_area(shpin: str, shpout: str) -> None:
    """
    Find the polygon from a shapefile with the biggest area and store it in an output shapefile

    Parameters
    ----------
    shpin:
        Input shapefile
    shpout:
        Output shapefile
    """

    def add_polygon(
        feat: ogr.Feature,
        simple_polygon: bytes,
        input_lyr: ogr.Layer,
        output_lyr: ogr.Layer,
    ) -> None:
        """
        usage : add polygon
        """
        feature_defn = input_lyr.GetLayerDefn()
        polygon = ogr.CreateGeometryFromWkb(simple_polygon)
        out_feat = ogr.Feature(feature_defn)
        for field in field_name_list:
            in_value = feat.GetField(field)
            out_feat.SetField(field, in_value)
        out_feat.SetGeometry(polygon)
        output_lyr.CreateFeature(out_feat)
        output_lyr.SetFeature(out_feat)

    gdal.UseExceptions()
    driver = ogr.GetDriverByName("ESRI Shapefile")
    field_name_list = get_all_fields_in_shape(shpin)
    in_ds = driver.Open(shpin, 0)
    in_lyr = in_ds.GetLayer()

    out_ds = create_output_layer(in_lyr, shpout, field_name_list)
    out_lyr = out_ds.GetLayer()
    area = []
    all_geom = []
    for in_feat in in_lyr:
        geom = in_feat.GetGeometryRef()
        area.append(geom.GetArea())
        all_geom.append(geom.ExportToWkb())

    index_max = int(np.argmax(np.array(area)))
    add_polygon(in_lyr[index_max], all_geom[index_max], in_lyr, out_lyr)


def erode_or_dilate_shapefile(
    infile: PathLike, outfile: PathLike, buffdist: float
) -> bool:
    """
    dilate or erode all features in the shapeFile In
    Return True if succeed
    Parameters
    ----------
    infile :
        the shape file
        ex : /xxx/x/x/x/x/yyy.shp
    outfile :
        the resulting shapefile
        ex : /x/x/x/x/x.shp
    buffdist :
        the distance of dilatation or erosion
        ex : -10 for erosion
             +10 for dilatation

    OUT :
    - the shapeFile outfile
    """

    retour = True
    try:
        data_source = ogr.Open(infile)
        drv = data_source.GetDriver()
        if Path(outfile).exists():
            drv.DeleteDataSource(outfile)
        drv.CopyDataSource(data_source, outfile)
        data_source.Destroy()

        data_source = ogr.Open(outfile, 1)
        lyr = data_source.GetLayer(0)
        for n_feature in range(0, lyr.GetFeatureCount()):
            feat = lyr.GetFeature(n_feature)
            lyr.DeleteFeature(n_feature)
            geom = feat.GetGeometryRef()
            feat.SetGeometry(geom.Buffer(float(buffdist)))
            lyr.CreateFeature(feat)
        data_source.Destroy()
    except (AttributeError, FileNotFoundError, TypeError, RuntimeError):
        retour = False
    return retour


def erode_shapefile(infile: PathLike, outfile: PathLike, buffdist: float) -> bool:
    """Erode all features in shapefile"""
    return erode_or_dilate_shapefile(infile, outfile, -math.fabs(buffdist))


def remove_shape(shape_path: PathLike) -> None:
    """
    Delete a shapefile (.shp) and all associated files (.prj, .shp, .dbf, .shx)

    Parameters
    ----------
    shape_path:
        Path to the vector file to delete

    Returns
    -------
    None
    """
    if Path(shape_path).suffix != ".shp":
        raise DataBaseError(f"Invalid shapefile name: {shape_path}")
    driver = ogr.GetDriverByName("ESRI shapefile")
    driver.DeleteDataSource(str(shape_path))


def copy_shapefile(shape: str, outpath: str) -> None:
    """
    Copy shapefile. If `outpath` is a directory, add the name of the source file to the
    destination path

    Parameters
    ----------
    shape:
        Input shapefile to copy
    outpath:
        Destination path where to copy the source file (can be a file or a directory)
    """
    if Path(outpath).is_dir():
        outpath = str(Path(outpath) / Path(shape).name)
    data_src = ogr.Open(shape)
    drv = data_src.GetDriver()
    drv.CopyDataSource(data_src, outpath)


def rename_shapefile(
    inpath: PathLike,
    filename: str,
    old_suffix: str,
    new_suffix: str,
    outpath: PathLike | None = None,
) -> Path:
    """Rename shapefile"""
    if not outpath:
        outpath = inpath

    for extension in [".shx", ".dbf", ".prj", ".shp"]:
        inpath_cp = Path(inpath) / (filename + old_suffix + extension)
        outpath_cp = Path(outpath) / (filename + new_suffix + extension)
        shutil.copy(inpath_cp, outpath_cp)
    return outpath_cp


def clip_vector_data(
    vector_file: PathLike,
    cut_file: PathLike,
    opath: PathLike,
    name_out: str | None = None,
) -> str:
    """
    Cuts a shapefile with another shapefile
    Parameters
    ----------
    vector_file:
        the shapefile to be cut
    cut_file:
        the other shapefile
    opath:
        path to output file directory
    name_out:
        name of the output file
    """
    if not name_out:
        name_vf = Path(vector_file).stem
        name_cf = Path(cut_file).stem
        outname = Path(opath) / f"{name_vf}_{name_cf}.shp"
    else:
        outname = Path(opath) / f"{name_out}.shp"

    Path(outname).unlink(missing_ok=True)

    clip = f"ogr2ogr -clipsrc {cut_file} {outname} {vector_file} -progress"
    run(clip)
    return str(outname)
