#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Module to deal with multipolygons.
"""

from pathlib import Path

from osgeo import gdal, ogr

from iota2.vector_tools import vector_functions as vf

gdal.UseExceptions()


def add_polygon(
    feat: ogr.Feature,
    simple_polygon: bytes,
    in_lyr: ogr.Layer,
    out_lyr: ogr.Layer,
    field_name_list: list[str],
) -> None:
    """Add a simple polygon"""
    feature_defn = in_lyr.GetLayerDefn()
    polygon = ogr.CreateGeometryFromWkb(simple_polygon)
    out_feat = ogr.Feature(feature_defn)
    for field in field_name_list:
        in_value = feat.GetField(field)
        out_feat.SetField(field, in_value)

    out_feat.SetGeometry(polygon)
    out_lyr.CreateFeature(out_feat)
    out_lyr.SetFeature(out_feat)


def manage_multi_poly2_poly(
    in_lyr: ogr.Layer,
    out_lyr: ogr.Layer,
    field_name_list: list[str],
    do_correction: bool = True,
) -> int:
    """
    Check all features in a layer. If a feature is a multipolygon and do_corrections is True, add
    all necessary polygons to reproduce the multipolygon. Return the number of multipolygons found.

    Parameters
    ----------
    in_lyr:
        Input layer
    out_lyr:
        Output layer
    field_name_list:
        List of field names
    do_correction: bool
        Only add polygons to output layer if set to True
    """
    multi_cpt = 0
    for in_feat in in_lyr:
        geom = in_feat.GetGeometryRef()
        if geom is not None:
            if geom.GetGeometryName() == "MULTIPOLYGON":
                multi_cpt += 1
                if do_correction:
                    for geom_part in geom:
                        add_polygon(
                            in_feat,
                            geom_part.ExportToWkb(),
                            in_lyr,
                            out_lyr,
                            field_name_list,
                        )
            else:
                if do_correction:
                    add_polygon(
                        in_feat, geom.ExportToWkb(), in_lyr, out_lyr, field_name_list
                    )
    return multi_cpt


def multipoly2poly(
    inshape: str,
    outshape: str,
    do_correction: bool = True,
    outformat: str = "ESRI shapefile",
) -> int:
    """
    Check if a geometry is a MULTIPOLYGON, if it is it will not be split in POLYGON.
    Return the number of  MULTIPOLYGON found

    Parameters
    ----------
    inshape:
        input shapeFile
    outshape:
        output shapeFile
    do_correction:
        flag to remove MULTIPOLYGONs
    outformat:
        Format of the output file
    """

    driver_name = vf.get_driver(inshape)

    # Get field list
    field_name_list = vf.get_fields(inshape, driver_name)

    # Open input and output shapefile
    driver = ogr.GetDriverByName(outformat)
    in_ds = driver.Open(inshape, 0)
    in_lyr = in_ds.GetLayer()
    in_layer_defn = in_lyr.GetLayerDefn()
    srs_obj = in_lyr.GetSpatialRef()
    if Path(outshape).exists():
        driver.DeleteDataSource(outshape)

    # remove multi-polygons
    out_lyr = None
    if do_correction:
        out_ds = driver.CreateDataSource(outshape)
        out_lyr = create_output_layer(field_name_list, in_layer_defn, out_ds, srs_obj)

    multipoly = manage_multi_poly2_poly(in_lyr, out_lyr, field_name_list, do_correction)
    return multipoly


def create_output_layer(
    field_name_list: list[str],
    in_layer_defn: ogr.FeatureDefn,
    out_ds: ogr.DataSource,
    srs_obj: ogr.osr.SpatialReference,
) -> ogr.Layer:
    """Create output layer with specified fields.

    Parameters
    ----------
    field_name_list: List[str]
        List of field names
    in_layer_defn:
        Input layer definition
    out_ds:
        Datasource of the output shapefile
    srs_obj:
        Spatial reference object
    """
    out_lyr = out_ds.CreateLayer("poly", srs_obj, geom_type=ogr.wkbPolygon)
    for n_field, _ in enumerate(field_name_list):
        field_defn = in_layer_defn.GetFieldDefn(n_field)
        field_name = field_defn.GetName()
        if field_name not in field_name_list:
            continue
        out_lyr.CreateField(field_defn)
    return out_lyr
