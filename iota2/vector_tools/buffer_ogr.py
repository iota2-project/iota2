# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Apply a buffer of a defined distance on features of an input shapefile
and create a new shapefile with same attributes.
"""
from pathlib import Path

from osgeo import ogr

from iota2.typings.i2_types import PathLike


def buffer_poly(
    inputfn: PathLike,
    output_buffer_fn: str = "",
    buffer_dist: str | float = "0",
    outformat: str = "ESRI Shapefile",
) -> ogr.Layer:
    """
    Apply a buffer of a defined distance on features of an input shapefile
    and create a new shapefile with same attributes.

    Parameters
    ----------
    inputfn:
        Path to the input shapefile.
    output_buffer_fn : str, optional
        Path to the output shapefile
    buffer_dist:
        The buffer distance (as string or float, default is "0")
    outformat:
        Format of the output shapefile ("ESRI Shapefile" by default)
    """

    buffer_dist = float(buffer_dist)
    inputds = ogr.Open(inputfn)
    inputlyr = inputds.GetLayer()

    shpdriver = ogr.GetDriverByName(outformat)
    if Path(output_buffer_fn).exists():
        shpdriver.DeleteDataSource(output_buffer_fn)
    output_bufferds = shpdriver.CreateDataSource(output_buffer_fn)
    bufferlyr = output_bufferds.CreateLayer(
        output_buffer_fn, srs=inputlyr.GetSpatialRef(), geom_type=ogr.wkbPolygon
    )
    feature_defn = bufferlyr.GetLayerDefn()

    copy_fields(bufferlyr, inputlyr)

    for feature in inputlyr:
        ingeom = feature.GetGeometryRef()
        geom_buffer = ingeom.Buffer(buffer_dist)
        if geom_buffer.GetArea() != 0:
            out_feature = ogr.Feature(feature_defn)
            out_feature.SetGeometry(geom_buffer)
            # copy input value
            for field_id in range(0, feature_defn.GetFieldCount()):
                out_feature.SetField(
                    feature_defn.GetFieldDefn(field_id).GetNameRef(),
                    feature.GetField(field_id),
                )

            bufferlyr.CreateFeature(out_feature)

    return bufferlyr


def copy_fields(buffer_layer: ogr.Layer, input_layer: ogr.Layer) -> None:
    """
    Copy field definitions from the input layer to the output layer.

    Parameters
    ----------
    input_layer:
        The input layer from which field definitions will be copied.
    buffer_layer : ogr.Layer
        The output layer to which field definitions will be copied.
    """
    in_layer_defn = input_layer.GetLayerDefn()
    for field_id in range(0, in_layer_defn.GetFieldCount()):
        field_defn = in_layer_defn.GetFieldDefn(field_id)
        buffer_layer.CreateField(field_defn)
