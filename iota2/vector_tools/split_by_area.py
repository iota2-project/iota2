#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Module used to distribute polygons of each class in a given number of folds, based on the
polygons' areas.
"""
import random
import sys
from typing import Any

from osgeo import ogr

from iota2.typings.i2_types import PathLike
from iota2.vector_tools import vector_functions as vf


def split_by_area(
    areas: list[tuple[int, int]], folds: int
) -> tuple[list[list[tuple[int, float]]], list[tuple[int, float]]]:
    """
    Split a list of tuples into a list of lists of tuples. The number of splits is defined by the
    `folds` parameter. The splits are done in a way that equilibrates the sum of heights of each
    split.

    Parameters
    ----------
    areas:
        List of tuples (int, int) of areas
    folds:
        Number of splits

    Notes
    -----
    Return:
        - A list of lists of tuples (the split initial list)
        - A list of tuples each containing:
            - The index of a split
            - The sum of heights of all areas in the corresponding split
    For example, splitByArea([(2, 5), (1, 30), (5, 5), (1, 1), (7, 2)], 2) can return:
        - [[(1, 30), (7, 2), (1, 1)], [(5, 5), (2, 5)]]
        - [(1, 33), (2, 10)]
    (a random process is used for the equilibration process, so multiple outputs are possible)
    """

    outputfolds = [[] for _ in range(folds)]  # type: list[list[tuple[int, float]]]
    areas = sorted(areas, key=lambda x: x[1])[::-1]

    offset = 0
    flag = 0
    while flag == 0:
        index_rand = random.sample(list(range(offset, offset + folds)), folds)
        # to manage the end
        if offset + 1 > len(areas) - folds:
            index_rand = random.sample(
                list(range(offset, len(areas))), len(areas) - offset
            )
            flag = 1
        for cpt, ind in enumerate(index_rand):
            outputfolds[cpt].append(areas[ind])
        offset += folds

    totares = []  # list[tuple[int, float]]
    for idx, fold in enumerate(outputfolds):
        totares.append((idx + 1, sum(x[1] for x in fold)))

    return outputfolds, totares


def get_fid_area(shapefile: PathLike, classf: str = "") -> list[list]:
    """Return a list of lists of feature characteristics, for all features in shape.
    The characteristics are:
        - Feature ID
        - Field value if classf is set, 1 otherwise
        - Feature's area
    Or [[fid1, field_value1, area1], [...], ...]

    Parameters
    ----------
    shapefile:
        Path to input shapefile
    classf:
        Field name (default is an empty string)
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    datasource = driver.Open(shapefile, 0)
    layer = datasource.GetLayer()

    fieldlist = vf.get_fields(layer)
    if classf != "" and classf is not None:
        try:
            fieldlist.index(classf)
        except ValueError:
            print(f"The field {classf} does not exist in the input shapefile")
            print(
                f"You must choose one of these existing fields : {' / '.join(fieldlist)}"
            )
            sys.exit(-1)

    listid = []
    for feat in layer:
        geom = feat.GetGeometryRef()
        if geom is not None:
            if classf != "" and classf is not None:
                listid.append([feat.GetFID(), feat.GetField(classf), geom.GetArea()])
            else:
                # fake class to 1 if no field class provided
                listid.append([feat.GetFID(), 1, geom.GetArea()])

    layer = datasource = None
    return listid


def get_features_folds(
    features: list[list], folds: int
) -> list[tuple[Any, list[list[tuple[int, float]]], list[tuple[int, float]]]]:
    """
    Return a list of tuples, each containing (for a given feature):
        - A specific field value
        - A list of lists of areas (list of features' areas, split in  sub-lists)
        - A list of tuples, containing the sum of areas
    (see documentation of `splitByArea` for more details on the last two components)

    Parameters
    ----------
    features:
        List of features' characteristics: [[fid1, field_value1, area1], [...], ...]
    folds:
        Number of splits in for the areas list
    """
    # features = [[fid1, field_value1, area1], [...], ...]
    classes = {y for x, y, z in features}
    statsclasses = []
    for classval in classes:
        areas = [(x, z) for x, y, z in features if y == classval]
        outputfolds, totares = split_by_area(areas, folds)
        statsclasses.append((classval, outputfolds, totares))
    return statsclasses
