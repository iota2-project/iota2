# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Change existing field name of an input shapefile."""
import sys

from osgeo import ogr

from iota2.typings.i2_types import PathLike
from iota2.vector_tools import delete_field
from iota2.vector_tools import vector_functions as vf


def change_name(filein: PathLike, fieldin: str, fieldout: str) -> None:
    """Change existing field name of an input shapefile."""
    field_list = vf.get_fields(filein)
    if fieldout in field_list:
        print(f"Field name {fieldout} already exists")
        sys.exit(1)

    # Get input file and field characteritics
    source = ogr.Open(filein, 1)
    layer = source.GetLayer()
    layer_defn = layer.GetLayerDefn()
    field_index = layer_defn.GetFieldIndex(fieldin)

    # Create the out field with in field characteristics
    try:
        field_type_code = layer_defn.GetFieldDefn(field_index).GetType()
        field_width = layer_defn.GetFieldDefn(field_index).GetWidth()
        field_precision = layer_defn.GetFieldDefn(field_index).GetPrecision()
    except AttributeError:
        print(f"Field {fieldin} not exists in the input shapefile")
        sys.exit(0)

    new_field = ogr.FieldDefn(fieldout, field_type_code)
    new_field.SetWidth(field_width)
    new_field.SetPrecision(field_precision)
    layer.CreateField(new_field)

    for feat in layer:
        val = feat.GetField(fieldin)
        layer.SetFeature(feat)
        feat.SetField(fieldout, val)
        layer.SetFeature(feat)

    layer = feat = new_field = source = None

    delete_field.delete_field(filein, fieldin)
