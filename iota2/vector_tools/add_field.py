# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Add a new field to an input shapefile and populate it."""
import sqlite3
from typing import Any

from osgeo import ogr

from iota2.vector_tools.vector_functions import get_driver


def add_field(
    filein: str,
    name_field: str,
    value_field: Any,
    value_type: type | str | None = None,
    f_width: int | None = None,
) -> int:
    """Add a new field to an input shapefile and populate it."""
    driver_name = get_driver(filein)
    driver = ogr.GetDriverByName(driver_name)
    source = driver.Open(filein, 1)
    layer = source.GetLayer()
    layer_name = layer.GetName()
    if not value_type:
        try:
            int(value_field)
            sqlite_type = "int"
            new_field1 = ogr.FieldDefn(name_field, ogr.OFTInteger)
        except ValueError:
            sqlite_type = "varchar"
            new_field1 = ogr.FieldDefn(name_field, ogr.OFTString)
    elif value_type == str:
        new_field1 = ogr.FieldDefn(name_field, ogr.OFTString)
        sqlite_type = "varchar"
    elif value_type == int:
        new_field1 = ogr.FieldDefn(name_field, ogr.OFTInteger)
        sqlite_type = "int"
    elif value_type == "int64":
        new_field1 = ogr.FieldDefn(name_field, ogr.OFTInteger64)
        sqlite_type = "BIGINT"
    elif value_type == float:
        new_field1 = ogr.FieldDefn(name_field, ogr.OFTReal)
        sqlite_type = "float"
    else:
        raise ValueError("can't add field")
    if f_width:
        new_field1.SetWidth(f_width)

    if driver_name == "ESRI Shapefile":
        layer.CreateField(new_field1)
        for feat in layer:
            layer.SetFeature(feat)
            feat.SetField(name_field, value_field)
            layer.SetFeature(feat)
    elif driver_name == "SQLite":
        conn = sqlite3.connect(filein)
        cursor = conn.cursor()
        cursor.execute(
            f"alter table {layer_name} add column {name_field} "
            f"{sqlite_type} default '{value_field}'"
        )
        conn.commit()
        cursor.close()

    return 0
