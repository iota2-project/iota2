#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module for boundary fusion step."""
import logging
import shutil
from pathlib import Path

import geopandas as gpd
import itk
import numpy as np
import otbApplication

from iota2.common import otb_app_bank as otb
from iota2.typings.i2_types import DistanceMapParameters, OtbAppWithDep, OtbDep

LOGGER = logging.getLogger("distributed.worker")


def get_sorted_regions_from_df(dataframe: gpd.geodataframe, field: str) -> list[str]:
    """Return the numerically sorted regions."""
    all_values = dataframe[field].unique()
    # ensure that regions are sorted in numerical order
    all_values = [int(reg) for reg in all_values]
    all_values.sort()
    sorted_regions = [str(reg) for reg in all_values]
    return sorted_regions


def shape_to_otb(
    shape: str, ref: str, background: int = 0, foreground: int = 1
) -> otbApplication.Application:
    """Convert a vector file to an otb application.

    Parameters
    ----------
    shape:
        the vector file to rasterize
    ref:
        the raster reference for rasterization
    background:
        set a value for no data (no geometry)
    foreground:
        set a value for valid data
    """
    rasterize, _ = otb.create_application(
        otb.AvailableOTBApp.RASTERIZATION,
        {
            "in": shape,
            "out": shape.replace("shp", "tif"),
            "im": ref,
            "background": background,
            "mode": "binary",
            "mode.binary.foreground": foreground,
            "pixType": "uint8",
        },
    )
    rasterize.Execute()

    return rasterize


def mask_ecoregion(region_file: str, region_mask: gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    """
    Apply a mask to an ecoregion. Return the masked region as a geodataframe

    Parameters
    ----------
    region_file:
        Path to the region file to be masked
    region_mask:
        Geodataframe used to mask the region
    """
    ecoregion_gdf = gpd.read_file(region_file)
    masked_ecoregion = gpd.overlay(ecoregion_gdf, region_mask, how="intersection")

    return masked_ecoregion


def buffer_mask_ecoregion(
    region_file: str, region_mask: gpd.GeoDataFrame, buffer_size: int
) -> gpd.GeoDataFrame:
    """
    Masks and apply a buffer to an ecoregion. Return the result region as a geodataframe.

    Parameters
    ----------
    region_file:
        Path to the region file to be masked
    region_mask:
        Geodataframe used to mask the region
    buffer_size:
        Size of the buffer to apply
    """
    masked_ecoregion = mask_ecoregion(region_file, region_mask)
    final_buffer = masked_ecoregion.copy()
    final_buffer["geometry"] = final_buffer.geometry.buffer(buffer_size)

    return final_buffer


def distance_maps(
    mask_gdf: gpd.GeoDataFrame,
    wdir: str,
    common_mask_buffer: str,
    distance_map_params: DistanceMapParameters,
    logger: logging.Logger,
) -> list[str]:
    """
    Write distance map for a region.

    Parameters
    ----------
    mask_gdf:
        Mask as a geopandas dataframe
    wdir:
        Folder to store temporary results
    common_mask_buffer:
        Common mask with a buffer
    distance_map_params:
        Parameters used to compute the distance map
    logger:
        Logger
    """
    # determine all the regions on the working area
    assert distance_map_params.paths and distance_map_params.paths.region_file
    assert distance_map_params.region_field
    ecoregion_gdf = gpd.read_file(distance_map_params.paths.region_file)
    global_regions = get_sorted_regions_from_df(
        ecoregion_gdf, distance_map_params.region_field
    )

    # Compute the negative buffer to compute the distance map
    output_file = str(
        Path(distance_map_params.paths.output_path)
        / f"{distance_map_params.tile}_buffer_area.shp"
    )
    ecoregion_buffer_neg = buffer_mask_ecoregion(
        distance_map_params.paths.region_file,
        mask_gdf,
        -distance_map_params.interior_buffer_size,
    )
    ecoregion_buffer_neg = ecoregion_buffer_neg[ecoregion_buffer_neg.area > 0]
    ecoregion_buffer_neg.to_file(output_file)

    regions = get_regions(
        distance_map_params.paths.region_file,
        distance_map_params.region_field,
        mask_gdf,
    )

    # Compute each distance map for all regions
    all_maps = []
    for region in global_regions:
        # handle the case when the tile is inside a unique region
        if len(regions) == 1:
            output_map_unique_reg = compute_dist_map_mono_region(
                distance_map_params.paths.common_mask, wdir
            )
            all_maps.append(output_map_unique_reg)
        elif region in regions:
            ss_region = ecoregion_buffer_neg[
                ecoregion_buffer_neg[distance_map_params.region_field] == region
            ]

            if ss_region.empty:
                logger.info(
                    f"For tile {distance_map_params.tile} the region {region} is skipped "
                    "as there is no feature in"
                )
                all_maps.append(str(Path(wdir) / "no_distance.tif"))
                continue

            output_map = compute_dist_map_multi_regions(
                common_mask_buffer, distance_map_params, region, ss_region, wdir
            )
            all_maps.append(output_map)
        else:
            # if regions not in tile insert empty map
            all_maps.append(str(Path(wdir) / "no_distance.tif"))
    return all_maps


def get_regions(
    region_file: str, region_field: str, mask_gdf: gpd.GeoDataFrame
) -> list[str]:
    """
    Return all unique regions covered by the tile.

    Parameters
    ----------
    region_file:
        Path to the region vector file
    region_field:
        The region column name in vector file
    mask_gdf:
        Geodataframe containing the mask
    """
    masked_ecoregion = mask_ecoregion(region_file, mask_gdf)
    regions = list(masked_ecoregion[region_field].unique())
    return regions


def compute_dist_map_mono_region(common_mask: str, wdir: str) -> str:
    """
    Compute the distance map in the case where the tile is over only one region.
    Return the path to the output map produced.

    Parameters
    ----------
    common_mask:
        Path to the buffered common mask
    wdir:
        Folder to store temporary files
    """
    output_map_unique_reg = str(Path(wdir) / "uni_distance.tif")
    bandmath, _ = otb.create_application(
        otb.AvailableOTBApp.BAND_MATH,
        {
            "il": [common_mask],
            "exp": "1000",
            "out": output_map_unique_reg,
            "pixType": "uint16",
        },
    )
    bandmath.ExecuteAndWriteOutput()
    return output_map_unique_reg


def compute_dist_map_multi_regions(
    common_mask_buffer: str,
    distance_map_params: DistanceMapParameters,
    region: str,
    ss_region: gpd.GeoDataFrame,
    wdir: str,
) -> str:
    """
    Compute the distance map in the case where the tile is over multiple regions.
    Return the path to the output map produced.

    Parameters
    ----------
    common_mask_buffer:
        Path to the buffered common mask
    distance_map_params:
        Parameters used to compute the distance map
    region:
        Region name
    ss_region:
        Dataframe containing the regions
    wdir:
        Folder to store temporary files
    """
    tmp_pos = str(
        Path(wdir) / f"Boundary_MASK_region_{region}_{distance_map_params.tile}.shp"
    )
    tmp_reg = str(Path(wdir) / f"tile_{distance_map_params.tile}_region_{region}.shp")
    dep_app: OtbDep = []
    ss_region.to_file(tmp_reg)
    # rasterize over the extended area
    region_raster, dep_app = get_distance(
        common_mask_buffer,
        dep_app,
        distance_map_params,
        tmp_pos,
        tmp_reg,
    )
    # Write image and apply mask
    # crop the weights for the tile
    assert distance_map_params.paths and distance_map_params.paths.common_mask
    output_map = tmp_reg.replace(".shp", "_distance.tif")
    roi, _ = otb.create_application(
        otb.AvailableOTBApp.EXTRACT_ROI,
        {
            "in": region_raster,
            "out": output_map,
            "mode": "fit",
            "mode.fit.im": distance_map_params.paths.common_mask,
            "pixType": "uint16",
        },
    )
    roi.ExecuteAndWriteOutput()
    return output_map


def get_distance(
    common_mask_buffer: str,
    dep_app: list[otbApplication.Application],
    distance_map_params: DistanceMapParameters,
    tmp_pos: str,
    tmp_reg: str,
) -> OtbAppWithDep:
    """
    Return the distance map as an otb app

    Parameters
    ----------
    common_mask_buffer:
        Common mask with a buffer
    dep_app:
        List of otb app dependencies
    distance_map_params:
        Parameters used to compute the distance map
    tmp_pos:
        Path to temporary boundary file
    tmp_reg:
        Path to temporary region file
    """
    region_raster = shape_to_otb(tmp_reg, common_mask_buffer)
    dep_app.append(region_raster)
    region_raster = region_raster.ExportImage("out")
    # Distance is in pixel units
    daniel = itk.DanielssonDistanceMapImageFilter.New(  # pylint:disable=no-member
        itk.GetImageViewFromArray(region_raster["array"])
    )
    np_view = itk.GetArrayViewFromImage(daniel.GetOutput())
    region_raster["array"] = np_view
    boundary_raster = shape_to_otb(tmp_pos, common_mask_buffer)
    dep_app.append(boundary_raster)
    boundary_raster = boundary_raster.ExportImage("out")
    dmax = (
        distance_map_params.interior_buffer_size
        + distance_map_params.exterior_buffer_size
    ) / max(
        abs(distance_map_params.spatial_res[0]), abs(distance_map_params.spatial_res[1])
    )
    masked_daniel = np.where(boundary_raster["array"] > 0, np_view, dmax)
    dmid = distance_map_params.interior_buffer_size / distance_map_params.spatial_res[0]
    weights = np.where(
        masked_daniel < dmax,
        (-0.5 / dmid) * masked_daniel + 1,
        distance_map_params.epsilon,
    )
    weights = np.where(
        weights < distance_map_params.epsilon, distance_map_params.epsilon, weights
    )
    region_raster["array"] = weights * 1000  # save as int16
    return region_raster, dep_app


def compute_distance_map(
    distance_map_params: DistanceMapParameters,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """Compute the distance map from regions.

    Parameters
    ----------
    distance_map_params:
        Parameters used to compute the distance map
    working_directory:
        folder to store intermediate results
    logger:
        logger
    """
    assert (
        distance_map_params.paths
        and distance_map_params.paths.output_path
        and distance_map_params.paths.masks_region_path
    )
    Path(distance_map_params.paths.output_path).mkdir(parents=True, exist_ok=True)
    Path(distance_map_params.paths.masks_region_path).mkdir(parents=True, exist_ok=True)
    wdir = (
        working_directory
        if working_directory
        else distance_map_params.paths.output_path
    )
    # create a mask to merge classif after fusion
    # Use a buffer to englobe the entire tile and use this to rasterize shapes
    common_mask_buffer = str(
        Path(wdir) / f"{distance_map_params.tile}_buffered_mask.tif"
    )
    mask_gdf = write_buffered_common_mask(common_mask_buffer, distance_map_params, wdir)

    # remove all regions outside the buffered area
    ecoregion_gdf = gpd.read_file(distance_map_params.paths.region_file)
    # Compute the positive buffer to generate mask
    ecoregion_buffer_pos = buffer_mask_ecoregion(
        distance_map_params.paths.region_file,
        mask_gdf,
        distance_map_params.exterior_buffer_size,
    )

    # Write classification masks with the positive buffer to ensure that
    # boundaries regions have classified pixels
    write_buffered_classif_masks(
        distance_map_params, ecoregion_buffer_pos, ecoregion_gdf, wdir
    )

    # create empty map to create the final R dimension distance map
    bandmath, _ = otb.create_application(
        otb.AvailableOTBApp.BAND_MATH,
        {
            "il": [distance_map_params.paths.common_mask],
            "exp": "0",
            "out": str(Path(wdir) / "no_distance.tif"),
            "pixType": "uint8",
        },
    )
    bandmath.ExecuteAndWriteOutput()
    # Compute each distance map for all regions
    liste_input = distance_maps(
        mask_gdf,
        wdir,
        common_mask_buffer,
        distance_map_params,
        logger,
    )

    # Concatenate all distance maps
    concat, _ = otb.create_application(
        otb.AvailableOTBApp.CONCATENATE_IMAGES,
        {
            "il": liste_input,
            "out": str(Path(wdir) / f"{distance_map_params.tile}_distance_map.tif"),
            "pixType": "uint16",
        },
    )
    concat.ExecuteAndWriteOutput()

    if working_directory:
        shutil.copy(
            str(Path(wdir) / f"{distance_map_params.tile}_distance_map.tif"),
            str(
                Path(distance_map_params.paths.output_path)
                / f"{distance_map_params.tile}_distance_map.tif"
            ),
        )


def write_buffered_classif_masks(
    distance_map_params: DistanceMapParameters,
    ecoregion_buffer_pos: gpd.GeoDataFrame,
    ecoregion_gdf: gpd.GeoDataFrame,
    wdir: str,
) -> None:
    """
    Write classif masks with a positive buffer

    Parameters
    ----------
    distance_map_params:
        Parameters used to compute the distance map
    ecoregion_buffer_pos:
        Region with a buffer
    ecoregion_gdf:
        Region without a buffer
    wdir:
        Folder to store temporary files
    """
    ecoregion_diff = gpd.overlay(
        ecoregion_buffer_pos, ecoregion_gdf, how="intersection"
    )
    assert distance_map_params.region_field
    assert distance_map_params.paths and distance_map_params.paths.masks_region_path
    assert distance_map_params.paths.common_mask
    for region in ecoregion_diff[distance_map_params.region_field + "_1"].unique():
        mask_region = f"Boundary_MASK_region_{region}_{distance_map_params.tile}.shp"
        ss_region = ecoregion_diff[
            ecoregion_diff[distance_map_params.region_field + "_1"] == region
        ]
        ss_region.to_file(str(Path(wdir) / mask_region))
        rasterize_mask_region, _ = otb.create_application(
            otb.AvailableOTBApp.RASTERIZATION,
            {
                "in": str(Path(wdir) / mask_region),
                "out": str(
                    Path(distance_map_params.paths.masks_region_path)
                    / mask_region.replace(".shp", ".tif")
                ),
                "im": distance_map_params.paths.common_mask,
                "background": 0,
                "mode": "attribute",
                "mode.attribute.field": distance_map_params.region_field + "_1",
                "pixType": "uint8",
            },
        )
        rasterize_mask_region.ExecuteAndWriteOutput()


def write_buffered_common_mask(
    common_mask_buffer: str, distance_map_params: DistanceMapParameters, wdir: str
) -> gpd.GeoDataFrame:
    """
    Write the buffered common mask raster. Return the mask as a geodataframe.

    Parameters
    ----------
    common_mask_buffer:
        Path to the output raster
    distance_map_params:
        Parameters used to compute the distance map
    wdir:
        Directory to store temporary files
    """
    assert distance_map_params.paths and distance_map_params.paths.common_mask
    shape_mask = distance_map_params.paths.common_mask.replace(".tif", ".shp")
    mask_gdf = gpd.read_file(shape_mask)
    mask_gdf["geometry"] = mask_gdf.geometry.buffer(
        2 * distance_map_params.interior_buffer_size
    )
    mask_gdf.to_file(str(Path(wdir) / f"{distance_map_params.tile}_buffered_mask.shp"))
    spy = (
        -distance_map_params.spatial_res[1]
        if distance_map_params.spatial_res[1] > 0
        else distance_map_params.spatial_res[1]
    )
    raster_buffer, _ = otb.create_application(
        otb.AvailableOTBApp.RASTERIZATION,
        {
            "in": str(Path(wdir) / f"{distance_map_params.tile}_buffered_mask.shp"),
            "out": common_mask_buffer,
            "background": 0,
            "mode": "binary",
            "pixType": "uint8",
            "spx": distance_map_params.spatial_res[0],
            "spy": spy,
        },
    )
    raster_buffer.ExecuteAndWriteOutput()
    return mask_gdf
