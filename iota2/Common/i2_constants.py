#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
from dataclasses import dataclass


@dataclass(frozen=True)
class iota2_constants:

    # general
    i2_const_module: str = __file__
    i2_tasks_status_filename: str = "IOTA2_tasks_status.txt"

    # classification builder
    re_encoding_label_name: str = "i2label"
    re_encoding_label_file: str = "reference_data.shp"

    # vectorization builder
    i2_vecto_clip_field: str = "code"
    i2_vecto_clip_value: str = 0

    # obia builder
    i2_segmentation_field_name: str = "i2seg"

    # fill missing dates
    i2_missing_dates_no_data: int = 0
    i2_missing_dates_no_data_mask: int = 2
