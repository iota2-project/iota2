#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import sys
import ogr
import rtree
import fiona
import argparse
import warnings
from typing import List, Optional
from fiona.crs import from_epsg
from shapely.geometry import shape
from collections import OrderedDict
from shapely.geometry import mapping

from iota2.Common.Utils import run
from iota2.VectorTools import vector_functions as vf


def intersectSqlites(data_base1: str,
                     data_base2: str,
                     tmp: str,
                     output: str,
                     epsg: int,
                     operation: str,
                     keepfields: List[str],
                     vectformat='SQLite',
                     data_base1_FID_field_name: Optional[str] = None,
                     data_base2_FID_field_name: Optional[str] = None) -> bool:
    """Perform vector intersection by using fiona.
    If there is intersection return True, else False.

    Parameters
    ----------
    data_base1 :
        input vector path
    data_base2 :
        input vector path
    tmp:
        not used
    output:
        output vector path    
    epsg:
        epsg code
    operation:
        not used
    keepfields:
        list of fields to keep
    vectformat:
        not used
    data_base1_FID_field_name:
        add FID column to first input database
    data_base1_FID_field_name
        add FID column to second input database

    Notes
    -----
        Intersections are done thanks to fiona which does not understand
        'SQLite' format, then every in/out vector are translated into 
        ShapeFile format if necessary
    """

    output_no_ext, output_ext = os.path.splitext(output)
    if output_ext == ".sqlite":
        output = f"{output_no_ext}.shp"

    db1_no_ext, db1_ext = os.path.splitext(data_base1)
    if db1_ext == ".sqlite":
        db1 = f"{db1_no_ext}.shp"
        if os.path.exists(db1):
            raise OSError(f"{db1} already exits, please remove it")
        run(f"ogr2ogr {db1} {data_base1}")
    else:
        db1 = data_base1

    db2_no_ext, db2_ext = os.path.splitext(data_base2)
    if db2_ext == ".sqlite":
        db2 = f"{db2_no_ext}.shp"
        if os.path.exists(db2):
            raise OSError(f"{db2} already exits, please remove it")
        run(f"ogr2ogr {db2} {data_base2}")
    else:
        db2 = data_base2

    with fiona.open(db1, "r") as layer1:
        with fiona.open(db2, "r") as layer2:
            new_schema = layer2.schema.copy()
            if keepfields:
                properties = OrderedDict()
                for field in keepfields:
                    if field in layer1.schema["properties"]:
                        properties[field] = layer1.schema["properties"][field]
                    elif field in layer2.schema["properties"]:
                        properties[field] = layer2.schema["properties"][field]
                    else:
                        warnings.warn(
                            f"Can't find field '{field}' in input databases : {vf.getFields(db1)} and {vf.getFields(db2)}"
                        )
                new_schema = {"properties": properties, "geometry": "Polygon"}
            else:
                for field_name, field_value in layer1.schema[
                        "properties"].items():
                    new_schema["properties"][field_name] = field_value

            if data_base1_FID_field_name:
                new_schema["properties"][data_base1_FID_field_name] = "int:9"
            if data_base2_FID_field_name:
                new_schema["properties"][data_base2_FID_field_name] = "int:9"

            crs = from_epsg(int(epsg))
            with fiona.open(output,
                            "w",
                            crs=crs,
                            driver="ESRI Shapefile",
                            schema=new_schema) as layer_out:
                index = rtree.index.Index()
                for feat1 in layer1:
                    fid = int(feat1["id"])
                    geom1 = shape(feat1["geometry"])
                    index.insert(fid, geom1.bounds)
                for feat2 in layer2:
                    geom2 = shape(feat2["geometry"])
                    for fid in list(index.intersection(geom2.bounds)):
                        feat1 = layer1[fid]
                        geom1 = shape(feat1["geometry"])
                        geom_intersection = geom1.intersects(geom2)
                        if geom_intersection:
                            prop = {}
                            if keepfields:
                                for field in keepfields:
                                    try:
                                        if field in feat1["properties"]:
                                            prop[field] = feat1["properties"][
                                                field]
                                        else:
                                            prop[field] = feat2["properties"][
                                                field]
                                    except KeyError:
                                        pass
                            else:
                                for f_n, f_v in feat1["properties"].items():
                                    prop[f_n] = f_v
                                for f_n, f_v in feat2["properties"].items():
                                    prop[f_n] = f_v
                            if data_base1_FID_field_name:
                                prop[data_base1_FID_field_name] = feat1["id"]
                            if data_base2_FID_field_name:
                                prop[data_base2_FID_field_name] = feat2["id"]
                            layer_out.write({
                                "geometry":
                                mapping(
                                    shape(feat1["geometry"]).intersection(
                                        shape(feat2["geometry"]))),
                                "properties":
                                prop
                            })

    output_field = vf.getFields(output)[0]
    features = vf.getFieldElement(output, field=output_field, elemType="str")

    if output_ext == ".sqlite":
        expected_out = f"{output_no_ext}.sqlite"
        run(f"ogr2ogr -f 'SQLite' {expected_out} {output}")
        vf.removeShape(output.replace(".shp", ""),
                       [".cpg", ".dbf", ".prj", ".shp", ".shx"])
    if db1_ext == ".sqlite":
        vf.removeShape(db1.replace(".shp", ""),
                       [".cpg", ".dbf", ".prj", ".shp", ".shx"])
    if db2_ext == ".sqlite":
        vf.removeShape(db2.replace(".shp", ""),
                       [".cpg", ".dbf", ".prj", ".shp", ".shx"])
    return bool(features)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print('      ' + sys.argv[0] + ' [options]')
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        usage = "usage: %prog [options] "
        parser = argparse.ArgumentParser(description = "Execute spatial operation (intersection or difference or union) "\
                                         "on two vector files (ESRI Shapefile or sqlite formats)")
        parser.add_argument("-s1", dest="s1", action="store", \
                            help="first sqlite vector file", required = True)
        parser.add_argument("-s2", dest="s2", action="store", \
                            help="second sqlite vector file", required = True)
        parser.add_argument("-tmp", dest="tmp", action="store", \
                            help="tmp folder", required = True)
        parser.add_argument("-output", dest="output", action="store", \
                            help="output path", required = True)
        parser.add_argument("-format", dest="outformat", action="store", \
                            help="OGR format (ogrinfo --formats). Default : SQLite ")
        parser.add_argument("-epsg", dest="epsg", action="store", \
                            help="EPSG code for projection. Default : 2154 - Lambert 93 ", type = int, default = 2154)
        parser.add_argument("-operation", dest="operation", action="store", \
                            help="spatial operation (intersection or difference or union). Default : intersection", default = "intersection")
        parser.add_argument("-keepfields", dest="keepfields", action="store", nargs="*", \
                            help="list of fields to keep in resulted vector file. Default : All fields")
        args = parser.parse_args()

        if args.operation not in ['intersection', 'difference', 'union']:
            raise Exception(
                "Only Intersection, Difference and Union permitted as Spatial Operation"
            )

        if args.outformat is None:
            intersectSqlites(args.s1, args.s2, args.tmp, args.output,
                             args.epsg, args.operation, args.keepfields)
        else:
            intersectSqlites(args.s1, args.s2, args.tmp, args.output,
                             args.epsg, args.operation, args.keepfields,
                             args.outformat)
