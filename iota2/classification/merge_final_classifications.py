#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Module containing function to merge classification using voting methods"""
import logging
import shutil
from pathlib import Path

import rasterio
from osgeo import gdal

import iota2.common.i2_constants as i2_const
from iota2.common import create_indexed_color_image as color
from iota2.common import file_utils as fut
from iota2.common import otb_app_bank as otbApp
from iota2.common.compression_options import compression_options
from iota2.common.otb_app_bank import AvailableOTBApp
from iota2.common.raster_utils import re_encode_raster
from iota2.typings.i2_types import ConfMatrixImageParameters, ConfMatrixParameters
from iota2.validation import results_utils as ru
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")

I2_CONST = i2_const.Iota2Constants()


def compute_fusion_options(
    final_classifications: list[str],
    method: str,
    indecidedlabel: int,
    dempstershafer_mob: str,
) -> dict:
    """Use to determine fusion parameters."""
    iota2_dir_final = Path(final_classifications[0]).parent
    if method == "majorityvoting":
        options = {
            "il": final_classifications,
            "method": method,
            "nodatalabel": "0",
            "indecidedlabel": str(indecidedlabel),
        }
    else:
        confusion_seed = [
            fut.file_search_and(
                str(Path(iota2_dir_final) / "TMP"),
                True,
                f"Classif_Seed_{run}.csv",
            )[0]
            for run in range(len(final_classifications))
        ]
        confusion_seed.sort()
        final_classifications.sort()
        options = {
            "il": final_classifications,
            "method": "dempstershafer",
            "nodatalabel": "0",
            "indecidedlabel": str(indecidedlabel),
            "method.dempstershafer.mob": dempstershafer_mob,
            "method.dempstershafer.cmfl": confusion_seed,
        }
    return options


def merge_final_classifications(
    otb_fusion_options: dict[str, str | int],
    otb_confusion_options: dict[str, str],
    iota2_dir: str,
    nom_path: str,
    color_file: str,
    keep_runs_results: bool = True,
    validation_shape: str | None = None,
    labels_raster_table: dict | None = None,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Merge classifications using either majority voting or Dempster-Shafer's method and evaluate the
    result. The classifications to be fused are all files named 'Classif_Seed_*.tif' in the /final
    directory. The output of the fusion is saved as 'Classifications_fusion.tif'. Finally, compute
    statistics using the results_utils library.

    Parameters
    ----------
    otb_fusion_options: dict[str, str]
        OTB understandable dictionary of parameters for FusionOfClassifications app
    otb_confusion_options: dict[str, str]
        OTB understandable dictionary of parameters for ComputeConfusionMatrix app
    iota2_dir : string
        path to the iota2's output path
    nom_path : string
        path to the nomenclature file
    color_file : string
        path to the color file description
    keep_runs_results : bool
        flag to inform if seeds results could be overwritten
    validation_shape : string
        path to a shape dedicated to validate fusion of classifications
    working_directory : string
        path to a working directory
    labels_raster_table : dictionary
        labels conversion dictionary : dict[old_label] = new_label
    logger : Logger
        Logger

    See Also
    --------
    results_utils.gen_confusion_matrix_fig
    results_utils.stats_report
    """

    iota2_dir_final = str(Path(iota2_dir) / "final")
    work_dir = iota2_dir_final
    wd_merge = str(Path(iota2_dir_final) / "merge_final_classifications")
    if working_directory:
        work_dir = working_directory
        wd_merge = working_directory

    fusion_path = str(Path(work_dir) / "Classifications_fusion.tif")
    fusion_color_index = compute_fusion(
        otb_fusion_options, fusion_path, color_file, labels_raster_table, logger
    )

    confusion_matrix = str(
        Path(iota2_dir_final)
        / "merge_final_classifications"
        / "confusion_mat_maj_vote.csv"
    )
    vector_val = fut.file_search_and(
        str(Path(iota2_dir_final) / "merge_final_classifications"),
        True,
        "majvote.sqlite",
    )

    otb_confusion_options["in"] = fusion_path
    compute_confusion_of_fusions_matrix(
        otb_confusion_options, validation_shape, vector_val, wd_merge
    )

    matrix_parameters = ConfMatrixParameters(
        indecidedlabel=int(otb_fusion_options["indecidedlabel"]),
        labels_table=labels_raster_table,
    )
    image_parameters = ConfMatrixImageParameters(
        dpi=900,
    )
    ru.gen_confusion_matrix_fig(
        csv_in=confusion_matrix,
        nomenclature_path=nom_path,
        out_png=str(Path(iota2_dir_final) / "fusionConfusion.png"),
        conf_matrix_image_parameters=image_parameters,
        conf_matrix_parameters=matrix_parameters,
    )

    if keep_runs_results:
        seed_results = fut.file_search_and(iota2_dir_final, True, "RESULTS.txt")[0]
        shutil.copy(seed_results, str(Path(iota2_dir_final) / "RESULTS_seeds.txt"))

    ru.stats_report(
        list_csv_in=[confusion_matrix],
        nomenclature_path=nom_path,
        out_report=str(Path(iota2_dir_final) / "RESULTS.txt"),
        labels_table=labels_raster_table,
        indecidedlabel=int(otb_fusion_options["indecidedlabel"]),
    )

    if working_directory:
        shutil.copy(fusion_path, iota2_dir_final)
        shutil.copy(fusion_color_index, iota2_dir_final)
        Path(fusion_path).unlink()


def compute_confusion_of_fusions_matrix(
    otb_confusion_options: dict,
    validation_shape: str | None,
    vector_val: list[str],
    wd_merge: str,
) -> None:
    """
    Compute the confusion matrix of the fusion of classifications using OTB.

    Parameters
    ----------
    otb_confusion_options : dict
        Dictionary of options for OTB's ConfusionMatrix application.
    validation_shape : str
        Path to the validation shape file.
    vector_val : list[str]
        List of paths to the vector files containing classification results.
    wd_merge : str
        Working directory where the merged validation vector will be stored.

    Returns
    -------
    None
    """
    fusion_vec_name = "fusion_validation"  # without extension
    if validation_shape:
        validation_vector = validation_shape
    else:
        vf.merge_db(fusion_vec_name, wd_merge, vector_val)
        validation_vector = str(Path(wd_merge) / fusion_vec_name) + ".sqlite"

    # The ref.vector.in key has to be placed before the ref.vector.field key
    otb_confusion_options = dict(
        {"ref.vector.in": validation_vector}, **otb_confusion_options
    )
    confusion, _ = otbApp.create_application(
        AvailableOTBApp.CONFUSION_MATRIX,
        otb_confusion_options,
    )
    confusion.ExecuteAndWriteOutput()


def compute_fusion(
    otb_fusion_options: dict,
    fusion_path: str,
    color_file: str,
    labels_raster_table: dict | None = None,
    logger: logging.Logger = LOGGER,
) -> str:
    """
    Compute the fusion of classification using the given parameters.

    Parameters
    ----------
    otb_fusion_options : dict
        OTB understandable dictionary containing options for the fusion OTB app
    fusion_path : str
        Path to the final fusion raster
    labels_raster_table : dictionary
        labels conversion dictionary : dict[old_label] = new_label
    color_file : string
        path to the color file description
    logger : Logger
        Logger

    Returns
    -------
    fusion_color_index : str
        Path to the color indexed raster
    """
    pix_format = [
        rasterio.open(raster_file).dtypes[0] for raster_file in otb_fusion_options["il"]
    ]
    pix_type = "uint32" if "uint32" in pix_format else "uint8"
    otb_fusion_options["pixType"] = "uint32" if "uint32" in pix_format else "uint8"
    otb_fusion_options["out"] = fusion_path
    logger.debug("fusion options:")
    logger.debug(otb_fusion_options)
    fusion_app, _ = otbApp.create_application(
        AvailableOTBApp.FUSION_OF_CLASSIFICATIONS, otb_fusion_options
    )
    logger.debug("START fusion of final classifications")
    fusion_app.ExecuteAndWriteOutput()
    logger.debug("END fusion of final classifications")
    encoded_raster_bool = False
    re_encoded_raster_path = fusion_path.replace(".tif", "_user_labels.tif")
    labels_conversion = None
    if labels_raster_table:
        reverse_labels = {v: k for k, v in labels_raster_table.items()}
        encoded_raster_bool, pix_type = re_encode_raster(
            fusion_path, re_encoded_raster_path, reverse_labels, logger=logger
        )
        labels_conversion = reverse_labels
    raster_to_color_path = fusion_path
    if encoded_raster_bool:
        raster_to_color_path = re_encoded_raster_path
    fusion_color_index: str = color.create_indexed_color_image(
        raster_to_color_path,
        color_file,
        co_option=[
            "COMPRESS=" + compression_options.algorithm,
            "PREDICTOR=" + str(compression_options.predictor),
        ],
        output_pix_type=gdal.GDT_Byte if pix_type == "uint8" else gdal.GDT_UInt16,
        labels_conversion=labels_conversion,
        logger=logger,
    )
    if encoded_raster_bool:
        Path(re_encoded_raster_path).unlink()
    return fusion_color_index
