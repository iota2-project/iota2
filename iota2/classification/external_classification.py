#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provide tools to do a user designed inference."""
import logging
import shutil
from dataclasses import dataclass
from pathlib import Path
from typing import Literal

import numpy as np
import rasterio

from iota2.classification.py_classifiers import (
    PredictionParameters,
    get_data_from_pipeline,
)
from iota2.common import file_utils as fu
from iota2.common import otb_app_bank as otb
from iota2.common.custom_numpy_features import FunctionNameWithParams
from iota2.common.file_utils import TileMergeOptions
from iota2.common.otb_app_bank import AvailableOTBApp
from iota2.configuration_files.check_config_parameters import (
    search_external_features_function,
)
from iota2.typings.i2_types import (
    CoordXY,
    CustomFeaturesParameters,
    PredictionOutput,
    RasterioSpatialMetadata,
)

LOGGER = logging.getLogger("distributed.worker")


@dataclass
class ExternalInferenceParameters:
    """
    Class containing parameters for computing inference using an external model

    Attributes
    ----------
    function_infer: list[FunctionNameWithParams]
        the inference function provided by the user
    inf_module_path: str
        the python file containing the inference function
    model: str
        the model used to classify the input data
    """

    function_infer: list[FunctionNameWithParams]
    inf_module_path: str
    model: str


def do_external_prediction(
    pred_parameters: PredictionParameters,
    pred_output: PredictionOutput,
    custom_features_parameters: CustomFeaturesParameters,
    inference_params: ExternalInferenceParameters,
    all_class_labels: list[int],
    number_of_chunks: int,
    targeted_chunk: int,
    working_dir: str,
    inference_mode: Literal["classif", "regression"],
) -> None:
    """
    Get features from OTB pipeline and call the user function.
    """
    # Get the OTB workflow
    if not all_class_labels and pred_output.proba:
        raise ValueError(
            "if probability map is asked, the parameter 'all_class_labels' "
            "become mandatory"
        )
    if Path(pred_output.classif).exists():
        print(f"{pred_output.classif} exists. Skip processing")
        return None
    if working_dir:
        back_out_classif = pred_output.classif
        back_out_confidence = pred_output.confidence
        back_out_proba = pred_output.proba
        pred_output.classif = str(Path(working_dir) / Path(pred_output.classif).name)
        pred_output.confidence = str(
            Path(working_dir) / Path(pred_output.confidence).name
        )
        if pred_output.proba:
            pred_output.proba = str(Path(working_dir) / Path(pred_output.proba).name)

    if number_of_chunks and targeted_chunk:
        if targeted_chunk > number_of_chunks - 1:
            raise ValueError("targeted_chunk must be inferior to the number of chunks")

    # Compute external features if required
    # mask is set to None to provide a classification of the entire tile
    # the returned mask array is usually used to not predict a part of chunk
    # this behaviour is disabled in this particular case
    pred_pipeline, geo_metadata = get_data_from_pipeline(
        pred_parameters=pred_parameters,
        pred_output=pred_output,
        working_dir=working_dir,
        custom_features_parameters=custom_features_parameters,
        mask=None,
        targeted_chunk=targeted_chunk,
        number_of_chunks=number_of_chunks,
    )
    # Load the external function
    # Only one function for prediction is allowed

    functions_founds = search_external_features_function(
        inference_params.inf_module_path, inference_params.function_infer
    )

    function_name = next(iter(functions_founds))

    # Provide the array, the model to the external function
    labels_map, confidence_map, proba_map = functions_founds[function_name](
        pred_pipeline.stack,
        [str(f).lower() for f in pred_pipeline.feat_labels],
        inference_params.model,
    )

    labels_map = rasterio.plot.reshape_as_raster(labels_map)

    if targeted_chunk is not None:
        pred_output.classif = pred_output.classif.replace(
            ".tif", f"_SUBREGION_{targeted_chunk}.tif"
        )
        pred_output.confidence = pred_output.confidence.replace(
            ".tif", f"_SUBREGION_{targeted_chunk}.tif"
        )
        if pred_output.proba:
            pred_output.proba = pred_output.proba.replace(
                ".tif", f"_SUBREGION_{targeted_chunk}.tif"
            )

    confidence_map, proba_map = write_output_maps(
        labels_map, confidence_map, proba_map, pred_output, geo_metadata, inference_mode
    )
    if working_dir:
        shutil.copy(pred_output.classif, back_out_classif)
        if confidence_map is not None and pred_output.confidence is not None:
            shutil.copy(pred_output.confidence, back_out_confidence)
        if (
            proba_map is not None
            and pred_output.proba is not None
            and back_out_proba is not None
        ):
            shutil.copy(pred_output.proba, back_out_proba)
    return None


def write_output_maps(
    labels_map: np.ndarray,
    confidence_map: np.ndarray | None,
    proba_map: np.ndarray | None,
    pred_output: PredictionOutput,
    geo_metadata: RasterioSpatialMetadata,
    mode: Literal["classif", "regression"],
) -> tuple[np.ndarray | None, np.ndarray | None]:
    """
    Write maps computed after inference from external model.

    Parameters
    ----------
    labels_map: np.ndarray
        Computed labels maps
    confidence_map: np.ndarray | None
        Computed confidence map
    proba_map: np.ndarray | None
        Computed proba map
    pred_output: PredictionOutput
        Dataclass used to stored output paths
    geo_metadata: RasterioSpatialMetadata
        Metadata used for rasterio
    mode: Literal["classif", "regression"]
        Either classif or regression

    Returns
    -------
    tuple[np.ndarray | None, np.ndarray | None]
        Tuple containing the confidence and proba maps
    """
    dtype: type
    if mode == "classif":
        dtype = np.uint8
    else:  # regression
        dtype = np.float32

    with rasterio.open(
        pred_output.classif,
        "w",
        driver="GTiff",
        height=labels_map.shape[1],
        width=labels_map.shape[2],
        count=labels_map.shape[0],
        crs=f"EPSG:{geo_metadata.epsg}",
        transform=geo_metadata.transform,
        dtype=dtype,
    ) as dest:
        dest.write(labels_map)
    if confidence_map is not None:
        confidence_map = rasterio.plot.reshape_as_raster(confidence_map)
        with rasterio.open(
            pred_output.confidence,
            "w",
            driver="GTiff",
            height=labels_map.shape[1],
            width=labels_map.shape[2],
            count=labels_map.shape[0],
            crs=f"EPSG:{geo_metadata.epsg}",
            transform=geo_metadata.transform,
            dtype=np.float32,
        ) as dest:
            dest.write(confidence_map)
    if proba_map is not None:
        proba_map = rasterio.plot.reshape_as_raster(proba_map)
        assert proba_map is not None
        with rasterio.open(
            pred_output.proba,
            "w",
            driver="GTiff",
            height=proba_map.shape[1],
            width=proba_map.shape[2],
            count=proba_map.shape[0],
            crs=f"EPSG:{geo_metadata.epsg}",
            transform=geo_metadata.transform,
            dtype=np.float32,
        ) as dest:
            dest.write(proba_map)
    return confidence_map, proba_map


def merge_external_classification_by_tiles(
    path_to_classif: str,
    output_path: str,
    tile: str,
    region: str,
    seed: int,
    res: CoordXY,
    mode: Literal["classif", "regression"],
    compress: dict[str, str] | None = None,
) -> None:
    """Merge all chunks of a tile for a given region and seed."""
    # Get product classif all products have subregion
    # as chunk mode is ensured

    if mode == "classif":
        list_classif_tile = fu.file_search_and(
            str(Path(path_to_classif) / "full_pred"),
            True,
            tile,
            f"Classif_{tile}_model_{region}_seed_{seed}",
            ".tif",
        )
        classif_map = str(
            Path(output_path) / f"Classif_{tile}_model_{region}_seed_{seed}.tif"
        )
    else:  # regression
        list_classif_tile = fu.file_search_and(
            path_to_classif,
            True,
            tile,
            f"Regression_{tile}_model_{region}_seed_{seed}",
            ".tif",
        )
        classif_map = str(
            Path(output_path) / f"Regression_{tile}_model_{region}_seed_{seed}.tif"
        )

    # Get product confidence (if exists)
    list_confidence_tile = fu.file_search_and(
        str(Path(path_to_classif) / "full_pred"),
        True,
        f"{tile}_model_{region}_confidence_seed_{seed}",
        ".tif",
    )
    # Get product proba (if exists)
    list_probas_tile = fu.file_search_and(
        str(Path(path_to_classif) / "full_pred"),
        True,
        f"PROBAMAP_{tile}_model_{region}_seed_{seed}",
        ".tif",
    )

    confidence_map = str(
        Path(output_path) / f"{tile}_model_{region}_confidence_seed_{seed}.tif"
    )
    proba_map = str(
        Path(output_path) / f"PROBAMAP_{tile}_model_{region}_seed_{seed}.tif"
    )

    if compress is None:
        compress_options = {"COMPRESS": "LZW", "BIGTIFF": "YES"}
    else:
        compress_options = compress
    if list_classif_tile:
        merge_options = TileMergeOptions(
            spatial_resolution=res,
            output_pixel_type="Float32",
            creation_options=compress_options,
        )
        fu.assemble_tile_merge(list_classif_tile, classif_map, merge_options)

    else:
        raise ValueError("No external classification found")
    if list_confidence_tile:

        merge_options = TileMergeOptions(
            spatial_resolution=res,
            output_pixel_type="Float32",
            creation_options=compress_options,
        )
        fu.assemble_tile_merge(list_confidence_tile, confidence_map, merge_options)

    if list_probas_tile:

        merge_options = TileMergeOptions(
            spatial_resolution=res,
            output_pixel_type="Float32",
            creation_options=compress_options,
        )
        fu.assemble_tile_merge(list_probas_tile, proba_map, merge_options)


def apply_mask_to_map(
    path_to_classif: str,
    tile: str,
    region: str,
    mask: str,
    runs: int,
    output_folder: str,
    working_directory: str | None = None,
) -> None:
    """Apply the classification mask to maps to prepare the tiles mosaic.

    Parameters
    ----------
    path_to_classif:
        the folder where the tiled maps are stored
    tile:
        the tile name to process
    region:
        the current region considered
    mask:
        the mask name
    runs:
        the number of seeds
    output_folder:
        path to store the masked maps
    """
    work_dir = working_directory if working_directory is not None else output_folder
    full_input_maps = []
    for seed in range(runs):
        input_maps = fu.file_search_and(
            path_to_classif, True, tile, f"model_{region}", f"seed_{seed}.tif"
        )
        full_input_maps += input_maps
    # Prevent the case when the step succeed but do nothing
    if not input_maps:
        raise ValueError(
            f"Not input maps found in {path_to_classif},"
            "according the template name _model_X*_seed_[0-9].tif"
        )
    # Classif mask contains the region ID
    mask_bin = str(Path(work_dir) / f"{tile}_{region}_tmp_mask.tif")
    bandmath_mask, _ = otb.create_application(
        AvailableOTBApp.BAND_MATH,
        {"il": [mask], "exp": "im1b1==0?0:1", "out": mask_bin, "pixType": "uint8"},
    )
    otb.execute_app(bandmath_mask)
    for in_map in input_maps:
        bandmath_app, _ = otb.create_application(
            AvailableOTBApp.BAND_MATH_X,
            {
                "il": [mask_bin, in_map],
                "exp": "im1b1*im2",
                "out": str(Path(output_folder) / Path(in_map).name),
            },
        )
        otb.execute_app(bandmath_app)
