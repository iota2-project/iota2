#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module interfacing pytorch for iota2."""
import pickle
from collections.abc import Generator
from dataclasses import dataclass
from itertools import zip_longest
from pathlib import Path
from typing import Literal

import numpy as np
import rasterio
import torch
from sklearn.preprocessing import StandardScaler
from torch.nn import functional as F

from iota2.classification.py_classifiers import (
    PredictionParameters,
    copy_to_working_dir,
    get_data_from_pipeline,
    reorder_features,
    update_output_path,
    write_output_maps,
)
from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.common.raster_utils import reorder_proba_map
from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetworkFactory
from iota2.learning.pytorch.torch_utils import (
    StatsCalculator,
    sensors_from_labels_to_index,
)
from iota2.typings.i2_types import (
    CustomFeaturesParameters,
    PredictionOutput,
    PredictionPipelineData,
    RasterioSpatialMetadata,
)


@dataclass
class PytorchPredictionParameters:
    """
    Parameters used for prediction with pytorch

    Attributes
    ----------
    nn_name: str
        Neural network name
    model_file: str
        Torch model save as a pickle file
    model_parameters_file: str
        file containing pytorch inputs arguments (to instantiate it)
    external_nn_module: str | None
        python module containing the user neural network class
    max_inference_size: str | None
        maximum batch size for inference
    """

    nn_name: str
    model_file: str
    model_parameters_file: str
    external_nn_module: str | None = None
    max_inference_size: int | None = None


@dataclass
class LabelsParameters:
    """
    Parameters concerning labels, used in pytorch prediction

    Attributes
    ----------
    actual_labels_order: list[str]
        Feature's order in feat_stack numpy array
    labels_converter: dict | StandardScaler | None
        In classif mode, a dictionary to convert labels
        In regression mode, a StandardScaler to (de)standardize
    nb_class: int | int
        Total number of classes
    """

    actual_labels_order: list[str]
    labels_converter: dict | StandardScaler | None = None
    nb_class: int | None = None


@dataclass
class TorchPredictionPaths:
    """
    Dataclass containing paths for the pytorch prediction

    Attributes
    ----------
    mask: str
        raster mask path
    encoder_convert_file
        file containing one hot encoder dictionary converter
    working_dir: str
        path to a working direction to store temporary data
    hyperparam_file : str
        pickle file containing model hyperparameters (batch's size...)
    """

    mask: str
    encoder_convert_file: str
    working_dir: str | None
    hyperparam_file: str


def max_inference_gen(
    tensor_as_array: np.ndarray, slice_len: int
) -> Generator[np.ndarray, None, None]:
    """
    Yield a numpy array split from the original array, of even slices (of a given length)

    Parameters
    ----------
    tensor_as_array: np.ndarray
        Tensor to be split
    slice_len: int
        Size of the slice to use

    Yields
    ------
    tensor_as_array: np.ndarray
        Sub-array split from input array
    """
    start = 0
    while start < tensor_as_array.shape[0]:
        yield tensor_as_array[start : start + slice_len, :]
        start += slice_len


def do_torch_prediction(
    torch_parameters: PytorchPredictionParameters,
    pred_pipeline: PredictionPipelineData,
    pred_output: PredictionOutput,
    geo_metadata: RasterioSpatialMetadata,
    sensors_ind: dict,
    labels_params: LabelsParameters,
    coeff_vect_proba: int = 1000,
    mode: Literal["classif", "regression"] = "classif",
) -> None:
    """Do torch prediction.

    Parameters
    ----------
    torch_parameters : PytorchPredictionParameters
        Torch parameters for prediction
    pred_pipeline : PredictionPipelineData
        Dataclass with the prediction pipeline's data (stack, masks, labels ...)
    pred_output : PredictionOutput
        Dataclass containing all prediction output files' paths
    geo_metadata : RasterioSpatialMetadata
        Spatial metadata as expected by rasterio
    sensors_ind:
        dictionary containing useful information to reshape the input 'feat_stack' according
        to sensors
    coeff_vect_proba:
        multiply vector of probabilities by this coeff
    mode:
        "classif" or "regression"
        regression mode skips label conversion
    """
    feat_stack = pred_pipeline.stack
    assert feat_stack is not None
    if labels_params.labels_converter is None:
        labels_params.labels_converter = {}
    inference_device = (
        torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    )
    if pred_output.proba and labels_params.nb_class is None:
        raise ValueError(
            "if parameter 'outproba' is enabled, the parameter 'nb_class' is mandatory"
        )
    mask_reshaped = None
    if pred_pipeline.mask is not None:
        mask_reshaped = pred_pipeline.mask.reshape(
            (pred_pipeline.mask.shape[0] * pred_pipeline.mask.shape[1]),
            pred_pipeline.mask.shape[2],
        ).astype(np.float32)

    if bool(sensors_ind) and (
        pred_pipeline.spatial_mask is None or np.sum(pred_pipeline.spatial_mask) != 0
    ):

        (batch_data_gen, batch_mask_gen), feat_stack, model = prepare_inference(
            labels_params.actual_labels_order,
            feat_stack,
            inference_device,
            mask_reshaped,
            pred_pipeline,
            torch_parameters,
        )

        confidence, predicted, proba = infer_all_batches(
            batch_data_gen,
            batch_mask_gen,
            inference_device,
            labels_params.labels_converter,
            mode,
            model,
            pred_output,
            sensors_ind,
        )
    else:
        assert feat_stack is not None
        predicted, confidence, proba = generate_empty_inference(
            feat_stack, mode, labels_params.nb_class, pred_output, pred_pipeline
        )
    assert feat_stack is not None
    predicted_array = predicted.reshape(feat_stack.shape[0], feat_stack.shape[1])
    if mode == "classif":
        confidence_map, labels_map, proba = reshape_maps(
            confidence,
            predicted_array,
            proba,
            coeff_vect_proba,
            feat_stack,
            pred_pipeline.spatial_mask,
            pred_output,
        )

        write_output_maps(pred_output, labels_map, confidence_map, proba, geo_metadata)
    else:
        # no conversion for regression
        labels_map = None

    # Deal with 0 values
    manage_zeros(
        predicted_array,
        pred_output.classif,
        labels_params.labels_converter,
        labels_map,
        geo_metadata,
        pred_pipeline.spatial_mask,
        mode,
    )


def prepare_inference(
    actual_labels_order: list[str],
    feat_stack: np.ndarray,
    inference_device: torch.device,
    mask_reshaped: np.ndarray | None,
    pred_pipeline: PredictionPipelineData,
    torch_parameters: PytorchPredictionParameters,
) -> tuple[
    tuple[
        Generator[np.ndarray, None, None],
        Generator[np.ndarray, None, None] | list,
    ],
    np.ndarray,
    torch.nn.Module,
]:
    """
    Prepare the inference, retrieve the batch data, the feat stack and pytorch model.

    Parameters
    ----------
    actual_labels_order: list[str]
        Feature's order in feat_stack numpy array
    feat_stack: np.ndarray
        The features stack
    inference_device: torch.device
        Pytorch device used for inference
    mask_reshaped: np.ndarray
        Feat mask reshaped in 2D
    pred_pipeline: PredictionPipelineData:
        Prediction pipeline's data contained in a dataclass
    torch_parameters: PytorchPredictionParameters
        Parameters for pytorch prediction

    Returns
    -------
    tuple[tuple, np.ndarray, torch.nn.Module]
        tuple containing:
            - Batch stack and masks (as generators of numpy arrays)
            - Feature stack
            - The loaded pytorch model
    """
    labels_order, model = load_model(inference_device, torch_parameters)
    new_feat_order = reorder_features(labels_order, actual_labels_order)
    if new_feat_order is not None:
        feat_stack = feat_stack[:, :, new_feat_order]
    feat_reshaped = feat_stack.reshape(
        (feat_stack.shape[0] * feat_stack.shape[1]), feat_stack.shape[2]
    )
    assert torch_parameters.max_inference_size is not None
    batch_data_gen = max_inference_gen(
        feat_reshaped, torch_parameters.max_inference_size
    )
    batch_mask_gen: Generator | list
    if pred_pipeline.mask is not None and mask_reshaped is not None:
        batch_mask_gen = max_inference_gen(
            mask_reshaped, torch_parameters.max_inference_size
        )
    else:
        batch_mask_gen = []
    return (batch_data_gen, batch_mask_gen), feat_stack, model


def infer_all_batches(
    batch_data_gen: Generator,
    batch_mask_gen: Generator | list,
    inference_device: torch.device,
    labels_converter: dict | StandardScaler,
    mode: Literal["classif", "regression"],
    model: torch.nn.Module,
    pred_output: PredictionOutput,
    sensors_ind: dict,
) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor | None]:
    """
    Infer all batches using pytorch. Return the computed confidence, predicted and proba maps.

    Parameters
    ----------
    batch_data_gen: Generator:
        Generator containing data as a numpy array for the inference
    batch_mask_gen: Generator | list
        Generator containing masks as a numpy array for the inference
    inference_device: torch.device
        Device to use for the inference
    labels_converter: dict | StandardScaler
        in classif mode, a dictionary to convert labels
        in regression mode, a StandardScaler to (de)standardize
    mode: Literal["classif", "regression"]
        Either "classif" or "regression"
    model: torch.nn.Module
        Loaded PyTorch model to use for inference
    pred_output: PredictionOutput
        Dataclass containing all prediction output files' paths
    sensors_ind: dict
        dictionary containing useful information to reshape the input 'feat_stack' according
        to sensors

    Returns
    -------

    """
    predicted = None
    confidence = None
    proba = None
    for batch_num, (batch_data_to_infer, batch_mask_to_infer) in enumerate(
        zip_longest(batch_data_gen, batch_mask_gen)
    ):
        kwargs_tensors = otb_pipeline_to_named_tensors(
            batch_data_to_infer, batch_mask_to_infer, sensors_ind, inference_device
        )

        with torch.no_grad():
            y_hat = model(**kwargs_tensors)
        predicted, confidence, proba = infer_batch(
            batch_num,
            y_hat,
            mode,
            pred_output,
            labels_converter,
            predicted,
            confidence,
            proba,
        )
    assert predicted is not None
    return confidence, predicted, proba


def pytorch_predict(
    pred_parameters: PredictionParameters,
    torch_pred_paths: TorchPredictionPaths,
    pred_output: PredictionOutput,
    torch_parameters: PytorchPredictionParameters,
    custom_features_parameters: CustomFeaturesParameters = CustomFeaturesParameters(
        concat_mode=False
    ),
    all_class_labels: list[int] | None = None,
    number_of_chunks: int | None = None,
    targeted_chunk: int | None = None,
    mode: Literal["classif", "regression"] = "classif",
) -> None:
    """Perform pytorch prediction.

    This function will produce 2 rasters, the classification map and the
    confidence map. The confidence map represent the classifier confidence in
    the chosen class.

    Parameters
    ----------
    pred_parameters:
        Generic prediction parameters
    torch_pred_paths: TorchPredictionPaths
        Dataclass containing paths for the prediction using pytorch
    pred_output:
        Output files paths of prediction (classif, confidence and proba maps + generic output path)
    torch_parameters: PytorchPredictionParameters:
        Dataclass containing parameters for the pytorch prediction
    custom_features_parameters: CustomFeaturesParameters
        Dataclass containing parameters for the custom features
    all_class_labels: list[int] | None
        List of all classes' labels
    number_of_chunks: int | None
        Total number of chunks
    targeted_chunk: int | None
        Target chunk
    mode:
        string with possible values "classif", "regression"
        controls the output_number_of_bands parameter
        regression mode disables label conversion
    """
    if custom_features_parameters.functions is None:
        custom_features_parameters.functions = []

    if not all_class_labels and pred_output.proba:
        raise ValueError(
            "if probability map is asked, the parameter 'all_class_labels' become mandatory"
        )

    update_output_path(torch_pred_paths.working_dir, pred_output)

    if number_of_chunks and targeted_chunk:
        if targeted_chunk > number_of_chunks - 1:
            raise ValueError("targeted_chunk must be inferior to the number of chunks")
    pred_pipeline, geo_metadata = get_data_from_pipeline(
        pred_parameters=pred_parameters,
        pred_output=pred_output,
        working_dir=torch_pred_paths.working_dir,
        custom_features_parameters=custom_features_parameters,
        mask=torch_pred_paths.mask,
        targeted_chunk=targeted_chunk,
        number_of_chunks=number_of_chunks,
    )

    if targeted_chunk is not None:
        pred_output.classif = pred_output.classif.replace(
            ".tif", f"_SUBREGION_{targeted_chunk}.tif"
        )
        pred_output.confidence = pred_output.confidence.replace(
            ".tif", f"_SUBREGION_{targeted_chunk}.tif"
        )
        if pred_output.proba:
            pred_output.proba = pred_output.proba.replace(
                ".tif", f"_SUBREGION_{targeted_chunk}.tif"
            )

    # load encoder
    with open(torch_pred_paths.encoder_convert_file, "rb") as conv_file:
        labels_converter = pickle.load(conv_file)

    if mode == "classif":
        # in classif mode, this encoder is a dictionary
        # the model class are the values in this dictionary
        model_class = sorted(labels_converter.values())

    else:  # regression
        # in regression mode, this encoder is a standardscaler
        # model_class can be anything with length == 1
        model_class = ["regression_target"]

    # without copy, the torch probability vector may contain 'nan' values
    assert pred_pipeline.stack is not None
    pred_pipeline.stack = pred_pipeline.stack.copy()
    feat_labels_no_mask = [
        str(feat)
        for feat in pred_pipeline.feat_labels
        if "mask" not in str(feat).lower()
    ]

    sensors_ind = sensors_from_labels_to_index(
        feat_labels_no_mask, pred_pipeline.mask_labels
    )
    learning_batch_size = guess_max_inference_size(torch_pred_paths.hyperparam_file)
    if not torch_parameters.max_inference_size:
        torch_parameters.max_inference_size = learning_batch_size

    do_torch_prediction(
        torch_parameters=torch_parameters,
        pred_pipeline=pred_pipeline,
        geo_metadata=geo_metadata,
        pred_output=pred_output,
        sensors_ind=sensors_ind,
        labels_params=LabelsParameters(
            actual_labels_order=feat_labels_no_mask,
            labels_converter=labels_converter,
            nb_class=len(model_class),
        ),
        mode=mode,
    )

    if pred_output.proba:
        assert all_class_labels is not None
        reorder_proba_map(
            pred_output.proba, pred_output.proba, model_class, all_class_labels, "float"
        )

    copy_to_working_dir(torch_pred_paths.working_dir, pred_output, mode)


def generate_empty_inference(
    feat_stack: np.ndarray,
    mode: Literal["classif", "regression"],
    nb_class: int | None,
    pred_output: PredictionOutput,
    pred_pipeline: PredictionPipelineData,
) -> tuple[np.ndarray, np.ndarray | None, np.ndarray | None]:
    """
    Generate empty prediction arrays filled with a mask value.

    Parameters
    ----------
    feat_stack : np.ndarray
        The feature stack used for inference, with shape (height, width, channels).
    mode : Literal["classif", "regression"]
        Mode of inference: "classif" or "regression".
    nb_class : int | None
        Number of classes.
    pred_output : PredictionOutput
        Object containing paths of all outputs of predictions.
    pred_pipeline : PredictionPipelineData
        Prediction pipeline data.

    Returns
    -------
    Tuple[np.ndarray, np.ndarray | None, np.ndarray | None]
        A tuple containing the confidence map, predicted labels, and probability map as numpy
        arrays (if applicable), filled with arbitrary value.
    """
    confidence = None
    proba = None
    predicted = (
        np.ones((feat_stack.shape[0], feat_stack.shape[1])) * pred_pipeline.mask_value
    )
    if mode == "classif":
        confidence = (
            np.ones((feat_stack.shape[0], feat_stack.shape[1]))
            * pred_pipeline.mask_value
        )
        if pred_output.proba:
            proba = (
                np.ones((nb_class, feat_stack.shape[0], feat_stack.shape[1]))
                * pred_pipeline.mask_value
            )
            proba = proba.astype(np.float32)
    return predicted, confidence, proba


def reshape_maps(
    confidence: torch.Tensor,
    predicted_array: np.ndarray | torch.Tensor,
    proba: torch.Tensor | None,
    coeff_vect_proba: int,
    feat_stack: np.ndarray,
    mask_spatial: np.ndarray | None,
    pred_output: PredictionOutput,
) -> tuple[np.ndarray, np.ndarray, np.ndarray | None]:
    """
    Reshape prediction arrays to their original spatial dimensions and apply spatial masks.

    Parameters
    ----------
    confidence : np.ndarray
        The array containing confidence values.
    predicted_array : np.ndarray | torch.Tensor
        Array of predicted labels.
    proba : torch.Tensor | None
        Tensor of probabilities.
    coeff_vect_proba : int
        Coefficient to multiply with the probability vector.
    feat_stack : np.ndarray
        The feature stack used for inference.
    mask_spatial : np.ndarray | None
        An optional spatial mask to apply to the prediction outputs.
    pred_output : PredictionOutput
        Object containing paths of all outputs of predictions.

    Returns
    -------
    Tuple[np.ndarray, np.ndarray, np.ndarray | None]
        A tuple containing the reshaped confidence map, labels map, and probability map.
    """
    confidence_array = confidence.reshape(feat_stack.shape[0], feat_stack.shape[1])
    if pred_output.proba and proba is not None:
        proba = (
            proba.cpu().reshape(-1, feat_stack.shape[0], feat_stack.shape[1]).numpy()
            * coeff_vect_proba
        )
    if isinstance(predicted_array, torch.Tensor):
        labels_map = np.expand_dims(predicted_array.cpu(), axis=0)
        confidence_map = np.expand_dims(confidence_array.cpu(), axis=0)
    else:
        labels_map = np.expand_dims(predicted_array, axis=0)
        confidence_map = np.expand_dims(confidence_array, axis=0)
    if mask_spatial is not None:
        labels_map = labels_map * mask_spatial
        confidence_map = confidence_map * mask_spatial
        if pred_output.proba:
            proba = proba * mask_spatial
    labels_map = labels_map.astype(np.uint8)
    confidence_map = confidence_map.astype(np.float32)
    return confidence_map, labels_map, proba


def load_model(
    inference_device: torch.device, torch_parameters: PytorchPredictionParameters
) -> tuple[list[str], torch.nn.Module]:
    """
    Loads a model and its parameters, and returns the detected labels order and the model.

    Parameters
    ----------
    inference_device : torch.device
        The device to which the model should be moved (CPU or GPU).
    torch_parameters : PytorchPredictionParameters
        Pytorch parameters.

    Returns
    -------
    tuple[labels_order, torch.nn.Module]
        Tuple containing the labels order and the loaded model.
    """
    with open(
        torch_parameters.model_parameters_file, "rb"
    ) as file_instance_nn_parameters:
        instance_param_dict = pickle.load(file_instance_nn_parameters)
    # instantiate model
    model = Iota2NeuralNetworkFactory().get_nn(
        torch_parameters.nn_name,
        instance_param_dict,
        external_nn_module=torch_parameters.external_nn_module,
    )
    # load model
    with open(torch_parameters.model_file, "rb") as model_file_ctx:
        state_dict, stats, labels_order = pickle.load(model_file_ctx)
    model.load_state_dict(state_dict)
    model.eval()
    model.to(inference_device)
    stats = StatsCalculator.stats_to_device(stats, inference_device)
    model.set_stats(stats)

    return labels_order, model


def infer_batch(
    batch_num: int,
    y_hat: torch.Tensor,
    mode: str,
    pred_output: PredictionOutput,
    labels_converter: dict | StandardScaler,
    predicted: torch.Tensor | None = None,
    confidence: torch.Tensor | None = None,
    proba: torch.Tensor | None = None,
) -> tuple[torch.Tensor, torch.Tensor | None, torch.Tensor | None]:
    """
    Performs inference on a batch.

    Parameters
    ----------
    batch_num : int
        Current batch number.
    y_hat : torch.Tensor
        The raw predictions output by the model for the current batch.
    mode : str
        The type of task, either "classif" or "regression".
    pred_output : PredictionOutput
        Dataclass containing prediction output files' paths.
    labels_converter : dict | StandardScaler
        For regression, a scaler (e.g., StandardScaler) used to inverse transform the predictions.
    predicted : torch.Tensor | None
        The accumulated predictions from previous batches.
    confidence : torch.Tensor | None
        The accumulated confidence scores from previous batches.
    proba : torch.Tensor | None
        The accumulated probabilities for all classes across batches (only in 'classif' mode).

    Returns
    -------
    predicted : torch.Tensor
        The concatenated predictions including the current batch.
    confidence : torch.Tensor | None
        The concatenated confidence scores including the current batch.
    proba : torch.Tensor | None
        The concatenated probability arrays including the current batch.
    """
    if mode == "classif":
        y_hat = F.softmax(y_hat, dim=1)
        confidence_batch, predicted_batch = y_hat.topk(1, dim=1)

        if pred_output.proba:
            nb_class = len(y_hat[0])
            all_confidences, all_predictions = y_hat.topk(nb_class, dim=1)
            _, new_idx = torch.sort(all_predictions, dim=1)
            all_confidences_bands = torch.gather(all_confidences, dim=1, index=new_idx)
            all_confidences_array_batch = all_confidences_bands.reshape(-1, nb_class).T

        if batch_num == 0:
            predicted = predicted_batch
            confidence = confidence_batch
            if pred_output.proba:
                proba = all_confidences_array_batch
        else:
            predicted = torch.cat([predicted, predicted_batch], axis=0)
            confidence = torch.cat([confidence, confidence_batch], axis=0)
            if pred_output.proba:
                proba = torch.cat([proba, all_confidences_array_batch], axis=1)

    elif mode == "regression":
        scaler: StandardScaler = labels_converter
        rescaled = scaler.inverse_transform(y_hat.cpu())
        predicted_batch = torch.from_numpy(rescaled)

        if batch_num == 0:
            predicted = predicted_batch
        else:
            predicted = torch.cat([predicted, predicted_batch], axis=0)

    return predicted, confidence, proba


def otb_pipeline_to_named_tensors(
    input_features: np.ndarray,
    input_masks: np.ndarray,
    sensors_position: dict[str, dict],
    inference_device: torch.device,
) -> dict:
    """TODO : factorize with torch_utils.tensors_to_kwargs"""
    out_tensors = {}
    for sensors_name, sensor_info in sensors_position.items():
        s_name = sensors_name.lower()
        if sensor_info["nb_date_comp"]:
            out_tensors[s_name] = torch.from_numpy(
                np.take(input_features, sensor_info["index"], axis=1).reshape(
                    -1,
                    int(len(sensor_info["index"]) / sensor_info["nb_date_comp"]),
                    int(sensor_info["nb_date_comp"]),
                )
            ).to(inference_device)
        else:
            out_tensors[s_name] = torch.from_numpy(
                np.take(input_features, sensor_info["index"], axis=1).reshape(
                    -1, len(sensor_info["index"])
                )
            ).to(inference_device)
        if "mask_index" in sensor_info and sensor_info["mask_index"]:
            out_tensors[f"{s_name}_masks"] = torch.from_numpy(
                np.take(input_masks, sensor_info["mask_index"], axis=1).reshape(
                    -1, len(sensor_info["mask_index"]), 1
                )
            ).to(inference_device)
    return out_tensors


def manage_zeros(
    predicted_array: np.ndarray,
    path_classif: str,
    labels_converter: dict | StandardScaler,
    labels_map: np.ndarray | None,
    geo_metadata: RasterioSpatialMetadata,
    mask_spatial: np.ndarray | None = None,
    mode: Literal["classif", "regression"] = "classif",
) -> None:
    """
    Handles the '0' label in PyTorch outputs, which represents masked values in spatial masks.

    Parameters
    ----------
    predicted_array : np.ndarray
        The array containing predicted labels or values.
    path_classif : str
        The file path where the output classification image will be saved.
    labels_converter : dict | StandardScaler
        Converter for mapping predicted labels to their respective values or classes.
    labels_map : np.ndarray
        Array representing the labels map.
    mask_spatial : np.ndarray | None
        Spatial mask used to mask out irrelevant areas in the prediction.
    geo_metadata : RasterioSpatialMetadata
        Geospatial metadata required for saving the output.
    mode : Literal["classif", "regression"]
        The mode of operation, either "classif" or "regression", defaults to "classif".
    """
    if mode == "classif" and labels_converter:
        # we multiply again the output by the mask raster
        tmp_mask = path_classif.replace(".tif", "_MASK.tif")
        if mask_spatial is not None and labels_map is not None:
            mask_spatial = np.expand_dims(mask_spatial, axis=0)
            with rasterio.open(
                tmp_mask,
                "w",
                driver="GTiff",
                height=labels_map.shape[1],
                width=labels_map.shape[2],
                count=labels_map.shape[0],
                crs=f"EPSG:{geo_metadata.epsg}",
                transform=geo_metadata.transform,
                dtype=np.float32,
            ) as dest:
                dest.write(mask_spatial)

        sub_exp = []
        for torch_label, i2_label in labels_converter.items():
            sub_exp.append(f"im1b1 == {torch_label} ? {i2_label}")
        exp = f"{':'.join(sub_exp)} : 0"
        img_list = [path_classif]
        if mask_spatial is not None:
            exp = f"im2b1 * ({exp})"
            img_list.append(tmp_mask)
        bm_converter, _ = create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": img_list,
                "out": path_classif,
                "exp": exp,
                "pixType": "uint8",
            },
        )
        bm_converter.ExecuteAndWriteOutput()
        if Path(tmp_mask).exists():
            Path(tmp_mask).unlink()

    elif mode == "regression":
        if isinstance(predicted_array, torch.Tensor):
            targets_map = np.expand_dims(
                predicted_array.cpu().numpy() * mask_spatial, axis=0
            )
        else:
            targets_map = np.expand_dims(predicted_array * mask_spatial, axis=0)

        # TODO masque de région, cf plus haut
        # here the region mask has to be applied
        with rasterio.open(
            path_classif,
            "w",
            driver="GTiff",
            height=targets_map.shape[1],
            width=targets_map.shape[2],
            count=targets_map.shape[0],
            crs=f"EPSG:{geo_metadata.epsg}",
            transform=geo_metadata.transform,
            dtype=np.float32,
        ) as dest:
            dest.write(targets_map)


def guess_max_inference_size(hyperparam_file: str) -> int:
    """Read hyperparam batch size."""
    with open(hyperparam_file, "rb") as handle:
        hyperparams: dict[str, int] = pickle.load(handle)
    return hyperparams["batch_size"]
