#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
This module handle indecision after post classification fusion
"""
import logging
import shutil
from pathlib import Path

import iota2.common.otb_app_bank as otb
from config import Config
from iota2.common import file_utils as fu
from iota2.typings.i2_types import DBInfo
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def config_model_use(output_path: str, region_field: str) -> str:
    """
    usage : determine which model will class which tile
    """
    # const
    formatting_vec_dir = str(Path(output_path) / "formattingVectors")
    samples = fu.file_search_and(formatting_vec_dir, True, ".shp")

    # init
    all_regions = []
    for sample in samples:
        tile_name = Path(sample).stem.split("_")[0]
        regions = vf.get_field_element(
            sample, elem_type=str, field=region_field, mode="unique"
        )
        assert regions is not None
        for region in regions:
            all_regions.append((region, tile_name))

    # {'model_name':[TileName, TileName...],'...':...,...}
    model_tiles = dict(fu.sort_by_first_elem(all_regions))

    # add tiles if they are missing by checking in /shapeRegion/ directory
    shape_region_dir = str(Path(output_path) / "shapeRegion")
    shape_region_path = fu.file_search_and(shape_region_dir, True, ".shp")

    for shape_region in shape_region_path:
        # check if there is actually polygons
        list_polygons = vf.get_field_element(
            shape_region, elem_type=str, field=region_field, mode="all"
        )
        if list_polygons and len(list_polygons) >= 1:
            tile = Path(shape_region).stem.split("_")[-1]
            region = Path(shape_region).stem.split("_")[-2]
            for model_name, tiles_model in list(model_tiles.items()):
                if model_name.split("f")[0] == region and tile not in tiles_model:
                    tiles_model.append(tile)

    # Construct output file string
    output = "AllModel:\n["
    for model_name, tiles_model in list(model_tiles.items()):
        output_tmp = (
            f"\n\tmodelName:'{model_name}'\n\ttilesList:'{'_'.join(tiles_model)}'"
        )
        output = output + "\n\t{" + output_tmp + "\n\t}"
    output += "\n]"

    return output


def get_model_in_classif(item: str) -> str:
    """
    Parameters
    ----------
    item: string to be splitted
    Return
    ------
    string: model name
    """
    return item.split("_")[-3]


def get_model_in_mask(item: str) -> str:
    """
    Parameters
    ----------
    item: string to be splitted
    Return
    ------
    string: model name
    """
    return item.split("_")[-2]


def gen_mask_region_by_tile(
    field_region: str,
    stack_ind: str,
    working_dir: str,
    current_tile: str,
    all_model: str,
    shp_rname: str,
    path_to_image: str,
    path_test: str,
    path_to_config: str,
    path_wd: str | None = None,
) -> list[str]:
    """
    Parameters
    ----------
    field_region: string
    stack_ind: string
    working_dir: string
    current_tile: string
    all_model: string
    shp_rname: string
    path_to_image: string
    path_test: string
    path_wd: string
    path_to_config: string
        config file containing which model is associated to a given tile
    Return
    ------
    list(str)
    """
    model_tile = []
    for path in all_model:
        current_model = path.split("/")[-1].split("_")[1]
        tiles = fu.get_list_tile_from_model(current_model, path_to_config)
        assert tiles is not None
        model = path.split("/")[-1].split("_")[1]
        for tile in tiles:
            # get the model which learn the current tile
            if tile == current_tile:
                model_tile.append(model)
            mask_shp = str(
                Path(path_test)
                / "shapeRegion"
                / f"{shp_rname}_region_{model}_{tile}.shp"
            )
            mask_tif = str(
                Path(working_dir) / f"{shp_rname}_region_{model}_{tile}_NODATA.tif"
            )
            mask_tif_f = str(
                Path(path_test)
                / "classif"
                / "MASK"
                / f"{shp_rname}_region_{model}_{tile}_NODATA.tif",
            )
            # Create mask
            if not Path(mask_tif_f).exists():

                rasterize_app, _ = otb.create_application(
                    otb.AvailableOTBApp.RASTERIZATION,
                    {
                        "in": mask_shp,
                        "mode.attribute.field": field_region,
                        "im": str(Path(path_to_image) / tile / "Final" / stack_ind),
                        "out": mask_tif,
                    },
                )
                rasterize_app.ExecuteAndWriteOutput()

                if path_wd is not None:
                    shutil.copy(mask_tif, Path(path_test) / "classif" / "MASK")
    return model_tile


def concat_classifs_one_tile(
    seed: str,
    current_tile: str,
    path_test: str,
    model_tile: list[str],
    concat_out: str,
    path_wd: str | None = None,
) -> str:
    """
    Parameters
    ----------
    path_wd: string
    seed: string
    current_tile: string
    path_test: string
    model_tile: string
    concat_out: string
    """

    assert path_test is not None

    classif_fusion = []

    for model in model_tile:
        classif_fusion.append(
            str(
                Path(path_test)
                / "classif"
                / f"Classif_{current_tile}_model_{model}_seed_{seed}.tif",
            )
        )

    if len(classif_fusion) == 1:
        shutil.copy(classif_fusion[0], concat_out)
    else:
        # in order to match images and their mask
        classif_fusion_sort = sorted(classif_fusion, key=get_model_in_classif)
        path_to_directory = str(Path(path_test) / "classif")
        if path_wd is not None:
            path_to_directory = path_wd
        concat_app, _ = otb.create_application(
            otb.AvailableOTBApp.CONCATENATE_IMAGES,
            {
                "il": classif_fusion_sort,
                "out": f"{str(Path(path_to_directory) / concat_out.split('/')[-1])}",
                "ram": 128,
            },
        )
        concat_app.ExecuteAndWriteOutput()
        if not Path(concat_out).exists() and path_wd is not None:
            shutil.copy(
                f"{str(Path(path_wd) / concat_out.split('/')[-1])}",
                f"{str(Path(path_test ) / 'classif')}",
            )

    return concat_out


def concat_region_one_tile(
    path_test: str,
    classif_fusion_mask: str,
    path_wd: str | None,
    tile_mask_concat: str,
) -> str:
    """
    Parameters
    ----------
    path_test: string
    classif_fusion_mask: string
    path_wd: string
    tile_mask_concat: string
    Return
    ------
    string
    """
    if len(classif_fusion_mask) == 1 and not Path(tile_mask_concat).exists():
        shutil.copy(classif_fusion_mask[0], tile_mask_concat)
    elif len(classif_fusion_mask) != 1 and not Path(tile_mask_concat).exists():
        # in order to match images and their mask
        classif_fusion_mask_sort = sorted(classif_fusion_mask, key=get_model_in_mask)

        path_directory = str(Path(path_test) / "classif")
        if path_wd is not None:
            path_directory = path_wd
        concat_app, _ = otb.create_application(
            otb.AvailableOTBApp.CONCATENATE_IMAGES,
            {
                "il": classif_fusion_mask_sort,
                "out": f"{str(Path(path_directory) / tile_mask_concat.split('/')[-1])}",
                "ram": 128,
            },
        )
        concat_app.ExecuteAndWriteOutput()

        if path_wd is not None:
            shutil.copy(
                f"{str(Path(path_wd) / tile_mask_concat.split('/')[-1])}",
                f"{str(Path(path_test ) / 'classif')}",
            )

    return tile_mask_concat


def build_confidence_exp(img_confidence: str, img_classif: str) -> str:
    """
    Create the bandmath expression for confidence computation.
    Return the expression and the list of images to use

    Parameters
    ----------
    img_confidence:
        Paths to confidence map
    img_classif:
        Paths to images of classifications

    Warnings
    --------
    The list of images of classifications and confidence maps must have follow the same order.
        Example :
            classif = ["cl1","cl2","cl3","cl4"]
            confidences = ["c1","c2","c3","c4"]
        Where 'c1' is the confidence map of the classification 'cl1' etc...
    """

    if len(img_confidence) != len(img_classif):
        raise ValueError(
            "Error, the list of classification and the list of confidence map "
            "must have the same length"
        )
    im_conf = []
    im_class = []
    im_ref = "im" + str(2 * len(img_confidence) + 1) + "b1"

    for n_image in range(len(img_confidence)):
        im_conf.append(f"im{n_image + 1}b1")
    for n_image in range(len(img_confidence), 2 * len(img_confidence)):
        im_class.append(f"im{n_image + 1}b1")
    # (c1>c2 and c1>c3 and c1>c4)?cl1:(c2>c1 and c2>c3 and c2>c4)?cl2:etc...
    # (c1>c2)?cl1:(c2>c1)?:cl2:0
    exp = im_ref + "!=0?" + im_ref + ":"
    for conf_image1 in im_conf:
        tmp = []
        for conf_image2 in im_conf:
            if conf_image1 != conf_image2:
                tmp.append(conf_image1 + ">" + conf_image2)
        exp_tmp = " and ".join(tmp)
        exp += "(" + exp_tmp + ")?" + conf_image1 + ":"

    exp += im_class[0]

    return exp


def get_nb_split_shape(model: str, config_model_path: str) -> int:
    """
    Parameters
    ----------
    model: string
    config_model_path: string
                     configuration file
    Return
    ------
    """
    cfg = Config(config_model_path).as_dict()
    fold = []

    for model_tile in cfg["AllModel"]:
        model_name = model_tile["modelName"]
        if model_name.split("f")[0] == model and len(model_name.split("f")) > 1:
            fold.append(int(model_name.split("f")[-1]))
    return max(fold)


def indecision_management(
    path_test: str,
    path_fusion: str,
    region_db: DBInfo,
    path_to_img: str,
    no_label_management: str,
    path_wd: str | None,
    list_indices: list[str],
    pix_type: str,
    user_feat_pattern: str | None = None,
) -> None:
    """
    Manage indecision coming from fusion of classifications

    Parameters
    ----------
    path_test: string
        Iota2 output dir.
    path_fusion: string
        Path to the fusion raster.
    region_db: DBInfo
        Dataclass containing information about the region database.
    path_to_img: string
        Path to the /features directory.
    no_label_management: string
        Method used to manage indecision.
    path_wd: string
        Path to working directory.
    list_indices: List[string]
        List of spectral indices to use.
    pix_type: string
        Pixel type (as expected by otb).
    user_feat_pattern: string
        Key names used to detect input images.
    """
    n_fold, path_directory = prepare_indecision_management(
        path_fusion, path_test, region_db
    )
    if path_wd is not None:
        working_dir = path_wd
        path_directory = path_wd
    else:
        working_dir = path_test + "/classif/MASK"

    current_tile = path_fusion.split("/")[-1].split("_")[0]

    shp_rname = str(region_db.db_file).rsplit("/", maxsplit=1)[-1].replace(".shp", "")

    all_model = fu.file_search_and(
        str(Path(path_test) / "model"), True, "model", ".txt"
    )

    if region_db.db_file is None:
        model_tile = gen_mask_region_by_tile(
            region_db.region_field,
            fu.get_feat_stack_name(list_indices, user_feat_pattern),
            working_dir,
            current_tile,
            all_model,
            shp_rname,
            path_to_img,
            path_test,
            str(Path(path_test) / "config_model" / "configModel.cfg"),
            path_wd,
        )
    elif region_db.db_file and no_label_management == "maxConfidence":
        model_tile = [path_fusion.split("/")[-1].split("_")[3]]
    elif region_db.db_file and no_label_management == "learningPriority":
        model_tile_tmp = path_fusion.split("/")[-1].split("_")[3]
        model_tile = [
            f"{model_tile_tmp}f{n_classif_mask + 1}" for n_classif_mask in range(n_fold)
        ]
    else:
        raise ValueError("Incorrect indecision configuration")

    if len(model_tile) == 0 or no_label_management == "maxConfidence":
        handle_max_confidence(
            path_fusion,
            path_directory,
            region_db,
            current_tile,
            path_test,
            pix_type,
            model_tile,
            path_wd,
        )

    elif len(model_tile) != 0 and no_label_management == "learningPriority":
        # Concaténation des classifications pour une tuile (qui a ou non
        # plusieurs régions) et Concaténation des masques de régions pour
        # une tuile (qui a ou non plusieurs régions)
        handle_learning_priority(
            path_fusion,
            path_directory,
            region_db,
            current_tile,
            path_test,
            model_tile,
            n_fold,
            pix_type,
            path_wd,
        )


def prepare_indecision_management(
    path_fusion: str,
    path_test: str,
    region_db: DBInfo,
) -> tuple[int, str]:
    """
    Prepare necessary configuration and directory setup for indecision management.

    Parameters
    ----------
    path_fusion : str
        Path to the fusion raster.
    path_test : str
        Iota2 output dir.
    region_db : DBInfo
        Dataclass containing information about the region database.

    Returns
    -------
    tuple[int, str]
        A tuple containing:
        - n_fold: int
            Number of folds or splits in the classification.
        - path_directory: str
            Path to the classification directory where results will be stored.
    """
    path_to_model_config = str(Path(path_test) / "config_model" / "configModel.cfg")
    assert region_db.region_field is not None
    config_model_rep = config_model_use(path_test, region_db.region_field)
    if not Path(path_to_model_config).exists():
        with open(path_to_model_config, "w", encoding="utf-8") as config_file:
            config_file.write(config_model_rep)
    current_model = path_fusion.split("/")[-1].split("_")[3]
    config_model = str(Path(path_test) / "config_model" / "configModel.cfg")
    n_fold = get_nb_split_shape(current_model, config_model)
    path_directory = path_test + "/classif"
    return n_fold, path_directory


def handle_learning_priority(
    path_fusion: str,
    path_directory: str,
    region_db: DBInfo,
    current_tile: str,
    path_test: str,
    model_tile: list[str],
    n_fold: int,
    pix_type: str,
    path_wd: str | None,
) -> None:
    """
    Handle the indecision management for the case where 'learningPriority'
    strategy is used, involving multiple classification models for the same tile.

    Parameters
    ----------
    path_fusion : str
        Path to the fusion raster.
    path_directory : str
        Directory where the classification results are stored.
    region_db : DBInfo
        Dataclass containing information about the region database.
    current_tile : str
        Name of the current tile being processed.
    path_test : str
        Iota2 output dir.
    model_tile : list[str]
        List of model identifiers used for fusion.
    n_fold : int
        Number of folds or splits in the classification.
    pix_type : str
        Pixel type (as expected by otb).
    path_wd : str | None
        Path to the working directory.

    Returns
    -------
    None
        The function writes the output directly to the file system.
    """

    seed = str(Path(path_fusion).name.split("_")[-1].split(".")[0])
    concat_out = str(
        Path(path_test) / "classif" / f"{current_tile}_FUSION_concat_seed{seed}.tif"
    )
    if region_db.db_file:
        concat_out = str(
            Path(path_test)
            / "classif"
            / f"{current_tile}_FUSION_model_{model_tile[0].split('f')[0]}concat_seed{seed}.tif"
        )
    path_to_classif_concat = concat_classifs_one_tile(
        seed, current_tile, path_test, model_tile, concat_out, path_wd
    )
    pattern_mask = f"*region_*_{current_tile}_NODATA.tif"
    classif_fusion_mask = fu.file_search_reg_ex(
        str(Path(path_test) / "classif" / "MASK" / pattern_mask)
    )
    out_concat_mask = str(Path(path_test) / "classif" / f"{current_tile}_MASK.tif")
    if region_db.db_file:
        pattern_mask = f"*region_{model_tile[0].split('f')[0]}_{current_tile}.tif"
        classif_fusion_mask_tmp = fu.file_search_reg_ex(
            str(Path(path_test) / "classif" / "MASK" / pattern_mask)
        )
        out_concat_mask = str(
            Path(path_test)
            / "classif"
            / f"{current_tile}_MASK_model_{model_tile[0].split('f')[0]}.tif"
        )

        classif_fusion_mask = [classif_fusion_mask_tmp[0]] * n_fold
    path_to_region_mask_concat = concat_region_one_tile(
        path_test, classif_fusion_mask, path_wd, out_concat_mask
    )
    # construction de la commande

    img_data = str(
        Path(path_directory) / f"{current_tile}_FUSION_NODATA_seed{seed}.tif"
    )
    if region_db.db_file:
        img_data = str(
            Path(path_directory) / f"Classif_{current_tile}_model_"
            f"{model_tile[0].split('f')[0]}_seed_{seed}.tif"
        )

    compute_learning_prio_indecision(
        [path_fusion, path_to_classif_concat, path_to_region_mask_concat],
        img_data,
        classif_fusion_mask,
        pix_type,
    )
    if path_wd is not None:
        shutil.copy(img_data, Path(path_test) / "classif")


def compute_learning_prio_indecision(
    input_images: list[str],
    output_raster: str,
    classif_fusion_mask: list[str],
    pix_type: str,
) -> None:
    """
    Build the bandmath expression for the band math app and execute the app when using the learning
    priority method.

    Parameters
    ----------
    input_images : list[str]
        List of input image file paths (fusion, classification, and mask images).
    output_raster : str
        Path to the output image file.
    classif_fusion_mask : list[str]
        List of classification mask file paths used to determine the region
        masks for the fusion process.
    pix_type : str
        Pixel type (as expected by otb).

    Returns
    -------
    None
    """
    exp = ""
    for n_classif_mask in range(len(classif_fusion_mask)):
        if n_classif_mask + 1 < len(classif_fusion_mask):
            exp = (
                exp
                + "im2b"
                + str(n_classif_mask + 1)
                + ">=1?im3b"
                + str(n_classif_mask + 1)
                + ":"
            )
        else:
            exp = (
                exp
                + "im2b"
                + str(n_classif_mask + 1)
                + ">=1?im3b"
                + str(n_classif_mask + 1)
                + ":0"
            )
    exp = "im1b1!=0?im1b1:(" + exp + ")"
    bandmath, _ = otb.create_application(
        otb.AvailableOTBApp.BAND_MATH,
        {
            "il": input_images,
            "out": output_raster,
            "exp": exp,
            "pixType": pix_type,
        },
    )
    bandmath.ExecuteAndWriteOutput()


def handle_max_confidence(
    path_fusion: str,
    path_directory: str,
    region_db: DBInfo,
    current_tile: str,
    path_test: str,
    pix_type: str,
    model_tile: list[str],
    path_wd: str | None,
) -> None:
    """
    Handle the indecision management for the case where 'maxConfidence'
    strategy is used or the list of region per tile is empty.

    Parameters
    ----------
    path_fusion : str
        Path to the fusion raster.
    path_directory : str
        Directory where the classification results are stored.
    region_db : DBInfo
        Dataclass containing information about the region database.
    current_tile : str
        Name of the current tile being processed.
    path_test : str
        Iota2 output dir.
    pix_type : str
        Pixel type (as expected by otb).
    model_tile : list[str]
        List of model identifiers used for fusion.
    path_wd : str | None
        Path to working directory.

    Returns
    -------
    None
        The function writes the output directly to the file system.
    """
    unique_model_tile = model_tile[0]
    seed = str(Path(path_fusion).name.split("_")[-1].split(".")[0])
    img_confidence = fu.file_search_and(
        path_test + "/classif",
        True,
        "confidence_seed_" + str(seed) + ".tif",
        current_tile,
    )
    img_classif = fu.file_search_and(
        str(Path(path_test) / "classif"),
        True,
        f"Classif_{current_tile}",
        f"seed_{seed}",
    )
    img_data = str(
        Path(path_directory) / f"{current_tile}_FUSION_NODATA_seed{seed}.tif"
    )
    if region_db.db_file:
        img_confidence = fu.file_search_reg_ex(
            str(
                Path(path_test) / f"classif/{current_tile}_model_{unique_model_tile}"
                f"f*_confidence_seed_{seed}.tif"
            )
        )
        img_classif = fu.file_search_reg_ex(
            str(
                Path(path_test)
                / f"classif/Classif_{current_tile}_model_{unique_model_tile}"
                f"f*_seed_{seed}.tif"
            )
        )
        img_data = str(
            Path(path_directory) / f"Classif_{current_tile}_model_{unique_model_tile}"
            f"_seed_{seed}.tif"
        )
    img_confidence.sort()
    img_classif.sort()
    exp = build_confidence_exp(img_confidence, img_classif)
    il_str = [path_fusion] + img_confidence + img_classif
    bandmath, _ = otb.create_application(
        otb.AvailableOTBApp.BAND_MATH,
        {"il": il_str, "out": img_data, "exp": exp, "pixType": pix_type},
    )
    bandmath.ExecuteAndWriteOutput()
    if path_wd is not None:
        shutil.copy(img_data, str(Path(path_test) / "classif"))
