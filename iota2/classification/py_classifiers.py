#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module interfacing scikit-learn and pytorch for iota2."""
import logging
import operator
import pickle
import shutil
from collections.abc import Callable
from dataclasses import asdict, dataclass
from functools import partial
from logging import Logger
from pathlib import Path
from typing import Any, Literal

import numpy as np
import rasterio
import torch
from rasterio.plot import reshape_as_image
from sklearn.preprocessing import binarize

from iota2.common import raster_utils as ru
from iota2.common.custom_numpy_features import (
    ChunkParameters,
    CustomFeaturesConfiguration,
    CustomNumpyFeatures,
    DataContainer,
    MaskingRaster,
    compute_custom_features,
    exogenous_data_tile,
)
from iota2.common.file_utils import get_iota2_project_dir
from iota2.common.generate_features import FeaturesMapParameters, generate_features
from iota2.common.raster_utils import (
    ChunkConfig,
    OtbPipelineCarrier,
    UserDefinedFunction,
    UserDefinedFunctionPayload,
    gdal_to_rio_metadata,
    gdal_transform_to_rio,
    pipelines_chunking,
    turn_off_otb_pipelines,
)
from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetwork
from iota2.learning.pytorch.torch_utils import SensorStats
from iota2.learning.utils import I2Label
from iota2.typings.i2_types import (
    CustomFeaturesParameters,
    EmptyList,
    PipelinesLabels,
    PredictionOutput,
    PredictionPipelineData,
    RasterioSpatialMetadata,
)

# pylint: disable=E1101

SensorsParams = dict[str, str | list[str] | int]
FunctionNameWithParams = tuple[str, dict[str, Any]]

LOGGER = logging.getLogger("distributed.worker")
MAX_SCORE = 100000  # high value to compare with metrics to minimize
MIN_SCORE = -100000  # low value to compare with metrics to maximize


@dataclass
class PredictionParameters:
    """
    Dataclass containing parameters for predictions

    Attributes
    ----------
    tile_name:
        Name of the tile
    enabled_gap:
        Flag to execute the gapfilled pipeline
    enabled_raw:
        Flag to execute the raw and cloud pipelines
    sensors_parameters:
        Parameters of sensor(s)
    sensors_dates:
        Dates for all enabled sensors
    exogeneous_data:
        Path to a Geotiff file containing additional data to be used in external features
    features_from_raw_dates:
        flag to get classify pixels from raw data
    """

    tile_name: str
    enabled_gap: bool
    enabled_raw: bool
    sensors_parameters: SensorsParams
    sensors_dates: dict | None = None
    exogeneous_data: str | None = None
    features_from_raw_dates: bool = False


def reorder_features(
    expected_labels_order: list[str], current_labels_order: list[str]
) -> list[int] | None:
    """Reorder features regarding current labels order and the expected one.

    Note
    ----
    if both orders are the same, return None
    """
    if len(expected_labels_order) != len(current_labels_order):
        raise ValueError(
            "input vector list must have the same length "
            f"{len(expected_labels_order)} and {len(current_labels_order)}"
        )
    expected_lab_order = [lab.lower() for lab in expected_labels_order]
    lab_order = [lab.lower() for lab in current_labels_order]
    new_order = []
    same_order = []
    for expect_ind, lab in enumerate(expected_lab_order):
        try:
            index = lab_order.index(lab)
            new_order.append(index)
            same_order.append(index == expect_ind)
        except ValueError as err:
            raise ValueError(f"iota2 error : can't find the label '{lab}'") from err
    return None if all(same_order) else new_order


def get_is_better(selection_criterion: str) -> tuple[int, Callable]:
    """Get initial value and is_better function for given criterion."""
    # if criterion is loss, then we want to minimize the value else maximize
    if selection_criterion in ["loss", "MAE", "MSE", "HuberLoss"]:
        best_criterion_value = MAX_SCORE
        is_better = operator.lt  # x < y
    elif selection_criterion in ["fscore", "oa", "kappa"]:
        best_criterion_value = MIN_SCORE
        is_better = operator.gt  # x > y
    else:
        raise ValueError(f"selection criterion {selection_criterion} not managed")
    return best_criterion_value, is_better


def choose_best_model(
    input_pytorch_models: list[str],
    selection_criterion: str,
    output_model_file: str,
    model_parameters_file: str,
    encoder_convert_file: str,
    hyperparameters_file: str,
    logger: logging.Logger = LOGGER,
    mode: Literal["classif", "regression"] = "classif",
) -> None:
    """
    Find the best model according criterion.

    mode:
        "classif" or "regression"
        in regression mode, the available metrics are not the same
    """
    best_state_dict, labels, stats = get_best_state(
        input_pytorch_models, selection_criterion, mode
    )

    if Path(output_model_file).exists():
        Path(output_model_file).unlink()
    logger.info(
        f"saving : {output_model_file} with the epoch selection"
        f" criterion '{selection_criterion}' with the score "
        f"{best_state_dict['score']} that was obtained at epoch "
        f"{best_state_dict['epoch']} with learning rate = "
        f"{best_state_dict['hyperparameters']['learning_rate']} "
        f"and batch's size = {best_state_dict['hyperparameters']['batch_size']}"
    )
    with open(hyperparameters_file, "wb") as hyperparam:
        pickle.dump(
            best_state_dict["hyperparameters"],
            hyperparam,
            protocol=pickle.HIGHEST_PROTOCOL,
        )
    torch.save((best_state_dict["state_dict"], stats), output_model_file)
    Iota2NeuralNetwork.save(
        output_model_file, best_state_dict["state_dict"], stats, labels
    )
    shutil.copy(best_state_dict["parameter_file"], model_parameters_file)
    shutil.copy(best_state_dict["enc"], encoder_convert_file)


def get_best_state(
    input_pytorch_models: list[str],
    selection_criterion: str,
    mode: Literal["classif", "regression"] = "classif",
) -> tuple[dict, list[str], dict[str, SensorStats],]:
    """
    Retrieve the state dictionary of the best pytorch model based on a specified selection
    criterion.

    Parameters
    ----------
    input_pytorch_models : list[str]
        A list of file paths to serialized PyTorch models.
    mode : str
        Either 'classif' or 'regression'.
    selection_criterion : str
        The criterion used to select the best model

    Returns
    -------
    best_state_dict : dict
        The state dictionary of the best model according to the selection criterion.
    labels : list[str]
        The labels associated with the best model.
    stats : dict[str, SensorStats]
        The statistics or additional information associated with the best model.

    Raises
    ------
    RuntimeError
        If none of the models have a score better than the initial best criterion value.
    """
    best_state_dict = None
    best_criterion_value, is_better = get_is_better(selection_criterion)
    for input_pytorch_model in input_pytorch_models:
        with open(input_pytorch_model, "rb") as input_pytorch_model_file:
            pytorch_model_dict, stats, labels = pickle.load(input_pytorch_model_file)
            if mode == "classif":
                if is_better(
                    pytorch_model_dict[selection_criterion]["score"],
                    best_criterion_value,
                ):
                    best_state_dict = pytorch_model_dict[selection_criterion]
                    best_criterion_value = pytorch_model_dict[selection_criterion][
                        "score"
                    ]
            elif mode == "regression":
                # regression way of managing state dict is different
                state_dict = pytorch_model_dict[selection_criterion.lower()]
                if is_better(state_dict.score, best_criterion_value):
                    best_state_dict = asdict(state_dict)
                    best_criterion_value = state_dict.score
    if best_state_dict is None:
        raise RuntimeError(
            f"none of the {len(input_pytorch_models)} models "
            f"got score better than {best_criterion_value}"
        )
    return best_state_dict, labels, stats


def get_class(input_array: np.ndarray, labels_dict: list[int]) -> int:
    """Return the class."""
    return labels_dict[np.argmax(input_array)]


def proba_to_label(
    proba_map: np.ndarray,
    out_classif: str,
    labels: list[int],
    geo_parameters: RasterioSpatialMetadata,
    mask_arr: np.ndarray | None = None,
) -> np.ndarray:
    """Return the class' label from the prediction probabilities.

    Parameters
    ----------
    proba_map:
        array of predictions probabilities (one probability for each class)
    out_classif:
        output classification
    labels:
        list of class labels
    geo_parameters:
        Dataclass containing geo parameters (epsg code + transform)
    mask_arr:
        mask
    """
    labels_map: np.ndarray = np.apply_along_axis(
        func1d=get_class, axis=0, arr=proba_map, labels_dict=labels
    )
    labels_map = labels_map.astype("int32")
    labels_map = np.expand_dims(labels_map, axis=0)
    if mask_arr is not None:
        mask_arr = binarize(mask_arr)
        labels_map = labels_map * mask_arr

    with rasterio.open(
        out_classif,
        "w",
        driver="GTiff",
        height=labels_map.shape[1],
        width=labels_map.shape[2],
        count=labels_map.shape[0],
        crs=f"EPSG:{geo_parameters.epsg}",
        transform=geo_parameters.transform,
        dtype=labels_map.dtype,
    ) as dest:
        dest.write(labels_map)
    return labels_map


def write_prediction(
    out_path: str,
    prediction: np.ndarray,
    geo_parameters: RasterioSpatialMetadata,
    mask_arr: np.ndarray | None = None,
) -> None:
    """Write prediction to output file."""
    if mask_arr is not None:
        mask_arr = binarize(mask_arr)
        prediction = prediction * mask_arr

    with rasterio.open(
        out_path,
        "w",
        driver="GTiff",
        height=prediction.shape[1],
        width=prediction.shape[2],
        count=prediction.shape[0],
        crs=f"EPSG:{geo_parameters.epsg}",
        transform=geo_parameters.transform,
        dtype=prediction.dtype,
    ) as dest:
        dest.write(prediction)


def probabilities_to_max_proba(
    proba_map: np.ndarray,
    geo_parameters: RasterioSpatialMetadata,
    out_max_confidence: str | None = None,
) -> np.ndarray:
    """Get the max probability from the prediction probabilities vector.

    Parameters
    ----------
    proba_map: numpy.array
        array of probabilities
    geo_parameters:
        Dataclass containing geo parameters (epsg code + transform)
    out_max_confidence: str
        output raster path

    Return
    ------
    numpy.array
        confidence
    """
    max_confidence_arr: np.ndarray = np.amax(proba_map, axis=0)
    max_confidence_arr = np.expand_dims(max_confidence_arr, axis=0)

    if out_max_confidence:
        with rasterio.open(
            out_max_confidence,
            "w",
            driver="GTiff",
            height=max_confidence_arr.shape[1],
            width=max_confidence_arr.shape[2],
            count=max_confidence_arr.shape[0],
            crs=f"EPSG:{geo_parameters.epsg}",
            transform=geo_parameters.transform,
            dtype=max_confidence_arr.dtype,
        ) as dest:
            dest.write(max_confidence_arr)
    return max_confidence_arr


def get_geo_params(data: UserDefinedFunctionPayload) -> RasterioSpatialMetadata:
    """
    Get the epsg and transform from a UserDefinedFunctionPayload

    Parameters
    ----------
    data: UserDefinedFunctionPayload
        Container for data

    Returns
    -------
    geo_params:
        Geo parameters retrieved from data
    """
    spatial_meta = data.spatial_meta
    geo_params = RasterioSpatialMetadata(
        epsg=spatial_meta.projection.GetAttrValue("AUTHORITY", 1),
        transform=gdal_transform_to_rio(spatial_meta.gdal_geo_transform),
    )
    return geo_params


def copy_to_working_dir(
    working_dir: str | None,
    pred_output: PredictionOutput,
    mode: Literal["classif", "regression"],
) -> None:
    """
    If a working directory is given, copy output files in that directory

    Parameters
    ----------
    working_dir: str
        Path to the working directory
    pred_output: PredictionOutput
        Dataclass containing output paths
    mode: Literal["classif", "regression"]
        Classification or regression

    Returns
    -------
    None
    """
    if working_dir:
        classification_dir = str(Path(pred_output.output_path) / "classif")
        shutil.copy(pred_output.classif, classification_dir)
        if mode == "classif":
            shutil.copy(pred_output.confidence, classification_dir)
            Path(pred_output.confidence).unlink()
        Path(pred_output.classif).unlink()
        if pred_output.proba:
            shutil.copy(pred_output.proba, classification_dir)
            Path(pred_output.proba).unlink()


def update_output_path(working_dir: str | None, pred_output: PredictionOutput) -> None:
    """
    If a working directory is given, update the output paths

    Parameters
    ----------
    working_dir: str
        Path to the working directory
    pred_output: PredictionOutput
        Dataclass containing output paths

    Returns
    -------
    None
    """
    if working_dir:
        out_classif_name = Path(pred_output.classif).name
        out_confidence_name = Path(pred_output.confidence).name
        pred_output.classif = str(Path(working_dir) / out_classif_name)
        pred_output.confidence = str(Path(working_dir) / out_confidence_name)
        if pred_output.proba:
            out_proba_name = Path(pred_output.proba).name
            pred_output.proba = str(Path(working_dir) / out_proba_name)


def get_raw_data(self: DataContainer) -> tuple[np.ndarray, list[I2Label]]:
    """Deal with irregular input dates for the custom numpy features."""
    coef, labels_refl = self.get_filled_stack()  # type: ignore # method dynamically created
    masks, label_masks = self.get_filled_masks()  # type: ignore # same reason than previous one

    coef = np.concatenate((coef, masks), axis=2)
    labels = labels_refl + label_masks
    return coef, labels


def do_nothing(array: np.ndarray) -> tuple[np.ndarray, EmptyList]:
    """Use with the insert_external_function_to_pipeline workflow."""
    empty_list: EmptyList = []
    return array, empty_list


def write_output_maps(
    pred_output: PredictionOutput,
    labels_map: np.ndarray,
    confidence_map: np.ndarray,
    proba_map: np.ndarray | None,
    geo_metadata: RasterioSpatialMetadata,
) -> None:
    """
    Write label and confidence maps + the proba-map if enabled

    Parameters
    ----------
    pred_output : PredictionOutput
        Dataclass with path to output maps
    labels_map : np.ndarray
        Numpy array containing the label map
    confidence_map : np.ndarray
        Numpy array containing the confidence map
    proba_map : np.ndarray
        Numpy array containing the proba map
    geo_metadata : RasterioSpatialMetadata
        Rasterio geospatial metadata for writing images

    Returns
    -------
    None

    """
    with rasterio.open(
        pred_output.classif,
        "w",
        driver="GTiff",
        height=labels_map.shape[1],
        width=labels_map.shape[2],
        count=labels_map.shape[0],
        crs=f"EPSG:{geo_metadata.epsg}",
        transform=geo_metadata.transform,
        dtype=np.uint8,
    ) as dest:
        dest.write(labels_map)
    with rasterio.open(
        pred_output.confidence,
        "w",
        driver="GTiff",
        height=labels_map.shape[1],
        width=labels_map.shape[2],
        count=labels_map.shape[0],
        crs=f"EPSG:{geo_metadata.epsg}",
        transform=geo_metadata.transform,
        dtype=np.float32,
    ) as dest:
        dest.write(confidence_map)
    if pred_output.proba is not None and proba_map is not None:
        with rasterio.open(
            pred_output.proba,
            "w",
            driver="GTiff",
            height=proba_map.shape[1],
            width=proba_map.shape[2],
            count=proba_map.shape[0],
            crs=f"EPSG:{geo_metadata.epsg}",
            transform=geo_metadata.transform,
            dtype=np.float32,
        ) as dest:
            dest.write(proba_map)


def get_data_from_pipeline(
    pred_parameters: PredictionParameters,
    pred_output: PredictionOutput,
    working_dir: str | None,
    custom_features_parameters: CustomFeaturesParameters,
    mask: str | None,
    targeted_chunk: int | None,
    number_of_chunks: int | None,
    logger: Logger = LOGGER,
) -> tuple[PredictionPipelineData, RasterioSpatialMetadata]:
    """
    Provide the desired pipeline for inference.

    Parameters
    ----------
    pred_parameters:
        Parameters for the prediction
    pred_output:
        Output paths for the prediction
    working_dir:
        Working directory
    custom_features_parameters:
        Parameters used in custom features
    mask:
        raster mask path
    number_of_chunks: int
        Total number of chunks
    targeted_chunk: int
        Target chunk
    logger:
        Logger
    """
    otb_pipelines, labs_dict = generate_features(
        FeaturesMapParameters(
            working_directory=working_dir,
            tile=pred_parameters.tile_name,
            output_path=pred_output.output_path,
            sensors_parameters=pred_parameters.sensors_parameters,
        ),
        logger=logger,
    )
    logger.info(f"producing {pred_output.classif}")
    if pred_parameters.features_from_raw_dates:
        custom_features_parameters.functions = [("get_raw_data", {})]
        custom_features_parameters.module = str(
            Path(get_iota2_project_dir()) / "iota2" / "common" / "external_code.py"
        )
        pred_parameters.enabled_raw = True
        pred_parameters.enabled_gap = False
    data_stack = None
    data_mask = None
    if pred_parameters.sensors_dates and (
        pred_parameters.features_from_raw_dates or custom_features_parameters.enable
    ):
        sensors_dates_str = build_sensors_dates_dict(pred_parameters.sensors_dates)

        pred_parameters.exogeneous_data = exogenous_data_tile(
            pred_parameters.exogeneous_data, pred_parameters.tile_name
        )
        otb_pipelines = turn_off_otb_pipelines(
            otb_pipelines,
            turn_off_raw=pred_parameters.enabled_raw is False,
            turn_off_interpolated=pred_parameters.enabled_gap is False,
        )
        assert custom_features_parameters.functions
        data, masks_list = compute_custom_features(
            functions_builder=CustomNumpyFeatures(
                sensors_params=pred_parameters.sensors_parameters,
                module_name=custom_features_parameters.module,
                list_functions=custom_features_parameters.functions,
                configuration=CustomFeaturesConfiguration(
                    concat_mode=custom_features_parameters.concat_mode,
                    enabled_raw=pred_parameters.enabled_raw,
                    enabled_gap=pred_parameters.enabled_gap,
                    fill_missing_dates=pred_parameters.enabled_raw,
                    missing_refl_values=np.nan,
                    allow_nans=True,
                ),
                all_dates_dict=sensors_dates_str,
                exogeneous_data_file=pred_parameters.exogeneous_data,
            ),
            otb_pipelines=otb_pipelines,
            feat_labels=labs_dict.interp,
            masking_raster=MaskingRaster(mask),
            chunk_params=ChunkParameters(
                ChunkConfig(
                    chunk_size_mode="split_number",
                    number_of_chunks=number_of_chunks,
                    chunk_size_x=None,
                    chunk_size_y=None,
                ),
                targeted_chunk,
            ),
            concatenate_features=custom_features_parameters.concat_mode,
            expected_output_bands=1,
            exogeneous_data=pred_parameters.exogeneous_data,
            logger=logger,
        )
        otbimage = data.otb_img
        feat_labels = data.labels
        if len(masks_list) > 1:
            raise ValueError("Only one mask is expected")
    else:
        feat_labels = labs_dict.interp
        data, masks_list = handle_pipeline_and_masks(
            otb_pipelines,
            labs_dict,
            mask,
            targeted_chunk,
            number_of_chunks,
            pred_parameters.exogeneous_data,
            logger,
        )
        data_stack = data.data
        otbimage = data.otb_img

        if len(masks_list) > 1:
            raise ValueError("Only one mask is expected")
        data_stack = reshape_as_image(data_stack)
        data_mask = None
        # segfault can append if we try to print
        # here the otbimage variable. solved in #430
        # it cannot be reproductible in a unittest case...

    assert otbimage is not None
    # find all indexes corresponding to masks labels
    list_masks_index = [
        label_index
        for label_index, label in enumerate(feat_labels)
        if "mask" in label.feat_name.lower()
    ]
    masks_labels: list[I2Label | str] | None = None
    # create a binary mask to separate mask and features
    # (True for index corresponding to masks, 0 otherwise)
    masks_indices = np.zeros(otbimage["array"].shape[2])
    masks_indices[list_masks_index] = True
    masks_indices = masks_indices.astype(int)
    if pred_parameters.features_from_raw_dates:
        data_stack = otbimage["array"][:, :, ~masks_indices].astype(np.float32)
        data_mask = otbimage["array"][:, :, masks_indices]
        masks_labels = [
            label
            for index, label in enumerate(feat_labels)
            if index in list_masks_index
        ]

    elif custom_features_parameters.enable:
        if not bool(feat_labels) and list_masks_index:
            data_stack = otbimage["array"][:, :, ~masks_indices].astype(np.float32)
            data_mask = otbimage["array"][:, :, masks_indices]
            masks_labels = [
                label
                for index, label in enumerate(feat_labels)
                if index in list_masks_index
            ]
            feat_labels = [
                label
                for index, label in enumerate(feat_labels)
                if index not in list_masks_index
            ]
        else:
            data_stack = otbimage["array"]
            data_mask = None

    return (
        PredictionPipelineData(
            stack=data_stack,
            mask=data_mask,
            feat_labels=feat_labels,
            mask_labels=masks_labels,
            spatial_mask=masks_list[0],
            dependencies=(
                [data.pipeline_dependencies] if data.pipeline_dependencies else None
            ),
        ),
        gdal_to_rio_metadata(data.spatial_meta),
    )


def build_sensors_dates_dict(
    sensor_dates_dict: dict[str, list[Any]]
) -> dict[str, list[str]]:
    """
    Convert dates to strings in a dictionary of sensor names and their dates.

    Parameters
    ----------
    sensor_dates_dict: dict
        A dictionary where keys are sensor names and values are lists of dates.

    Returns
    -------
    sensors_dates_str: dict

    """
    sensors_dates_str = {}
    for sensor_name, sensor_dates in sensor_dates_dict.items():
        sensors_dates_str[sensor_name] = [str(date) for date in sensor_dates]
    return sensors_dates_str


def handle_pipeline_and_masks(
    otb_pipelines: OtbPipelineCarrier,
    labs_dict: PipelinesLabels,
    mask: str | None,
    targeted_chunk: int | None,
    number_of_chunks: int | None,
    exogeneous_data: str | None = None,
    logger: Logger = LOGGER,
) -> tuple[UserDefinedFunctionPayload, list[np.ndarray | None]]:
    """
    Chunk pipelines and apply a placeholder function

    Parameters
    ----------
    otb_pipelines : OtbPipelineCarrier
        Dataclass containing an OTB pipeline
    labs_dict : PipelinesLabels
        Dataclass containing labels corresponding to otb pipelines
    mask : str | None
        Raster mask path
    targeted_chunk : int | None
        Target chunk
    number_of_chunks : int | None
        Total number of chunks
    exogeneous_data : str | None
        Path to a Geotiff file containing additional data to be used in external features
    logger: Logger
        Logger

    Returns
    -------
    tuple[ru.PipelineData, list]
        - data: Output otb pipeline
        - masks: List of masks
    """
    function_partial = partial(do_nothing)
    output_number_of_bands = len(labs_dict.interp)

    otb_pipeline_chunked = pipelines_chunking(
        otb_pipelines,
        ru.ChunkConfig(
            chunk_size_mode="split_number",
            number_of_chunks=number_of_chunks,
            chunk_size_x=0,
            chunk_size_y=0,
        ),
        spatial_mask=mask,
        exogenous_data_file=exogeneous_data,
        targeted_chunk=targeted_chunk,
    )
    data, masks = ru.apply_udf_to_pipeline(
        otb_pipeline_chunked,
        UserDefinedFunction(function_partial),
        mask_value=0,
        output_number_of_bands=output_number_of_bands,
        logger=logger,
    )

    return data, masks
