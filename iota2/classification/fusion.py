#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""module to manage fusion of classifications in iota2"""
import argparse
import logging
import sys
from pathlib import Path
from typing import Literal

from iota2.common import file_utils as fu
from iota2.common import otb_app_bank
from iota2.common.otb_app_bank import AvailableOTBApp
from iota2.common.utils import run
from iota2.typings.i2_types import PathLike
from iota2.validation.results_utils import gen_confusion_matrix_fig

LOGGER = logging.getLogger("distributed.worker")


def fusion(
    in_classif: list[str],
    fusion_options: str,
    out_classif: str,
    out_pix_type: str,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Launch the otb application FusionOfClassifications

    Parameters
    ----------
    in_classif :
        the list of classification to fuse
    fusion_options:
        the fusion method and the associated parameters
    out_classif:
        the output classification
    out_pix_type:
        the output pixel type
    """
    in_classif_str = " ".join(in_classif)
    cmd = (
        f"otbcli_FusionOfClassifications -il {in_classif_str} {fusion_options}"
        f" -out {out_classif} {out_pix_type}"
    )
    run(cmd, logger=logger)


def get_parameters_for_fusion(
    output_dirs_list: list[str],
) -> tuple[list[str], list[str]]:
    """
    Retrieve all the paths to classifications and confusion matrices for the fusion from a list of
    all iota2 output directories

    Parameters
    ----------
    output_dirs_list: list[str]
        The list of all iota2 output directories

    Returns
    -------
    tuple[list[str], list[str]]
        Return a tuple with the list of classifications and the list confusion matrices
    """
    list_classifs = []
    list_confusion_matrices = []
    for iota2_dir in output_dirs_list:
        # Find the classification
        final_dir = Path(iota2_dir) / "final"
        classif = fu.file_search_and(final_dir, True, "Classif", ".tif")[0]

        # Find the confusion matrix
        conf_mat_dir = final_dir / "TMP"
        conf_mat = fu.file_search_and(conf_mat_dir, True, "Classif_Seed", ".csv")[0]

        list_classifs.append(classif)
        list_confusion_matrices.append(conf_mat)

    return list_classifs, list_confusion_matrices


def perform_fusion(
    list_classifs: list[str],
    list_confusion_matrices: list[str] | None,
    output_dir: PathLike,
    method: Literal["dempstershafer", "majorityvoting"],
    mob: Literal["precision", "recall", "overall accuracy", "kappa"] = "precision",
) -> str:
    """
    Run the FusionOfClassifications otb app on the collected classifications

    Parameters
    ----------
    list_classifs: list[str]
        List of paths to the classification rasters
    list_confusion_matrices: list[str] | None
        List of paths to the confusion matrices as csv (only used if method='demsptershafer')
    output_dir:
        Output directory
    method: Literal["dempstershafer", "majorityvoting"]
        Method used for choosing which classif to use for each pixel
    mob: Literal["precision", "recall", "overall accuracy", "kappa"]
        Mass of belief measurement

    Returns
    -------
    str
        Path to the output fusion of classifications
    """
    output_file = f"{output_dir}/final_classification.tif"
    if Path(output_file).exists():
        Path(output_file).unlink()

    fusion_parameters = {
        "il": list_classifs,
        "method": method,
        "out": output_file,
    }

    if method == "dempstershafer":
        if not list_confusion_matrices:
            raise ValueError(
                "When using the Dempster-Schafer method, confusion matrices are required"
            )

        fusion_parameters.update(
            {
                "method.dempstershafer.mob": mob,
                "method.dempstershafer.cmfl": list_confusion_matrices,
            }
        )

    ds_fus, _ = otb_app_bank.create_application(
        AvailableOTBApp.FUSION_OF_CLASSIFICATIONS, fusion_parameters
    )
    ds_fus.ExecuteAndWriteOutput()
    return output_file


def compute_confusion_of_fusion(
    ref_vector: str,
    classification: str,
    data_field: str,
    output_dir: str,
    ram: int = 128,
) -> str:
    """Compute confusion matrix for Dempster-Shaffer fusion.

    function use to compute a confusion matrix dedicated to the Dempster-Shafer
    classification fusion.

    Parameters
    ----------
    ref_vector:
        Path to the reference vector
    classification:
        Path to the fused classification raster
    data_field:
        labels fields in reference vector
    output_dir:
        Directory where the confusion matrix will be stored
    ram:
        ram dedicated to produce the confusion matrix (OTB's pipeline size)
    """
    csv_out = f"{output_dir}/final_confusion_matrix.csv"
    Path(csv_out).unlink(missing_ok=True)

    confusion_parameters = {
        "in": classification,
        "out": csv_out,
        "ref": "vector",
        "ref.vector.in": ref_vector,
        "ref.vector.field": data_field.lower(),
        "ram": str(0.8 * ram),
    }
    confusion_matrix, _ = otb_app_bank.create_application(
        otb_app_bank.AvailableOTBApp.CONFUSION_MATRIX, confusion_parameters
    )
    confusion_matrix.ExecuteAndWriteOutput()

    return csv_out


def main() -> int:
    """
    Function used as the entry point for the fusion of classifications
    """
    parser = argparse.ArgumentParser(
        description=(
            "Merge all classifications using the FusionOfClassifications otb app and produce the "
            "corresponding confusion matrix."
        )
    )

    parser.add_argument(
        "-iota2_directories",
        help="List of iota2 output directories with fully completed classifications.",
        dest="iota2_dirs",
        required=True,
        nargs="+",
    )
    parser.add_argument(
        "-output_dir",
        help="Path to the final output directory",
        dest="output_dir",
        required=True,
    )
    parser.add_argument(
        "-path_reference_data",
        help="Path to the vector reference data file",
        dest="ref_vector",
        required=True,
    )
    parser.add_argument(
        "-data_field",
        help="Label column name in the reference data file",
        dest="data_field",
        required=True,
    )
    parser.add_argument(
        "-path_nomenclature",
        help="Path to the nomenclature file",
        dest="path_nomenclature",
        required=True,
    )
    parser.add_argument(
        "-method",
        help="Method used for the fusion (either 'dempsterschafer' or 'majorityvoting')",
        dest="path_nomenclature",
        required=True,
        choices=["dempsterschafer", "majorityvoting"],
    )
    parser.add_argument(
        "-mob",
        help="Mass of belief measurement used in otb (can be 'precision', 'recall', 'overall "
        "accuracy' or 'kappa', default='precision')",
        dest="mob",
        required=False,
        default="precision",
        choices=["precision", "recall", "overall accuracy", "kappa"],
    )
    args = parser.parse_args()

    # Get all classif rasters and confusion matrices
    list_classifs, list_confusion_matrices = get_parameters_for_fusion(args.iota2_dirs)
    # Compute the fused classification raster
    final_classif = perform_fusion(
        list_classifs=list_classifs,
        list_confusion_matrices=list_confusion_matrices,
        output_dir=args.output_dir,
        method=args.method,
        mob=args.mob,
    )
    # Compute the corresponding confusion matrix
    confusion_matrix = compute_confusion_of_fusion(
        ref_vector=args.ref_vector,
        classification=final_classif,
        data_field=args.data_field,
        output_dir=args.output_dir,
    )
    # Save the matrix as a figure
    output_image = f"{args.output_dir}/final_confusion_matrix.png"
    gen_confusion_matrix_fig(
        confusion_matrix,
        args.path_nomenclature,
        output_image,
    )

    return 0


if __name__ == "__main__":
    sys.exit(main())
