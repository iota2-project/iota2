#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module interfacing scikit for iota2."""
import logging
import pickle
from functools import partial
from pathlib import Path
from typing import Any, Literal

import numpy as np
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

from iota2.classification.py_classifiers import (
    LOGGER,
    PredictionParameters,
    copy_to_working_dir,
    get_geo_params,
    proba_to_label,
    probabilities_to_max_proba,
    update_output_path,
    write_prediction,
)
from iota2.common import raster_utils as ru
from iota2.common.compression_options import compression_options
from iota2.common.custom_numpy_features import (
    ChunkParameters,
    CustomFeaturesConfiguration,
    CustomNumpyFeatures,
    MaskingRaster,
    compute_custom_features,
    exogenous_data_tile,
)
from iota2.common.generate_features import FeaturesMapParameters, generate_features
from iota2.common.raster_utils import (
    ChunkConfig,
    OtbPipelineCarrier,
    UserDefinedFunction,
    UserDefinedFunctionPayload,
    merge_rasters,
    pipelines_chunking,
    turn_off_otb_pipelines,
)
from iota2.learning.utils import I2Label
from iota2.typings.i2_types import (
    CustomFeaturesParameters,
    OtbImage,
    PipelinesLabels,
    PredictionOutput,
)


def merge_sk_classifications(
    rasters_to_merge: tuple[list[str], ...],
    mosaic_file: tuple[str, ...],
    logger: logging.Logger = LOGGER,
    mode: Literal["classif", "regression"] = "classif",
) -> None:
    """Mosaic rasters.

    Parameters
    ----------
    rasters_to_merge :
        tuple of list of raster to be merged
    logger :
        root logger
    mode :
        "classif" or "regression"
        in regression mode, ignores confidence files
        and use float instead of int
    """
    creation_options = {
        "COMPRESS": compression_options.algorithm,
        "PREDICTOR": compression_options.predictor,
        "TILDED": "YES",
        "BIGTIFF": "YES",
    }

    if mode == "classif":
        pix_type = "Int16"
    else:  # regression
        pix_type = "Float32"

    classifications = [
        str(Path(raster)) for raster in rasters_to_merge[0] if Path(raster).exists()
    ]
    classif_mosaic = str(Path(mosaic_file[0]))
    confidences = [
        str(Path(raster)) for raster in rasters_to_merge[1] if Path(raster).exists()
    ]
    confidence_mosaic = str(Path(mosaic_file[1]))

    logger.info(f"creating : {mosaic_file}")
    # merge chunks
    merge_rasters(classifications, classif_mosaic, pix_type, creation_options)
    if mode == "classif":
        merge_rasters(confidences, confidence_mosaic, "Float32", creation_options)
    proba_map_mosaic = ""
    if len(rasters_to_merge) == 3 and len(mosaic_file) == 3:
        proba_map = [
            str(Path(raster)) for raster in rasters_to_merge[2] if Path(raster).exists()
        ]
        proba_map_mosaic = mosaic_file[2]
        merge_rasters(proba_map, proba_map_mosaic, "Float32", creation_options)
    if Path(classif_mosaic).exists():
        for classif in classifications:
            Path(classif).unlink()
    if Path(confidence_mosaic).exists():
        for confidence in confidences:
            Path(confidence).unlink()
    if proba_map_mosaic and Path(proba_map_mosaic).exists():
        for proba in proba_map:
            Path(proba).unlink()


def do_predict(
    array: np.ndarray,
    model: SVC | RandomForestClassifier | ExtraTreesClassifier,
    scaler: StandardScaler | None = None,
    dtype: str = "float",
    mode: Literal["classif", "regression"] = "classif",
) -> tuple[np.ndarray, list[I2Label]]:
    """Perform scikit-learn prediction

    Parameters
    ----------
    array : np.array
        array of features to predict, shape = (y, x, features)
    model : SVC / RandomForestClassifier / ExtraTreesClassifier
        scikit-learn classifier
    scaler : StandardScaler
        scaler to standardize features
    dtype : str
        output array format
    mode : str
        "classif" or "regression"
        controls if model should predict probability

    Notes
    -----
        Description of return value:
            tuple[np.array, list[I2Label]]
            np.array represent predictions
            list[I2Label] attach a label for each prediction/proba.
    """
    array_reshaped = array.reshape((array.shape[0] * array.shape[1]), array.shape[2])

    if mode == "classif":
        if scaler is not None:
            predicted_array = model.predict_proba(scaler.transform(array_reshaped))
        else:
            predicted_array = model.predict_proba(array_reshaped)
        predicted_array = predicted_array.reshape(
            array.shape[0], array.shape[1], predicted_array.shape[-1]
        )
    elif mode == "regression":
        if scaler is not None:
            predicted_array = model.predict(scaler.transform(array_reshaped))
        else:
            predicted_array = model.predict(array_reshaped)
        predicted_array = predicted_array.reshape(array.shape[0], array.shape[1], 1)
    labels = [
        I2Label(
            sensor_name="prediction" if mode == "regression" else "proba",
            feat_name=band,
        )
        for band in range(predicted_array.shape[-1])
    ]
    return predicted_array.astype(dtype), labels


def scikit_learn_predict(
    pred_parameters: PredictionParameters,
    mask: str,
    model: str,
    pred_output: PredictionOutput,
    working_dir: str,
    custom_features_parameters: CustomFeaturesParameters,
    number_of_chunks: int | None = None,
    targeted_chunk: int | None = None,
    logger: logging.Logger = LOGGER,
    mode: Literal["classif", "regression"] = "classif",
) -> None:
    """Perform scikit-learn prediction.

    This function will produce 2 rasters, the classification map and the
    confidence map. The confidence map represent the classifier confidence in
    the chosen class.

    Parameters
    ----------
    pred_parameters:
        Dataclass containing parameters for predictions
    mask:
        raster mask path
    model:
        input model path
    pred_output:
        Dataclass containing output paths for the prediction
    working_dir:
        path to a working direction to store temporary data
    custom_features_parameters:

    number_of_chunks:
        The prediction process can be done by strips. This parameter
        set the number of strips
    targeted_chunk:
        If this parameter is provided, only the targeted strip will be computed (parallelization).
    logger:
        root logger
    mode:
        string with possible values "classif", "regression"
        controls the output_number_of_bands parameter
        and the presence of prediction probability
    """
    if custom_features_parameters.functions is None:
        custom_features_parameters.functions = []

    update_output_path(working_dir, pred_output)
    with open(model, "rb") as model_file:
        scikit_model, scaler_features, _ = pickle.load(model_file)

    if number_of_chunks and targeted_chunk:
        if targeted_chunk > number_of_chunks - 1:
            raise ValueError("targeted_chunk must be inferior to the number of chunks")

    dict_stack, labs_dict = generate_features(
        FeaturesMapParameters(
            working_directory=working_dir,
            tile=pred_parameters.tile_name,
            output_path=pred_output.output_path,
            sensors_parameters=pred_parameters.sensors_parameters,
        ),
        logger=logger,
    )
    logger.info(f"producing {pred_output.classif}")

    if custom_features_parameters.enable and pred_parameters.sensors_dates:
        data, masks, predicted_proba = handle_custom_features(
            custom_features_parameters,
            dict_stack,
            labs_dict,
            logger,
            mask,
            mode,
            pred_parameters,
            scaler_features,
            scikit_model,
            (number_of_chunks, targeted_chunk),
        )

    else:
        data, masks, predicted_proba = handle_standard_prediction(
            dict_stack,
            logger,
            mask,
            mode,
            pred_parameters,
            scaler_features,
            scikit_model,
            (number_of_chunks, targeted_chunk),
        )

    logger.info("predictions done")
    if len(masks) > 1:
        raise ValueError("Only one mask is expected")

    if targeted_chunk is not None:
        pred_output.classif = pred_output.classif.replace(
            ".tif", f"_SUBREGION_{targeted_chunk}.tif"
        )
        pred_output.confidence = pred_output.confidence.replace(
            ".tif", f"_SUBREGION_{targeted_chunk}.tif"
        )

    geo_params = get_geo_params(data)
    if mode == "classif":
        if (
            (np.sum(masks[0]) > 0) | (not custom_features_parameters.enable)
            and predicted_proba is not None
            and pred_output.classif is not None
        ):
            proba_to_label(
                predicted_proba,
                pred_output.classif,
                scikit_model.classes_,
                geo_params,
                masks[0],
            )
        else:
            write_prediction(pred_output.classif, predicted_proba, geo_params, masks[0])
        probabilities_to_max_proba(predicted_proba, geo_params, pred_output.confidence)
    elif mode == "regression":
        write_prediction(pred_output.classif, predicted_proba, geo_params, masks[0])
    copy_to_working_dir(working_dir, pred_output, mode)


def handle_standard_prediction(
    dict_stack: OtbPipelineCarrier,
    logger: logging.Logger,
    mask: str,
    mode: Literal["classif", "regression"],
    pred_parameters: PredictionParameters,
    scaler_features: Any,
    scikit_model: Any,
    chunks_options: tuple[int | None, int | None],
) -> tuple[UserDefinedFunctionPayload, list[np.ndarray | None], np.ndarray]:
    """
    Perform standard prediction using scikit-learn models.

    This function handles the prediction process when custom features are not enabled.
    It computes the prediction for either classification or regression modes and returns
    the predicted probabilities along with the data and masks.

    Parameters
    ----------
    dict_stack : OtbPipelineCarrier
        Carrier for the OTB pipelines used for feature extraction.
    logger : logging.Logger
        Logger instance for logging messages.
    mask : str
        Path to the raster mask file used to limit the area of prediction.
    mode : Literal["classif", "regression"]
        Specifies whether the prediction is for classification or regression.
    pred_parameters : PredictionParameters
        Dataclass containing parameters necessary for the prediction process.
    scaler_features : Any
        Scaler object used to normalize the features before prediction.
    scikit_model : Any
        Trained scikit-learn model used for prediction.
    chunks_options : tuple[int | None, int | None]
        Tuple containing options for chunking the data:
        - number_of_chunks (int | None): Number of chunks to split the data into.
        - targeted_chunk (int | None): Specifies the chunk to be processed, if any.

    Returns
    -------
    tuple
        A tuple containing:
        - UserDefinedFunctionPayload: The data payload resulting from the prediction.
        - list[np.ndarray | None]: A list of masks applied during prediction.
        - np.ndarray | None: The predicted probabilities or values.
    """
    output_number_of_bands = 1
    if mode == "classif":
        output_number_of_bands = len(scikit_model.classes_)
    # ~ sk-learn provide only methods 'predict' and 'predict_proba', no proba_max.
    # ~ Then we have to compute the full probability vector to get the maximum
    # ~ confidence and generate the confidence map
    otb_pipeline_chunked = pipelines_chunking(
        dict_stack,
        ru.ChunkConfig(
            chunk_size_mode="split_number",
            number_of_chunks=chunks_options[0],
            chunk_size_x=None,
            chunk_size_y=None,
        ),
        spatial_mask=mask,
        exogenous_data_file=pred_parameters.exogeneous_data,
        targeted_chunk=chunks_options[1],
    )
    (data, masks) = ru.apply_udf_to_pipeline(
        otb_pipeline_chunked,
        UserDefinedFunction(
            partial(
                do_predict,
                model=scikit_model,
                scaler=scaler_features,
                mode=mode,
            )
        ),
        mask_value=0,
        output_number_of_bands=output_number_of_bands,
        logger=logger,
    )
    predicted_proba = data.data
    assert predicted_proba is not None
    return data, masks, predicted_proba


def handle_custom_features(
    custom_features_parameters: CustomFeaturesParameters,
    dict_stack: OtbPipelineCarrier,
    labs_dict: PipelinesLabels,
    logger: logging.Logger,
    mask: str,
    mode: Literal["classif", "regression"],
    pred_parameters: PredictionParameters,
    scaler_features: Any,
    scikit_model: Any,
    chunks_options: tuple[int | None, int | None],
) -> tuple[UserDefinedFunctionPayload, list[np.ndarray | None], np.ndarray]:
    """
    Handle custom feature extraction and prediction using scikit-learn models.

    This function manages the custom feature extraction process and then performs
    prediction using the provided scikit-learn model. It supports both classification
    and regression modes, and allows for processing in chunks.

    Parameters
    ----------
    custom_features_parameters : CustomFeaturesParameters
        Dataclass containing parameters for custom feature extraction, including the
        module name and the list of functions to apply.
    dict_stack : OtbPipelineCarrier
        Carrier for the OTB pipelines used for feature extraction.
    labs_dict : PipelinesLabels
        Dataclass containing the labels for the OTB pipelines.
    logger : logging.Logger
        Logger instance for logging messages.
    mask : str
        Path to the raster mask file used to limit the area of prediction.
    mode : Literal["classif", "regression"]
        Specifies whether the prediction is for classification or regression.
    pred_parameters : PredictionParameters
        Dataclass containing parameters necessary for the prediction process.
    scaler_features : Any
        Scaler object used to normalize the features before prediction.
    scikit_model : Any
        Trained scikit-learn model used for prediction.
    chunks_options : tuple[int | None, int | None]
        Tuple containing options for chunking the data:
        - number_of_chunks (int | None): Number of chunks to split the data into.
        - targeted_chunk (int | None): Specifies the chunk to be processed, if any.

    Returns
    -------
    tuple
        A tuple containing:
        - UserDefinedFunctionPayload: The data payload resulting from the custom feature extraction
          and prediction.
        - list[np.ndarray | None]: A list of masks applied during prediction.
        - np.ndarray: The predicted probabilities or values.
    """
    sensors_dates_str = {}
    assert isinstance(pred_parameters.sensors_dates, dict)
    for sensor_name, sensor_dates in pred_parameters.sensors_dates.items():
        sensors_dates_str[sensor_name] = [str(date) for date in sensor_dates]
    pred_parameters.exogeneous_data = exogenous_data_tile(
        pred_parameters.exogeneous_data, pred_parameters.tile_name
    )
    dict_stack = turn_off_otb_pipelines(
        dict_stack,
        turn_off_raw=pred_parameters.enabled_raw is False,
        turn_off_interpolated=pred_parameters.enabled_gap is False,
    )
    assert custom_features_parameters.functions
    data, masks = compute_custom_features(
        functions_builder=CustomNumpyFeatures(
            sensors_params=pred_parameters.sensors_parameters,
            module_name=custom_features_parameters.module,
            list_functions=custom_features_parameters.functions,
            configuration=CustomFeaturesConfiguration(
                concat_mode=custom_features_parameters.concat_mode,
                enabled_raw=pred_parameters.enabled_raw,
                enabled_gap=pred_parameters.enabled_gap,
                fill_missing_dates=pred_parameters.enabled_raw,
            ),
            exogeneous_data_file=pred_parameters.exogeneous_data,
        ),
        otb_pipelines=dict_stack,
        feat_labels=labs_dict.interp,
        chunk_params=ChunkParameters(
            ChunkConfig(
                chunk_size_mode="split_number",
                number_of_chunks=chunks_options[0],
                chunk_size_x=None,
                chunk_size_y=None,
            ),
            chunks_options[1],
        ),
        concatenate_features=custom_features_parameters.concat_mode,
        exogeneous_data=pred_parameters.exogeneous_data,
        masking_raster=MaskingRaster(mask),
        logger=logger,
        expected_output_bands=len(labs_dict.interp),
    )
    otbimage = data.otb_img
    assert otbimage is not None
    assert all(mask is not None for mask in masks)
    predicted_proba = get_pred_proba(
        otbimage, masks, scikit_model, scaler_features, mode
    )
    return data, masks, predicted_proba


def get_pred_proba(
    otbimage: OtbImage,
    masks: list[np.ndarray | None],
    scikit_model: SVC | RandomForestClassifier | ExtraTreesClassifier,
    scaler_features: StandardScaler | None = None,
    mode: Literal["classif", "regression"] = "classif",
) -> np.ndarray:
    """
    Compute predicted probabilities

    Parameters
    ----------
    otbimage: OtbImage
        Object containing all data of the otb image
    masks: np.ndarray
        Numpy array of the masks
    scikit_model: SVC | RandomForestClassifier | ExtraTreesClassifier
        Trained scikit model used for predictions
    scaler_features: StandardScaler | None
        Scaler object used for normalizing features
    mode: Literal["classif", "regression"]
        Classif or regression

    Returns
    -------
    predicted_proba:
        Numpy array containing the predicted probabilities
    """
    if np.sum(masks[0]) > 0:
        predicted_proba, _ = do_predict(
            otbimage["array"], scikit_model, scaler_features, mode=mode
        )
        predicted_proba = np.einsum("ijk->kij", predicted_proba)
    else:
        predicted_proba = (
            np.ones((otbimage["array"].shape[0], otbimage["array"].shape[1])) * masks[0]
        )
        predicted_proba = predicted_proba.reshape(
            (1, otbimage["array"].shape[0], otbimage["array"].shape[1])
        )
    return predicted_proba
