#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" This module contains pixel classification tools"""
import logging
import shutil
import sqlite3
from dataclasses import dataclass
from pathlib import Path
from typing import Any

import pandas as pd

from iota2.common import file_utils as fu
from iota2.common import generate_features as genFeatures
from iota2.common import raster_utils as ru
from iota2.common.compression_options import delete_compression_suffix
from iota2.common.custom_numpy_features import (
    ChunkParameters,
    CustomFeaturesConfiguration,
    CustomNumpyFeatures,
    MaskingRaster,
    compute_custom_features,
    exogenous_data_tile,
)
from iota2.common.file_utils import file_search_and
from iota2.common.generate_features import FeaturesMapParameters
from iota2.common.otb_app_bank import (
    AvailableOTBApp,
    create_application,
    get_input_parameter_output,
)
from iota2.common.raster_utils import (
    OtbPipelineCarrier,
    UserDefinedFunctionPayload,
    turn_off_otb_pipelines,
)
from iota2.common.utils import TaskConfig
from iota2.typings.i2_types import (
    CustomFeaturesParameters,
    OtbApp,
    OtbImage,
    PathLike,
    PredictionOutput,
)
from iota2.vector_tools.vector_functions import get_field_element, get_layer_name

SensorsParamsType = dict[str, str | list[str] | int]
FunctionNameWithParams = tuple[str, dict[str, Any]]


@dataclass
class ClassificationPaths:
    """
    Paths used in the classification process

    Attributes
    ----------
    model:
        the model name
    classif_mask:
        the classification mask
    stats:
        the statistics file name
    path_wd:
        working directory path for temporary data
    output_path:
        the path where final classification must be stored
    boundary_mask:
        Region mask, for the given tile
    """

    model: str | list[str]
    classif_dir: str
    classif_mask: str | None = None
    stats: str | None = None
    path_wd: str | None = None
    output_path: str | None = None
    boundary_mask: str | None = None


@dataclass
class ClassificationOptions:
    """
    Options used for classifying

    Attributes
    ----------
    classifier_type:
        the classifier name
    tile:
        the tile name to be classified
    proba_map_expected:
        enable the writing of probability map (sharkrf only)
    write_features:
        write the features stack
    all_class:
        list containing all possible labels
    force_standard_labels:
        use generic label (value_x) instead of named label (sensor_band_date)
    """

    classifier_type: str
    tile: str
    all_class: list[int]
    write_features: bool = False
    proba_map_expected: bool = False
    force_standard_labels: bool = False
    pix_type: str = ("uint8",)


LOGGER = logging.getLogger("distributed.worker")


class Iota2Classification:
    """Class for pixel classification using OTB"""

    def __init__(
        self,
        classif_options: ClassificationOptions,
        classif_paths: ClassificationPaths,
        path_wd: str | None = None,
        targeted_chunk: int | None = None,
        logger: logging.Logger = LOGGER,
    ) -> None:
        """
        Create an instance for classification

        Parameters
        ----------
        classif_paths:
            Dataclass containing paths used in the classification
        classif_options:
            Dataclass containing options used in the classification
        targeted_chunk:
            Indicate the part of image to be processed
        """
        self.classif_options = classif_options
        self.classif_paths = classif_paths
        self.logger = logger
        self.path_wd = path_wd
        if isinstance(self.classif_paths.model, list):
            self.model_name = self.get_model_name(
                str(Path(self.classif_paths.model[0]).parent)
            )
            self.seed = self.get_model_seed(
                str(Path(self.classif_paths.model[0]).parent)
            )
        else:
            self.model_name = self.get_model_name(self.classif_paths.model)
            self.seed = self.get_model_seed(self.classif_paths.model)
        sub_name = "" if targeted_chunk is None else f"_SUBREGION_{targeted_chunk}"
        classification_name = (
            f"Classif_{self.classif_options.tile}_model_{self.model_name}"
            f"_seed_{self.seed}{sub_name}.tif"
        )
        confidence_name = (
            f"{self.classif_options.tile}_model_{self.model_name}_"
            f"confidence_seed_{self.seed}{sub_name}.tif"
        )
        proba_map_name = (
            f"PROBAMAP_{self.classif_options.tile}_model_"
            f"{self.model_name}_seed_{self.seed}{sub_name}.tif"
        )
        classification = str(Path(self.classif_paths.classif_dir) / classification_name)
        confidence = str(Path(self.classif_paths.classif_dir) / confidence_name)
        proba_map_path = self.get_proba_map(
            self.classif_options.classifier_type,
            self.classif_paths.classif_dir,
            self.classif_options.proba_map_expected,
            proba_map_name,
        )
        assert classif_paths.output_path is not None
        self.output_files = PredictionOutput(
            classif=classification,
            confidence=confidence,
            proba=proba_map_path,
            output_path=classif_paths.output_path,
        )

    def get_proba_map(
        self,
        classifier_type: str,
        output_directory: str,
        gen_proba: bool,
        proba_map_name: str,
    ) -> str:
        """
        Get probability map absolute path

        Parameters
        ----------
        classifier_type :
            classifier's name (provided by OTB)
        output_directory :
            output directory
        gen_proba :
            indicate if proba map is asked
        proba_map_name :
            probability raster name
        """
        proba_map = ""
        classifier_avail = ["sharkrf"]
        if classifier_type in classifier_avail:
            proba_map = str(Path(output_directory) / proba_map_name)
        if gen_proba and proba_map == "":
            warn_mes = (
                f"classifier '{classifier_type}' not available to generate"
                f" a probability map, those available are {classifier_avail}"
            )
            self.logger.warning(warn_mes)
        return proba_map if gen_proba else ""

    @staticmethod
    def get_model_name(model: str) -> str:
        """
        from a full path return the basename of model

        Parameters
        ----------
        model:
            the model full path
        """
        model_name = Path(model).stem.split("_")[1]
        return model_name

    @staticmethod
    def get_model_seed(model: str) -> str:
        """
        From model name return the seed number
        Parameters
        ----------
        model:
            the model name
        """
        return Path(model).stem.split("_")[3]

    def apply_mask(
        self, nb_class_run: int, pix_type: str = "uint8", ram: int = 128
    ) -> None:
        """
        If a mask is provided, apply it to all products

        Parameters
        ----------
        nb_class_run:
            the number of classes read in the model file
        pix_type:
            define the pixel type for classification
        ram:
            the ram available for OTB applications
        """
        if self.classif_paths.classif_mask:
            mask_filter, _ = create_application(
                AvailableOTBApp.BAND_MATH,
                {
                    "il": [self.output_files.classif, self.classif_paths.classif_mask],
                    "ram": str(ram),
                    "pixType": pix_type,
                    "out": self.output_files.classif,
                    "exp": "im2b1>=1?im1b1:0",
                },
            )
            mask_filter.ExecuteAndWriteOutput()
            mask_filter, _ = create_application(
                AvailableOTBApp.BAND_MATH,
                {
                    "il": [
                        self.output_files.confidence,
                        self.classif_paths.classif_mask,
                    ],
                    "ram": str(ram),
                    "pixType": "float",
                    "out": self.output_files.confidence,
                    "exp": "im2b1>=1?im1b1:0",
                },
            )
            mask_filter.ExecuteAndWriteOutput()
            if self.output_files.proba:
                mask = (
                    self.classif_paths.classif_mask
                    if self.classif_paths.boundary_mask is None
                    else self.classif_paths.boundary_mask
                )
                expr = f"im2b1>=1?im1:{'{' + ';'.join(['0'] * nb_class_run) + '}'}"
                mask_filter, _ = create_application(
                    AvailableOTBApp.BAND_MATH_X,
                    {
                        "il": [self.output_files.proba, mask],
                        "ram": str(ram),
                        "pixType": "uint16",
                        "out": self.output_files.proba,
                        "exp": expr,
                    },
                )
                mask_filter.ExecuteAndWriteOutput()

    def clean_working_directory(self) -> None:
        """
        Move classification from working directory to output_path
        Then remove all temporary data
        """
        if self.path_wd:
            shutil.copy(
                self.output_files.classif,
                str(
                    Path(self.classif_paths.classif_dir)
                    / Path(self.output_files.classif).name
                ),
            )
            shutil.copy(
                self.output_files.confidence,
                str(
                    Path(self.classif_paths.classif_dir)
                    / Path(self.output_files.confidence).name
                ),
            )
            if self.output_files.proba:
                shutil.copy(
                    self.output_files.proba,
                    str(
                        Path(self.classif_paths.classif_dir)
                        / Path(self.output_files.proba).name
                    ),
                )

    def generate(
        self,
        features_stack: OtbApp | OtbImage | str,
        models_class: dict[str, dict[int, list[int]]],
        pix_type: str = "uint8",
        ram: int = 128,
        external_features_flag: bool = False,
    ) -> None:
        """
        Create and execute all applications to produce classification

        Parameters
        ----------
        features_stack: str
            the time series to be classified
        models_class:
            Dictionary informing which class will be used by models
        pix_type:
            define the pixel type for classification
        ram:
            the ram available for OTB applications
        external_features_flag:
            enable the use of external features
        """
        if self.path_wd:
            self.output_files.classif = str(
                Path(self.path_wd) / Path(self.output_files.classif).name
            )
            self.output_files.confidence = str(
                Path(self.path_wd) / Path(self.output_files.confidence).name
            )

        classifier_options = {
            "in": features_stack,
            "model": self.classif_paths.model,
            "confmap": f"{self.output_files.confidence}?&writegeom=false",
            "ram": str(0.2 * float(ram)),
            "pixType": pix_type,
            "out": f"{self.output_files.classif}?&writegeom=false",
        }

        # Used only if probamap is asked then 0 is a good default value
        nb_class_run = 0
        if self.output_files.proba:
            nb_class_run = len(self.classif_options.all_class)
            if self.path_wd:
                self.output_files.proba = str(
                    Path(self.path_wd) / Path(self.output_files.proba).name
                )
            classifier_options[
                "probamap"
            ] = f"{self.output_files.proba}?&writegeom=false"
            classifier_options["nbclasses"] = str(nb_class_run)

        if self.classif_paths.stats:
            classifier_options["imstat"] = self.classif_paths.stats
        else:
            classifier, _ = create_application(
                AvailableOTBApp.IMAGE_CLASSIFIER, classifier_options
            )
            if external_features_flag:
                classifier.ImportVectorImage("in", features_stack)

        self.logger.info(f"Compute Classification : {self.output_files.classif}")
        self.logger.info(f"ram before classification : {fu.memory_usage_psutil()} MB")
        classifier.ExecuteAndWriteOutput()
        self.logger.info(f"ram after classification : {fu.memory_usage_psutil()} MB")
        self.logger.info(f"Classification : {self.output_files.classif} done")

        self.apply_mask(nb_class_run, pix_type, ram)
        if self.output_files.proba:
            class_model = models_class[self.model_name][int(self.seed)]
            if len(class_model) != len(self.classif_options.all_class):
                self.logger.info(
                    f"reordering the probability map : {self.output_files.proba}"
                )

                ru.reorder_proba_map(
                    self.output_files.proba,
                    self.output_files.proba,
                    class_model,
                    self.classif_options.all_class,
                    pix_type="uint16",
                )
        self.clean_working_directory()


def get_model_dictionary(model: str) -> list[int]:
    """
    From a model file, get the class dictionary
    Parameters
    ----------
    model:
        the model file
    """
    classes: list[int] = []
    with open(model, encoding="UTF-8") as modelfile:
        line = next(modelfile)
        if "#" in line and "with_dictionary" in line:
            _classes = next(modelfile).split(" ")[1:-1]
            classes = [int(x) for x in _classes]
    return classes


def get_class_by_models_from_i2_learn(
    iota2_learning_database_dir: str, label_field: str, region_field: str
) -> dict:
    """
    inform which class will be used to by models

    Parameters
    ----------
    iota2_learning_database_dir:
        folder containing learning samples vector files
    label_field:
        label name column in shapefile
    region_field:
        region name column in shapefile
    """
    # check only seed 0
    learning_files = fu.file_search_and(
        iota2_learning_database_dir, True, "seed_0_learn.sqlite"
    )
    class_model: dict = {}
    for learning_file in learning_files:
        layer_name = get_layer_name(learning_file, "SQLite")
        conn = sqlite3.connect(learning_file)
        df_train = pd.read_sql_query(
            f"select {region_field},{label_field} from {layer_name}", conn
        )
        region_labels = fu.sort_by_first_elem(list(map(list, list(df_train.values))))
        for region, labels in region_labels:
            if region not in class_model:
                class_model[region] = set()
            class_model[region] = class_model[region].union(set(labels))
    class_model_out = {}
    for region_name, region_labels in class_model.items():
        class_model_out[region_name] = sorted(list(region_labels))
    return class_model_out


def get_class_by_models(
    iota2_samples_dir: str, data_field: str, model: PathLike | None = None
) -> dict[str, dict[int, list[int]]]:
    """inform which class will be used to by models

    Parameters
    ----------
    iota2_samples_dir :
        path to the directory containing samples dedicated to learn models
    data_field :
        field which contains labels in vector file
    model :
        the model file
    Return
    ------
    dic[model][seed]

    Example
    -------
    >>> dico_models = get_class_by_models("/somewhere/learningSamples", "code")
    >>> print dico_models["1"][0]
    >>> [11, 12, 31]
    """
    class_models: dict[str, dict[int, list[int]]] = {}
    if model is not None:
        modelpath = str(Path(model).parent)
        models_files = file_search_and(modelpath, True, "model", "seed", ".txt")

        for model_file in models_files:
            model_name = Path(model_file).stem.split("_")[1]
            class_models[model_name] = {}

        for model_file in models_files:
            model_name = Path(model_file).stem.split("_")[1]
            seed_number = int(Path(model_file).stem.split("_")[3].replace(".txt", ""))
            classes = get_model_dictionary(model_file)
            class_models[model_name][seed_number] = classes
    else:
        samples_files: list[str] = file_search_and(
            iota2_samples_dir, True, "Samples_region_", "_seed", "_learn.sqlite"
        )

        for samples_file in samples_files:
            model_name = Path(samples_file).stem.split("_")[2]
            class_models[model_name] = {}
            seed_number = int(Path(samples_file).stem.split("_")[3].replace("seed", ""))
            seed_class = get_field_element(
                samples_file, elem_type=int, field=data_field.lower(), mode="unique"
            )
            assert seed_class
            class_models[model_name][seed_number] = sorted(seed_class)
    return class_models


def launch_classification(
    classif_paths: ClassificationPaths,
    classif_options: ClassificationOptions,
    data_field: str,
    sensors_parameters: SensorsParamsType,
    multi_param_cust: dict,
    custom_features_parameters: CustomFeaturesParameters,
    path_wd: str | None = None,
    chunk_config: ru.ChunkConfig | None = None,
    targeted_chunk: int | None = None,
    task_config: TaskConfig = TaskConfig(ram=500, logger=LOGGER),
) -> None:
    """
    Launch the classification

    Parameters
    ----------
    classif_paths:
        Dataclass containing paths used in the classification
    classif_options:
        Dataclass containing options used in the classification
    data_field:
        the column name for class labels
    sensors_parameters:
        dictionary with all sensors information
    pix_type:
        the output pixel type
    multi_param_cust
        custom features parameters
    custom_features_parameters:
        Parameters concerning the custom features process
    chunk_config:
        number_of_chunks: the number of chunks required
        chunk_size_mode: the image split mode
        chunk_size_x: the number of pixel in column for extracted chunk
        chunk_size_y: the number of pixel in row for extracted chunk
    targeted_chunk:
        indicate which chunk must be processed
    task_config:
        Dataclass containing ram and logger

    Notes
    -----

    - Several parameter are activated only if external_features is set to true:
        - module_path
        - list_functions
        - number_of_chunks
        - chunk_size_mode
        - chunk_size_x
        - chunk_size_y
        - targeted_chunk
        - concat_mode

    - chunk_size_mode has two value and requires different parameters for each

    +-------------------+-----------------------+
    | Parameter         | chunk_size_mode value |
    +-------------------+-----------------------+
    | chunk_size_x      | user_fixed            |
    +-------------------+-----------------------+
    | chunk_size_y      | user_fixed            |
    +-------------------+-----------------------+
    | number_of_chunks  | split_number          |
    +-------------------+-----------------------+
    | targeted_chunk    | both                  |
    +-------------------+-----------------------+

    - concat_mode:
        If enabled the external features are concatenated with all iota2
        features (reflectance, NDVI, NDWI,...)
        If disabled only the external features are used
    """
    assert classif_paths.output_path
    working_dir = str(
        Path(classif_paths.output_path) / "features" / classif_options.tile
    )

    if path_wd:
        working_dir = str(Path(path_wd) / classif_options.tile)
        if not Path(working_dir).exists():
            try:
                Path(working_dir).mkdir()
            except OSError:
                task_config.logger.warning(f"{working_dir} already exists")

    features_map_parameters = FeaturesMapParameters(
        working_directory=working_dir,
        tile=classif_options.tile,
        output_path=classif_paths.output_path,
        sensors_parameters=sensors_parameters,
        force_standard_labels=classif_options.force_standard_labels,
    )
    otb_pipelines, _ = genFeatures.generate_features(
        features_map_params=features_map_parameters,
        logger=task_config.logger,
    )
    chunked_mask = None
    if custom_features_parameters.enable:
        assert chunk_config
        data = prepare_custom_features_classif(
            custom_features_parameters,
            multi_param_cust,
            otb_pipelines,
            classif_options,
            chunk_config,
            targeted_chunk,
            sensors_parameters,
            task_config,
        )
        classif_input: str | OtbImage | OtbApp | None = data.otb_img
        if classif_paths.classif_mask and targeted_chunk:
            assert classif_input and isinstance(classif_input, dict)
            chunked_mask = apply_classif_mask(
                classif_paths.classif_mask,
                classif_input,
                targeted_chunk,
                task_config.ram,
                working_dir,
            )
            classif_paths.classif_mask = chunked_mask

    else:
        assert otb_pipelines.interpolated_pipeline
        all_features: OtbApp = otb_pipelines.interpolated_pipeline
        feature_raster = delete_compression_suffix(
            all_features.GetParameterValue(get_input_parameter_output(all_features))
        )
        if classif_options.write_features:
            if not Path(feature_raster).exists():
                all_features.ExecuteAndWriteOutput()
            classif_input = feature_raster
        else:
            all_features.Execute()
            classif_input = all_features

    assert isinstance(classif_paths.model, str)
    models_class = get_class_by_models(
        str(Path(classif_paths.output_path) / "learningSamples"),
        data_field,
        model=classif_paths.model if classif_options.proba_map_expected else None,
    )
    classif = Iota2Classification(
        classif_options=classif_options,
        classif_paths=classif_paths,
        targeted_chunk=targeted_chunk,
        logger=task_config.logger,
    )
    assert classif_input
    classif.generate(
        features_stack=classif_input,
        models_class=models_class,
        pix_type=classif_options.pix_type,
        ram=task_config.ram,
        external_features_flag=custom_features_parameters.enable,
    )
    if chunked_mask:
        Path(chunked_mask).unlink()


def prepare_custom_features_classif(
    custom_features_parameters: CustomFeaturesParameters,
    multi_param_cust: dict,
    otb_pipelines: OtbPipelineCarrier,
    classif_options: ClassificationOptions,
    chunk_config: ru.ChunkConfig,
    targeted_chunk: int | None,
    sensors_parameters: SensorsParamsType,
    task_config: TaskConfig,
) -> UserDefinedFunctionPayload:
    """
    Prepare classification inputs when using custom numpy features

    Parameters
    ----------
    custom_features_parameters: CustomFeaturesParameters
        Parameters concerning the custom features process
    multi_param_cust: dict
        Custom features parameters
    otb_pipelines: OtbPipelineCarrier
        blabla
    classif_options: ClassificationOptions
        Dataclass containing options used in the classification
    chunk_config: ru.ChunkConfig
        Chunk configuration
    targeted_chunk: int | None
        Indicate which chunk must be processed
    sensors_parameters: SensorsParamsType
        Dictionary with all sensors information
    task_config: TaskConfig
        Dataclass containing ram and logger

    Returns
    -------
    data: UserDefinedFunctionPayload
        Data obtained from a user defined function
    """
    exogeneous_data = exogenous_data_tile(
        multi_param_cust["exogeneous_data"], classif_options.tile
    )
    assert custom_features_parameters.functions
    cust = CustomNumpyFeatures(
        sensors_params=sensors_parameters,
        module_name=custom_features_parameters.module,
        list_functions=custom_features_parameters.functions,
        configuration=CustomFeaturesConfiguration(
            concat_mode=custom_features_parameters.concat_mode,
            enabled_raw=multi_param_cust["enable_raw"],
            enabled_gap=multi_param_cust["enable_gap"],
            fill_missing_dates=multi_param_cust["fill_missing_dates"],
        ),
        all_dates_dict=multi_param_cust["all_dates_dict"],
        exogeneous_data_file=exogeneous_data,
    )
    otb_pipelines = turn_off_otb_pipelines(
        otb_pipelines,
        turn_off_raw=multi_param_cust["enable_raw"] is False,
        turn_off_interpolated=multi_param_cust["enable_gap"] is False,
    )
    data, _ = compute_custom_features(
        functions_builder=cust,
        otb_pipelines=otb_pipelines,
        feat_labels=[],
        chunk_params=ChunkParameters(chunk_config, targeted_chunk),
        concatenate_features=custom_features_parameters.concat_mode,
        exogeneous_data=exogeneous_data,
        masking_raster=MaskingRaster(None, 0),
        logger=task_config.logger,
    )
    return data


def apply_classif_mask(
    classif_mask: str,
    classif_input: OtbImage,
    targeted_chunk: int,
    ram: int,
    working_dir: str,
) -> str:
    """
    Apply the classification mask (using an ROI otb app)

    Parameters
    ----------
    classif_mask:
        Path to the classification mask
    classif_input:
        Classification input as an otb image
    targeted_chunk:
        Number of the selected chunk
    ram:
        Available ram for otb application
    working_dir:
        Working directory

    Returns
    -------
    chunked_mask: str
        Path to the chunked mask
    """
    chunked_mask = str(Path(working_dir) / f"Chunk_{targeted_chunk}_classif_mask.tif")
    roi_param = {
        "in": classif_mask,
        "out": chunked_mask,
        "mode": "fit",
        "mode.fit.im": classif_input,
        "ram": ram,
    }
    roi_mask, _ = create_application(AvailableOTBApp.EXTRACT_ROI, roi_param)
    roi_mask.ExecuteAndWriteOutput()
    roi_mask = None
    return chunked_mask
