#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""The vector Samples Merge module"""
import os
import ogr
import osr
import shutil
import logging
import argparse
from logging import Logger
from typing import List, Optional
from iota2.Common import FileUtils as fu
from iota2.VectorTools import vector_functions as vf
from collections import OrderedDict

LOGGER = logging.getLogger("distributed.worker")


def split_vectors_by_regions(path_list: List[str]) -> List[str]:
    """
    split vectors by regions
    IN:
        path_list [list(str)]: list of path
    OUT:
        list[str] : the list of vector by region
    """
    regions_position = 2
    seed_position = 3

    output = []
    seed_vector_ = fu.sortByFirstElem([
        (os.path.split(vec)[-1].split("_")[seed_position], vec)
        for vec in path_list
    ])
    seed_vector = [seed_vector for seed, seed_vector in seed_vector_]

    for current_seed in seed_vector:
        region_vector = [(os.path.split(vec)[-1].split("_")[regions_position],
                          vec) for vec in current_seed]
        region_vector_sorted_ = fu.sortByFirstElem(region_vector)
        region_vector_sorted = [
            r_vectors for region, r_vectors in region_vector_sorted_
        ]
        for seed_vect_region in region_vector_sorted:
            output.append(seed_vect_region)
    return output


def tile_vectors_to_models(iota2_learning_samples_dir: str) -> List[str]:
    """
    use to feed vector_samples_merge function

    Parameters
    ----------
    iota2_learning_samples_dir : string
        path to "learningSamples" iota² directory
    sep_sar_opt : bool
        flag use to inform if SAR data has to be computed separately

    Return
    ------
    list
        list of list of vectors to be merged to form a vector by model
    """
    vectors = fu.FileSearch_AND(iota2_learning_samples_dir, True,
                                "Samples_learn.sqlite")
    vectors_sar = fu.FileSearch_AND(iota2_learning_samples_dir, True,
                                    "Samples_SAR_learn.sqlite")

    vect_to_model = split_vectors_by_regions(
        vectors) + split_vectors_by_regions(vectors_sar)
    return vect_to_model


def check_duplicates(sqlite_file: str,
                     logger: Optional[Logger] = LOGGER) -> None:
    """
    check_duplicates
    Parameters
    ----------
    sqlite_file : string
        the input sqlite file
    Return
    ------
    None
    """
    import sqlite3 as lite
    conn = lite.connect(sqlite_file)
    cursor = conn.cursor()
    sql_clause = ("select * from output where ogc_fid in (select min(ogc_fid)"
                  " from output group by GEOMETRY having count(*) >= 2);")
    cursor.execute(sql_clause)
    results = cursor.fetchall()

    if results:
        sql_clause = (
            "delete from output where ogc_fid in (select min(ogc_fid)"
            " from output group by GEOMETRY having count(*) >= 2);")
        cursor.execute(sql_clause)
        conn.commit()
        logger.warning(f"{len(results)} were removed in {sqlite_file}")


def clean_repo(output_path: str, logger: Optional[Logger] = LOGGER):
    """
    remove from the directory learningSamples all unnecessary files
    """
    learning_content = os.listdir(output_path + "/learningSamples")
    for c_content in learning_content:
        c_path = output_path + "/learningSamples/" + c_content
        if os.path.isdir(c_path):
            try:
                shutil.rmtree(c_path)
            except OSError:
                logger.debug(f"{c_path} does not exists")


def is_sar(path: str, sar_pos: Optional[int] = 5) -> bool:
    """
    Check if the input image is a SAR product
    Parameters
    ----------
    path: string
        the input image
    Return
    bool
    ------
    """
    return os.path.basename(path).split("_")[sar_pos] == "SAR"


def prod_dummy_vector(output_dummy: str, vector_list: List[str],
                      sensors_order: List[str]):
    """TODO : unittest
    """
    no_features_fields = []
    features_fields = OrderedDict()
    masks = OrderedDict()

    for sensor in sensors_order:
        features_fields[sensor] = []
        masks[sensor] = []

    for vector in vector_list:
        # fields = vf.getFields(vector, "SQLite")
        driver = ogr.GetDriverByName("SQLite")
        ds = driver.Open(vector, 0)
        lyr = ds.GetLayer()
        lyr_dfn = lyr.GetLayerDefn()

        for field_num in range(lyr_dfn.GetFieldCount()):
            field_name = lyr_dfn.GetFieldDefn(field_num).GetName()
            field_width = lyr_dfn.GetFieldDefn(field_num).GetWidth()
            field_type = lyr_dfn.GetFieldDefn(field_num).GetType()
            field_prec = lyr_dfn.GetFieldDefn(field_num).GetPrecision()
            field_type_name = lyr_dfn.GetFieldDefn(field_num).GetTypeName()

            try:
                sensors_name, feat, date = field_name.split("_")
                new_field = (field_name, field_width, field_type, field_prec,
                             field_type_name, int(date))
                if feat.lower() == "mask":
                    if new_field not in masks[sensors_name]:
                        masks[sensors_name].append(new_field)
                else:
                    if new_field not in features_fields[sensors_name]:
                        features_fields[sensors_name].append(new_field)
            except ValueError:
                new_field = (field_name, field_width, field_type, field_prec,
                             field_type_name, "")
                if new_field not in no_features_fields:
                    no_features_fields.append(new_field)

    # features_fields = sorted(features_fields, key=lambda x: x[-1])
    all_fields = no_features_fields
    for sensor_name, fields in features_fields.items():
        all_fields += sorted(fields, key=lambda x: x[-1])
    for sensor_name, fields in masks.items():
        all_fields += sorted(fields, key=lambda x: x[-1])

    driver = ogr.GetDriverByName("SQLite")
    t_ds = driver.CreateDataSource(output_dummy)
    srs = osr.SpatialReference()
    proj = int(vf.get_vector_proj(vector_list[0], "SQLite"))
    srs.ImportFromEPSG(proj)

    layer = t_ds.CreateLayer("output", srs, ogr.wkbPoint)

    for field_name, field_width, field_type, field_prec, field_type_name, _ in all_fields:
        if field_type_name == "Real":
            nfield_type = ogr.OFTReal
        if field_type_name == "Integer":
            nfield_type = ogr.OFTInteger
        if field_type_name == "String":
            nfield_type = ogr.OFTString
        field_name = ogr.FieldDefn(field_name, nfield_type)
        layer.CreateField(field_name)


def vector_samples_merge(vector_list: List[str],
                         output_path: str,
                         features_from_raw_dates: Optional[bool] = False,
                         sensors_order: Optional[List[str]] = [],
                         logger: Optional[Logger] = LOGGER) -> None:
    """

    Parameters
    ----------
    vector_list : List[string]
    output_path : string
    Return
    ------

    """
    regions_position = 2
    seed_position = 3

    clean_repo(output_path, logger=logger)

    current_model = os.path.split(
        vector_list[0])[-1].split("_")[regions_position]
    seed = os.path.split(vector_list[0])[-1].split("_")[seed_position].replace(
        "seed", "")

    shape_out_name = (f"Samples_region_{current_model}_seed{seed}_learn")

    if is_sar(vector_list[0]):
        shape_out_name = shape_out_name + "_SAR"

    logger.info(f"Vectors to merge in {shape_out_name}")
    logger.info("\n".join(vector_list))

    vector_list = list(filter(os.path.exists, vector_list))

    if features_from_raw_dates:
        dummy_db = vector_list[0].replace(".sqlite", "_dummy.sqlite")
        prod_dummy_vector(dummy_db, vector_list, sensors_order)
        vector_list.insert(0, dummy_db)

    vf.mergeSQLite(shape_out_name,
                   os.path.join(output_path, "learningSamples"), vector_list)
    if features_from_raw_dates:
        os.remove(dummy_db)

    check_duplicates(os.path.join(os.path.join(output_path, "learningSamples"),
                                  shape_out_name + ".sqlite"),
                     logger=logger)


if __name__ == "__main__":

    PARSER = argparse.ArgumentParser(
        description="This function merge sqlite to perform training step")
    PARSER.add_argument("-output_path",
                        help="path to output directory (mandatory)",
                        dest="output_path",
                        required=True)
    PARSER.add_argument("-vector_list",
                        nargs='+',
                        help="list of vectorFiles to merge (mandatory)",
                        dest="vector_list",
                        required=True)

    ARGS = PARSER.parse_args()
    vector_samples_merge(ARGS.vector_list, ARGS.output_path)
