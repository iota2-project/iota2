#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
The Sentinel_2_L3A class
"""
import glob
import logging
import multiprocessing as mp
import shutil
from pathlib import Path
from typing import Any

from osgeo.gdal import Warp
from osgeo.gdalconst import GDT_Int16

from iota2.common.compression_options import delete_compression_suffix
from iota2.common.file_utils import (
    date_interval,
    file_search_and,
    get_date_s2,
    get_iota2_project_dir,
    get_raster_projection_epsg,
    get_raster_resolution,
    is_dir_empty,
)
from iota2.common.otb_app_bank import (
    AvailableOTBApp,
    compute_user_features,
    create_application,
    execute_app,
)
from iota2.configuration_files import read_config_file as rcf
from iota2.learning.utils import I2TemporalLabel
from iota2.typings.i2_types import I2FeaturesPipeline, OtbApp, OtbDep

LOGGER = logging.getLogger("distributed.worker")

# in order to avoid issue 'No handlers could be found for logger...'
LOGGER.addHandler(logging.NullHandler())


class Sentinel2L3a:
    """
    The Sentinel_2_L3A class
    """

    name = "Sentinel2L3A"
    nodata_value = -10000

    def __init__(
        self,
        tile_name: str,
        target_proj: int,
        all_tiles: str,
        image_directory: str,
        write_dates_stack: bool,
        extract_bands_flag: bool,
        output_target_dir: str | None,
        keep_bands: list,
        i2_output_path: str,
        temporal_res: int,
        auto_date_flag: bool,
        date_interp_min_user: str,
        date_interp_max_user: str,
        write_outputs_flag: bool,
        features: list,
        enable_gapfilling: bool,
        hand_features_flag: bool,
        hand_features: str,
        copy_input: bool,
        rel_refl: bool,
        keep_dupl: bool,
        acor_feat: bool,
        **kwargs: dict,
    ):
        """
        Build the Sentinel_2_L3A class
        """

        # running attributes
        self.tile_name = tile_name
        self.target_proj = target_proj
        self.all_tiles = all_tiles
        self.s2_l3a_data = image_directory
        self.tile_directory = str(Path(self.s2_l3a_data) / tile_name)
        self.write_dates_stack = write_dates_stack
        self.extract_bands_flag = extract_bands_flag
        self.keep_bands = keep_bands
        self.i2_output_path = i2_output_path
        self.temporal_res = temporal_res
        self.auto_date_flag = auto_date_flag
        self.date_interp_min_user = date_interp_min_user
        self.date_interp_max_user = date_interp_max_user
        self.write_outputs_flag = write_outputs_flag
        self.features = features
        self.enable_gapfilling = enable_gapfilling
        self.hand_features_flag = hand_features_flag
        self.hand_features = hand_features
        self.copy_input = copy_input
        self.rel_refl = rel_refl
        self.keep_dupl = keep_dupl
        self.acor_feat = acor_feat
        self.ref_image_nodata_val = -10000

        self.features_dir = str(Path(self.i2_output_path) / "features" / tile_name)
        # self.cfg_IOTA2 = rcf.ReadConfigFile(config_path)
        cfg_sensors_file = str(
            Path(get_iota2_project_dir(), "iota2", "sensors", "sensors.cfg")
        )
        cfg_sensors = rcf.ReadInternalConfigFile(cfg_sensors_file)
        self.struct_path_data = cfg_sensors.get_param("Sentinel_2_L3A", "arbo")
        self.struct_path_masks = cfg_sensors.get_param("Sentinel_2_L3A", "arbomask")

        # attributes
        self.features_names_list = ["NDVI", "NDWI", "Brightness"]

        self.tile_directory = str(Path(self.s2_l3a_data) / tile_name)

        self.suffix = "STACK"
        self.suffix_mask = "BINARY_MASK"
        self.masks_pattern = "FLG_R1.tif"
        self.masks_values = [0, 1]  # NODATA, CLOUD
        self.date_position = 1  # if date's name split by "_"

        # outputs
        self.footprint_name = f"{self.__class__.name}_{tile_name}" "_footprint.tif"
        self.time_series_name = f"{self.__class__.name}_{tile_name}_TS.tif"
        self.time_series_gapfilling_name = (
            f"{self.__class__.name}_" f"{tile_name}_TSG.tif"
        )
        self.time_series_masks_name = f"{self.__class__.name}" f"_{tile_name}_MASKS.tif"
        self.features_names = f"{self.__class__.name}_{tile_name}_Features.tif"
        ref_image_name = f"{self.__class__.name}_{tile_name}_reference.tif"
        self.ref_image = str(
            Path(self.i2_output_path, "features", tile_name, "tmp", ref_image_name)
        )
        # define bands to get and their order
        self.stack_band_position = [
            "B2",
            "B3",
            "B4",
            "B5",
            "B6",
            "B7",
            "B8",
            "B8A",
            "B11",
            "B12",
        ]
        # TODO move into the base-class
        self.extracted_bands = None
        if extract_bands_flag:
            # TODO check every mandatory bands still selected
            # -> def check_mandatory bands() return True/False
            self.extracted_bands = [
                (band_name, band_position + 1)
                for band_position, band_name in enumerate(self.stack_band_position)
                if band_name in self.keep_bands
            ]

        # about gapFilling interpolations
        self.input_dates = f"{self.__class__.name}_{tile_name}_input_dates.txt"
        self.interpolated_dates = (
            f"{self.__class__.name}_{tile_name}" f"_interpolation_dates.txt"
        )
        if output_target_dir:
            self.output_preprocess_directory = str(Path(output_target_dir, tile_name))
            try:
                Path(self.output_preprocess_directory).mkdir(
                    parents=True, exist_ok=True
                )
            except OSError as e:
                LOGGER.warning(
                    f"Unable to create directory "
                    f"{self.output_preprocess_directory}: {e}"
                )
        else:
            self.output_preprocess_directory = self.tile_directory
        self.working_resolution = kwargs["working_resolution"]
        base_ref_images = file_search_and(self.tile_directory, True, self.masks_pattern)
        if not base_ref_images:
            raise FileNotFoundError(
                f"*{self.masks_pattern} rasters can't be found in {self.tile_directory}"
            )
        self.base_ref_image = base_ref_images[0]

        # manage 'get_raw'
        self.get_time_series_raw = self.get_time_series
        self.get_time_series_masks_raw = self.get_time_series_masks
        self.get_time_series_gapfilling_raw = self.get_time_series_gapfilling

    def get_available_dates(self) -> list[str]:
        """
        return sorted available dates
        """
        pattern = f"{self.suffix}.tif"

        stacks = sorted(
            file_search_and(self.output_preprocess_directory, True, pattern),
            key=lambda x: Path(x).name.split("_")[self.date_position].split("-")[0],
        )
        return stacks

    def get_dates_directories(self) -> list[str]:
        """call before any treatements

        TODO : add data consistency
        """
        input_dates = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]
        return input_dates

    def sort_dates_directories(self, dates_directories: list[str]) -> list[str]:
        """
        sort dates directories
        """
        return sorted(
            dates_directories,
            key=lambda x: Path(x).name.split("_")[self.date_position].split("-")[0],
        )

    def get_available_dates_masks(self) -> list[str]:
        """
        return sorted available masks
        """
        pattern = f"{self.suffix_mask}.tif"
        masks = sorted(
            file_search_and(self.output_preprocess_directory, True, pattern),
            key=lambda x: Path(x).name.split("_")[self.date_position].split("-")[0],
        )

        return masks

    def build_stack_date_name(self, date_dir: str) -> str:
        """
        build stack date name
        """
        b2_band = file_search_and(date_dir, True, "FRC_B2.tif")
        stack_bands = file_search_and(date_dir, True, f"FRC_{self.suffix}.tif")
        if b2_band:
            b2_name = Path(b2_band[0]).name
            return b2_name.replace("FRC_B2.tif", f"FRC_{self.suffix}.tif")
        if stack_bands:
            return Path(stack_bands[0]).name
        raise ValueError("cannot build stack name")

    def get_date_from_name(self, product_name: str) -> str:
        """
        get date from name
        """
        return product_name.split("_")[self.date_position].split("-")[0]

    def raster_consistent(self, raster_file: str) -> bool:
        """detect if the output stack is consistent with user demand (resolution and projection)"""
        stack_consistent = False
        stack_ref_res_x, stack_ref_res_y = get_raster_resolution(raster_file)
        stack_ref_projection = get_raster_projection_epsg(raster_file)

        same_res = True
        if self.working_resolution:
            same_res = abs(stack_ref_res_x) == abs(self.working_resolution[0]) and abs(
                stack_ref_res_y
            ) == abs(self.working_resolution[1])

        if int(stack_ref_projection) == int(self.target_proj) and same_res:
            stack_consistent = True
            out_dir_roi = Path(self.ref_image).parent

            out_dir_roi.mkdir(parents=True, exist_ok=True)

            if Path(self.ref_image).exists():
                ref_res_x, ref_res_y = get_raster_resolution(self.ref_image)
                ref_projection = get_raster_projection_epsg(self.ref_image)

                not_same_res_ref = False
                if self.working_resolution:
                    not_same_res_ref = abs(ref_res_x) != abs(
                        self.working_resolution[0]
                    ) or abs(ref_res_y) != abs(self.working_resolution[1])
                if int(ref_projection) != int(self.target_proj) or not_same_res_ref:
                    Path(self.ref_image).unlink()
                    base_ref_projection = get_raster_projection_epsg(
                        self.base_ref_image
                    )
                    base_ref_res_x, base_ref_res_y = get_raster_resolution(
                        self.base_ref_image
                    )
                    if self.working_resolution:
                        base_ref_res_x = self.working_resolution[0]
                        base_ref_res_y = self.working_resolution[1]
                    Warp(
                        self.ref_image,
                        self.base_ref_image,
                        multithread=True,
                        format="GTiff",
                        xRes=base_ref_res_x,
                        yRes=base_ref_res_y,
                        outputType=GDT_Int16,
                        srcSRS=f"EPSG:{base_ref_projection}",
                        dstSRS=f"EPSG:{self.target_proj}",
                        warpOptions=[f"INIT_DEST={self.ref_image_nodata_val}"],
                    )
            else:
                base_ref_projection = get_raster_projection_epsg(self.base_ref_image)
                base_ref_res_x, base_ref_res_y = get_raster_resolution(
                    self.base_ref_image
                )
                if self.working_resolution:
                    base_ref_res_x = self.working_resolution[0]
                    base_ref_res_y = self.working_resolution[1]
                Warp(
                    self.ref_image,
                    self.base_ref_image,
                    multithread=True,
                    format="GTiff",
                    xRes=base_ref_res_x,
                    yRes=base_ref_res_y,
                    outputType=GDT_Int16,
                    srcSRS=f"EPSG:{base_ref_projection}",
                    dstSRS=f"EPSG:{self.target_proj}",
                    warpOptions=[f"INIT_DEST={self.ref_image_nodata_val}"],
                )

        return stack_consistent

    def preprocess_date(
        self,
        date_dir: str,
        out_prepro: str,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> dict[str, OtbApp] | str:
        """
        preprocess date
        """
        # manage directories
        date_stack_name = self.build_stack_date_name(date_dir)
        logger.debug(f"preprocessing {date_dir}")
        out_stack = str(Path(date_dir) / date_stack_name)
        if out_prepro:
            date_dir_name = Path(date_dir).name
            out_dir = str(Path(out_prepro) / date_dir_name)
            Path(out_dir).mkdir(parents=True, exist_ok=True)
            out_stack = str(Path(out_dir) / date_stack_name)

        if Path(out_stack).exists():
            consistent = self.raster_consistent(out_stack)
            if consistent:
                return out_stack

        base_ref_projection = get_raster_projection_epsg(self.base_ref_image)
        base_ref_res_x, base_ref_res_y = get_raster_resolution(self.base_ref_image)
        if self.working_resolution:
            base_ref_res_x = self.working_resolution[0]
            base_ref_res_y = self.working_resolution[1]
        if not Path(self.ref_image).exists():
            Path(self.ref_image).parent.mkdir(parents=True, exist_ok=True)
            Warp(
                self.ref_image,
                self.base_ref_image,
                multithread=True,
                format="GTiff",
                xRes=base_ref_res_x,
                yRes=base_ref_res_y,
                outputType=GDT_Int16,
                srcSRS=f"EPSG:{base_ref_projection}",
                dstSRS=f"EPSG:{self.target_proj}",
                warpOptions=[f"INIT_DEST={self.ref_image_nodata_val}"],
            )
        out_stack_processing = out_stack
        if working_dir:
            out_stack_processing = str(Path(working_dir) / date_stack_name)

        # get bands
        date_bands = [
            file_search_and(date_dir, True, f"FRC_{bands_name}.tif")[0]
            for bands_name in self.stack_band_position
        ]

        # reproject / resample
        bands_proj = {}
        all_reproj = []
        for band, band_name in zip(date_bands, self.stack_band_position):
            superimp, _ = create_application(
                AvailableOTBApp.SUPERIMPOSE,
                {"inr": self.ref_image, "inm": band, "ram": str(ram)},
            )
            bands_proj[band_name] = superimp
            all_reproj.append(superimp)

        if self.write_dates_stack:
            same_proj = False
            same_res = True
            if Path(out_stack).exists():
                same_proj = int(get_raster_projection_epsg(out_stack)) == int(
                    self.target_proj
                )
                same_res = get_raster_projection_epsg(
                    out_stack
                ) == get_raster_projection_epsg(self.ref_image)

            if not Path(out_stack).exists() or same_proj is False or not same_res:
                for reproj in all_reproj:
                    reproj.Execute()
                date_stack, _ = create_application(
                    AvailableOTBApp.CONCATENATE_IMAGES,
                    {
                        "il": all_reproj,
                        "ram": str(ram),
                        "pixType": "int16",
                        "out": out_stack_processing,
                    },
                )
                date_stack.ExecuteAndWriteOutput()
                # multi_proc = mp.Process(target=execute_app, args=[date_stack])
                # multi_proc.start()
                # multi_proc.join()
                if working_dir:
                    shutil.copy(out_stack_processing, out_stack)
                    Path(out_stack_processing).unlink()

        return bands_proj if self.write_dates_stack is False else out_stack

    def build_mask_date_name(self, date_dir: str) -> str:
        """build mask date name"""
        dir_name = Path(date_dir).name
        return f"{dir_name}_{self.suffix_mask}.tif"

    def preprocess_date_masks(
        self,
        date_dir: str,
        out_prepro: str,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> str:
        """
        preprocess date masks
        """
        # manage directories
        mask_dir = str(Path(date_dir) / "MASKS")
        if is_dir_empty(mask_dir):
            logger.error(f"Mask directory {mask_dir} is empty !")
            raise FileNotFoundError(f"Mask directory {mask_dir} is empty !")
        logger.debug(f"preprocessing {mask_dir} masks")
        mask_name = self.build_mask_date_name(date_dir)
        out_mask = str(Path(mask_dir) / mask_name)
        if out_prepro:
            out_mask_dir = mask_dir.replace(
                str(Path(self.s2_l3a_data) / self.tile_name), out_prepro
            )
            Path(out_mask_dir).mkdir(parents=True, exist_ok=True)

            out_mask = str(Path(out_mask_dir) / mask_name)

            # copy flag mask
            flag_masks = file_search_and(mask_dir, True, self.masks_pattern)
            if not flag_masks:
                raise ValueError(f"cannot find mask in repository : {mask_dir}")
            flag_mask = flag_masks[0]
            flag_mask_name = Path(flag_mask).name
            out_flag = str(Path(out_mask_dir) / flag_mask_name)
            if not Path(out_flag).exists():
                shutil.copy(flag_mask, out_flag)

        if Path(out_mask).exists():
            consistent = self.raster_consistent(out_mask)
            if consistent:
                return out_mask

        out_mask_processing = out_mask
        if working_dir:
            out_mask_processing = str(Path(working_dir) / mask_name)

        date_mask = glob.glob(
            str(Path(date_dir) / f"{self.struct_path_masks}{self.masks_pattern}")
        )[0]
        # compute mask
        if not Path(out_mask).exists():
            mask_exp = "?1:".join([f"im1b1=={value}" for value in self.masks_values])
            mask_exp = f"{mask_exp}?1:0"
            mask_gen, _ = create_application(
                AvailableOTBApp.BAND_MATH,
                {
                    "il": date_mask,
                    "ram": str(ram),
                    "exp": mask_exp,
                    "pixType": "uint8",
                    "out": out_mask_processing,
                },
            )
            # mask_gen.ExecuteAndWriteOutput()
            multi_proc = mp.Process(target=execute_app, args=[mask_gen])
            multi_proc.start()
            multi_proc.join()
            if working_dir:
                shutil.copy(out_mask_processing, out_mask)
                Path(out_mask_processing).unlink()

        # reproject if needed
        mask_projection = get_raster_projection_epsg(out_mask)
        out_mask_res_x, out_mask_res_y = get_raster_resolution(out_mask)
        if self.working_resolution:
            out_mask_res_x = self.working_resolution[0]
            out_mask_res_y = self.working_resolution[1]
        same_res = get_raster_resolution(out_mask) == get_raster_resolution(
            self.ref_image
        )
        if int(self.target_proj) != int(mask_projection) or not same_res:
            logger.info(f"Reprojecting {out_mask}")
            Warp(
                out_mask_processing,
                out_mask,
                multithread=True,
                format="GTiff",
                xRes=out_mask_res_x,
                yRes=out_mask_res_y,
                srcSRS=f"EPSG:{mask_projection}",
                dstSRS=f"EPSG:{self.target_proj}",
                warpOptions=["INIT_DEST=0"],
            )

            if working_dir:
                shutil.copy(out_mask_processing, out_mask)
                Path(out_mask_processing).unlink()
            logger.info("Reprojection succeed")
        logger.info("End preprocessing")
        return out_mask

    def preprocess(
        self,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,  # pylint: disable=unused-argument
    ) -> dict[str, dict[str, dict[str, Any] | str | str]]:
        """
        preprocess
        """
        input_dates = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]
        input_dates = self.sort_dates_directories(input_dates)

        preprocessed_dates = {}
        for date in input_dates:
            data_prepro = self.preprocess_date(
                date, self.output_preprocess_directory, working_dir, ram
            )
            data_mask = self.preprocess_date_masks(
                date, self.output_preprocess_directory, working_dir, ram
            )
            current_date = self.get_date_from_name(Path(data_mask).name)
            # manage date duplicate
            if current_date in preprocessed_dates:
                current_date = f"{current_date}.1"
            preprocessed_dates[current_date] = {"data": data_prepro, "mask": data_mask}
        return preprocessed_dates

    def footprint(self, ram: int = 128) -> tuple[OtbApp, list[OtbApp]]:
        """
        in this case (L3A), we consider the whole tile
        """
        footprint_dir = str(Path(self.features_dir) / "tmp")
        Path(footprint_dir).mkdir(parents=True, exist_ok=True)
        footprint_out = str(Path(footprint_dir) / self.footprint_name)

        reference_raster = self.ref_image

        s2_l3a_border, _ = create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": reference_raster,
                "out": footprint_out,
                "exp": f"im1b1=={self.ref_image_nodata_val}?0:1",
                "pixType": "uint8",
                "ram": str(ram),
            },
        )
        # needed to travel through iota2's library
        app_dep: list = []

        return s2_l3a_border, app_dep

    def write_interpolation_dates_file(
        self, write: bool = True
    ) -> tuple[str, list[str]]:
        """
        TODO : mv to base-class
        """
        interp_date_dir = str(Path(self.features_dir) / "tmp")
        Path(interp_date_dir).mkdir(parents=True, exist_ok=True)
        interp_date_file = str(Path(interp_date_dir) / self.interpolated_dates)
        # get dates in the whole S2 data-set
        date_interp_min, date_interp_max = get_date_s2(
            self.s2_l3a_data, self.all_tiles.split(" ")
        )
        # force dates

        if not self.auto_date_flag:
            date_interp_min = self.date_interp_min_user
            date_interp_max = self.date_interp_max_user
        dates = [
            str(date).replace("-", "")
            for date in date_interval(
                date_interp_min, date_interp_max, self.temporal_res
            )
        ]
        if not Path(interp_date_file).exists() and write:

            with open(
                interp_date_file, "w", encoding="utf-8"
            ) as interpolation_date_file:
                interpolation_date_file.write("\n".join(dates))
        return interp_date_file, dates

    def write_dates_file(self) -> tuple[str, list[str]]:
        """
        write dates file
        """
        input_dates_dir = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]
        date_file = str(Path(self.features_dir) / "tmp" / self.input_dates)
        all_available_dates = [
            int(Path(date).name.split("_")[self.date_position].split("-")[0])
            for date in input_dates_dir
        ]
        all_available_dates = sorted(all_available_dates)
        all_available_dates_str = [str(x) for x in all_available_dates]
        if not Path(date_file).exists():
            with open(date_file, "w", encoding="utf-8") as input_date_file:
                input_date_file.write("\n".join(all_available_dates_str))
        return date_file, all_available_dates_str

    def get_time_series(self, ram: int = 128) -> I2FeaturesPipeline:
        """
        TODO : be able of using a date interval
        Return
        ------
            list
                [(otb_Application, some otb's objects), time_series_labels]
                Functions dealing with otb's application instance has to
                returns every objects in the pipeline
        """
        # needed to travel through iota2's library
        app_dep = []

        preprocessed_dates = self.preprocess(working_dir=None, ram=ram)

        use_stacks = False
        first_key = next(iter(preprocessed_dates))
        if isinstance(preprocessed_dates[first_key]["data"], str):
            stacks_exists = []
            for _, stack in preprocessed_dates.items():
                data_file = stack["data"]
                assert isinstance(data_file, str)
                stacks_exists.append(Path(data_file).exists())
            use_stacks = all(stacks_exists)

        if self.write_dates_stack is False and not use_stacks:
            dates_concatenation = []
            for _, dico_date in list(preprocessed_dates.items()):
                data_elem = dico_date["data"]
                assert isinstance(data_elem, dict)
                for _, reproj_date in list(data_elem.items()):
                    dates_concatenation.append(reproj_date)
                    reproj_date.Execute()
                    app_dep.append(reproj_date)
        else:
            dates_concatenation = self.get_available_dates()

        time_series_dir = str(Path(self.features_dir) / "tmp")
        Path(time_series_dir).mkdir(parents=True, exist_ok=True)
        times_series_raster = str(Path(time_series_dir) / self.time_series_name)

        dates_time_series, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {
                "il": dates_concatenation,
                "out": times_series_raster,
                "pixType": "int16",
                "ram": str(ram),
            },
        )
        _, dates_in = self.write_dates_file()
        # build labels
        features_labels = [
            I2TemporalLabel(
                sensor_name=self.__class__.name, feat_name=band_name, date=date
            )
            for date in dates_in
            for band_name in self.stack_band_position
        ]

        # if not all bands must be used
        if self.extracted_bands:
            app_dep.append(dates_time_series)
            (dates_time_series, features_labels) = self.extract_bands_time_series(
                dates_time_series,
                dates_in,
                len(self.stack_band_position),
                self.extracted_bands,
                ram,
            )
        return (dates_time_series, app_dep), features_labels

    def extract_bands_time_series(
        self,
        dates_time_series: OtbApp,
        dates_in: list[str],
        comp: int,
        extract_bands: list[tuple[str, int]],
        ram: int,
    ) -> tuple[OtbApp, I2TemporalLabel]:
        """
        TODO : mv to base class ?
        extract_bands : list
             [('bandName', band_position), ...]
        comp : number of bands in original stack
        """

        nb_dates = len(dates_in)
        channels_interest = []
        for date_number in range(nb_dates):
            for _, band_position in extract_bands:
                channels_interest.append(band_position + int(date_number * comp))

        features_labels = [
            I2TemporalLabel(
                sensor_name=self.__class__.name, feat_name=band_name, date=date
            )
            for date in dates_in
            for band_name, band_pos in extract_bands
        ]
        channels_list = [f"Channel{channel}" for channel in channels_interest]
        dates_time_series.Execute()
        extract, _ = create_application(
            AvailableOTBApp.EXTRACT_ROI,
            {
                "in": dates_time_series,
                "cl": channels_list,
                "ram": str(ram),
                "out": delete_compression_suffix(
                    dates_time_series.GetParameterString("out")
                ),
            },
        )
        return extract, features_labels

    def get_time_series_masks(
        self, ram: int = 128, logger: logging.Logger = LOGGER
    ) -> tuple[OtbApp, OtbDep, int]:
        """
        get time series masks
        """
        preprocessed_dates = self.preprocess(working_dir=None, ram=ram)

        if self.write_dates_stack is False:
            nb_available_dates = len(preprocessed_dates)
        else:
            nb_available_dates = len(self.get_available_dates())
        available_masks = self.get_available_dates_masks()

        if nb_available_dates != len(available_masks):

            error = (
                f"Available dates ({nb_available_dates}) and available "
                f" masks ({len(available_masks)}) are different"
            )
            logger.error(error)
            raise Exception(error)

        time_series_dir = str(Path(self.features_dir) / "tmp")
        Path(time_series_dir).mkdir(parents=True, exist_ok=True)
        times_series_masks_raster = str(
            Path(time_series_dir, self.time_series_masks_name)
        )
        dates_time_series, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {
                "il": available_masks,
                "out": times_series_masks_raster,
                "pixType": "int16",
                "ram": str(ram),
            },
        )
        dep: list = []
        return dates_time_series, dep, len(available_masks)

    def get_time_series_gapfilling(self, ram: int = 128) -> I2FeaturesPipeline:
        """
        get time series gapfilling
        """
        gap_dir = str(Path(self.features_dir) / "tmp")
        Path(gap_dir).mkdir(parents=True, exist_ok=True)
        gap_out = str(Path(gap_dir) / self.time_series_gapfilling_name)

        _, dates_in = self.write_dates_file()
        masks, masks_dep, _ = self.get_time_series_masks()
        (time_series, time_series_dep), _ = self.get_time_series()

        time_series.Execute()
        masks.Execute()
        # time_series.ExecuteAndWriteOutput()
        # masks.ExecuteAndWriteOutput()
        comp = (
            len(self.stack_band_position)
            if not self.extracted_bands
            else len(self.extracted_bands)
        )

        # no temporal interpolation (only cloud)
        gap, _ = create_application(
            AvailableOTBApp.IMAGE_TIME_SERIES_GAPFILLING,
            {
                "in": time_series,
                "mask": masks,
                "comp": str(comp),
                "it": "linear",
                "out": gap_out,
                "ram": str(ram),
                "pixType": "int16",
            },
        )
        app_dep = [time_series, masks, masks_dep, time_series_dep]

        bands = self.stack_band_position
        if self.extracted_bands:
            bands = [band_name for band_name, band_pos in self.extracted_bands]

        features_labels = [
            I2TemporalLabel(
                sensor_name=self.__class__.name, feat_name=band_name, date=date
            )
            for date in dates_in
            for band_name in bands
        ]
        return (gap, app_dep), features_labels

    def get_features_labels(
        self, dates: list[str], rel_refl: bool, keep_dupl: bool, copy_in: bool
    ) -> list[I2TemporalLabel]:
        """
        get features labels
        """
        if rel_refl and keep_dupl is False and copy_in is True:
            self.features_names_list = ["NDWI", "Brightness"]
        out_labels = []

        for feature in self.features_names_list:
            for date in dates:
                out_labels.append(
                    I2TemporalLabel(
                        sensor_name=self.__class__.name, feat_name=feature, date=date
                    )
                )
        return out_labels

    # pylint: disable=unused-argument; logger is used in user_features
    def get_features(
        self, ram: int = 128, logger: logging.Logger = LOGGER
    ) -> I2FeaturesPipeline:
        """
        get features
        """
        features_dir = str(Path(self.features_dir) / "tmp")
        Path(features_dir).mkdir(parents=True, exist_ok=True)
        features_out = str(Path(features_dir) / self.features_names)

        (
            (in_stack, in_stack_dep),
            in_stack_features_labels,
        ) = self.get_time_series_gapfilling()
        _, dates_enabled = self.write_dates_file()

        if not self.enable_gapfilling:
            (in_stack, in_stack_dep), in_stack_features_labels = self.get_time_series()

        in_stack.Execute()

        app_dep = []
        if self.hand_features_flag:
            hand_features = self.hand_features
            comp = (
                len(self.stack_band_position)
                if not self.extracted_bands
                else len(self.extracted_bands)
            )
            (
                user_date_features,
                fields_user_feat,
                user_feat_date,
                stack,
            ) = compute_user_features(
                in_stack, dates_enabled, comp, hand_features.split(",")
            )
            user_date_features.Execute()
            app_dep.append([user_date_features, user_feat_date, stack])

        if self.features:
            bands_avail = self.stack_band_position
            if self.extracted_bands:
                bands_avail = [band_name for band_name, _ in self.extracted_bands]
                # check mandatory bands
                if "B4" not in bands_avail:
                    raise Exception("red band (B4) is needed to compute features")
                if "B8" not in bands_avail:
                    raise Exception("nir band (B8) is needed to compute features")
                if "B11" not in bands_avail:
                    raise Exception("swir band (B11) is needed to compute features")
            feat_parameters = {
                "in": in_stack,
                "out": features_out,
                "comp": len(bands_avail),
                "red": bands_avail.index("B4") + 1,
                "nir": bands_avail.index("B8") + 1,
                "swir": bands_avail.index("B11") + 1,
                "copyinput": self.copy_input,
                "relrefl": self.rel_refl,
                "keepduplicates": self.keep_dupl,
                "acorfeat": self.acor_feat,
                "pixType": "int16",
                "ram": str(ram),
            }

            features_app, _ = create_application(
                AvailableOTBApp.IOTA2_FEATURES_EXTRACTION, feat_parameters
            )
            if self.copy_input is False:
                in_stack_features_labels = []
            features_labels = in_stack_features_labels + self.get_features_labels(
                dates_enabled, self.rel_refl, self.keep_dupl, self.copy_input
            )
        else:
            features_app = in_stack
            features_labels = in_stack_features_labels

        app_dep.append([in_stack, in_stack_dep])

        if self.hand_features_flag:
            features_app.Execute()
            app_dep.append(features_app)

            features_app, _ = create_application(
                AvailableOTBApp.CONCATENATE_IMAGES,
                {
                    "il": [features_app, user_date_features],
                    "out": features_out,
                    "ram": str(ram),
                },
            )
            features_labels += fields_user_feat
        return (features_app, app_dep), features_labels
