# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Define functions for pre-processing, common mask and pixel validity (first steps of the chain)
"""
import logging
import shutil
from pathlib import Path
from typing import Any

import geopandas as gpd

import iota2.configuration_files.read_config_file as rcf
from iota2.common.compression_options import delete_compression_suffix
from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.common.utils import run
from iota2.sensors.sensorscontainer import SensorsContainer
from iota2.vector_tools.vector_functions import erode_shapefile, remove_shape

LOGGER = logging.getLogger("distributed.worker")


def preprocess(
    tile_name: str,
    config_path: str,
    output_path: str,
    working_directory: str | None = None,
    ram: int = 128,
    logger: logging.Logger = LOGGER,
) -> None:
    """Preprocessing input rasters data by tile.

    Parameters
    ----------
    tile_name [string]
        tile's name
    config_path [string]
        absolute path to the configuration file
    output_path : str
        iota2 output path
    working_directory [string]
        absolute path to a working directory
    ram [int]
        pipeline's size (Mo)
    """
    logger.debug(f"pre-processing {tile_name}")
    running_parameters = rcf.Iota2Parameters(rcf.ReadConfigFile(config_path))
    sensors_parameters = running_parameters.get_sensors_parameters(tile_name)

    remote_sensor_container = SensorsContainer(
        tile_name, working_directory, output_path, **sensors_parameters
    )
    remote_sensor_container.sensors_preprocess(available_ram=ram, logger=logger)


def common_mask(
    tile_name: str,
    output_path: str,
    sensors_parameters: dict[str, Any],
    working_directory: str | None,
    ram: int = 128,
    logger: logging.Logger = LOGGER,
) -> None:
    """Compute common mask considering all sensors by tile.

    Parameters
    ----------
    tile_name [string]
        tile's name
    config_path [string]
        absolute path to the configuration file
    output_path : str
        iota2 output path
    working_directory [string]
        absolute path to a working directory
    ram [int]
        pipeline's size (Mo)
    """
    logger.info(f"{tile_name} common masks generation")
    remote_sensor_container = SensorsContainer(
        tile_name, working_directory, output_path, **sensors_parameters
    )
    common_mask_app, _ = remote_sensor_container.get_common_sensors_footprint(
        available_ram=ram
    )
    common_mask_raster = delete_compression_suffix(
        common_mask_app.GetParameterValue("out")
    )
    common_mask_vector = common_mask_raster.replace(".tif", ".shp")
    Path(common_mask_raster).parent.mkdir(parents=True, exist_ok=True)

    common_mask_app.ExecuteAndWriteOutput()

    common_mask_vector_cmd = (
        'gdal_polygonize.py -f "ESRI Shapefile" -mask '
        f"{common_mask_raster} {common_mask_raster} {common_mask_vector}"
    )
    run(common_mask_vector_cmd, logger=logger)

    df = gpd.read_file(common_mask_vector)
    df3 = df
    df3.geometry = df.simplify(100)
    df3.to_file(common_mask_vector)


def validity(
    tile_name: str,
    config_path: str,
    output_path: str,
    maskout_name: str,
    view_threshold: int,
    working_directory: str,
    ram: int = 128,
    logger: logging.Logger = LOGGER,
) -> None:
    """Compute validity raster/vector by tile.

    Parameters
    ----------
    tile_name [string]
        tile's name
    config_path [string]
        absolute path to the configuration file
    maskout_name [string]
        output vector mask's name
    view_threshold [int]
        threshold
    working_directory [string]
        absolute path to a working directory
    ram [int]
        pipeline's size (Mo)
    """
    features_dir = str(Path(output_path) / "features" / tile_name)
    validity_name = "nbView.tif"

    validity_out = str(Path(features_dir) / validity_name)
    validity_processing = validity_out
    if working_directory:
        (Path(working_directory) / tile_name).mkdir(parents=True, exist_ok=True)
        validity_processing = str(Path(working_directory) / tile_name / validity_name)

    running_parameters = rcf.Iota2Parameters(rcf.ReadConfigFile(config_path))
    sensors_parameters = running_parameters.get_sensors_parameters(tile_name)
    remote_sensor_container = SensorsContainer(
        tile_name, working_directory, output_path, **sensors_parameters
    )

    sensors_time_series_masks = remote_sensor_container.get_sensors_time_series_masks(
        available_ram=ram
    )
    sensors_masks_size = []
    sensors_masks = []
    for sensor_name, (
        time_series_masks,
        _,
        nb_bands,
    ) in sensors_time_series_masks:
        if sensor_name.lower() == "sentinel1":
            for _, time_series_masks_app in list(time_series_masks.items()):
                time_series_masks_app.Execute()
                sensors_masks.append(time_series_masks_app)
        else:
            time_series_masks.Execute()
            sensors_masks.append(time_series_masks)
        sensors_masks_size.append(nb_bands)

    total_dates = sum(sensors_masks_size)
    merge_masks, _ = create_application(
        AvailableOTBApp.CONCATENATE_IMAGES, {"il": sensors_masks, "ram": str(ram)}
    )
    merge_masks.Execute()

    validity_app, _ = create_application(
        AvailableOTBApp.BAND_MATH,
        {
            "il": merge_masks,
            "exp": f"{total_dates}-({'+'.join([f'im1b{i + 1}' for i in range(total_dates)])})",
            "ram": str(0.7 * ram),
            "pixType": "uint8" if total_dates < 255 else "uint16",
            "out": validity_processing,
        },
    )
    validity_app.ExecuteAndWriteOutput()
    if working_directory:
        shutil.copy(validity_processing, str(Path(features_dir, validity_name)))
    threshold_raster_out = str(Path(features_dir, maskout_name.replace(".shp", ".tif")))
    threshold_vector_out_tmp = str(
        Path(features_dir, maskout_name.replace(".shp", "_TMP.shp"))
    )
    threshold_vector_out = str(Path(features_dir) / maskout_name)

    input_threshold = (
        validity_processing if Path(validity_processing).exists() else validity_out
    )

    threshold_raster, _ = create_application(
        AvailableOTBApp.BAND_MATH,
        {
            "il": input_threshold,
            "exp": f"im1b1>={view_threshold}?1:0",
            "ram": str(0.7 * ram),
            "pixType": "uint8",
            "out": threshold_raster_out,
        },
    )
    threshold_raster.ExecuteAndWriteOutput()
    cmd_poly = (
        f"gdal_polygonize.py -mask {threshold_raster_out} {threshold_raster_out} "
        f'-f "ESRI Shapefile" {threshold_vector_out_tmp} '
        f"{Path(threshold_vector_out_tmp).stem} cloud"
    )
    run(cmd_poly, logger=logger)

    erode_shapefile(threshold_vector_out_tmp, threshold_vector_out, 0.1)
    Path(threshold_raster_out).unlink()
    remove_shape(threshold_vector_out_tmp)
