#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Set of functions to deal with SAR files and dates
"""
import configparser
import logging
import re
from collections.abc import Generator
from pathlib import Path

import otbApplication as otb

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils, otb_app_bank
from iota2.common.compression_options import delete_compression_suffix
from iota2.typings.i2_types import OtbApp

LOGGER = logging.getLogger("distributed.worker")
S1_CONST = i2_const.S1Constants()


def get_ortho(ortho_list: list, pattern: str) -> Generator:
    """
    Filters files based on provided regex pattern

    Parameters
    ----------
    ortho_list:
        list of files
    pattern:
        regex pattern (for example: "s1b(.*)ASC(.*)tif")
    """
    for ortho in ortho_list:
        name = delete_compression_suffix(Path(ortho).name)
        compiled = re.compile(pattern)
        search = compiled.search(name)
        try:
            assert search
            search.group(1).strip()
            yield ortho
        except (AssertionError, IndexError):
            continue


def get_dates_in_otb_output_name(otb_obj: str | OtbApp) -> int:
    """
    Gets date from OTB output
    """
    if isinstance(otb_obj, str):
        return int(otb_obj.split("/")[-1].split("_")[4].split("t")[0])
    if isinstance(otb_obj, otb.Application):
        output_parameter = otb_app_bank.get_input_parameter_output(otb_obj)
        return int(
            delete_compression_suffix(otb_obj.GetParameterValue(output_parameter))
            .rsplit("/", maxsplit=1)[-1]
            .split("_")[4]
            .split("t")[0]
        )
    raise TypeError(f"Object type {type(otb_obj)} is incorrect")


def compare_dates(dates_file: str, dates: list[str]) -> list[str]:
    """
    Return all dates in dates which aren't in dates_file

    Parameters
    ----------
    dates_file:
        path to dates file, used as comparison
    dates:
        dates to compare
    """
    old_dates = []

    if Path(dates_file).exists():
        with open(dates_file, encoding="utf-8") as f_dates:
            old_dates = [line.rstrip() for line in f_dates]
    new_dates = [date for date in dates if date not in old_dates]
    new_dates.sort()

    return new_dates


def remove_old_dates(img_list: list[str], new_dates: list[str]) -> list[str]:
    """remove dates already computed in outCore Stack"""
    date_pos = -1

    img_to_outcore = []
    for img in img_list:
        img_date = Path(img).name.split("_")[date_pos].replace(".tif", "")
        if img_date in new_dates:
            img_to_outcore.append(img)
    return img_to_outcore


def main(
    ortho: list[str],
    config_file: str,
    dates: dict[str, list],
    logger: logging.Logger = LOGGER,
) -> list:
    """
    Main function for SAR filtering
    """
    config = configparser.ConfigParser()
    config.read(config_file)
    ram_per_process = int(
        config.get(
            "Processing",
            "RAMPerProcess",
            fallback=S1_CONST.s1_ram_per_process,
        )
    )
    output_pre_process = config.get("Paths", "Output")
    window_radius = config.get(
        "Filtering", "Window_radius", fallback=S1_CONST.s1_window_radius
    )

    # directories = next(file_utils.walk(output_pre_process))
    output_pre_process_path = Path(output_pre_process)
    directories = (
        str(output_pre_process_path),  # outputPreProcess_path
        [
            str(directory.name)
            for directory in output_pre_process_path.iterdir()
            if directory.is_dir()
        ],
        [str(file) for file in output_pre_process_path.iterdir() if file.is_file()],
    )

    sar_filter = []
    # OUTCOREs
    for directory in directories[1]:
        s1_vv_des_scene = sorted(
            list(get_ortho(ortho, "s1(.*)" + directory + "(.*)vv(.*)DES(.*)tif")),
            key=get_dates_in_otb_output_name,
        )
        if s1_vv_des_scene:
            outcore_s1_vv_des = str(
                Path(directories[0]) / directory / "outcore_S1_vv_DES.tif"
            )
            outcore_s1_vv_des_dates = str(
                Path(directories[0], directory, "S1_vv_DES_dates.txt")
            )
            new_outcore_s1_vv_des_dates = compare_dates(
                outcore_s1_vv_des_dates, dates["s1_DES"]
            )
            if new_outcore_s1_vv_des_dates:
                file_utils.write_new_file(
                    outcore_s1_vv_des_dates, "\n".join(dates["s1_DES"])
                )
            s1_vv_des_outcore = remove_old_dates(
                s1_vv_des_scene, new_outcore_s1_vv_des_dates
            )
            if s1_vv_des_outcore or not Path(outcore_s1_vv_des).exists():
                s1_vv_des_outcore_app, _ = otb_app_bank.create_application(
                    otb_app_bank.AvailableOTBApp.MULTITEMP_FILTERING_OUTCORE,
                    {
                        "inl": s1_vv_des_outcore,
                        "oc": outcore_s1_vv_des,
                        "wr": str(window_radius),
                        "ram": str(ram_per_process),
                        "pixType": "float",
                    },
                )
                logger.info(f"writing : {outcore_s1_vv_des}")
                s1_vv_des_outcore_app.ExecuteAndWriteOutput()
                logger.info(f"{outcore_s1_vv_des} : done")

        s1_vh_des_scene = sorted(
            list(get_ortho(ortho, "s1(.*)" + directory + "(.*)vh(.*)DES(.*)tif")),
            key=get_dates_in_otb_output_name,
        )
        if s1_vh_des_scene:
            outcore_s1_vh_des = str(
                Path(directories[0]) / directory / "outcore_S1_vh_DES.tif"
            )
            outcore_s1_vh_des_dates = str(
                Path(directories[0], directory, "S1_vh_DES_dates.txt")
            )
            new_outcore_s1_vh_des_dates = compare_dates(
                outcore_s1_vh_des_dates, dates["s1_DES"]
            )
            if new_outcore_s1_vh_des_dates:
                file_utils.write_new_file(
                    outcore_s1_vh_des_dates, "\n".join(dates["s1_DES"])
                )
            s1_vh_des_outcore = remove_old_dates(
                s1_vh_des_scene, new_outcore_s1_vh_des_dates
            )
            if s1_vh_des_outcore or not Path(outcore_s1_vh_des).exists():
                s1_vh_des_outcore_app, _ = otb_app_bank.create_application(
                    otb_app_bank.AvailableOTBApp.MULTITEMP_FILTERING_OUTCORE,
                    {
                        "inl": s1_vh_des_outcore,
                        "oc": outcore_s1_vh_des,
                        "wr": str(window_radius),
                        "ram": str(ram_per_process),
                        "pixType": "float",
                    },
                )
                logger.info(f"writing : {outcore_s1_vh_des}")
                s1_vh_des_outcore_app.ExecuteAndWriteOutput()
                logger.info(f"{outcore_s1_vh_des} : done")

        s1_vv_asc_scene = sorted(
            list(get_ortho(ortho, "s1(.*)" + directory + "(.*)vv(.*)ASC(.*)tif")),
            key=get_dates_in_otb_output_name,
        )
        if s1_vv_asc_scene:
            outcore_s1_vv_asc = str(
                Path(directories[0]) / directory / "outcore_S1_vv_ASC.tif"
            )
            outcore_s1_vv_asc_dates = str(
                Path(directories[0], directory, "S1_vv_ASC_dates.txt")
            )
            new_outcore_s1_vv_asc_dates = compare_dates(
                outcore_s1_vv_asc_dates, dates["s1_ASC"]
            )
            if new_outcore_s1_vv_asc_dates:
                file_utils.write_new_file(
                    outcore_s1_vv_asc_dates, "\n".join(dates["s1_ASC"])
                )
            s1_vv_asc_outcore = remove_old_dates(
                s1_vv_asc_scene, new_outcore_s1_vv_asc_dates
            )
            if s1_vv_asc_outcore or not Path(outcore_s1_vv_asc).exists():
                s1_vv_asc_outcore_app, _ = otb_app_bank.create_application(
                    otb_app_bank.AvailableOTBApp.MULTITEMP_FILTERING_OUTCORE,
                    {
                        "inl": s1_vv_asc_outcore,
                        "oc": outcore_s1_vv_asc,
                        "wr": str(window_radius),
                        "ram": str(ram_per_process),
                        "pixType": "float",
                    },
                )
                logger.info(f"writing : {outcore_s1_vv_asc}")
                s1_vv_asc_outcore_app.ExecuteAndWriteOutput()
                logger.info(f"{outcore_s1_vv_asc} : done")

        s1_vh_asc_scene = sorted(
            list(get_ortho(ortho, "s1(.*)" + directory + "(.*)vh(.*)ASC(.*)tif")),
            key=get_dates_in_otb_output_name,
        )
        if s1_vh_asc_scene:
            outcore_s1_vh_asc = str(
                Path(directories[0]) / directory / "outcore_S1_vh_ASC.tif"
            )
            outcore_s1_vh_asc_dates = str(
                Path(directories[0], directory, "S1_vh_ASC_dates.txt")
            )
            new_outcore_s1_vh_asc_dates = compare_dates(
                outcore_s1_vh_asc_dates, dates["s1_ASC"]
            )
            if new_outcore_s1_vh_asc_dates:
                file_utils.write_new_file(
                    outcore_s1_vh_asc_dates, "\n".join(dates["s1_ASC"])
                )
            s1_vh_asc_outcore = remove_old_dates(
                s1_vh_asc_scene, new_outcore_s1_vh_asc_dates
            )
            if s1_vh_asc_outcore or not Path(outcore_s1_vh_asc).exists():
                s1_vh_asc_outcore_app, _ = otb_app_bank.create_application(
                    otb_app_bank.AvailableOTBApp.MULTITEMP_FILTERING_OUTCORE,
                    {
                        "inl": s1_vh_asc_outcore,
                        "oc": outcore_s1_vh_asc,
                        "wr": str(window_radius),
                        "ram": str(ram_per_process),
                        "pixType": "float",
                    },
                )
                logger.info(f"writing : {outcore_s1_vh_asc}")
                s1_vh_asc_outcore_app.ExecuteAndWriteOutput()
                logger.info(f"{outcore_s1_vh_asc} : done")

        (Path(directories[0]) / directory / "filtered").mkdir(
            parents=True, exist_ok=True
        )

        # FILTERING
        if s1_vv_des_scene:
            enl = str(Path(directories[0]) / directory / "filtered/enl_S1_vv_DES.tif")
            s1_vv_des_filtered = str(
                Path(directories[0], directory, "filtered/S1_vv_DES_Filtered.tif")
            )
            (
                s1_vv_des_filtered_app,
                (in_img, outcore),
            ) = otb_app_bank.create_application(
                otb_app_bank.AvailableOTBApp.MULTITEMP_FILTERING_FILTER,
                {
                    "inl": s1_vv_des_scene,
                    "oc": outcore_s1_vv_des,
                    "wr": str(window_radius),
                    "enl": enl,
                    "ram": str(ram_per_process),
                    "pixType": "float",
                    "outputstack": s1_vv_des_filtered,
                },
            )

            sar_filter.append((s1_vv_des_filtered_app, in_img, outcore))
        if s1_vh_des_scene:
            enl = str(Path(directories[0]) / directory / "filtered/enl_S1_vh_DES.tif")
            s1_vh_des_filtered = str(
                Path(directories[0], directory, "filtered/S1_vh_DES_Filtered.tif")
            )
            (
                s1_vh_des_filtered_app,
                (in_img, outcore),
            ) = otb_app_bank.create_application(
                otb_app_bank.AvailableOTBApp.MULTITEMP_FILTERING_FILTER,
                {
                    "inl": s1_vh_des_scene,
                    "oc": outcore_s1_vh_des,
                    "wr": str(window_radius),
                    "enl": enl,
                    "ram": str(ram_per_process),
                    "pixType": "float",
                    "outputstack": s1_vh_des_filtered,
                },
            )

            sar_filter.append((s1_vh_des_filtered_app, in_img, outcore))
        if s1_vv_asc_scene:
            enl = str(Path(directories[0]) / directory / "filtered/enl_S1_vv_ASC.tif")
            s1_vv_asc_filtered = str(
                Path(directories[0], directory, "filtered/S1_vv_ASC_Filtered.tif")
            )
            (
                s1_vv_asc_filtered_app,
                (in_img, outcore),
            ) = otb_app_bank.create_application(
                otb_app_bank.AvailableOTBApp.MULTITEMP_FILTERING_FILTER,
                {
                    "inl": s1_vv_asc_scene,
                    "oc": outcore_s1_vv_asc,
                    "wr": str(window_radius),
                    "enl": enl,
                    "ram": str(ram_per_process),
                    "pixType": "float",
                    "outputstack": s1_vv_asc_filtered,
                },
            )

            sar_filter.append((s1_vv_asc_filtered_app, in_img, outcore))
        if s1_vh_asc_scene:
            enl = str(Path(directories[0]) / directory / "filtered/enl_S1_vh_ASC.tif")
            s1_vh_asc_filtered = str(
                Path(directories[0], directory, "filtered/S1_vh_ASC_Filtered.tif")
            )
            (
                s1_vh_asc_filtered_app,
                (in_img, outcore),
            ) = otb_app_bank.create_application(
                otb_app_bank.AvailableOTBApp.MULTITEMP_FILTERING_FILTER,
                {
                    "inl": s1_vh_asc_scene,
                    "oc": outcore_s1_vh_asc,
                    "wr": str(window_radius),
                    "enl": enl,
                    "ram": str(ram_per_process),
                    "pixType": "float",
                    "outputstack": s1_vh_asc_filtered,
                },
            )

            sar_filter.append((s1_vh_asc_filtered_app, in_img, outcore))
    return sar_filter
