#!/usr/bin/env python3
# =========================================================================
#   Program:   S1Processor
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
#
# Authors: Thierry KOLECK (CNES)
#
# =========================================================================
"""
Sentinel 1 file manager
"""

import configparser
import glob
import sys
from pathlib import Path

from osgeo import ogr

from iota2.typings.i2_types import CoordXYList, OtbApp


class S1DateAcquisition:
    """
    Class to store acquisition data
    """

    def __init__(self, manifest: str, image_filenames_list: list[str]):
        self.manifest = manifest
        self.image_filenames_list = image_filenames_list
        self.calibration_application = (
            None  # composed by [[calib,calibDependance],[...]...]
        )

    def get_manifest(self) -> str:
        """
        Gets manifest
        """
        return self.manifest

    def add_image(self, image_list: str) -> None:
        """
        Add image to the images list
        """
        self.image_filenames_list.append(image_list)

    def get_image_list(self) -> list[str]:
        """
        Gets images list
        """
        return self.image_filenames_list

    def set_calibration_application(self, calib: OtbApp) -> None:
        """
        Set the instance's calibration app

        Parameters
        ----------
        calib: OtbApp
            OTB app for calibration

        Returns
        -------
        None
        """
        self.calibration_application = calib

    def get_calibration_application(self) -> OtbApp:
        """
        Get the instance's calibration app

        Returns
        -------
        self.calibration_application: OtbApp
            The instance's OTB calibration app
        """
        return self.calibration_application


class S1FileManager:
    """
    Sentinel 1 file manager
    """

    def __init__(self, config_file: str):

        config = configparser.ConfigParser()
        config.read(config_file)

        self.mgrs_shape_file = config.get("Processing", "TilesShapefile")
        if not Path(self.mgrs_shape_file).exists():
            print("ERROR: " + self.mgrs_shape_file + " is a wrong path")
            sys.exit(1)

        self.srtm_shape_file = config.get("Processing", "SRTMShapefile")
        if not Path(self.srtm_shape_file).exists():
            print("ERROR: " + self.srtm_shape_file + " is a wrong path")
            sys.exit(1)

        self.raw_directory = config.get("Paths", "S1Images")

        self.output_preprocess = config.get("Paths", "Output")

        self.vh_pattern = "measurement/*vh*-???.tiff"
        self.vv_pattern = "measurement/*vv*-???.tiff"
        self.hh_pattern = "measurement/*hh*-???.tiff"
        self.hv_pattern = "measurement/*hv*-???.tiff"
        self.manifest_pattern = "manifest.safe"

        self.tiles_list = config.get("Processing", "Tiles").split(",")

        self.processed_filenames: list = []
        self.get_s1_img()
        try:
            Path(self.raw_directory).mkdir(parents=True, exist_ok=True)
        except OSError:
            pass

    def get_s1_img(self) -> None:
        """
        Add all input images path to a list
        """
        self.raw_raster_list = []
        self.nb_images = 0
        if not Path(self.raw_directory).exists():
            Path(self.raw_directory).mkdir(parents=True, exist_ok=True)
            return
        content = [str(p) for p in Path(self.raw_directory).iterdir()]
        for current_content in content:
            safe_dir = str(Path(self.raw_directory) / current_content)
            if Path(safe_dir).is_dir():
                manifest = str(Path(safe_dir) / self.manifest_pattern)
                acquisition = S1DateAcquisition(manifest, [])
                vv_files = list(glob.glob(str(Path(safe_dir) / self.vv_pattern)))
                for vv_image in vv_files:
                    if vv_image not in self.processed_filenames:
                        acquisition.add_image(vv_image)
                        self.nb_images += 1
                vh_files = list(glob.glob(str(Path(safe_dir) / self.vh_pattern)))
                for vh_image in vh_files:
                    if vh_image not in self.processed_filenames:
                        acquisition.add_image(vh_image)
                        self.nb_images += 1
                hh_files = list(glob.glob(str(Path(safe_dir) / self.hh_pattern)))
                for hh_image in hh_files:
                    if hh_image not in self.processed_filenames:
                        acquisition.add_image(hh_image)
                        self.nb_images += 1
                hv_files = list(glob.glob(str(Path(safe_dir) / self.hv_pattern)))
                for hv_image in hv_files:
                    if hv_image not in self.processed_filenames:
                        acquisition.add_image(hv_image)
                        self.nb_images += 1
                self.raw_raster_list.append(acquisition)

    def get_s1_intersect_by_tile(
        self, tile_name_field: str
    ) -> list[tuple[S1DateAcquisition, CoordXYList]]:
        """
        If the specified tile exists in the MGRS shapefile, returns the coordinates of the
        intersection polygon between the tile and each input image.

        Parameters
        ----------
        tile_name_field:
            tile name

        Notes
        -----
            Returns a list of tuples, each containing:
                - The image as S1DateAcquisition
                - Its intersection coordinates with the specified tile.
            An empty list is returned if the tile does not exist.
        """
        intersect_raster: list[tuple[S1DateAcquisition, CoordXYList]] = []
        driver = ogr.GetDriverByName("ESRI Shapefile")
        data_source = driver.Open(self.mgrs_shape_file, 0)
        layer = data_source.GetLayer()
        found = False
        current_tile = None
        for current_tile in layer:
            if current_tile.GetField("NAME") == tile_name_field:
                found = True
                break
        if not found:
            print("Tile " + str(tile_name_field) + " does not exist")
            return intersect_raster
        assert current_tile
        tile_foot_print = current_tile.GetGeometryRef()

        for image in self.raw_raster_list:
            manifest = image.get_manifest()
            nw, ne, se, sw = self.get_origin(manifest)

            poly = ogr.Geometry(ogr.wkbPolygon)
            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(nw[1], nw[0], 0)
            ring.AddPoint(ne[1], ne[0], 0)
            ring.AddPoint(se[1], se[0], 0)
            ring.AddPoint(sw[1], sw[0], 0)
            ring.AddPoint(nw[1], nw[0], 0)
            poly.AddGeometry(ring)

            intersection = poly.Intersection(tile_foot_print)
            if intersection.GetArea() != 0:
                area_polygon = tile_foot_print.GetGeometryRef(0)
                points = area_polygon.GetPoints()
                intersect_raster.append(
                    (image, [(point[0], point[1]) for point in points[:-1]])
                )

        return intersect_raster

    def get_mgrs_tile_geometry_by_name(self, mgrs_tile_name: str) -> ogr.Geometry:
        """
        Returns the geometry of the tile with the specified name

        Parameters
        ----------
        mgrs_tile_name:
            name of the tile
        """
        driver = ogr.GetDriverByName("ESRI Shapefile")
        mgrs_ds = driver.Open(self.mgrs_shape_file, 0)
        mgrs_layer = mgrs_ds.GetLayer()

        for mgrs_tile in mgrs_layer:
            if mgrs_tile.GetField("NAME") == mgrs_tile_name:
                return mgrs_tile.GetGeometryRef().Clone()
        raise Exception("MGRS tile does not exist " + mgrs_tile_name)

    def check_srtm_coverage(
        self, tiles_to_process: list[str]
    ) -> dict[str, list[tuple[str, float]]]:
        """
        For each tile, return its ratio of coverage with SRTM tiles (if they intersect)

        Parameters
        ----------
        tiles_to_process:
            list of tiles
        """
        driver = ogr.GetDriverByName("ESRI Shapefile")
        srtm_ds = driver.Open(self.srtm_shape_file, 0)
        srtm_layer = srtm_ds.GetLayer()

        needed_srtm_tiles = {}

        for tile in tiles_to_process:
            srtm_tiles = []
            mgrs_footprint = self.get_mgrs_tile_geometry_by_name(tile)
            area = mgrs_footprint.GetArea()
            srtm_layer.ResetReading()
            for srtm_tile in srtm_layer:
                srtm_footprint = srtm_tile.GetGeometryRef()
                intersection = mgrs_footprint.Intersection(srtm_footprint)
                if intersection.GetArea() > 0:
                    coverage = intersection.GetArea() / area
                    srtm_tiles.append((srtm_tile.GetField("FILE"), coverage))
            needed_srtm_tiles[tile] = srtm_tiles
        return needed_srtm_tiles

    def get_origin(self, manifest: str) -> tuple[tuple[float, float], ...]:
        """
        Gets coordinates of a tile from manifest file

        Parameters
        ----------
        manifest:
            path to manifest file
        """
        with open(manifest, encoding="utf-8") as save_file:
            for line in save_file:
                if "<gml:coordinates>" in line:
                    coor = (
                        line.replace("                <gml:coordinates>", "")
                        .replace("</gml:coordinates>", "")
                        .split(" ")
                    )
                    coord = [
                        (
                            float(val.replace("\n", "").split(",")[0]),
                            float(val.replace("\n", "").split(",")[1]),
                        )
                        for val in coor
                    ]
                    return coord[0], coord[1], coord[2], coord[3]
            raise Exception("Coordinates not found in " + str(manifest))
