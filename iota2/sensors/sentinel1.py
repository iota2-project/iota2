#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""sentinel_1 class definition"""

import configparser
import glob
import logging
import re
from pathlib import Path
from typing import Literal

from sensorsio import sentinel1 as sio_s1
from shapely import geometry

import iota2.common.i2_constants as i2_const
from iota2.common.compression_options import delete_compression_suffix
from iota2.common.file_utils import (
    file_search_and,
    get_dates_in_tile,
    sort_by_first_elem,
)
from iota2.common.otb_app_bank import (
    AvailableOTBApp,
    create_application,
    generate_sar_feat_dates,
    get_input_parameter_output,
    get_sar_stack,
)
from iota2.common.utils import chunker
from iota2.learning.utils import I2TemporalLabel
from iota2.typings.i2_types import (
    I2FeaturesPipeline,
    I2SARFeaturesPipeline,
    ListPathLike,
    OtbApp,
    OtbDep,
)

S1_CONST = i2_const.S1Constants()
LOGGER = logging.getLogger("distributed.worker")

# in order to avoid issue 'No handlers could be found for logger...'
LOGGER.addHandler(logging.NullHandler())

OrbitPassVal = Literal["Ascending", "Descending"]


def group_by_date(s1_product_list: list[Path]) -> list[Path]:
    """
    Return a list of S1 products sorted by date

    Parameters
    ----------
    s1_product_list:
        List of s1 products
    """
    tmp = []
    for s1_prod in s1_product_list:
        name = Path(s1_prod).stem
        date = name.split("_")[Sentinel1.date_position].split("T")[0]
        tmp.append((date, s1_prod))
    tmp = sort_by_first_elem(tmp)
    return [prod_list for _, prod_list in tmp]


def sort_s1_orbit_pass(s1_products: ListPathLike) -> dict[OrbitPassVal, list[Path]]:
    """Return a dictionary (orbit pass as key) containing products."""
    return dict(
        sort_by_first_elem(
            [
                (
                    sio_s1.get_orbit_pass(
                        Path(glob.glob(f"{s1_prod}/annotation/*vv*xml")[0])
                    ),
                    s1_prod,
                )
                for s1_prod in s1_products
            ]
        )
    )


class Sentinel1:
    """Sentinel_1 class definition."""

    name = "Sentinel1"
    nodata_value = -10000
    date_position = 4

    def __init__(  # pylint: disable=unused-argument
        self,
        tile_name: str,
        image_directory: str,
        all_tiles: str,
        i2_output_path: str,
        enable_gapfilling: bool,
        write_outputs_flag: bool,
        **kwargs: dict,
    ):
        self.tile_name = tile_name

        config_parser = configparser.ConfigParser()

        # running attributes
        self.s1_cfg = image_directory
        self.write_outputs_flag = write_outputs_flag
        config_parser.read(self.s1_cfg)
        s1_output_processing = config_parser.get("Paths", "output")
        self.all_tiles = all_tiles
        self.output_processing = str(Path(s1_output_processing) / tile_name[1:])
        self.i2_output_path = i2_output_path
        self.features_dir = str(Path(self.i2_output_path) / "features" / tile_name)
        self.use_gapfilling = enable_gapfilling
        # sensors attributes
        self.mask_pattern = "BorderMask.tif"
        # output's names
        self.mask_orbit_pol_name = f"{self.__class__.name}_{tile_name}_MASK"
        self.gapfilling_orbit_pol_name = f"{self.__class__.name}_{tile_name}_TSG"
        self.gapfilling_orbit_pol_name_mask = f"{self.__class__.name}_{tile_name}_MASK"
        self.sar_features_name = f"{self.__class__.name}_{tile_name}_Features.tif"
        self.user_sar_features_name = (
            f"{self.__class__.name}_{tile_name}_USER_Features.tif"
        )
        self.footprint_name = f"{self.__class__.name}_{tile_name}_footprint.tif"

        self.stack_band_position = ["vv", "vh"]
        self.features_names_list = ["DES", "ASC"]

    @classmethod
    def s1_product_footprint(cls, product_dir: Path) -> geometry:
        """
        Return the footprint of S1 product as a polygon

        Parameters
        ----------
        product_dir:
            Directory where the product is located
        """
        if not cls.is_s1_product(product_dir):
            raise ValueError(f"{str(product_dir)} is not a valid Sentinel1 path.")
        manifest_file = product_dir / "manifest.safe"
        with open(manifest_file, "rb") as manifest_content:
            coordinates = None
            srs = None
            for line in [str(elem.rstrip()) for elem in manifest_content]:
                if "http://www.opengis.net/gml/srs/epsg.xml#" in line:
                    srs = int(re.findall(r"\d+", line)[0])
                if "<gml:coordinates>" in line:
                    coordinates = re.findall(r"[-+]?(?:\d*\.*\d+)", line)
            if not coordinates:
                raise ValueError(
                    f"iota2 can't find coordinates in {str(manifest_file)}"
                )
            if not srs:
                raise ValueError(f"iota2 can't find srs in {str(manifest_file)}")
            coordinates = [float(_) for _ in coordinates]
            geom = geometry.Polygon([[x, y] for y, x in chunker(coordinates, 2)])
        return geom, srs

    @classmethod
    def sort_products(cls, product_list: list[Path]) -> list[Path]:
        """Sort products."""
        return sorted(
            product_list,
            key=lambda x: int(
                Path(x).name.split("_")[cls.date_position].replace("T", "")
            ),
        )

    @classmethod
    def is_s1_product(cls, path_file: Path) -> bool:
        """Return True if the pathlike is a s1 product."""
        if not path_file.is_dir():
            return False
        dir_name = path_file.stem.split("_")
        if len(dir_name) != 9:
            return False
        if "S1" not in dir_name[0]:
            return False
        if not ["IW", "GRDH"] == dir_name[1:3]:
            return False
        return True

    def _get_dates(
        self, date_file_prefix: str, date_file_suffix: str
    ) -> dict[str, list[str]]:
        """
        return sorted available dates per obits
        """
        i2_input_date_dir = str(
            Path(self.i2_output_path, "features", self.tile_name, "tmp")
        )
        orbits = ["DES", "ASC"]
        sensors_dates: dict[str, list[str]] = {}
        for orbit in orbits:
            files_tile_dates = file_search_and(
                i2_input_date_dir,
                True,
                f"{date_file_prefix}_{orbit}_{date_file_suffix}.txt",
            )
            if files_tile_dates:
                sensors_dates[orbit + "_vv"] = []
                with open(files_tile_dates[0], encoding="utf-8") as date_file:
                    for line in date_file:
                        try:
                            new_date = str(int(line))
                            sensors_dates[orbit + "_vv"].append(new_date)
                        except ValueError:
                            pass
                sensors_dates[orbit + "_vh"] = sensors_dates[orbit + "_vv"]
        return sensors_dates

    def get_available_dates(self) -> dict[str, list[str]]:
        """Return sorted available input dates per obits."""
        return self._get_dates("Sentinel1", f"{self.tile_name}_input_dates")

    def get_interpolated_dates(self) -> dict[str, list[str]]:
        """Return sorted available interpolated dates per obits."""
        return self._get_dates("S1_vv", "dates_interpolation")

    def get_available_dates_masks(self) -> None:
        """Return sorted available masks."""

    def preprocess(  # pylint: disable=unused-argument
        self,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> None:
        """sentinel_1 preprocessing"""

        # generate dates txt file per dates
        get_sar_stack(
            self.s1_cfg,
            self.tile_name,
            self.all_tiles.split(" "),
            str(Path(self.i2_output_path) / "features"),
            working_directory=working_dir,
        )

    def footprint(self, ram: int = 128) -> tuple[OtbApp, OtbDep]:
        """get sentinel_1 footprint"""

        s1_border_masks = file_search_and(
            self.output_processing, True, self.mask_pattern
        )

        sum_mask = "+".join([f"im{i + 1}b1" for i in range(len(s1_border_masks))])
        expression = f"{sum_mask}=={len(s1_border_masks)}?0:1"
        raster_footprint = str(Path(self.features_dir) / "tmp" / self.footprint_name)
        footprint_app, _ = create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": s1_border_masks,
                "out": raster_footprint,
                "exp": expression,
                "ram": str(ram),
            },
        )
        footprint_app_dep: list = []
        return footprint_app, footprint_app_dep

    def get_time_series(
        self, ram: int = 128  # pylint: disable=unused-argument
    ) -> I2SARFeaturesPipeline:
        """
        Due to the sar data, time series must be split by polarisation
        and orbit (ascending / descending)
        """
        (all_filtered, all_masks, interp_date_files, input_date_files) = get_sar_stack(
            self.s1_cfg,
            self.tile_name,
            self.all_tiles.split(" "),
            str(Path(self.i2_output_path) / "features"),
            working_directory=None,
        )
        # to be clearer
        s1_data = {}
        s1_labels = {}

        for filtered, _, _, in_dates in zip(
            all_filtered, all_masks, interp_date_files, input_date_files
        ):
            sar_mode = Path(
                delete_compression_suffix(filtered.GetParameterValue("outputstack"))
            ).name
            sar_mode = "_".join(Path(sar_mode).stem.split("_")[0:-1])
            polarisation = sar_mode.split("_")[1]
            orbit = sar_mode.split("_")[2]

            s1_data[sar_mode] = filtered
            sar_dates = sorted(
                get_dates_in_tile(in_dates, display=False),
                key=int,
            )
            labels = [
                I2TemporalLabel(
                    sensor_name=f"{self.__class__.name}",
                    feat_name=f"{orbit}{polarisation}",
                    date=date,
                )
                for date in sar_dates
            ]
            s1_labels[sar_mode] = labels
        dependancies: list = []
        return (s1_data, dependancies), s1_labels

    def get_time_series_raw(
        self, ram: int = 128  # pylint: disable=unused-argument
    ) -> I2FeaturesPipeline:  # pylint: disable=unused-argument
        """Concatenate all S1 data"""
        (s1_data, dependancies), s1_labels = self.get_time_series()

        for _, app in s1_data.items():
            app.Execute()
            dependancies.append(app)
        data_to_stack = [app for _, app in s1_data.items()]

        stack_time_series, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES, {"il": data_to_stack}
        )
        stack_time_series.Execute()
        stacked_features_labels = []
        for _, labels in s1_labels.items():
            stacked_features_labels += labels
        return (stack_time_series, dependancies), stacked_features_labels

    def get_time_series_masks(
        self,
        ram: int = 128,
        logger: logging.Logger = LOGGER,  # pylint: disable=unused-argument
    ) -> tuple[dict[str, OtbApp], OtbDep, int]:
        """
        Due to the sar data, masks series must be split by polarisation
        and orbit (ascending / descending)
        """
        (all_filtered, all_masks, interp_date_files, input_date_files) = get_sar_stack(
            self.s1_cfg,
            self.tile_name,
            self.all_tiles.split(" "),
            str(Path(self.i2_output_path) / "features"),
            working_directory=None,
        )
        # to be clearer
        s1_masks = {}
        nb_avail_masks = 0
        for filtered, masks, _, _ in zip(
            all_filtered, all_masks, interp_date_files, input_date_files
        ):
            sar_file = Path(
                delete_compression_suffix(filtered.GetParameterValue("outputstack"))
            )
            sar_mode = "_".join(Path(sar_file).stem.split("_")[0:-1])
            polarisation = sar_mode.split("_")[1]
            orbit = sar_mode.split("_")[2]
            mask_orbit_pol_name = (
                f"{self.mask_orbit_pol_name}_{orbit}_{polarisation}.tif"
            )

            mask_orbit_pol = str(Path(self.features_dir) / "tmp" / mask_orbit_pol_name)
            masks_app, _ = create_application(
                AvailableOTBApp.CONCATENATE_IMAGES,
                {
                    "il": masks,
                    "out": mask_orbit_pol,
                    "pixType": "uint8" if len(masks) > 255 else "uint16",
                    "ram": str(ram),
                },
            )
            s1_masks[sar_mode] = masks_app
            nb_avail_masks += len(masks)

        dependancies: list = []
        return s1_masks, dependancies, nb_avail_masks

    def get_time_series_masks_raw(  # pylint: disable=unused-argument
        self,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> tuple[OtbApp, list[OtbApp], int]:
        """
        Concatenate all the time series masks

        Parameters
        ----------
        ram:
            RAM available
        logger:
            Logger
        """
        s1_masks, dependancies, nb_avail_masks = self.get_time_series_masks()
        data_to_stack = []
        for _, app in s1_masks.items():
            app.Execute()
            dependancies.append(app)

        # vv masks are the same as vh masks
        if "S1_vv_DES" in s1_masks:
            data_to_stack.append(s1_masks["S1_vv_DES"])
        if "S1_vv_ASC" in s1_masks:
            data_to_stack.append(s1_masks["S1_vv_ASC"])

        mask_time_series, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES, {"il": data_to_stack}
        )
        nb_avail_masks = int(nb_avail_masks / 2)
        return mask_time_series, dependancies, nb_avail_masks

    def write_dates_file(self) -> None:
        """Actually this method do not write the file containing intput tile dates
        but return
        """
        _ = get_sar_stack(
            self.s1_cfg,
            self.tile_name,
            self.all_tiles.split(" "),
            str(Path(self.i2_output_path) / "features"),
            working_directory=None,
        )

    def get_time_series_gap_filling(self, ram: int = 128) -> I2SARFeaturesPipeline:
        """
        Due to the sar data, time series must be split by polarisation
        and orbit (ascending / descending)
        """
        (all_filtered, all_masks, interp_date_files, input_date_files) = get_sar_stack(
            self.s1_cfg,
            self.tile_name,
            self.all_tiles.split(" "),
            str(Path(self.i2_output_path) / "features"),
            working_directory=None,
        )
        # to be clearer
        s1_data = {}
        s1_labels = {}

        config = configparser.ConfigParser()
        config.read(self.s1_cfg)

        interpolation_method = config.get(
            "Processing",
            "gapFilling_interpolation",
            fallback=S1_CONST.s1_interp_method,
        )
        dependencies = []

        for filtered, masks, interp_dates, in_dates in zip(
            all_filtered, all_masks, interp_date_files, input_date_files
        ):
            sar_mode = Path(
                delete_compression_suffix(filtered.GetParameterValue("outputstack"))
            ).name
            sar_mode = "_".join(Path(sar_mode).stem.split("_")[0:-1])
            polarisation = sar_mode.split("_")[1]
            orbit = sar_mode.split("_")[2]

            gapfilling_orbit_pol_name_masks = (
                f"{self.gapfilling_orbit_pol_name_mask}_{orbit}_{polarisation}.tif"
            )
            gapfilling_raster_mask = str(
                Path(self.features_dir, "tmp", gapfilling_orbit_pol_name_masks)
            )

            masks_stack, _ = create_application(
                AvailableOTBApp.CONCATENATE_IMAGES,
                {"il": masks, "out": gapfilling_raster_mask, "ram": str(ram)},
            )

            if self.write_outputs_flag is False:
                filtered.Execute()
                masks_stack.Execute()
            else:
                filtered_raster = delete_compression_suffix(
                    filtered.GetParameterValue(get_input_parameter_output(filtered))
                )
                masks_stack_raster = delete_compression_suffix(
                    masks_stack.GetParameterValue(
                        get_input_parameter_output(masks_stack)
                    )
                )
                if not Path(masks_stack_raster).exists():
                    masks_stack.ExecuteAndWriteOutput()
                if not Path(filtered_raster).exists():
                    filtered.ExecuteAndWriteOutput()
                if Path(masks_stack_raster).exists():
                    masks_stack = masks_stack_raster
                if Path(filtered_raster).exists():
                    filtered = filtered_raster
            dependencies.append((filtered, masks_stack))
            gapfilling_orbit_pol_name = (
                f"{self.gapfilling_orbit_pol_name}_{orbit}_{polarisation}.tif"
            )
            gapfilling_raster = str(
                Path(self.features_dir, "tmp", gapfilling_orbit_pol_name)
            )

            gap_app, _ = create_application(
                AvailableOTBApp.IMAGE_TIME_SERIES_GAPFILLING,
                {
                    "in": filtered,
                    "mask": masks_stack,
                    "it": interpolation_method,
                    "id": in_dates,
                    "od": interp_dates,
                    "comp": str(1),
                    "out": gapfilling_raster,
                },
            )
            s1_data[sar_mode] = gap_app

            sar_dates = sorted(
                get_dates_in_tile(interp_dates, display=False),
                key=int,
            )
            labels = [
                I2TemporalLabel(
                    sensor_name=f"{self.__class__.name}",
                    feat_name=f"{orbit}{polarisation}",
                    date=date,
                )
                for date in sar_dates
            ]
            s1_labels[sar_mode] = labels
        return (s1_data, dependencies), s1_labels

    # pylint: disable=unused-argument; logger is used in user_features
    def get_features(
        self, ram: int = 128, logger: logging.Logger = LOGGER
    ) -> I2FeaturesPipeline:
        """get sar features"""
        if self.use_gapfilling:
            (s1_data, dependancies), s1_labels = self.get_time_series_gap_filling(ram)
        else:
            (s1_data, dependancies), s1_labels = self.get_time_series(ram)
        config = configparser.ConfigParser()
        config.read(self.s1_cfg)

        sar_features_expr = None
        if config.has_option("Features", "expression"):
            sar_features_expr_cfg = config.get("Features", "expression")
            if sar_features_expr_cfg.lower() != "none":
                sar_features_expr = sar_features_expr_cfg.split(",")

        dependancies = [dependancies]
        s1_features = []
        sar_time_series = {
            "asc": {
                "vv": {"App": None, "availDates": None},
                "vh": {"App": None, "availDates": None},
            },
            "des": {
                "vv": {"App": None, "availDates": None},
                "vh": {"App": None, "availDates": None},
            },
        }
        for sensor_mode, time_series_app in list(s1_data.items()):
            _, polarisation, orbit = sensor_mode.split("_")
            # inputs
            if self.write_outputs_flag is False:
                time_series_app.Execute()
            else:
                time_series_raster = delete_compression_suffix(
                    time_series_app.GetParameterValue(
                        get_input_parameter_output(time_series_app)
                    )
                )
                if not Path(time_series_raster).exists():
                    time_series_app.ExecuteAndWriteOutput()
                if Path(time_series_raster).exists():
                    time_series_app = time_series_raster

            sar_time_series[orbit.lower()][polarisation.lower()][
                "App"
            ] = time_series_app

            s1_features.append(time_series_app)
            dependancies.append(time_series_app)
            if self.use_gapfilling:
                date_file = file_search_and(
                    self.features_dir,
                    True,
                    f"{polarisation.lower()}_{orbit.upper()}_dates_interpolation.txt",
                )[0]
            else:
                tar_dir = str(Path(config.get("Paths", "output"), self.tile_name[1:]))
                date_file = file_search_and(
                    tar_dir,
                    True,
                    f"{polarisation.lower()}_{orbit.upper()}_dates_input.txt",
                )[0]
            sar_time_series[orbit.lower()][polarisation.lower()][
                "availDates"
            ] = get_dates_in_tile(date_file, display=False)
        features_labels = []
        for sensor_mode, features in list(s1_labels.items()):
            features_labels += features
        if sar_features_expr:
            sar_user_features_raster = str(
                Path(self.features_dir, "tmp", self.user_sar_features_name)
            )
            user_sar_features, user_sar_features_lab = generate_sar_feat_dates(
                sar_features_expr, sar_time_series, sar_user_features_raster
            )
            if self.write_outputs_flag is False:
                user_sar_features.Execute()
            else:
                if not Path(sar_user_features_raster).exists():
                    user_sar_features.ExecuteAndWriteOutput()
                if Path(sar_user_features_raster).exists():
                    user_sar_features = sar_user_features_raster
            dependancies.append(user_sar_features)
            s1_features.append(user_sar_features)
            features_labels += user_sar_features_lab
        sar_features_raster = str(
            Path(self.features_dir, "tmp", self.sar_features_name)
        )
        sar_features, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {"il": s1_features, "out": sar_features_raster, "ram": str(ram)},
        )
        return (sar_features, dependancies), features_labels
