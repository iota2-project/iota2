#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Landsat 8 for USGS class definition
"""
import logging
import multiprocessing as mp
import shutil
from pathlib import Path

from osgeo.gdal import Warp
from osgeo.gdalconst import GDT_Byte

from iota2.common.file_utils import (
    date_interval,
    file_search_and,
    get_date_l8usgs,
    get_raster_projection_epsg,
    get_raster_resolution,
)
from iota2.common.otb_app_bank import AvailableOTBApp, create_application, execute_app
from iota2.common.raster_utils import extract_raster_bands
from iota2.learning.utils import I2TemporalLabel
from iota2.typings.i2_types import I2FeaturesPipeline, OtbApp, OtbAppWithDep, OtbDep

LOGGER: logging.Logger = logging.getLogger("distributed.worker")

# in order to avoid issue 'No handlers could be found for logger...'
LOGGER.addHandler(logging.NullHandler())


class Landsat8UsgsCommon:
    """
    Landsat 8 for USGS class definition
    """

    name = "Landsat8USGSCommon"
    nodata_value = 0

    def __init__(
        self,
        tile_name: str,
        target_proj: int,
        all_tiles: str,
        image_directory: str,
        write_dates_stack: bool,
        extract_bands_flag: bool,
        output_target_dir: str,
        keep_bands: list,
        i2_output_path: str,
        temporal_res: int,
        auto_date_flag: bool,
        date_interp_min_user: str,
        date_interp_max_user: str,
        write_outputs_flag: bool,
        features: list[str],
        enable_gapfilling: bool,
        hand_features_flag: bool,
        hand_features: str,
        copy_input: bool,
        rel_refl: bool,
        keep_dupl: bool,
        acor_feat: bool,
        **kwargs: dict,
    ):
        """
        Construct the parent l8 usgs class
        """
        self.tile_name = tile_name
        self.target_proj = target_proj
        self.all_tiles = all_tiles
        self.l8_usgs_data = image_directory
        self.write_dates_stack = write_dates_stack
        # self.write_dates_stack = True
        self.extract_bands_flag = extract_bands_flag
        self.output_target_dir = output_target_dir
        self.keep_bands = keep_bands
        self.i2_output_path = i2_output_path
        self.temporal_res = temporal_res
        self.auto_date_flag = auto_date_flag
        self.date_interp_min_user = date_interp_min_user
        self.date_interp_max_user = date_interp_max_user
        self.write_outputs_flag = write_outputs_flag
        self.features = features
        self.enable_gapfilling = enable_gapfilling
        self.hand_features_flag = hand_features_flag
        self.hand_features = hand_features
        self.copy_input = copy_input
        self.rel_refl = rel_refl
        self.keep_dupl = keep_dupl
        self.acor_feat = acor_feat
        # self.mask_no_data = False  # option to mask no data values for all bands
        self.use_physical_units = False

        logger: logging.Logger = kwargs.get("logger", LOGGER)

        self.tile_directory = str(Path(self.l8_usgs_data) / tile_name)
        if len(file_search_and(self.tile_directory, True, ".tif")) > 0:
            self.extension = ".tif"
        else:
            self.extension = ".TIF"
        self.features_dir = str(Path(self.i2_output_path) / "features" / tile_name)

        if output_target_dir:
            self.output_preprocess_directory = str(Path(output_target_dir) / tile_name)
            if not Path(self.output_preprocess_directory).exists():
                try:
                    Path(self.output_preprocess_directory).mkdir()
                except OSError:
                    logger.warning(
                        f"Unable to create directory"
                        f"{self.output_preprocess_directory}"
                    )
        else:
            self.output_preprocess_directory = self.tile_directory

        # class specific
        self.date_position = 3  # if date's name split by "_"

        self.masks_rules = {
            f"QA_PIXEL{self.extension}": 0,
            f"QA_RADSAT{self.extension}": 0,
        }

        self.border_pos = 2

        self.extracted_bands: list[tuple[str, int]] | None = None
        # output's names
        self.footprint_name = f"{self.__class__.name}_{tile_name}_footprint.tif"

        # ref_image_name = f"{self.__class__.name}_{tile_name}_reference.tif"
        ref_image_name = f"Landsat8USGS_{tile_name}_reference.tif"
        self.ref_image = str(
            Path(self.i2_output_path) / "features" / tile_name / "tmp" / ref_image_name
        )

        self.time_series_name = f"{self.__class__.name}_{tile_name}_TS.tif"
        self.time_series_masks_name = f"{self.__class__.name}_{tile_name}_MASKS.tif"
        self.time_series_gapfilling_name = f"{self.__class__.name}_{tile_name}_TSG.tif"
        self.features_names = f"{self.__class__.name}_{tile_name}_Features.tif"
        # about gapFilling interpolations
        self.input_dates = f"{self.__class__.name}_{tile_name}_input_dates.txt"
        self.interpolated_dates = (
            f"{self.__class__.name}_{tile_name}_interpolation_dates.txt"
        )

        self.working_resolution = kwargs["working_resolution"]

        # manage 'get_raw'
        self.get_time_series_raw = self.get_time_series
        self.get_time_series_masks_raw = self.get_time_series_masks

        self.get_time_series_gapfilling_raw = self.get_time_series_gapfilling

        # Initialize variables coming from subclasses
        self.suffix = ""
        self.masks_date_suffix = ""
        self.stack_band_position = [""]

    def get_time_series_gapfilling(self, ram: int = 128) -> I2FeaturesPipeline:
        """
        Get time series gapfilling. Defined in each subclass.
        """
        raise NotImplementedError("Not implemented for the parent class")

    def build_stack_date_name(self, date_dir: str) -> str:
        """
        Build stack date name. Defined in each subclass.
        """
        raise NotImplementedError("Not implemented for the parent class")

    def get_bands_names(self, date_dir: str) -> list[str]:
        """
        Get bands names from sensor's input images names. Defined in each subclass.
        """
        raise NotImplementedError("Not implemented for the parent class")

    @staticmethod
    def get_conversion_expression(**kwargs: str) -> str:
        """
        Returns the OTB bandmath expression to convert a raster from digital number to reflectance
        Not in use yet. Defined in each subclass. In the case of IR, the method requires two
        additional arguments: date_directory and band_name
        """
        raise NotImplementedError("Not implemented for the parent class")

    def preprocess_date_masks(
        self,
        date_dir: str,
        out_prepro: str,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> OtbAppWithDep | tuple[str, None]:
        """
        Preprocess date masks. Defined in each subclass.
        """
        raise NotImplementedError("Not implemented for the parent class")

    def sort_dates_directories(self, dates_directories: list[str]) -> list[str]:
        """Sort dates directories"""
        return sorted(
            dates_directories,
            key=lambda x: Path(x).name.split("_")[self.date_position],
        )

    def get_available_dates(self) -> list[str]:
        """
        return sorted available dates
        """
        pattern = f"{self.suffix}{self.extension}"

        stacks = sorted(
            file_search_and(self.output_preprocess_directory, True, f"{pattern}"),
            key=lambda x: Path(x).name.split("_")[self.date_position],
        )
        return stacks

    def get_dates_directories(self) -> list[str]:
        """call before any processing

        TODO : add data consistency
        """
        input_dates = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]
        return input_dates

    def get_available_dates_masks(self) -> list[str]:
        """
        return sorted available masks
        """
        pattern = f"{self.masks_date_suffix}{self.extension}"
        masks = sorted(
            file_search_and(self.output_preprocess_directory, True, f"{pattern}"),
            key=lambda x: Path(x).name.split("_")[self.date_position],
        )
        return masks

    def check_out_stack(self, out_stack: str) -> bool:
        """detect if the output stack is consistent with user demand (resolution and projection)"""
        stack_consistent = False
        stack_ref_res_x, stack_ref_res_y = get_raster_resolution(out_stack)
        stack_ref_projection = get_raster_projection_epsg(out_stack)

        same_res = True
        if self.working_resolution:
            same_res = abs(stack_ref_res_x) == abs(self.working_resolution[0]) and abs(
                stack_ref_res_y
            ) == abs(self.working_resolution[1])

        if int(stack_ref_projection) == int(self.target_proj) and same_res:
            stack_consistent = True
            out_dir_roi = str(Path(self.ref_image).parent)
            Path(out_dir_roi).mkdir(parents=True, exist_ok=True)
            if Path(self.ref_image).exists():
                ref_res_x, ref_res_y = get_raster_resolution(self.ref_image)
                ref_projection = get_raster_projection_epsg(self.ref_image)
                not_same_res_ref = False
                if self.working_resolution:
                    not_same_res_ref = abs(ref_res_x) != abs(
                        self.working_resolution[0]
                    ) or abs(ref_res_y) != abs(self.working_resolution[1])
                if int(ref_projection) != int(self.target_proj) or not_same_res_ref:
                    extract_raster_bands(out_stack, self.ref_image, [1])
            else:
                extract_raster_bands(out_stack, self.ref_image, [1])  # write ref image
        return stack_consistent

    def preprocess_date(
        self,
        date_dir: str,
        out_prepro: str,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> dict[str, OtbApp] | str:
        """
        preprocess date
        """
        # pylint: disable=assignment-from-no-return; overridden in subclasses to return a value
        date_stack_name = self.build_stack_date_name(date_dir)
        logger.debug(f"preprocessing {date_dir}")
        out_stack = str(Path(date_dir) / date_stack_name)
        if out_prepro:
            date_dir_name = Path(date_dir).name
            out_dir = str(Path(out_prepro) / date_dir_name)
            try:
                Path(out_dir).mkdir(exist_ok=True, parents=True)
            except OSError:
                logger.warning(f"{out_dir} already exists")
            out_stack = str(Path(out_dir) / date_stack_name)

        if Path(out_stack).exists():
            consistent = self.check_out_stack(out_stack)
            if consistent:
                return out_stack

        out_stack_processing = out_stack
        if working_dir:
            out_stack_processing = str(Path(working_dir) / date_stack_name)

        # get bands
        # pylint: disable=assignment-from-no-return; overridden in subclasses to return a value
        date_bands = self.get_bands_names(date_dir)

        # tile reference image generation
        base_ref = date_bands[0]
        logger.info(f"reference image generation {self.ref_image} from {base_ref}")
        Path(self.ref_image).parent.mkdir(parents=True, exist_ok=True)
        base_ref_projection = get_raster_projection_epsg(base_ref)
        base_ref_res_x, base_ref_res_y = get_raster_resolution(base_ref)
        if self.working_resolution:
            base_ref_res_x = self.working_resolution[0]
            base_ref_res_y = self.working_resolution[1]
        if not Path(self.ref_image).exists():
            Warp(
                self.ref_image,
                base_ref,
                multithread=True,
                format="GTiff",
                xRes=base_ref_res_x,
                yRes=base_ref_res_y,
                outputType=GDT_Byte,
                srcSRS=f"EPSG:{base_ref_projection}",
                dstSRS=f"EPSG:{self.target_proj}",
            )

        # reproject / resample
        bands_proj = {}
        all_reproj = []
        for band, band_name in zip(date_bands, self.stack_band_position):
            superimp, _ = create_application(
                AvailableOTBApp.SUPERIMPOSE,
                {"inr": self.ref_image, "inm": band, "ram": str(ram)},
            )
            bands_proj[band_name] = superimp
            all_reproj.append(superimp)
        # convert to physical units
        if self.use_physical_units:
            bands_converted = {}
            all_converted = []
            for band_name, app in bands_proj.items():
                conversion_expression = self.get_conversion_expression(
                    date_dir=date_dir, band_name=date_dir
                )
                app.Execute()
                bandmath, _ = create_application(
                    AvailableOTBApp.BAND_MATH,
                    {
                        "il": app,
                        "exp": conversion_expression,
                        "ram": str(ram),
                        "pixType": "float",
                    },
                )
                bands_converted[band_name] = bandmath
                all_converted.append(bandmath)

        # write stack
        if self.write_dates_stack:
            same_proj = False
            same_res = True
            if Path(out_stack).exists():
                same_proj = int(get_raster_projection_epsg(out_stack)) == int(
                    self.target_proj
                )
                same_res = tuple(map(abs, get_raster_resolution(out_stack))) == (
                    base_ref_res_x,
                    abs(base_ref_res_y),
                )
            if not Path(out_stack).exists() or same_proj is False or not same_res:
                for reproj in all_reproj:
                    reproj.Execute()

                date_stack, _ = create_application(
                    AvailableOTBApp.CONCATENATE_IMAGES,
                    {
                        "il": all_reproj,
                        "ram": str(ram),
                        "pixType": "uint16",
                        "out": out_stack_processing,
                    },
                )
                # ~ date_stack.ExecuteAndWriteOutput()
                multi_proc = mp.Process(target=execute_app, args=[date_stack])
                multi_proc.start()
                multi_proc.join()
                if working_dir:
                    shutil.copy(out_stack_processing, out_stack)
                    Path(out_stack_processing).unlink()
        return bands_proj if self.write_dates_stack is False else out_stack

    def build_mask_date_name(self, date_dir: str) -> str:
        """build mask date name"""
        dir_name = Path(date_dir).name
        return f"{dir_name}_{self.masks_date_suffix}{self.extension}"

    def check_out_mask(self, out_mask: str) -> bool:
        """detect if the output stack is consistent with user demand (resolution and projection)"""
        mask_consistent = False
        mask_ref_res_x, mask_ref_res_y = get_raster_resolution(out_mask)
        mask_ref_projection = get_raster_projection_epsg(out_mask)

        same_res = True
        if self.working_resolution:
            same_res = abs(mask_ref_res_x) == abs(self.working_resolution[0]) and abs(
                mask_ref_res_y
            ) == abs(self.working_resolution[1])

        if int(mask_ref_projection) == int(self.target_proj) and same_res:
            mask_consistent = True
            out_dir_roi = str(Path(self.ref_image).parent)
            Path(out_dir_roi).mkdir(parents=True, exist_ok=True)
            if Path(self.ref_image).exists():
                ref_res_x, ref_res_y = get_raster_resolution(self.ref_image)
                ref_projection = get_raster_projection_epsg(self.ref_image)
                not_same_res_ref = False
                if self.working_resolution:
                    not_same_res_ref = abs(ref_res_x) != abs(
                        self.working_resolution[0]
                    ) or abs(ref_res_y) != abs(self.working_resolution[1])
                if int(ref_projection) != int(self.target_proj) or not_same_res_ref:
                    extract_raster_bands(out_mask, self.ref_image, [1])
            else:
                extract_raster_bands(out_mask, self.ref_image, [1])
        return mask_consistent

    def get_date_from_name(self, product_name: str) -> str:
        """
        get_date_from_name
        """
        return product_name.split("_")[self.date_position]

    def preprocess(
        self,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> dict[str, dict[str, OtbApp]]:
        """
        preprocess
        """
        input_dates = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]
        input_dates = self.sort_dates_directories(input_dates)
        preprocessed_dates = {}
        for date in input_dates:
            # pylint: disable=assignment-from-no-return; overridden in subclasses to return a value
            data_prepro = self.preprocess_date(
                date, self.output_preprocess_directory, working_dir, ram, logger
            )
            data_mask = self.preprocess_date_masks(
                date, self.output_preprocess_directory, working_dir, ram, logger
            )
            current_date = self.get_date_from_name(Path(date).name)
            # manage date duplicate
            if current_date in preprocessed_dates:
                current_date = f"{current_date}.1"
            preprocessed_dates[current_date] = {"data": data_prepro, "mask": data_mask}
        return preprocessed_dates

    def footprint(self, ram: int = 128) -> OtbAppWithDep:
        """
        compute footprint of images
        """

        footprint_dir = str(Path(self.features_dir) / "tmp")
        Path(footprint_dir).mkdir(parents=True, exist_ok=True)
        footprint_out = str(Path(footprint_dir) / self.footprint_name)

        input_dates = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]
        input_dates = self.sort_dates_directories(input_dates)
        # get date's footprint

        # superimpose QA
        app_dep = []
        all_qa_files_superimposed = []
        all_super_impose_apps = []
        all_qa_files = []
        for date in input_dates:
            qa_file = file_search_and(date, True, f"QA_PIXEL{self.extension}")[0]
            all_qa_files.append(qa_file)

        for qa_file in all_qa_files:
            superimpose_app, _ = create_application(
                AvailableOTBApp.SUPERIMPOSE,
                {
                    "inr": self.ref_image,
                    "inm": qa_file,
                    "ram": str(ram),
                },
            )
            superimpose_app.Execute()
            app_dep.extend([_, superimpose_app])
            all_super_impose_apps.append(superimpose_app)

            all_qa_files_superimposed.append(
                superimpose_app.GetParameterOutputImage("out")
            )

        expr = " + ".join(
            f"(im{i + 1}b1>1?1:0)" for i in range(len(all_super_impose_apps))
        )
        expr = f"({expr})>=2?1:0"  # pixel seen at least twice

        border, _ = create_application(
            AvailableOTBApp.BAND_MATH,
            {"il": all_super_impose_apps, "exp": expr, "ram": str(ram)},
        )

        app_dep.append(border)

        border.Execute()

        reference_raster = self.ref_image
        # superimpose footprint
        superimp, _ = create_application(
            AvailableOTBApp.SUPERIMPOSE,
            {
                "inr": reference_raster,
                "inm": border,
                "out": footprint_out,
                "pixType": "uint8",
                "ram": str(ram),
            },
        )

        superimp.Execute()

        # needed to travel through iota2's library
        app_dep.append(_)

        return superimp, app_dep

    def write_interpolation_dates_file(
        self, write: bool = True
    ) -> tuple[str, list[str]]:
        """
        TODO : mv to base-class
        """
        interp_date_dir = str(Path(self.features_dir) / "tmp")
        Path(interp_date_dir).mkdir(parents=True, exist_ok=True)
        interp_date_file = str(Path(interp_date_dir) / self.interpolated_dates)
        # get dates in the whole S2 data-set
        date_interp_min, date_interp_max = get_date_l8usgs(
            self.l8_usgs_data, self.all_tiles.split(" ")
        )

        # force dates
        if not self.auto_date_flag:
            date_interp_min = self.date_interp_min_user
            date_interp_max = self.date_interp_max_user

        dates = [
            str(date).replace("-", "")
            for date in date_interval(
                date_interp_min, date_interp_max, self.temporal_res
            )
        ]
        if not Path(interp_date_file).exists() and write:
            with open(
                interp_date_file, "w", encoding="UTF-8"
            ) as interpolation_date_file:
                interpolation_date_file.write("\n".join(dates))
        return interp_date_file, dates

    def write_dates_file(self) -> tuple[str, list[str]]:
        """
        write dates file
        """
        input_dates_dir = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]
        date_file = str(Path(self.features_dir) / "tmp" / self.input_dates)

        all_available_dates = [
            int(Path(date).name.split("_")[self.date_position])
            for date in input_dates_dir
        ]
        all_available_dates = sorted(all_available_dates)
        # sort requires int but input_date_file requires str
        all_available_dates_str = [str(x) for x in all_available_dates]

        if not Path(date_file).exists():
            with open(date_file, "w", encoding="UTF-8") as input_date_file:
                input_date_file.write("\n".join(all_available_dates_str))

        return date_file, all_available_dates_str

    def get_time_series(self, ram: int = 128) -> I2FeaturesPipeline:
        """
        TODO : be able of using a date interval
        Return
        ------
            list
                [(otb_Application, some otb's objects), time_series_labels]
                Functions dealing with otb's application instance has to
                returns every objects in the pipeline
        """
        # needed to travel through iota2's library
        app_dep = []

        preprocessed_dates = self.preprocess(working_dir=None, ram=ram)

        use_stacks = False
        first_key = next(iter(preprocessed_dates))
        if isinstance(preprocessed_dates[first_key]["data"], str):
            stacks_exists = []
            for _, stack in preprocessed_dates.items():
                stacks_exists.append(Path(stack["data"]).exists())
            use_stacks = all(stacks_exists)

        if self.write_dates_stack is False and not use_stacks:
            print("else")
            dates_concatenation = []
            for _, dico_date in list(preprocessed_dates.items()):
                for _, reproj_date in list(dico_date["data"].items()):
                    dates_concatenation.append(reproj_date)
                    reproj_date.Execute()
                    app_dep.append(reproj_date)
        else:
            print("else")
            dates_concatenation = self.get_available_dates()

        time_series_dir = str(Path(self.features_dir) / "tmp")
        Path(time_series_dir).mkdir(parents=True, exist_ok=True)
        times_series_raster = str(Path(time_series_dir) / self.time_series_name)

        dates_time_series, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {
                "il": dates_concatenation,
                "out": times_series_raster,
                "pixType": "float",
                "ram": str(ram),
            },
        )
        _, dates_in = self.write_dates_file()

        # build labels
        features_labels = [
            I2TemporalLabel(
                sensor_name=self.__class__.name, feat_name=band_name, date=date
            )
            for date in dates_in
            for band_name in self.stack_band_position
        ]

        # if not all bands must be used
        if self.extracted_bands:
            app_dep.append(dates_time_series)
            (dates_time_series, features_labels) = self.extract_bands_time_series(
                dates_time_series,
                dates_in,
                len(self.stack_band_position),
                self.extracted_bands,
                ram,
            )
        return (dates_time_series, app_dep), features_labels

    def extract_bands_time_series(
        self,
        dates_time_series: OtbApp,
        dates_in: list[str],
        comp: int,
        extract_bands: list,
        ram: int,
    ) -> tuple[OtbApp, I2TemporalLabel]:
        """
        TODO : mv to base class ?
        extract_bands : list
             [('bandName', band_position), ...]
        comp : number of bands in original stack
        """
        nb_dates = len(dates_in)
        channels_interest = []
        for date_number in range(nb_dates):
            for _, band_position in extract_bands:
                channels_interest.append(band_position + int(date_number * comp))

        features_labels = [
            I2TemporalLabel(
                sensor_name=self.__class__.name, feat_name=band_name, date=date
            )
            for date in dates_in
            for band_name, band_pos in extract_bands
        ]
        channels_list = [f"Channel{channel}" for channel in channels_interest]
        dates_time_series.Execute()
        extract, _ = create_application(
            AvailableOTBApp.EXTRACT_ROI,
            {
                "in": dates_time_series,
                "cl": channels_list,
                "ram": str(ram),
                "out": dates_time_series.GetParameterString("out"),
            },
        )
        return extract, features_labels

    def get_time_series_masks(self, ram: int = 128) -> tuple[OtbApp, OtbDep, int]:
        """
        TODO : be able of using a date interval
        Return
        ------
            list
                [(otb_Application, some otb's objects), time_series_labels]
                Functions dealing with otb's application instance has to
                returns every objects in the pipeline
        """

        # needed to travel through iota2's library
        app_dep = []

        preprocessed_dates = self.preprocess(working_dir=None, ram=ram)

        dates_masks = []
        if self.write_dates_stack is False:
            for _, dico_date in list(preprocessed_dates.items()):
                mask_app, mask_app_dep = dico_date["mask"]
                if not isinstance(mask_app, str):
                    mask_app.Execute()
                dates_masks.append(mask_app)
                app_dep.append(mask_app)
                app_dep.append(mask_app_dep)
        else:
            dates_masks = self.get_available_dates_masks()

        time_series_dir = str(Path(self.features_dir) / "tmp")
        Path(time_series_dir).mkdir(parents=True, exist_ok=True)
        times_series_mask = str(Path(time_series_dir) / self.time_series_masks_name)
        dates_time_series_mask, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {
                "il": dates_masks,
                "out": times_series_mask,
                "pixType": "uint8",
                "ram": str(ram),
            },
        )
        return dates_time_series_mask, app_dep, len(dates_masks)
