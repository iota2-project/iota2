#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
The landsat 8 sensor class
"""
import glob
import logging
import multiprocessing as mp
import shutil
from pathlib import Path

from osgeo.gdal import Warp
from osgeo.gdalconst import GDT_Byte

# from iota2.common.file_utils import (
#     date_interval,
#     file_search_and,
#     get_date_s2,
#     get_iota2_project_dir,
#     get_raster_projection_epsg,
#     get_raster_resolution,
# )
import iota2.common.file_utils as fut
import iota2.common.otb_app_bank as oap
from iota2.common.compression_options import delete_compression_suffix
from iota2.configuration_files import read_config_file as rcf
from iota2.learning.utils import I2TemporalLabel
from iota2.typings.i2_types import I2FeaturesPipeline, OtbApp, OtbAppWithDep, OtbDep

LOGGER = logging.getLogger("distributed.worker")

# in order to avoid issue 'No handlers could be found for logger...'
LOGGER.addHandler(logging.NullHandler())


class Landsat8:
    """
    Landsat 8 sensor
    """

    name = "Landsat8"
    nodata_value = -10000

    def __init__(
        self,
        tile_name: str,
        target_proj: int,
        all_tiles: str,
        image_directory: str,
        write_dates_stack: bool,
        extract_bands_flag: bool,
        output_target_dir: str,
        keep_bands: list,
        i2_output_path: str,
        temporal_res: int,
        auto_date_flag: bool,
        date_interp_min_user: str,
        date_interp_max_user: str,
        write_outputs_flag: bool,
        features: list[str],
        enable_gapfilling: bool,
        hand_features_flag: bool,
        hand_features: str,
        copy_input: bool,
        rel_refl: bool,
        keep_dupl: bool,
        acor_feat: bool,
        **kwargs: dict,
    ):
        """
        initialize the class landsat_8
        """

        self.tile_name = tile_name

        cfg_sensors_file = str(
            Path(fut.get_iota2_project_dir(), "iota2", "sensors", "sensors.cfg")
        )
        cfg_sensors = rcf.ReadInternalConfigFile(cfg_sensors_file)

        # running attributes

        self.target_proj = target_proj
        self.all_tiles = all_tiles
        self.l8_data = image_directory
        self.tile_directory = str(Path(self.l8_data) / tile_name)
        self.write_dates_stack = write_dates_stack
        self.extract_bands_flag = extract_bands_flag
        self.keep_bands = keep_bands
        self.i2_output_path = i2_output_path
        self.temporal_res = temporal_res
        self.auto_date_flag = auto_date_flag
        self.date_interp_min_user = date_interp_min_user
        self.date_interp_max_user = date_interp_max_user
        self.write_outputs_flag = write_outputs_flag
        self.features = features
        self.enable_gapfilling = enable_gapfilling
        self.hand_features_flag = hand_features_flag
        self.hand_features = hand_features
        self.copy_input = copy_input
        self.rel_refl = rel_refl
        self.keep_dupl = keep_dupl
        self.acor_feat = acor_feat
        self.features_dir = str(Path(self.i2_output_path) / "features" / tile_name)

        if output_target_dir:
            self.output_preprocess_directory = str(Path(output_target_dir, tile_name))
            if not Path(self.output_preprocess_directory).exists():
                try:
                    Path(self.output_preprocess_directory).mkdir()
                except OSError:
                    print(
                        "Impossible to create directory :"
                        f" {self.output_preprocess_directory}"
                    )
        else:
            self.output_preprocess_directory = self.tile_directory

        self.struct_path_masks = cfg_sensors.get_param("Landsat8", "arbomask")

        # sensors attributes
        self.native_res = 30
        self.data_type = "FRE"
        self.suffix = "STACK"
        self.masks_date_suffix = "BINARY_MASK"
        self.date_position = 1  # if date's name split by "_"
        self.masks_rules = {
            "CLM_XS.tif": 0,
            "SAT_XS.tif": 0,
            "EDG_XS.tif": 0,
        }  # 0 mean data, else noData
        self.border_pos = 2
        self.features_names_list = ["NDVI", "NDWI", "Brightness"]

        # define bands to get and their order
        self.stack_band_position = ["B1", "B2", "B3", "B4", "B5", "B6", "B7"]
        self.extracted_bands = None
        if extract_bands_flag:
            # TODO check every mandatory bands still selected
            # -> def check_mandatory bands() return True/False
            self.extracted_bands = [
                (band_name, band_position + 1)
                for band_position, band_name in enumerate(self.stack_band_position)
                if band_name in self.keep_bands
            ]

        # output's names
        self.footprint_name = f"{self.__class__.name}_{tile_name}_" "footprint.tif"
        ref_image_name = f"{self.__class__.name}_{tile_name}_reference.tif"
        self.ref_image = str(
            Path(self.i2_output_path, "features", tile_name, "tmp", ref_image_name)
        )
        self.time_series_name = f"{self.__class__.name}_{tile_name}_TS.tif"
        self.time_series_masks_name = f"{self.__class__.name}_{tile_name}" "_MASKS.tif"
        self.time_series_gapfilling_name = (
            f"{self.__class__.name}_" "{tile_name}_TSG.tif"
        )
        self.features_names = f"{self.__class__.name}_{tile_name}_Features.tif"
        # about gapFilling interpolations
        self.input_dates = f"{self.__class__.name}_{tile_name}_input_dates.txt"
        self.interpolated_dates = (
            f"{self.__class__.name}_{tile_name}" "_interpolation_dates.txt"
        )
        self.working_resolution = kwargs["working_resolution"]

        # manage 'get_raw'
        self.get_time_series_raw = self.get_time_series
        self.get_time_series_masks_raw = self.get_time_series_masks
        self.get_time_series_gapfilling_raw = self.get_time_series_gapfilling

    def get_date_from_name(self, product_name: str) -> str:
        """
        return the date from the name
        """
        return product_name.split("_")[self.date_position].split("-")[0]

    def sort_dates_directories(self, dates_directories: list[str]) -> list[str]:
        """
        sort the dates
        """
        return sorted(
            dates_directories,
            key=lambda x: int(
                Path(x).name.split("_")[self.date_position].split("-")[0]
            ),
        )

    def get_dates_directories(self) -> list[str]:
        """call before any treatements

        TODO : add data consistency
        """
        input_dates = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]
        return input_dates

    def get_available_dates(self) -> list[str]:
        """
        return sorted available dates
        """
        stacks = sorted(
            fut.file_search_and(
                self.output_preprocess_directory, True, f"{self.suffix}.tif"
            ),
            key=lambda x: int(
                Path(x).name.split("_")[self.date_position].split("-")[0]
            ),
        )
        return stacks

    def get_available_dates_masks(self) -> list[str]:
        """
        return sorted available masks
        """
        masks = sorted(
            fut.file_search_and(
                self.output_preprocess_directory, True, f"{self.masks_date_suffix}.tif"
            ),
            key=lambda x: int(
                Path(x).name.split("_")[self.date_position].split("-")[0]
            ),
        )
        return masks

    def build_stack_date_name(self, date_dir: str) -> str:
        """
        build the stack date name
        """
        b2_name = Path(
            fut.file_search_and(date_dir, True, f"{self.data_type}_B2.tif")[0]
        ).name

        return b2_name.replace(
            f"{self.data_type}_B2.tif", f"{self.data_type}_{self.suffix}.tif"
        )

    def preprocess_date(
        self,
        date_dir: str,
        out_prepro: str,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> dict[str, OtbApp] | str:
        """
        Preprocess each date
        """
        # manage directories
        date_stack_name = self.build_stack_date_name(date_dir)
        logger.debug(f"preprocessing {date_dir}")
        out_stack = str(Path(date_dir) / date_stack_name)
        if out_prepro:
            date_dir_name = Path(date_dir).name
            out_dir = str(Path(out_prepro) / date_dir_name)
            if not Path(out_dir).exists():
                try:
                    Path(out_dir).mkdir()
                except OSError:
                    logger.warning(f"{out_dir} already exists")
            out_stack = str(Path(out_dir) / date_stack_name)

        out_stack_processing = out_stack
        if working_dir:
            out_stack_processing = str(Path(working_dir) / date_stack_name)

        # get bands
        date_bands = [
            fut.file_search_and(date_dir, True, f"{self.data_type}_{bands_name}.tif")[0]
            for bands_name in self.stack_band_position
        ]

        # tile reference image generation
        base_ref = date_bands[0]
        Path(self.ref_image).parent.mkdir(parents=True, exist_ok=True)
        base_ref_projection = fut.get_raster_projection_epsg(base_ref)
        base_ref_res_x, base_ref_res_y = fut.get_raster_resolution(base_ref)
        if self.working_resolution:
            base_ref_res_x = self.working_resolution[0]
            base_ref_res_y = self.working_resolution[1]

        if not Path(self.ref_image).exists():
            logger.info(f"reference image generation {self.ref_image} from {base_ref}")
            Warp(
                self.ref_image,
                base_ref,
                multithread=True,
                format="GTiff",
                xRes=base_ref_res_x,
                yRes=base_ref_res_y,
                outputType=GDT_Byte,
                srcSRS=f"EPSG:{base_ref_projection}",
                dstSRS=f"EPSG:{self.target_proj}",
            )

        # reproject / resample
        bands_proj = {}
        all_reproj = []
        for band, band_name in zip(date_bands, self.stack_band_position):
            superimp, _ = oap.create_application(
                oap.AvailableOTBApp.SUPERIMPOSE,
                {"inr": self.ref_image, "inm": band, "ram": str(ram)},
            )
            bands_proj[band_name] = superimp
            all_reproj.append(superimp)

        if self.write_dates_stack:
            same_proj = False
            same_res = True
            if Path(out_stack).exists():
                same_proj = int(fut.get_raster_projection_epsg(out_stack)) == int(
                    self.target_proj
                )
                same_res = tuple(map(abs, fut.get_raster_resolution(out_stack))) == (
                    base_ref_res_x,
                    abs(base_ref_res_y),
                )
            if not Path(out_stack).exists() or same_proj is False or not same_res:
                for reproj in all_reproj:
                    reproj.Execute()
                date_stack, _ = oap.create_application(
                    oap.AvailableOTBApp.CONCATENATE_IMAGES,
                    {
                        "il": all_reproj,
                        "ram": str(ram),
                        "pixType": "int16",
                        "out": out_stack_processing,
                    },
                )
                date_stack.ExecuteAndWriteOutput()
                # multi_proc = mp.Process(target=oap.execute_app, args=[date_stack])
                # multi_proc.start()
                # multi_proc.join()
                if working_dir:
                    shutil.copy(out_stack_processing, out_stack)
                    Path(out_stack_processing).unlink()
        return bands_proj if self.write_dates_stack is False else out_stack

    def preprocess_date_masks(
        self,
        date_dir: str,
        out_prepro: str,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> OtbAppWithDep:
        """
        preprocess date mask
        """
        date_mask = []
        for mask_name, _ in list(self.masks_rules.items()):
            date_mask.append(
                glob.glob(str(Path(date_dir) / f"{self.struct_path_masks}{mask_name}"))[
                    0
                ]
            )

        # manage directories
        mask_dir = Path(date_mask[0]).parent
        if fut.is_dir_empty(mask_dir):
            logger.error(f"Mask directory {mask_dir} is empty !")
            raise FileNotFoundError(f"Mask directory {mask_dir} is empty !")
        logger.debug(f"preprocessing {mask_dir} masks")
        mask_name = Path(date_mask[0]).name.replace(
            list(self.masks_rules.items())[0][0],
            f"{self.masks_date_suffix}.tif",
        )
        out_mask = str(Path(mask_dir) / mask_name)
        if out_prepro:
            out_mask_dir = str(mask_dir).replace(
                str(Path(self.l8_data) / self.tile_name), str(Path(out_prepro))
            )

            Path(out_mask_dir).mkdir(parents=True, exist_ok=True)
            out_mask = str(Path(out_mask_dir) / mask_name)

        out_mask_processing = out_mask
        if working_dir:
            out_mask_processing = str(Path(working_dir) / mask_name)

        # build binary mask
        expr = "+".join([f"im{cpt + 1}b1" for cpt in range(len(date_mask))])
        expr = f"({expr})==0?0:1"
        binary_mask_rule, _ = oap.create_application(
            oap.AvailableOTBApp.BAND_MATH, {"il": date_mask, "exp": expr}
        )
        binary_mask_rule.Execute()
        # reproject using reference image
        superimp, _ = oap.create_application(
            oap.AvailableOTBApp.SUPERIMPOSE,
            {
                "inr": self.ref_image,
                "inm": binary_mask_rule,
                "interpolator": "nn",
                "out": out_mask_processing,
                "pixType": "uint8",
                "ram": str(ram),
            },
        )

        # needed to travel through iota2's library
        app_dep = [binary_mask_rule]

        if self.write_dates_stack:
            same_proj = False
            same_res = True
            if Path(out_mask).exists():
                same_proj = int(fut.get_raster_projection_epsg(out_mask)) == int(
                    self.target_proj
                )
                same_res = fut.get_raster_resolution(
                    out_mask
                ) == fut.get_raster_resolution(self.ref_image)
            if not Path(out_mask).exists() or same_proj is False or not same_res:
                # ~ superimp.ExecuteAndWriteOutput()
                multi_proc = mp.Process(target=oap.execute_app, args=[superimp])
                multi_proc.start()
                multi_proc.join()
                if working_dir:
                    shutil.copy(out_mask_processing, out_mask)
                    Path(out_mask_processing).unlink()

        return superimp, app_dep

    def preprocess(
        self,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,  # pylint: disable=unused-argument
    ) -> dict[str, dict[str, OtbApp]]:
        """
        preprocess
        """
        input_dates = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]
        input_dates = self.sort_dates_directories(input_dates)

        preprocessed_dates = {}
        for date in input_dates:
            data_prepro = self.preprocess_date(
                date, self.output_preprocess_directory, working_dir, ram
            )
            data_mask = self.preprocess_date_masks(
                date, self.output_preprocess_directory, working_dir, ram
            )
            current_date = self.get_date_from_name(Path(date).name)
            # manage date duplicate
            if current_date in preprocessed_dates:
                current_date = f"{current_date}.1"
            preprocessed_dates[current_date] = {"data": data_prepro, "mask": data_mask}
        return preprocessed_dates

    def footprint(self, ram: int = 128) -> OtbAppWithDep:
        """
        compute the footprint
        """
        footprint_dir = str(Path(self.features_dir) / "tmp")
        Path(footprint_dir).mkdir(parents=True, exist_ok=True)
        footprint_out = str(Path(footprint_dir) / self.footprint_name)

        input_dates = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]
        input_dates = self.sort_dates_directories(input_dates)

        # get date's footprint
        date_edge = []
        for date_dir in input_dates:
            date_edge.append(
                glob.glob(
                    str(
                        Path(
                            date_dir,
                            f"{self.struct_path_masks}"
                            f"{list(self.masks_rules.keys())[self.border_pos]}",
                        )
                    )
                )[0]
            )

        expr = " || ".join(f"1 - im{i + 1}b1" for i in range(len(date_edge)))
        l8_border, _ = oap.create_application(
            oap.AvailableOTBApp.BAND_MATH,
            {"il": date_edge, "exp": expr, "ram": str(ram)},
        )
        l8_border.Execute()

        reference_raster = self.ref_image

        # superimpose footprint
        print(f"\n\n\nReference raster: {reference_raster}\n\n\n")
        superimp, _ = oap.create_application(
            oap.AvailableOTBApp.SUPERIMPOSE,
            {
                "inr": reference_raster,
                "inm": l8_border,
                "out": footprint_out,
                "pixType": "uint8",
                "ram": str(ram),
            },
        )

        # needed to travel through iota2's library
        app_dep = [l8_border, _]

        return superimp, app_dep

    def write_dates_file(self) -> tuple[str, list[str]]:
        """
        write dates file
        """
        input_dates_dir = [
            str(Path(self.tile_directory) / cdir)
            for cdir in Path(self.tile_directory).iterdir()
        ]

        date_file = str(Path(self.features_dir) / "tmp" / self.input_dates)
        all_available_dates = [
            int(Path(date).name.split("_")[self.date_position].split("-")[0])
            for date in input_dates_dir
        ]
        all_available_dates = sorted(all_available_dates)
        all_available_dates_str = [str(x) for x in all_available_dates]
        if not Path(date_file).exists():
            with open(date_file, "w", encoding="utf-8") as input_date_file:
                input_date_file.write("\n".join(all_available_dates_str))
        return date_file, all_available_dates_str

    def write_interpolation_dates_file(
        self, write: bool = True
    ) -> tuple[str, list[str]]:
        """
        TODO : mv to base-class
        """

        interp_date_dir = str(Path(self.features_dir) / "tmp")
        Path(interp_date_dir).mkdir(parents=True, exist_ok=True)
        interp_date_file = str(Path(interp_date_dir) / self.interpolated_dates)
        # get dates in the whole L8 data-set (get_date_s2 -> avail to L8)
        date_interp_min, date_interp_max = fut.get_date_s2(
            self.l8_data, self.all_tiles.split(" ")
        )
        # force dates
        if not self.auto_date_flag:
            date_interp_min = self.date_interp_min_user
            date_interp_max = self.date_interp_max_user

        dates = [
            str(date).replace("-", "")
            for date in fut.date_interval(
                date_interp_min, date_interp_max, self.temporal_res
            )
        ]
        if not Path(interp_date_file).exists() and write:
            with open(
                interp_date_file, "w", encoding="utf-8"
            ) as interpolation_date_file:
                interpolation_date_file.write("\n".join(dates))
        return interp_date_file, dates

    def get_time_series(self, ram: int = 128) -> I2FeaturesPipeline:
        """
        TODO : be able of using a date interval
        Return
        ------
            list
                [(otb_Application, some otb's objects), time_series_labels]
                Functions dealing with otb's application instance has to
                returns every objects in the pipeline
        """
        # needed to travel through iota2's library
        app_dep = []

        preprocessed_dates = self.preprocess(working_dir=None, ram=ram)

        if self.write_dates_stack is False:
            dates_concatenation = []
            for _, dico_date in list(preprocessed_dates.items()):
                for _, reproj_date in list(dico_date["data"].items()):
                    dates_concatenation.append(reproj_date)
                    reproj_date.Execute()
                    app_dep.append(reproj_date)
        else:
            dates_concatenation = self.get_available_dates()

        time_series_dir = str(Path(self.features_dir) / "tmp")
        Path(time_series_dir).mkdir(parents=True, exist_ok=True)
        times_series_raster = str(Path(time_series_dir) / self.time_series_name)
        dates_time_series, _ = oap.create_application(
            oap.AvailableOTBApp.CONCATENATE_IMAGES,
            {
                "il": dates_concatenation,
                "out": times_series_raster,
                "pixType": "int16",
                "ram": str(ram),
            },
        )
        _, dates_in = self.write_dates_file()

        # build labels
        features_labels = [
            I2TemporalLabel(
                sensor_name=self.__class__.name, feat_name=band_name, date=date
            )
            for date in dates_in
            for band_name in self.stack_band_position
        ]

        # if not all bands must be used
        if self.extracted_bands:
            app_dep.append(dates_time_series)
            (dates_time_series, features_labels) = self.extract_bands_time_series(
                dates_time_series,
                dates_in,
                len(self.stack_band_position),
                self.extracted_bands,
                ram,
            )
        return (dates_time_series, app_dep), features_labels

    def extract_bands_time_series(
        self,
        dates_time_series: OtbApp,
        dates_in: list[str],
        comp: int,
        extract_bands: list,
        ram: int,
    ) -> tuple[OtbApp, I2TemporalLabel]:
        """
        TODO : mv to base class ?
        extract_bands : list
             [('bandName', band_position), ...]
        comp : number of bands in original stack
        """

        nb_dates = len(dates_in)
        channels_interest = []
        for date_number in range(nb_dates):
            for _, band_position in extract_bands:
                channels_interest.append(band_position + int(date_number * comp))

        features_labels = [
            I2TemporalLabel(
                sensor_name=self.__class__.name, feat_name=band_name, date=date
            )
            for date in dates_in
            for band_name, band_pos in extract_bands
        ]
        channels_list = [f"Channel{channel}" for channel in channels_interest]

        dates_time_series.Execute()
        extract, _ = oap.create_application(
            oap.AvailableOTBApp.EXTRACT_ROI,
            {
                "in": dates_time_series,
                "cl": channels_list,
                "ram": str(ram),
                "out": delete_compression_suffix(
                    dates_time_series.GetParameterString("out")
                ),
            },
        )
        return extract, features_labels

    def get_time_series_masks(self, ram: int = 128) -> tuple[OtbApp, OtbDep, int]:
        """
        TODO : be able of using a date interval
        Return
        ------
            list
                [(otb_Application, some otb's objects), time_series_labels]
                Functions dealing with otb's application instance has to
                returns every objects in the pipeline
        """

        # needed to travel through iota2's library
        app_dep = []

        preprocessed_dates = self.preprocess(working_dir=None, ram=ram)

        dates_masks = []

        if self.write_dates_stack is False:
            for _, dico_date in list(preprocessed_dates.items()):
                mask_app, mask_app_dep = dico_date["mask"]
                mask_app.Execute()
                dates_masks.append(mask_app)
                app_dep.append(mask_app)
                app_dep.append(mask_app_dep)
        else:
            dates_masks = self.get_available_dates_masks()

        time_series_dir = str(Path(self.features_dir) / "tmp")
        Path(time_series_dir).mkdir(parents=True, exist_ok=True)
        times_series_mask = str(Path(time_series_dir) / self.time_series_masks_name)
        dates_time_series_mask, _ = oap.create_application(
            oap.AvailableOTBApp.CONCATENATE_IMAGES,
            {
                "il": dates_masks,
                "out": times_series_mask,
                "pixType": "uint8",
                "ram": str(ram),
            },
        )
        return dates_time_series_mask, app_dep, len(dates_masks)

    def get_time_series_gapfilling(self, ram: int = 128) -> I2FeaturesPipeline:
        """
        get time series gapfilling
        """
        gap_dir = str(Path(self.features_dir) / "tmp")
        Path(gap_dir).mkdir(parents=True, exist_ok=True)
        gap_out = str(Path(gap_dir) / self.time_series_gapfilling_name)

        dates_interp_file, dates_interp = self.write_interpolation_dates_file()
        dates_in_file, _ = self.write_dates_file()

        masks, masks_dep, _ = self.get_time_series_masks()
        (time_series, time_series_dep), _ = self.get_time_series()

        time_series.Execute()
        masks.Execute()

        comp = (
            len(self.stack_band_position)
            if not self.extracted_bands
            else len(self.extracted_bands)
        )

        gap, _ = oap.create_application(
            oap.AvailableOTBApp.IMAGE_TIME_SERIES_GAPFILLING,
            {
                "in": time_series,
                "mask": masks,
                "comp": str(comp),
                "it": "linear",
                "id": dates_in_file,
                "od": dates_interp_file,
                "out": gap_out,
                "ram": str(ram),
                "pixType": "int16",
            },
        )
        app_dep = [time_series, masks, masks_dep, time_series_dep]

        bands = self.stack_band_position
        if self.extracted_bands:
            bands = [band_name for band_name, band_pos in self.extracted_bands]

        features_labels = [
            I2TemporalLabel(
                sensor_name=self.__class__.name, feat_name=band_name, date=date
            )
            for date in dates_interp
            for band_name in bands
        ]
        return (gap, app_dep), features_labels

    def get_features_labels(
        self, dates: list[str], rel_refl: bool, keep_dupl: bool, copy_in: bool
    ) -> list[I2TemporalLabel]:
        """
        get features labels
        """
        if rel_refl and keep_dupl is False and copy_in is True:
            self.features_names_list = ["NDWI", "Brightness"]
        out_labels = []

        for feature in self.features_names_list:
            for date in dates:
                out_labels.append(
                    I2TemporalLabel(
                        sensor_name=self.__class__.name, feat_name=feature, date=date
                    )
                )
        return out_labels

    # pylint: disable=unused-argument; logger is used in user_features
    def get_features(
        self, ram: int = 128, logger: logging.Logger = LOGGER
    ) -> I2FeaturesPipeline:
        """
        get features
        """

        features_dir = str(Path(self.features_dir) / "tmp")
        Path(features_dir).mkdir(parents=True, exist_ok=True)
        features_out = str(Path(features_dir) / self.features_names)

        (
            (in_stack, in_stack_dep),
            in_stack_features_labels,
        ) = self.get_time_series_gapfilling()
        _, dates_enabled = self.write_interpolation_dates_file()

        if not self.enable_gapfilling:
            (in_stack, in_stack_dep), in_stack_features_labels = self.get_time_series()
            _, dates_enabled = self.write_dates_file()

        in_stack.Execute()
        app_dep = []
        if self.hand_features_flag:
            hand_features = self.hand_features
            comp = (
                len(self.stack_band_position)
                if not self.extracted_bands
                else len(self.extracted_bands)
            )
            (
                user_date_features,
                fields_userfeat,
                user_feat_date,
                stack,
            ) = oap.compute_user_features(
                in_stack, dates_enabled, comp, hand_features.split(",")
            )
            user_date_features.Execute()
            app_dep.append([user_date_features, user_feat_date, stack])

        if self.features:
            bands_avail = self.stack_band_position
            if self.extracted_bands:
                bands_avail = [band_name for band_name, _ in self.extracted_bands]
                # check mandatory bands
                if "B4" not in bands_avail:
                    raise Exception("red band (B4) is needed to compute features")
                if "B5" not in bands_avail:
                    raise Exception("nir band (B5) is needed to compute features")
                if "B6" not in bands_avail:
                    raise Exception("swir band (B6) is needed to compute features")
            feat_parameters = {
                "in": in_stack,
                "out": features_out,
                "comp": len(bands_avail),
                "red": bands_avail.index("B4") + 1,
                "nir": bands_avail.index("B5") + 1,
                "swir": bands_avail.index("B6") + 1,
                "copyinput": self.copy_input,
                "relrefl": self.rel_refl,
                "keepduplicates": self.keep_dupl,
                "acorfeat": self.acor_feat,
                "pixType": "int16",
                "ram": str(ram),
            }

            features_app, _ = oap.create_application(
                oap.AvailableOTBApp.IOTA2_FEATURES_EXTRACTION, feat_parameters
            )
            if self.copy_input is False:
                in_stack_features_labels = []
            features_labels = in_stack_features_labels + self.get_features_labels(
                dates_enabled, self.rel_refl, self.keep_dupl, self.copy_input
            )
        else:
            features_app = in_stack
            features_labels = in_stack_features_labels

        app_dep.append([in_stack, in_stack_dep])

        if self.hand_features_flag:
            features_app.Execute()
            app_dep.append(features_app)

            features_app, _ = oap.create_application(
                oap.AvailableOTBApp.CONCATENATE_IMAGES,
                {
                    "il": [features_app, user_date_features],
                    "out": features_out,
                    "ram": str(ram),
                },
            )
            features_labels += fields_userfeat
        return (features_app, app_dep), features_labels
