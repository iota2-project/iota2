# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
user_features class definition
"""

import logging
from pathlib import Path

from osgeo.gdal import Warp
from osgeo.gdalconst import GDT_Byte

from iota2.common.file_utils import (
    file_search_and,
    get_raster_n_bands,
    get_raster_projection_epsg,
    get_raster_resolution,
)
from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.learning.utils import I2Label
from iota2.sensors.sentinel2 import I2FeaturesPipeline, OtbAppWithDep
from iota2.typings.i2_types import OtbApp, OtbDep

LOGGER = logging.getLogger("distributed.worker")

# in order to avoid issue 'No handlers could be found for logger...'
LOGGER.addHandler(logging.NullHandler())


class UserFeatures:
    """user_feature class definition"""

    name = "userFeatures"
    nodata_value = -10000

    def __init__(
        self,
        tile_name: str,
        image_directory: str,
        i2_output_path: str,
        target_proj: int,
        patterns: list[str],
        **kwargs: dict,
    ):
        self.tile_name = tile_name
        self.user_feat_data = image_directory
        tile_dir_name = [
            dir_name.name
            for dir_name in Path(self.user_feat_data).iterdir()
            if tile_name in dir_name.name
        ][0]

        # run attributes
        self.tile_directory = str(Path(self.user_feat_data) / tile_dir_name)
        self.features_dir = str(Path(i2_output_path) / "features" / tile_name)
        self.target_proj = target_proj

        # sensors attributes
        self.data_type = patterns

        # output's names
        self.time_series_masks_name = f"{self.__class__.name}_{tile_name}_MASKS.tif"
        self.features_names = f"{self.__class__.name}_{tile_name}_Features.tif"
        self.footprint_name = f"{self.__class__.name}_{tile_name}_footprint.tif"
        ref_image_name = f"{self.__class__.name}_{tile_name}_reference.tif"
        self.ref_image = str(
            Path(i2_output_path) / "features" / tile_name / "tmp" / ref_image_name
        )
        self.working_resolution = kwargs["working_resolution"]

        # manage 'get_raw'
        self.get_time_series_raw = self.get_features
        self.get_time_series_gapfilling_raw = self.get_features
        self.get_time_series_masks_raw = self.get_time_series_masks

    def footprint(self, ram: int = 128, data_value: int = 1) -> OtbAppWithDep:
        """get footprint"""

        footprint_dir = str(Path(self.features_dir) / "tmp")
        Path(footprint_dir).mkdir(parents=True, exist_ok=True)
        footprint_out = str(Path(footprint_dir) / self.footprint_name)

        user_feature = file_search_and(self.tile_directory, True, self.data_type[0])

        # tile reference image generation
        base_ref = user_feature[0]
        LOGGER.info(f"reference image generation {self.ref_image} from {base_ref}")
        Path(self.ref_image).parent.mkdir(parents=True, exist_ok=True)
        base_ref_projection = get_raster_projection_epsg(base_ref)
        base_ref_res_x, base_ref_res_y = get_raster_resolution(base_ref)
        if self.working_resolution:
            base_ref_res_x = self.working_resolution[0]
            base_ref_res_y = self.working_resolution[1]
        if not Path(self.ref_image).exists():
            Warp(
                self.ref_image,
                base_ref,
                multithread=True,
                format="GTiff",
                xRes=base_ref_res_x,
                yRes=base_ref_res_y,
                outputType=GDT_Byte,
                srcSRS=f"EPSG:{base_ref_projection}",
                dstSRS=f"EPSG:{self.target_proj}",
            )

        # user features must not contains NODATA
        # -> "exp" : 'data_value' mean every data available
        footprint, _ = create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": self.ref_image,
                "out": footprint_out,
                "exp": str(data_value),
                "pixType": "uint8",
                "ram": str(ram),
            },
        )

        # needed to travel through iota2's library
        app_dep: list = []

        return footprint, app_dep

    def preprocess(
        self,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> None:
        """
        No specific preprocessing for the user features sensor
        """

    def get_time_series_masks(
        self, ram: int = 128, logger: logging.Logger = LOGGER
    ) -> tuple[OtbApp, OtbDep, int]:
        """
        Get time series masks
        """

        time_series_dir = str(Path(self.features_dir) / "tmp")
        Path(time_series_dir).mkdir(parents=True, exist_ok=True)
        times_series_mask = str(Path(time_series_dir) / self.time_series_masks_name)

        # check patterns
        for pattern in self.data_type:
            user_feature = file_search_and(self.tile_directory, True, pattern)
            if not user_feature:
                msg = f"WARNING : '{pattern}' not found in {self.tile_directory}"
                logger.error(msg)
                raise Exception(msg)
        nb_patterns = len(self.data_type)
        masks = []
        app_dep = []
        for _ in range(nb_patterns):
            dummy_mask, _ = self.footprint(data_value=0)
            dummy_mask.Execute()
            app_dep.append(dummy_mask)
            masks.append(dummy_mask)
        masks_stack, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {"il": masks, "out": times_series_mask, "ram": str(ram)},
        )
        return masks_stack, app_dep, nb_patterns

    def get_features(
        self, ram: int = 128, logger: logging.Logger = LOGGER
    ) -> I2FeaturesPipeline:
        """generate user features. Concatenates all of them"""
        features_dir = str(Path(self.features_dir) / "tmp")
        Path(features_dir).mkdir(parents=True, exist_ok=True)
        features_out = str(Path(features_dir) / self.features_names)

        user_features_found = []
        user_features_bands = []
        for pattern in self.data_type:
            user_feature = file_search_and(self.tile_directory, True, pattern)
            if user_feature:
                user_features_bands.append(get_raster_n_bands(user_feature[0]))
                user_features_found.append(user_feature[0])
            else:
                msg = f"WARNING : '{pattern}' not found in {self.tile_directory}"
                logger.error(msg)
                raise Exception(msg)

        user_feat_stack, _ = create_application(
            AvailableOTBApp.CONCATENATE_IMAGES,
            {"il": user_features_found, "ram": str(ram), "out": features_out},
        )
        base_ref = user_features_found[0]
        base_ref_projection = get_raster_projection_epsg(base_ref)
        base_ref_res_x, base_ref_res_y = get_raster_resolution(base_ref)
        if self.working_resolution:
            base_ref_res_x = self.working_resolution[0]
            base_ref_res_y = self.working_resolution[1]
        if not Path(self.ref_image).exists():
            Warp(
                self.ref_image,
                base_ref,
                multithread=True,
                format="GTiff",
                xRes=base_ref_res_x,
                yRes=base_ref_res_y,
                outputType=GDT_Byte,
                srcSRS=f"EPSG:{base_ref_projection}",
                dstSRS=f"EPSG:{self.target_proj}",
            )
        app_dep = []

        same_res = get_raster_resolution(base_ref) == (base_ref_res_x, base_ref_res_y)
        if int(base_ref_projection) != (self.target_proj) or not same_res:
            user_feat_stack.Execute()
            app_dep.append(user_feat_stack)
            user_feat_stack, _ = create_application(
                AvailableOTBApp.SUPERIMPOSE,
                {
                    "inr": self.ref_image,
                    "inm": user_feat_stack,
                    "out": features_out,
                    "ram": str(ram),
                },
            )
        features_labels = [
            I2Label(sensor_name=pattern, feat_name=band_num)
            for pattern, nb_bands in zip(self.data_type, user_features_bands)
            for band_num in range(nb_bands)
        ]
        return (user_feat_stack, app_dep), features_labels
