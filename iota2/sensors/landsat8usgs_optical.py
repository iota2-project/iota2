#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Landsat 8 for USGS class definition
"""
import logging
import multiprocessing as mp
import shutil
from pathlib import Path

from iota2.common.file_utils import (
    file_search_and,
    get_iota2_project_dir,
    get_raster_projection_epsg,
    get_raster_resolution,
    is_dir_empty,
)
from iota2.common.otb_app_bank import (
    AvailableOTBApp,
    compute_user_features,
    create_application,
    execute_app,
    get_input_parameter_output,
)
from iota2.configuration_files import read_config_file as rcf
from iota2.learning.utils import I2TemporalLabel
from iota2.sensors.landsat8usgs_common import Landsat8UsgsCommon
from iota2.typings.i2_types import I2FeaturesPipeline, OtbAppWithDep

LOGGER = logging.getLogger("distributed.worker")

# in order to avoid issue 'No handlers could be found for logger...'
LOGGER.addHandler(logging.NullHandler())


class Landsat8UsgsOptical(Landsat8UsgsCommon):
    """
    Landsat 8 for USGS class definition
    """

    name = "Landsat8USGSOptical"
    nodata_value = 0

    def __init__(
        self,
        tile_name: str,
        target_proj: int,
        all_tiles: str,
        image_directory: str,
        write_dates_stack: bool,
        extract_bands_flag: bool,
        output_target_dir: str,
        keep_bands: list,
        i2_output_path: str,
        enable_sensor_gapfilling: bool,
        temporal_res: int,
        auto_date_flag: bool,
        date_interp_min_user: str,
        date_interp_max_user: str,
        write_outputs_flag: bool,
        features: list[str],
        enable_gapfilling: bool,
        hand_features_flag: bool,
        hand_features: str,
        copy_input: bool,
        rel_refl: bool,
        keep_dupl: bool,
        acor_feat: bool,
        **kwargs: dict,
    ):
        """
        Construct the l8 usgs class for optical products
        """
        super().__init__(
            tile_name,
            target_proj,
            all_tiles,
            image_directory,
            write_dates_stack,
            extract_bands_flag,
            output_target_dir,
            keep_bands,
            i2_output_path,
            temporal_res,
            auto_date_flag,
            date_interp_min_user,
            date_interp_max_user,
            write_outputs_flag,
            features,
            enable_gapfilling,
            hand_features_flag,
            hand_features,
            copy_input,
            rel_refl,
            keep_dupl,
            acor_feat,
            **kwargs,
        )
        self.enable_gapfilling = enable_gapfilling and enable_sensor_gapfilling
        self.mask_no_data = False  # option to mask no data values for all bands

        cfg_sensors_file = str(
            Path(get_iota2_project_dir()) / "iota2" / "sensors" / "sensors.cfg"
        )
        cfg_sensors = rcf.ReadInternalConfigFile(cfg_sensors_file)

        self.struct_path_masks = cfg_sensors.get_param(
            "Landsat8_usgs_optical", "arbomask"
        )

        # sensors attributes
        self.suffix = "SR_STACK"
        self.masks_date_suffix = "OPTICAL_BINARY_MASK"

        self.stack_band_position = ["B1", "B2", "B3", "B4", "B5", "B6", "B7"]
        self.features_names_list = ["NDVI", "NDWI", "Brightness"]
        self.extracted_bands = None
        if extract_bands_flag:
            self.extracted_bands = [
                (band_name, band_position + 1)
                for band_position, band_name in enumerate(self.stack_band_position)
                if band_name in keep_bands
            ]

    @staticmethod
    def get_conversion_expression(**kwargs: str) -> str:
        """
        Returns the OTB bandmath expression to convert a raster from digital number to reflectance
        Not in use yet
        """
        mult_factor = 0.0000275
        add_factor = -0.2
        return f"im1b1 * {mult_factor} + {add_factor}"

    def get_bands_names(self, date_dir: str) -> list[str]:
        """
        Get bands names from sensor's input images names

        Parameters
        ----------
        date_dir:
            directory containing sensor's input images for a given date

        Notes
        -----
            Returns a list of all file names corresponding to all available bands
        """
        date_bands = [
            file_search_and(date_dir, True, "L2", f"SR_{bands_name}{self.extension}")[0]
            for bands_name in self.stack_band_position
        ]
        return date_bands

    def build_stack_date_name(self, date_dir: str) -> str:
        """build stack date name"""
        b1_band = file_search_and(date_dir, True, f"B1{self.extension}")
        stack_bands = file_search_and(date_dir, True, f"{self.suffix}{self.extension}")

        if b1_band:
            b1_name = Path(b1_band[0]).name
            return b1_name.replace(
                f"SR_B1{self.extension}", f"{self.suffix}{self.extension}"
            )
        if stack_bands:
            return Path(stack_bands[0]).name
        raise ValueError("cannot build stack name")

    def preprocess_date_masks(
        self,
        date_dir: str,
        out_prepro: str,
        working_dir: str | None = None,
        ram: int = 128,
        logger: logging.Logger = LOGGER,
    ) -> OtbAppWithDep | tuple[str, None]:
        """
        preprocess date masks
        """
        # manage directories
        mask_dir = date_dir
        if is_dir_empty(mask_dir):
            logger.error(f"Mask directory {mask_dir} is empty !")
            raise FileNotFoundError(f"Mask directory {mask_dir} is empty !")
        logger.debug(f"preprocessing {mask_dir} masks")
        mask_name = self.build_mask_date_name(date_dir)
        out_mask = str(Path(mask_dir) / mask_name)
        if out_prepro:
            out_mask_dir = mask_dir.replace(
                str(Path(self.l8_usgs_data) / self.tile_name), out_prepro
            )
            Path(out_mask_dir).mkdir(parents=True, exist_ok=True)
            out_mask = str(Path(out_mask_dir) / mask_name)

        if Path(out_mask).exists():
            consistent = self.check_out_mask(out_mask)
            if consistent:
                return out_mask, None

        out_mask_processing = out_mask
        if working_dir:
            out_mask_processing = str(Path(working_dir) / mask_name)

        date_mask = []
        for mask_name, _ in list(self.masks_rules.items()):
            full_mask_name = date_dir.split("/")[-1] + "_" + mask_name
            date_mask.append(str(Path(date_dir) / full_mask_name))
        band_math_input = date_mask
        # build binary mask
        expr = "(((im1b1) == 21824) + ((im1b1) == 21952) + ((im1b1) == 30048))*(im2b1 == 0)"

        if self.mask_no_data:
            # 0 -> garde la donnée (1 -> cache la donnée)
            expr = f"((({expr})==0?1:0) + (im4b1==0?1:0))>=1?1:0"
            b10_filename = str(
                Path(date_dir)
                / str(date_dir.split("/")[-1] + f"_ST_B10{self.extension}")
            )
            band_math_input.append(b10_filename)
        else:
            expr = f"({expr})==0?1:0"  # 0 -> garde la donnée (1 -> cache la donnée)
            # band_math_input = date_mask

        binary_mask_rule, _ = create_application(
            AvailableOTBApp.BAND_MATH, {"il": band_math_input, "exp": expr}
        )
        binary_mask_rule.Execute()
        # reproject using reference image
        superimp, _ = create_application(
            AvailableOTBApp.SUPERIMPOSE,
            {
                "inr": self.ref_image,
                "inm": binary_mask_rule,
                "interpolator": "nn",
                "out": out_mask_processing,
                "pixType": "uint8",
                "ram": str(ram),
            },
        )

        # needed to travel through iota2's library
        app_dep = [binary_mask_rule]

        base_ref_res_x, base_ref_res_y = get_raster_resolution(self.ref_image)
        if self.working_resolution:
            base_ref_res_x = self.working_resolution[0]
            base_ref_res_y = self.working_resolution[1]

        if self.write_dates_stack:
            same_proj = False
            same_res = True
            if Path(out_mask).exists():
                same_proj = int(get_raster_projection_epsg(out_mask)) == int(
                    self.target_proj
                )
                same_res = tuple(map(abs, get_raster_resolution(out_mask))) == (
                    base_ref_res_x,
                    abs(base_ref_res_y),
                )
            if not Path(out_mask).exists() or same_proj is False or not same_res:
                # superimp.ExecuteAndWriteOutput()
                multi_proc = mp.Process(target=execute_app, args=[superimp])
                multi_proc.start()
                multi_proc.join()
                if working_dir:
                    shutil.copy(out_mask_processing, out_mask)
                    Path(out_mask_processing).unlink()
        return superimp, app_dep

    def get_time_series_gapfilling(self, ram: int = 128) -> I2FeaturesPipeline:
        """
        get_time_series_gapfilling
        """
        gap_dir = str(Path(self.features_dir) / "tmp")
        Path(gap_dir).mkdir(parents=True, exist_ok=True)
        gap_out = str(Path(gap_dir) / self.time_series_gapfilling_name)

        dates_interp_file, dates_interp = self.write_interpolation_dates_file()
        dates_in_file, _ = self.write_dates_file()

        masks, masks_dep, _ = self.get_time_series_masks()
        (time_series, time_series_dep), _ = self.get_time_series()

        if self.write_outputs_flag is False:
            time_series.Execute()
            masks.Execute()
        else:
            time_series_raster = time_series.GetParameterValue(
                get_input_parameter_output(time_series)
            )
            masks_raster = masks.GetParameterValue(get_input_parameter_output(masks))
            if not Path(masks_raster).exists():
                multi_proc = mp.Process(target=execute_app, args=[masks])
                multi_proc.start()
                multi_proc.join()
            if not Path(time_series_raster).exists():
                # time_series.ExecuteAndWriteOutput()
                multi_proc = mp.Process(target=execute_app, args=[time_series])
                multi_proc.start()
                multi_proc.join()
            if Path(masks_raster).exists():
                masks = masks_raster
            if Path(time_series_raster).exists():
                time_series = time_series_raster

        comp = (
            len(self.stack_band_position)
            if not self.extracted_bands
            else len(self.extracted_bands)
        )

        gap, _ = create_application(
            AvailableOTBApp.IMAGE_TIME_SERIES_GAPFILLING,
            {
                "in": time_series,
                "mask": masks,
                "comp": str(comp),
                "it": "linear",
                "id": dates_in_file,
                "od": dates_interp_file,
                "out": gap_out,
                "ram": str(ram),
                "pixType": "float",
            },
        )
        app_dep = [time_series, masks, masks_dep, time_series_dep]

        bands = self.stack_band_position
        if self.extracted_bands:
            bands = [band_name for band_name, band_pos in self.extracted_bands]

        features_labels = [
            I2TemporalLabel(
                sensor_name=self.__class__.name, feat_name=band_name, date=date
            )
            for date in dates_interp
            for band_name in bands
        ]
        return (gap, app_dep), features_labels

    def get_features_labels(
        self, dates: list[str], rel_refl: bool, keep_dupl: bool, copy_in: bool
    ) -> list[I2TemporalLabel]:  # Différent par sous classe
        """
        get feature labels
        """
        if rel_refl and keep_dupl is False and copy_in is True:
            self.features_names_list = ["NDWI", "Brightness"]
        out_labels = []

        for feature in self.features_names_list:
            for date in dates:
                out_labels.append(
                    I2TemporalLabel(
                        sensor_name=self.__class__.name, feat_name=feature, date=date
                    )
                )
        return out_labels

    # pylint: disable=unused-argument; logger is used in user_features
    def get_features(
        self, ram: int = 128, logger: logging.Logger = LOGGER
    ) -> I2FeaturesPipeline:
        """
        get features
        """
        features_dir = str(Path(self.features_dir) / "tmp")
        Path(features_dir).mkdir(parents=True, exist_ok=True)
        features_out = str(Path(features_dir) / self.features_names)
        (
            in_stack,
            in_stack_dep,
        ), in_stack_features_labels = self.get_time_series_gapfilling()
        _, dates_enabled = self.write_interpolation_dates_file()

        if not self.enable_gapfilling:
            (in_stack, in_stack_dep), in_stack_features_labels = self.get_time_series()
            _, dates_enabled = self.write_dates_file()

        if self.write_outputs_flag is False:
            in_stack.Execute()
        else:
            in_stack_raster = in_stack.GetParameterValue(
                get_input_parameter_output(in_stack)
            )
            if not Path(in_stack_raster).exists():
                # in_stack.ExecuteAndWriteOutput()
                multi_proc = mp.Process(target=execute_app, args=[in_stack])
                multi_proc.start()
                multi_proc.join()
            if Path(in_stack_raster).exists():
                in_stack = in_stack_raster
        # output
        app_dep = []
        if self.hand_features_flag:
            # ~ hand_features = self.cfg_IOTA2.getParam("Sentinel_2",
            # ~ "additionalFeatures")
            comp = (
                len(self.stack_band_position)
                if not self.extracted_bands
                else len(self.extracted_bands)
            )
            (
                user_date_features,
                fields_userfeat,
                user_feat_date,
                stack,
            ) = compute_user_features(
                in_stack, dates_enabled, comp, self.hand_features.split(",")
            )
            user_date_features.Execute()
            app_dep.append([user_date_features, user_feat_date, stack])
        if self.features:
            bands_avail = self.stack_band_position
            if self.extracted_bands:
                bands_avail = [band_name for band_name, _ in self.extracted_bands]
                # check mandatory bands
                if "B4" not in bands_avail:
                    raise IndexError("red band (B4) is needed to compute features")
                if "B5" not in bands_avail:
                    raise IndexError("nir band (B5) is needed to compute features")
                if "B6" not in bands_avail:
                    raise IndexError("swir band 1 (B6) is needed to compute features")

            feat_parameters = {
                "in": in_stack,
                "out": features_out,
                "comp": len(bands_avail),
                "red": bands_avail.index("B4") + 1,
                "nir": bands_avail.index("B5") + 1,
                "swir": bands_avail.index("B6") + 1,
                "copyinput": self.copy_input,
                "relrefl": self.rel_refl,
                "keepduplicates": self.keep_dupl,
                "acorfeat": self.acor_feat,
                "pixType": "float",
                "ram": str(ram),
            }
            features_app, _ = create_application(
                AvailableOTBApp.IOTA2_FEATURES_EXTRACTION, feat_parameters
            )
            if self.copy_input is False:
                in_stack_features_labels = []
            features_labels = in_stack_features_labels + self.get_features_labels(
                dates_enabled, self.rel_refl, self.keep_dupl, self.copy_input
            )
        else:
            features_app = in_stack
            features_labels = in_stack_features_labels

        app_dep.append([in_stack, in_stack_dep])

        if self.hand_features_flag:
            features_app.Execute()
            app_dep.append(features_app)
            features_app, _ = create_application(
                AvailableOTBApp.CONCATENATE_IMAGES,
                {
                    "il": [features_app, user_date_features],
                    "out": features_out,
                    "ram": str(ram),
                },
            )
            features_labels += fields_userfeat
        return (features_app, app_dep), features_labels
