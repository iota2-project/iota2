#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
This class manage sensor's data by tile, providing services needed in whole
IOTA² library
"""
import logging
from pathlib import Path
from typing import TypeVar

import iota2.sensors.landsat5old as l5old
from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.common.service_error import ConfigError
from iota2.sensors.landsat8 import Landsat8
from iota2.sensors.landsat8old import Landsat8Old
from iota2.sensors.landsat8usgs import Landsat8Usgs
from iota2.sensors.landsat8usgs_infrared import Landsat8UsgsInfrared
from iota2.sensors.landsat8usgs_optical import Landsat8UsgsOptical
from iota2.sensors.landsat8usgs_thermal import Landsat8UsgsThermal
from iota2.sensors.sentinel1 import Sentinel1
from iota2.sensors.sentinel2 import Sentinel2
from iota2.sensors.sentinel2_l3a import Sentinel2L3a
from iota2.sensors.sentinel2_s2c import Sentinel2S2c
from iota2.sensors.userfeatures import UserFeatures
from iota2.typings.i2_types import (
    I2FeaturesPipeline,
    I2SARFeaturesPipeline,
    OtbApp,
    OtbAppWithDep,
)

LOGGER = logging.getLogger("distributed.worker")

SensorName = str

SensorType = (
    Landsat8
    | Landsat8Old
    | Landsat8Usgs
    | Landsat8UsgsInfrared
    | Landsat8UsgsOptical
    | Landsat8UsgsThermal
    | Sentinel2
    | Sentinel2L3a
    | Sentinel2S2c
    | Sentinel1
    | UserFeatures
)

GenericSensor = (
    Landsat8
    | Landsat8Old
    | Landsat8Usgs
    | Landsat8UsgsInfrared
    | Landsat8UsgsOptical
    | Landsat8UsgsThermal
    | Sentinel2
    | Sentinel2L3a
    | Sentinel2S2c
)

OpticalSensorType = (
    Landsat8
    | Landsat8Old
    | Landsat8Usgs
    | Landsat8UsgsInfrared
    | Landsat8UsgsOptical
    | Landsat8UsgsThermal
    | Sentinel2
    | Sentinel2L3a
    | Sentinel2S2c
)

Sensor = TypeVar(
    "Sensor",
    bound=(
        # l5old.Landsat5Old, TODO: solve circular import
        Landsat8
        | Landsat8Old
        | Landsat8Usgs
        | Landsat8UsgsInfrared
        | Landsat8UsgsOptical
        | Landsat8UsgsThermal
        | Sentinel2
        | Sentinel2L3a
        | Sentinel2S2c
        | Sentinel1
        | UserFeatures
    ),
)
Sensors = list[Sensor]


class SensorsContainer:
    """
    The sensors container class
    """

    def __init__(
        self,
        tile_name: str | None,
        working_dir: str | None,
        output_path: str | None,
        **kwargs: str | list[str] | int,
    ) -> None:
        """
        Build the sensors container class

        Parameters
        ----------
        tile_name :
            tile to consider : "T31TCJ"
        working_dir :
            absolute path to a working directory dedicated to compute temporary
            data
        output_path :
            the final output path
        sensors_description :
            :class:`iota2.common.ServiceConfigFile.iota2_parameters`

        """
        self.tile_name = tile_name
        self.working_dir = working_dir
        self.sensors_description = kwargs
        self.enabled_sensors: Sensors = self.get_enabled_sensors()
        self.common_mask_name = "MaskCommunSL.tif"
        self.common_mask_dir = None
        if output_path and tile_name:
            features_dir = str(Path(output_path) / "features" / tile_name)
            self.common_mask_dir = str(Path(features_dir) / "tmp")

        self.logger = kwargs.get("logger", LOGGER)

    def __str__(self) -> str:
        """return enabled sensors and the current tile"""
        return (
            f"tile's name : {self.tile_name}, available sensors : "
            f"{', '.join(self.print_enabled_sensors_name())}"
        )

    def __repr__(self) -> str:
        """return enabled sensors and the current tile"""
        return self.__str__()

    def get_iota2_sensors_names(self) -> list[str]:
        """
        Define the list of supported sensors in iota2

        """
        available_sensors_name = [
            l5old.Landsat5Old.name,
            Landsat8.name,
            Landsat8Usgs.name,
            Landsat8UsgsOptical.name,
            Landsat8UsgsThermal.name,
            Landsat8UsgsInfrared.name,
            Landsat8Old.name,
            Sentinel1.name,
            Sentinel2.name,
            Sentinel2S2c.name,
            Sentinel2L3a.name,
            UserFeatures.name,
        ]
        return available_sensors_name

    def print_enabled_sensors_name(self) -> list[SensorName]:
        """
        Create a list of name of enabled sensors
        """
        l5_old = self.sensors_description.get("Landsat5_old", None)
        land8 = self.sensors_description.get("Landsat8", None)
        l8_usgs = self.sensors_description.get("Landsat8_usgs", None)
        l8_usgs_optical = self.sensors_description.get("Landsat8_usgs_optical", None)
        l8_usgs_thermal = self.sensors_description.get("Landsat8_usgs_thermal", None)
        l8_usgs_infrared = self.sensors_description.get("Landsat8_usgs_infrared", None)
        l8_old = self.sensors_description.get("Landsat8_old", None)
        sen1 = self.sensors_description.get("Sentinel_1", None)
        sen2 = self.sensors_description.get("Sentinel_2", None)
        s2_s2c = self.sensors_description.get("Sentinel_2_S2C", None)
        s2_l3a = self.sensors_description.get("Sentinel_2_L3A", None)
        user_feat = self.sensors_description.get("userFeat", None)

        enabled_sensors = []
        if l5_old is not None:
            enabled_sensors.append(l5old.Landsat5Old.name)
        if land8 is not None:
            enabled_sensors.append(Landsat8.name)
        if l8_usgs is not None:
            enabled_sensors.append(Landsat8Usgs.name)
        if l8_usgs_optical is not None:
            enabled_sensors.append(Landsat8UsgsOptical.name)
        if l8_usgs_thermal is not None:
            enabled_sensors.append(Landsat8UsgsThermal.name)
        if l8_usgs_infrared is not None:
            enabled_sensors.append(Landsat8UsgsInfrared.name)
        if l8_old is not None:
            enabled_sensors.append(Landsat8Old.name)
        if sen1 is not None:
            enabled_sensors.append(Sentinel1.name)
        if sen2 is not None:
            enabled_sensors.append(Sentinel2.name)
        if s2_s2c is not None:
            enabled_sensors.append(Sentinel2S2c.name)
        if s2_l3a is not None:
            enabled_sensors.append(Sentinel2L3a.name)
        if user_feat is not None:
            enabled_sensors.append(UserFeatures.name)
        return enabled_sensors

    def get_sensor(self, sensor_name: str) -> Sensor | None:
        """

        Return a sensor class object found by it's name

        Return None if not found

        Parameters
        ----------
        sensor_name :
            sensor's name

        """
        sensor_found = []
        for sensor in self.enabled_sensors:
            if sensor_name == sensor.__class__.name:
                sensor_found.append(sensor)
        if len(sensor_found) > 1:
            raise Exception(f"Too many sensors found with the name {sensor_name}")
        return sensor_found[0] if sensor_found else None

    def remove_sensor(self, sensor_name: str) -> None:
        """
        Remove a sensor in the available sensor list

        Parameters
        ----------
        sensor_name :
            the sensor's name to remove

        """
        for index, sensor in enumerate(self.enabled_sensors):
            if sensor_name == sensor.__class__.name:
                self.enabled_sensors.pop(index)

    def get_enabled_sensors(self) -> list[SensorType]:
        """
        Build enabled sensor list

        This function define sensors to use in IOTA2 run. It is where sensor's
        order is defined :

        1. Landsat5_old
        2. Landsat8
        3. Landsat8_old
        4. Sentinel_1
        5. Sentinel_2
        6. Sentinel_2_S2C
        7. Sentinel_2_L3A
        8. userFeat
        """

        enabled_sensors: Sensors = []
        if (l5_old := self.sensors_description.get("Landsat5_old", None)) is not None:
            enabled_sensors.append(l5old.Landsat5Old(**l5_old))
        if (land8 := self.sensors_description.get("Landsat8", None)) is not None:
            enabled_sensors.append(Landsat8(**land8))
        if (l8_usgs := self.sensors_description.get("Landsat8_usgs", None)) is not None:
            enabled_sensors.append(Landsat8Usgs(**l8_usgs))
        if (
            l8_usgs_optical := self.sensors_description.get(
                "Landsat8_usgs_optical", None
            )
        ) is not None:
            enabled_sensors.append(Landsat8UsgsOptical(**l8_usgs_optical))
        if (
            l8_usgs_thermal := self.sensors_description.get(
                "Landsat8_usgs_thermal", None
            )
        ) is not None:
            enabled_sensors.append(Landsat8UsgsThermal(**l8_usgs_thermal))
        if (
            l8_usgs_infrared := self.sensors_description.get(
                "Landsat8_usgs_infrared", None
            )
        ) is not None:
            enabled_sensors.append(Landsat8UsgsInfrared(**l8_usgs_infrared))
        if (l8_old := self.sensors_description.get("Landsat8_old", None)) is not None:
            enabled_sensors.append(Landsat8Old(**l8_old))
        if (sen1 := self.sensors_description.get("Sentinel_1", None)) is not None:
            enabled_sensors.append(Sentinel1(**sen1))
        if (sen2 := self.sensors_description.get("Sentinel_2", None)) is not None:
            enabled_sensors.append(Sentinel2(**sen2))
        if (s2_s2c := self.sensors_description.get("Sentinel_2_S2C", None)) is not None:
            enabled_sensors.append(Sentinel2S2c(**s2_s2c))
        if (s2_l3a := self.sensors_description.get("Sentinel_2_L3A", None)) is not None:
            enabled_sensors.append(Sentinel2L3a(**s2_l3a))
        if (user_feat := self.sensors_description.get("userFeat", None)) is not None:
            enabled_sensors.append(UserFeatures(**user_feat))
        if len(enabled_sensors) == 0:
            raise ConfigError("No active sensors. Check your configuration file.")
        return enabled_sensors

    def get_enabled_sensors_path(self) -> list[str]:
        """
        Return the enabled sensors path to images
        """
        # self.enabled_sensors

        l5_old = self.sensors_description.get("Landsat5_old", None)
        land8 = self.sensors_description.get("Landsat8", None)
        l8_usgs = self.sensors_description.get("Landsat8_usgs", None)
        l8_usgs_optical = self.sensors_description.get("Landsat8_usgs_optical", None)
        l8_usgs_thermal = self.sensors_description.get("Landsat8_usgs_thermal", None)
        l8_usgs_infrared = self.sensors_description.get("Landsat8_usgs_infrared", None)
        l8_old = self.sensors_description.get("Landsat8_old", None)
        sen1 = self.sensors_description.get("Sentinel_1", None)
        sen2 = self.sensors_description.get("Sentinel_2", None)
        s2_s2c = self.sensors_description.get("Sentinel_2_S2C", None)
        s2_l3a = self.sensors_description.get("Sentinel_2_L3A", None)
        user_feat = self.sensors_description.get("userFeat", None)

        paths = []
        for sensor in self.enabled_sensors:
            if l5_old and l5old.Landsat5Old.name == sensor.__class__.name:
                paths.append(l5_old["image_directory"])
            elif land8 and Landsat8.name == sensor.__class__.name:
                paths.append(land8["image_directory"])
            elif l8_usgs and Landsat8Usgs.name == sensor.__class__.name:
                paths.append(l8_usgs["image_directory"])
            elif l8_usgs_optical and Landsat8UsgsOptical.name == sensor.__class__.name:
                paths.append(l8_usgs_optical["image_directory"])
            elif l8_usgs_thermal and Landsat8UsgsThermal.name == sensor.__class__.name:
                paths.append(l8_usgs_thermal["image_directory"])
            elif (
                l8_usgs_infrared and Landsat8UsgsInfrared.name == sensor.__class__.name
            ):
                paths.append(l8_usgs_infrared["image_directory"])
            elif l8_old and Landsat8Old.name == sensor.__class__.name:
                paths.append(l8_old["image_directory"])
            elif sen1 and Sentinel1.name == sensor.__class__.name:
                paths.append(sen1["image_directory"])
            elif sen2 and Sentinel2.name == sensor.__class__.name:
                paths.append(sen2["image_directory"])
            elif s2_s2c and Sentinel2S2c.name == sensor.__class__.name:
                paths.append(s2_s2c["image_directory"])
            elif s2_l3a and Sentinel2L3a.name == sensor.__class__.name:
                paths.append(s2_l3a["image_directory"])
            elif user_feat and UserFeatures.name == sensor.__class__.name:
                paths.append(user_feat["image_directory"])
        return paths

    def sensors_preprocess(
        self, available_ram: int = 128, logger: logging.Logger = LOGGER
    ) -> None:
        """preprocessing every enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, useful to many OTB's applications.

        """
        for sensor in self.enabled_sensors:
            self.sensor_preprocess(sensor, self.working_dir, available_ram, logger)

    def sensor_preprocess(
        self,
        sensor: Sensor,
        working_dir: str | None,
        available_ram: int,
        logger: logging.Logger = LOGGER,
    ) -> None:
        """
        Sensor preprocessing

        Parameters
        ----------
        sensor :
            A sensor object, see :meth:`get_sensor`
        working_dir :
            absolute path to a working directory dedicated
            to compute temporary data
        available_ram :
            RAM, useful to many OTB's applications.

        Notes
        -----
        object
            return the object returned from sensor.preprocess. Nowadays,
            nothing is done with it, no object's type are imposed
            but it could change.
        """
        if hasattr(sensor, "preprocess"):
            sensor.preprocess(working_dir=working_dir, ram=available_ram, logger=logger)

    def sensors_dates(self) -> list[tuple[SensorName, list[str]]]:
        """
        Get available dates by sensors

        if exists get_available_dates's sensor method is called. The method
        must return a list of sorted available dates.

        Notes
        -------
        The list of tuple returned contains:
            (sensor's name, [list of available dates])

        """
        sensors_dates = []
        for sensor in self.enabled_sensors:
            dates_list = []
            # if "get_available_dates" in dir(sensor):
            if hasattr(sensor, "get_dates_directories"):
                dates_list = sensor.get_dates_directories()
            sensors_dates.append((sensor.__class__.name, dates_list))
        return sensors_dates

    def get_sensors_footprint(
        self, available_ram: int = 128
    ) -> list[tuple[SensorName, OtbAppWithDep]]:
        """
        Get sensor's footprint

        It provide the list of otb's application dedicated to generate sensor's
        footprint. Each otb's application must be dedicated to the generation
        of a binary raster, 0 meaning 'NODATA'

        Parameters
        ----------
        available_ram :
            RAM, useful to many OTB's applications.

        Notes
        -----
        The returned list is a list of tuple containing
        (sensor's name, otbApplication)

        """
        sensors_footprint = []
        for sensor in self.enabled_sensors:
            sensors_footprint.append(
                (sensor.__class__.name, sensor.footprint(available_ram))
            )
        return sensors_footprint

    def get_common_sensors_footprint(self, available_ram: int = 128) -> OtbAppWithDep:
        """
        Get common sensor's footprint

        provide an otbApplication ready to be 'ExecuteAndWriteOutput'
        generating a binary raster which is the intersection of all sensor's
        footprint

        Parameters
        ----------
        available_ram :
            RAM, useful to many OTB's applications.

        Notes
        -----
        The output tuple is given by (otbApplication, dependencies)
        where otbApplication is the application and dependencies is a list of
        otbApplication for avoid pipeline issues

        """
        sensors_footprint = []
        all_dep = []
        for sensor in self.enabled_sensors:
            footprint, _ = sensor.footprint(available_ram)
            footprint.Execute()
            sensors_footprint.append(footprint)
            all_dep.append(_)
            all_dep.append(footprint)

        expr = "+".join(f"im{i + 1}b1" for i in range(len(sensors_footprint)))
        expr = f"{expr}=={len(sensors_footprint)}?1:0"
        assert self.common_mask_dir
        common_mask_out = str(Path(self.common_mask_dir) / self.common_mask_name)
        common_mask, _ = create_application(
            AvailableOTBApp.BAND_MATH,
            {
                "il": sensors_footprint,
                "exp": expr,
                "out": common_mask_out,
                "pixType": "uint8",
                "ram": str(available_ram),
            },
        )
        return common_mask, all_dep

    def get_sensors_time_series(
        self, available_ram: int = 128
    ) -> list[tuple[str, I2FeaturesPipeline | I2SARFeaturesPipeline]]:
        """
        Get time series to each enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, useful to many OTB's applications.

        Notes
        -----
        The returned list is a list of tuple :
            (sensor's name, (otbApplication, dependencies), labels)
        where:

        * Sensors name is the string returned by
          :attr:`iota2.sensors.Sentinel_2.name` for instance
        * (otbApplication, dependencies) is a tuple containing the
          aplication to be executed and dependencies is a list of all
          application needed to avoid pipeline issue
        * labels are sorted feature's name

        """
        sensors_time_series = []
        for sensor in self.enabled_sensors:
            if "get_time_series" in dir(sensor):
                sensors_time_series.append(
                    (str(sensor.__class__.name), sensor.get_time_series(available_ram))
                )
        return sensors_time_series

    def get_sensors_time_series_raw(
        self, available_ram: int = 128
    ) -> list[tuple[SensorName, I2FeaturesPipeline]]:
        """
        Get time series to each enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, useful to many OTB's applications.

        Notes
        -----
        The returned list is a list of tuple :
            (sensor's name, (otbApplication, dependencies), labels)
        where:

        * Sensors name is the string returned by
          :attr:`iota2.sensors.Sentinel_2.name` for instance
        * (otbApplication, dependencies) is a tuple containing the
          aplication to be executed and dependencies is a list of all
          application needed to avoid pipeline issue
        * labels are sorted feature's name

        """
        sensors_time_series = []
        for sensor in self.enabled_sensors:
            if "get_time_series_raw" in dir(sensor):
                sensors_time_series.append(
                    (sensor.__class__.name, sensor.get_time_series_raw(available_ram))
                )
        return sensors_time_series

    def get_sensors_time_series_masks(
        self, available_ram: int = 128
    ) -> list[tuple[str, tuple[OtbApp, list[OtbApp], int]]]:
        """get time series masks to each enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, useful to many OTB's applications.

        Notes
        -----
        The returned list is a list of tuple :
            (sensor's name, (otbApplication, dependencies), number of masks)
        where:

        * Sensors name is the string returned by
          :attr:`iota2.sensors.Sentinel_2.name` for instance
        * (otbApplication, dependencies) is a tuple containing the
          aplication to be executed and dependencies is a list of all
          application needed to avoid pipeline issue
        * number of mask is an integer

        """

        sensors_time_series_masks = []
        for sensor in self.enabled_sensors:
            if "get_time_series_masks" in dir(sensor):
                sensors_time_series_masks.append(
                    (sensor.__class__.name, sensor.get_time_series_masks(available_ram))
                )
        return sensors_time_series_masks

    def get_sensors_time_series_masks_raw(
        self, available_ram: int = 128
    ) -> list[tuple[str, tuple[OtbApp, list[OtbApp], int]]]:
        """get time series masks for each enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, useful to many OTB's applications.

        Notes
        -----
        The returned list is a list of tuple :
            (sensor's name, (otbApplication, dependencies), number of masks)
        where:

        * Sensors name is the string returned by
          :attr:`iota2.sensors.Sentinel_2.name` for instance
        * (otbApplication, dependencies) is a tuple containing the
          aplication to be executed and dependencies is a list of all
          application needed to avoid pipeline issue
        * number of mask is an integer

        """

        sensors_time_series_masks = []
        for sensor in self.enabled_sensors:
            if "get_time_series_masks_raw" in dir(sensor):
                sensors_time_series_masks.append(
                    (
                        sensor.__class__.name,
                        sensor.get_time_series_masks_raw(available_ram),
                    )
                )
        return sensors_time_series_masks

    def get_sensors_time_series_gapfilling(
        self, available_ram: int = 128
    ) -> list[tuple[str, I2FeaturesPipeline]]:
        """
        Get time series gapfilled to each enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, useful to many OTB's applications.

        Notes
        -----
        The returned list is a list of tuple :
            (sensor's name, (otbApplication, dependencies), labels)
        where:

        * Sensors name is the string returned by
          :attr:`iota2.sensors.Sentinel_2.name` for instance
        * (otbApplication, dependencies) is a tuple containing the
          aplication to be executed and dependencies is a list of all
          application needed to avoid pipeline issue
        * labels are sorted feature's name

        """
        sensors_time_series = []
        for sensor in self.enabled_sensors:
            if "get_time_series_gapfilling" in dir(sensor):
                sensors_time_series.append(
                    (
                        sensor.__class__.name,
                        sensor.get_time_series_gapfilling(available_ram),
                    )
                )
        return sensors_time_series

    def get_sensors_features(
        self, available_ram: int = 128, logger: logging.Logger = LOGGER
    ) -> list[tuple[str, I2FeaturesPipeline]]:
        """
        Get features to each enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, useful to many OTB's applications.

        Notes
        -----
        The returned list of tuple is composed by
        (sensor's name, ((otbApplication, dependencies), labels))
        where:

        * Sensors name is the string returned by
          :attr:`iota2.sensors.Sentinel_2.name` for instance
        * (otbApplication, dependencies) is a tuple containing the
          application to be executed and dependencies is a list of all
          application needed to avoid pipeline issue
        * labels are sorted feature's name

        """
        sensors_features = []
        for sensor in self.enabled_sensors:
            sensors_features.append(
                (
                    sensor.__class__.name,
                    sensor.get_features(available_ram, logger=logger),
                )
            )
        return sensors_features
