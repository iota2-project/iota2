#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Module containing function to merge classification using voting methods"""
import logging
import shutil
from pathlib import Path

import pandas as pd

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fut
from iota2.common import otb_app_bank as otbApp
from iota2.typings.i2_types import OtbPixType, RegressionMetricsParameters
from iota2.validation.regression_metrics import generate_multi_tile_metrics

LOGGER = logging.getLogger("distributed.worker")

I2_CONST = i2_const.Iota2Constants()


def fusion_regression(
    input_paths: list[str],
    fusion_path: str,
    confidence_path: str,
    method: str,
    pix_type: OtbPixType,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Generates a raster that is the mean or median between several rasters
    and another that is the standard deviation
    or median absolute deviation between these same rasters.
    Merges the rasters of the input_paths list into the fusion_path and confidence_path.

    Parameters
    ----------
    input_paths : [string]
        list of paths to the rasters to merge
    fusion_path : string
        path to save the fusion raster
    confidence_path : int
        path to save the confidence raster
    method : string
        fusion's method (median/mean)
    pix_type : string
        Otb parameter 'pixType'
    """
    concat_options = {
        "il": input_paths,
    }
    concat_app, _ = otbApp.create_application(
        otbApp.AvailableOTBApp.CONCATENATE_IMAGES, concat_options
    )
    concat_app.Execute()

    exp = method + "(im1)"

    if method == "median":
        multi_med_exp = "1"
        for _ in range(len(input_paths) - 1):
            multi_med_exp = multi_med_exp + ",1"

    confidence_exp = (
        "var(im1) pw 0.5"
        if method == "mean"
        else "median(vabs(im1-bands(median(im1), { " + multi_med_exp + "} ))) "
    )

    fusion_options = {
        "il": concat_app,
        "out": fusion_path,
        "exp": exp,
        "pixType": pix_type,
    }

    logger.debug("fusion options:")
    logger.debug(fusion_options)
    fusion_app, _ = otbApp.create_application(
        otbApp.AvailableOTBApp.BAND_MATH_X, fusion_options
    )
    logger.debug("START fusion of final classifications")
    fusion_app.ExecuteAndWriteOutput()
    logger.debug("END fusion of final classifications")

    fusion_confidence_options = {
        "il": concat_app,
        "out": confidence_path,
        "exp": confidence_exp,
        "pixType": pix_type,
    }

    logger.debug("fusion options:")
    logger.debug(fusion_confidence_options)
    fusion_app_confidence, _ = otbApp.create_application(
        otbApp.AvailableOTBApp.BAND_MATH_X, fusion_confidence_options
    )
    logger.debug("START fusion of final classifications")
    fusion_app_confidence.ExecuteAndWriteOutput()
    logger.debug("END fusion of final classifications")


def merge_final_regressions(
    iota2_dir: str,
    tile: str,
    runs: int,
    method: str = "mean",
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Use to merge regressions by median
    or mean method.
    Get all regressions tile_seed*.tif in the /final/TMP directory,
     fusion them under the raster call tile_fusion.tif and made a confidence map
     under the raster call tile_confidence.tif.

    Parameters
    ----------
    iota2_dir : string
        path to the iota2's output path
    tile : string
        tile's name
    runs : int
        number of iota2 runs (random learning splits)
    method : string
        fusion's method (median/mean)
    """

    fusion_name = str(tile) + "_fusion.tif"
    confidence_name = str(tile) + "_confidence.tif"

    if method not in ["mean", "median"]:
        err_msg = (
            "the fusion method must be " " 'mean' or 'median'" f" and {method} readed"
        )
        logger.error(err_msg)
        raise Exception(err_msg)

    iota2_dir_final_tmp = str(Path(iota2_dir) / "final/TMP")
    work_dir = iota2_dir_final_tmp
    if working_directory:
        work_dir = working_directory
    final_regressions = [
        fut.file_search_and(iota2_dir_final_tmp, True, tile + f"_seed_{run}.tif")[0]
        for run in range(runs)
    ]
    fusion_path = str(Path(work_dir) / fusion_name)

    confidence_path = str(Path(work_dir) / confidence_name)
    fusion_regression(
        final_regressions,
        fusion_path,
        confidence_path,
        method,
        "float",
        logger=LOGGER,
    )

    if working_directory:
        shutil.copy(fusion_path, iota2_dir_final_tmp)
        shutil.copy(confidence_path, iota2_dir_final_tmp)
        Path(fusion_path).unlink()
        Path(confidence_path).unlink()


def merge_final_validation(
    iota2_dir: str, data_field: str, runs: int = 1, keep_runs_results: bool = True
) -> None:
    """
    Use to compute validation over all the runs.

    Parameters
    ----------
    iota2_dir : string
        path to the iota2's output path
    dataField : string
        data's field name
    runs : int
        number of iota2 runs (random learning splits)
    keep_runs_results : bool
        flag to inform if seeds results will be included in the report
    validation_shape : string
        path to a shape dedicated to validate fusion of classifications
    working_directory : string
        path to a working directory

    """

    iota2_dir_final = str(Path(iota2_dir) / "final")

    vector_val = fut.file_search_and(
        str(Path(iota2_dir_final) / "merge_final_classifications"),
        True,
        "majvote.sqlite",
    )

    fusion_image = str(Path(iota2_dir_final) / "Regressions_fusion.tif")
    # TODO validation shape
    # if validation_shape:
    #     validation_vector = validation_shape

    merge_report = str(Path(iota2_dir_final) / "fusion_metrics.csv")

    if keep_runs_results:
        index_col = ["Seed_0"]
        seed_image = str(Path(iota2_dir_final) / "Regression_Seed_0.tif")
        generate_multi_tile_metrics(
            seed_image,
            vector_val,
            I2_CONST.fake_class,
            merge_report,
            RegressionMetricsParameters(data_field=data_field),
        )
        for cpt in range(1, runs):
            seed_image = str(Path(iota2_dir_final) / f"Regression_Seed_{str(cpt)}.tif")
            generate_multi_tile_metrics(
                seed_image,
                vector_val,
                I2_CONST.fake_class,
                merge_report,
                RegressionMetricsParameters(
                    data_field=data_field,
                    mode="a",
                    column=False,
                ),
            )
            index_col.append("Seed_" + str(cpt))

        generate_multi_tile_metrics(
            fusion_image,
            vector_val,
            I2_CONST.fake_class,
            merge_report,
            RegressionMetricsParameters(
                data_field=data_field,
                mode="a",
                column=False,
            ),
        )
        df = pd.read_csv(merge_report)
        index_col.append("Fusion")
        index = pd.Index(index_col)
        df.set_index(index, inplace=True)
        df.to_csv(merge_report)

    else:
        generate_multi_tile_metrics(
            fusion_image,
            vector_val,
            I2_CONST.fake_class,
            merge_report,
            RegressionMetricsParameters(data_field=data_field),
        )
