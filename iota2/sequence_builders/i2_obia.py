#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
OBIA builder
"""
import logging
import shutil
from functools import partial
from pathlib import Path
from typing import Any

from osgeo import ogr

import iota2.common.i2_constants as i2_const
from iota2.common import service_error as sErr
from iota2.common.iota2_directory import generate_directories_obia
from iota2.common.utils import run
from iota2.configuration_files import check_config_parameters as ccp
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.sections.cfg_utils import ConfigError
from iota2.sequence_builders.i2_sequence_builder import (
    I2Builder,
    check_databases_consistency,
    check_input_text_files,
    check_sensors_data,
)
from iota2.steps import (
    check_inputs_step,
    classification_obia,
    common_masks,
    compute_intersection_seg_regions,
    compute_metrics_obia,
    envelope,
    gen_region_vector,
    intersect_seg_learn,
    learning_obia,
    learning_zonal_statistics,
    merge_tiles_obia,
    obia_final_metrics,
    pixel_validity,
    prepare_obia_seg,
    samples_merge,
    sensors_preprocess,
    slic_segmentation,
    split_samples,
    vector_formatting,
)
from iota2.steps.iota2_step import Step, StepContainer
from iota2.typings.i2_types import AvailSchedulers
from iota2.vector_tools.vector_functions import (
    get_fields,
    get_layer_name,
    get_re_encoding_labels_dic,
)

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()

I2ParameterT = dict[str, Any]


class I2Obia(I2Builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """

    def __init__(
        self,
        cfg: str,
        config_resources: Path | None,
        scheduler_type: AvailSchedulers,
        restart: bool = False,
        tasks_states_file: str | None = None,
        hpc_working_directory: str = "TMPDIR",
    ):
        super().__init__(
            cfg,
            config_resources,
            scheduler_type,
            restart,
            tasks_states_file,
            hpc_working_directory,
        )

        # create dict for control variables
        self.control_var: I2ParameterT = {}
        # steps definitions
        # self.steps_group = {}

        self.steps_group["init"] = {}
        self.steps_group["sampling"] = {}
        self.steps_group["learning"] = {}
        self.steps_group["classification"] = {}
        self.steps_group["mosaic"] = {}
        self.steps_group["validation"] = {}
        self.steps_group["regularisation"] = {}
        self.steps_group["crown"] = {}
        self.steps_group["mosaictiles"] = {}
        self.steps_group["vectorisation"] = {}
        self.steps_group["simplification"] = {}
        self.steps_group["smoothing"] = {}
        self.steps_group["clipvectors"] = {}
        self.steps_group["lcstatistics"] = {}

        # build steps
        self.sort_step()

    def get_dir(self) -> list[str]:
        """
        usage : return iota2_directories
        """
        directories = [
            "classif",
            "config_model",
            "dataRegion",
            "envelope",
            "formattingVectors",
            "metaData",
            "samplesSelection",
            "stats",
            "dataAppVal",
            "final",
            "learningSamples",
            "model",
            "shapeRegion",
            "features",
        ]

        iota2_outputs_dir = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "output_path"
        )

        return [str(Path(iota2_outputs_dir) / d) for d in directories]

    def formatting_reference_data(self) -> None:
        """
        Formats reference data for re-encoding
        """
        ref_data = self.control_var["ground_truth"]
        ref_data_field = self.control_var["data_field"]
        ref_data_i2_formatted = str(
            Path(self.control_var["iota2_outputs_dir"], "reference_data.shp")
        )
        self.ref_data_formatting(ref_data, ref_data_field, ref_data_i2_formatted)

    def ref_data_formatting(
        self, ref_data: str, ref_data_field: str, ref_data_i2_formatted: str
    ) -> None:
        """re-encode reference data

        Parameters
        ----------
        ref_data
            input reference database
        ref_data_field
            column containing labels database
        ref_data_i2_formatted
            output formatted database
        """
        i2_label_column = I2_CONST.re_encoding_label_name
        ref_data_fields = get_fields(ref_data)
        if i2_label_column in ref_data_fields:
            raise ValueError(
                f"the reference data : {ref_data} contains the field"
                f" {i2_label_column}. Please rename the column name"
            )
        lut_labels = get_re_encoding_labels_dic(ref_data, ref_data_field)

        user_fields = get_fields(ref_data)
        layer_name = get_layer_name(ref_data)
        lower_clause = ",".join(
            [f"{user_field} AS {user_field.lower()}" for user_field in user_fields]
        )
        run(
            f"ogr2ogr {ref_data_i2_formatted} {ref_data} "
            f"-sql 'SELECT {lower_clause} FROM {layer_name}'"
        )

        ds = ogr.Open(ref_data_i2_formatted)
        driver = ds.GetDriver()
        data_src = driver.Open(ref_data_i2_formatted, 1)
        layer = data_src.GetLayer()
        layer.CreateField(ogr.FieldDefn(i2_label_column, ogr.OFTInteger))
        assert lut_labels is not None
        for feat in layer:
            feat.SetField(i2_label_column, lut_labels[feat.GetField(ref_data_field)])
            layer.SetFeature(feat)

    def build_steps(self, cfg: str) -> list[StepContainer]:
        """
        build steps
        """
        Step.set_models_spatial_information(self.tiles, {})

        # control variable
        self.init_dict_control_variables()

        # will contains all IOTA² steps
        s_container_pre_processing = StepContainer(name="preprocessing")
        s_container_pre_processing.prelaunch_functions = [
            partial(self.formatting_reference_data)
        ]
        s_container_init = StepContainer(name="classifications_init")

        s_container_spatial_distrib = StepContainer(
            name="classifications_spatial_distrib"
        )
        s_container_spatial_distrib.prelaunch_functions = [
            partial(self.models_distribution)
        ]

        # build chain
        # init steps
        self.init_step_group(s_container_pre_processing, s_container_init)
        # sampling steps
        self.sampling_step_group(s_container_init, s_container_spatial_distrib)
        # learning steps
        self.learning_step_group(s_container_spatial_distrib)

        # # classifications
        self.classification_step_group(s_container_spatial_distrib)

        # # mosaic step
        # self.mosaic_step_group(s_container_spatial_distrib)

        # validation steps
        self.validation_steps_group(s_container_spatial_distrib)
        return [
            s_container_pre_processing,
            s_container_init,
            s_container_spatial_distrib,
        ]

    def pre_check(self) -> None:
        """Perform some checks on data provided by the user."""
        params = rcf.Iota2Parameters(self.i2_cfg_params)
        check_sensors_data(
            params,
            self.__class__.__name__,
            self.i2_cfg_params.get_param("chain", "minimum_required_dates"),
            self.i2_cfg_params.get_param(
                "sensors_data_interpolation", "use_gapfilling"
            ),
            self.i2_cfg_params.get_param("arg_train", "features_from_raw_dates"),
        )
        check_databases_consistency(
            self.i2_cfg_params.get_param("chain", "ground_truth"),
            self.i2_cfg_params.get_param("chain", "data_field"),
            self.i2_cfg_params.get_param("chain", "region_path"),
            self.i2_cfg_params.get_param("chain", "region_field"),
        )
        check_input_text_files(
            self.i2_cfg_params.get_param("chain", "ground_truth"),
            self.i2_cfg_params.get_param("chain", "data_field"),
            self.i2_cfg_params.get_param("chain", "nomenclature_path"),
            self.i2_cfg_params.get_param("chain", "color_table"),
        )
        self.check_config_parameters()

    def check_config_parameters(self) -> None:
        """Check parameters consistency across all sections."""
        try:
            # ensure that all tiles required are in folder
            tiles = self.i2_cfg_params.get_param("chain", "list_tile").split(" ")
            path_to_test = [
                self.i2_cfg_params.get_param("chain", "s2_path"),
                self.i2_cfg_params.get_param("chain", "s2_l3a_path"),
                self.i2_cfg_params.get_param("chain", "l8_path"),
                self.i2_cfg_params.get_param("chain", "l5_path_old"),
                self.i2_cfg_params.get_param("chain", "l8_path_old"),
                self.i2_cfg_params.get_param("chain", "s2_s2c_path"),
            ]

            for path in path_to_test:
                if path is not None:
                    for tile in tiles:
                        ccp.test_ifexists(str(Path(path) / tile))
        except sErr.ConfigFileError as err:
            raise ConfigError(str(err)) from err

        # parameters compatibilities check
        classier_probamap_avail = ["sharkrf"]
        if (
            self.i2_cfg_params.get_param("arg_classification", "enable_probability_map")
            is True
            and self.i2_cfg_params.get_param("arg_train", "classifier").lower()
            not in classier_probamap_avail
        ):
            raise ConfigError(
                "'enable_probability_map:True' only available with "
                "the 'sharkrf' classifier"
            )
        if (
            self.i2_cfg_params.get_param("chain", "region_path") is None
            and self.i2_cfg_params.get_param("arg_classification", "classif_mode")
            == "fusion"
        ):
            raise ConfigError(
                "you can't chose 'one_region' mode and ask a fusion of "
                "classifications\n"
            )
        if (
            self.i2_cfg_params.get_param(
                "arg_classification", "merge_final_classifications"
            )
            and self.i2_cfg_params.get_param("arg_train", "runs") == 1
        ):
            raise ConfigError(
                "these parameters are incompatible runs:1 and"
                " merge_final_classifications:True"
            )
        if (
            self.i2_cfg_params.get_param("arg_train", "split_ground_truth") is False
            and self.i2_cfg_params.get_param("arg_train", "runs") != 1
        ):
            raise ConfigError(
                "these parameters are incompatible split_ground_truth : False and "
                "runs different from 1"
            )
        if (
            self.i2_cfg_params.get_param(
                "arg_classification", "merge_final_classifications"
            )
            and self.i2_cfg_params.get_param("arg_train", "split_ground_truth") is False
        ):
            raise ConfigError(
                "these parameters are incompatible merge_final_classifications"
                ":True and split_ground_truth : False"
            )
        if self.i2_cfg_params.get_param(
            "scikit_models_parameters", "model_type"
        ) is not None and self.i2_cfg_params.get_param(
            "external_features", "external_features_flag"
        ):
            raise ConfigError(
                "these parameters are incompatible external_features "
                "and scikit_models_parameters"
            )
        self.check_externalfeatures_consistency()

    def init_dict_control_variables(self) -> None:
        """
        Create and fill the control variables dictionary (self.control_var) using configuration
        file parameters
        """
        # control variable
        self.control_var["tile_list"] = self.i2_cfg_params.get_param(
            "chain", "list_tile"
        ).split(" ")
        self.control_var["check_inputs"] = self.i2_cfg_params.get_param(
            "chain", "check_inputs"
        )
        self.control_var["Sentinel1"] = self.i2_cfg_params.get_param("chain", "s1_path")
        self.control_var["shapeRegion"] = self.i2_cfg_params.get_param(
            "chain", "region_path"
        )
        self.control_var["classif_mode"] = self.i2_cfg_params.get_param(
            "arg_classification", "classif_mode"
        )
        self.control_var["sample_management"] = self.i2_cfg_params.get_param(
            "arg_train", "sample_management"
        )
        self.control_var["sample_augmentation"] = dict(
            self.i2_cfg_params.get_param("arg_train", "sample_augmentation")
        )
        self.control_var["sample_augmentation_flag"] = self.control_var[
            "sample_augmentation"
        ]["activate"]
        self.control_var["classifier"] = self.i2_cfg_params.get_param(
            "arg_train", "classifier"
        )
        self.control_var["keep_runs_results"] = self.i2_cfg_params.get_param(
            "arg_classification", "keep_runs_results"
        )
        self.control_var["merge_final_classifications"] = self.i2_cfg_params.get_param(
            "arg_classification", "merge_final_classifications"
        )
        self.control_var["ground_truth"] = self.i2_cfg_params.get_param(
            "chain", "ground_truth"
        )
        self.control_var["runs"] = self.i2_cfg_params.get_param("arg_train", "runs")
        self.control_var["data_field"] = self.i2_cfg_params.get_param(
            "chain", "data_field"
        )
        self.control_var["outStat"] = self.i2_cfg_params.get_param(
            "chain", "output_statistics"
        )
        self.control_var["gridsize"] = self.i2_cfg_params.get_param(
            "simplification", "gridsize"
        )
        self.control_var["umc1"] = self.i2_cfg_params.get_param(
            "simplification", "umc1"
        )
        self.control_var["umc2"] = self.i2_cfg_params.get_param(
            "simplification", "umc2"
        )
        self.control_var["rssize"] = self.i2_cfg_params.get_param(
            "simplification", "rssize"
        )
        self.control_var["inland"] = self.i2_cfg_params.get_param(
            "simplification", "inland"
        )
        self.control_var["iota2_outputs_dir"] = self.i2_cfg_params.get_param(
            "chain", "output_path"
        )
        self.control_var["use_scikitlearn"] = (
            self.i2_cfg_params.get_param("scikit_models_parameters", "model_type")
            is not None
        )
        self.control_var["nomenclature"] = self.i2_cfg_params.get_param(
            "simplification", "nomenclature"
        )
        if self.i2_cfg_params.get_param("obia", "obia_segmentation_path") is None:
            self.control_var["provided_segmentation"] = False
        else:
            self.control_var["provided_segmentation"] = True
        self.control_var["enable_external_features"] = self.i2_cfg_params.get_param(
            "external_features", "external_features_flag"
        )
        self.control_var["remove_output_path"] = self.i2_cfg_params.get_param(
            "chain", "remove_output_path"
        )

    def init_step_group(
        self, pre_processing_steps: StepContainer, s_container: StepContainer
    ) -> None:
        """
        Initialize initialisation steps
        """
        if self.control_var["check_inputs"]:
            pre_processing_steps.append(
                partial(
                    check_inputs_step.CheckInputsClassifWorkflow,
                    self.cfg,
                    self.config_resources,
                ),
                "init",
            )
        pre_processing_steps.append(
            partial(
                sensors_preprocess.SensorsPreprocess,
                self.cfg,
                self.config_resources,
            ),
            "init",
        )
        s_container.append(
            partial(
                common_masks.CommonMasks,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "init",
        )
        s_container.append(
            partial(
                pixel_validity.PixelValidity,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "init",
        )
        if not self.control_var["provided_segmentation"]:
            s_container.append(
                partial(
                    slic_segmentation.SlicSegmentation,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "init",
            )
        s_container.append(
            partial(
                prepare_obia_seg.PrepareObiaSeg,
                self.cfg,
                self.config_resources,
            ),
            "init",
        )
        s_container.append(
            partial(
                compute_intersection_seg_regions.ComputeIntersectionSegRegions,
                self.cfg,
                self.config_resources,
            ),
            "init",
        )

    def sampling_step_group(
        self, first_container: StepContainer, second_container: StepContainer
    ) -> None:
        """
        Declare each step for samples management
        """
        first_container.append(
            partial(
                envelope.Envelope,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )
        if not self.control_var["shapeRegion"]:
            first_container.append(
                partial(
                    gen_region_vector.GenRegionVector,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "sampling",
            )
        first_container.append(
            partial(
                vector_formatting.VectorFormatting,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )
        if (
            self.control_var["shapeRegion"]
            and self.control_var["classif_mode"] == "fusion"
        ):
            first_container.append(
                partial(
                    split_samples.SplitSamples,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "sampling",
            )

        second_container.append(
            partial(
                samples_merge.SamplesMerge,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )
        second_container.append(
            partial(
                intersect_seg_learn.IntersectSegLearn,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )
        second_container.append(
            partial(
                learning_zonal_statistics.LearningZonalStatistics,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )

    def learning_step_group(self, s_container: StepContainer) -> None:
        """
        Initialize learning step
        """
        s_container.append(
            partial(
                learning_obia.ObiaLearning,
                self.cfg,
                self.config_resources,
            ),
            "learning",
        )

    def classification_step_group(self, s_container: StepContainer) -> None:
        """
        Initialize classification steps
        """
        s_container.append(
            partial(
                classification_obia.ObiaClassification,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "classification",
        )

    def validation_steps_group(self, s_container: StepContainer) -> None:
        """Initialize validation steps."""
        s_container.append(
            partial(
                compute_metrics_obia.ComputeMetricsObia,
                self.cfg,
                self.config_resources,
            ),
            "validation",
        )
        s_container.append(
            partial(
                merge_tiles_obia.MergeTilesObia,
                self.cfg,
                self.config_resources,
            ),
            "validation",
        )
        s_container.append(
            partial(
                obia_final_metrics.MergeFinalMetrics,
                self.cfg,
                self.config_resources,
            ),
            "validation",
        )

    def generate_output_directories(self, first_step_index: int, restart: bool) -> None:
        """
        Generate expected output directories, depending on the given state of the chain
        parameters
        """
        i2_output_dir = self.control_var["iota2_outputs_dir"]
        rm_if_exists = self.control_var["remove_output_path"]

        task_status_file = str(Path(i2_output_dir, I2_CONST.i2_tasks_status_filename))

        restart = restart and Path(task_status_file).exists()
        rm_if_exists = rm_if_exists and Path(i2_output_dir).exists()

        if rm_if_exists and first_step_index != 0 or restart:
            pass
        elif rm_if_exists:
            shutil.rmtree(i2_output_dir)

        if restart:
            pass
        elif (first_step_index == 0 and not restart) or not Path(
            i2_output_dir
        ).exists():
            generate_directories_obia(
                i2_output_dir,
                self.control_var["merge_final_classifications"],
                self.control_var["tile_list"],
            )
        else:
            pass
