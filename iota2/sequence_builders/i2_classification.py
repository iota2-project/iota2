#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Classification builder
"""
import logging
import shutil
from collections import Counter
from functools import partial
from pathlib import Path
from typing import Any

from osgeo import ogr

import iota2.common.i2_constants as i2_const
from iota2.common.iota2_directory import generate_directories
from iota2.common.utils import run
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.sections.cfg_utils import ConfigError
from iota2.sequence_builders.i2_sequence_builder import (
    I2Builder,
    check_databases_consistency,
    check_input_text_files,
    check_sensors_data,
    check_tiled_exogenous_data,
)
from iota2.steps import (
    check_inputs_step,
    classification_fusion,
    classification_otb,
    classification_pytorch,
    classification_scikit,
    common_masks,
    confusion_cmd,
    confusions_merge,
    copy_samples,
    envelope,
    fusions_indecisions,
    gen_region_vector,
    gen_synthetic_samples,
    generate_boundary_reports,
    learn_model,
    merge_classification_boundary,
    merge_seed_classifications,
    model_choice,
    mosaic,
    pixel_validity,
    prob_boundary_fusion,
    produce_boundary_validation,
    report_generation,
    samples_by_models,
    samples_by_tiles,
    samples_extraction,
    samples_merge,
    sampling_learning_polygons,
    sensors_preprocess,
    sk_classifications_merge,
    split_samples,
    stats_samples_model,
    vector_formatting,
)
from iota2.steps.iota2_step import Step, StepContainer
from iota2.typings.i2_types import AvailSchedulers
from iota2.vector_tools.vector_functions import (
    get_fields,
    get_geometry,
    get_layer_name,
    get_re_encoding_labels_dic,
)

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()


class I2Classification(I2Builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """

    def __init__(
        self,
        cfg: str,
        config_resources: Path | None,
        scheduler_type: AvailSchedulers,
        restart: bool = False,
        tasks_states_file: str | None = None,
        hpc_working_directory: str = "TMPDIR",
    ):
        super().__init__(
            cfg,
            config_resources,
            scheduler_type,
            restart,
            tasks_states_file,
            hpc_working_directory,
        )

        # create dict for control variables
        self.control_var: dict[str, Any] = {}
        # steps definitions
        self.steps_group["init"] = {}
        self.steps_group["sampling"] = {}
        self.steps_group["dimred"] = {}
        self.steps_group["learning"] = {}
        self.steps_group["classification"] = {}
        self.steps_group["mosaic"] = {}
        self.steps_group["validation"] = {}

        # build steps
        self.sort_step()

    def get_dir(self) -> list[str]:
        """
        usage : return iota2_directories
        """
        directories = [
            "classif",
            "config_model",
            "dataRegion",
            "envelope",
            "formattingVectors",
            "metaData",
            "samplesSelection",
            "stats",
            "dataAppVal",
            "dimRed",
            "final",
            "learningSamples",
            "model",
            "shapeRegion",
            "features",
        ]

        iota2_outputs_dir = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "output_path"
        )

        return [str(Path(iota2_outputs_dir) / d) for d in directories]

    def formatting_reference_data(self) -> None:
        """
        Formats reference data for re-encoding
        """
        ref_data = self.control_var["ground_truth"]
        ref_data_field = self.control_var["data_field"]
        ref_data_i2_formatted = str(
            Path(self.control_var["iota2_outputs_dir"], "reference_data.shp")
        )
        self.ref_data_formatting(ref_data, ref_data_field, ref_data_i2_formatted)

    def ref_data_formatting(
        self, ref_data: str, ref_data_field: str, ref_data_i2_formatted: str
    ) -> None:
        """re-encode reference data

        Parameters
        ----------
        ref_data
            input reference database
        ref_data_field
            column containing labels database
        ref_data_i2_formatted
            output formatted database
        """
        i2_label_column = I2_CONST.re_encoding_label_name
        ref_data_fields = get_fields(ref_data)
        if i2_label_column in ref_data_fields:
            raise ValueError(
                f"the reference data : {ref_data} contains the field"
                f" {i2_label_column}. Please rename the column name"
            )
        lut_labels = get_re_encoding_labels_dic(ref_data, ref_data_field)
        assert lut_labels is not None
        user_fields = get_fields(ref_data)
        layer_name = get_layer_name(ref_data)
        lower_clause = ",".join(
            [f"{user_field} AS {user_field.lower()}" for user_field in user_fields]
        )
        run(
            f"ogr2ogr {ref_data_i2_formatted} {ref_data} "
            f"-sql 'SELECT {lower_clause} FROM \"{layer_name}\"'"
        )

        ds = ogr.Open(ref_data_i2_formatted)
        driver = ds.GetDriver()
        data_src = driver.Open(ref_data_i2_formatted, 1)
        layer = data_src.GetLayer()
        layer.CreateField(ogr.FieldDefn(i2_label_column, ogr.OFTInteger))
        for feat in layer:
            feat.SetField(i2_label_column, lut_labels[feat.GetField(ref_data_field)])
            layer.SetFeature(feat)

    def build_steps(self, cfg: str) -> list[StepContainer]:
        """
        build steps
        """
        Step.set_models_spatial_information(self.tiles, {})

        # control variable
        self.init_dict_control_variables()

        # will contains all IOTA² steps
        s_container_pre_processing = StepContainer(name="preprocessing")
        s_container_pre_processing.prelaunch_functions = [
            partial(self.formatting_reference_data)
        ]
        s_container_init = StepContainer(name="classifications_init")
        if self.i2_cfg_params.get_param("external_features", "functions"):
            s_container_init.prelaunch_functions = [
                self.test_user_feature_with_fake_data
            ]

        s_container_spatial_distrib = StepContainer(
            name="classifications_spatial_distrib"
        )
        s_container_spatial_distrib.prelaunch_functions = [
            partial(self.models_distribution)
        ]
        if self.control_var["external_features_flag"]:
            s_container_spatial_distrib.prelaunch_functions.append(
                partial(self.chunks_intersections)
            )

        # build chain
        # init steps
        self.init_step_group(s_container_pre_processing, s_container_init)
        # sampling steps
        self.sampling_step_group(s_container_init, s_container_spatial_distrib)
        # learning steps
        self.learning_step_group(s_container_spatial_distrib)

        # classifications
        self.classification_step_group(s_container_spatial_distrib)

        # mosaic step
        self.mosaic_step_group(s_container_spatial_distrib)

        # validation steps
        self.validation_steps_group(s_container_spatial_distrib)
        return [
            s_container_pre_processing,
            s_container_init,
            s_container_spatial_distrib,
        ]

    def pre_check(self) -> None:
        """Perform some checks on data provided by the user."""
        params = rcf.Iota2Parameters(self.i2_cfg_params)
        check_sensors_data(
            params,
            self.__class__.__name__,
            self.i2_cfg_params.get_param("chain", "minimum_required_dates"),
            self.i2_cfg_params.get_param(
                "sensors_data_interpolation", "use_gapfilling"
            ),
            self.i2_cfg_params.get_param("arg_train", "features_from_raw_dates"),
        )
        check_databases_consistency(
            self.i2_cfg_params.get_param("chain", "ground_truth"),
            self.i2_cfg_params.get_param("chain", "data_field"),
            self.i2_cfg_params.get_param("chain", "region_path"),
            self.i2_cfg_params.get_param("chain", "region_field"),
        )
        check_input_text_files(
            self.i2_cfg_params.get_param("chain", "ground_truth"),
            self.i2_cfg_params.get_param("chain", "data_field"),
            self.i2_cfg_params.get_param("chain", "nomenclature_path"),
            self.i2_cfg_params.get_param("chain", "color_table"),
        )
        check_tiled_exogenous_data(
            self.i2_cfg_params.get_param("external_features", "exogeneous_data"),
            self.i2_cfg_params.get_param("chain", "list_tile"),
        )
        self.check_config_parameters()

    def check_config_parameters(self) -> None:
        """Check parameters consistency across the whole iota2's cfg file."""
        super().check_config_parameters()
        # parameters compatibilities check
        classier_probamap_avail = ["sharkrf"]
        neural_network_params = self.i2_cfg_params.get_param(
            "arg_train", "deep_learning_parameters"
        )
        nn_enable = "dl_name" in neural_network_params

        if (
            self.i2_cfg_params.get_param("arg_classification", "enable_probability_map")
            is True
            and self.i2_cfg_params.get_param("arg_train", "classifier")
            and (
                self.i2_cfg_params.get_param("arg_train", "classifier").lower()
                not in classier_probamap_avail
                and not nn_enable
            )
        ):
            raise ConfigError(
                "'enable_probability_map:True' only available with "
                "the 'sharkrf' and pytorch classifiers"
            )

        if (
            self.i2_cfg_params.get_param("chain", "region_path") is None
            and self.i2_cfg_params.get_param("arg_classification", "classif_mode")
            == "fusion"
        ):
            raise ConfigError(
                "you can't chose 'one_region' mode and ask a fusion of "
                "classifications\n"
            )
        if (
            self.i2_cfg_params.get_param(
                "arg_classification", "merge_final_classifications"
            )
            and self.i2_cfg_params.get_param("arg_train", "runs") == 1
        ):
            raise ConfigError(
                "these parameters are incompatible runs:1 and"
                " merge_final_classifications:True"
            )
        if (
            self.i2_cfg_params.get_param("arg_train", "split_ground_truth") is False
            and self.i2_cfg_params.get_param("arg_train", "runs") != 1
        ):
            raise ConfigError(
                "these parameters are incompatible split_ground_truth : False and "
                "runs different from 1"
            )
        if (
            self.i2_cfg_params.get_param(
                "arg_classification", "merge_final_classifications"
            )
            and self.i2_cfg_params.get_param("arg_train", "split_ground_truth") is False
        ):
            raise ConfigError(
                "these parameters are incompatible merge_final_classifications"
                ":True and split_ground_truth : False"
            )

        self.check_externalfeatures_consistency()
        self.exclusive_classification_workflow()

    def exclusive_classification_workflow(self) -> None:
        """Check if the user is asking only one classifier."""
        otb_classifier_enable = bool(
            self.i2_cfg_params.get_param("arg_train", "classifier")
        )
        scikit_enable = bool(
            self.i2_cfg_params.get_param("scikit_models_parameters", "model_type")
        )

        deep_enable = False
        if self.i2_cfg_params.get_param("arg_train", "deep_learning_parameters"):
            deep_enable = bool(
                self.i2_cfg_params.get_param("arg_train", "deep_learning_parameters")[
                    "dl_name"
                ]
            )

        classifiers = [
            ("deep learning", deep_enable),
            ("otb", otb_classifier_enable),
            ("scikit-learn", scikit_enable),
        ]
        if not any(enable for _, enable in classifiers):
            raise ConfigError(
                "No classifier detected in the configuration file, please set one."
            )
        count = Counter([enable for _, enable in classifiers])
        if count[True] > 1:
            classifiers_detected = []
            for classifier_name, enable in classifiers:
                if enable:
                    classifiers_detected.append(classifier_name)
            raise ConfigError(
                "Multiple classifiers detected please choose one among :"
                f" {', '.join(classifiers_detected)}"
            )

        # in pytorch mode
        if deep_enable:
            deep_learning_parameters = self.i2_cfg_params.get_param(
                "arg_train", "deep_learning_parameters"
            )
            # checks optimization/selection criterion
            ko_criterion = ["MSE", "MAE", "HuberLoss"]
            moc = deep_learning_parameters["model_optimization_criterion"]
            msc = deep_learning_parameters["model_selection_criterion"]
            if moc in ko_criterion:
                raise ConfigError(
                    f"model_optimization_criterion '{moc}' "
                    "not valid in case of classification"
                )
            if msc in ko_criterion:
                raise ConfigError(
                    f"model_selection_criterion '{msc}' "
                    "not valid in case of classification"
                )

    def init_dict_control_variables(self) -> None:
        """
        Create and fill the control variables dictionary (self.control_var) using configuration
        file parameters
        """
        # control variable
        self.control_var["tile_list"] = self.i2_cfg_params.get_param(
            "chain", "list_tile"
        ).split(" ")
        self.control_var["check_inputs"] = self.i2_cfg_params.get_param(
            "chain", "check_inputs"
        )
        self.control_var["Sentinel1"] = self.i2_cfg_params.get_param("chain", "s1_path")
        self.control_var["shapeRegion"] = self.i2_cfg_params.get_param(
            "chain", "region_path"
        )
        self.control_var["classif_mode"] = self.i2_cfg_params.get_param(
            "arg_classification", "classif_mode"
        )
        self.control_var["sample_management"] = self.i2_cfg_params.get_param(
            "arg_train", "sample_management"
        )
        self.control_var["sample_augmentation"] = dict(
            self.i2_cfg_params.get_param("arg_train", "sample_augmentation")
        )
        self.control_var["sample_augmentation_flag"] = self.control_var[
            "sample_augmentation"
        ]["activate"]
        self.control_var["classifier"] = self.i2_cfg_params.get_param(
            "arg_train", "classifier"
        )
        self.control_var["keep_runs_results"] = self.i2_cfg_params.get_param(
            "arg_classification", "keep_runs_results"
        )
        self.control_var["merge_final_classifications"] = self.i2_cfg_params.get_param(
            "arg_classification", "merge_final_classifications"
        )
        self.control_var["ground_truth"] = self.i2_cfg_params.get_param(
            "chain", "ground_truth"
        )
        self.control_var["runs"] = self.i2_cfg_params.get_param("arg_train", "runs")
        self.control_var["data_field"] = self.i2_cfg_params.get_param(
            "chain", "data_field"
        )
        self.control_var["outStat"] = self.i2_cfg_params.get_param(
            "chain", "output_statistics"
        )
        self.control_var["gridsize"] = self.i2_cfg_params.get_param(
            "simplification", "gridsize"
        )
        self.control_var["umc1"] = self.i2_cfg_params.get_param(
            "simplification", "umc1"
        )
        self.control_var["umc2"] = self.i2_cfg_params.get_param(
            "simplification", "umc2"
        )
        self.control_var["rssize"] = self.i2_cfg_params.get_param(
            "simplification", "rssize"
        )
        self.control_var["inland"] = self.i2_cfg_params.get_param(
            "simplification", "inland"
        )
        self.control_var["iota2_outputs_dir"] = self.i2_cfg_params.get_param(
            "chain", "output_path"
        )
        self.control_var["use_scikitlearn"] = (
            self.i2_cfg_params.get_param("scikit_models_parameters", "model_type")
            is not None
        )
        self.control_var["nomenclature"] = self.i2_cfg_params.get_param(
            "simplification", "nomenclature"
        )
        self.control_var["enable_external_features"] = self.i2_cfg_params.get_param(
            "external_features", "external_features_flag"
        )
        self.control_var["remove_output_path"] = self.i2_cfg_params.get_param(
            "chain", "remove_output_path"
        )
        self.control_var["enable_neural_network"] = self.i2_cfg_params.get_param(
            "arg_train", "deep_learning_parameters"
        )
        self.control_var["features_from_raw_dates"] = self.i2_cfg_params.get_param(
            "arg_train", "features_from_raw_dates"
        )
        self.control_var["sampling_validation"] = self.i2_cfg_params.get_param(
            "arg_train", "sampling_validation"
        )
        self.control_var["enable_boundary_fusion"] = self.i2_cfg_params.get_param(
            "arg_classification", "enable_boundary_fusion"
        )
        self.control_var["comparison_mode"] = self.i2_cfg_params.get_param(
            "arg_classification", "boundary_comparison_mode"
        )
        self.control_var["chunk_size_mode"] = self.i2_cfg_params.get_param(
            "python_data_managing", "chunk_size_mode"
        )
        self.control_var["external_features_flag"] = self.i2_cfg_params.get_param(
            "external_features", "external_features_flag"
        )

    def init_step_group(
        self, pre_processing_steps: StepContainer, init_steps: StepContainer
    ) -> None:
        """
        Initialize initialisation steps
        """
        if self.control_var["check_inputs"]:
            pre_processing_steps.append(
                partial(
                    check_inputs_step.CheckInputsClassifWorkflow,
                    self.cfg,
                    self.config_resources,
                    "classif",
                ),
                "init",
            )
        pre_processing_steps.append(
            partial(
                sensors_preprocess.SensorsPreprocess,
                self.cfg,
                self.config_resources,
            ),
            "init",
        )
        init_steps.append(
            partial(
                common_masks.CommonMasks,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "init",
        )
        init_steps.append(
            partial(
                pixel_validity.PixelValidity,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "init",
        )

    def sampling_step_group(
        self, first_container: StepContainer, second_container: StepContainer
    ) -> None:
        """
        Declare each step for samples management
        """
        first_container.append(
            partial(
                envelope.Envelope,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )
        if not self.control_var["shapeRegion"]:
            first_container.append(
                partial(
                    gen_region_vector.GenRegionVector,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "sampling",
            )
        first_container.append(
            partial(
                vector_formatting.VectorFormatting,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )
        if (
            self.control_var["shapeRegion"]
            and self.control_var["classif_mode"] == "fusion"
        ):
            first_container.append(
                partial(
                    split_samples.SplitSamples,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "sampling",
            )
        if get_geometry(self.control_var["ground_truth"]) != "POINT":
            second_container.append(
                partial(
                    samples_merge.SamplesMerge,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "sampling",
            )
            second_container.append(
                partial(
                    stats_samples_model.StatsSamplesModel,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "sampling",
            )
            second_container.append(
                partial(
                    sampling_learning_polygons.SamplingLearningPolygons,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "sampling",
            )

        second_container.append(
            partial(
                samples_by_tiles.SamplesByTiles,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )

        second_container.append(
            partial(
                samples_extraction.SamplesExtraction,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )
        second_container.append(
            partial(samples_by_models.SamplesByModels, self.cfg, self.config_resources),
            "sampling",
        )
        transfert_samples = False
        if (
            self.control_var["sample_management"]
            and self.control_var["sample_management"].lower() != "none"
        ):
            transfert_samples = True
            second_container.append(
                partial(
                    copy_samples.CopySamples,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "sampling",
            )
        if self.control_var["sample_augmentation_flag"]:
            second_container.append(
                partial(
                    gen_synthetic_samples.GenSyntheticSamples,
                    self.cfg,
                    self.config_resources,
                    transfert_samples,
                    self.working_directory,
                ),
                "sampling",
            )

    def learning_step_group(self, s_container: StepContainer) -> None:
        """Initialize learning step."""
        s_container.append(
            partial(
                learn_model.LearnModel,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "learning",
        )

    def classification_step_group(self, s_container: StepContainer) -> None:
        """Initialize classification steps."""
        deep_learning_enabled = "dl_name" in self.control_var["enable_neural_network"]
        if self.control_var["features_from_raw_dates"] or deep_learning_enabled:
            s_container.append(
                partial(model_choice.ModelChoice, self.cfg, self.config_resources),
                "classification",
            )

        if not self.control_var["use_scikitlearn"] and not deep_learning_enabled:
            s_container.append(
                partial(
                    classification_otb.ClassificationUsingOTB,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "classification",
            )
        elif self.control_var["use_scikitlearn"]:
            s_container.append(
                partial(
                    classification_scikit.ClassificationUsingScikit,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "classification",
            )
        else:
            s_container.append(
                partial(
                    classification_pytorch.ClassificationUsingPytorch,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "classification",
            )
        if (
            self.control_var["use_scikitlearn"]
            or self.control_var["enable_external_features"]
            or deep_learning_enabled
            or self.control_var["features_from_raw_dates"]
        ):
            s_container.append(
                partial(
                    sk_classifications_merge.ScikitClassificationsMerge,
                    self.cfg,
                    self.config_resources,
                ),
                "classification",
            )
        if (
            self.control_var["classif_mode"] == "fusion"
            and self.control_var["shapeRegion"]
        ):
            s_container.append(
                partial(
                    classification_fusion.ClassificationsFusion,
                    self.cfg,
                    self.config_resources,
                ),
                "classification",
            )
            s_container.append(
                partial(
                    fusions_indecisions.FusionsIndecisions,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "classification",
            )

    def mosaic_step_group(self, s_container: StepContainer) -> None:
        """
        Initialize mosaic step
        """
        # fuse at boundary steps
        if self.control_var["enable_boundary_fusion"]:
            s_container.append(
                partial(
                    prob_boundary_fusion.ProbBoundaryFusion,
                    self.cfg,
                    self.config_resources,
                ),
                "mosaic",
            )
            s_container.append(
                partial(
                    merge_classification_boundary.MergeClassificationBoundary,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "mosaic",
            )
        s_container.append(
            partial(
                mosaic.Mosaic, self.cfg, self.config_resources, self.working_directory
            ),
            "mosaic",
        )

    def validation_steps_group(self, s_container: StepContainer) -> None:
        """Initialize validation steps."""
        s_container.append(
            partial(
                confusion_cmd.ConfusionCmd,
                self.cfg,
                self.config_resources,
            ),
            "validation",
        )
        if self.control_var["keep_runs_results"]:
            s_container.append(
                partial(
                    confusions_merge.ConfusionsMerge,
                    self.cfg,
                    self.config_resources,
                ),
                "validation",
            )
            s_container.append(
                partial(
                    report_generation.ReportGeneration,
                    self.cfg,
                    self.config_resources,
                ),
                "validation",
            )
            # only if comparison mode is enabled
            if self.control_var["comparison_mode"]:
                s_container.append(
                    partial(
                        produce_boundary_validation.ProduceBoundaryValidation,
                        self.cfg,
                        self.config_resources,
                        self.working_directory,
                    ),
                    "validation",
                )
                s_container.append(
                    partial(
                        generate_boundary_reports.GenerateBoundaryReports,
                        self.cfg,
                        self.config_resources,
                    ),
                    "validation",
                )

        if (
            self.control_var["merge_final_classifications"]
            and self.control_var["runs"] > 1
        ):
            s_container.append(
                partial(
                    merge_seed_classifications.MergeSeedClassifications,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "validation",
            )

    def generate_output_directories(
        self, first_step_index: int, restart: bool, builder_index: int = 0
    ) -> None:
        """
        Generate expected output directories, depending on the given state of the chain
        parameters
        """
        i2_output_dir = self.control_var["iota2_outputs_dir"]
        rm_if_exists = self.control_var["remove_output_path"]

        task_status_file = str(Path(i2_output_dir, I2_CONST.i2_tasks_status_filename))

        restart = restart and Path(task_status_file).exists()
        rm_if_exists = rm_if_exists and Path(i2_output_dir).exists()
        if rm_if_exists and first_step_index != 0 or restart:
            pass
        elif builder_index == 0 and rm_if_exists:
            shutil.rmtree(i2_output_dir)
        if restart:
            pass
        elif (first_step_index == 0 and not restart) or not Path(
            i2_output_dir
        ).exists():
            generate_directories(
                i2_output_dir,
                self.control_var["merge_final_classifications"],
                self.control_var["tile_list"],
                self.control_var["enable_boundary_fusion"],
                self.control_var["comparison_mode"],
            )

        else:
            pass
