#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Features map builder
"""
import logging
import shutil
from functools import partial
from pathlib import Path
from typing import Any

import iota2.common.i2_constants as i2_const
from iota2.common import service_error as sErr
from iota2.common.iota2_directory import generate_features_maps_directories
from iota2.configuration_files import check_config_parameters as ccp
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.sections.cfg_utils import ConfigError
from iota2.sequence_builders.i2_sequence_builder import (
    I2Builder,
    check_sensors_data,
    check_tiled_exogenous_data,
)
from iota2.steps import (
    common_masks,
    merge_features_maps,
    merge_features_maps_by_tiles,
    sensors_preprocess,
    write_features_map,
)
from iota2.steps.iota2_step import Step, StepContainer
from iota2.typings.i2_types import AvailSchedulers

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()
I2ParameterT = dict[str, Any]


class I2FeaturesMap(I2Builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """

    def __init__(
        self,
        cfg: str,
        config_resources: Path | None,
        scheduler_type: AvailSchedulers,
        restart: bool = False,
        tasks_states_file: str | None = None,
        hpc_working_directory: str = "TMPDIR",
    ):
        super().__init__(
            cfg,
            config_resources,
            scheduler_type,
            restart,
            tasks_states_file,
            hpc_working_directory,
        )

        # steps definitions
        self.steps_group["init"] = {}
        self.steps_group["writing"] = {}
        self.steps_group["mosaic"] = {}

        # build steps
        self.sort_step()

        # control variable
        self.control_var = self.init_dict_control_variables()

    def get_dir(self) -> list[str]:
        """
        usage : return iota2_directories
        """
        directories = ["final", "features", "customF", "by_tiles"]

        iota2_outputs_dir = self.i2_cfg_params.get_param("chain", "output_path")

        return [str(Path(iota2_outputs_dir) / d) for d in directories]

    def build_steps(self, cfg: str) -> list[StepContainer]:
        """Build steps."""
        # Init self.tiles for step communication
        Step.set_models_spatial_information(self.tiles, {})
        # will contains all IOTA² steps
        s_container_pre_processing = StepContainer("preprocess")
        s_container = StepContainer("featuresbuilder")
        if self.i2_cfg_params.get_param("external_features", "functions"):
            s_container.prelaunch_functions = [
                partial(self.test_user_feature_with_fake_data)
            ]

        s_container_pre_processing.append(
            partial(
                sensors_preprocess.SensorsPreprocess,
                self.cfg,
                self.config_resources,
            ),
            "init",
        )
        s_container.append(
            partial(common_masks.CommonMasks, self.cfg, self.config_resources), "init"
        )

        s_container.append(
            partial(
                write_features_map.WriteFeaturesMap, self.cfg, self.config_resources
            ),
            "writing",
        )

        s_container.append(
            partial(
                merge_features_maps_by_tiles.MergeFeaturesMapsByTiles,
                self.cfg,
                self.config_resources,
            ),
            "writing",
        )

        s_container.append(
            partial(
                merge_features_maps.MergeFeaturesMaps, self.cfg, self.config_resources
            ),
            "mosaic",
        )
        return [s_container_pre_processing, s_container]

    def pre_check(self) -> None:
        """Check i2_features configuration file parameters."""
        params = rcf.Iota2Parameters(self.i2_cfg_params)
        check_sensors_data(
            params,
            self.__class__.__name__,
            self.i2_cfg_params.get_param("chain", "minimum_required_dates"),
            self.i2_cfg_params.get_param(
                "sensors_data_interpolation", "use_gapfilling"
            ),
            self.i2_cfg_params.get_param("arg_train", "features_from_raw_dates"),
        )
        check_tiled_exogenous_data(
            self.i2_cfg_params.get_param("external_features", "exogeneous_data"),
            self.i2_cfg_params.get_param("chain", "list_tile"),
        )
        self.check_config_parameters()

    def check_config_parameters(self) -> None:
        """Check parameters consistency."""
        try:
            # ensure that all tiles required are in folder
            tiles = self.i2_cfg_params.get_param("chain", "list_tile").split(" ")
            path_to_test = [
                self.i2_cfg_params.get_param("chain", "s2_path"),
                self.i2_cfg_params.get_param("chain", "s2_l3a_path"),
                self.i2_cfg_params.get_param("chain", "l8_path"),
                self.i2_cfg_params.get_param("chain", "l5_path_old"),
                self.i2_cfg_params.get_param("chain", "l8_path_old"),
                self.i2_cfg_params.get_param("chain", "s2_s2c_path"),
            ]

            for path in path_to_test:
                if path is not None:
                    for tile in tiles:
                        tile_path = str(Path(path) / tile)
                        ccp.test_ifexists(tile_path)
        except sErr.ConfigFileError as err:
            raise ConfigError(str(err)) from err
        self.check_externalfeatures_consistency()

    def init_dict_control_variables(self) -> I2ParameterT:
        """
        Create and fill the control variables dictionary (self.control_var) using configuration
        file parameters
        """
        # control variable
        control_var: I2ParameterT = {}
        control_var["iota2_outputs_dir"] = self.i2_cfg_params.get_param(
            "chain", "output_path"
        )
        control_var["remove_output_path"] = self.i2_cfg_params.get_param(
            "chain", "remove_output_path"
        )
        return control_var

    def generate_output_directories(
        self, first_step_index: int, restart: bool, builder_index: int = 0
    ) -> None:
        """
        Generate expected output directories, depending on the given state of the chain
        parameters
        """
        i2_output_dir = self.control_var["iota2_outputs_dir"]
        rm_if_exists = self.control_var["remove_output_path"]

        task_status_file = str(Path(i2_output_dir, I2_CONST.i2_tasks_status_filename))

        restart = restart and Path(task_status_file).exists()
        rm_if_exists = rm_if_exists and Path(i2_output_dir).exists()

        if rm_if_exists and first_step_index != 0 or restart:
            pass
        elif rm_if_exists and builder_index == 0:
            shutil.rmtree(i2_output_dir)

        if restart:
            pass
        elif (first_step_index == 0 and not restart) or not Path(
            i2_output_dir
        ).exists():
            tiles = self.i2_cfg_params.get_param("chain", "list_tile").split(" ")
            generate_features_maps_directories(i2_output_dir, tiles)
        else:
            pass
