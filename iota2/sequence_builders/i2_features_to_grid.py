#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Builder for tiling images
"""
import logging
import re
import shutil
from functools import partial
from pathlib import Path

import geopandas as gpd
import pandas as pd
from sensorsio import sentinel1 as sio_s1
from shapely import wkb

import iota2.common.i2_constants as i2_const
from iota2.common.file_utils import file_search_and, file_search_or, sort_by_first_elem
from iota2.common.raster_utils import rasters_footprints
from iota2.sensors.sentinel1 import Sentinel1
from iota2.sequence_builders.i2_sequence_builder import I2Builder
from iota2.steps import tile_features
from iota2.steps.iota2_step import StepContainer
from iota2.typings.i2_types import AvailSchedulers

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()


class I2FeaturesToGrid(I2Builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """

    def __init__(
        self,
        cfg: str,
        config_resources: Path | None,
        scheduler_type: AvailSchedulers,
        restart: bool = False,
        tasks_states_file: str | None = None,
        hpc_working_directory: str = "TMPDIR",
    ):
        super().__init__(
            cfg,
            config_resources,
            scheduler_type,
            restart,
            tasks_states_file,
            hpc_working_directory,
        )

        self.steps_group["tiler"] = {}

        self.output_path = self.i2_cfg_params.get_param("chain", "output_path")
        s1_dir = self.i2_cfg_params.get_param("chain", "s1_dir")
        self.remove_output_path = self.i2_cfg_params.get_param(
            "chain", "remove_output_path"
        )
        tiles = self.i2_cfg_params.get_param("chain", "list_tile").split(" ")
        tile_field = self.i2_cfg_params.get_param("chain", "tile_field")
        grid_vector_file = self.i2_cfg_params.get_param("chain", "grid")
        raster_grid_path = self.i2_cfg_params.get_param("chain", "rasters_grid_path")
        target_epsg = int(self.i2_cfg_params.get_param("chain", "proj").split(":")[-1])
        features_path = self.i2_cfg_params.get_param("chain", "features_path")
        intersections_list = []
        if features_path is not None:
            features_path = Path(features_path)
            rasters_of_interest = [
                Path(r_file)
                for r_file in filter(
                    lambda x: x.endswith(".tif") or x.endswith(".TIF"),
                    file_search_or(features_path, True, ".tif", ".TIF"),
                )
            ]
            if len(rasters_of_interest) == 0:
                raise FileNotFoundError(f"No image found in {Path(features_path)}")

            if grid_vector_file is not None:
                grid_vector_file = Path(grid_vector_file)
                intersections_list = self.intersections(
                    grid_vector_file,
                    rasters_of_interest,
                    target_epsg,
                    tile_field,
                    tiles,
                )
            if raster_grid_path is not None:
                raster_grid_path = Path(raster_grid_path)
                rasters_grid = [
                    Path(r_file)
                    for r_file in filter(
                        lambda x: x.endswith(".tif")
                        and re.findall(r"(?=(" + "|".join(tiles) + r"))", str(x)),
                        file_search_and(raster_grid_path, True, ".tif"),
                    )
                ]
                intersections_list = self.intersections_from_rasters(
                    rasters_grid, rasters_of_interest
                )

        self.intersection_list = intersections_list
        self.s1_intersections_list: list[tuple[str, str]] | None
        if s1_dir and grid_vector_file:
            self.s1_intersections_list = sort_by_first_elem(
                self.s1_intersections(grid_vector_file, tile_field, s1_dir, tiles)
            )
        elif s1_dir and raster_grid_path:
            self.s1_intersections_list = sort_by_first_elem(
                self.s1_intersections_from_rasters(
                    Path(raster_grid_path), s1_dir, tiles, target_epsg
                )
            )
        else:
            self.s1_intersections_list = None

        # build steps
        self.sort_step()

    def s1_intersections_from_rasters(
        self,
        rasters_grid_dir: Path,
        s1_path: Path,
        tiles_list: list[str],
        target_epsg: int,
    ) -> list[tuple[str, tuple[str, str, str]]]:
        """
        Compute intersections between user tiles and S1 tiles

        Parameters
        ----------
        rasters_grid_dir:
            Path to the input rasters directory
        s1_path:
            Path to S1 data
        tiles_list:
            List of tile names
        target_epsg
            Target projection
        """
        s1_products = list(filter(Sentinel1.is_s1_product, Path(s1_path).iterdir()))
        s1_products = Sentinel1.sort_products(s1_products)
        s1_grid_db = self.s1_grid(s1_products)
        s1_grid_db.to_crs(epsg=4326, inplace=True)
        s1_grid_db.to_crs(epsg=target_epsg, inplace=True)

        raster_grid_files = []
        for tile in tiles_list:
            tile_file = rasters_grid_dir / f"{tile}.tif"
            if tile_file.exists():
                raster_grid_files.append(tile_file)
            else:
                print(
                    f"Warning : tile '{tile}.tif' "
                    f"not found in {str(rasters_grid_dir)}"
                )
        user_grid = rasters_footprints(raster_grid_files, target_epsg)
        intersections_db = gpd.sjoin(user_grid, s1_grid_db, how="left")
        s1_task_args = []
        for _, series in intersections_db.iterrows():
            s1_product = series["path_right"]
            tile_name = series["path_left"]
            tile_geometry = user_grid.loc[user_grid["path"] == tile_name][
                "geometry"
            ].values[0]
            tile_geometry = wkb.loads(wkb.dumps(tile_geometry, output_dimension=2))
            tile_geom_wkt = tile_geometry.wkt
            s1_geometry = s1_grid_db.loc[s1_grid_db["path"] == str(s1_product)][
                "geometry"
            ].values[0]
            s1_geometry_wkt = s1_geometry.wkt
            s1_task_args.append(
                (Path(tile_name).stem, (s1_product, tile_geom_wkt, s1_geometry_wkt))
            )
        return s1_task_args

    def s1_intersections(
        self, grid_vector_file: str, tile_column: str, s1_dir: str, tiles: list[str]
    ) -> list[tuple[str, tuple[str, str, str]]]:
        """
        Compute intersections between user grid and S1 tiles

        Parameters
        ----------
        grid_vector_file:
            Path to the input vector grid directory
        tile_column:
            Name of the column containing tile names
        s1_dir:
            Directory containing S1 data
        tiles:
            List of tile names
        """
        s1_crs = 4326
        s1_products = list(filter(Sentinel1.is_s1_product, Path(s1_dir).iterdir()))
        s1_products = Sentinel1.sort_products(s1_products)
        s1_grid_db = self.s1_grid(s1_products)
        s1_grid_db.to_crs(epsg=s1_crs, inplace=True)

        user_grid_db = gpd.read_file(grid_vector_file)
        user_grid_db = user_grid_db[[tile_column, "geometry"]]
        user_grid_db.to_crs(epsg=s1_crs, inplace=True)
        user_grid_db = user_grid_db.loc[user_grid_db[tile_column].isin(tiles)]

        intersections_db = gpd.sjoin(user_grid_db, s1_grid_db, how="left")
        s1_task_args = []
        for _, series in intersections_db.iterrows():
            s1_product = series["path"]
            tile_name = series[tile_column]
            tile_geometry = user_grid_db.loc[user_grid_db[tile_column] == tile_name][
                "geometry"
            ].values[0]
            tile_geometry = wkb.loads(wkb.dumps(tile_geometry, output_dimension=2))
            tile_geom_wkt = tile_geometry.wkt
            s1_geometry = s1_grid_db.loc[s1_grid_db["path"] == str(s1_product)][
                "geometry"
            ].values[0]
            s1_geometry_wkt = s1_geometry.wkt
            s1_task_args.append(
                (tile_name, (s1_product, tile_geom_wkt, s1_geometry_wkt))
            )
        return s1_task_args

    @staticmethod
    def s1_grid(s1_products: list[Path]) -> gpd.GeoDataFrame:
        """
        Create a vector grid from S1 products (returned as a geopandas dataframe)

        Parameters
        ----------
        s1_products:
            List of paths containing S1 products
        """
        geoms, srs = zip(
            *[
                sio_s1.s1_product_footprint(s1_product / "manifest.safe")
                for s1_product in s1_products
            ]
        )
        assert len(srs) == srs.count(srs[0])

        df_grid = pd.DataFrame(
            {
                "path": [str(path) for path in s1_products],
                "geometry": geoms,
            }
        )
        df_grid = gpd.GeoDataFrame(df_grid, geometry="geometry", crs=srs[0])
        return df_grid

    @staticmethod
    def intersections_from_rasters(
        rasters_grid: list[Path], rasters: list[Path], common_proj: int = 4326
    ) -> list[tuple[str, str]]:
        """Check intersections between two lists of rasters in a common projection.

        Notes
        -----
        Return a list of intersected pairs. Where the first elem of the pair
        is a path of 'rasters_grid'.
        """
        gdf_grid = rasters_footprints(rasters_grid, common_proj)
        gdf_features = rasters_footprints(rasters, common_proj)

        intersections_db = gpd.sjoin(gdf_features, gdf_grid, how="left")
        intersections_db = intersections_db.groupby(
            ["path_right"], group_keys=True
        ).apply(lambda x: x)

        rasters_tiles = [
            (tile_path, series["path_left"])
            for (tile_path, _), series in intersections_db.iterrows()
        ]
        return rasters_tiles

    @staticmethod
    def intersections(
        vector_db: Path,
        rasters: list[Path],
        target_epsg: int,
        column_name: str,
        tiles: list[str],
    ) -> list[tuple[str, str]]:
        """
        Return the intersection between tiles from a reference shapefile and a list of rasters

        Parameters
        ----------
        vector_db:
            path to the reference database
        rasters:
            list of paths to rasters
        target_epsg:
            target epsg
        column_name:
            column containing tile names
        tiles:
            list of names of tiles

        Notes
        -----
        The output is a list of tuples with tile names and paths of rasters. It includes all raster
        footprints, even those not intersecting with any given tile.
        """
        grid_db = gpd.read_file(vector_db)
        grid_db = grid_db[[column_name, "geometry"]]
        grid_db.to_crs(epsg=target_epsg, inplace=True)
        grid_db = grid_db.loc[grid_db[column_name].isin(tiles)]
        gdf_features = rasters_footprints(rasters, target_epsg)
        intersections_db = gpd.sjoin(gdf_features, grid_db, how="left")
        intersections_db = intersections_db.groupby(
            [str(column_name)], group_keys=True
        ).apply(lambda x: x)

        rasters_tiles = [
            (tile_name, series["path"])
            for (tile_name, _), series in intersections_db.iterrows()
        ]
        return rasters_tiles

    def build_steps(
        self,
        cfg: str,
    ) -> list[StepContainer]:
        """
        build steps
        """

        step_container = StepContainer("tile features")
        step_container.append(
            partial(
                tile_features.FeaturesTiler,
                self.cfg,
                self.config_resources,
                self.intersection_list,
                self.s1_intersections_list,
            ),
            "tiler",
        )
        return [step_container]

    def pre_check(self) -> None:
        """Perform some checks on data provided by the user."""

    def generate_output_directories(
        self, first_step_index: int, restart: bool, builder_index: int = 0
    ) -> None:
        """
        Generate expected output directories, depending on the given state of the chain
        parameters
        """

        task_status_file = str(
            Path(self.output_path) / I2_CONST.i2_tasks_status_filename
        )

        restart = restart and Path(task_status_file).exists()
        self.remove_output_path = (
            self.remove_output_path and Path(self.output_path).exists()
        )

        if self.remove_output_path and first_step_index != 0 or restart:
            pass
        elif self.remove_output_path and builder_index == 0:
            shutil.rmtree(self.output_path)

        if restart:
            pass
        elif (first_step_index == 0 and not restart) or not Path(
            self.output_path
        ).exists():
            tiles = self.i2_cfg_params.get_param("chain", "list_tile").split(" ")
            for tile in tiles:
                tile_dir = Path(self.output_path) / Path(tile)
                Path(tile_dir).mkdir(parents=True, exist_ok=True)
        else:
            pass
