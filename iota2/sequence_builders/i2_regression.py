# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
regression builder inspired by `builder_example.py` and `I2Classification.py`
inherits from `i2_sequence_builder.py/i2_builder` ABC

intput data field is standardized and put in "i2label" column as flot values
a new column is introduced for samples selection purpose and is named "split"

scikit learn RandomForestRegressor and pytorch MLPRegressor are available
but one and only one of these can be set in the config file
"""
import shutil
from functools import partial
from pathlib import Path
from typing import Self

import iota2.common.i2_constants as i2_const
import iota2.vector_tools.vector_functions as vf
from iota2.common.utils import run
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.config_file import ConfigFile, ModelLib, Parameters
from iota2.configuration_files.sections.cfg_utils import ConfigError

# in order to steal it's model_distribution method
# TODO avoid stealing I2Classification methods (see issue #543)
from iota2.sequence_builders.i2_sequence_builder import (
    I2Builder,
    check_databases_consistency,
    check_sensors_data,
    check_tiled_exogenous_data,
)
from iota2.steps import (
    check_inputs_step,
    common_masks,
    envelope,
    gen_region_vector,
    gen_synthetic_samples,
    merge_seed_regressions,
    model_choice,
    mosaic,
    pixel_validity,
    regression,
    samples_by_models,
    samples_by_tiles,
    samples_extraction,
    samples_merge,
    sampling_learning_polygons,
    sensors_preprocess,
    sk_classifications_merge,
    stats_samples_model,
    vector_formatting,
)
from iota2.steps.iota2_step import Step, StepContainer
from iota2.typings.i2_types import AvailSchedulers

# constants for regression
I2_CONST = i2_const.Iota2Constants()


# TODO refactor this function (similar to issue #535)
# it copies ground truth data and add a "split" column in the shapefile
# which will serve for sample selection app
def copy_ref_data_and_add_fake_column(
    ground_truth_path: Path,
    reference_data_path: Path,
    column_name: str,
    data_aug_params: dict,
    data_field: str,
) -> None:
    """creates a fake class for sample selection"""
    run(f"ogr2ogr {reference_data_path} {ground_truth_path}")
    data = vf.open_to_write(str(reference_data_path))
    # WARNING do not chain with previous line → segfault
    layer = data.GetLayer()
    vf.create_field(layer, column_name, vf.ogr.OFTInteger)
    if data_aug_params["activate"]:
        fake_class_array = vf.get_fake_class_vector(
            str(reference_data_path), data_field, data_aug_params
        )
        vf.fill_field_with_array(layer, column_name, fake_class_array)
    else:
        vf.fill_field(layer, column_name, lambda *_: 0)


class I2Regression(I2Builder):  # pylint: disable=C0103
    """
    builder for regression
    """

    def __init__(
        self,
        cfg: str,
        config_ressources: Path,
        scheduler_type: AvailSchedulers,
        restart: bool = False,
        tasks_states_file: str | None = None,
        hpc_working_directory: str = "TMPDIR",
    ):
        """builder constructor
        arguments are the one defined by parent class i2_builder
        """

        super().__init__(
            cfg,
            config_ressources,
            scheduler_type,
            restart,
            tasks_states_file,
            hpc_working_directory,
        )

        # read config file
        self.config = ConfigFile.from_config_path(cfg)
        self.params = Parameters.from_config(self.config)

        # initialize groups without steps
        for group in [
            "init",
            "sampling",
            "training",
            "prediction",
            "mosaic",
            "validation",
        ]:
            self.steps_group[group] = {}

        # fills groups with steps
        self.sort_step()

    def pre_check(self) -> None:
        """
        Check all parameters specific to regression
        """
        params = rcf.Iota2Parameters(self.i2_cfg_params)
        check_sensors_data(
            params,
            self.__class__.__name__,
            self.i2_cfg_params.get_param("chain", "minimum_required_dates"),
            self.i2_cfg_params.get_param(
                "sensors_data_interpolation", "use_gapfilling"
            ),
            self.i2_cfg_params.get_param("arg_train", "features_from_raw_dates"),
        )
        check_databases_consistency(
            self.i2_cfg_params.get_param("chain", "ground_truth"),
            self.i2_cfg_params.get_param("chain", "data_field"),
            self.i2_cfg_params.get_param("chain", "region_path"),
            self.i2_cfg_params.get_param("chain", "region_field"),
        )
        check_tiled_exogenous_data(
            self.i2_cfg_params.get_param("external_features", "exogeneous_data"),
            self.i2_cfg_params.get_param("chain", "list_tile"),
        )
        self.check_config_parameters()
        data_field = self.i2_cfg_params.get_param("chain", "data_field")
        # check data_field format
        normalized = data_field.lower().replace("_", "")
        if data_field != normalized:
            raise ConfigError(
                f"data_field {data_field} must not contain uppercase "
                f"or underscores. Sugested value: {normalized}"
            )

        # in pytorch mode
        if self.params.model_lib == ModelLib.PYTORCH:
            deep_learning_parameters = self.i2_cfg_params.get_param(
                "arg_train", "deep_learning_parameters"
            )
            # checks optimization/selection criterion
            ko_criterion = ["loss", "cross_entropy", "kappa"]
            moc = deep_learning_parameters["model_optimization_criterion"]
            msc = deep_learning_parameters["model_selection_criterion"]
            if moc in ko_criterion:
                raise ConfigError(
                    f"model_optimization_criterion '{moc}' "
                    "not valid in case of regression"
                )
            if msc in ko_criterion:
                raise ConfigError(
                    f"model_selection_criterion '{msc}' "
                    "not valid in case of regression"
                )
            # checks pytorch options not available for regression
            if deep_learning_parameters["enable_early_stop"]:
                raise ConfigError("early stop not available for regression")
            if deep_learning_parameters["adaptive_lr"]:
                raise ConfigError("adaptive learning rate not available for regression")
            if deep_learning_parameters["weighted_labels"]:
                raise ConfigError("weights not available for regression")

    def build_steps(
        self,
        cfg: str,
    ) -> list[StepContainer]:
        """see parent class i2_builder for documentation about parameters"""
        # set Step "spatial information"
        # use class variable instead of object attributes for shared information
        Step.set_models_spatial_information(self.tiles, {})

        # step containers
        sc1 = StepContainer(name="regression_1")
        sc2 = StepContainer(name="regression_2")
        sc3 = StepContainer(name="regression_3")

        def prelaunch(self: Self) -> None:
            """pre-launch method only used in build_steps"""
            copy_ref_data_and_add_fake_column(
                self.i2_cfg_params.get_param("chain", "ground_truth"),
                self.params.reference_data,
                I2_CONST.fake_class,
                self.i2_cfg_params.get_param("arg_train", "sample_augmentation"),
                self.i2_cfg_params.get_param("chain", "data_field"),
            )
            if self.i2_cfg_params.get_param("external_features", "functions"):
                self.test_user_feature_with_fake_data()

        # before preprocessing, add a fake class for sample selection
        sc1.prelaunch_functions = [partial(prelaunch, self)]
        sc3.prelaunch_functions = [partial(self.models_distribution)]
        if self.i2_cfg_params.get_param("external_features", "external_features_flag"):
            sc3.prelaunch_functions.append(partial(self.chunks_intersections))

        # ===== init =====
        if self.i2_cfg_params.get_param("chain", "check_inputs"):
            sc1.append(
                partial(
                    check_inputs_step.CheckInputsClassifWorkflow,
                    self.cfg,
                    self.config_resources,
                    "regression",
                ),
                "init",
            )

        sc1.append(
            partial(
                sensors_preprocess.SensorsPreprocess,
                self.cfg,
                self.config_resources,
            ),
            "init",
        )
        sc2.append(
            partial(
                common_masks.CommonMasks,
                cfg,
                self.config_resources,
                self.working_directory,
            ),
            "init",
        )
        sc2.append(
            partial(
                pixel_validity.PixelValidity,
                cfg,
                self.config_resources,
                self.working_directory,
            ),
            "init",
        )

        # ===== sampling =====
        sc2.append(
            partial(
                envelope.Envelope, cfg, self.config_resources, self.working_directory
            ),
            "sampling",
        )
        sc2.append(
            partial(
                gen_region_vector.GenRegionVector,
                cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )
        sc2.append(
            partial(
                vector_formatting.VectorFormatting,
                cfg,
                self.config_resources,
                self.working_directory,
                data_field=I2_CONST.fake_class,  # used to split samples
                original_label_column=self.i2_cfg_params.get_param(
                    "chain", "data_field"
                ),
                # keep data_field in shapefile
            ),
            "sampling",
        )

        # see sc3.prelaunch_function before

        if (
            vf.get_geometry(self.i2_cfg_params.get_param("chain", "ground_truth"))
            != "POINT"
        ):
            sc3.append(
                partial(
                    samples_merge.SamplesMerge,
                    cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "sampling",
            )
            sc3.append(
                partial(
                    stats_samples_model.StatsSamplesModel,
                    cfg,
                    self.config_resources,
                    self.working_directory,
                    data_field=I2_CONST.fake_class,
                ),
                "sampling",
            )
            sc3.append(
                partial(
                    sampling_learning_polygons.SamplingLearningPolygons,
                    cfg,
                    self.config_resources,
                    self.working_directory,
                    data_field=I2_CONST.fake_class,
                ),
                "sampling",
            )
        sc3.append(
            partial(
                samples_by_tiles.SamplesByTiles,
                cfg,
                self.config_resources,
                self.working_directory,
            ),
            "sampling",
        )
        sc3.append(
            partial(
                samples_extraction.SamplesExtraction,
                cfg,
                self.config_resources,
                self.working_directory,
                data_field=I2_CONST.fake_class,
                mode="regression",
            ),
            "sampling",
        )
        # TODO when the sampling_validation is active
        # this step should also merge the validation sample per model
        sc3.append(
            partial(samples_by_models.SamplesByModels, cfg, self.config_resources),
            "sampling",
        )

        if self.i2_cfg_params.get_param("arg_train", "sample_augmentation")["activate"]:
            sc3.append(
                partial(
                    gen_synthetic_samples.GenSyntheticSamples,
                    cfg,
                    self.config_resources,
                    False,
                    self.working_directory,
                ),
                "sampling",
            )
        # ===== training =====

        # match / case only available in python3.10
        if self.params.model_lib == ModelLib.SKLEARN:
            sc3.append(
                partial(
                    regression.TrainRegressionScikit,
                    cfg,
                    self.config_resources,
                ),
                "training",
            )
        if self.params.model_lib == ModelLib.PYTORCH:
            sc3.append(
                partial(
                    regression.TrainRegressionPytorch,
                    cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "training",
            )

        # ===== prediction =====
        # inference / regression

        # match / case only available in python3.10
        if self.params.model_lib == ModelLib.SKLEARN:
            sc3.append(
                partial(
                    regression.PredictRegressionScikit,
                    cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "prediction",
            )
        if self.params.model_lib == ModelLib.PYTORCH:
            sc3.append(
                partial(
                    model_choice.ModelChoice,
                    cfg,
                    self.config_resources,
                    mode="regression",
                ),
                "prediction",
            )
            sc3.append(
                partial(
                    regression.PredictRegressionPytorch,
                    cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "prediction",
            )

        # in both case, the prediction is done per chunk
        # these chunks must be merged before mosaic
        sc3.append(
            partial(
                sk_classifications_merge.ScikitClassificationsMerge,
                self.cfg,
                self.config_resources,
                mode="regression",
            ),
            "prediction",
        )

        # ===== mosaic =====
        sc3.append(
            partial(
                mosaic.Mosaic,
                cfg,
                self.config_resources,
                self.working_directory,
                mode="regression",
            ),
            "mosaic",
        )

        if (
            self.i2_cfg_params.get_param("multi_run_fusion", "merge_run")
            and self.i2_cfg_params.get_param("arg_train", "runs") > 1
        ):
            sc3.append(
                partial(
                    merge_seed_regressions.MergeSeedRegressions,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "mosaic",
            )

        # ===== validation =====
        sc3.append(
            partial(
                regression.GenerateRegressionMetrics,
                cfg,
                self.config_resources,
            ),
            "validation",
        )
        if len(self.tiles) > 1 or self.i2_cfg_params.get_param("arg_train", "runs") > 1:
            sc3.append(
                partial(
                    regression.GenerateRegressionMetricsSummary,
                    cfg,
                    self.config_resources,
                ),
                "validation",
            )

        if (
            self.i2_cfg_params.get_param("multi_run_fusion", "merge_run")
            and self.i2_cfg_params.get_param("arg_train", "runs") > 1
        ):
            sc3.append(
                partial(
                    merge_seed_regressions.MergeSeedValidation,
                    self.cfg,
                    self.config_resources,
                ),
                "validation",
            )

        # ===== return step containers =====
        return [sc1, sc2, sc3]

    def generate_output_directories(
        self,
        first_step_index: int,
        restart: bool,
        builder_index: int = 0,
    ) -> None:
        """
        Generate expected output directories, depending on the given state of the chain
        parameters
        """
        # search output directory in config
        output_dir = self.i2_cfg_params.get_param("chain", "output_path")

        # only erase if asked and not restarting chain at given step
        do_erase = (
            self.i2_cfg_params.get_param("chain", "remove_output_path")
            and not restart
            and first_step_index == 0
            and Path(output_dir).exists()
            and builder_index == 0
        )

        # recursively remove output files (warning, do not set to user folder)
        if do_erase:
            shutil.rmtree(str(output_dir))

        # TODO should be refactored with 'get_dir' method (see issue #536)
        dirs = [
            "samplesSelection",
            "model",
            "formattingVectors",
            "config_model",
            "envelope",
            "classif",
            "shapeRegion",
            "final",
            "final/TMP",
            "features",
            "dataRegion",
            "learningSamples",
            "dataAppVal",
            "stats",
            "logs",
            *[
                f"features/{tile}"
                for tile in self.i2_cfg_params.get_param("chain", "list_tile").split(
                    " "
                )
            ],
            *[
                f"features/{tile}/tmp"
                for tile in self.i2_cfg_params.get_param("chain", "list_tile").split(
                    " "
                )
            ],
        ]

        if self.i2_cfg_params.get_param("multi_run_fusion", "merge_run"):
            dirs.append("final/merge_final_classifications")

        for d in dirs:  # pylint: disable=C0103
            Path(output_dir, d).mkdir(parents=True, exist_ok=True)
