#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Parent class for builders
"""

import logging
import os
import pickle
import shutil
import string
import time
import zipfile
from abc import ABC, abstractmethod
from collections import Counter
from collections.abc import Callable
from dataclasses import dataclass
from functools import partial, reduce
from itertools import zip_longest
from pathlib import Path
from typing import Self, TypeVar

from dask.base import collections_to_dsk
from dask.delayed import Delayed
from dask.dot import graphviz_to_file, to_graphviz
from osgeo import ogr
from shapely.geometry import Polygon

from iota2.typings.i2_types import AvailSchedulers, PathLike

try:
    import zlib  # noqa: F401, pylint: disable=unused-import

    COMPRESSION = zipfile.ZIP_DEFLATED
except ModuleNotFoundError:
    COMPRESSION = zipfile.ZIP_STORED

from iota2.common.compression_options import compression_options
from iota2.common.custom_numpy_features import (
    CustomFeaturesConfiguration,
    CustomNumpyFeatures,
    exogenous_data_tile,
)
from iota2.common.debug_utils import gen_html_logging_pages
from iota2.common.file_utils import file_search_and, sort_by_first_elem
from iota2.common.i2_constants import NodeColors
from iota2.common.raster_utils import ChunkConfig, get_raster_chunks_boundaries_carto
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.sections.cfg_utils import ConfigError
from iota2.steps.i2_job_helpers import JobHelper
from iota2.steps.iota2_step import Step, StepContainer
from iota2.validation.results_utils import get_color_table, get_nomenclature
from iota2.vector_tools.vector_functions import (
    get_field_element,
    get_field_type,
    get_fields,
    nb_geom_intersections,
)

# pylint: disable = too-many-instance-attributes
LOGGER = logging.getLogger("distributed.worker")
# PEP 673, python 3.11 introduce 'Self' typing
TI2Builder = TypeVar("TI2Builder", bound="I2Builder")  # pylint: disable=invalid-name


@dataclass
class WaitingI2Graph:
    """
    data class containing dask's graph ready to be built.
    """

    def __init__(
        self,
        builder: TI2Builder,
        step_container: StepContainer,
        starting_step: int,
        ending_step: int,
        output_graph: PathLike,
        prelaunch_functions: list[Callable] | None = None,
        figure_suffix: str = "",
    ):
        self.container = step_container
        self.starting_step = starting_step
        self.ending_step = ending_step
        self.prelaunch_functions = prelaunch_functions
        self.builder = builder
        self.output_graph = output_graph
        self.figure_graph = None
        if figure_suffix:
            self.figure_suffix = figure_suffix
        else:
            self.figure_suffix = builder.__class__.__name__


def build_graph(waiting_graph: WaitingI2Graph) -> Delayed:
    """
    Build a dask graph corresponding to the required tasks and builder(s)
    """
    if waiting_graph.prelaunch_functions:
        for prelaunch_function in waiting_graph.prelaunch_functions:
            prelaunch_function()

    steps_list = waiting_graph.container[
        waiting_graph.starting_step : waiting_graph.ending_step + 1
    ]
    assert isinstance(steps_list, list)
    for step in steps_list:
        assert isinstance(step, partial)
        step()

    figure_graph = Step.get_figure_graph()
    waiting_graph.builder.figure_graph = figure_graph
    waiting_graph.figure_graph = figure_graph
    waiting_graph.builder.dask_figure_graphics.append(
        (figure_graph, waiting_graph.figure_suffix)
    )
    if waiting_graph.output_graph:
        waiting_graph.builder.figure_graph.visualize(
            filename=waiting_graph.output_graph,
            optimize_graph=True,
            collapse_outputs=True,
        )
        print(f"graph generated at : {waiting_graph.output_graph}")
    return Step.get_exec_graph()


def zip_iota2_logging_dir(i2_root_dir: str, out_zip: str) -> None:
    """prepare i2 archive containing logs"""
    i2_log_dir = str(Path(i2_root_dir) / "logs")

    # get logs
    log_err_files = filter(
        lambda x: "html" not in x, file_search_and(i2_log_dir, True, ".err")
    )
    log_out_files = filter(
        lambda x: "html" not in x, file_search_and(i2_log_dir, True, ".out")
    )
    status_figures = file_search_and(i2_log_dir, True, "tasks_status")

    with zipfile.ZipFile(out_zip, "w") as my_zip:
        for file_n in log_err_files:
            my_zip.write(
                file_n, file_n.replace(i2_root_dir, ""), compress_type=COMPRESSION
            )
        for file_n in log_out_files:
            my_zip.write(
                file_n, file_n.replace(i2_root_dir, ""), compress_type=COMPRESSION
            )
        if status_figures:
            for status_figure in status_figures:
                my_zip.write(
                    status_figure,
                    status_figure.replace(i2_root_dir, ""),
                    compress_type=COMPRESSION,
                )


def check_build_steps_returns_type(
    steps_containers: StepContainer | list[StepContainer],
) -> None:
    """raise an exception if the returned value of self.build_steps()
    is incorrect
    """
    if isinstance(steps_containers, list):
        for steps_container in steps_containers:
            if not isinstance(steps_container, StepContainer):
                raise TypeError(
                    "the list returned from build_steps() method "
                    "must contains only StepContainer class instances"
                )
    elif not isinstance(steps_containers, StepContainer):
        raise TypeError(
            "the returned value from build_steps() method must be StepContainer"
        )


def update_graph_status(dask_graph: Delayed) -> None:
    """call once an error occurs on a previous dask graph,
    this function set every tasks in 'dask_graph' to
    the 'unlaunchable' status
    """
    tasks_key = dask_graph.dask.keys()
    tasks_name = [task_key.split("-")[0] for task_key in tasks_key]

    if Step.tasks_status_file is not None and Path(Step.tasks_status_file).exists():
        with open(Step.tasks_status_file, "rb") as tasks_status_file:
            tasks_status = pickle.load(tasks_status_file)
        tasks_status_updated = tasks_status.copy()
        for task_name in tasks_name:
            tasks_status_updated[task_name] = "unlaunchable"
        with open(Step.tasks_status_file, "wb") as tasks_status_file:
            pickle.dump(tasks_status_updated, tasks_status_file)


class I2Builder(ABC):
    """
    class use to describe steps sequence and variable to use at each
    step (config)
    """

    _instances: dict[str, Self] = {}

    def __init__(
        self,
        cfg: str,
        config_resources: Path | None,
        scheduler_type: AvailSchedulers,
        restart: bool = False,
        tasks_states_file: str | None = None,
        hpc_working_directory: str | None = "TMPDIR",
    ):
        self.config_resources = None
        if config_resources:
            self.config_resources = Path(config_resources)
        self.cfg = cfg
        self.working_directory = None
        self.i2_cfg_params = rcf.ReadConfigFile(self.cfg)
        if hpc_working_directory:
            self.working_directory = os.getenv(hpc_working_directory)
        self.output_i2_directory = self.i2_cfg_params.get_param("chain", "output_path")
        self.log_dir = str(Path(self.output_i2_directory) / "logs")

        max_ram_by_tasks = self.i2_cfg_params.get_param(
            "task_retry_limits", "maximum_ram"
        )
        max_cpu_by_tasks = self.i2_cfg_params.get_param(
            "task_retry_limits", "maximum_cpu"
        )
        max_task_retry = self.i2_cfg_params.get_param(
            "task_retry_limits", "allowed_retry"
        )

        i2_tiles = self.i2_cfg_params.get_param("chain", "list_tile")
        self.tiles: list[str] = []
        if i2_tiles:
            self.tiles = i2_tiles.split(" ")

        desc_type = dict[str, str]
        step_index_desc = dict[int, desc_type]
        self.steps_group: dict[str, step_index_desc] = {}

        self.figure_graph: Delayed = None
        Step.set_tasks_status_directory(self.output_i2_directory)
        if restart and not tasks_states_file:
            Step.restart_from_i2_state(Step.tasks_status_file)
        if tasks_states_file:
            Step.restart_from_i2_state(tasks_states_file)

        Step.set_allowed_task_failure(max_task_retry)
        Step.set_hardware_limits(max_cpu_by_tasks, max_ram_by_tasks)

        Step.execution_mode = scheduler_type

        self.dask_figure_graphics: list[tuple[Delayed, str]] = []

        # mypy doesn't recognize the right class
        self._instances[self.__class__.__name__] = self  # type: ignore

    @property
    def builders_instances(self) -> dict[str, Self]:
        """Return instance"""
        return self._instances

    @property
    def steps(self) -> list[StepContainer]:
        """
        Return step container
        """
        i2_running_steps_container = self.build_steps(self.cfg)
        check_build_steps_returns_type(i2_running_steps_container)
        return i2_running_steps_container

    def sort_step(self) -> None:
        """
        use to establish which step is going to which step group
        """
        for step_place, step in enumerate(
            [step for container in self.steps for step in container]
        ):
            # type: ignore
            self.steps_group[step.step_group][step_place + 1] = {
                "description": step.func.step_description(),
                "resources_block": step.func.resources_block_name,
                "log_id": step.func.__name__,
            }

    def gen_figure_tasks_attributes(
        self, tasks_keys: list[str], i2_tasks_status_dico: dict[str, str], rst_dir: str
    ) -> dict:
        """generate attributes in order to colorize a dask graph, one color by task's state

        Parameters
        ----------
        tasks_keys
            dask task's key
        i2_tasks_status_dico:
            dictionary build as : {'task_name': 'tasks_status'}
        rst_dir:
            directory containing all rst files
        """
        # waiting every log coming from scheduler
        time.sleep(10)
        failed_tasks_attributes = {
            "color": "black",
            "fontcolor": "black",
            "fillcolor": NodeColors.fail_node_color,
            "style": "filled",
        }
        succeeded_tasks_attributes = {
            "color": "black",
            "fontcolor": "black",
            "fillcolor": NodeColors.done_node_color,
            "style": "filled",
        }
        unlaunchable_tasks_attributes = {
            "color": "black",
            "fontcolor": "black",
            "fillcolor": NodeColors.unlaunchable_node_color,
            "style": "filled",
        }
        attributes = {}
        for task_name, task_status in i2_tasks_status_dico.items():
            # get the dask key corresponding to the the task_name
            for key in tasks_keys:
                if task_name == key.split("-")[0]:
                    if task_status == "done":
                        attributes[key] = succeeded_tasks_attributes.copy()
                        url = self.__get_task_log(
                            task_name, std_flux="out", rst_dir=rst_dir
                        )
                    elif task_status == "failed":
                        attributes[key] = failed_tasks_attributes.copy()
                        url = self.__get_task_log(
                            task_name, std_flux="err", rst_dir=rst_dir
                        )
                    else:
                        attributes[key] = unlaunchable_tasks_attributes.copy()
                        url = ""
                    if url:
                        attributes[key]["URL"] = url
                    break
        # get last key
        for key in tasks_keys:
            if "ending" in key:
                attributes[key] = unlaunchable_tasks_attributes.copy()
        return attributes

    def __get_task_log(self, task_name: str, std_flux: str, rst_dir: str) -> str:
        """from task's name, return it's associated log file

        Parameters
        ----------
        task_name:
            task's name
        std_flux :
            define which log file is returned : 'err' or 'out'
        rst_dir:
            directory containing all rst files
        """
        log_file = ""
        if std_flux:
            log_files: list[str] = list(
                filter(
                    lambda x: std_flux in Path(x).suffix,
                    file_search_and(
                        str(Path(self.output_i2_directory) / "logs"),
                        True,
                        task_name + ".",
                    ),
                )
            )
            if log_files:
                log_file = log_files[0]
                file_n = Path(log_file).name
                if not (Path(rst_dir) / file_n).exists():
                    shutil.copy(log_file, rst_dir)
                log_file = str(Path("..") / "html" / "source" / file_n)
            else:
                print(f"WARNING : cannot find log for task {task_name}")
        return log_file

    def tasks_summary(self) -> None:
        """generate a colorized a dask graph according to task's state"""
        html_dir = str(Path(self.output_i2_directory) / "logs" / "html")
        rst_dir = str(Path(self.output_i2_directory) / "logs" / "html" / "source")
        if Path(html_dir).exists():
            shutil.rmtree(html_dir)
        Path(html_dir).mkdir(parents=True, exist_ok=True)
        if Path(rst_dir).exists():
            shutil.rmtree(rst_dir)
        Path(rst_dir).mkdir(parents=True, exist_ok=True)
        relative_tasks_fig_files = []
        for graph_num, (dask_figure_graphic, suffix_name) in enumerate(
            self.dask_figure_graphics
        ):
            tasks_figure_file = str(
                Path(
                    self.output_i2_directory,
                    "logs",
                    f"tasks_status_{suffix_name}_{graph_num + 1}.svg",
                )
            )
            if Path(tasks_figure_file).exists():
                Path(tasks_figure_file).unlink()

            i2_tasks_status_file = Step.tasks_status_file
            assert i2_tasks_status_file is not None
            with open(i2_tasks_status_file, "rb") as tasks_status_file:
                i2_tasks_status_dico = pickle.load(tasks_status_file)

            tasks_keys = dask_figure_graphic.dask.keys()
            tasks_attributes = self.gen_figure_tasks_attributes(
                tasks_keys, i2_tasks_status_dico, rst_dir
            )

            dsk = dict(collections_to_dsk([dask_figure_graphic], optimize_graph=True))
            graph = to_graphviz(
                dsk, collapse_outputs=True, data_attributes=tasks_attributes
            )

            with graph.subgraph() as sub_graph:
                sub_graph.attr(rank="same")
                sub_graph.node(
                    "DONE",
                    fillcolor=NodeColors.done_node_color,
                    style="filled",
                    shape="circle",
                )
                sub_graph.node(
                    "UNLAUNCH",
                    fillcolor=NodeColors.unlaunchable_node_color,
                    style="filled",
                    shape="circle",
                )
                sub_graph.node(
                    "FAIL",
                    fillcolor=NodeColors.fail_node_color,
                    style="filled",
                    shape="circle",
                    URL="https://framagit.org/iota2-project/iota2/-/issues",
                )
            graphviz_to_file(graph, tasks_figure_file, None)
            relative_tasks_fig_files.append(
                str(Path("..", "..") / Path(tasks_figure_file).name)
            )
        gen_html_logging_pages(
            self.cfg,
            html_dir,
            rst_dir,
            relative_tasks_fig_files,
            self.output_i2_directory,
        )
        out_zip = str(Path(self.output_i2_directory) / "logs.zip")
        zip_iota2_logging_dir(self.output_i2_directory, out_zip)
        print(f"logs zip file available at {out_zip}")

    def print_step_summarize(
        self,
        start: int,
        end: int,
        show_resources: bool = False,
        running_step: bool = False,
        step_num_offset: int = 0,
    ) -> str:
        """
        print iota2 steps that will be run
        """
        step_position = 0
        summarize = ""
        for group in list(self.steps_group.keys()):
            if len(self.steps_group[group]) > 0:
                summarize += f"Group {group}:\n"

            for key in self.steps_group[group]:
                highlight = "[ ]"
                if start <= key <= end:
                    highlight = "[x]"
                if key == end and running_step:
                    highlight = "[r]"
                summarize += (
                    f"\t {highlight} Step {key + step_num_offset}: "
                    f"{self.steps_group[group][key]['description']}"
                )
                if show_resources:
                    assert self.config_resources is not None
                    block_name = self.steps_group[group][key]["resources_block"]
                    resources = JobHelper.parse_resource_file(
                        block_name, Path(self.config_resources)
                    )
                    cpu = resources.nb_cpu
                    ram = resources.ram
                    walltime = resources.walltime
                    resource_block_name = resources.resource_block_name
                    resource_block_found = resources.resource_block_found
                    log_identifier = self.steps_group[group][key]["log_id"]
                    resource_miss = "" if resource_block_found else " -> MISSING"
                    summarize += (
                        f"\n\t\t\tresources block name : {resource_block_name}"
                        f"{resource_miss}\n\t\t\tcpu : {cpu}\n\t\t\tram : {ram}\n"
                        f"\t\t\twalltime : {walltime}\n"
                        f"\t\t\tlog identifier : {log_identifier}"
                    )
                summarize += "\n"
                step_position += 1
        return summarize

    def get_steps_number(self) -> list[int]:
        """
        Return list of ordered steps numbers
        """
        start = self.i2_cfg_params.get_param("chain", "first_step")
        end = self.i2_cfg_params.get_param("chain", "last_step")
        start_ind = list(self.steps_group.keys()).index(start)
        end_ind = list(self.steps_group.keys()).index(end)

        steps = []
        for key in list(self.steps_group.keys())[start_ind : end_ind + 1]:
            steps.append(self.steps_group[key])
        step_to_compute = [step for step_group in steps for step in step_group]
        return step_to_compute

    def get_steps_sequence_size(self) -> int:
        """
        Return total number of steps
        """
        number_of_steps = 0
        for step_sequence_description in self.steps_group.values():
            if step_sequence_description:
                number_of_steps += len(step_sequence_description)
        return number_of_steps

    def __get_indexes_by_container(
        self, first_step_index: int, last_step_index: int
    ) -> list[tuple[()] | tuple[int, int]]:
        """the purpose of this function is to get for each container of steps,
        indexes of steps to process according to user demand.

        print([len(steps_container) for steps_container in self.build_steps()])
        >>> [3, 1]
        print(self.get_indexes_by_container(0, 0))
        >>> [(0, 0), []]
        print(self.get_indexes_by_container(0, 4))
        >>> [(0, 2), (0, 0)
        """

        dico: dict[int, dict] = {}
        len_container_buff = 0
        max_step_index = 0
        for container_num, container in enumerate(self.steps):
            dico[container_num] = {}
            for cpt in range(len(container)):
                indice = cpt + len_container_buff
                dico[container_num][indice] = cpt
                max_step_index += 1
            len_container_buff += len(container)

        last_step_index = min(last_step_index, max_step_index - 1)

        list_ind: list[tuple[()] | tuple[int, int]] = []
        for container_num, container in enumerate(self.steps):
            if (
                first_step_index in dico[container_num]
                and last_step_index not in dico[container_num]
            ):
                list_ind.append(
                    (
                        dico[container_num][first_step_index],
                        dico[container_num][next(reversed(dico[container_num]))],
                    )
                )
            elif (
                last_step_index in dico[container_num]
                and first_step_index not in dico[container_num]
            ):
                list_ind.append((0, dico[container_num][last_step_index]))
            elif (
                last_step_index in dico[container_num]
                and first_step_index in dico[container_num]
            ):
                list_ind.append(
                    (
                        dico[container_num][first_step_index],
                        dico[container_num][last_step_index],
                    )
                )
            elif (
                last_step_index not in dico[container_num]
                and first_step_index not in dico[container_num]
            ):
                first_user_index = next(iter(dico[container_num]))
                last_user_index = next(reversed(dico[container_num]))

                if (
                    first_user_index > first_step_index
                    and last_user_index < last_step_index
                ):
                    first_container_index = dico[container_num][first_user_index]
                    last_container_index = dico[container_num][last_user_index]
                    list_ind.append((first_container_index, last_container_index))
                else:
                    list_ind.append(())
        return list_ind

    def get_final_i2_exec_graph(
        self,
        first_step_index: int,
        last_step_index: int,
        output_figures: list[str] | None = None,
    ) -> list[WaitingI2Graph]:
        """get every processing graph ready to be build.

        first_step_index:
            index of the first step to launch
        last_step_index:
            index of the last step to launch
        output_figures:
            list of file to draw processing graph
        """
        if output_figures is None:
            output_figures = []
        # instanciate steps which must me launched
        i2_graphs = []
        indexes = self.__get_indexes_by_container(first_step_index, last_step_index)
        # if user asked too much figures, limit by the number of steps container
        if output_figures:
            output_figures = output_figures[0 : len(self.steps)]

        output_figures_files = []
        if output_figures:
            for cpt, (figure_file, _) in enumerate(
                zip_longest(output_figures, range(len(self.steps)))
            ):
                if figure_file is None:
                    fig_dir = Path(output_figures[0]).parent
                    base_name = Path(output_figures[0]).stem
                    ext = Path(output_figures[0]).suffix
                    file_name = f"{base_name}_{cpt + 1}{ext}"
                    output_figures_files.append(str(fig_dir / file_name))
                else:
                    output_figures_files.append(figure_file)
        for container, tuple_index, out_figure in zip_longest(
            self.steps, indexes, output_figures_files
        ):
            if tuple_index:
                container_start_ind, container_end_ind = tuple_index
                i2_graphs.append(
                    WaitingI2Graph(
                        self,
                        container,
                        container_start_ind,
                        container_end_ind,
                        out_figure,
                        container.prelaunch_functions,
                    )
                )
        return i2_graphs

    def preliminary_informations(self, file_content: str) -> None:
        """save informations in a txt file"""
        Path(self.log_dir).mkdir(parents=True, exist_ok=True)

        background_informations_file = str(Path(self.log_dir) / "run_informations.txt")

        with open(background_informations_file, "w", encoding="utf-8") as info_file:
            info_file.write(file_content)

    def check_externalfeatures_consistency(self) -> None:
        """
        Check external features consistency.

        - check if interpolation parameters are coherent with user request
        """
        use_interpolation = self.i2_cfg_params.get_param(
            "sensors_data_interpolation", "use_gapfilling"
        )
        external_features_flag = self.i2_cfg_params.get_param(
            "external_features", "external_features_flag"
        )
        data_mode_access = self.i2_cfg_params.get_param(
            "python_data_managing", "data_mode_access"
        )
        if (
            not use_interpolation
            and external_features_flag
            and data_mode_access != (False, True)
        ):
            raise ConfigError(
                "inconsistent parameters : use_gapfilling : False "
                "and data_mode_access 'gapfilled' or 'both'"
            )

    def pre_check(self) -> None:
        """function triggered before the launching iota2's steps
           in order to do some checks.

        Notes
        -----
        the purpose of this function is to be define in subclass,
        but it is not mandatory. In case of failure, exceptions
        must be raise
        """

    def check_config_parameters(self) -> None:
        """Check parameters consistency across the whole iota2's cfg file."""

        if (
            self.i2_cfg_params.get_param("multi_run_fusion", "merge_run")
            and self.i2_cfg_params.get_param("arg_train", "runs") == 1
        ):
            raise ConfigError(
                "these parameters are incompatible runs:1 and merge_run:True"
            )

        if self.i2_cfg_params.get_param("arg_train", "sample_augmentation")[
            "activate"
        ] and (
            not self.i2_cfg_params.get_param(
                "sensors_data_interpolation", "use_gapfilling"
            )
        ):
            raise ConfigError(
                "these parameters are incompatible samples_augmentation:activate and"
                " use_gapfilling:False"
            )

        if self.i2_cfg_params.get_param(
            "arg_train", "learning_samples_extension"
        ) == "csv" and (
            self.i2_cfg_params.get_param("arg_train", "classifier") is not None
        ):
            raise ConfigError(
                "It is not possible to use a CSV file as a learning samples "
                "file with an OTB classifier."
            )

    @abstractmethod
    def generate_output_directories(self, first_step_index: int, restart: bool) -> None:
        """generate needed output directories for a dedicated builder"""

    @abstractmethod
    def build_steps(self, cfg: str) -> list[StepContainer]:
        """
        Placeholder method to be replaced by actual builders to build the required steps.
        """

    def test_user_feature_with_fake_data(self) -> None:
        """test user feature before launching chain to catch error early
        this method is available to i2_builder child classes and should be used
        where custom features are used"""

        # get parameters
        param = rcf.Iota2Parameters(self.i2_cfg_params)
        tile_name = self.tiles[0]  # config.getParam('chain', 'list_tile')
        sensors_param = param.get_sensors_parameters(tile_name)
        module_name = self.i2_cfg_params.get_param("external_features", "module")
        list_functions = self.i2_cfg_params.get_param("external_features", "functions")
        # optional parameters
        concat_mode = self.i2_cfg_params.get_param("external_features", "concat_mode")
        enabled_gap, enabled_raw = self.i2_cfg_params.get_param(
            "python_data_managing", "data_mode_access"
        )
        fill_missing_dates = self.i2_cfg_params.get_param(
            "python_data_managing", "fill_missing_dates"
        )
        all_dates_dict = param.get_available_sensors_dates()
        if "Sentinel1" in all_dates_dict:
            s1_dates = param.get_sentinel1_input_dates()
            all_dates_dict = {**all_dates_dict, **s1_dates}
            all_dates_dict.pop("Sentinel1", None)

        exogenous_data = exogenous_data_tile(
            self.i2_cfg_params.get_param("external_features", "exogeneous_data"),
            tile_name,
        )
        epsg = self.i2_cfg_params.get_param("chain", "proj").split(":")[-1]

        # instantiantes custom_numpy_features data container
        data = CustomNumpyFeatures(
            sensors_params=sensors_param,
            module_name=module_name,
            list_functions=list_functions,
            configuration=CustomFeaturesConfiguration(
                concat_mode=concat_mode,
                enabled_raw=enabled_raw,
                enabled_gap=enabled_gap,
                fill_missing_dates=fill_missing_dates,
            ),
            all_dates_dict=all_dates_dict,
            exogeneous_data_file=exogenous_data,
        )
        # tests feature with fake data
        LOGGER.info("testing custom feature with fake data")
        # print("testing custom feature with fake data")

        if error := data.test_user_feature_with_fake_data(int(epsg)):
            LOGGER.error("error caught while calling external feature on fake data")
            raise error

    def set_compression_options(self) -> None:
        """
        Sets compression options
        """
        compression_options.algorithm = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "compression_algorithm"
        )
        compression_options.predictor = rcf.ReadConfigFile(self.cfg).get_param(
            "chain", "compression_predictor"
        )

    def chunks_intersections(self, by_runs: bool = False) -> None:
        """Check if there is an intersection with at least one learning polygon.

        list of True/False for sorted chunks for all runs or by runs

        ie:
        if by_runs = True
        return = {"T31TCJ":
                  {
                   0: [True, False, True, True, True, True, False, True, True],
                   1: [True, False, True, True, True, True, False, True, True],
                   2: [False, False, True, True, True, True, False, True, True],
                  }
                 }
        if by_runs = False
        return = {"T31TCJ":
                  [False, False, True, True, True, True, False, True, True]
                 }
        """

        def chunks_intersections_red(
            list1: list[bool], list2: list[bool]
        ) -> list[bool]:
            """Perform AND operation for all pairs of elements in two lists"""
            return [
                (intersect_1 and intersect_2)
                for intersect_1, intersect_2 in zip(list1, list2)
            ]

        intersections_dict: dict[str, dict[int, list[bool]]] = {}
        intersections_dict_all_runs = {}

        chunk_config = ChunkConfig(
            chunk_size_mode=self.i2_cfg_params.get_param(
                "python_data_managing", "chunk_size_mode"
            ),
            number_of_chunks=self.i2_cfg_params.get_param(
                "python_data_managing", "number_of_chunks"
            ),
            chunk_size_x=self.i2_cfg_params.get_param(
                "python_data_managing", "chunk_size_x"
            ),
            chunk_size_y=self.i2_cfg_params.get_param(
                "python_data_managing", "chunk_size_y"
            ),
            padding_size_x=self.i2_cfg_params.get_param(
                "python_data_managing", "padding_size_x"
            ),
            padding_size_y=self.i2_cfg_params.get_param(
                "python_data_managing", "padding_size_y"
            ),
        )

        for tile in self.tiles:
            ref_img = (
                Path(self.output_i2_directory)
                / "features"
                / tile
                / "tmp"
                / "MaskCommunSL.tif"
            )
            intersections_dict[tile] = {}
            runs = self.i2_cfg_params.get_param("arg_train", "runs")
            for seed in range(runs):
                learning_polygons = (
                    Path(self.output_i2_directory)
                    / "dataAppVal"
                    / f"{tile}_seed_{seed}_learn.sqlite"
                )
                intersections_dict[tile][seed] = []
                boundaries = get_raster_chunks_boundaries_carto(ref_img, chunk_config)
                if not learning_polygons.exists():
                    intersections_dict[tile][seed] = [False] * len(boundaries)
                else:
                    for boundary in boundaries:
                        llx = boundary["lowerleft_x"]
                        lly = boundary["lowerleft_y"]
                        urx = boundary["upperright_x"]
                        ury = boundary["upperright_y"]

                        geom_wkt = ogr.CreateGeometryFromWkt(
                            Polygon(
                                [
                                    [llx, lly],
                                    [llx, ury],
                                    [urx, ury],
                                    [urx, lly],
                                    [llx, lly],
                                ]
                            ).wkt
                        )
                        intersections_dict[tile][seed].append(
                            nb_geom_intersections(geom_wkt, learning_polygons) > 0
                        )
            intersections_dict_all_runs[tile] = list(
                reduce(
                    lambda x, y: chunks_intersections_red(list(x), list(y)),
                    intersections_dict[tile].values(),
                )
            )
        if by_runs:
            Step.set_chunks_intersections(intersections_dict)
        else:
            Step.set_chunks_intersections(intersections_dict_all_runs)

    def models_distribution(self) -> None:
        """function to determine which models intersect which tiles
        by using intermediate iota2 results
        """
        regions_distrib, tiles = self.get_tile_region_mapping()
        Step.set_models_spatial_information(tiles, regions_distrib)

        # manage tiles to classify
        shape_region_dir = str(Path(self.output_i2_directory) / "shapeRegion")
        region_tiles_files = file_search_and(shape_region_dir, False, ".shp")
        regions_to_classify = []
        for region_tile in region_tiles_files:
            region = region_tile.split("_")[-2]
            tile = region_tile.split("_")[-1]
            regions_to_classify.append((region, tile))
        regions_to_classify = sort_by_first_elem(regions_to_classify)
        dico_regions_to_classify = {}
        for region, tiles in regions_to_classify:
            dico_regions_to_classify[region] = {"tiles": tiles}

        regions_classif_distrib = {}

        for region_name, dico_tiles in regions_distrib.items():
            regions_classif_distrib[region_name] = {
                "tiles": dico_regions_to_classify[region_name.split("f")[0]]["tiles"]
            }
            if "nb_sub_model" in dico_tiles:
                regions_classif_distrib[region_name]["nb_sub_model"] = dico_tiles[
                    "nb_sub_model"
                ]

        regions_classif_distrib_no_split = {}
        for (
            region,
            region_meta,
        ) in Step.spatial_models_distribution_no_sub_splits.copy().items():
            regions_classif_distrib_no_split[region] = {
                "nb_sub_model": region_meta["nb_sub_model"],
                "tiles": dico_regions_to_classify[region]["tiles"].copy(),
            }
        Step.spatial_models_distribution_classify = regions_classif_distrib
        Step.spatial_models_distribution_no_sub_splits_classify = (
            regions_classif_distrib_no_split
        )

    def get_tile_region_mapping(
        self,
    ) -> tuple[dict[str, dict[str, list[str]]], list[str]]:
        """
        Retrieve the mapping between tiles and regions, and constructs the distribution of regions
        with their corresponding tiles.

        Returns
        -------
        tuple[dict, list]
            A tuple containing:
            - A dictionary where keys are region names and values are dictionaries
              with a key "tiles" associated with a list of tile names.
            - A list of unique tile names.

        Raises
        ------
        ValueError
            If no tiles are found.
        """
        formatting_dir = str(Path(self.output_i2_directory) / "formattingVectors")
        formatting_files = file_search_and(formatting_dir, True, ".shp")
        tiles = []
        regions_tiles = []
        region_field = self.i2_cfg_params.get_param("chain", "region_field")
        for formatting_file in formatting_files:
            tile_name = Path(formatting_file).stem
            regions_tile = get_field_element(
                formatting_file, elem_type=str, field=region_field, mode="unique"
            )
            if tile_name not in tiles:
                tiles.append(tile_name)
            assert regions_tile is not None
            for region in regions_tile:
                regions_tiles.append((region, tile_name))
        regions = sort_by_first_elem(regions_tiles)
        regions_distrib = {}
        for region_name, region_tiles in dict(regions).items():
            regions_distrib[region_name] = {"tiles": region_tiles}
        if not tiles:
            raise ValueError(
                "The chain stopped prematurely. Please check logs in"
                f"'{self.log_dir}'"
            )
        return regions_distrib, tiles


def check_tiled_exogenous_data(exogenous_data: str, list_tile: str) -> None:
    """If multiple tiles are used with exogenous data, make sure the name contains $TILE."""
    if exogenous_data and " " in list_tile and "$TILE" not in exogenous_data:
        raise ValueError(
            "when using multiple tiles, exogenous data must be split into one raster per tile "
            "this can be done with split_raster_into_tiles.py script "
            "the exogenous_data field must then contain $TILE in its name wich will be "
            "interpolated to tile name see "
            "I2FeaturesMap_builder.html#i2-features-map-external-features-exogeneous-data "
            "in documentation"
        )


def check_databases_consistency(
    reference_data: str,
    label_field: str,
    region_file: str | None = None,
    region_field: str | None = None,
) -> None:
    """Check if the databases provided is usable."""
    fields = get_fields(reference_data, driver="ESRI Shapefile")
    if label_field not in fields:
        raise ConfigError(
            f"the field '{label_field}' does " f"not exists in '{reference_data}'"
        )

    if region_file:
        assert isinstance(region_field, str)
        check_database_name(region_file)
        check_regions_values(region_file, region_field)


def check_regions_values(region_file: str, region_field: str) -> None:
    """Check every region values."""
    # check if the field exists
    fields = get_fields(region_file, driver="ESRI Shapefile")
    if region_field not in fields:
        raise ConfigError(
            f"the field '{region_field}' does " f"not exists in '{region_file}'"
        )

    # check region's type
    region_type = get_field_type(region_file, region_field, "ESRI Shapefile")

    if not region_type == str:
        raise ConfigError(
            f"the field's type of '{region_field}' "
            f"in '{region_file}' must be String."
        )

    # check if every values are decimal
    region_values = get_field_element(region_file, str, region_field, "unique")
    non_decimal_values = []
    assert region_values is not None
    for region_value in region_values:
        if not region_value.isdecimal():
            non_decimal_values.append(region_value)
    if non_decimal_values:
        raise ConfigError(
            f"non-decimal values detected in the region file '{region_file}' "
            f"in '{region_field}' : '{', '.join(non_decimal_values)}'"
        )


def check_database_name(input_vector: str) -> None:
    """Check if the first character is an ascii letters."""
    avail_characters = string.ascii_letters
    if (first_character := Path(input_vector).name[0]) not in avail_characters:
        raise ConfigError(
            f"the file '{input_vector}' is containing a non-ascii letter "
            f"at first position in it's name : {first_character}"
        )


def check_input_text_files(
    input_vector_db_file: str, label_field: str, nomenclature_file: str, color_file: str
) -> None:
    """Check nomenclature and color files consistency."""
    nomenclature = get_nomenclature(nomenclature_file)
    colors = get_color_table(color_file)
    if len(nomenclature.keys()) != len(colors.keys()):
        raise ConfigError(
            "inconsistency, "
            f"color file {color_file} as {len(colors.keys())} entries "
            f" while the nomenclature file {nomenclature_file} as "
            f"{len(nomenclature.keys())} entries"
        )

    missing_labels = []
    for class_label in colors:
        if class_label not in nomenclature:
            missing_labels.append(class_label)
    if missing_labels:
        raise ConfigError(
            "Iota2 detected an inconsistency : "
            f" these class '{', '.join(missing_labels)}'"
            " are in the color table file, but does not"
            "exists in the nomenclature file"
        )

    labels = get_field_element(
        input_vector_db_file, elem_type=str, field=label_field, mode="unique"
    )
    missing_labels = []
    assert labels is not None
    for label in labels:
        if label not in nomenclature:
            missing_labels.append(label)
    if missing_labels:
        raise ConfigError(
            "Iota2 detected an inconsistency : "
            f" these class '{', '.join(missing_labels)}'"
            " are in the input database, but does not"
            "exists in the nomenclature file and / or in the "
            "color table file"
        )


def check_sensors_data(
    i2_params: rcf.Iota2Parameters,
    current_builder: str,
    minimum_required_dates: int,
    temporal_interp: bool,
    features_from_raw_dates: bool,
) -> None:
    """Check raster data consistency."""

    def unique_date_required(
        current_builder: str, temporal_interp: bool, features_from_raw_dates: bool
    ) -> bool:
        """Check if dates must be unique."""
        is_required = False
        if current_builder in [
            "I2Classification",
            "I2FeaturesMap",
            "I2Obia",
            "I2Regression",
        ]:
            if not temporal_interp or features_from_raw_dates:
                is_required = True
        return is_required

    sensors_dates = i2_params.get_available_sensors_dates()
    for sensor_name, sensor_dates in sensors_dates.items():
        dates_counter = Counter(sensor_dates)
        if sensor_name.lower() != "userfeatures" and sensor_name.lower() != "sentinel1":
            duplicated_dates = []
            for date, count in dates_counter.items():
                if count > 1:
                    duplicated_dates.append(date)
            if duplicated_dates and unique_date_required(
                current_builder, temporal_interp, features_from_raw_dates
            ):
                raise ValueError(
                    "Iota2 detected duplicated dates "
                    f"'{', '.join(map(str, duplicated_dates))}' "
                    "for the sensor '{sensor_name}' "
                    "please merge or remove them."
                    "Iota2 provide the tool 'merge_s2_acquisitions.py',"
                    "You can check it's usage with the command "
                    "merge_s2_acquisitions.py -h"
                )
        if (
            sensor_name.lower() != "sentinel1"
            and sensor_name.lower() != "userfeatures"
            and len(sensor_dates) < minimum_required_dates
        ):
            raise ValueError(
                "Iota2 detected insufficient number of "
                f"available dates for the sensor '{sensor_name}'"
                f", {len(sensor_dates)} dates detected."
                f" Minimum is {minimum_required_dates}"
            )
