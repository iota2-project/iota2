#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Builder to produce a large scale map from an external model."""
import logging
from functools import partial
from pathlib import Path
from typing import Any

import iota2.common.i2_constants as i2_const
from iota2.common.file_utils import file_search_and, sort_by_first_elem
from iota2.common.iota2_directory import generate_directories_external_classification
from iota2.sequence_builders.i2_sequence_builder import I2Builder
from iota2.steps import (
    apply_mask_to_classif,
    compute_agreement_matrix,
    confusion_cmd,
    confusions_merge,
    extract_boundary_validation_points,
    generate_boundary_files,
    merge_pretrained_model,
    merge_seed_regressions,
    mosaic,
    pretrained_model,
    regression,
    report_generation,
)
from iota2.steps.iota2_step import Step, StepContainer
from iota2.typings.i2_types import AvailSchedulers

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()


class I2PreTrainedModel(I2Builder):
    """Describe steps sequence and variable to use at each step (config)."""

    def __init__(
        self,
        cfg: str,
        config_resources: Path | None,
        scheduler_type: AvailSchedulers,
        restart: bool = False,
        tasks_states_file: str | None = None,
        hpc_working_directory: str = "TMPDIR",
    ):
        """Initialize the builder."""
        super().__init__(
            cfg,
            config_resources,
            scheduler_type,
            restart,
            tasks_states_file,
            hpc_working_directory,
        )
        self.output_i2_dir = self.i2_cfg_params.get_param("chain", "output_path")

        # create dict for control variables
        self.control_var: dict[str, Any] = {}
        # steps definitions
        # self.steps_group = {}

        self.steps_group["init"] = {}
        self.steps_group["sampling"] = {}
        self.steps_group["learning"] = {}
        self.steps_group["classification"] = {}
        self.steps_group["prediction"] = {}
        self.steps_group["mosaic"] = {}
        self.steps_group["validation"] = {}

        # build steps
        self.sort_step()

    def models_distribution(self) -> None:
        """Find intersections between models and tiles.

        Uses intermediates results to determine the intersections.
        """
        regions_distrib, tiles = self.get_tile_region_mapping()
        # ensure that only tiles in config files are considered
        clear_tiles = []
        for tile_in_folder in tiles:
            if tile_in_folder in self.control_var["tile_list"]:
                clear_tiles.append(tile_in_folder)
        if not clear_tiles:
            raise ValueError(
                "Tiles found in 'formattingVectors' "
                "directory not match with config file"
            )
        Step.set_models_spatial_information(clear_tiles, regions_distrib)

        # manage tiles to classify
        shape_region_dir = str(Path(self.output_i2_directory) / "shapeRegion")
        region_tiles_files = file_search_and(shape_region_dir, False, ".shp")
        regions_to_classify = []
        for region_tile in region_tiles_files:
            region = region_tile.split("_")[-2]
            tile = region_tile.split("_")[-1]
            # Ensure that tile is in config file
            if tile in self.control_var["tile_list"]:
                regions_to_classify.append((region, tile))
        regions_to_classify = sort_by_first_elem(regions_to_classify)
        dico_regions_to_classify = {}
        for region, tiles in regions_to_classify:
            dico_regions_to_classify[region] = {"tiles": tiles}

        regions_classif_distrib = {}

        for region_name, dico_tiles in regions_distrib.items():
            regions_classif_distrib[region_name] = {
                "tiles": dico_regions_to_classify[region_name.split("f")[0]]["tiles"]
            }
            if "nb_sub_model" in dico_tiles:
                regions_classif_distrib[region_name]["nb_sub_model"] = dico_tiles[
                    "nb_sub_model"
                ]

        regions_classif_distrib_no_split = {}
        for (
            region,
            region_meta,
        ) in Step.spatial_models_distribution_no_sub_splits.copy().items():
            regions_classif_distrib_no_split[region] = {
                "nb_sub_model": region_meta["nb_sub_model"],
                "tiles": dico_regions_to_classify[region]["tiles"].copy(),
            }
        Step.spatial_models_distribution_classify = regions_classif_distrib
        Step.spatial_models_distribution_no_sub_splits_classify = (
            regions_classif_distrib_no_split
        )

    def build_steps(self, cfg: str) -> list[StepContainer]:
        """Build steps."""
        Step.set_models_spatial_information(self.tiles, {})

        # control variable
        self.init_dict_control_variables()

        # will contain all IOTA² steps
        s_container = StepContainer(name="classifications_init")
        s_container.prelaunch_functions = [partial(self.models_distribution)]

        # inference
        self.inference_step_group(s_container)

        # mosaic step
        self.mosaic_step_group(s_container)

        # validation steps
        self.validation_steps_group(s_container)
        # don't validate boundaries if no boundaries or region is set in config file
        if self.control_var["boundary_size"] and self.control_var["region_path"]:
            self.boundary_validation_steps_group(s_container)
        return [s_container]

    def pre_check(self) -> None:
        """
        Do some quick checks.

        check configuration file mandatory section
        check configuration parameters consistency
        check region vector
        """
        # Check if parameters are coherent

        self.check_config_parameters()

    def check_config_parameters(self) -> None:
        pass
        # config_content = rcf.ReadConfigFile(self.cfg)
        # ensure that the date and tiles are the same as previously

    def init_dict_control_variables(self) -> None:
        """
        Create and fill the control variables dictionary (self.control_var) using configuration
        file parameters
        """
        self.control_var["tile_list"] = self.i2_cfg_params.get_param(
            "chain", "list_tile"
        ).split(" ")
        self.control_var["keep_runs_results"] = self.i2_cfg_params.get_param(
            "arg_classification", "keep_runs_results"
        )
        self.control_var["iota2_outputs_dir"] = self.i2_cfg_params.get_param(
            "chain", "output_path"
        )
        self.control_var["boundary_size"] = self.i2_cfg_params.get_param(
            "pretrained_model", "boundary_buffer"
        )
        self.control_var["region_path"] = self.i2_cfg_params.get_param(
            "chain", "region_path"
        )
        self.control_var["mode"] = self.i2_cfg_params.get_param(
            "pretrained_model", "mode"
        )

    def inference_step_group(self, s_container: StepContainer) -> None:
        """Initialize classification steps."""

        if self.control_var["mode"] == "classif":
            step_group = "classification"
        elif self.control_var["mode"] == "regression":
            step_group = "prediction"
        else:
            raise ValueError(
                f"Pretrained mode '{self.control_var['mode']}' is invalid. Mode can"
                f"only be either 'classif' or 'regression'"
            )

        # 2. Call the external inference function step
        s_container.append(
            partial(
                pretrained_model.PreTrainedModel,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            step_group=step_group,
        )
        # 3. Merge for each tile/seed/region separately (fuse chunks)
        s_container.append(
            partial(
                merge_pretrained_model.MergePreTrainedModelByTiles,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            step_group=step_group,
        )

        # 4. Apply classification masks
        s_container.append(
            partial(
                apply_mask_to_classif.ApplyMaskToMap,
                self.cfg,
                self.config_resources,
            ),
            step_group=step_group,
        )

    def mosaic_step_group(self, s_container: StepContainer) -> None:
        """Initialize mosaic step."""

        s_container.append(
            partial(
                mosaic.Mosaic,
                self.cfg,
                self.config_resources,
                self.working_directory,
                mode=self.control_var["mode"],
            ),
            "mosaic",
        )

        if (
            self.i2_cfg_params.get_param("multi_run_fusion", "merge_run")
            and self.i2_cfg_params.get_param("arg_train", "runs") > 1
        ):

            s_container.append(
                partial(
                    merge_seed_regressions.MergeSeedRegressions,
                    self.cfg,
                    self.config_resources,
                    self.working_directory,
                ),
                "mosaic",
            )

    def validation_steps_group(self, s_container: StepContainer) -> None:
        """Fill the step container with the validation steps."""

        if self.control_var["mode"] == "classif":
            s_container.append(
                partial(
                    confusion_cmd.ConfusionCmd,
                    self.cfg,
                    self.config_resources,
                ),
                "validation",
            )
            if self.control_var["keep_runs_results"]:
                s_container.append(
                    partial(
                        confusions_merge.ConfusionsMerge,
                        self.cfg,
                        self.config_resources,
                    ),
                    "validation",
                )
                s_container.append(
                    partial(
                        report_generation.ReportGeneration,
                        self.cfg,
                        self.config_resources,
                    ),
                    "validation",
                )
        elif self.control_var["mode"] == "regression":

            s_container.append(
                partial(
                    regression.GenerateRegressionMetrics,
                    self.cfg,
                    self.config_resources,
                ),
                "validation",
            )
            if (
                len(self.tiles) > 1
                or self.i2_cfg_params.get_param("arg_train", "runs") > 1
            ):
                s_container.append(
                    partial(
                        regression.GenerateRegressionMetricsSummary,
                        self.cfg,
                        self.config_resources,
                    ),
                    "validation",
                )

            if (
                self.i2_cfg_params.get_param("multi_run_fusion", "merge_run")
                and self.i2_cfg_params.get_param("arg_train", "runs") > 1
            ):
                s_container.append(
                    partial(
                        merge_seed_regressions.MergeSeedValidation,
                        self.cfg,
                        self.config_resources,
                    ),
                    "validation",
                )

    def boundary_validation_steps_group(self, s_container: StepContainer) -> None:
        """Fill the step container with step required for boundary studies."""

        s_container.append(
            partial(
                generate_boundary_files.GenerateBoundaryRegionFiles,
                self.cfg,
                self.config_resources,
            ),
            "validation",
        )
        s_container.append(
            partial(
                extract_boundary_validation_points.ExtractBoundariesPoints,
                self.cfg,
                self.config_resources,
                self.working_directory,
            ),
            "validation",
        )
        s_container.append(
            partial(
                compute_agreement_matrix.ComputeAgreementMatrix,
                self.cfg,
                self.config_resources,
            ),
            "validation",
        )

    def generate_output_directories(
        self, first_step_index: int, restart: bool, builder_index: int = 0
    ) -> None:
        """Generate the output directories.

        Parameters
        ----------
        first_step_index:
            The index of the first step processed
        restart:
            Indicate if the chain is in restart mode (all steps done are skipped)
        builder_index:
            Used to chain builders. This one is not chainable.
        """
        i2_output_dir = self.control_var["iota2_outputs_dir"]
        # never remove the output path
        if first_step_index == 0 and not restart and builder_index == 0:
            generate_directories_external_classification(
                i2_output_dir, self.control_var["boundary_size"]
            )
