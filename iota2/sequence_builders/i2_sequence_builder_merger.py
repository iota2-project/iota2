#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Builder used to merge other builders together
"""

import logging
from pathlib import Path

from dask.delayed import Delayed

from iota2.sequence_builders.i2_sequence_builder import I2Builder, WaitingI2Graph
from iota2.steps.iota2_step import StepContainer
from iota2.typings.i2_types import AvailSchedulers

LOGGER = logging.getLogger("distributed.worker")


class WorkflowMerger(I2Builder):
    """class dedicated to merge i2_sequence_builder objects"""

    def __init__(
        self,
        builders_list: list,
        cfg: str,
        config_resources: Path | None,
        scheduler_type: AvailSchedulers,
        restart: bool = False,
        tasks_states_file: str | None = None,
        hpc_working_directory: str | None = "TMPDIR",
    ):
        super().__init__(
            cfg,
            config_resources,
            scheduler_type,
            restart,
            tasks_states_file,
            hpc_working_directory,
        )
        self.builders_instance = []
        self.builders_len = []
        for builder in builders_list:
            dummy_builder = builder(
                cfg,
                config_resources,
                scheduler_type,
                restart,
                tasks_states_file,
                hpc_working_directory,
            )
            self.builders_instance.append(dummy_builder)
            self.builders_len.append(dummy_builder.get_steps_sequence_size())
            for group_name, _ in dummy_builder.steps_group.items():
                self.steps_group[group_name] = {}
        # build steps
        self.sort_step()

    def get_builders_steps(
        self, first_step_index: int, last_step_index: int, *builders_length: int
    ) -> list[tuple[int, int]]:
        """
        Return list of pairs of indices corresponding to each builder's step

        Parameters
        ----------
        first_step_index:
            first index
        last_step_index:
            last index
        builders_length:
            list of builder's lengths

        Notes
        -----
        The returned list contains pairs of indices corresponding to the range of steps of each
        builder
        """
        dico: dict[int, dict] = {}
        len_container_buff = 0
        max_step_index = 0
        for builder_num, length in enumerate(builders_length):
            dico[builder_num] = {}
            for cpt in range(length):
                indice = cpt + len_container_buff
                dico[builder_num][indice] = cpt
                max_step_index += 1
            len_container_buff = length

        last_step_index = min(last_step_index, max_step_index - 1)

        list_ind: list[tuple] = []
        for builder_num, length in enumerate(builders_length):
            if (
                first_step_index in dico[builder_num]
                and last_step_index not in dico[builder_num]
            ):
                list_ind.append(
                    (
                        dico[builder_num][first_step_index],
                        dico[builder_num][next(reversed(dico[builder_num]))],
                    )
                )
            elif (
                last_step_index in dico[builder_num]
                and first_step_index not in dico[builder_num]
            ):
                list_ind.append((0, dico[builder_num][last_step_index]))
            elif (
                last_step_index in dico[builder_num]
                and first_step_index in dico[builder_num]
            ):
                list_ind.append(
                    (
                        dico[builder_num][first_step_index],
                        dico[builder_num][last_step_index],
                    )
                )
            else:
                list_ind.append(())
        return list_ind

    def build_steps(self, cfg: str) -> list[StepContainer]:
        steps_containers = []
        for builder in self.builders_instance:
            steps_containers += builder.build_steps(cfg)
        return steps_containers

    def generate_output_directories(self, first_step_index: int, restart: bool) -> None:

        for builder_index, builder in enumerate(self.builders_instance):
            builder.generate_output_directories(
                first_step_index, restart, builder_index
            )

    def pre_check(self) -> None:
        """
        Run all builder's pre-check functions
        """
        for builder in self.builders_instance:
            builder.pre_check()

    def get_final_i2_exec_graph(
        self,
        first_step_index: int,
        last_step_index: int,
        output_figures: list[str] | None = None,
    ) -> Delayed:
        """
        Constructs and returns the complete merged i2 execution graph based
        on the specified steps and optional output figures.

        Parameters
        ----------
        first_step_index : int
            The index of the first step in the sequence.
        last_step_index : int
            The index of the last step in the sequence.
        output_figures : list[str] | None
            A list of filenames to save the figures of the execution graphs.
            If not provided, no output figures are saved.

        Returns
        -------
        Delayed
            The final execution graph for the i2 pipeline, possibly with figure outputs.
        """
        step_to_run = self.get_builders_steps(
            first_step_index, last_step_index, *self.builders_len
        )

        final_graph, graphs_per_builders = self.retrieve_graphs_from_builders(
            step_to_run
        )

        if not output_figures:
            return final_graph

        graph_figures_list = self.generate_figure_filenames(
            graphs_per_builders, output_figures
        )

        final_graph = self.retrieve_graphs_with_figures(graph_figures_list, step_to_run)
        return final_graph

    def retrieve_graphs_with_figures(
        self, graph_figures_list: list[list[str]], step_to_run: list[tuple[int, int]]
    ) -> list[WaitingI2Graph]:
        """
        Retrieve and build the execution graphs with the specified figure filenames.

        Parameters
        ----------
        graph_figures_list : list[list[str]]
            A list of lists containing filenames for the output figures, where each sublist
            corresponds to a builder.
        step_to_run : list[tuple[int, int]]
            The list of start and end step indices to execute for each builder.

        Returns
        -------
        list[WaitingI2Graph]
            A list of `WaitingI2Graph` objects representing the final merged execution graphs
            for the builders.
        """
        final_graph = []
        graph_figures_ind = 0
        for tuple_start_end, builder in zip(step_to_run, self.builders_instance):
            if tuple_start_end:
                new_graphs = builder.get_final_i2_exec_graph(
                    tuple_start_end[0],
                    tuple_start_end[1],
                    graph_figures_list[graph_figures_ind],
                )
                for wating_new_graph in new_graphs:
                    final_graph.append(
                        WaitingI2Graph(
                            self,
                            wating_new_graph.container,
                            wating_new_graph.starting_step,
                            wating_new_graph.ending_step,
                            wating_new_graph.output_graph,
                            wating_new_graph.container.prelaunch_functions,
                            wating_new_graph.figure_suffix,
                        )
                    )
                graph_figures_ind += 1
        return final_graph

    def generate_figure_filenames(
        self, graphs_per_builders: dict[str, int], output_figures: list[str]
    ) -> list[list[str]]:
        """
        Generate the filenames for the output figures based on the builders
        and provided figure filenames.

        Parameters
        ----------
        graphs_per_builders : dict[str, int]
            A dictionary mapping builder names to the number of graphs each builder generated.
        output_figures : list[str]
            A list of filenames to be used for the output figures. If not enough filenames are
            provided, defaults are generated.

        Returns
        -------
        list[list[str]]
            A list of lists where each sublist contains the figure filenames for the graphs of
            a specific builder.
        """
        default_graph_fig_names = []
        for builder_name, builder_size in graphs_per_builders.items():
            builder_graphs = []
            for builder_graph_ind in range(builder_size):
                builder_graphs.append(
                    f"{builder_name}_graph_{builder_graph_ind + 1}.png"
                )
            default_graph_fig_names.append(builder_graphs)
        graph_figures_list = []
        default_fig_directory = Path(output_figures[0]).parent
        ind = 0
        for builder_fig_list in default_graph_fig_names:
            buff = []
            for file_name in builder_fig_list:
                try:
                    buff.append(output_figures[ind])
                except IndexError:
                    buff.append(str(Path(default_fig_directory) / file_name))

                ind += 1
            graph_figures_list.append(buff)
        return graph_figures_list

    def retrieve_graphs_from_builders(
        self, step_to_run: list[tuple[int, int]]
    ) -> tuple[list[WaitingI2Graph], dict[str, int]]:
        """
        Retrieve the execution graphs for the specified steps and builders, and track how many
        graphs each builder generates.

        Parameters
        ----------
        step_to_run : List[Tuple[int, int]]
            A list of start and end step indices for each builder to execute.

        Returns
        -------
        Tuple[List[WaitingI2Graph], Dict[str, int]]
            - A list of `WaitingI2Graph` objects representing the execution graphs for the builders
            - A dictionary mapping builder names to the number of graphs each builder generated.
        """
        final_graph = []
        graphs_per_builders = {}
        for tuple_start_end, builder in zip(step_to_run, self.builders_instance):
            if tuple_start_end:
                new_graphs = builder.get_final_i2_exec_graph(
                    tuple_start_end[0], tuple_start_end[1], ""
                )
                for wating_new_graph in new_graphs:
                    final_graph.append(
                        WaitingI2Graph(
                            self,
                            wating_new_graph.container,
                            wating_new_graph.starting_step,
                            wating_new_graph.ending_step,
                            wating_new_graph.output_graph,
                            wating_new_graph.container.prelaunch_functions,
                            wating_new_graph.figure_suffix,
                        )
                    )
                graphs_per_builders[type(builder).__name__] = len(new_graphs)
        return final_graph, graphs_per_builders
