#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provide function to compute metrics."""
import logging
from pathlib import Path

import numpy as np
from numpy._typing import ArrayLike

from iota2.common import file_utils as fu
from iota2.typings.i2_types import ClassifScores, PathLike
from iota2.validation.results_utils import parse_csv
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def compute_kappa(confusion_matrix: np.ndarray) -> float:
    """Compute kappa coefficients.

    Parameters
    ----------
    confusion_matrix : np.array representing confusion matrix
    """
    nbr_classes = confusion_matrix.shape[0]
    nbr_good = confusion_matrix.trace()
    nbr_sample = confusion_matrix.sum()

    overall_accuracy = -1.0
    if nbr_sample > 0:
        overall_accuracy = nbr_good / nbr_sample

    lucky_rate = 0.0
    for cpt_i in range(nbr_classes):
        sum_ij = 0.0
        sum_ji = 0.0
        for cpt_j in range(nbr_classes):
            sum_ij += confusion_matrix[cpt_i][cpt_j]
            sum_ji += confusion_matrix[cpt_j][cpt_i]
        lucky_rate += sum_ij * sum_ji

    kappa = np.nan
    denom = (nbr_sample * nbr_sample) - lucky_rate
    if np.abs(denom) > 1e-15:
        kappa = ((overall_accuracy * nbr_sample * nbr_sample) - lucky_rate) / denom

    return kappa


def compute_overall_accuracy(confusion_matrix: np.ndarray) -> float:
    """
    Compute the overall accuracy using a confusion matrix

    Parameters
    ----------
    confusion_matrix:
        Numpy array containing the confusion matrix
    """
    nbr_good = confusion_matrix.trace()

    if (nbr_sample := confusion_matrix.sum()) > 1:
        overall_acc = float(nbr_good) / float(nbr_sample)
    else:
        overall_acc = 0.0

    return overall_acc


def compute_precision_by_class(
    confusion_matrix: np.ndarray, all_class: list[int]
) -> dict[int, float]:
    """Compute precision by class.

    Parameters
    ----------
    confusion_matrix :
        numpy array representing confusion matrix
    all_class :
        list of class' label
    """
    precision = {}  # {class:precision}
    nom = 0.0
    for cpt_i, class_id in enumerate(all_class):
        denom = 0
        for cpt_j, _ in enumerate(all_class):
            denom += confusion_matrix[cpt_j][cpt_i]
            if cpt_i == cpt_j:
                nom = float(confusion_matrix[cpt_j][cpt_i])
        if denom != 0:
            current_pre = float(nom) / float(denom)
        else:
            current_pre = 0.0
        precision[class_id] = current_pre
    return precision


def compute_recall_by_class(
    confusion_matrix: np.ndarray, all_class: list[int]
) -> dict[int, float]:
    """Compute recall by class.

    Parameters
    ----------
    confusion_matrix : np.array of np.array
        numpy array representing confusion matrix
    all_class : list
        list of class' label
    """
    recall = {}  # [(class,rec),(...),()...()]
    nom = 0.0
    for cpt_i, class_id in enumerate(all_class):
        denom = 0
        for cpt_j, _ in enumerate(all_class):
            denom += confusion_matrix[cpt_i][cpt_j]
            if cpt_i == cpt_j:
                nom = float(confusion_matrix[cpt_i][cpt_j])
        if denom != 0:
            current_recall = float(nom) / float(denom)
        else:
            current_recall = 0.0
        recall[class_id] = current_recall
    return recall


def compute_fscore_by_class(
    precision: dict[int, float], recall: dict[int, float], all_class: list[int]
) -> dict[int, float]:
    """Compute fscore for each class from precisions and recalls."""
    f_score = {}
    for class_id in all_class:
        if float(recall[class_id] + precision[class_id]) != 0:
            f_score[class_id] = float(
                2 * recall[class_id] * precision[class_id]
            ) / float(recall[class_id] + precision[class_id])
        else:
            f_score[class_id] = 0.0
    return f_score


def write_csv(
    confusion_matrix: np.ndarray, all_class: list[int], path_out: PathLike
) -> None:
    """Write CSV matrix in OTB format."""
    all_c = ",".join([str(x) for x in all_class])
    with open(path_out, "w", encoding="utf-8") as csv_file:
        # csv_file =
        csv_file.write("#Reference labels (rows):" + all_c + "\n")
        csv_file.write("#Produced labels (columns):" + all_c + "\n")
        for line in confusion_matrix:
            csv_file.write(",".join([str(x) for x in line]) + "\n")
        csv_file.close()


def write_results(
    scores: ClassifScores,
    all_class: list[int],
    path_out: str,
) -> None:
    """Write report in a text file in OTB format.

    Parameters
    ----------
    scores:
        Dataclass containing:
            f_score:
                Dict of F-score by class {class_id: fscore_value}
            recall:
                Dict of recall by class {class_id: recall_value}
            precision:
                Dict of precision by class {class_id: precision_value}
            kappa:
                kappa value
            overall_accuracy:
                overall accuracy value
    all_class:
        list of all class
    path_out:
        output file to store report
    """
    # Convert values to str to simplify writing
    all_class_str = [str(x) for x in all_class]
    all_recall_str = [str(x) for x in scores.recall.values()]
    all_precision_str = [str(x) for x in scores.precision.values()]
    all_fscore_str = [str(x) for x in scores.fscore.values()]

    with open(path_out, "w", encoding="utf-8") as res_file:
        res_file.write("#Reference labels (rows):")
        res_file.write(",".join(all_class_str) + "\n")
        res_file.write("#Produced labels (columns):")
        res_file.write(",".join(all_class_str) + "\n\n")

        for n_class in all_class:
            res_file.write(
                f"Precision of class [{n_class}] vs all: "
                f"{scores.precision[n_class]}\n"
            )
            res_file.write(
                f"Recall of class [{n_class}] vs all: " f"{scores.recall[n_class]}\n"
            )
            res_file.write(
                f"F-score of class [{n_class}] vs all: " f"{scores.fscore[n_class]}\n\n"
            )

        res_file.write(
            "Precision of the different classes: [" f"{','.join(all_precision_str)}]\n"
        )
        res_file.write(
            "Recall of the different classes: [" f"{','.join(all_recall_str)}]\n"
        )
        res_file.write(
            "F-score of the different classes: [" f"{','.join(all_fscore_str)}]\n"
        )
        res_file.write("Kappa index: " + str(scores.kappa) + "\n")
        res_file.write("Overall accuracy index: " + str(scores.oacc))

        res_file.close()


def replace_annual_crop_in_conf_mat(
    confusion_matrix: np.ndarray,
    all_class: list[int | str],
    annual_crop: list[str],
    label_replacement: int,
) -> tuple[np.ndarray, list]:
    """Replace some labels by fusing them in the matrix.

    Parameters
    ----------
    confusion_matrix:
        the confusion matrix as a numpy array
    all_class:
        the list of all labels contained in the confusion matrix
    annual_crop:
        the list of class to regroup.
        Only one group can be provided at each call.
    label_replacement:
        the label name of the fused class
    Example
    -------
    all_class = [1,2,3,4]
    confusion_matrix = [[1 2 3 4] [5 6 7 8] [9 10 11 12] [13 14 15 16]]

    confusion_matrix.csv
    #ref label rows : 1,2,3,4
    #prod label col : 1,2,3,4
    1,2,3,4
    5,6,7,8
    9,10,11,12
    13,14,15,16

    annual_crop = ['1','2']
    label_replacement = '0'

    outMatrix,outall_class = replace_annual_crop_in_conf_mat(confusion_matrix,
                                                              all_class,
                                                              annual_crop,
                                                              label_replacement)

    outall_class = [0,3,4]
    confusion_matrix = [[14 10 12] [19 11 12] [27 15 16]]
    """
    all_index = []

    for current_class in annual_crop:
        try:
            ind = all_class.index(int(current_class))
            all_index.append(ind)
        except ValueError as err:
            raise Exception(f"Class : {current_class} " "doesn't exists") from err

    all_class_ac = all_class[:]
    for label_annual_crop in annual_crop:
        all_class_ac.remove(int(label_annual_crop))
    all_class_ac.append(int(label_replacement))
    all_class_ac.sort()
    index_ac = all_class_ac.index(int(label_replacement))

    out_matrix = replace_required_label(
        all_class, all_index, confusion_matrix, index_ac
    )
    return out_matrix, all_class_ac


def replace_required_label(
    all_class: list[int | str],
    all_index: list[int],
    confusion_matrix: np.ndarray,
    index_ac: int,
) -> ArrayLike:
    """
    Replace the required annual crop label
    Parameters
    ----------
    all_class
        the list of all labels contained in the confusion matrix
    all_index
        List of all classes' indexes
    confusion_matrix
        the confusion matrix as a numpy array
    index_ac
        Index of the label to replace
    """
    # replace ref labels in confusion matrix
    out_matrix = []
    matrix = []
    for cpt_i, _ in enumerate(all_class):
        if cpt_i not in all_index:
            matrix.append(confusion_matrix[cpt_i])
    tmp_y = [0] * len(all_class)
    for cpt_i in all_index:
        tmp_y = tmp_y + confusion_matrix[cpt_i, :]
    matrix.insert(index_ac, np.array(tmp_y))
    # replace produced labels in confusion matrix
    for line in matrix:
        tmp_x = []
        buff = 0
        for cpt_i, val in enumerate(line):
            if cpt_i not in all_index:
                tmp_x.append(val)
            else:
                buff += val
        tmp_x.insert(index_ac, buff)
        out_matrix.append(tmp_x)
    return np.asarray(out_matrix)


def merge_confusions(csv_in: list[str], labels: list[int], csv_out: str) -> None:
    """Merge all input matrices into an unique one.

    Parameters
    ----------
    csv_in :
        paths to csv confusion matrix
    labels :
        all possible labels
    csv_out :
        output path
    """
    csv = fu.conf_coordinates_csv(csv_in)
    csv_f = fu.sort_by_first_elem(csv)
    conf_mat = fu.gen_confusion_matrix(csv_f, labels)
    write_csv(conf_mat, labels, csv_out)


def confusion_models_merge(csv_list: list[str]) -> None:
    """Merge all confusion matrices according to the model name.

    Parameters
    ----------
    csv_list:
        List of matrices to be merged
    """
    csv_path = str(Path(csv_list[0]).parent)
    csv_name = Path(csv_list[0]).name
    csv_seed_pos = 4
    csv_model_pos = 2
    csv_seed = csv_name.split("_")[csv_seed_pos]
    csv_model = csv_name.split("_")[csv_model_pos]
    csv_suffix = ""
    if "sar" in csv_name:
        csv_suffix = "_SAR"
    output_merged_csv_name = f"model_{csv_model}_seed_{csv_seed}{csv_suffix}.csv"
    output_merged_csv = str(Path(csv_path) / output_merged_csv_name)
    labels: list[int] = []
    for csv in csv_list:
        conf_mat_dic = parse_csv(csv)
        labels_ref = list(conf_mat_dic.keys())
        labels_prod = list(conf_mat_dic[list(conf_mat_dic.keys())[0]].keys())
        labels = labels + labels_ref + labels_prod

    labels = sorted(list(set(labels)))
    merge_confusions(csv_list, labels, output_merged_csv)


def confusion_fusion(
    input_vector: str, data_field: str, out_path: PathLike, runs: int
) -> None:
    """Merge otb tile confusion matrix.

    Parameters
    ----------
    input_vector: str
        input database
    data_field: str
        data field
    out_path:
        list of path containing *.csv files to merge and where the merged matrices will be stored
    runs: int
        number of random learning/validation samples-set
    """
    for seed in range(runs):
        # Look for all possible classes
        all_class = vf.get_field_element(input_vector, int, data_field, "unique")
        assert all_class
        all_class = sorted(all_class)

        # Initialize output matrices
        all_conf = fu.file_search_and(out_path, True, f"seed_{seed}.csv")

        csv = fu.conf_coordinates_csv(all_conf)
        csv_f = fu.sort_by_first_elem(csv)

        conf_mat = fu.gen_confusion_matrix(csv_f, all_class)
        write_csv(conf_mat, all_class, str(Path(out_path) / f"Classif_Seed_{seed}.csv"))
        precision = compute_precision_by_class(conf_mat, all_class)
        recall = compute_recall_by_class(conf_mat, all_class)
        scores = ClassifScores(
            kappa=compute_kappa(conf_mat),
            oacc=compute_overall_accuracy(conf_mat),
            precision=precision,
            recall=recall,
            fscore=compute_fscore_by_class(precision, recall, all_class),
        )
        write_results(
            scores,
            all_class,
            str(Path(out_path) / f"Results_seed_{seed}.txt"),
        )


def confusion_fusion_folder(
    input_vector: str, data_field: str, out_path_list: list[str], runs: int
) -> None:
    """Launch merge otb tile confusion matrix.

    Parameters
    ----------
    input_vector:
        input database
    data_field:
        data field
    out_path_list:
        list of path containing *.csv files to merge and where the merged matrices will be stored
    runs:
        number of random learning/validation samples-set
    """
    for out_path in out_path_list:
        confusion_fusion(input_vector, data_field, out_path, runs)
