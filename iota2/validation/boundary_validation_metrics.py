#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provide tools for computing validation metrics at boundary scale."""
import os
import shutil
from pathlib import Path

import geopandas as gpd
import numpy as np
import pandas as pd

from iota2.common import otb_app_bank as otb
from iota2.common.file_utils import file_search_and
from iota2.common.otb_app_bank import AvailableOTBApp
from iota2.typings.i2_types import (
    BoundaryValidationParameters,
    LabelsConversionDict,
    MergeMetricMatricesParameters,
    PathLike,
)
from iota2.validation import confusion_fusion as cf
from iota2.validation import results_utils as rus


def compute_buffer_adjacent_regions(
    input_file: str, output_file: str, size: int, region1: str, region2: str
) -> None:
    """
    Generate N shapefiles containing boundaries between two regions.

    Parameters
    ----------
    input_file:
        the eco_region shapefile
    output_file:
        the name of the output boundaries.
        If not exists at the end, there is no boundary between regions.
    size:
        The buffer size to define the boundary area
    region1:
        The first region name
    region2:
        The second region name
    """
    # convert to string:
    region1 = str(region1)
    region2 = str(region2)
    size_buffer = size  # / 2.0
    ecoregion_gdf = gpd.read_file(input_file)
    ecoregion = ecoregion_gdf.copy()
    # Keep only the polygones of region 1 or region 2 (sélectionner
    # uniquement les 2 régions d'intérêt)
    ecoregion_region12 = ecoregion.loc[
        (ecoregion["region"] == region1) | (ecoregion["region"] == region2)
    ]
    ecoregion_buffer = ecoregion_region12.copy()
    # Add a positive buffer (ce qui correspond à un buffer extérieur, on va
    # donc sortir de la zone)
    ecoregion_buffer["geometry"] = ecoregion_buffer.geometry.buffer(size_buffer)
    # Attention à l'ordre d'abord buffer puis ecoregion!!
    # Union entre le shapefile ecoregion et le shapefile buffer plus
    ecoregion_union = gpd.overlay(ecoregion_buffer, ecoregion_region12, how="union")
    # Delete boundary outside regions (on va avoir des Nan lorsque le buffer
    # sort des régions)
    ecoregion_union = ecoregion_union.dropna()
    # Keep only the boundary (on ne garde uniquement la zone où le buffer et
    # correspond à la différence entre le buffer et le fichier ecoregion)
    ecoregion_exter = ecoregion_union.loc[
        (ecoregion_union["region_1"] != ecoregion_union["region_2"])
    ]
    # renommer le nom du tableau pour la suite
    ecoregion_exter2 = ecoregion_exter.rename(columns={"region_1": "region"})

    # Si la frontière entre deux régions n'existe pas alors, on n'écrit pas le
    # fichier
    if not ecoregion_exter2.empty:
        ecoregion_exter2["extractid"] = np.ones(len(ecoregion_exter2.index), dtype=int)
        ecoregion_exter2.to_file(output_file)
    else:
        print(f"No boundary between {region1} and {region2}")


def compute_all_boundaries(
    eco_region_file: str, region_field: str, buffer_size: int, output_path: str
) -> None:
    """
    Generate boundary files for all possible pairs of regions.

    Parameters
    ----------
    eco_region_file:
        Path to the file containing ecoregions.
    region_field:
        Field name containing region number.
    buffer_size:
        Size of the buffer for generating boundaries.
    output_path:
        Path to the directory where boundary files will be saved.
    """
    # Open file and get all regions
    df_ecoregion = gpd.read_file(eco_region_file)
    regions = df_ecoregion[region_field].unique()
    regions.sort()
    seen_pairs = []
    for region1 in regions:
        for region2 in regions:
            if region1 != region2 and (region1, region2) not in seen_pairs:
                seen_pairs.append((region1, region2))
                seen_pairs.append((region2, region1))
                output_name = str(
                    Path(output_path)
                    / f"boundary_region_{region1}_with_region_{region2}.shp"
                )
                compute_buffer_adjacent_regions(
                    input_file=eco_region_file,
                    output_file=output_name,
                    size=buffer_size,
                    region1=region1,
                    region2=region2,
                )


def compute_agreement_matrix(
    validation_parameters: BoundaryValidationParameters,
    classifs_to_compare: tuple[str, str],
    boundary_region: str,
    output_path: PathLike,
    working_directory: PathLike | None = None,
) -> None:
    """
    Compute the agreement matrix between two classification models.

    Parameters
    ----------
    validation_parameters:
        Object containing validation parameters
    classifs_to_compare:
        Tuple containing paths of both of the classification models
    boundary_region:
        File path of the boundary area vector file.
    output_path:
        Directory where output files will be saved.
    working_directory:
        Working directory path, default is None.
    """
    wdir = working_directory if working_directory else output_path
    region_1 = classifs_to_compare[0].split("_")[-3]
    region_2 = classifs_to_compare[1].split("_")[-3]
    # Create agreement map
    agreement_map = str(
        Path(wdir) / f"{validation_parameters.tile}_agreement_{region_1}"
        f"_{region_2}_{validation_parameters.seed}.tif"
    )
    bandmath, _ = otb.create_application(
        AvailableOTBApp.BAND_MATH,
        {"il": classifs_to_compare, "exp": "im1b1==im2b1?1:0", "out": agreement_map},
    )
    bandmath.ExecuteAndWriteOutput()

    # Compute agreement
    conf_app, _ = otb.create_application(
        AvailableOTBApp.CONFUSION_MATRIX,
        {
            "in": agreement_map,
            "ref": "vector",
            "ref.vector.in": boundary_region,
            "ref.vector.field": "extractid",
            "nodatalabel": 255,
            "out": agreement_map.replace("tif", "csv"),
        },
    )
    conf_app.ExecuteAndWriteOutput()
    if working_directory:
        shutil.copy(
            agreement_map.replace("tif", "csv"),
            str(
                Path(output_path) / f"{validation_parameters.tile}_agreement_{region_1}"
                f"_{region_2}_{validation_parameters.seed}.csv"
            ),
        )


# #############################
# From boundary fusion
# #############################


def prepare_entry_for_boundary_validation(
    tile: str,
    seed: int,
    data_field: str,
    iota2_directory: str,
    working_directory: str | None = None,
) -> None:
    """
    Use a tile and a path to validate at the boundary scale.

    Parameters
    ----------
    tile:
        Name of the tile.
    seed:
        Seed.
    data_field:
        The data field in the validation data set.
    iota2_directory:
        Path to iota2 root output path.
    working_directory:
        Working directory path.
    """
    path_to_classif = str(Path(iota2_directory) / "classif" / "full_pred")
    boundary_region_path = str(
        Path(iota2_directory) / "boundary_vectors" / "boundary_files"
    )
    all_classif_in_tile = file_search_and(
        path_to_classif, True, f"Classif_{tile}", f"seed_{seed}.tif"
    )
    all_classif_in_tile.sort()
    if len(all_classif_in_tile) == 1:
        print(f"Only one region for tile {tile}. No boundary validation to process.")
        return None
    # Identify the region used to classify the tile
    # and ensure that they have a common boundary
    all_boundaries = find_common_boundaries(all_classif_in_tile, boundary_region_path)
    for keys, boundary_files in all_boundaries.items():
        classifications = (keys[0], keys[1])
        for boundary_region in boundary_files:
            buffer_size = int(boundary_region.split("/")[-2])
            validation_parameters = BoundaryValidationParameters(
                tile=tile,
                iota2_directory=iota2_directory,
                data_field=data_field,
                seed=seed,
            )
            prepare_boundary_validation_dataset(
                validation_parameters,
                classifications,
                boundary_region,
                buffer_size,
                working_directory,
            )
    return None


def find_common_boundaries(
    all_classif_in_tile: list[str], boundary_region_path: str
) -> dict[tuple[str, str], list[str]]:
    """
    Search common boundaries for all classification files.
    Return a dict containing tuples of classification files as keys and corresponding boundary
    files as values.

    Parameters
    ----------
    all_classif_in_tile:
        List of paths to classification files
    boundary_region_path:
        Path to the directory containing the boundary region files
    """
    tested_classif = []
    all_boundary = {}
    for classif in all_classif_in_tile:
        tested_classif.append(classif)
        for classif_2 in all_classif_in_tile:
            if classif_2 not in tested_classif:
                region_a = classif.split("_")[-3]
                region_b = classif_2.split("_")[-3]
                # region are sorted in increasing order
                region1, region2 = (
                    (region_a, region_b)
                    if int(region_a) < int(region_b)
                    else (region_b, region_a)
                )
                # search if boundary file exist for a pair of region

                boundary_file = file_search_and(
                    boundary_region_path,
                    True,
                    f"boundary_region_{region1}_with_region_{region2}.shp",
                )

                if boundary_file:
                    all_boundary[classif, classif_2] = boundary_file
    return all_boundary


def prepare_boundary_validation_dataset(
    validation_parameters: BoundaryValidationParameters,
    classifications: tuple[str, str],
    boundary_area: str,
    buffer_size: int,
    working_directory: str | None = None,
) -> None:
    """Extract validation in a vector format.

    Parameters
    ----------
    validation_parameters:
        Object containing validation parameters
    classifications:
        Tuple of paths to the classifications models
    boundary_area:
        The boundary area vector file
    buffer_size:
        The buffer size
    working_directory:
        Path to store intermediate results
    """
    region_1 = classifications[0].split("_")[-3]
    region_2 = classifications[1].split("_")[-3]

    output_path = (
        Path(validation_parameters.iota2_directory)
        / "boundary_vectors"
        / "validation_files"
        / f"{buffer_size}"
    )

    wdir: PathLike = working_directory if working_directory is not None else output_path
    # Get the validation shape file
    ref_shape = os.path.join(
        validation_parameters.iota2_directory,
        "formattingVectors",
        f"{validation_parameters.tile}.shp",
    )
    gdf_ref = gpd.read_file(ref_shape)
    gdf_boundary = gpd.read_file(boundary_area)
    val_boundary = str(
        Path(wdir) / f"{validation_parameters.tile}_{region_1}_{region_2}_"
        f"{buffer_size}_validation_boundary_{validation_parameters.seed}.shp",
    )
    # compute intersection with geopandas to prevent empty intersection
    # ogr can produce empty file which leads to segfault with OTB
    if not Path(val_boundary).exists():
        gdf_ref = gdf_ref.drop("region", 1)

        res_intersection = gpd.overlay(gdf_boundary, gdf_ref, how="intersection")
        res_intersection = res_intersection.loc[
            res_intersection[f"seed_{validation_parameters.seed}"] == "validation"
        ]
        print(res_intersection.columns)
        res_intersection = res_intersection.drop("originfid", axis="columns")
        if res_intersection.empty:
            print("empty")
            return None
        res_intersection.to_file(val_boundary)
    # Compute the agreement matrix
    compute_agreement_matrix(
        validation_parameters,
        classifications,
        boundary_area,
        output_path,
        working_directory=None,
    )
    # From polygons extract values
    extract_samples(
        classifications,
        output_path,
        val_boundary,
        validation_parameters,
        wdir,
    )
    return None


def extract_samples(
    classifications: tuple[str, str],
    output_path: PathLike,
    val_boundary: PathLike,
    validation_parameters: BoundaryValidationParameters,
    wdir: PathLike,
) -> None:
    """
    Launch a pipeline with a polygon class statistics, sample selection and sample extraction apps
    to extract points.

    Parameters
    ----------
    classifications:
        Tuple of paths to the classifications models
    output_path:
        Path to store the resulting file(s)
    val_boundary:
        Path to the boundary validation shapefile
    validation_parameters:
        Object containing validation parameters
    wdir:
        Working directory
    """
    region_1 = classifications[0].split("_")[-3]
    region_2 = classifications[1].split("_")[-3]
    stats_file = str(
        Path(wdir) / f"{validation_parameters.tile}_region_{region_1}_region_"
        f"{region_2}_seed_{validation_parameters.seed}.xml"
    )
    stats, _ = otb.create_application(
        AvailableOTBApp.POLYGON_CLASS_STATISTICS,
        {
            "in": classifications[0],
            "out": stats_file,
            "vec": val_boundary,
            "field": validation_parameters.data_field,
        },
    )
    stats.ExecuteAndWriteOutput()
    select_file = str(
        Path(wdir) / f"{validation_parameters.tile}_region_{region_1}_region_"
        f"{region_2}_seed_{validation_parameters.seed}.shp"
    )
    select, _ = otb.create_application(
        AvailableOTBApp.SAMPLE_SELECTION,
        {
            "in": classifications[0],
            "out": select_file,
            "vec": val_boundary,
            "instats": stats_file,
            "sampler": "periodic",
            "strategy": "all",
            "field": validation_parameters.data_field,
        },
    )
    select.ExecuteAndWriteOutput()
    concat, _ = otb.create_application(
        AvailableOTBApp.CONCATENATE_IMAGES,
        {"il": classifications, "out": "fake_out.tif"},
    )
    concat.Execute()
    extract_points = (
        f"{validation_parameters.tile}_region_{region_1}_region_{region_2}_"
        f"seed_{validation_parameters.seed}.gpkg"
    )
    output_name = str(Path(output_path) / extract_points)
    extract, _ = otb.create_application(
        AvailableOTBApp.SAMPLE_EXTRACTION,
        {
            "in": concat,
            "out": output_name,
            "vec": select_file,
            "field": validation_parameters.data_field,
            "outfield": "list",
            "outfield.list.names": ["cl_1", "cl_2"],
        },
    )
    extract.ExecuteAndWriteOutput()


def find_entry_merge_metrics_matrices(
    iota2_directory: PathLike,
    merge_metrics_parameters_list: list[MergeMetricMatricesParameters],
    eco_region_file: PathLike,
    region_field: str,
    nomenclature_path: PathLike,
) -> None:
    """
    Find entry for merging metrics matrices.

    Parameters
    ----------
    iota2_directory:
        Path to iota2 root output path.
    merge_metrics_parameters_list:
        List of parameters for merging the metrics matrices at boundaries
    eco_region_file:
        File path of the ecoregion shapefile.
    region_field:
        Field name containing region identifiers.
    nomenclature_path:
        Path to the nomenclature file.
    """
    df_ecoregion = gpd.read_file(eco_region_file)
    regions = df_ecoregion[region_field].unique()
    regions.sort()
    seen_pairs = []
    for region_a in regions:
        for region_b in regions:
            region1, region2 = (
                (region_a, region_b)
                if int(region_a) < int(region_b)
                else (region_b, region_a)
            )
            if region1 != region2 and (region1, region2) not in seen_pairs:
                seen_pairs.append((region1, region2))
                for parameters in merge_metrics_parameters_list:
                    merge_metrics_matrices(
                        iota2_directory,
                        parameters,
                        region1,
                        region2,
                        nomenclature_path,
                    )


def get_validation_points(
    iota2_directory: PathLike, buffer_size: int, region1: str, region2: str, seed: int
) -> gpd.geodataframe:
    """
    Return validation points as a geo-dataframe

    Parameters
    ----------
    iota2_directory:
        Path to iota2 root output path.
    buffer_size:
        Size of the buffer
    region1:
        Name of the first region.
    region2:
        Name of the second region.
    seed:
        Seed number
    """
    all_validation_points = file_search_and(
        Path(iota2_directory)
        / "boundary_vectors"
        / "validation_files"
        / str(buffer_size),
        True,
        f"region_{region1}_region_{region2}_seed_{seed}.gpkg",
    )
    if not all_validation_points:
        return None
    points_valid = gpd.GeoDataFrame(
        pd.concat([gpd.read_file(i) for i in all_validation_points], ignore_index=True),
        crs=gpd.read_file(all_validation_points[0]).crs,
    )
    return points_valid


def merge_metrics_matrices(
    iota2_directory: PathLike,
    merge_metrics_parameters: MergeMetricMatricesParameters,
    region1: str,
    region2: str,
    nomenclature_path: PathLike,
) -> None:
    """Merge metrics matrices for given regions.

    Parameters
    ----------
    iota2_directory:
        Path to iota2 root output path.
    merge_metrics_parameters:
        Parameters for merging the metrics matrices at boundaries
    region1:
        Name of the first region.
    region2:
        Name of the second region.
    nomenclature_path:
        Path to the nomenclature file.
    """
    cond_val_bound = []
    cond_val_standard = []
    val_bound = []
    val_standard = []

    for seed in range(merge_metrics_parameters.runs):
        points_valid = get_validation_points(
            iota2_directory,
            merge_metrics_parameters.buffer_size,
            region1,
            region2,
            seed,
        )
        if points_valid is None:
            return None
        output_path_tmp = str(Path(iota2_directory) / "final" / "TMP")
        # conf(M1|Y=Yref, M2)
        output_name = str(
            Path(output_path_tmp) / f"Conditionnal_region_{region1}_"
            f"with_region_{region2}"
            f"_{seed}_{merge_metrics_parameters.buffer_size}.csv",
        )
        df_validation_points = points_valid[
            points_valid[merge_metrics_parameters.data_field] == points_valid["cl_1"]
        ]
        compute_metrics_with_ref(
            df_validation_points,
            output_name,
            ref_field=merge_metrics_parameters.data_field,
            target_field="cl_2",
            label_conv_dict=merge_metrics_parameters.label_conv_dict,
        )
        rus.stats_report(
            [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
        )
        cond_val_bound.append(output_name)
        # conf(M2|Y=Yref, M1)
        output_name = str(
            Path(output_path_tmp) / f"Conditional_region_{region2}"
            f"_with_region_{region1}"
            f"_seed_{seed}_{merge_metrics_parameters.buffer_size}.csv",
        )
        df_validation_points = points_valid[
            points_valid[merge_metrics_parameters.data_field] == points_valid["cl_2"]
        ]
        compute_metrics_with_ref(
            df_validation_points,
            output_name,
            ref_field=merge_metrics_parameters.data_field,
            target_field="cl_1",
            label_conv_dict=merge_metrics_parameters.label_conv_dict,
        )
        rus.stats_report(
            [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
        )
        cond_val_standard.append(output_name)
        # conf(M1,Yref)
        output_name = str(
            Path(output_path_tmp) / f"Confusion_region_{region1}_with_"
            f"region_{region2}_seed_{seed}_{merge_metrics_parameters.buffer_size}.csv",
        )
        compute_metrics_with_ref(
            points_valid,
            output_name,
            ref_field=merge_metrics_parameters.data_field,
            target_field="cl_1",
            label_conv_dict=merge_metrics_parameters.label_conv_dict,
        )
        rus.stats_report(
            [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
        )
        val_standard.append(output_name)
        # conf(M2,Yref)
        output_name = str(
            Path(output_path_tmp) / f"Confusion_region_{region2}_"
            f"with_region_{region1}_seed_{seed}_{merge_metrics_parameters.buffer_size}.csv",
        )
        compute_metrics_with_ref(
            points_valid,
            output_name,
            ref_field=merge_metrics_parameters.data_field,
            target_field="cl_2",
            label_conv_dict=merge_metrics_parameters.label_conv_dict,
        )
        rus.stats_report(
            [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
        )
        val_bound.append(output_name)

    output_path = str(Path(iota2_directory) / "final")
    rus.stats_report(
        cond_val_standard,
        nomenclature_path,
        str(
            Path(output_path) / f"Conditional_region_{region2}_region_{region1}"
            f"_{merge_metrics_parameters.buffer_size}.txt",
        ),
    )
    rus.stats_report(
        cond_val_bound,
        nomenclature_path,
        str(
            Path(output_path) / f"Conditional_region_{region1}_region_{region2}"
            f"_{merge_metrics_parameters.buffer_size}.txt",
        ),
    )
    rus.stats_report(
        val_standard,
        nomenclature_path,
        str(
            Path(output_path) / f"Confusion_region_{region2}_with_region_{region1}"
            f"_{merge_metrics_parameters.buffer_size}.txt",
        ),
    )
    rus.stats_report(
        val_bound,
        nomenclature_path,
        str(
            Path(output_path) / f"Confusion_region_{region1}_with_region_{region2}"
            f"_{merge_metrics_parameters.buffer_size}.txt",
        ),
    )
    return None


def compute_metrics_with_ref(
    points_valid: pd.DataFrame,
    output_path: PathLike,
    ref_field: str,
    target_field: str,
    label_conv_dict: LabelsConversionDict,
) -> None:
    """
    Compute metrics with reference.

    Parameters
    ----------
    points_valid:
        Dataframe containing all validation points.
    output_path:
        Output file path.
    ref_field:
        Reference field name.
    target_field:
        Target field name.
    label_conv_dict:
        Dictionary for label conversion.
    """

    cross_tab = pd.crosstab(
        points_valid[ref_field].astype(int), points_valid[target_field].astype(int)
    )
    ref_lab = [int(x) for x in points_valid[ref_field].unique()]
    pred_lab = [int(x) for x in points_valid[target_field].unique()]
    all_labels = list(set(ref_lab + pred_lab))
    dict_tab = cross_tab.to_dict()
    convert_dataframe_to_confusion_matrix(
        all_labels, dict_tab, output_path, label_conv_dict
    )


def convert_dataframe_to_confusion_matrix(
    all_labels: list[int],
    dict_tab: dict,
    output_path: PathLike,
    label_conv_dict: LabelsConversionDict,
) -> None:
    """Convert a dataframe to confusion matrix in CSV format.

    Parameters
    ----------
    all_labels:
        List of all labels.
    dict_tab:
        Dictionary containing cross-tabulation.
    output_path:
        Output csv file name.
    label_conv_dict:
        Dictionary for label conversion.
    """

    matrix = np.zeros((len(all_labels), len(all_labels)))
    all_labels_index = range(1, len(all_labels) + 1)
    all_labels_conv = [label_conv_dict[i] for i in all_labels]
    for row in all_labels_index:
        for col in all_labels_index:
            if row in dict_tab.keys():
                ss_dict = dict_tab[row]
                if col in ss_dict.keys():
                    matrix[col - 1, row - 1] += ss_dict[col]
    cf.write_csv(matrix, [int(_) for _ in all_labels_conv], output_path)
