#!/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Generate Confusion Matrix."""
import logging
from pathlib import Path

import iota2.common.i2_constants as i2_const
from iota2.common import otb_app_bank, utils
from iota2.typings.i2_types import ConfusionMatrixAppParameters

LOGGER = logging.getLogger("distributed.worker")

I2_CONST = i2_const.Iota2Constants()


def run_gen_conf_matrix(
    conf_matrix_params_list: list[ConfusionMatrixAppParameters],
    labels_table: dict[int, str | int],
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Iterate over list to produce confusion matrix.

    Parameters
    ----------
    conf_matrix_params_list:
        List of ConfusionMatrixAppParameters containing the parameters of each app
    labels_table
        table str labels to integers
    logger
        python logg
    """

    for conf_matrix_params in conf_matrix_params_list:
        gen_conf_matrix(
            conf_matrix_params,
            labels_table=labels_table,
            logger=logger,
        )


def gen_conf_matrix(
    conf_matrix_params: ConfusionMatrixAppParameters,
    labels_table: dict[int, str | int],
    logger: logging.Logger = LOGGER,
) -> None:
    """Generate an otb confusion matrix.

    Parameters
    ----------
    conf_matrix_params:
        ConfusionMatrixAppParameters object containing the required parameters
    labels_table:
        dictionary to convert labels labels_table[old_label] = new_label
    logger:
        logger
    """
    if Path(conf_matrix_params.ref_vector).exists():
        re_encode_labels = utils.is_nomenclature_castable_to_int(labels_table)
        if not re_encode_labels:
            data_field = I2_CONST.re_encoding_label_name
        else:
            data_field = conf_matrix_params.data_field

        confusion_app, _ = otb_app_bank.create_application(
            otb_app_bank.AvailableOTBApp.CONFUSION_MATRIX,
            {
                "in": conf_matrix_params.in_classif,
                "out": conf_matrix_params.out_csv,
                "ref": "vector",
                "ref.vector.in": conf_matrix_params.ref_vector,
                "ref.vector.field": data_field.lower(),
                "ram": conf_matrix_params.ram,
            },
        )
        confusion_app.ExecuteAndWriteOutput()
    else:
        logger.warning(
            f"{conf_matrix_params.ref_vector} does not exists, maybe there is "
            "no validation samples available in the tile"
        )
