#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provides function to boundary fusion processing."""
import logging
import shutil
from functools import partial
from pathlib import Path

import geopandas as gpd
import numpy as np
import pandas as pd
import rasterio

from iota2.common import otb_app_bank as otb
from iota2.common import raster_utils as ru
from iota2.common.compression_options import compression_options
from iota2.common.file_utils import file_search_and
from iota2.common.otb_app_bank import AvailableOTBApp
from iota2.common.raster_utils import (
    OtbPipelineCarrier,
    UserDefinedFunction,
    UserDefinedFunctionPayload,
    merge_rasters,
)
from iota2.typings.i2_types import (
    BoundariesProbaFusionParameters,
    BoundaryValidationParameters,
    PathLike,
)
from iota2.validation import confusion_fusion as cf
from iota2.validation import results_utils as rus

LOGGER = logging.getLogger("distributed.worker")


def take_fusion_decision(
    array: np.ndarray, num_class: int, all_regions: list[int], tile_regions: list[int]
) -> tuple[np.ndarray, list[None]]:
    """Compute the weights for probabilities fusion.

    Parameters
    ----------
    array:
        A numpy array containing all the weights for all regions and the proba.
    num_class:
        The number of classes i.e. the dimension of all probabilities vectors.
    all_regions:
        The number of all regions i.e. the number of probabilities maps.
    tile_regions:
        The list of regions involved in the tile.
    """
    acc = np.zeros((array.shape[0], array.shape[1], num_class))
    sum_weight = np.zeros((array.shape[0], array.shape[1], 1))
    for n_region, region in enumerate(tile_regions):
        # N first bands contains weights
        weight = array[:, :, all_regions.index(region)]
        weight = weight[:, :, np.newaxis]
        sum_weight += weight
        # after weights comes the probabilities per class
        proba = array[
            :,
            :,
            len(all_regions)
            + (n_region * num_class) : len(all_regions)
            + (n_region * num_class)
            + num_class,
        ]
        acc += weight * proba  # np.where(weight==0, 0, wei)
    # Apply a mask if the sum of proba is 0 (border of image)
    acc = np.where(sum_weight == 0, 0, acc / sum_weight)
    # Empty list is returned to match the format of insert_function_to_pipeline
    return acc, []


def proba_fusion_at_boundaries(
    distance_map_folder: str,
    probabilities_map_folder: str,
    boundary_folder_out: str,
    original_region_file: str,
    proba_fusion_params: BoundariesProbaFusionParameters,
) -> None:
    """Process the fusion at boundaries.

    Parameters
    ----------
    distance_map_folder:
        folder containing the distance maps
    probabilities_map_folder:
        folder containing probabilities maps
    boundary_folder_out:
        folder to store fusion result
    original_region_file:
        the region vector file
    proba_fusion_params:
        Parameters for the proba-map fusion at boundaries
    """
    # Concatenate distance and probabilities maps
    # Find distance map
    all_distances = file_search_and(
        distance_map_folder, True, proba_fusion_params.tile, "distance_map.tif"
    )

    # Find proba maps
    all_proba = file_search_and(
        probabilities_map_folder,
        True,
        "PROBAMAP",
        proba_fusion_params.tile,
        f"seed_{proba_fusion_params.seed}",
        ".tif",
    )
    all_proba.sort(key=lambda x: int(Path(x).name.split("_")[3]))
    tile_regions = [int(Path(x).name.split("_")[3]) for x in all_proba]
    gdf_reg = gpd.read_file(original_region_file)
    all_regions = gdf_reg[proba_fusion_params.region_field].unique()
    all_regions = [int(x) for x in all_regions]
    all_regions.sort()
    # check if all_proba len is no sup to all_distances bands ?

    tmp_res = str(Path("") / "concat.tif")
    concat, _ = otb.create_application(
        AvailableOTBApp.CONCATENATE_IMAGES,
        {"il": all_distances + all_proba, "out": tmp_res},
    )
    concat.Execute()
    partial_fun = partial(
        take_fusion_decision,
        num_class=proba_fusion_params.nb_class,
        all_regions=all_regions,
        tile_regions=tile_regions,
    )
    otb_pipelines_chunked = ru.pipelines_chunking(
        OtbPipelineCarrier(concat, concat, concat),
        chunk_config=ru.ChunkConfig(
            "split_number", proba_fusion_params.number_of_chunks, 0, 0
        ),
        targeted_chunk=proba_fusion_params.target_chunk,
    )
    (data, _) = ru.apply_udf_to_pipeline(
        otb_pipelines_chunked,
        function=UserDefinedFunction(partial_fun),
    )
    write_maps_at_boundaries(
        boundary_folder_out=boundary_folder_out,
        data=data,
        filename_suffix=f"boundary_{proba_fusion_params.tile}_seed_{proba_fusion_params.seed}_"
        f"{proba_fusion_params.target_chunk}.tif",
        generate_final_probability_map=proba_fusion_params.generate_final_probability_map,
    )


def write_maps_at_boundaries(
    boundary_folder_out: str,
    data: UserDefinedFunctionPayload,
    filename_suffix: str,
    generate_final_probability_map: bool,
) -> None:
    """
    Write probability map, confidence map and classification

    Parameters
    ----------
    boundary_folder_out:
        Folder where to store the rasters
    data:
        Object containing the numpy array with the required data
    generate_final_probability_map:
        Flag indicating whether to write the proba-map or not
    filename_suffix:
        Suffix of the filenames (containing tile, seed and chunk)
    """
    mosaic = data.data
    assert mosaic is not None
    spatial_meta = data.spatial_meta
    out_trans = ru.gdal_transform_to_rio(spatial_meta.gdal_geo_transform)
    epsg_code = spatial_meta.projection.GetAttrValue("AUTHORITY", 1)
    # mosaic contains the accumulated probability map
    # mosaic comes from rasterio then axis are inverted
    mask = np.sum(mosaic, axis=0)
    mask = mask[np.newaxis, :, :]
    mask = np.where(mask == 0, 0, 1)
    # confidence
    conf = np.max(mosaic, axis=0) / 10
    conf = conf[np.newaxis :, :]
    conf = np.where(mask == 0, 0, conf)
    # dec is the chosen class
    dec = np.argmax(mosaic, axis=0)
    dec = dec[np.newaxis, :, :]
    dec = np.where(mask == 0, 0, dec + 1)
    classif_path = str(Path(boundary_folder_out) / f"classif_{filename_suffix}")
    proba_path = str(Path(boundary_folder_out) / f"proba_{filename_suffix}")
    if proba_path and generate_final_probability_map:
        with rasterio.open(
            proba_path,
            "w",
            driver="GTiff",
            height=mosaic.shape[1],
            width=mosaic.shape[2],
            count=mosaic.shape[0],
            crs=f"EPSG:{epsg_code}",
            transform=out_trans,
            dtype="uint16",
        ) as dest:
            dest.write(np.where(mask == 0, 0, mosaic).astype("uint16"))
    if conf_path := str(Path(boundary_folder_out) / f"confidence_{filename_suffix}"):
        with rasterio.open(
            conf_path,
            "w",
            driver="GTiff",
            height=conf.shape[1],
            width=conf.shape[2],
            count=conf.shape[0],
            crs=f"EPSG:{epsg_code}",
            transform=out_trans,
            dtype="uint8",
        ) as dest:
            dest.write(conf.astype("uint8"))
    with rasterio.open(
        classif_path,
        "w",
        driver="GTiff",
        height=dec.shape[1],
        width=dec.shape[2],
        count=dec.shape[0],
        crs=f"EPSG:{epsg_code}",
        transform=out_trans,
        dtype="uint8",
    ) as dest:
        dest.write(dec.astype("uint8"))


def merge_map_and_boundary(
    tile: str,
    path_to_chunks: str,
    path_to_env: str,
    seed: int,
    working_directory: str | None = None,
) -> None:
    """Merge all boundary products.

    Parameters
    ----------
    tile:
        the tile name
    path_to_chunks:
        folder which contains chunks and where results are stored
    path_to_env:
        envelope folder
    seed:
        the targeted seed
    working_directory:
        temporary folder to store results
    """
    wdir = working_directory if working_directory is not None else path_to_chunks
    output_classif = f"Classif_{tile}_seed_{seed}.tif"
    # get all chunks for the boundary
    boundary_map = str(Path(wdir) / f"boundary_map_{tile}_seed_{seed}.tif")

    all_chunks = file_search_and(
        path_to_chunks, True, f"classif_boundary_{tile}", f"seed_{seed}", ".tif"
    )

    creation_options = {
        "COMPRESS": compression_options.algorithm,
        "PREDICTOR": compression_options.predictor,
        "NUM_THREADS": "ALL_CPUS",
        "BIGTIFF": "YES",
    }

    # merge them as one file
    merge_rasters(
        all_chunks, boundary_map, output_type="byte", creation_options=creation_options
    )

    # Apply classification map
    apply_classif_mask(boundary_map, output_classif, path_to_env, tile, wdir)

    # confidence
    # get all chunks for the boundary
    all_chunks_conf = file_search_and(
        path_to_chunks, True, f"confidence_boundary_{tile}", f"seed_{seed}", ".tif"
    )
    if all_chunks_conf:
        output_conf = str(Path(path_to_chunks) / f"{tile}_confidence_seed_{seed}.tif")
        # merge them as one file
        merge_rasters(
            all_chunks_conf,
            output_conf,
            output_type="uint16",
            creation_options=creation_options,
        )

    # Proba
    # get all chunks for the boundary
    all_chunks_proba = file_search_and(
        path_to_chunks, True, f"proba_boundary_{tile}", f"seed_{seed}", ".tif"
    )
    # merge them as one file
    if all_chunks_proba:
        output_proba = str(Path(path_to_chunks) / f"PROBAMAP_{tile}_seed_{seed}.tif")

        merge_rasters(
            all_chunks_proba,
            output_proba,
            output_type="uint16",
            creation_options=creation_options,
        )

    if working_directory:
        shutil.copy(
            str(Path(wdir) / output_classif),
            str(Path(path_to_chunks) / output_classif),
        )


def apply_classif_mask(
    boundary_map: str, output_classif: str, path_to_env: str, tile: str, wdir: str
) -> None:
    """
    Apply classification mask to boundary map

    Parameters
    ----------
    boundary_map:
        Path to boundary map raster
    output_classif:
        Name of the output classif raster
    path_to_env:
        Path to envelope folder
    tile:
        Tile name
    wdir:
        Working dir
    """
    mask = str(Path(path_to_env) / f"{tile}.tif")
    bandmath, _ = otb.create_application(
        AvailableOTBApp.BAND_MATH,
        {
            "il": [boundary_map, mask],
            "exp": "im2b1==0?0:im1b1",
            "out": str(Path(wdir) / output_classif),
            "pixType": "uint8",
        },
    )
    bandmath.ExecuteAndWriteOutput()


def prepare_boundary_validation_dataset(
    iota2_directory: PathLike,
    validation_parameters: BoundaryValidationParameters,
    classifications: tuple[str, str],
    working_directory: str | None = None,
) -> None:
    """Prepare the validation dataset for comparison.

    Parameters
    ----------
    iota2_directory:
        Output iota2 folder
    validation_parameters:
        Object containing validation parameters
    classifications:
        Classifications to compare
    working_directory:
        temporary folder to store intermediate results
    """
    output_path = str(Path(iota2_directory) / "final" / "TMP")
    wdir = working_directory if working_directory is not None else output_path
    # Get the boundary area
    boundary_area = str(
        Path(iota2_directory)
        / "features"
        / validation_parameters.tile
        / "tmp"
        / f"{validation_parameters.tile}_buffer_area.shp"
    )

    # Get the validation shape file
    ref_shape = str(
        Path(iota2_directory)
        / "formattingVectors"
        / f"{validation_parameters.tile}.shp"
    )
    gdf_ref = gpd.read_file(ref_shape)
    gdf_boundary = gpd.read_file(boundary_area)
    val_boundary = str(
        Path(iota2_directory)
        / "final"
        / "TMP"
        / f"{validation_parameters.tile}_validation_boundary.shp"
    )
    # compute intersection with geopandas to prevent empty intersection
    # ogr can produce empty file which leads to segfault with OTB
    if not Path(val_boundary).exists():
        res_intersection = gpd.overlay(gdf_ref, gdf_boundary, how="difference")
        res_intersection = res_intersection.loc[
            res_intersection[f"seed_{validation_parameters.seed}"] == "validation"
        ]
        res_intersection = res_intersection.drop("originfid", axis="columns")
        if res_intersection.empty:
            print("empty")
            return None
        res_intersection.to_file(val_boundary)

    # From polygons extract values
    extract_samples(
        classifications, iota2_directory, validation_parameters, val_boundary, wdir
    )
    return None


def extract_samples(
    classifications: tuple[str, str],
    iota2_directory: PathLike,
    validation_parameters: BoundaryValidationParameters,
    val_boundary: PathLike,
    wdir: PathLike,
) -> None:
    """
    Launch a pipeline with a polygon class statistics, sample selection and sample extraction apps
    to extract points.

    Parameters
    ----------
    classifications:
        Tuple of paths to the classifications models
    iota2_directory:
        Path to iota2 output directory
    val_boundary:
        Path to the boundary validation shapefile
    validation_parameters:
        Object containing validation parameters
    wdir:
        Working directory
    """
    extract_points = (
        f"Boundary_Validation_points_{validation_parameters.tile}_"
        f"{validation_parameters.seed}.gpkg"
    )
    extract_labels = ["cl_1", "cl_2"]
    output_name = str(Path(iota2_directory) / "final" / "TMP" / extract_points)
    stats_file = str(
        Path(wdir) / f"{validation_parameters.tile}_validation_boundary_stats.xml"
    )
    stats, _ = otb.create_application(
        AvailableOTBApp.POLYGON_CLASS_STATISTICS,
        {
            "in": classifications[0],
            "out": stats_file,
            "vec": val_boundary,
            "field": validation_parameters.data_field.lower(),
        },
    )
    stats.ExecuteAndWriteOutput()
    select_file = str(
        Path(wdir) / f"{validation_parameters.tile}_validation_boundary_selection.shp"
    )
    select, _ = otb.create_application(
        AvailableOTBApp.SAMPLE_SELECTION,
        {
            "in": classifications[0],
            "out": select_file,
            "vec": val_boundary,
            "instats": stats_file,
            "sampler": "periodic",
            "strategy": "all",
            "field": validation_parameters.data_field.lower(),
        },
    )
    select.ExecuteAndWriteOutput()
    concat, _ = otb.create_application(
        AvailableOTBApp.CONCATENATE_IMAGES,
        {"il": [classifications[0], classifications[1]], "out": "fake_out.tif"},
    )
    concat.Execute()
    extract, _ = otb.create_application(
        AvailableOTBApp.SAMPLE_EXTRACTION,
        {
            "in": concat,
            "out": output_name,
            "vec": select_file,
            "field": validation_parameters.data_field,
            "outfield": "list",
            "outfield.list.names": extract_labels,
        },
    )
    extract.ExecuteAndWriteOutput()


def merge_metrics_matrices(
    iota2_directory: str,
    data_field: str,
    seeds: int,
    nomenclature_path: str,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Compute conditional matrices for boundary comparison.

    Parameters
    ----------
    iota2_directory:
        iota2 output folder
    data_field:
        label column name in reference data
    seeds:
        the number of seeds
    nomenclature_path:
        nomenclature file
    logger:
        logger
    """
    output_files: dict[str, list[str]] = {
        "conditional_bound": [],
        "conditional_standard": [],
        "bound": [],
        "standard": [],
    }

    for seed in range(seeds):
        all_validation_points = file_search_and(
            str(Path(iota2_directory) / "final" / "TMP"),
            True,
            "Boundary_Validation_points_",
            f"{seed}.gpkg",
        )
        points_valid = gpd.GeoDataFrame(
            pd.concat(
                [gpd.read_file(i) for i in all_validation_points], ignore_index=True
            ),
            crs=gpd.read_file(all_validation_points[0]).crs,
        )
        if points_valid.empty:
            print(
                f"No valid points in {all_validation_points}."
                "Matrices can't be computed"
            )
            logger.info(
                f"No valid points in {all_validation_points}."
                "Matrices can't be computed"
            )
            continue
        output_path = str(Path(iota2_directory) / "final")

        # conf(M1|Y=Yref, M2)
        output_name = str(
            Path(output_path) / "Confusion_conditional_classification_"
            f"boundary_seed_{seed}.csv"
        )
        df_r1_true = points_valid[points_valid[data_field] == points_valid["cl_1"]]
        if df_r1_true.empty:
            logger.info("No valid points are found for %s", output_name)
            print(f"No valid points are found for {output_name}")
        else:
            compute_metrics_with_ref(
                df_r1_true, output_name, ref_field=data_field, target_field="cl_2"
            )
            rus.stats_report(
                [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
            )
            output_files["conditional_bound"].append(output_name)
        # conf(M2|Y=Yref, M1)
        output_name = str(
            Path(output_path) / "Confusion_conditional_classification_"
            f"standard_seed_{seed}.csv"
        )
        df_r2_true = points_valid[points_valid[data_field] == points_valid["cl_2"]]
        if df_r2_true.empty:
            logger.info("No valid points are found for %s", output_name)
            print(f"No valid points are found for {output_name}")
        else:
            compute_metrics_with_ref(
                df_r2_true, output_name, ref_field=data_field, target_field="cl_1"
            )
            rus.stats_report(
                [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
            )
            output_files["conditional_standard"].append(output_name)
        # conf(M1,Yref)
        output_name = str(
            Path(output_path) / "Confusion_classification_standard" f"_seed_{seed}.csv"
        )
        cross_tab = compute_metrics_with_ref(
            points_valid, output_name, ref_field=data_field, target_field="cl_1"
        )
        if cross_tab.empty:
            logger.info("Empty validation set %s", output_name)
        else:
            rus.stats_report(
                [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
            )
            output_files["standard"].append(output_name)
        # conf(M2,Yref)
        output_name = str(
            Path(output_path) / "Confusion_classification_boundary" f"_seed_{seed}.csv"
        )
        cross_tab = compute_metrics_with_ref(
            points_valid, output_name, ref_field=data_field, target_field="cl_2"
        )
        if cross_tab.empty:
            logger.info("Empty validation set %s", output_name)
        else:
            rus.stats_report(
                [output_name], nomenclature_path, output_name.replace(".csv", ".txt")
            )
            output_files["bound"].append(output_name)

    if output_files["conditional_standard"]:
        rus.stats_report(
            output_files["conditional_standard"],
            nomenclature_path,
            str(
                Path(output_path) / "Confusion_conditional_classification_standard.txt"
            ),
        )
        rus.stats_report(
            output_files["standard"],
            nomenclature_path,
            str(Path(output_path) / "Confusion_classification_standard.txt"),
        )
    if output_files["conditional_bound"]:
        rus.stats_report(
            output_files["conditional_bound"],
            nomenclature_path,
            str(
                Path(output_path) / "Confusion_conditional_classification_boundary.txt"
            ),
        )
        rus.stats_report(
            output_files["bound"],
            nomenclature_path,
            str(Path(output_path) / "Confusion_classification_boundary.txt"),
        )


def compute_metrics_with_ref(
    points_valid: pd.DataFrame, output_path: str, ref_field: str, target_field: str
) -> pd.DataFrame:
    """Extract labels from a dataframe and create confusion matrix.

    Parameters
    ----------
    points_valid:
        The dataframe containing labels.
    output_path:
        Name of the output csv file
    ref_field:
        The reference field name
    target_field:
        The target field name
    """
    cross_tab = pd.crosstab(
        points_valid[ref_field].astype(int), points_valid[target_field].astype(int)
    )
    ref_lab = [int(x) for x in points_valid[ref_field].unique()]
    pred_lab = [int(x) for x in points_valid[target_field].unique()]
    all_labels = list(set(ref_lab + pred_lab))
    dict_tab = cross_tab.to_dict()
    # dict_tab exact type can't be inferred from pandas
    convert_dict_to_confusion_matrix(all_labels, dict_tab, output_path)  # type: ignore
    return cross_tab


def convert_dict_to_confusion_matrix(
    all_labels: list[int], dict_tab: dict[int, dict], output_name: str
) -> None:
    """Convert a dictionary to a confusion matrix array.

    Parameters
    ----------
    all_labels:
        The list of all labels in the nomenclature
    dict_tab:
        A dictionary containing the confusion matrix
    output_name:
        The name of the output csv
    """
    matrix = np.zeros((len(all_labels), len(all_labels)))
    # Note that labels are already decoded in both maps
    all_labels_index = range(1, len(all_labels) + 1)
    for row_index in all_labels_index:  # row
        for col_index in all_labels_index:  # cols
            if row_index in dict_tab.keys():
                ss_dict = dict_tab[row_index]
                if col_index in ss_dict.keys():
                    matrix[col_index - 1, row_index - 1] += ss_dict[col_index]
    cf.write_csv(matrix, all_labels, output_name)
