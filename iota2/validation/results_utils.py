# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
utility functions for report generation
"""

import csv
import logging
import re
from dataclasses import dataclass
from decimal import Decimal
from pathlib import Path
from typing import Any, Literal

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import gridspec
from matplotlib.colors import LinearSegmentedColormap
from scipy import stats

from iota2.common import file_utils as fu
from iota2.typings.i2_types import (
    ClassifScores,
    ConfMatrixImageParameters,
    ConfMatrixParameters,
    ConfusionMatrixT,
    FileObject,
    LabelsConversionDict,
    MultipleClassifScores,
    PathLike,
    SampleSelectionConcatParams,
)

LOGGER = logging.getLogger("distributed.worker")


@dataclass
class LabelsParameters:
    """
    Attributes
    ----------
    nom_dict:
        Nomenclature dictionary
    labels_table:
        Labels conversion table
    """

    nom_dict: dict[Any, str]
    labels_table: LabelsConversionDict | None = None


def remove_indecidedlabel(
    conf_mat_dic: ConfusionMatrixT,
    indecidedlabel: int,
) -> ConfusionMatrixT:
    """
    Remove samples with the indecidedlabel label from the confusion matrix.

    Parameters
    ----------
    conf_mat_dic : input confusion as dictionary
    indecidedlabel : label representing indecisions
    """
    # remove prod labels
    for _, prod_dict in list(conf_mat_dic.items()):
        prod_dict.pop(indecidedlabel, None)

    # remove ref labels
    conf_mat_dic.pop(indecidedlabel, None)

    return conf_mat_dic


def initialize_confusion_matrix(
    r_labels: list[int], p_labels: list[int], init_value: Any
) -> ConfusionMatrixT:
    """
    Initialize a confusion matrix with an init value

    Mutable objects are allowed, a copy is used as init value

    Parameters
    ----------
    r_labels
        ref labels
    p_labels
        produced labels
    init_value
        initial value
    """
    matrix: ConfusionMatrixT = {}
    for ref_label in r_labels:
        matrix[ref_label] = {}
        for prod_label in p_labels:
            if hasattr(init_value, "copy") and callable(getattr(init_value, "copy")):
                matrix[ref_label][prod_label] = init_value.copy()
            else:
                matrix[ref_label][prod_label] = init_value
    return matrix


def parse_csv(csv_in: PathLike) -> ConfusionMatrixT:
    """
    Parse OTB's confusion matrix.

    Parameters
    ----------
    csv_in : string
        path to a csv file representing a confusion matrix

    Example
    -------
    if csv_in contains :

    >>> #Reference labels (rows):11,12,221,222
    >>> #Produced labels (columns):11,12,221,222,255
    >>> 1,2,4,7,0
    >>> 8,9,10,30,0
    >>> 27,24,16,28,0
    >>> 98,9,0,1,0

    >>> matrix = parse_csv(csv_in)
    >>> print matrix[11][221]
    >>> 4
    >>> print matrix[11][222]
    >>> 7
    >>> print matrix[255]
    >>> dict([(11, 0), (12, 0),..., (255, 0)])

    Return
    ------
    {}
        confusion matrix
    """
    with open(csv_in, encoding="utf-8") as csvfile:
        csv_reader = csv.reader(csvfile)
        ref_lab = [
            elem.replace("#Reference labels (rows):", "") for elem in next(csv_reader)
        ]
        prod_lab = [
            elem.replace("#Produced labels (columns):", "") for elem in next(csv_reader)
        ]
        all_labels = sorted([int(label) for label in list(set(ref_lab + prod_lab))])
        matrix: ConfusionMatrixT = initialize_confusion_matrix(
            all_labels, all_labels, 0.0
        )
        # fill-up confusion matrix
        csv_dict = csv.DictReader(csvfile, fieldnames=prod_lab)
        for row_num, row_ref in enumerate(csv_dict):
            for klass, value in list(row_ref.items()):
                ref = int(ref_lab[row_num])
                prod = int(klass)
                matrix[ref][prod] += float(value)
    return matrix


def get_coeff(
    matrix: ConfusionMatrixT,
) -> ClassifScores:
    """
    Extract coefficients (OA, K, Precision, Recall, F-Score) from a confusion matrix and pass them
    into a dataclass.
    Precision, Recall, F-Score are dictionaries.

    Parameters
    ----------
    matrix : dict
        a confusion matrix stored in dictionnaries

    Example
    -------
        >>> conf_mat_dic = dict([(1, dict([(1, 50), (2, 78), (3, 41)])),
        >>>                             (2, dict([(1, 20), (2, 52), (3, 31)])),
        >>>                             (3, dict([(1, 27), (2, 72), (3, 98)]))])
        >>> scores = get_coeff(conf_mat_dic)
        >>> print scores.precision[1]
        >>> 0.5154639175257731
    """
    nan = 0
    classes_labels = list(matrix.keys())

    output_scores = ClassifScores(
        kappa=0,
        oacc=0,
        precision={},
        recall={},
        fscore={},
    )

    oacc_nom = sum(matrix[class_name][class_name] for class_name in matrix.keys())
    nb_samples = sum(
        matrix[ref_class_name][prod_class_name]
        for ref_class_name in matrix.keys()
        for prod_class_name in matrix.keys()
    )

    # compute overall accuracy
    if nb_samples != 0:
        output_scores.oacc = float(oacc_nom) / float(nb_samples)
    else:
        output_scores.oacc = nan

    lucky_rate = 0.0
    for class_label in classes_labels:
        p_denom, r_denom = compute_scores(
            class_label, classes_labels, matrix, nan, output_scores
        )
        lucky_rate += p_denom * r_denom

    k_denom = float((nb_samples * nb_samples) - lucky_rate)
    k_nom = float((output_scores.oacc * nb_samples * nb_samples) - lucky_rate)
    if k_denom != 0.0:
        output_scores.kappa = k_nom / k_denom
    else:
        output_scores.kappa = nan

    return output_scores


def compute_scores(
    class_label: int,
    classes_labels: list[int],
    matrix: ConfusionMatrixT,
    nan: float,
    output_scores: ClassifScores,
) -> tuple[int, int]:
    """
    Compute and update scores (precision, recall and fscore) for a given class

    Parameters
    ----------
    class_label:
        class label
    classes_labels:
        list of all classes names
    matrix:
        confusion matrix
    nan:
        default nan value
    output_scores:
        all output scores (stored in a ClassifScores dataclass)
    """
    oacc_class = matrix[class_label][class_label]
    p_denom = sum(matrix[ref][class_label] for ref in classes_labels)
    r_denom = sum(matrix[class_label][ref] for ref in classes_labels)
    if float(p_denom) != 0.0:
        p_class = float(oacc_class) / float(p_denom)
    else:
        p_class = nan
    if float(r_denom) != 0.0:
        r_class = float(oacc_class) / float(r_denom)
    else:
        r_class = nan
    if float(p_class + r_class) != 0.0:
        f_class = float(2.0 * p_class * r_class) / float(p_class + r_class)
    else:
        f_class = nan
    output_scores.precision[class_label] = p_class
    output_scores.recall[class_label] = r_class
    output_scores.fscore[class_label] = f_class
    return p_denom, r_denom


def get_color_table(color_file: str) -> dict[str, tuple[int, int, int]]:
    """Parse color file.

    Parameters
    ----------
    color_file : string
        color file path

    Return
    ------
    dict
        dict[label] = (R, G, B)
    """
    color_dict = {}
    with open(color_file, encoding="utf-8") as filein:
        for line in filein:
            entry = line
            if not entry.rstrip():
                continue
            rgb_colors = re.findall(r"\d+", entry)
            rgb_values = (
                rgb_colors[-3],
                rgb_colors[-2],
                rgb_colors[-1],
            )
            class_id = entry.rstrip().replace(" ".join(rgb_values), "").strip()
            rgb_values_as_ints = (
                int(rgb_values[0]),
                int(rgb_values[1]),
                int(rgb_values[2]),
            )
            color_dict[class_id] = rgb_values_as_ints
    return color_dict


def get_nomenclature(nom_path: PathLike) -> dict[Any, str]:
    """Parse nomenclature file.

    Parameters
    ----------
    nom_path : string
        nomenclature file path

    Return
    ------
    dict
        dict[1] = "className"
    """
    nom_dict = {}
    with open(nom_path, encoding="utf-8") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=":")
        for entry in csv_reader:
            if not entry:
                continue
            class_name, label = entry
            try:
                # if castable in int, remove whitespaces
                nom_dict[str(int(label))] = class_name
            except ValueError:
                nom_dict[label] = class_name
    return nom_dict


def get_rgb_mat(
    norm_conf: np.ndarray,
    diag_cmap: LinearSegmentedColormap,
    not_diag_cmap: LinearSegmentedColormap,
) -> list[list[tuple]]:
    """Convert normalize confusion matrix to a RGB image."""
    rgb_list = []
    for row_num, row in enumerate(norm_conf):
        rgb_list_row = []
        for col_num, col in enumerate(row):
            if col_num == row_num:
                rgb_list_row.append(diag_cmap(col))
            else:
                rgb_list_row.append(not_diag_cmap(col))
        rgb_list.append(rgb_list_row)
    return rgb_list


def get_rgb_pre(
    pre_val: np.ndarray, coeff_cmap: LinearSegmentedColormap
) -> list[list[tuple]]:
    """Convert values to an RGB code thanks to a colormap."""
    rgb_list = []
    for raw in pre_val:
        raw_rgb = []
        for val in raw:
            raw_rgb.append(coeff_cmap(val))
        rgb_list.append(raw_rgb)
    return rgb_list


def get_rgb_rec(
    coeff: np.ndarray, coeff_cmap: LinearSegmentedColormap
) -> list[list[tuple]]:
    """Convert normalized confusion matrix to an RGB image."""
    rgb_list = []
    for raw in coeff:
        raw_rgb = []
        for val in raw:
            if val != 0.0:
                raw_rgb.append(coeff_cmap(val))
            else:
                raw_rgb.append((0, 0, 0, 0))
        rgb_list.append(raw_rgb)
    return rgb_list


def normalize_conf(conf_mat_array: np.ndarray, norm: str = "ref") -> np.ndarray:
    """
    Normalize a confusion matrix with ref as row and production in column.

    Parameters
    ----------
    conf_mat_array : numpy.array
        confusion matrix as numpy array
    norm : string
        define how to normalize the confusion matrice by row or columns
        respectively "ref" and "prod"

    Return
    ------
    numpy array
        a normalized confusion matrix
    """
    nan = -1
    if norm.lower() == "prod":
        conf_mat_array = np.transpose(conf_mat_array)
    norm_conf_tmp = []
    for row in conf_mat_array:
        raw_sum = 0
        tmp_arr = []
        raw_sum = sum(row, 0)
        for cell in row:
            if float(raw_sum) != 0:
                tmp_arr.append(float(cell) / float(raw_sum))
            else:
                tmp_arr.append(nan)
        norm_conf_tmp.append(tmp_arr)
    norm_conf: np.ndarray = np.array(norm_conf_tmp)
    if norm.lower() == "prod":
        norm_conf = np.transpose(norm_conf)
    return norm_conf


def labels_from_confusion_mat(
    conf_mat: dict, nom_dict: dict[Any, str]
) -> tuple[list[str], list[str], int]:
    """Guess labels (ref+prod) by reading the confusion matrix."""
    labels_ref = [nom_dict[str(lab)] for lab in list(conf_mat.keys())]
    labels_prod = [
        nom_dict[str(lab)] for lab in list(conf_mat[list(conf_mat.keys())[0]].keys())
    ]
    nb_labels = len(set(labels_prod + labels_ref))
    return labels_ref, labels_prod, nb_labels


def conf_score_to_fig(
    norm_conf: np.ndarray,
    conf_mat_array: np.ndarray,
    conf_score: Literal["count", "count_sci"],
    threshold: float,
    axe: plt.Axes,
) -> None:
    """Add inplace confusion score to a matplot lib ax."""
    width, height = norm_conf.shape
    val_to_print = ""
    for x_coord in range(width):
        for y_coord in range(height):
            val_percentage = norm_conf[x_coord][y_coord] * 100.0
            if conf_score.lower() == "count":
                val_to_print = str(int(conf_mat_array[x_coord][y_coord]))
            elif conf_score.lower() == "count_sci":
                conf_value = conf_mat_array[x_coord][y_coord]
                if conf_value > 999.0:
                    val_to_print = f"{Decimal(float(conf_value)):.2E}"
                else:
                    val_to_print = str(int(conf_value))
            elif conf_score.lower() == "percentage":
                val_to_print = f"{val_percentage:.1f}%"
            if val_percentage <= float(threshold):
                val_to_print = ""
            axe.annotate(
                val_to_print,
                xy=(y_coord, x_coord),
                horizontalalignment="center",
                verticalalignment="center",
                fontsize="xx-small",
                rotation=45,
            )


def precision_to_fig(
    precision_dic: dict,
    color_map: LinearSegmentedColormap,
    nb_prod_labels: int,
    fig_ax: plt.Axes,
) -> None:
    """Add precisions scores to confusion figure."""

    pre_val_tmp = [p_val for _, p_val in list(precision_dic.items())]
    pre_val = np.array([pre_val_tmp, pre_val_tmp])

    pre_val_rgb = get_rgb_pre(pre_val, color_map)
    fig_ax.imshow(pre_val_rgb, interpolation="none", alpha=0.8, aspect="auto")
    fig_ax.set_ylim(0.5, 1.5)
    fig_ax.get_yaxis().set_visible(False)

    for x_coord in range(nb_prod_labels):
        fig_ax.annotate(
            f"{pre_val[0][x_coord]:.3f}",
            xy=(x_coord, 1),
            horizontalalignment="center",
            verticalalignment="center",
            fontsize="xx-small",
        )


def recall_to_fig(
    recall_dict: dict,
    nb_labels: int,
    color_map: LinearSegmentedColormap,
    axis: plt.Axes,
) -> None:
    """Add recall scores to confusion figure."""
    rec_val = np.array([[0, r_val] for _, r_val in list(recall_dict.items())])

    rec_val_rgb = get_rgb_rec(rec_val, color_map)
    axis.imshow(rec_val_rgb, interpolation="nearest", alpha=0.8, aspect="auto")

    axis.set_xlim(0.5, 1.5)
    axis.set_title("Rappel", rotation=90, verticalalignment="bottom")
    axis.get_yaxis().set_visible(False)
    axis.get_xaxis().set_visible(False)

    for y_coord in range(nb_labels):
        axis.annotate(
            f"{rec_val[y_coord][1]:.3f}",
            xy=(1, y_coord),
            horizontalalignment="center",
            verticalalignment="center",
            fontsize="xx-small",
        )


def fscore_to_fig(
    fscore_dict: dict,
    nb_labels: int,
    color_map: LinearSegmentedColormap,
    plt_ax: plt.Axes,
) -> None:
    """Add f-score to confusion figure."""
    fs_val = np.array([[0, f_val] for _, f_val in list(fscore_dict.items())])
    fs_val_rgb = get_rgb_rec(fs_val, color_map)
    plt_ax.imshow(fs_val_rgb, interpolation="none", alpha=0.8, aspect="auto")
    plt_ax.set_xlim(0.5, 1.5)
    plt_ax.set_title("F-Score", rotation=90, verticalalignment="bottom")
    plt_ax.get_yaxis().set_visible(False)
    plt_ax.get_xaxis().set_visible(False)

    for y_coord in range(nb_labels):
        plt_ax.annotate(
            f"{fs_val[y_coord][1]:.3f}",
            xy=(1, y_coord),
            horizontalalignment="center",
            verticalalignment="center",
            fontsize="xx-small",
        )


def setup_conf_matrix_plot(
    image_parameters: ConfMatrixImageParameters,
    labels_prod: list[str],
    labels_ref: list[str],
    nb_labels: int,
) -> list[plt.Axes]:
    """
    Set up the confusion matrix plot

    Parameters
    ----------
    image_parameters
        Object containing the output image parameters
    labels_prod
        Produced labels
    labels_ref
        Reference labels
    nb_labels
        Total number of labels
    """
    fig = plt.figure(figsize=(nb_labels / 2, nb_labels / 2))
    grid_s = gridspec.GridSpec(
        3,
        3,
        width_ratios=[1, 1.0 / len(labels_ref), 1.0 / len(labels_ref)],
        height_ratios=[1, 1.0 / len(labels_prod), 1.0 / len(labels_prod)],
    )
    grid_s.update(wspace=0.1, hspace=0.1)
    plt.clf()
    axs = [fig.add_subplot(grid_s[i]) for i in range(4)]
    for axis in axs:
        axis.set_aspect(1)
        axis.xaxis.tick_top()
        axis.xaxis.set_label_position("top")
        # Confusion matrix grid
        if image_parameters.grid_conf:
            axis.set_xticks(np.arange(-0.5, len(labels_prod), 1), minor=True)
            axis.set_yticks(np.arange(-0.5, len(labels_ref), 1), minor=True)
            axis.grid(
                which="minor", color="gray", linestyle="-", linewidth=1, alpha=0.5
            )
    return axs


def fig_conf_mat(
    conf_mat_dic: dict,
    nom_dict: dict[Any, str],
    scores: ClassifScores,
    out_png: PathLike,
    image_parameters: ConfMatrixImageParameters,
) -> None:
    """Generate a figure representing the confusion matrix.

    Parameters
    ----------
    conf_mat_dic : {}
        confusion matrix
    nom_dict : dict
        nomenclature dictionary
    scores: ClassifScores
        dataclass containing all scores (kappa, overall accuracy, precision, recall, fscore)
    image_parameters:
        object containing parameters for generating the confusion matrix image
    """
    matplotlib.get_backend()
    matplotlib.use("Agg")

    labels_ref, labels_prod, nb_labels = labels_from_confusion_mat(
        conf_mat_dic, nom_dict
    )

    # convert conf_mat_dic to a list of lists
    conf_mat_array = np.array(
        [
            [v for __, v in list(prod_dict.items())]
            for _, prod_dict in list(conf_mat_dic.items())
        ]
    )

    # normalize by ref samples
    norm_conf = normalize_conf(conf_mat_array, norm=image_parameters.point_of_view)
    rgb_matrix = get_rgb_mat(
        norm_conf, plt.cm.RdYlGn, plt.cm.Reds  # pylint: disable=no-member
    )

    axs = setup_conf_matrix_plot(image_parameters, labels_prod, labels_ref, nb_labels)

    axs[0].imshow(rgb_matrix, interpolation="nearest", alpha=0.8, aspect="auto")

    if image_parameters.write_conf_score:
        conf_score_to_fig(
            norm_conf,
            conf_mat_array,
            image_parameters.conf_score,
            image_parameters.threshold,
            axs[0],
        )

    axs[0].set_title(f"KAPPA : {scores.kappa:.3f} OA : {scores.oacc:.3f}")
    axs[0].set_xticks(list(range(norm_conf.shape[0])))  # width
    axs[0].set_xticklabels(labels_prod, rotation=90)
    axs[0].set_yticks(list(range(norm_conf.shape[1])))  # height
    axs[0].set_yticklabels(labels_ref)

    color_map_coeff = plt.cm.RdYlGn  # pylint: disable=no-member
    # Recall
    recall_to_fig(scores.recall, len(labels_ref), color_map_coeff, axs[1])

    # Precision
    precision_to_fig(scores.precision, color_map_coeff, len(labels_prod), axs[3])
    axs[3].set_xlabel("Precision")
    axs[3].set_xticklabels([])
    axs[3].xaxis.set_label_position("bottom")
    axs[3].set_xticks([])
    # axs[3].xaxis.tick_bottom()

    # F-score
    fscore_to_fig(scores.fscore, len(labels_ref), color_map_coeff, axs[2])

    plt.savefig(
        out_png,
        format="png",
        dpi=image_parameters.dpi,
        bbox_inches="tight",
    )


def merge_class_matrix(
    conf_mat_dic: ConfusionMatrixT, merge_class: list[dict]
) -> ConfusionMatrixT:
    """Merge class in confusion matrix.

    Parameters
    ----------
    conf_mat_dic : dict
        a confusion matrix (index represent row reference)
    merge_class : list
        list of dictionaries.
        example :
            merge_class = [{"src_label":[1,2,3],
                            "dst_label": 50,
                            "dst_name" : "URBAN"},
                           {...}]
    """
    all_labels, dico_match = setup_matrix_merge(conf_mat_dic, merge_class)

    # init an empty matrix with output labels
    matrix: ConfusionMatrixT = {}
    for lab_ref in all_labels:
        matrix[lab_ref] = {}
        for lab_prod in all_labels:
            matrix[lab_ref][lab_prod] = 0

    # fill-up output merged matrix
    for ref_label, prod_dict in list(conf_mat_dic.items()):
        for prod_label, prod_val in list(prod_dict.items()):
            matrix[dico_match[ref_label]][dico_match[prod_label]] += prod_val
    return matrix


def setup_matrix_merge(
    conf_mat_dic: ConfusionMatrixT, merge_class: list[dict]
) -> tuple[list[int], dict[int, int]]:
    """
    Prepare the labels for merging matrices

    Parameters
    ----------
    conf_mat_dic
        a confusion matrix (index represent row reference)
    merge_class
        list of dictionaries
    """
    labels_ref = list(conf_mat_dic.keys())
    labels_prod = list(list(conf_mat_dic[labels_ref[0]].keys()))
    all_labels = list(set(labels_ref + labels_prod))
    for merge_rule in merge_class:
        # add new label
        all_labels.append(merge_rule["dst_label"])
        # remove old labels
        for sub_label in merge_rule["src_label"]:
            all_labels.remove(sub_label)
    all_labels.sort()
    # matching label dictionary
    input_labels = list(set(labels_ref + labels_prod))
    dico_match = {}
    for input_label in input_labels:
        dico_match[input_label] = input_label
        for merge_rule in merge_class:
            if input_label in merge_rule["src_label"]:
                dico_match[input_label] = merge_rule["dst_label"]
    return all_labels, dico_match


def gen_confusion_matrix_fig(
    csv_in: str,
    nomenclature_path: PathLike,
    out_png: PathLike,
    conf_matrix_image_parameters: ConfMatrixImageParameters = ConfMatrixImageParameters(),
    conf_matrix_parameters: ConfMatrixParameters = ConfMatrixParameters(),
) -> None:
    """Generate confusion matrix as a matplotlib figure.

    Parameters
    ----------
    csv_in : string
        path to a csv confusion matrix (OTB's computeConfusionMatrix output)
    nomenclature_path : string
        path to the file which describe the nomenclature
    conf_matrix_parameters:
        Parameters concerning the production of the confusion matrix
    conf_matrix_image_parameters:
        Parameters concerning the production of the confusion matrix's resulting image
    """
    conf_mat_dic = parse_csv(csv_in)
    if conf_matrix_parameters.labels_table:
        conf_mat_dic_tmp = {}
        for row, col_dict in conf_mat_dic.items():
            col_val_tmp = {}
            for col, col_val in col_dict.items():
                col_val_tmp[conf_matrix_parameters.labels_table[col]] = col_val
            conf_mat_dic_tmp[conf_matrix_parameters.labels_table[row]] = col_val_tmp
        conf_mat_dic = conf_mat_dic_tmp

    nom_dict = get_nomenclature(nomenclature_path)

    if conf_matrix_parameters.merge_class:
        conf_mat_dic = merge_class_matrix(
            conf_mat_dic, conf_matrix_parameters.merge_class
        )
        for merge_rule in conf_matrix_parameters.merge_class:
            nom_dict[merge_rule["dst_label"]] = merge_rule["dst_name"]
    # remove indecided label
    if conf_matrix_parameters.indecidedlabel:
        conf_mat_dic = remove_indecidedlabel(
            conf_mat_dic, conf_matrix_parameters.indecidedlabel
        )

    conf_mat_dic_order = conf_mat_dic

    # re-order classes
    if conf_matrix_parameters.class_order:
        conf_mat_dic_order = reorder_classes(
            conf_matrix_parameters.class_order,
            conf_mat_dic,
            conf_mat_dic_order,
            conf_matrix_parameters.merge_class,
        )

    scores = get_coeff(conf_mat_dic_order)

    fig_conf_mat(
        conf_mat_dic_order,
        nom_dict,
        scores,
        out_png,
        conf_matrix_image_parameters,
    )


def reorder_classes(
    class_order: list[int],
    conf_mat_dic: ConfusionMatrixT,
    conf_mat_dic_order: ConfusionMatrixT,
    merge_class: list[dict] | None,
) -> ConfusionMatrixT:
    """
    Reorder the classes in the confusion matrix dictionary based on the specified class order

    Parameters
    ----------
    class_order:
        List of labels specifying the desired order
    conf_mat_dic:
        Original confusion matrix dictionary
    conf_mat_dic_order:
        Confusion matrix dictionary with classes reordered
    merge_class : list
        list of dictionary.
        example :
            merge_class = [{"src_label": [1,2,3],
                            "dst_label": 50,
                            "dst_name" : "URBAN"},
                           {...}]
    """
    labels_ref = list(conf_mat_dic.keys())
    labels_prod = list(list(conf_mat_dic[labels_ref[0]].keys()))
    all_labels = labels_ref + labels_prod
    # check if input's labels correspond to labels detected in csv file
    check_in_order = all(label_in in all_labels for label_in in class_order)
    check_in_csv = all(label_csv in class_order for label_csv in all_labels)

    if not merge_class:
        if not check_in_order:
            raise ValueError(
                "'Class_order' parameter contains classes that are not present in input csv file"
            )
        if not check_in_csv:
            raise ValueError("Missing classes in 'class_order' parameter")

    conf_mat_dic_order = {}
    for class_number_ref in class_order:
        conf_mat_dic_prod = {}
        for class_number_prod in class_order:
            conf_mat_dic_prod[class_number_prod] = conf_mat_dic[class_number_ref][
                class_number_prod
            ]
        conf_mat_dic_order[class_number_ref] = conf_mat_dic_prod
    return conf_mat_dic_order


def get_labels(
    conf_mat_dic: ConfusionMatrixT, nom_dict: dict[Any, str]
) -> tuple[list[str], list[str]]:
    """Return the maximum len of all labels.

    Parameters
    ----------
    conf_mat_dic : dict
        format example in :py:meth:`get_coeff`
    nom_dict : dict
        nomenclature dictionary

    Return
    ------
    int
        maximum len of all labels
    """
    labels_ref = [nom_dict[str(lab)] for lab in list(conf_mat_dic.keys())]
    labels_prod = [
        nom_dict[str(lab)]
        for lab in list(conf_mat_dic[list(conf_mat_dic.keys())[0]].keys())
    ]
    return labels_prod, labels_ref


def get_conf_max(
    conf_mat_dic: ConfusionMatrixT, nom_dict: dict[Any, str]
) -> dict[str, Any]:
    """Get confusion max by class.

    Parameters
    ----------
    conf_mat_dic : dict
        dictionary representing a confusion matrix
    nom_dict : dict
        dictionary linking label to class name

    Example
    -------
        >>> conf_mat_dic = dict([(1, dict([(1, 20), (2, 10), (3, 11)])),
        >>>                             (2, dict([(1, 10), (2, 20), (3, 2)])),
        >>>                             (3, dict([(1, 0), (2, 11), (3, 21)]))])
        >>> nom_dict = {1: 'A', 2: 'B', 3: 'C'}
        >>> print get_conf_max(conf_mat_dic, nom_dict)
        >>> {1:['A','C','B'], 2:['B','A','C'],3:['C','B','A']}

    Return
    ------
    dict
        confusion max between labels
    """
    conf_max = {}
    for class_ref, prod in list(conf_mat_dic.items()):
        buff_dico = {}
        for class_prod, value in list(prod.items()):
            buff_dico[nom_dict[str(class_prod)]] = float(value)
            buff = sorted(
                iter(list(buff_dico.items())), key=lambda k_v: (k_v[1], k_v[0])
            )[::-1]
        conf_max[str(class_ref)] = [class_name for class_name, value in buff]
    return conf_max


def compute_interest_matrix(
    all_matrix: list, f_interest: str = "mean"
) -> ConfusionMatrixT:
    """Compute the mean of each confusion matrix.

    Parameters
    ----------
    all_matrix : list
        list of confusion matrix, same format as :py:meth:`get_coeff` input
    """
    # get all ref' labels
    ref_labels = []
    prod_labels = []
    for c_matrix in all_matrix:
        ref_labels += [ref for ref, _ in list(c_matrix.items())]
        prod_labels += [
            prod_label
            for _, prod in list(c_matrix.items())
            for prod_label, _ in list(prod.items())
        ]

    ref_labels = sorted(list(set(ref_labels)))
    prod_labels = sorted(list(set(prod_labels)))
    matrix = initialize_confusion_matrix(ref_labels, prod_labels, [])
    output_matrix = initialize_confusion_matrix(ref_labels, prod_labels, -1)
    # fill-up matrix
    for c_matrix in all_matrix:
        for ref_lab, prod in list(c_matrix.items()):
            for prod_lab, prod_val in list(prod.items()):
                matrix[ref_lab][prod_lab].append(prod_val)
    # Compute interest output matrix
    for ref_lab, prod in list(matrix.items()):
        for prod_lab, prod_val in list(prod.items()):
            if f_interest.lower() == "mean":
                output_matrix[ref_lab][
                    prod_lab
                ] = f"{np.mean(matrix[ref_lab][prod_lab]):.2f}"

    return output_matrix


def get_interest_coeff(
    runs_coeff: list[dict[int, float]], nb_lab: int, f_interest: str = "mean"
) -> dict[int, str]:
    """Compute mean coefficient and 95% confidence interval and store it by class as a string

    Parameters
    ----------
    runs_coeff : list
        a list of dictioanries representing a coefficient by class
    nb_lab : int
        class number
    f_interest : string
        determine which function to apply, only 'mean' is available
    """
    nb_run = len(runs_coeff)

    # get all labels
    for run in runs_coeff:
        ref_labels = [label for label, value in list(run.items())]
    ref_labels = sorted(list(set(ref_labels)))
    # init
    coeff_buff: dict[int, list[float]] = {}
    for ref_lab in ref_labels:
        coeff_buff[ref_lab] = []
    # fill-up
    for run in runs_coeff:
        for label, value in list(run.items()):
            coeff_buff[label].append(value)
    # Compute interest coeff
    coeff_out = {}
    for label, values in list(coeff_buff.items()):
        if f_interest.lower() == "mean":
            mean = np.mean(values)
            _, b_sup = stats.t.interval(
                0.95, nb_lab - 1, loc=np.mean(values), scale=stats.sem(values)
            )
            if nb_run > 1:
                coeff_out[label] = f"{mean:.3f} +- {b_sup - mean:.3f}"
            elif nb_run == 1:
                coeff_out[label] = f"{mean:.3f}"
    return coeff_out


def stats_to_file(
    conf_mat_dic: dict,
    all_scores: MultipleClassifScores,
    nb_seed: int,
    out_report: PathLike,
    labels_parameters: LabelsParameters,
) -> None:
    """Write statistics to file."""

    labels_prod, labels_ref = get_labels(conf_mat_dic, labels_parameters.nom_dict)
    all_labels = set(labels_prod + labels_ref)
    size_max = max(len(lab) for lab in all_labels)

    confusion_max = compute_mean_scores(
        all_scores,
        conf_mat_dic,
        len(labels_ref),
        labels_parameters.nom_dict,
        labels_parameters.labels_table,
    )

    with open(out_report, "w", encoding="utf-8") as res_file:
        # Confusion Matrix
        write_confusion_matrix(
            res_file, conf_mat_dic, labels_prod, labels_parameters.nom_dict, size_max
        )

        # KAPPA and OA
        write_kappa_oa(res_file, all_scores, nb_seed, labels_ref)

        # Precision, Recall, F-score, max confusion
        write_summary(
            res_file, all_scores, labels_parameters.nom_dict, size_max, confusion_max
        )


def write_confusion_matrix(
    res_file: FileObject,
    conf_mat_dic: dict,
    labels_prod: list,
    nom_dict: dict[Any, str],
    size_max: int,
) -> None:
    """
    Write confusion matrix to the result file

    Parameters
    ----------
    res_file:
        File object to write the output
    conf_mat_dic:
        Dictionary containing the confusion matrix
    labels_prod:
        List of production labels
    nom_dict:
        Dictionary mapping labels to their names
    size_max:
        Maximum size of labels for formatting
    """
    res_file.write(
        "#row = reference\n#col = production\n"
        "\n*********** Matrice de confusion ***********\n\n"
    )

    prod_ref_labels = (
        "".join([" " for _ in range(size_max)])
        + "|"
        + "|".join(label.center(size_max) for label in labels_prod)
        + "\n"
    )
    res_file.write(prod_ref_labels)
    for lab_ref, prod_conf in list(conf_mat_dic.items()):
        prod = ""
        prod += nom_dict[str(lab_ref)].center(size_max) + "|"
        for _, conf_val in list(prod_conf.items()):
            prod += str(conf_val).center(size_max) + "|"
        prod += nom_dict[str(lab_ref)].center(size_max) + "\n"
        res_file.write(prod)


def write_summary(
    res_file: FileObject,
    all_scores: MultipleClassifScores,
    nom_dict: dict[Any, str],
    size_max: int,
    confusion_max: dict,
) -> None:
    """
    Write summary statistics to the result file

    Parameters
    ----------
    res_file:
        File object to write the output
    all_scores:
        Object containing multiple classification scores
    nom_dict:
        Dictionary mapping labels to their names
    size_max:
        Maximum size of labels for formatting
    confusion_max:
        Dictionary containing the maximum confusion values
    """
    coeff_summarize_lab = [
        "Classes",
        "Precision mean",
        "Rappel mean",
        "F-score mean",
        "Confusion max",
    ]
    sum_head = [lab.center(size_max) for lab in coeff_summarize_lab]
    sum_head_str = " | ".join(sum_head) + "\n"
    sep_c = "-"
    sep = ""
    for _ in range(len(sum_head_str)):
        sep += sep_c
    res_file.write(sum_head_str)
    res_file.write(sep + "\n")
    for label in list(all_scores.precision_mean.keys()):
        confusion_labels = ", ".join(list(confusion_max[str(label)])[0:3])
        class_sum = [
            nom_dict[str(label)].center(size_max),
            all_scores.precision_mean[label].center(size_max),
            all_scores.recall_mean[label].center(size_max),
            all_scores.fscore_mean[label].center(size_max),
            confusion_labels,
        ]
        class_confusion = " | ".join(class_sum) + "\n"
        res_file.write(class_confusion)


def write_kappa_oa(
    res_file: FileObject,
    all_scores: MultipleClassifScores,
    nb_seed: int,
    labels_ref: list,
) -> None:
    """
    Write Kappa and Overall Accuracy (OA) to the result file

    Parameters
    ----------
    res_file:
        File object to write the output
    all_scores:
        Object containing multiple classification scores
    nb_seed:
        Number of seeds
    labels_ref:
        List of reference labels
    """
    kappa_mean = np.mean(all_scores.all_kappa)
    oacc_mean = np.mean(all_scores.all_oacc)
    kappa = f"\nKAPPA : {kappa_mean:.3f}\n"
    oacc = f"OA : {oacc_mean:.3f}\n\n"
    if nb_seed > 1:
        _, k_sup = stats.t.interval(
            0.95,
            len(labels_ref) - 1,
            loc=np.mean(all_scores.all_kappa),
            scale=stats.sem(all_scores.all_kappa),
        )
        kappa = f"\nKAPPA : {kappa_mean:.3f} +- {k_sup - kappa_mean:.3f}\n"
        _, oa_sup = stats.t.interval(
            0.95,
            len(labels_ref) - 1,
            loc=np.mean(all_scores.all_oacc),
            scale=stats.sem(all_scores.all_oacc),
        )
        oacc = f"OA : {oacc_mean:.3f} +- {oa_sup - oacc_mean:.3f}\n\n"
    res_file.write(kappa)
    res_file.write(oacc)


def stats_report(
    list_csv_in: list[str],
    nomenclature_path: PathLike,
    out_report: PathLike,
    indecidedlabel: int | None = None,
    labels_table: dict | None = None,
) -> None:
    """Sum-up statistics in a txt file.

    Parameters
    ----------
    list_csv_in :
        List of csv file to sum-up
    nomenclature_path :
        path to a text file representing the nomenclature
    out_report :
        name of the generated report
    indecidedlabel :
        indecided label value
    labels_table :
        dict[csv_label] = new_label
    """
    all_scores = MultipleClassifScores(
        all_kappa=[],
        all_oacc=[],
        all_precision=[],
        all_recall=[],
        all_fscore=[],
        precision_mean={},
        recall_mean={},
        fscore_mean={},
    )
    all_matrix = []
    for csv_f in list_csv_in:
        conf_mat_dic = parse_csv(csv_f)
        if indecidedlabel:
            conf_mat_dic = remove_indecidedlabel(conf_mat_dic, indecidedlabel)
        scores = get_coeff(conf_mat_dic)

        all_matrix.append(conf_mat_dic)
        all_scores.all_kappa.append(scores.kappa)
        all_scores.all_oacc.append(scores.oacc)
        all_scores.all_precision.append(scores.precision)
        all_scores.all_recall.append(scores.recall)
        all_scores.all_fscore.append(scores.fscore)

    conf_mat_dic = compute_interest_matrix(all_matrix, f_interest="mean")
    nom_dict = get_nomenclature(nomenclature_path)
    if labels_table:
        conf_mat_dic = {
            labels_table[row]: {
                labels_table[col]: col_val for col, col_val in col_dict.items()
            }
            for row, col_dict in conf_mat_dic.items()
        }

    stats_to_file(
        conf_mat_dic,
        all_scores,
        len(list_csv_in),
        out_report,
        LabelsParameters(nom_dict, labels_table),
    )


def compute_mean_scores(
    all_scores: MultipleClassifScores,
    conf_mat_dic: ConfusionMatrixT,
    nb_labels: int,
    nom_dict: dict[Any, str],
    labels_table: LabelsConversionDict | None = None,
) -> dict[str, Any]:
    """
    Compute mean precision, recall and fscore and convert the labels if needed

    Parameters
    ----------
    all_scores
        MultipleClassifScore object containing all scores
    conf_mat_dic
        Ordered dictionary containing confusion matrices
    nb_labels
        Number of labels
    labels_table
        Labels conversion table
    nom_dict
        Nomenclature dictionary
    """
    all_scores.precision_mean = get_interest_coeff(
        all_scores.all_precision, nb_lab=nb_labels, f_interest="mean"
    )
    all_scores.recall_mean = get_interest_coeff(
        all_scores.all_recall, nb_lab=nb_labels, f_interest="mean"
    )
    all_scores.fscore_mean = get_interest_coeff(
        all_scores.all_fscore, nb_lab=nb_labels, f_interest="mean"
    )
    confusion_max = get_conf_max(conf_mat_dic, nom_dict)
    if labels_table:
        p_mean_tmp = {}
        for i2_label, p_mean_val in all_scores.precision_mean.items():
            p_mean_tmp[labels_table[i2_label]] = p_mean_val
        all_scores.precision_mean = p_mean_tmp
        r_mean_tmp = {}
        for i2_label, r_mean_val in all_scores.recall_mean.items():
            r_mean_tmp[labels_table[i2_label]] = r_mean_val
        all_scores.recall_mean = r_mean_tmp
        f_mean_tmp = {}
        for i2_label, f_mean_val in all_scores.fscore_mean.items():
            f_mean_tmp[labels_table[i2_label]] = f_mean_val
        all_scores.fscore_mean = f_mean_tmp

    return confusion_max


def get_region_and_seeds(path: str) -> dict[str, set[str]]:
    """Search path to find the regions and seeds of outrates files."""
    files = fu.file_search_and(path, False, "_outrates.csv")
    regions = set()
    seeds = set()

    # samples_region_1_seed_0_outrates
    def split_region_seed(fln: str) -> tuple[str, str]:
        fln = fln.replace("samples_region_", "")
        fln = fln.replace("_outrates", "")
        parts = fln.split("_seed_")
        return parts[0], parts[1]

    for file in files:
        region, seed = split_region_seed(file)
        regions.update([region])
        seeds.update([seed])
    return {"regions": regions, "seeds": seeds}


def concatenate_sample_selection(
    result_folder: PathLike,
    nomenclature_path: PathLike,
    outrates_path: PathLike,
    concat_parameters: SampleSelectionConcatParams,
) -> None:
    """Concatenates the csv files produced by samples selection step.

    Parameters
    ----------
    result_folder:
        folder where output csv will be written
    nomenclature_path:
        file where labels nomenclature is defined
    outrates_path:
        path of the OTB generated outrates.csv files
    concat_parameters:
        Parameters containing the seed, a set of region labels and the labels conversion dict
    """
    nom_dict = get_nomenclature(nomenclature_path)

    region_col = [f"Region {r}" for r in concat_parameters.regions]
    columns = ["Class", "Label"] + region_col + ["Total"]
    index = list(nom_dict.keys()) + ["Total"]
    cat = pd.DataFrame(columns=columns, index=index)
    cat[region_col] = 0  # initialize to zero
    cat.loc["Total", "Class"] = ""
    cat.loc["Total", "Label"] = ""

    # fill class names
    for key, val in nom_dict.items():
        cat.loc[key, "Class"] = val
        cat.loc[key, "Label"] = key

    # read csv
    read_sample_selection_csv(
        cat,
        concat_parameters.labels_conversion,
        outrates_path,
        concat_parameters.regions,
        concat_parameters.seed,
    )

    # compute totals
    for reg in region_col:
        cat.loc["Total", reg] = np.nansum(cat[reg][:-1])
    cat["Total"] = np.nansum(cat[region_col], axis=1)

    # write output
    cat.to_csv(
        str(
            Path(result_folder)
            / f"class_statistics_seed{concat_parameters.seed}_learn.csv"
        )
    )


def read_sample_selection_csv(
    cat: pd.DataFrame,
    labels_conversion: LabelsConversionDict,
    outrates_path: PathLike,
    regions: set[str],
    seed: int,
) -> None:
    """
    Read the csv files produced by samples selection step.

    Parameters
    ----------
    cat:
        Dataframe containing the regions columns, empty
    labels_conversion
        maps i2-encoded label to original label
    outrates_path
        path of the OTB generated outrates.csv files
    regions
        set of region labels
    seed
        number of the seed
    """
    for region in regions:
        outrate_file = str(
            Path(outrates_path, f"samples_region_{region}_seed_{seed}_outrates.csv")
        )
        outrates = pd.read_csv(
            outrate_file,
            sep="\t",
            skiprows=0,
            header=0,
            names=["className", "requiredSamples", "totalSamples", "rate"],
        )
        for _, row in outrates.iterrows():
            label = str(labels_conversion[row.className])
            nsamples: float = row.requiredSamples
            total_samples: float = row.totalSamples
            nsamples = min(float(nsamples), float(total_samples))
            cat.loc[label, f"Region {region}"] = nsamples


def merge_all_sample_selection(
    output_path: PathLike,
    nomenclature_path: PathLike,
    labels_conversion: dict,
) -> None:
    """
    Search for all existing sample selections csv files.

    For each seed, merges them in a single file.
    output_path:
        output path root containing learningSamples folder and sampleSelections folders
    nomenclature_path:
        file where labels nomenclature is defined
    labels_conversion:
        maps i2-encoded label to original label
    """
    result_path = str(Path(output_path) / "learningSamples")
    samples_selection_path = str(Path(output_path) / "samplesSelection")
    reg_seed = get_region_and_seeds(samples_selection_path)
    regions = reg_seed["regions"]
    for seed in reg_seed["seeds"]:
        concatenate_sample_selection(
            result_path,
            nomenclature_path,
            samples_selection_path,
            SampleSelectionConcatParams(
                seed=int(seed), regions=regions, labels_conversion=labels_conversion
            ),
        )
