#!/usr/bin/env python3
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Mosaic classifications in order to produce final maps."""

import logging
import re
import shutil
from pathlib import Path
from typing import Literal

from osgeo import gdal

from iota2.common import create_indexed_color_image as color
from iota2.common import file_utils as fu
from iota2.common import otb_app_bank as otb
from iota2.common.compression_options import compression_options
from iota2.common.file_utils import get_raster_resolution
from iota2.common.otb_app_bank import AvailableOTBApp, create_application
from iota2.common.raster_utils import re_encode_raster
from iota2.common.utils import run
from iota2.typings.i2_types import (
    LabelsConversionDict,
    MosaicInputPaths,
    MosaicParameters,
    PathLike,
    SpatialResolution,
)

LOGGER = logging.getLogger("distributed.worker")


def generate_diff_map(
    runs: int,
    all_tiles: list[str],
    spatial_resolution: SpatialResolution,
    input_paths: MosaicInputPaths,
    working_directory: str | None = None,
) -> None:
    """
    Produce a map of well/bad classified learning/validation pixels.

    Parameters
    ----------
    runs:
        the number of seeds
    all_tiles:
        the list of all tiles processed
    spatial_resolution:
        the spatial resolution (res X, res Y)
    input_paths:
        Object containing paths to final, final/TMP and dataAppVal folders
    working_directory:
        the working directory
    """

    for seed in range(runs):
        for tile in all_tiles:
            val_tiles = fu.file_search_and(
                Path(input_paths.validation_input_folder),
                True,
                tile,
                "_seed_" + str(seed) + "_val.sqlite",
            )
            if not val_tiles:
                continue
            val_tile = val_tiles[0]

            classif = str(
                Path(input_paths.final_tmp_folder) / f"{tile}_seed_{seed}.tif"
            )
            diff = str(
                Path(input_paths.final_tmp_folder) / f"{tile}_seed_{seed}_CompRef.tif"
            )

            compare_ref(
                shape_ref=val_tile,
                classif=classif,
                diff=diff,
                working_directory=working_directory,
                output_path=input_paths.final_tmp_folder,
            )

        assemble_diff(
            input_paths.final_folder,
            input_paths.final_tmp_folder,
            seed,
            spatial_resolution,
            working_directory,
        )


def assemble_diff(
    final_folder: str,
    final_tmp_folder: str,
    seed: int,
    spatial_resolution: SpatialResolution,
    working_directory: str | None = None,
) -> None:
    """
    Assemble difference files for a given seed and copy the result to the final folder.

    Parameters
    ----------
    final_folder:
        Path to final/ folder
    final_tmp_folder
        Path to final/TMP folder
    seed
        Seed number
    spatial_resolution
        Spatial resolution (x, y)
    working_directory
        the working directory
    """
    wdir = working_directory if working_directory else final_tmp_folder
    diff_seed_folder = wdir if working_directory else final_folder
    creation_options = {
        "COMPRESS": compression_options.algorithm,
        "PREDICTOR": compression_options.predictor,
        "BIGTIFF": "YES",
        "TILED": "YES",
    }

    all_diff = fu.file_search_and(
        Path(final_tmp_folder), True, f"_seed_{seed}_CompRef.tif"
    )
    diff_seed = str(Path(diff_seed_folder) / f"diff_seed_{seed}.tif")
    if all_diff:
        if not spatial_resolution:
            res_x, res_y = fu.get_raster_resolution(all_diff[0])
            spatial_resolution = (res_x, res_y)

        merge_options = fu.TileMergeOptions(
            spatial_resolution=spatial_resolution,
            output_pixel_type="Byte",
            creation_options=creation_options,
        )
        fu.assemble_tile_merge(all_diff, diff_seed, merge_options)

        if working_directory:
            shutil.copy(
                f"{wdir}/diff_seed_{seed}.tif",
                str(Path(final_folder) / f"diff_seed_{seed}.tif"),
            )


def compare_ref(
    shape_ref: str,
    classif: str,
    diff: str,
    output_path: str,
    working_directory: str | None = None,
) -> str:
    """
    Compare the reference data with the predicted classification.

    Parameters
    ----------
    shape_ref:
        validation shapefile
    classif:
        produced classification
    diff:
        output raster file
    working_directory:
        path to working directory
    output_path:
        the final output path

    """
    wdir = working_directory if working_directory else output_path
    shape_learn = str(
        Path(shape_ref).parent / Path(shape_ref).name.replace("val", "learn")
    )
    assert Path(shape_learn).exists()
    shape_raster_val = str(Path(wdir) / Path(shape_ref).name.replace(".sqlite", ".tif"))

    shape_raster_learn = str(
        Path(wdir) / Path(shape_learn).name.replace(".sqlite", ".tif")
    )
    # Rasterize val
    rasterize_app, _ = create_application(
        AvailableOTBApp.RASTERIZATION,
        {"in": shape_ref, "out": shape_raster_val, "im": classif},
    )
    rasterize_app.ExecuteAndWriteOutput()

    # Rasterize learn
    rasterize_app, _ = create_application(
        AvailableOTBApp.RASTERIZATION,
        {"in": shape_learn, "out": shape_raster_learn, "im": classif},
    )
    rasterize_app.ExecuteAndWriteOutput()

    # diff val

    diff_val = str(Path(wdir) / Path(diff).name.replace(".tif", "_val.tif"))
    # reference val == classif  -> 2  | reference val != classif -> 1 |
    # no reference -> 0
    bandmath_val, _ = otb.create_application(
        AvailableOTBApp.BAND_MATH,
        {
            "il": [shape_raster_val, classif],
            "out": diff_val,
            "pixType": "uint8",
            "exp": "im1b1==0?0:im1b1==im2b1?2:1",
        },
    )
    bandmath_val.ExecuteAndWriteOutput()
    Path(shape_raster_val).unlink()

    # diff learn
    diff_learn = str(Path(wdir) / Path(diff).name.replace(".tif", "_learn.tif"))
    # reference learn == classif -> 4  | reference learn != -> 3
    # | no reference -> 0
    bandmath_learn, _ = otb.create_application(
        AvailableOTBApp.BAND_MATH,
        {
            "il": [shape_raster_learn, classif],
            "out": diff_learn,
            "pixType": "uint8",
            "exp": "im1b1==0?0:im1b1==im2b1?4:3",
        },
    )
    bandmath_learn.ExecuteAndWriteOutput()
    Path(shape_raster_learn).unlink()

    # sum diff val + learn
    diff_tmp = str(Path(wdir) / Path(diff).name)
    bandmath_sum, _ = otb.create_application(
        AvailableOTBApp.BAND_MATH,
        {
            "il": [diff_val, diff_learn],
            "out": diff_tmp,
            "pixType": "uint8",
            "exp": "im1b1+im2b1",
        },
    )
    bandmath_sum.ExecuteAndWriteOutput()
    Path(diff_val).unlink()
    Path(diff_learn).unlink()

    if working_directory:  # and not Path(diff).exists():
        shutil.copy(diff_tmp, diff)
        Path(diff_tmp).unlink()

    return diff


def create_dummy_rasters(
    missing_tiles: list[str], runs: int, classifications_dir: str, final_dir: str
) -> None:
    """Create empty rasters.

    Parameters
    ----------
    missing_tiles:
        List of tiles
    runs:
        The number of random seeds
    classifications_dir:
        Directory containing classifications
    final_dir:
        The output path

    Notes
    -----
    use when mode is 'one_region' but there is no validations / learning
    samples into a specific tile
    """

    for tile in missing_tiles:
        classif_tile = fu.file_search_and(
            classifications_dir, True, "Classif_" + str(tile)
        )[0]
        for seed in range(runs):
            dummy_raster_name = f"{tile}_seed_{seed}_CompRef.tif"
            dummy_raster = f"{final_dir}/{dummy_raster_name}"
            dummy_raster_cmd = (
                f"gdal_merge.py -ot Byte -n 0 -createonly -o "
                f"{dummy_raster} {classif_tile}"
            )
            run(dummy_raster_cmd)


def remove_in_list_by_regex(
    input_list: list[str],
    regex: str,
) -> list[str]:
    """
    Remove all element in the list that match regex.

    Parameters
    ----------
    input_list:
        the list to filter
    regex:
        the regular expression
    """
    out_list = []
    for elem in input_list:
        match = re.match(regex, elem)
        if not match:
            out_list.append(elem)

    return out_list


def proba_map_fusion(
    proba_map_list: list[str],
    ram: int = 128,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Fusion of probabilities map.

    Parameters
    ----------
    proba_map_list :
        list of probabilities map to merge
    ram :
        available ram in mb
    working_directory :
        working directory absolute path
    logger
        python logger
    """
    model_pos = 3

    proba_map_fus_dir = str(Path(proba_map_list[0]).parent)
    proba_map_fus_name: str = Path(proba_map_list[0]).name
    proba_map_fus_names: list[str] = proba_map_fus_name.split("_")
    proba_map_fus_names[model_pos] = proba_map_fus_names[model_pos].split("f")[0]
    proba_map_fus_name = "_".join(proba_map_fus_names)
    list_exp = "+".join([f"im{i+1}" for i in range(len(proba_map_list))])
    exp = f"({list_exp}) dv {len(proba_map_list)}"
    proba_map_fus_path = str(Path(proba_map_fus_dir) / proba_map_fus_name)
    if working_directory:
        proba_map_fus_path = str(Path(working_directory) / proba_map_fus_name)
    logger.info(
        f"Fusion of probability maps : {proba_map_list} at {proba_map_fus_path}"
    )
    proba_merge, _ = create_application(
        AvailableOTBApp.BAND_MATH_X,
        {"il": proba_map_list, "ram": str(ram), "out": proba_map_fus_path, "exp": exp},
    )
    proba_merge.ExecuteAndWriteOutput()
    if working_directory:
        copy_target = str(Path(proba_map_fus_dir) / proba_map_fus_name)
        logger.debug("copy %s to %s", proba_map_fus_path, copy_target)
        shutil.copy(proba_map_fus_path, copy_target)
        Path(proba_map_fus_path).unlink()


def merge_confidence(
    final_tile: str,
    classif_tile: list[str],
    confidence: list[str],
    output_confidence: str,
    pix_type: Literal["uint8", "float"] = "uint8",
) -> None:
    """Merge confidence maps

    Parameters
    ----------
    final_tile:
        merged tile classification
    classif_tile:
        the list of classification
    confidence:
        the list of confidence
    output_confidence:
        the output name for the confidence map
    pix_type:
        The pixel type used to store confidence. If the pix_type is uint8, the confidence is
        multiplied by a factor of 100, and a factor of 1 otherwise.

    Notes
    -----
    This function is used in 'fusion' classification mode.
    If the final decision is the same as the voter, the confidence value is added.
    If the final decision is not the same as the voter, (1 - confidence) is added
    The final confidence is divided by the number of voters
    """
    if len(classif_tile) != len(confidence):
        raise ValueError(
            "number of confidence map and classification map must be the same"
        )
    if pix_type == "uint8":
        fact = 100
    else:
        fact = 1
    number_of_classif = len(classif_tile)
    exp = []
    for n_classif_tile in range(len(classif_tile)):
        # if final == voting add the confidence value
        # if final != voting add 1-confidence
        # Divided by the number of votes
        exp.append(
            "(im"
            + str(n_classif_tile + 2)
            + "b1==0?0:im1b1!=im"
            + str(n_classif_tile + 2)
            + "b1?1-im"
            + str(n_classif_tile + 2 + number_of_classif)
            + "b1:im"
            + str(n_classif_tile + 2 + number_of_classif)
            + "b1)"
        )
    # im1b1==0 means image border here
    exp_confidence = "im1b1==0?0:(" + "+".join(exp) + ")/" + str(len(classif_tile))

    all_images: list[str] = [final_tile] + classif_tile + confidence

    bandmath, _ = otb.create_application(
        otb.AvailableOTBApp.BAND_MATH,
        {
            "il": all_images,
            "exp": f"{fact}*({exp_confidence})",
            "out": output_confidence,
            "pixType": pix_type,
            "ram": 5120,
        },
    )
    bandmath.ExecuteAndWriteOutput()


def get_split_models(path_classif: str, tile: str, seed: int, suffix: str) -> list[str]:
    """
    Return the list of split models for a given tile and seed

    Parameters
    ----------
    path_classif:
        Path to the classif/ folder
    tile:
        Tile name
    seed:
        Seed number
    suffix:
        suffix
    """
    # if model has been split, some classifications have an f in the region name
    classif_tile = fu.file_search_reg_ex(
        str(Path(path_classif) / f"Classif_{tile}*model_*f*_seed_{seed}{suffix}")
    )
    split_models_list = []
    for classif in classif_tile:
        model = Path(classif).name.split("_")[3].split("f")[0]
        if model not in split_models_list:
            split_models_list.append(model)

    return split_models_list


def merge_confidence_in_fusion_mode(
    all_tiles: list[str],
    mosaic_input_paths: MosaicInputPaths,
    mosaic_parameters: MosaicParameters,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> list[str]:
    """
    Handle the case when models are split

    Parameters
    ----------
    all_tiles:
        List of tiles
    mosaic_input_paths:
        Object containing different paths
    mosaic_parameters:
        Parameters
    working_directory:
        path to store intermediate results
    logger:
        logger
    """
    all_confidences_out = []
    wdir = working_directory if working_directory else mosaic_input_paths.path_classif
    suffix = "*"
    for seed in range(mosaic_parameters.runs):
        for tile in all_tiles:

            split_confidence = get_split_confidences(
                mosaic_input_paths.path_classif,
                mosaic_parameters,
                seed,
                tile,
                wdir,
                logger,
            )

            confidence_all = fu.file_search_reg_ex(
                str(
                    Path(mosaic_input_paths.path_classif)
                    / f"{tile}*model_*_confidence_seed_{seed}{suffix}"
                )
            )
            confidence_without_split = remove_in_list_by_regex(
                confidence_all, f".*model_.*f.*_confidence.{suffix}"
            )
            confidence_list = split_confidence + confidence_without_split
            all_confidences_out.append(
                merge_all_confidences(
                    confidence_list,
                    tile,
                    seed,
                    mosaic_input_paths.final_tmp_folder,
                    str(Path(wdir) / "tmpClassif"),
                )
            )
    return all_confidences_out


def get_split_confidences(
    path_classif: str,
    mosaic_parameters: MosaicParameters,
    seed: int,
    tile: str,
    wdir: str,
    logger: logging.Logger = LOGGER,
) -> list[str]:
    """
    Get all split confidence maps and merge them

    Parameters
    ----------
    path_classif:
        Path to the 'classif/' folder
    mosaic_parameters:
        Parameters for the mosaic.
    seed:
        Seed number
    tile:
        Tile name
    wdir:
        Working directory
    logger:
        Logger
    """
    suffix = "*"
    split_models_list = get_split_models(path_classif, tile, seed, suffix)
    split_confidence = []
    for model in split_models_list:

        if mosaic_parameters.flags.proba_map_flag:
            proba_map_fusion(
                proba_map_list=fu.file_search_reg_ex(
                    str(
                        Path(path_classif)
                        / f"PROBAMAP_{tile}_model_{model}f*_seed_{seed}"
                    )
                ),
                working_directory=wdir,
                ram=2000,
                logger=logger,
            )
        # get all sub-regions for the model selected
        # 'model'f* and not *f* in regex
        list_classif_tile_by_model = fu.file_search_reg_ex(
            str(
                Path(path_classif)
                / f"Classif_{tile}*model_{model}f*_seed_{seed}{suffix}"
            )
        )
        confidence = fu.file_search_reg_ex(
            str(
                Path(path_classif)
                / f"{tile}*model_{model}f*_confidence_seed_{seed}{suffix}"
            )
        )
        list_classif_tile_by_model = sorted(list_classif_tile_by_model)
        confidence = sorted(confidence)
        # final_tile is the already existing result
        # of the fusion of classification
        final_tile = str(
            Path(path_classif) / f"Classif_{tile}_model_{model}_seed_{seed}.tif"
        )
        model_output_confidence = str(
            Path(wdir)
            / "tmpClassif"
            / f"{tile}_model_{model}_confidence_seed_{seed}.tif",
        )
        merge_confidence(
            final_tile,
            list_classif_tile_by_model,
            confidence,
            model_output_confidence,
            pix_type="float",
        )
        split_confidence.append(model_output_confidence)
    return split_confidence


def merge_all_confidences(
    confidence_list: list[str],
    tile: str,
    seed: int,
    final_tmp_folder: str,
    tmp_classif: str,
) -> str:
    """
    Merge all confidences for a given tile and seed.

    Parameters
    ----------
    confidence_list
        List of confidence file paths.
    tile
        The tile name.
    seed
        The seed number.
    final_tmp_folder
        The path to the final temporary folder.
    tmp_classif
        The path to the temporary classification folder.
    """

    output_confidence = str(
        Path(tmp_classif) / f"{tile}_GlobalConfidence_seed_{seed}.tif"
    )
    spatial_resolution = fu.get_raster_resolution(confidence_list[0])
    assert spatial_resolution
    merge_options = fu.TileMergeOptions(
        spatial_resolution=spatial_resolution,
        output_pixel_type="Float32",
        creation_options={
            "COMPRESS": compression_options.algorithm,
            "PREDICTOR": compression_options.predictor,
            "BIGTIFF": "YES",
            "TILED": "YES",
        },
    )
    fu.assemble_tile_merge(confidence_list, output_confidence, merge_options)

    bandmath, _ = otb.create_application(
        AvailableOTBApp.BAND_MATH,
        {
            "il": output_confidence,
            "out": output_confidence,
            "pixType": "uint8",
            "exp": "100 * im1b1",
        },
    )
    bandmath.ExecuteAndWriteOutput()

    shutil.copyfile(
        output_confidence,
        str(Path(final_tmp_folder) / f"{tile}_GlobalConfidence_seed_{seed}.tif"),
    )
    Path(output_confidence).unlink()
    global_confidence_out = str(
        Path(final_tmp_folder) / f"{tile}_GlobalConfidence_seed_{seed}.tif"
    )

    return global_confidence_out


def gen_global_confidence(
    mosaic_input_paths: MosaicInputPaths,
    mosaic_parameters: MosaicParameters,
    all_tiles: list[str],
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> list[str]:
    """
    Get all confidence maps and generate the global product for each tile.

    Parameters
    ----------
    mosaic_parameters:
        Parameters for the mosaic.
    mosaic_input_paths:
        Mosaic input files.
    all_tiles:
        the list of all tiles processed
        enable the probability final product creation
    working_directory:
        the working directory where tmp files can be stored
    logger:
        logger
    """
    wdir = working_directory if working_directory else mosaic_input_paths.path_classif
    tmp_classif = str(Path(wdir) / "tmpClassif")

    Path(tmp_classif).mkdir(parents=True, exist_ok=True)

    if mosaic_parameters.classif_mode != "separate":
        confidences_out = merge_confidence_in_fusion_mode(
            all_tiles=all_tiles,
            mosaic_input_paths=mosaic_input_paths,
            mosaic_parameters=mosaic_parameters,
            working_directory=wdir,
            logger=logger,
        )
    else:
        confidences_out = merge_confidence_in_separate_mode(
            mosaic_parameters, mosaic_input_paths, all_tiles, tmp_classif
        )
    return confidences_out


def merge_confidence_in_separate_mode(
    mosaic_parameters: MosaicParameters,
    mosaic_input_paths: MosaicInputPaths,
    all_tiles: list[str],
    tmp_classif: str,
) -> list[str]:
    """
    Merge confidence maps in separate mode.

    Parameters
    ----------
    mosaic_parameters:
        Parameters for the mosaic.
    mosaic_input_paths:
        Mosaic input files.
    suffix:
        Filename suffix
    all_tiles:
        the list of all tiles processed
    tmp_classif:
        Temporary classification directory.

    Returns
    -------
    global_conf_f:
        File path of the merged global confidence map.
    """
    confidences_out = []
    for seed in range(mosaic_parameters.runs):
        for tile in all_tiles:
            confidence = fu.file_search_reg_ex(
                str(
                    Path(mosaic_input_paths.path_classif)
                    / f"{tile}*confidence_seed_{seed}*"
                )
            )
            if not confidence:
                return []
            global_conf = str(
                Path(tmp_classif) / f"{tile}_GlobalConfidence_seed_{seed}.tif"
            )
            global_conf_f = str(
                Path(mosaic_input_paths.final_tmp_folder)
                / f"{tile}_GlobalConfidence_seed_{seed}.tif"
            )
            confidence = sorted(confidence, key=lambda x: Path(x).name.split("_")[2])
            spatial_resolution = fu.get_raster_resolution(confidence[0])
            assert spatial_resolution
            merge_options = fu.TileMergeOptions(
                spatial_resolution=spatial_resolution,
                output_pixel_type="Float32",
                creation_options={
                    "COMPRESS": compression_options.algorithm,
                    "PREDICTOR": compression_options.predictor,
                    "BIGTIFF": "YES",
                    "TILED": "YES",
                },
            )
            fu.assemble_tile_merge(confidence, global_conf, merge_options)
            bandmath, _ = otb.create_application(
                AvailableOTBApp.BAND_MATH,
                {
                    "il": global_conf,
                    "out": global_conf,
                    "pixType": "uint8",
                    "exp": "100 * im1b1",
                },
            )
            bandmath.ExecuteAndWriteOutput()
            shutil.copyfile(global_conf, global_conf_f)
            Path(global_conf).unlink()
            confidences_out.append(global_conf_f)
    return confidences_out


def classification_shaping(
    mosaic_input_paths: MosaicInputPaths,
    mosaic_parameters: MosaicParameters,
    spatial_resolution: SpatialResolution,
    available_ram: int | None = None,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    Create the mosaic of all iota2 products.

    Parameters
    ----------
    mosaic_input_paths:
        Input files paths
    mosaic_parameters:
        Parameters used during the shaping operation
    spatial_resolution:
        the spatial resolution
    available_ram:
        Available ram
    working_directory:
        path to store intermediate products
    logger:
        logger

    """
    wdir = working_directory if working_directory else mosaic_input_paths.final_folder
    (Path(wdir) / "TMP").mkdir(parents=True, exist_ok=True)

    all_tiles = sorted(
        list(
            {
                classif.split("_")[1]
                for classif in fu.file_search_and(
                    mosaic_input_paths.path_classif, False, "Classif", ".tif"
                )
            }
        )
    )
    # Prepare confidence and probabilities to be merged
    if mosaic_parameters.mode == "classif":
        gen_global_confidence(
            mosaic_input_paths,
            mosaic_parameters,
            all_tiles,
            working_directory,
            logger,
        )

    if spatial_resolution:
        res_x = spatial_resolution[0]
        res_y = spatial_resolution[1]
    else:
        # use a classification as ref image
        res_x, res_y = get_resolution_from_ref_image(mosaic_input_paths.path_classif)
    # classification
    prepare_classification(
        mosaic_input_paths=mosaic_input_paths,
        mosaic_parameters=mosaic_parameters,
        target_spatial_resolution=(res_x, res_y),
    )
    # confidence
    if mosaic_parameters.mode == "classif":
        get_global_confidence(
            assemble_folder=mosaic_input_paths.final_folder,
            list_tile=all_tiles,
            confidence_path=mosaic_input_paths.final_tmp_folder,
            runs=mosaic_parameters.runs,
            target_spatial_resolution=(res_x, res_y),
        )
    # proba_map
    if mosaic_parameters.flags.proba_map_flag:
        get_tiled_proba_map(
            mosaic_input_paths=mosaic_input_paths,
            runs=mosaic_parameters.runs,
            target_spatial_resolution=(res_x, res_y),
            available_ram=available_ram,
        )
    # cloud
    if mosaic_parameters.mode == "classif":
        get_final_cloud(
            mosaic_input_paths=mosaic_input_paths,
            mosaic_parameters=mosaic_parameters,
            list_tile=all_tiles,
            target_spatial_resolution=(res_x, res_y),
            working_directory=working_directory,
        )

        # reencoding
        for seed in range(mosaic_parameters.runs):
            re_encoded_raster_path = str(
                Path(mosaic_input_paths.final_folder)
                / f"Classif_Seed_{seed}_reencoded.tif"
            )
            raster_classif_seed_path = str(
                Path(mosaic_input_paths.final_folder) / f"Classif_Seed_{seed}.tif"
            )
            assert mosaic_parameters.labels_conversion
            decode_and_color_index_map(
                raster_classif_seed_path=raster_classif_seed_path,
                re_encoded_raster_path=re_encoded_raster_path,
                labels_conversion=mosaic_parameters.labels_conversion,
                color_path=mosaic_input_paths.color_path,
                logger=logger,
            )

    generate_diff_map(
        runs=mosaic_parameters.runs,
        all_tiles=all_tiles,
        spatial_resolution=spatial_resolution,
        input_paths=mosaic_input_paths,
        working_directory=working_directory,
    )
    missing_tiles = [
        elem for elem in all_tiles if elem not in mosaic_parameters.tiles_from_cfg
    ]
    create_dummy_rasters(
        missing_tiles,
        mosaic_parameters.runs,
        mosaic_input_paths.path_classif,
        mosaic_input_paths.final_tmp_folder,
    )


def get_resolution_from_ref_image(
    path_classif: PathLike,
) -> SpatialResolution:
    """
    Find a reference image depending on the mode (classif or regression) and retrieve the
    resolution from this raster.

    Parameters
    ----------
    path_classif:
        Path to classif folder
    """
    # find a reference file to use
    reference = fu.file_search_and(path_classif, False, ".tif")[0]
    # get the resolution from the image
    res = get_raster_resolution(reference)
    if res is None:
        raise FileNotFoundError(f"File {reference} can't be read")
    res_x, res_y = res
    res_y = -res_y
    return res_x, res_y


def prepare_classification(
    mosaic_input_paths: MosaicInputPaths,
    mosaic_parameters: MosaicParameters,
    target_spatial_resolution: SpatialResolution,
) -> None:
    """
    Get all classifications and reconstruct them by tiles then merge all.

    Parameters
    ----------
    mosaic_parameters:
        Parameters for the mosaic.
    mosaic_input_paths:
        Mosaic input files.
    target_spatial_resolution:
        the spatial resolution
    """
    # set head of file name
    # example
    # Classif_seed_0.tif
    # Regression_seed_0.tif
    file_name = "Classif"
    output_pixel_type = "Byte"
    if mosaic_parameters.mode == "regression":
        file_name = "Regression"
        output_pixel_type = "float32"

    for seed in range(mosaic_parameters.runs):
        classification = []
        sort = get_sorted_classif(
            file_name, mosaic_input_paths, mosaic_parameters, seed
        )
        for tile, paths in sort:
            sorted_paths = sorted(paths, key=lambda x: Path(x).name.split("_")[3])
            path_cl_final = str(
                Path(mosaic_input_paths.final_tmp_folder) / f"{tile}_seed_{seed}.tif"
            )
            classification.append(path_cl_final)

            merge_options = fu.TileMergeOptions(
                spatial_resolution=target_spatial_resolution,
                output_pixel_type=output_pixel_type,
                creation_options={
                    "COMPRESS": compression_options.algorithm,
                    "PREDICTOR": compression_options.predictor,
                    "BIGTIFF": "YES",
                    "TILED": "YES",
                },
            )
            fu.assemble_tile_merge(sorted_paths, path_cl_final, merge_options)

        mosaic_file = str(
            Path(mosaic_input_paths.final_folder) / f"{file_name}_Seed_{seed}.tif"
        )
        if mosaic_parameters.mode == "regression":
            mosaic_file = str(
                Path(mosaic_input_paths.final_folder) / f"Regression_Seed_{seed}.tif"
            )

        merge_options = fu.TileMergeOptions(
            spatial_resolution=target_spatial_resolution,
            output_pixel_type=output_pixel_type,
            creation_options={
                "COMPRESS": compression_options.algorithm,
                "PREDICTOR": compression_options.predictor,
                "BIGTIFF": "YES",
                "TILED": "YES",
            },
        )
        fu.assemble_tile_merge(classification, mosaic_file, merge_options)


def get_sorted_classif(
    file_name: str,
    mosaic_input_paths: MosaicInputPaths,
    mosaic_parameters: MosaicParameters,
    seed: int,
) -> list[tuple[str, str]]:
    """
    Get paths of sorted classification files and corresponding tile. Ex:
    [
        ("T31TCJ", "path/to/classif/Classif_T31TCJ_model_1_seed_0"),
        ("T31TDJ", "path/to/classif/Classif_T31TDJ_model_1_seed_0"),
    ]

    Parameters
    ----------
    file_name:
        File name
    mosaic_parameters:
        Parameters for the mosaic.
    mosaic_input_paths:
        Mosaic input files.
    seed:
        Seed number
    """
    sort = []
    if mosaic_parameters.classif_mode == "separate" or mosaic_input_paths.region_shape:
        all_classif_seed = fu.file_search_and(
            mosaic_input_paths.path_classif,
            True,
            ".tif",
            file_name,
            "seed_" + str(seed),
        )
        ind = 1
    else:  # fusion mode
        all_classif_seed = fu.file_search_and(
            mosaic_input_paths.path_classif,
            True,
            "_FUSION_NODATA_seed" + str(seed) + ".tif",
        )
        ind = 0
    all_classif_seed = remove_in_list_by_regex(all_classif_seed, ".*model_.f._seed")
    for tile in all_classif_seed:
        sort.append((tile.split("/")[-1].split("_")[ind], tile))
    sort = fu.sort_by_first_elem(sort)
    return sort


def get_global_confidence(
    list_tile: list[str],
    confidence_path: str,
    runs: int,
    assemble_folder: str,
    target_spatial_resolution: SpatialResolution,
) -> None:
    """Get all confidence maps and merge them.

    Parameters
    ----------
    list_tile:
        the list of tiles produced
    confidence_path:
        path where confidence maps are stored
    runs:
        the number of random seeds
    assemble_folder:
        path to store the mosaic
    target_spatial_resolution:
        the expected spatial resolution
    """
    list_tile = sorted(list_tile)
    for seed in range(runs):
        confidence = []
        for tile in list_tile:
            tile_confidence = str(
                Path(confidence_path) / f"{tile}_GlobalConfidence_seed_{seed}.tif"
            )
            if Path(tile_confidence).exists():
                confidence.append(tile_confidence)

        confidence_mosaic = str(Path(assemble_folder) / f"Confidence_Seed_{seed}.tif")
        if confidence:

            merge_options = fu.TileMergeOptions(
                spatial_resolution=target_spatial_resolution,
                output_pixel_type="Byte",
                creation_options={
                    "COMPRESS": compression_options.algorithm,
                    "PREDICTOR": compression_options.predictor,
                    "BIGTIFF": "YES",
                    "TILED": "YES",
                },
            )
            fu.assemble_tile_merge(confidence, confidence_mosaic, merge_options)


def get_tiled_proba_map(
    mosaic_input_paths: MosaicInputPaths,
    runs: int,
    target_spatial_resolution: SpatialResolution,
    available_ram: int | None = None,
) -> None:
    """
    Get all probabilities map and merge them.

    Parameters
    ----------
    mosaic_input_paths:
        Input files paths
    runs:
        the number of random seeds
    target_spatial_resolution:
        the expected spatial resolution
    available_ram:
        Available ram
    """
    tile_ind = 1
    model_ind = 3
    for seed in range(runs):
        proba_map_list = fu.file_search_reg_ex(
            str(Path(mosaic_input_paths.path_classif) / f"PROBAMAP_*_seed_{seed}*.tif")
        )
        proba_map_list = remove_in_list_by_regex(proba_map_list, ".*model_.*f.*_seed.*")

        sort = []
        for proba_map_file in proba_map_list:
            sort.append(
                (proba_map_file.split("/")[-1].split("_")[tile_ind], proba_map_file)
            )
        sort = fu.sort_by_first_elem(sort)
        sorted_files = []
        for _, paths in sort:
            sorted_files += sorted(
                paths, key=lambda x: Path(x).name.split("_")[model_ind]
            )
        proba_map_mosaic_compress = str(
            Path(mosaic_input_paths.final_folder) / f"ProbabilityMap_seed_{seed}.tif"
        )

        merge_options = fu.TileMergeOptions(
            spatial_resolution=target_spatial_resolution,
            output_pixel_type="Int16",
            available_ram=available_ram,
            creation_options={
                "COMPRESS": compression_options.algorithm,
                "PREDICTOR": compression_options.predictor,
                "BIGTIFF": "YES",
                "TILED": "YES",
            },
        )
        fu.assemble_tile_merge(sorted_files, proba_map_mosaic_compress, merge_options)


def get_final_cloud(
    mosaic_input_paths: MosaicInputPaths,
    mosaic_parameters: MosaicParameters,
    list_tile: list[str],
    target_spatial_resolution: SpatialResolution,
    working_directory: str | None = None,
) -> None:
    """Prepare validity masks to be merged.

    Parameters
    ----------
    mosaic_parameters:
        Parameters for the mosaic.
    mosaic_input_paths:
        Mosaic input files.
    list_tile:
        the list of tiles
    target_spatial_resolution:
        the expected spatial resolution
    working_directory:
        folder to store intermediate results
    """
    working_dir = (
        working_directory
        if working_directory
        else str(Path(mosaic_input_paths.final_folder) / "TMP")
    )
    list_tile = sorted(list_tile)
    # for seed in range(runs):
    cloud = []
    # cloud.append([])
    for tile in list_tile:
        cloud_tile = str(
            Path(mosaic_input_paths.nb_view_folder) / f"{tile}" / "nbView.tif"
        )
        if Path(cloud_tile).exists():
            classif_tile = str(
                Path(mosaic_input_paths.final_folder) / "TMP" / f"{tile}_seed_0.tif"
            )
            cloud_tile_priority = f"{tile}_Cloud.tif"
            cloud_tile_priority_stats_ok = f"{tile}_Cloud_StatsOK.tif"
            cloud.append(
                str(Path(mosaic_input_paths.final_folder) / "TMP" / cloud_tile_priority)
            )
            bandmath, _ = otb.create_application(
                AvailableOTBApp.BAND_MATH,
                {
                    "il": [cloud_tile, classif_tile],
                    "out": str(Path(working_dir) / cloud_tile_priority),
                    "pixType": "int16",
                    "exp": "im2b1>0?im1b1:0",
                },
            )
            bandmath.ExecuteAndWriteOutput()
            if mosaic_parameters.flags.output_statistics:
                bandmath, _ = otb.create_application(
                    AvailableOTBApp.BAND_MATH,
                    {
                        "il": [cloud_tile, classif_tile],
                        "out": str(Path(working_dir) / cloud_tile_priority_stats_ok),
                        "pixType": "int16",
                        "exp": "im2b1>0?im1b1:-1",
                    },
                )
                bandmath.ExecuteAndWriteOutput()
                if working_directory:
                    shutil.copy(
                        str(Path(working_dir) / cloud_tile_priority_stats_ok),
                        str(
                            Path(mosaic_input_paths.final_folder)
                            / "TMP"
                            / cloud_tile_priority_stats_ok
                        ),
                    )
                    (Path(working_dir) / cloud_tile_priority_stats_ok).unlink()
            if working_directory:
                shutil.copy(
                    str(Path(working_dir) / cloud_tile_priority),
                    str(
                        Path(mosaic_input_paths.final_folder)
                        / "TMP"
                        / cloud_tile_priority
                    ),
                )
                (Path(working_dir) / cloud_tile_priority).unlink()
    cloud_mosaic_compress = str(
        Path(mosaic_input_paths.final_folder) / "PixelsValidity.tif"
    )

    merge_options = fu.TileMergeOptions(
        spatial_resolution=target_spatial_resolution,
        output_pixel_type="Byte",
        creation_options={
            "COMPRESS": compression_options.algorithm,
            "PREDICTOR": compression_options.predictor,
            "BIGTIFF": "YES",
            "TILED": "YES",
        },
    )
    fu.assemble_tile_merge(cloud, cloud_mosaic_compress, merge_options)


def decode_and_color_index_map(
    raster_classif_seed_path: str,
    re_encoded_raster_path: str,
    labels_conversion: LabelsConversionDict,
    color_path: str,
    logger: logging.Logger = LOGGER,
) -> None:
    """
    For an input map decode the labels and produce the colored map.

    Parameters
    ----------
    raster_classif_seed_path:
        path to input classification
    re_encoded_raster_path:
        path to re encoded classification maps
    labels_conversion:
        dictionary to map label conversion
    color_path:
        path to color file
    logger:
        logger
    """
    encoded_raster_bool, pix_type = re_encode_raster(
        raster_classif_seed_path,
        re_encoded_raster_path,
        labels_conversion,
        logger=logger,
    )
    raster_to_color_path = raster_classif_seed_path
    if encoded_raster_bool:
        shutil.move(re_encoded_raster_path, raster_classif_seed_path)
        labels_conversion_seed = None
    else:
        labels_conversion_seed = labels_conversion.copy()

    color.create_indexed_color_image(
        raster_to_color_path,
        color_path,
        output_pix_type=gdal.GDT_Byte if pix_type == "uint8" else gdal.GDT_UInt16,
        labels_conversion=labels_conversion_seed,
        co_option=[
            "COMPRESS=" + compression_options.algorithm,
            "PREDICTOR=" + str(compression_options.predictor),
        ],
    )


def provide_input_to_mosaic(
    mosaic_input_paths_list: list[MosaicInputPaths],
    mosaic_parameters: MosaicParameters,
    spatial_resolution: SpatialResolution,
    available_ram: int,
    working_directory: str | None = None,
    logger: logging.Logger = LOGGER,
) -> None:
    """Handle the case of several mosaics to produce.

    Parameters
    ----------
    mosaic_input_paths_list:
        Mosaic input files.
    mosaic_parameters:
        Parameters for the mosaic.
    spatial_resolution:
        Spatial resolution
    available_ram:
        RAM to use in the process
    working_directory:
        path to store intermediate products
    logger:
        logger
    """
    for mosaic_input_paths in mosaic_input_paths_list:
        classification_shaping(
            mosaic_input_paths=mosaic_input_paths,
            mosaic_parameters=mosaic_parameters,
            available_ram=available_ram,
            spatial_resolution=spatial_resolution,
            working_directory=working_directory,
            logger=logger,
        )


def mosaic_regression(
    tiles: list[str],
    file_name: str,
    output_path: str,
    path_classif: str,
    reference_image: str,
) -> None:
    """
    Get all classifications and reconstruct them by tiles then merge all.

    Parameters
    ----------
    tiles:
        List of tiles
    file_name:
        File name
    output_path:
        the output path
    path_classif:
        path to classifications
    reference_image:
        reference image for the spatial resolution
    """
    if res_xy := get_raster_resolution(reference_image):
        res_x, res_y = res_xy
    else:
        raise FileNotFoundError(f"Can't read {reference_image}")
    res_y = -res_y
    target_spatial_resolution = (res_x, res_y)

    output_pixel_type = "float32"

    mosaic_images = []
    for tile in tiles:
        mosaic_images.append(
            fu.file_search_and(path_classif, True, tile + "_" + file_name)[0]
        )

    merge_options = fu.TileMergeOptions(
        spatial_resolution=target_spatial_resolution,
        output_pixel_type=output_pixel_type,
        creation_options={
            "COMPRESS": compression_options.algorithm,
            "PREDICTOR": compression_options.predictor,
            "BIGTIFF": "YES",
            "TILED": "YES",
        },
    )
    fu.assemble_tile_merge(mosaic_images, output_path, merge_options)
