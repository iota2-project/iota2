# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""generate regression metrics"""

import sqlite3
from pathlib import Path

import pandas
from sklearn import metrics

from iota2.common import otb_app_bank as otb
from iota2.sampling.vector_formatting import get_total_number_of_samples
from iota2.typings.i2_types import PathLike, RegressionMetricsParameters
from iota2.vector_tools import vector_functions as vf


def extract_points_from_polygons(
    raster_file: PathLike, polygon_file: PathLike, split_field: str
) -> str:
    """Extract samples from a raster, using a shapefile"""
    # 1. compute polygon class statistics to feed sampleselection app
    class_stats_xml = f"{Path(polygon_file).with_suffix('')}.xml"
    params = {
        "in": str(raster_file),
        "vec": str(polygon_file),
        "field": split_field.lower(),
        "out": str(class_stats_xml),
    }

    app, _ = otb.create_application(
        otb.AvailableOTBApp.POLYGON_CLASS_STATISTICS, params
    )
    app.ExecuteAndWriteOutput()

    if get_total_number_of_samples(class_stats_xml) > 0:
        # 2. select all pixels in validation polygons for extraction
        points = (
            f"{Path(polygon_file).with_suffix('')}_points{Path(polygon_file).suffix}"
        )
    # No raster pixel centers within the polygons of interest
    else:
        return ""

    params = {
        "in": str(raster_file),
        "vec": str(polygon_file),
        "field": split_field.lower(),
        "out": str(points),
        "sampler": "periodic",
        "strategy": "all",
        "instats": str(class_stats_xml),
    }
    app, _ = otb.create_application(otb.AvailableOTBApp.SAMPLE_SELECTION, params)
    app.ExecuteAndWriteOutput()
    return points


def extract_samples(
    final_image: str,
    validation_data: str,
    split_field: str,
) -> str:
    """
    extract samples from final_image at validation_data positions
    and write them in output_path
    returns path of predicted file
    """
    if vf.get_geometry(validation_data, ogr_driver="SQLite") != "POINT":
        validation_points: str = extract_points_from_polygons(
            final_image, validation_data, split_field
        )
        if not validation_points:
            return ""
    else:
        validation_points = validation_data

    # 3. extract validation pixels from predicted image
    # -------------------------------------------------

    predicted_points = (
        f"{Path(validation_data).with_suffix('')}_predicted_"
        f"{Path(final_image).stem}{Path(validation_data).suffix}"
    )

    params = {
        "in": str(final_image),
        "vec": str(validation_points),
        "field": split_field,
        "out": str(predicted_points),
        "outfield": "list",
        "outfield.list.names": ["predicted"],
    }
    app, _ = otb.create_application(otb.AvailableOTBApp.SAMPLE_EXTRACTION, params)
    app.ExecuteAndWriteOutput()

    return predicted_points


def compute_metrics(
    predicted_path: PathLike,
    output_file: PathLike,
    data_field: str,
    mode: str = "w",
    column: bool = True,
) -> None:
    """
    reads original data and predicted values from database predicted_path
    compute the metrics and write to output_file as a csv file.
    """
    # read columns data_field and predicted from sqlite
    data = pandas.read_sql_query(
        f"SELECT {data_field}, predicted FROM output", sqlite3.connect(predicted_path)
    )
    y_true = data[data_field]
    y_pred = data["predicted"]
    # compute metrics on it
    # regression metrics
    # https://scikit-learn.org/stable/modules/classes.html#regression-metrics
    out = {
        "max_error": [str(metrics.max_error(y_true, y_pred))],
        "mean_absolute_error": [str(metrics.mean_absolute_error(y_true, y_pred))],
        "mean_squared_error": [str(metrics.mean_squared_error(y_true, y_pred))],
        "median_absolute_error": [str(metrics.median_absolute_error(y_true, y_pred))],
        "r2_score": [str(metrics.r2_score(y_true, y_pred))],
    }
    pandas.DataFrame(out).to_csv(output_file, index=False, mode=mode, header=column)


def generate_metrics(
    final_image: str,
    validation_data: str,
    split_field: str,
    output_file: str,
    metrics_parameters: RegressionMetricsParameters,
) -> None:
    """
    combines functions
    - extract_samples
    - compute metrics

    Parameters
    ----------
    final_image:
        Path of mosaic data
    validation_data:
        Path of shapefile of validation points to extract
    split_field:
        Field of shapefile containing fake classes used for sample selection
    output_file:
        Path of csv file in which print the metrics
    metrics_parameters:
        Parameters for computing the metrics
    """
    if Path(validation_data).exists():
        predicted_path = extract_samples(final_image, validation_data, split_field)
        if predicted_path:
            compute_metrics(
                predicted_path,
                output_file,
                metrics_parameters.data_field,
                metrics_parameters.mode,
                metrics_parameters.column,
            )


def generate_multi_tile_metrics(
    final_image: str,
    validation_data: list[str],
    split_field: str,
    output_file: str,
    metrics_parameters: RegressionMetricsParameters,
) -> None:
    """
    combines functions
    - extract_samples
    - compute metrics

    Parameters
    ----------
    final_image:
        Path of mosaic data
    validation_data:
        List of path of shapefile of validation points to extract
    split_field:
        Field of shapefile containing fake classes used for sample selection
    output_file:
        Path of csv file in which print the metrics
    metrics_parameters:
        Parameters for computing the metrics
    """
    predicted_paths = []
    for path in validation_data:
        if Path(path).exists():
            predicted_points = (
                f"{Path(path).with_suffix('')}_predicted_"
                f"{Path(final_image).stem}{Path(path).suffix}"
            )
            if Path(predicted_points).exists():
                predicted_paths.append(predicted_points)
            else:
                extract_samples_file = extract_samples(final_image, path, split_field)
                if extract_samples_file:
                    predicted_paths.append(extract_samples_file)
    if predicted_paths:
        vf.merge_db(
            f"fusion_val_{Path(final_image).stem}",
            str(
                Path(path).parent  # pylint: disable=W0631
            ),  # we check if predicted path has elements -> `path` is necessarily defined
            predicted_paths,
        )

        compute_metrics(
            # pylint: disable=W0631
            f"{Path(path).parent}/fusion_val_{Path(final_image).stem}.sqlite",
            output_file,
            metrics_parameters.data_field,
            metrics_parameters.mode,
            metrics_parameters.column,
        )


def merge_metrics(
    output_path: str,
    seeds: int,
    tile: str,
) -> None:
    """Merge the metrics files for given tile for all seeds."""
    tile_metrics = str(Path(output_path) / "final" / f"{tile}_metrics.csv")
    dataframes = []
    for seed in range(seeds):
        metrics_path = str(
            Path(output_path) / "final" / f"{tile}_seed{seed}_metrics.csv"
        )
        if Path(metrics_path).exists():
            df_metrics = pandas.read_csv(metrics_path)
            dataframes.append(df_metrics)
    if dataframes:
        cat = pandas.concat(dataframes, ignore_index=True, keys=list(range(seeds)))
        cat.loc["Average", "max_error"] = cat["max_error"].mean()
        cat.loc["Average", "mean_absolute_error"] = cat["mean_absolute_error"].mean()
        cat.loc["Average", "mean_squared_error"] = cat["mean_squared_error"].mean()
        cat.loc["Average", "median_absolute_error"] = cat[
            "median_absolute_error"
        ].mean()
        cat.loc["Average", "r2_score"] = cat["r2_score"].mean()
        cat.loc["Std", "max_error"] = cat["max_error"].std()
        cat.loc["Std", "mean_absolute_error"] = cat["mean_absolute_error"].std()
        cat.loc["Std", "mean_squared_error"] = cat["mean_squared_error"].std()
        cat.loc["Std", "median_absolute_error"] = cat["median_absolute_error"].std()
        cat.loc["Std", "r2_score"] = cat["r2_score"].std()
        cat.to_csv(tile_metrics)
