mkdir build
cd build

cmake -G "Ninja" -DCMAKE_INSTALL_PREFIX=%PREFIX% ^
      -DCMAKE_BUILD_TYPE=Release ^
      -DBUILD_SHARED_LIBS:BOOL=ON ^
      -DBUILD_EXAMPLES:BOOL=OFF ..

ninja install
