mkdir build
cd build

cmake -G "Ninja" ^
    -DCMAKE_INSTALL_PREFIX=%PREFIX% ^
    -DBUILD_SHARED_LIBS:BOOL=ON .. ^
    -DCMAKE_BUILD_TYPE=Release

ninja install
