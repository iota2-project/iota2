#!/usr/bin/env bash

set +x
set -e # Abort on error

[[ -d build ]] || mkdir build
cd build

if [[ $target_platform =~ linux.* ]]; then
    export LDFLAGS="$LDFLAGS -Wl,-rpath-link,${PREFIX}/lib"
fi

cmake -G "Ninja" \
      -D CMAKE_INSTALL_PREFIX=$PREFIX \
      -D CMAKE_MACOSX_RPATH:BOOL=ON \
      -D CMAKE_OSX_SYSROOT=${CONDA_BUILD_SYSROOT:-OFF} \
      -D BUILD_SHARED_LIBS:BOOL=ON ..
ninja install
