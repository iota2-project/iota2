"""Module containing a list of custom features"""

import numpy as np


# ############################################################################
# S2 tests utilities functions
# ############################################################################
def get_identity(self):
    """return Sentinel-2 B2"""
    return self.get_Sentinel2_B2(), []


def test_index_sum(self):
    """return the sum of Sentinel-2 B2+B4 on last axis"""
    coef = self.get_Sentinel2_B2() + self.get_Sentinel2_B4()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index(self):
    """return the sum of Sentinel-2 B2+B4"""
    coef = self.get_Sentinel2_B2() + self.get_Sentinel2_B4()
    labels = []
    return coef, labels


def duplicate_ndvi(self):
    """return the NDVI from Sentinel-2"""
    ndvi = self.get_Sentinel2_NDVI()
    labels = [f"dupndvi_{i+1}" for i in range(ndvi.shape[2])]
    return ndvi, labels


def get_ndvi(self):
    """return the NDVI from Sentinel-2"""
    coef = (self.get_Sentinel2_B8() - self.get_Sentinel2_B4()) / (
        self.get_Sentinel2_B4() + self.get_Sentinel2_B8()
    )
    labels = [f"ndvi_{i+1}" for i in range(coef.shape[2])]
    return coef, labels


def custom_function(self):
    """return Sentinel-2 B1+B2"""
    print(dir(self))
    print(self.get_Sentinel2_b1(), self.get_Sentinel2_b2())
    return self.get_Sentinel2_b1() + self.get_Sentinel2_b2()


def custom_function_inv(self):
    """return Sentinel-2 B1-B2"""
    print(dir(self))
    print(self.get_Sentinel2_b1(), self.get_Sentinel2_b2())
    return self.get_Sentinel2_b1() - self.get_Sentinel2_b2()


def get_ndsi(self):
    """return Sentinel-2 NDSI"""
    coef = (self.get_Sentinel2_B3() - self.get_Sentinel2_B11()) / (
        self.get_Sentinel2_B3() + self.get_Sentinel2_B11()
    )
    labels = [f"ndsi_{i+1}" for i in range(coef.shape[2])]
    print("out custom features")
    return coef, labels


def get_cari(self):
    """return Sentinel-2 CARI"""
    a_coeff = (
        ((self.get_Sentinel2_B5() - self.get_Sentinel2_B3()) / 150) * 670
        + self.get_Sentinel2_B4()
        + (
            self.get_Sentinel2_B3()
            - ((self.get_Sentinel2_B5() - self.get_Sentinel2_B3()) / 150) * 550
        )
    )
    b_coeff = ((self.get_Sentinel2_B5() - self.get_Sentinel2_B3()) / (150 * 150)) + 1
    coef = (self.get_Sentinel2_B5() / self.get_Sentinel2_B4()) * (
        (np.sqrt(a_coeff * a_coeff)) / (np.sqrt(b_coeff))
    )
    labels = [f"cari_{i+1}" for i in range(coef.shape[2])]

    return coef, labels


def get_red_edge2(self):
    """return Sentinel-2 red edge 2"""
    coef = (self.get_Sentinel2_B5() - self.get_Sentinel2_B4()) / (
        self.get_Sentinel2_B5() + self.get_Sentinel2_B4()
    )
    labels = [f"rededge2_{i+1}" for i in range(coef.shape[2])]
    return coef, labels


def get_gndvi(self):
    """
    compute the Green Normalized Difference Vegetation Index
    DO NOT USE this except for test as Sentinel2 has not B9
    """
    coef = (self.get_Sentinel2_B9() - self.get_Sentinel2_B3()) / (
        self.get_Sentinel2_B9() + self.get_Sentinel2_B3()
    )
    labels = [f"gndvi_{i+1}" for i in range(coef.shape[2])]
    return coef, labels


def get_soi(self):
    """
    compute the Soil Composition Index
    """
    coef = (self.get_Sentinel2_B11() - self.get_Sentinel2_B8()) / (
        self.get_Sentinel2_B11() + self.get_Sentinel2_B8()
    )
    labels = [f"soi_{i+1}" for i in range(coef.shape[2])]
    return coef, labels


def get_raw_data(self):
    """return the concatenation of raw data and masks"""
    coef, labels_refl = self.get_filled_stack()
    masks, label_masks = self.get_filled_masks()
    coef = np.concatenate((coef, masks), axis=2)
    labels = labels_refl + label_masks
    return coef, labels


# ############################################################################
# DHI INDEX
# ############################################################################


def get_cumulative_productivity(self):
    """return sum of Sentinel-2 NDVI (time axe)"""
    print("cumul")
    coef = np.sum(self.get_Sentinel2_NDVI(), axis=2)
    labels = ["cumul_prod"]
    return coef, labels


def get_minimum_productivity(self):
    """return min of Sentinel-2 NDVI (time axe)"""
    print("min")
    coef = np.min(self.get_Sentinel2_NDVI(), axis=2)
    labels = ["min_prod"]
    return coef, labels


def get_seasonal_variation(self):
    """return seasonal variation of Sentinel-2"""
    print("var")
    coef = np.std(self.get_Sentinel2_NDVI(), axis=2) / (
        np.mean(self.get_Sentinel2_NDVI(), axis=2) + 1e-6
    )

    labels = ["var_prod"]
    return coef, labels


# ###########################################################################
# Functions for testing all sensors
# ###########################################################################


def test_index_sum_l5_old(self):
    """return sum of Landsat-5 B2 + B4, old format, time axe"""
    coef = self.get_Landsat5Old_B2() + self.get_Landsat5Old_B4()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index_l5_old(self):
    """return Landsat-5 B2 + B4"""
    coef = self.get_Landsat5Old_B2() + self.get_Landsat5Old_B4()
    labels = []
    return coef, labels


def test_index_sum_l8_old(self):
    """return sum of Landsat-8 B2 + B4, old format, time axe"""
    coef = self.get_Landsat8Old_B2() + self.get_Landsat8Old_B4()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index_l8_old(self):
    """return sum of Landsat-8 B2 + B4, old format, time axe"""
    coef = self.get_Landsat8Old_B2() + self.get_Landsat8Old_B4()
    labels = []
    return coef, labels


def test_index_sum_l8(self):
    """return sum of Landsat-8 B2 + B4, time axe"""
    coef = self.get_Landsat8_B2() + self.get_Landsat8_B4()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index_l8(self):
    """return sum of Landsat-8 B2 + B4"""
    coef = self.get_Landsat8_B2() + self.get_Landsat8_B4()
    labels = []
    return coef, labels


def test_index_sum_s2_s2c(self):
    """return sum of Sentinel-2 S2C B2 + B4, time axe"""
    coef = self.get_Sentinel2S2C_B02() + self.get_Sentinel2S2C_B04()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index_s2_s2c(self):
    """return sum of Sentinel-2 S2C B2 + B4"""
    coef = self.get_Sentinel2S2C_B02() + self.get_Sentinel2S2C_B04()
    labels = []
    return coef, labels


def test_index_sum_s2_l3a(self):
    """return sum of Sentinel-2 S2C B2 + B4 across time axis"""
    coef = self.get_Sentinel2L3A_B2() + self.get_Sentinel2L3A_B4()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index_s2_l3a(self):
    """return sum of Sentinel-2 S2C B2 + B4"""
    coef = self.get_Sentinel2L3A_B2() + self.get_Sentinel2L3A_B4()
    labels = []
    return coef, labels
