#row = reference
#col = production

*********** Matrice de confusion ***********

                |  batis diffus  |zones ind et com
  batis diffus  |      0.00      |      0.00      |  batis diffus  
zones ind et com|      0.00      |      0.00      |zones ind et com

KAPPA : 0.000
OA : 0.000

    Classes      |  Precision mean  |   Rappel mean    |   F-score mean   |  Confusion max  
---------------------------------------------------------------------------------------------
  batis diffus   |      0.000       |      0.000       |      0.000       | zones ind et com, batis diffus
zones ind et com |      0.000       |      0.000       |      0.000       | zones ind et com, batis diffus
