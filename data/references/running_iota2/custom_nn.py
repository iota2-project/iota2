"""Example of a custom ANN definition"""

import torch
import torch.nn.functional as F
from torch import nn

from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetwork


class CustomANN(Iota2NeuralNetwork):
    """Same as ANN but reshape it's inputs"""

    def __init__(
        self,
        nb_class: int,
        sensors_information: dict,
        layer: int = 1,
        doy_sensors_dic: dict | None = None,
        default_std: int = 1,
        default_mean: int = 0,
        dropout=True,
    ):
        super().__init__(
            nb_class, sensors_information, doy_sensors_dic, default_std, default_mean
        )
        self.nb_class = nb_class
        self.dropout = dropout
        self.sensors_doy = doy_sensors_dic
        feat_per_date = sensors_information["sentinel2"]["nb_components"]
        nb_dates = sensors_information["sentinel2"]["nb_dates"]
        self.fc1 = nn.Linear(in_features=feat_per_date * nb_dates, out_features=16)
        self.fc2 = nn.Linear(in_features=16, out_features=12)
        self.dropout_layer = nn.Dropout(0.25)
        self.output = nn.Linear(in_features=12, out_features=nb_class)

    def forward(self, sentinel2):  # pylint: disable=W0221

        mean = self._stats["sentinel2"].mean
        x = self.nans_imputation(sentinel2, mean, self.default_mean)
        std = torch.sqrt(self._stats["sentinel2"].var)
        x = self.standardize(x, mean, std, self.default_mean, self.default_std)

        # flat x due to flat input nn first layer
        x = sentinel2.reshape(-1, x.shape[1] * x.shape[-1])
        x = self.fc1(x)
        if self.dropout:
            x = self.dropout_layer(x)
        x = F.relu(x)
        x = F.relu(self.fc2(x))
        x = self.output(x)
        return x
