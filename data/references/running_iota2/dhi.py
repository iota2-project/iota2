"""Module containing usable function as external features."""

import numpy as np

from iota2.learning.utils import I2Label


def get_cumulative_productivity(self):  # pylint: disable=C0116

    coef = np.sum(self.get_interpolated_Sentinel2_NDVI() / 1000.0, axis=2)
    return coef, [I2Label(sensor_name="cumul", feat_name="prod")]


def get_minimum_productivity(self):  # pylint: disable=C0116
    coef = np.min(self.get_interpolated_Sentinel2_NDVI() / 1000.0, axis=2)
    return coef, [I2Label(sensor_name="min", feat_name="prod")]


def get_seasonal_variation(self):  # pylint: disable=C0116
    coef = np.std(self.get_interpolated_Sentinel2_NDVI() / 1000.0, axis=2) / (
        np.mean(self.get_interpolated_Sentinel2_NDVI() / 1000.0, axis=2) + 1e-6
    )
    return coef, [I2Label(sensor_name="var", feat_name="prod")]
