"""Example of Iota2NeuralNetwork"""

import torch
from torch import nn

from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetwork


class Constante(Iota2NeuralNetwork):
    """
    add one to every linear output.
    """

    def __init__(
        self,
        nb_features: int,
        nb_class: int,
        doy_sensors_dic: dict | None = None,
        default_std: int = 1,
        default_mean: int = 0,
    ):
        super().__init__(
            nb_features, nb_class, doy_sensors_dic, default_std, default_mean
        )
        self.output = nn.Linear(
            in_features=nb_features, out_features=nb_class, bias=False
        )

    def forward(
        self,
        sentinel2,
    ):  # pylint: disable=W0221

        mean = self._stats["sentinel2"]["mean"]
        x = self.nans_imputation(sentinel2, mean, self.default_mean)
        std = torch.sqrt(self._stats["sentinel2"]["var"])
        x = self.standardize(x, mean, std, self.default_mean, self.default_std)

        # flat x due to flat input nn first layer
        x = sentinel2.reshape(-1, x.shape[1] * x.shape[-1])

        return self.output(0 * x) + torch.ones(self.output(x).size())
