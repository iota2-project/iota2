# !/usr/bin/env python3

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module containing all natively usable pytorch neural networks in iota2."""


import torch
import torch.nn.functional as F
from torch import nn

from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetwork


class ANN(Iota2NeuralNetwork):
    """Same as ANN but reshape it's inputs."""

    def __init__(
        self,
        nb_features: int,
        nb_class: int,
        layer: int = 1,
        doy_sensors_dic: dict | None = None,
    ):
        super().__init__(nb_features, nb_class, doy_sensors_dic)
        self.nb_class = nb_class
        self.layer = layer
        self.sensors_doy = doy_sensors_dic
        self.fc1 = nn.Linear(in_features=nb_features, out_features=16)
        self.fc2 = nn.Linear(in_features=16, out_features=12)
        self.output = nn.Linear(in_features=12, out_features=nb_class)

    def forward(
        self,
        sentinel2,
    ):  # pylint: disable=W0221
        # flat x due to flat input nn first layer
        data_val = sentinel2.reshape(-1, sentinel2.shape[1] * sentinel2.shape[-1])
        data_val = F.relu(self.fc1(data_val))
        data_val = F.relu(self.fc2(data_val))
        data_val = torch.softmax(self.output(data_val), dim=1)
        return data_val
